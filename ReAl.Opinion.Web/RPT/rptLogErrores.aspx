﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="rptLogErrores.aspx.cs" Inherits="ReAl.Opinion.Web.RPT.rptLogErrores" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="server">
    <link href="<%= ResolveClientUrl("~/Content/bootstrap-table/bootstrap-table.min.css") %>" rel="stylesheet">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentSection" runat="server">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Log de Errores</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>            
    <div class="row">
        <div class="col-lg-12">
                    
            <div class="panel panel-default">
                <div class="panel-heading">
                    Reporte
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <asp:Button ID="btnVerReporte" runat="server" CssClass="btn btn-success" 
								CausesValidation="true" UseSubmitBehavior="False" AccessKey="R"                                           
								Text="Ver Reporte" OnClick="btnVerReporte_Click"  />
                            <div class="btn-group" role="group">
                                <asp:Button ID="btnExportarExcel" runat="server" CssClass="btn btn-success" 
								            CausesValidation="true" UseSubmitBehavior="False" AccessKey="R"                                           
								            Text="Exportar Reporte" OnClick="btnExportarExcel_Click"  />
                                <button type="button" class="btn btn-success dropdown-toggle"
                                    data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Desplegar menú</span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <asp:LinkButton ID="lnkExportarExcel" runat="server" OnClick="btnExportarExcel_Click">A Excel</asp:LinkButton>                                        
                                    </li>
                                    <li>
                                        <asp:LinkButton ID="lnkExportarCsv" runat="server" OnClick="btnExportarCsv_Click">A Archivo Plano</asp:LinkButton>                                        
                                    </li>
                                    <li>
                                        <asp:LinkButton ID="lnkExportarPdf" runat="server" OnClick="btnExportarPdf_Click">A PDF</asp:LinkButton>                                        
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
					<div class="dataTable_wrapper">
                        <asp:GridView ID="dtgList" runat="server" OnRowCommand="dtgList_RowCommand"
                            data-toggle="table" data-show-columns="true" data-pagination="true" 
                            data-search="true" data-show-toggle="true" data-sortable="true" 
                            CssClass="table table-striped table-bordered table-hover" >                                    
                            <Columns>
                                <asp:TemplateField HeaderText="Acciones">
									<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
									<HeaderStyle></HeaderStyle>
									<ItemTemplate>
										<asp:LinkButton ID="ImbVer" CommandName="detalles" CommandArgument='<%# Bind("id") %>' runat="server" 
                                            ToolTip="Ver detalles del Registro"
                                            CssClass="btn btn-success btn-circle" Text="<i class='fa fa-indent'></i>"/>												
									</ItemTemplate>
								</asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="datos" runat="server"/>
       
    
    <!-- Detail Modal -->
    <div class="modal fade" id="currentdetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
                    
            <asp:UpdatePanel ID="updModale" runat="server">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Detalle del registro</h4>
                        </div>
                        <div class="modal-body">
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <asp:DetailsView ID="dtgDetalles" runat="server" DefaultMode="ReadOnly"
                                        CssClass="table table-striped table-bordered table-hover"
                                        FieldHeaderStyle-Font-Bold="true" AutoGenerateRows="True">                                           
                                    </asp:DetailsView>
                               </ContentTemplate>
                               <Triggers>
                                   <asp:AsyncPostBackTrigger ControlID="dtgList"  EventName="RowCommand" /> 
                               </Triggers>
                            </asp:UpdatePanel>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </ContentTemplate>
            </asp:UpdatePanel> 

        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="server">
    <script src="<%= ResolveClientUrl("~/Scripts/bootstrap-table/bootstrap-table.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Scripts/bootstrap-table/locale/bootstrap-table-es-MX.min.js") %>"></script>
    <script>

        // tooltip demo
        $('.tooltip-demo').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });

        // popover demo
        $("[data-toggle=popover]")
            .popover();
    </script>
</asp:Content>
