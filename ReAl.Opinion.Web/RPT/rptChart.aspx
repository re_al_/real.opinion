﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="rptChart.aspx.cs" Inherits="ReAl.Opinion.Web.RPT.rptChart" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="server">
    <link href="<%= ResolveClientUrl("~/bower_components/font-awesome/css/font-awesome.min.css") %>" rel="stylesheet">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentSection" runat="server">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Morris.js Charts</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Morris.js Usage
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div id="area-bar"></div>
                    
                    <div id="area-donut"></div>
                    
                    <div id="area-area"></div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-6 -->
    </div>
    <!-- /.row -->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="server">
    <script src="<%= ResolveClientUrl("~/bower_components/raphael/raphael-min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/bower_components/morrisjs/morris.min.js") %>"></script>
    
    <script>
        $(document).ready(function () {

            Morris.Bar({
                element: 'area-bar',
                data: Graph(),
                xkey: 'label',
                ykeys: ['value'],
                labels: ['Usuarios']
            });

            Morris.Donut({
                element: 'area-donut',
                data: Graph(),
                xkey: 'label',
                ykeys: ['value'],
                labels: ['Usuarios']
            });

            Morris.Area({
                element: 'area-area',
                data: Graph(),
                xkey: 'label',
                ykeys: ['value'],
                labels: ['Usuarios']
            });
        });

        function Graph() {
            var data = "";

            $.ajax({
                type: 'GET',
                url: "<%= ResolveClientUrl("~/RPT/rptHttpHandler.ashx") %>",
                dataType: 'json',
                async: false,
                contentType: "application/json; charset=utf-8",
                data: { 'GraphName': 'line' },
                success: function (result) {
                    data = result;
                },
                error: function (xhr, status, error) {
                    alert(error);
                }
            });

            return data;
        }
    </script>
</asp:Content>
