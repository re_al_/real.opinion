﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using ReAl.Opinion.Dal.Modelo;

namespace ReAl.Opinion.Web.RPT
{
    /// <summary>
    /// Descripción breve de rptHttpHandler
    /// </summary>
    public class rptHttpHandler : IHttpHandler
    {
        public class GraphData
        {
            public string label { get; set; }
            public string value { get; set; }
        }

        public class GraphDataList
        {
            public List<GraphData> ListOfGraphData { get; set; }
        }

        public void ProcessRequest(HttpContext context)
        {
            System.Collections.Specialized.NameValueCollection forms = context.Request.Form;
            string chartName = context.Request["GraphName"];

            rnVista rn = new rnVista();
            DataTable dtUsuario =
                rn.ObtenerDatos("(SELECT su.id_departamento, COUNT(*) as total FROM seg_usuario su WHERE su.id_departamento > 0 GROUP BY su.id_departamento) as aux");

            GraphDataList graphList = new GraphDataList();
            graphList.ListOfGraphData = new List<GraphData>();

            for (int i = 0; i < dtUsuario.Rows.Count; i++)
            {
                GraphData obj = new GraphData();
                obj.label = "-" + dtUsuario.Rows[i]["id_departamento"].ToString() + "-";
                obj.value = dtUsuario.Rows[i]["total"].ToString();
                graphList.ListOfGraphData.Add(obj);
            }


            int programID = 1;

            if (!string.IsNullOrEmpty(chartName))
            {
                //oper = null which means its first load.
                var jsonSerializer = new JavaScriptSerializer();
                string data = jsonSerializer.Serialize(graphList.ListOfGraphData);
                context.Response.Write(data);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}