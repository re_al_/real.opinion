﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using ClosedXML.Excel;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using ReAl.Opinion.Dal.Entidades;
using ReAl.Opinion.Utils;
using ReAl.Opinion.Web.App_Class;

namespace ReAl.Opinion.Web.RPT
{
    public partial class rptLogErrores : System.Web.UI.Page
    {
        #region Parametros de llegada

        public rptLogErrores()
        {
            dsReporte = null;
        }

        public DataSet dsReporte { set; get; }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            //Validamos el salto directo
            cSessionHandler miSesion = new cSessionHandler();
            if (miSesion.arrMenu != null)
                if (!miSesion.arrMenu.Contains(Request.AppRelativeCurrentExecutionFilePath))
                    Server.Transfer("~/Template/Dashboard.aspx");


            if (!Page.IsPostBack)
            {
                //Titulo de Pagina
                this.Title = cParametrosWeb.strNombrePagina;
            }
            else
            {
                if (dtgList.HeaderRow != null)
                    dtgList.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            throw new Exception("Holaaa2222");
            cargarReporte();

            if (!Page.IsPostBack)
            {
                //cargarListado();
            }
        }

        public class Post
        {
            public string Guid;
            public string PublishedDate;
            public string Description;
            public string Title;
            public string Link;
        }

        private void cargarReporte()
        {
            String strPath = "http://10.1.0.16:8080/elmah.axd/rss.xml";

            var rssFeed = XDocument.Load(strPath);

            IEnumerable<Post> posts = from item in rssFeed.Descendants("item")
                        select new Post
                        {
                            Guid = item.Element("guid").Value,
                            Title = item.Element("title").Value,
                            Description = item.Element("description").Value,
                            PublishedDate = item.Element("pubDate").Value,
                            Link = item.Element("link").Value
                        };


            DataTable dtPopUp = cFuncionesObjetos.ConvertToDataTable(posts);
            dtPopUp.Columns[0].ColumnName = "id";

            DataTable dtListado = dtPopUp.Copy();

            ArrayList arrHide = new ArrayList();
            arrHide.Add("guid");
            arrHide.Add("link");

            foreach (string strColumna in arrHide)
            {
                dtListado.Columns.Remove(strColumna);
            }

            DataSet dsRpt = new DataSet();
            dsRpt.Tables.Add(dtPopUp);
            dsRpt.Tables.Add(dtListado);

            this.dsReporte = dsRpt;
        }

        private void cargarListado()
        {
            try
            {
                dtgList.DataSource = this.dsReporte.Tables[1];
                dtgList.DataBind();
                dtgList.HeaderRow.TableSection = TableRowSection.TableHeader;

                for (int i = 0; i < dtgList.Columns.Count; i++)
                    dtgList.HeaderRow.Cells[i].Attributes.Add("data-sortable", "true");
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }
        }

        protected void dtgList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("detalles"))
                {
                    string strId = e.CommandArgument.ToString();

                    //Filtramos el Dataset
                    DataView dv = this.dsReporte.Tables[0].DefaultView;
                    dv.RowFilter = "id = " + strId;

                    DataTable detailTable = dv.ToTable();


                    dtgDetalles.DataSource = detailTable;
                    dtgDetalles.DataBind();
                    dtgDetalles.HeaderRow.TableSection = TableRowSection.TableHeader;

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#currentdetail').appendTo('body').modal('show');", true);
                }
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }
        }

        protected void btnVerReporte_Click(object sender, EventArgs e)
        {
            cargarListado();
        }

        protected void btnExportarExcel_Click(object sender, EventArgs e)
        {
            try
            {
                String strNombreReporte = "Reporte-" + DateTime.Now.ToString("yyyy-MM-dd HH-mm");
                using (XLWorkbook wb = new XLWorkbook())
                {
                    wb.Worksheets.Add(this.dsReporte.Tables[0]);
                    wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    wb.Style.Font.Bold = true;

                    Response.Clear();
                    Response.Buffer = true;
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment;filename= " + strNombreReporte + ".xlsx");

                    using (MemoryStream MyMemoryStream = new MemoryStream())
                    {
                        wb.SaveAs(MyMemoryStream);
                        MyMemoryStream.WriteTo(Response.OutputStream);
                        Response.Flush();
                        Response.End();
                    }
                }
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }

        }

        protected void btnExportarCsv_Click(object sender, EventArgs e)
        {
            try
            {
                String strNombreReporte = "Reporte-" + DateTime.Now.ToString("yyyy-MM-dd HH-mm");

                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=" + strNombreReporte + ".txt");
                Response.Charset = "";
                Response.ContentType = "application/text";

                DataTable dtReporte = this.dsReporte.Tables[0];

                StringBuilder sb = new StringBuilder();
                for (int k = 0; k < dtReporte.Columns.Count; k++)
                {
                    //add separator
                    sb.Append(dtReporte.Columns[k].ColumnName + '|');
                }
                //append new line
                sb.Append("\r\n");
                for (int i = 0; i < dtReporte.Rows.Count; i++)
                {
                    for (int k = 0; k < dtReporte.Columns.Count; k++)
                    {
                        //add separator
                        sb.Append(dtReporte.Rows[i][k].ToString() + '|');
                    }
                    //append new line
                    sb.Append("\r\n");
                }
                Response.Output.Write(sb.ToString());
                Response.Flush();
                Response.End();
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }

        }

        public override void VerifyRenderingInServerForm(Control control)
        {

            /* Verifies that the control is rendered */

        }

        protected void btnExportarPdf_Click(object sender, EventArgs e)
        {
            String strNombreReporte = "Reporte-" + DateTime.Now.ToString("yyyy-MM-dd HH-mm");
            Document pdfDoc = new Document(iTextSharp.text.PageSize.LETTER.Rotate(), 10, 10, 10, 10);

            try
            {
                PdfWriter.GetInstance(pdfDoc, System.Web.HttpContext.Current.Response.OutputStream);
                pdfDoc.Open();
                Paragraph p = new Paragraph();
                p.Alignment = Element.ALIGN_CENTER;
                pdfDoc.Add(p);

                throw new Exception("Holaaa");
                Font font8 = FontFactory.GetFont("ARIAL", 7);
                DataTable dt = this.dsReporte.Tables[0];
                if (dt != null)
                {
                    //Craete instance of the pdf table and set the number of column in that table  
                    PdfPTable PdfTable = new PdfPTable(dt.Columns.Count);
                    PdfPCell PdfPCell = null;

                    //Primero la cabecera
                    for (int k = 0; k < dt.Columns.Count; k++)
                    {
                        //add separator
                        PdfPCell = new PdfPCell(new Phrase(new Chunk(dt.Columns[k].ColumnName, font8)));
                        PdfTable.AddCell(PdfPCell);
                    }

                    //Ahora los datos
                    for (int rows = 0; rows < dt.Rows.Count; rows++)
                    {
                        for (int column = 0; column < dt.Columns.Count; column++)
                        {
                            PdfPCell = new PdfPCell(new Phrase(new Chunk(dt.Rows[rows][column].ToString(), font8)));
                            PdfTable.AddCell(PdfPCell);
                        }
                    }
                    //PdfTable.SpacingBefore = 15f; // Give some space after the text or it may overlap the table            
                    pdfDoc.Add(PdfTable); // add pdf table to the document   
                }
                pdfDoc.Close();
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment; filename=" + strNombreReporte + ".pdf");
                HttpContext.Current.Response.Write(pdfDoc);
                Response.Flush();
                Response.End();
                //HttpContext.Current.ApplicationInstance.CompleteRequest();  
            }
            catch (DocumentException de)
            {
                ((Main)this.Master).mostrarPopUp(this, de); ;
            }
            catch (IOException ioEx)
            {
                ((Main)this.Master).mostrarPopUp(this, ioEx); ;
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }
        }
    }
}