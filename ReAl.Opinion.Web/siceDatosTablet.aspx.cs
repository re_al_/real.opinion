﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ionic.Zip;
using ReAl.Opinion.Dal.Entidades;
using ReAl.Opinion.Dal.Modelo;
using ReAl.Opinion.Utils;
using ReAl.Opinion.Web.App_Class;

namespace ReAl.Opinion.Web
{
    public partial class siceDatosTablet : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            string summary = "Datos dispositivo...<br />";
            Boolean bProcede = false;
            try
            {
                //Obtenemos la serie
                string strExtension = "";
                string strSerie = Request["serie"];

                if (strSerie.IsNullOrEmpty())
                    throw new cSimpleException("No se ha recibido el parametro correcto");

                string strPath = Server.MapPath("~/carga/" + strSerie + "/");
                if (!System.IO.Directory.Exists(strPath))
                    System.IO.Directory.CreateDirectory(strPath);

                //Verificamos si la serie esta asignada:
                ArrayList arrColWhereTab = new ArrayList();
                arrColWhereTab.Add(entOpeTablet.Fields.imei.ToString());
                arrColWhereTab.Add(entOpeTablet.Fields.apiestado.ToString());
                ArrayList arrValWhereTab = new ArrayList();
                arrValWhereTab.Add("'" + strSerie + "'");
                arrValWhereTab.Add("'ELABORADO'");
                entOpeTablet objTab = (new rnOpeTablet()).ObtenerObjeto(arrColWhereTab, arrValWhereTab);

                if (objTab == null)
                    throw new cSimpleException("El Dispositivo Móvil con serie " + strSerie + " no se ha registrado");

                //Obtenemos la asignacion
                ArrayList arrColWhere = new ArrayList();
                arrColWhere.Add(entOpeAsignacion.Fields.id_tablet.ToString());
                arrColWhere.Add(entOpeAsignacion.Fields.apiestado.ToString());
                ArrayList arrValWhere = new ArrayList();
                arrValWhere.Add(objTab.id_tablet);
                arrValWhere.Add("'ELABORADO'");
                entOpeAsignacion objAsig = (new rnOpeAsignacion()).ObtenerObjeto(arrColWhere, arrValWhere);

                if (objAsig == null)
                    throw new cSimpleException("El Dispositivo Móvil con serie " + strSerie + " no se ha Asignado a ningun proyecto");

                cFuncionesCsv.DefaultCsvDefinition.FieldSeparator = '|';

                var bytesSegProy =
                    Encoding.UTF8
                        .GetBytes(cExportarCsv.csvSegProyecto(strPath + "seg_proyecto" + strExtension));
                var bytesSegUsu =
                    Encoding.UTF8
                        .GetBytes(cExportarCsv.csvSegUsuario(strPath + "seg_usuario" + strExtension));
                var bytesSegRol =
                    Encoding.UTF8
                        .GetBytes(cExportarCsv.csvSegRol(strPath + "seg_rol" + strExtension));
                var bytesSegUsuRes =
                    Encoding.UTF8
                        .GetBytes(cExportarCsv.csvSegUsuarioRol(strPath + "seg_usuariorestriccion" + strExtension));

                var bytesOpeTab =
                    Encoding.UTF8
                        .GetBytes(cExportarCsv.csvOpeTablet(strPath + "ope_tablet" + strExtension));
                var bytesOpeBri =
                    Encoding.UTF8
                        .GetBytes(cExportarCsv.csvOpeBrigada(strPath + "ope_brigada" + strExtension));
                var bytesOpeAsig =
                    Encoding.UTF8
                        .GetBytes(cExportarCsv.csvOpeAsignacion(strPath + "ope_asignacion" + strExtension,
                            objAsig.id_proyecto.ToString()));
                var bytesCatTipoPreg =
                    Encoding.UTF8
                        .GetBytes(cExportarCsv.csvCatTipoPregunta(strPath + "cat_tipo_pregunta" + strExtension));
                var bytesCatCat =
                    Encoding.UTF8
                        .GetBytes(cExportarCsv.csvCatCatalogo(strPath + "cat_catalogo" + strExtension));

                var bytesEncSec =
                    Encoding.UTF8
                        .GetBytes(cExportarCsv.csvEncSeccion(strPath + "enc_seccion" + strExtension,
                            objAsig.id_proyecto.ToString()));
                var bytesEncPreg =
                    Encoding.UTF8
                        .GetBytes(cExportarCsv.csvEncPregunta(strPath + "enc_pregunta" + strExtension,
                            objAsig.id_proyecto.ToString()));
                var bytesEncResp =
                    Encoding.UTF8
                        .GetBytes(cExportarCsv.csvEncRespuesta(strPath + "enc_respuesta"+ strExtension));
                var bytesEncFlu =
                    Encoding.UTF8
                        .GetBytes(cExportarCsv.csvEncFlujo(strPath + "enc_flujo" + strExtension,
                            objAsig.id_proyecto.ToString()));
                
                Response.Clear();
                Response.BufferOutput = false; // for large files...
                string archiveName = "CSV_" + strSerie + ".zip";
                Response.ContentType = "application/zip";
                Response.AddHeader("content-disposition", "filename=" + archiveName);

                using (ZipFile zip = new ZipFile())
                {
                    // filesToInclude is an IEnumerable<String>, like String[] or List<String>                    
                    zip.AddEntry("seg_proyecto" + strExtension, bytesSegProy);
                    zip.AddEntry("seg_usuario" + strExtension, bytesSegUsu);
                    zip.AddEntry("seg_rol" + strExtension, bytesSegRol);
                    zip.AddEntry("seg_usuariorestriccion" + strExtension, bytesSegUsuRes);

                    zip.AddEntry("ope_tablet" + strExtension, bytesOpeTab);
                    zip.AddEntry("ope_brigada" + strExtension, bytesOpeBri);
                    zip.AddEntry("ope_asignacion" + strExtension, bytesOpeAsig);
                    zip.AddEntry("cat_tipo_pregunta" + strExtension, bytesCatTipoPreg);
                    zip.AddEntry( "cat_catalogo" + strExtension, bytesCatCat);

                    zip.AddEntry("enc_seccion" + strExtension, bytesEncSec);
                    zip.AddEntry("enc_pregunta" + strExtension, bytesEncPreg);
                    zip.AddEntry("enc_respuesta" + strExtension, bytesEncResp);
                    zip.AddEntry("enc_flujo" + strExtension, bytesEncFlu);

                    // Add a file from a string                
                    zip.Save(Response.OutputStream);
                }
                // Response.End();  // no! See http://stackoverflow.com/questions/1087777
                Response.Close();
            }
            catch (Exception exp)
            {
                if (exp.GetType() == typeof(cSimpleException))
                    summary += exp.Message + "<br />";
                else
                {
                    summary += exp.Message + "<br />";
                    summary += exp.StackTrace + "<br />";
                }
            }
            finally
            {
                if (bProcede)
                    lblRes.Text = "OK";
                else
                    lblRes.Text = summary;
            }
        }
    }
}