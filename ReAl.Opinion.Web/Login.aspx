﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="ReAl.Opinion.Web.Login" %>

<%@ Import Namespace="System.Web.Optimization" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="R. Alonzo Vera Arias">

    <title>Instituto Nacional de Estadistica</title>

    <%-- Style Section --%>
    <%: Styles.Render("~/bundles/BootstrapCss")  %>
    
    <!-- Custom Fonts -->
    <link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" type="image/x-icon" href="<%= ResolveClientUrl("~/images/favicon.ico") %>" />

    <style>
        .carousel { z-index: -99; } /* keeps this behind all content */
        .carousel.carousel-fade .item {
            transition-property: opacity;
            opacity:0.6;
        }

        .carousel.carousel-fade .active.item {
          transition-property: opacity;
          opacity:0.6;
        }

        .carousel .item {
            position: fixed; 
            width: 100%; height: 100%;
            -webkit-transition: opacity 2s ease-in-out;
            -moz-transition: opacity 2s ease-in-out;
            -ms-transition: opacity 2s ease-in-out;
            -o-transition: opacity 2s ease-in-out;
            transition: opacity 1s;
        }
        .carousel .one {
            background: url(images/carousel/1.jpg);
            background-size: cover;
            -moz-background-size: cover;
        }
        .carousel .two {
            background: url(images/carousel/2.jpg);
            background-size: cover;
            -moz-background-size: cover;
        }
        .carousel .three {
            background: url(images/carousel/3.jpg);
            background-size: cover;
            -moz-background-size: cover;
        }
        .carousel .active.left {
            left:0;
            opacity:0;
            z-index:2;
            -webkit-transition: opacity 0.5s ease-in-out !important;
            -moz-transition: opacity 0.5s ease-in-out !important;
            -ms-transition: opacity 0.5s ease-in-out !important;
            -o-transition: opacity 0.5s ease-in-out !important;
            transition: opacity 0.3s ease-in-out !important;
        } 
    </style>
</head>
<body>
    <div id="myCarousel" class="carousel container slide carousel-fade">
        <div class="carousel-inner">            
            <div class="active item one"></div>
            <div class="item two"></div>
            <div class="item three"></div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <asp:Label ID="lblTitulo" runat="server" Text="Inicio de Sesion"></asp:Label>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <form id="frmLogin" runat="server" enctype="multipart/form-data" method="post" role="form">
                            <fieldset>
                                <div class="form-group" align="center">
									<a href="<%= ResolveClientUrl("~/apk/EncuestaPercepcion.apk") %>">
										<img src="images/splash-single.png" class="img-responsive" alt="Responsive image">
									</a>
                                </div>
                                <div class="form-group">
                                    <asp:TextBox ID="txtUsuario" runat="server" CssClass="form-control" PlaceHolder="Usuario"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <asp:TextBox ID="txtPass" runat="server" CssClass="form-control" TextMode="Password" PlaceHolder="Password"></asp:TextBox>
                                </div>
                                <asp:Panel ID="pnlError" runat="server" CssClass="alert alert-danger alert-dismissable" Visible="False">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                                </asp:Panel>
                                <!-- Change this to a button or input when using this as a form -->
                                
                                <asp:Button ID="btnLogin" runat="server" Text="Ingresar" CssClass="btn btn-lg btn-success btn-block" Class="btn btn-lg btn-success btn-block" OnClick="btnLogin_Click" />
                                
                            </fieldset>
                        </form>                        
                    </div>
                </div>
            </div>
        </div>        
    </div>    

    <!-- jQuery -->
    <script src="bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="bower_components/dist/js/sb-admin-2.js"></script>
    
    <script type="text/javascript">
      $(document).ready(function() {
          $('.carousel').carousel({ interval: 5000 });
      });
    </script>
</body>
</html>
