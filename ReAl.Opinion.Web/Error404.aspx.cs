﻿using System;
using System.Web;
using System.Web.Security;

using ReAl.Opinion.Web.App_Class;

namespace ReAl.Opinion.Web
{
    public partial class Error404 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //Titulo de Pagina
                this.Title = cParametrosWeb.strNombrePagina;

            }
        }
    }
}