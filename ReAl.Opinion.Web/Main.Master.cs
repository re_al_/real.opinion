﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using ReAl.Opinion.BitBucket;
using ReAl.Opinion.BitBucket.Controllers;
using ReAl.Opinion.BitBucket.Models;

using ReAl.Opinion.Dal.Entidades;
using ReAl.Opinion.Dal.Modelo;
using ReAl.Opinion.Utils;
using ReAl.Opinion.Web.App_Class;

namespace ReAl.Opinion.Web
{
    public partial class Main : System.Web.UI.MasterPage
    {
        protected string _navColor = "";
        protected string _navColorFont = "#FFFFFF";

        protected void Page_Load(object sender, EventArgs e)
        {
            //Tratamos de Obtener el Proyecto del Usuario
            try
            {
                rnSegProyecto rnPro = new rnSegProyecto();
                cSessionHandler miSesion = new cSessionHandler();
                entSegProyecto objPro = rnPro.ObtenerObjeto(miSesion.appRestriccion.id_proyecto);

                this.Page.Title = cParametrosWeb.strNombrePagina + " (" + objPro.codigo + ")";
                lblTituloPagina.Text = cParametrosWeb.strNombrePagina + " (" + objPro.codigo + ")";
                _navColor = objPro.color_web;
                _navColorFont = objPro.color_font;

                lblUsuario.Text = "Bienvenido " + miSesion.appUsuario.login;
            }
            catch (Exception)
            {
                this.Page.Title = cParametrosWeb.strNombrePagina;
                lblTituloPagina.Text = cParametrosWeb.strNombrePagina;
            }

            CargarMenu();
        }

        private void CargarMenu()
        {
            try
            {
                //Primero el PADER ul
                HtmlGenericControl ulControlPadre = new HtmlGenericControl();
                ulControlPadre.Attributes["class"] = "nav";
                ulControlPadre.Attributes["id"] = "side-menu";
                ulControlPadre.TagName = "ul";
                divMenu.Controls.Add(ulControlPadre);

                //El Primer Hijo: Busqueda
                HtmlGenericControl liBuscarControl = new HtmlGenericControl();
                liBuscarControl.Attributes["class"] = "sidebar-search";
                liBuscarControl.TagName = "li";
                ulControlPadre.Controls.Add(liBuscarControl);

                HtmlGenericControl divBuscarControl = new HtmlGenericControl();
                divBuscarControl.Attributes["class"] = "input-group custom-search-form";
                divBuscarControl.TagName = "div";
                liBuscarControl.Controls.Add(divBuscarControl);

                HtmlGenericControl inputBuscarControl = new HtmlGenericControl();
                inputBuscarControl.Attributes["class"] = "form-control";
                inputBuscarControl.Attributes["placeholder"] = "Buscar";
                inputBuscarControl.TagName = "input";
                divBuscarControl.Controls.Add(inputBuscarControl);


                HtmlGenericControl spanBuscarControl = new HtmlGenericControl();
                spanBuscarControl.Attributes["class"] = "input-group-btn";
                spanBuscarControl.TagName = "span";
                divBuscarControl.Controls.Add(spanBuscarControl);


                HtmlButton botonBuscarControl = new HtmlButton();
                botonBuscarControl.Attributes["class"] = "btn btn-default";
                botonBuscarControl.Attributes["type"] = "button";
                botonBuscarControl.InnerHtml = "<i class='fa fa-search'></i>";
                spanBuscarControl.Controls.Add(botonBuscarControl);


                //Ahora los Hijos
                DataTable dtMenu = ObtenerMenu();

                DataTable dtMenuNivel1 = ObtenerMenuPrimerNivel(dtMenu);
                foreach (DataRow drMenu in dtMenuNivel1.Rows)
                {
                    HtmlGenericControl liControl = new HtmlGenericControl();
                    liControl.TagName = "li";
                    ulControlPadre.Controls.Add(liControl);


                    if (String.IsNullOrEmpty(drMenu["enlace"].ToString()))
                    {
                        //Tiene hijos
                        HtmlGenericControl aControl = new HtmlGenericControl();
                        aControl.Attributes["href"] = "#";
                        aControl.InnerHtml = "<i class='" + drMenu["icono"] + "'></i> " + drMenu["nombre"] + " <span class='fa arrow'></span>";
                        aControl.TagName = "a";
                        liControl.Controls.Add(aControl);

                        HtmlGenericControl ulControlHijos = new HtmlGenericControl();
                        ulControlHijos.Attributes["class"] = "nav nav-second-level";
                        ulControlHijos.TagName = "ul";
                        liControl.Controls.Add(ulControlHijos);

                        //Añadimos los hijos
                        DataTable dtMenuNivel2 = ObtenerMenuSegundoNivel(dtMenu, int.Parse(drMenu["id_pagina"].ToString()));
                        foreach (DataRow drHijos in dtMenuNivel2.Rows)
                        {
                            HtmlGenericControl liControlHijos = new HtmlGenericControl();
                            liControlHijos.TagName = "li";
                            ulControlHijos.Controls.Add(liControlHijos);

                            HtmlGenericControl aControlHijo = new HtmlGenericControl();
                            aControlHijo.Attributes["href"] = ResolveClientUrl(drHijos["enlace"].ToString());
                            aControlHijo.InnerHtml = drHijos["nombre"].ToString();
                            aControlHijo.TagName = "a";
                            liControlHijos.Controls.Add(aControlHijo);
                        }
                    }
                    else
                    {
                        //No tiene Hijos
                        HtmlGenericControl aControl = new HtmlGenericControl();
                        aControl.Attributes["href"] = ResolveClientUrl(drMenu["enlace"].ToString());
                        aControl.InnerHtml = "<i class='" + drMenu["icono"] + "'></i> " + drMenu["nombre"] + "</a>";
                        aControl.TagName = "a";
                        liControl.Controls.Add(aControl);
                    }
                }
            }
            catch (Exception exp)
            {
                mostrarPopUp(this.Page, exp);
            }
        }

        private DataTable ObtenerMenu()
        {            
            rnVista rn = new rnVista();
            cSessionHandler miSesion = new cSessionHandler();

            if (miSesion.dtMenu == null)
            {
                ArrayList arrColWhere = new ArrayList();
                arrColWhere.Add(entSegRol.Fields.id_rol.ToString());
                ArrayList arrValWhere = new ArrayList();
                arrValWhere.Add(miSesion.appRol.id_rol);

                DataTable dtMenu = rn.ObtenerDatos("vw_seg_menu_lis", arrColWhere, arrValWhere);

                miSesion.dtMenu = dtMenu;
                return dtMenu;
            }
            else
            {
                return miSesion.dtMenu;
            }
        }
        
        private DataTable ObtenerMenuPrimerNivel(DataTable dtMenu)
        {
            DataView dvMenu = dtMenu.DefaultView;
            dvMenu.RowFilter = "id_pagina_padre = 0";
            dvMenu.Sort = "orden";
            return dvMenu.ToTable();
        }

        private DataTable ObtenerMenuSegundoNivel(DataTable dtMenu, int idPadre)
        {
            DataView dvMenu = dtMenu.DefaultView;
            dvMenu.RowFilter = "id_pagina_padre = " + idPadre;
            dvMenu.Sort = "orden";
            return dvMenu.ToTable();
        }

        public void mostrarPopUp(Page anfitrion, Exception exp)
        {
            try
            {
                if (exp.GetType() == typeof (cSimpleException))
                {
                    lblErrorMensaje.Text = exp.Message;
                    lblErrorDetalle.Text = "";
                }
                else
                {
                    lblErrorMensaje.Text = exp.Message;
                    lblErrorDetalle.Text = exp.StackTrace;
                }
                

                ScriptManager.RegisterStartupScript(anfitrion, anfitrion.GetType(), "myModalError", "$('#myModalError').appendTo('body').modal('show');", true);                
            }
            catch (Exception e2)
            {
                Debug.Print(e2.Message);
            }
        }

        protected void btnErrorReportar_Click(object sender, EventArgs e)
        {
            try
            {
                Client cli = new Client(cParametrosIssues.strBitBucketUser, cParametrosIssues.strBitBucketPassword);

                CreateIssueModel miIssue = new CreateIssueModel();
                miIssue.Title = lblErrorMensaje.Text;
                miIssue.Content = lblErrorDetalle.Text;
                miIssue.Kind = "bug";
                miIssue.Priority = "critical";
                RepositoryController repo = new RepositoryController(cli, new UserController(cli, cParametrosIssues.strBitBucketUser), cParametrosIssues.strBitBucketRepository);
                repo.Issues.Create(miIssue);

            }
            catch (Exception exp)
            {
                Debug.Print(exp.Message);
            }
        }

        protected void btnRecargar_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl);
        }

        protected void LoginStatus_LoggedOut(object sender, EventArgs e)
        {
            Session.Clear();
            Session.Abandon();
            FormsAuthentication.SignOut();
            FormsAuthentication.RedirectToLoginPage();
        }

        protected void lnkCerrar_Click(object sender, EventArgs e)
        {
            Session.Clear();
            Session.Abandon();
            FormsAuthentication.SignOut();
            FormsAuthentication.RedirectToLoginPage();
        }

        protected void lnkUserProfile_Click(object sender, EventArgs e)
        {
            cSessionHandler miSesion = new cSessionHandler();
            String strUrl = "~/SEG/docSegUsuario.aspx?id=" + miSesion.appUsuario.id_usuario;
            Response.Redirect(strUrl);
        }

        protected void lnkCambiarPass_Click(object sender, EventArgs e)
        {
            cSessionHandler miSesion = new cSessionHandler();
            Response.Redirect("~/SEG/docSegUsuarioPassword.aspx?id=" + miSesion.appUsuario.id_usuario);
        }        
    }
}