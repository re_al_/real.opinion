﻿#region 
/***********************************************************************************************************
	NOMBRE:       lstEncPregunta
	DESCRIPCION:
		Clase que define los metodos y propiedades de la página docEncPregunta

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        09/07/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion

#region

using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReAl.Opinion.Dal.Entidades;
using ReAl.Opinion.Dal.Modelo;
using ReAl.Opinion.Web.App_Class;
using Microsoft.Ajax.Utilities;


#endregion

namespace ReAl.Opinion.Web.ENC
{
    public partial class docEncRespuesta : Page
    {
		#region Parametros de llegada
        public int localid
        {
            get
            {
                if (Request["id"] == null)
                {
                    return 0;
                }
                return int.Parse(Request["id"].ToString());
            }
        }
        #endregion

		protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //Titulo de Pagina
                this.Title = cParametrosWeb.strNombrePagina;

                //Otorgamos valores a los Labels
                CargarDdlPregunta();
                //Colocamos los mensajes de Error
                
                if (localid > 0)
                {
                    llenarControles();
                }	
            }
            
        }
        
        private void CargarDdlPregunta()
        {
            try
            {
                //Funcion que inicializa los controles
                rnVista rn = new rnVista();
                cSessionHandler miSession = new cSessionHandler();
                DataTable dt = rn.ObtenerDatos("vw_enc_pregunta_dropdownlist");
                DataView dv = dt.DefaultView;
                dv.RowFilter = "id_proyecto = " + miSession.appRestriccion.id_proyecto;

                ddlid_pregunta.DataValueField = entEncPregunta.Fields.id_pregunta.ToString();
                ddlid_pregunta.DataTextField = entEncPregunta.Fields.pregunta.ToString();
                ddlid_pregunta.DataSource = dv.ToTable();
                ddlid_pregunta.DataBind();
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp);
            }
        }

		protected void llenarControles()
        {
			try
			{
				//Funcion que inicializa los controles
				rnEncRespuesta rnModificar = new rnEncRespuesta();
				entEncRespuesta objPag = rnModificar.ObtenerObjeto(localid);

				if(objPag != null)
				{
                    //Asignamos los valores
				    ddlid_pregunta.SelectedValue = objPag.id_pregunta.ToString();
                    txtcodigo.Text = objPag.codigo;
                    txtrespuesta.Text = objPag.respuesta;
                    
                }
			}
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }
        }

        protected void limpiarControles()
        {
            //Funcion que inicializa los controles
            txtcodigo.Text = "";
            txtrespuesta.Text = "";            
        }

        protected void btnAtras_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/OPE/lstEncRespuesta.aspx");
        }
        
        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            limpiarControles();
        }
        
		protected void btnRegistrar_Click(object sender, EventArgs e)
        {
			Boolean bProcede = false;
            
			try
			{
				Page.Validate("cabecera");
                if (!Page.IsValid) return;

			    guardarRegistro();

                bProcede = true;
			}
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this,exp);
            }
			if (bProcede)
                Response.Redirect("~/ENC/lstEncRespuesta.aspx");
        }

        protected void btnRegistrarNuevo_Click(object sender, EventArgs e)
        {
            Boolean bProcede = false;

            try
            {
                Page.Validate("cabecera");
                if (!Page.IsValid) return;

                guardarRegistro();

                bProcede = true;
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp);
            }
            if (bProcede)
                limpiarControles();
        }



        private void guardarRegistro()
        {
            rnEncRespuesta rnModificar = new rnEncRespuesta();
            entEncRespuesta objPag = new entEncRespuesta();
            cSessionHandler miSesion = new cSessionHandler();

            if (localid == 0)
            {
                //Apropiamos los valores para INSERTAR
                objPag = new entEncRespuesta();


                objPag.id_pregunta = rnModificar.GetColumnType(ddlid_pregunta.SelectedValue, entEncRespuesta.Fields.id_pregunta);
                objPag.codigo = txtcodigo.Text;
                objPag.respuesta = txtrespuesta.Text;
                objPag.apiestado = "ELABORADO";
                objPag.usucre = miSesion.appUsuario.login;

                rnModificar.Insert(objPag);


            }
            else
            {
                //Apropiamos los valores para MODIFICAR
                objPag = rnModificar.ObtenerObjeto(localid);
                objPag.id_pregunta = rnModificar.GetColumnType(ddlid_pregunta.SelectedValue, entEncRespuesta.Fields.id_pregunta);
                objPag.codigo = txtcodigo.Text;
                objPag.respuesta = txtrespuesta.Text;
                objPag.apiestado = "ELABORADO";
                objPag.usumod = miSesion.appUsuario.login;

                rnModificar.Update(objPag);
            }
        }     
    }
}
