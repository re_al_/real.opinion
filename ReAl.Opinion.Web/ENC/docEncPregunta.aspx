<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="docEncPregunta.aspx.cs" Inherits="ReAl.Opinion.Web.ENC.docEncPregunta" Title="docEncPregunta" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %> 

<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="server">
    <link href="<%= ResolveClientUrl("~/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css") %>" rel="stylesheet">
    <link href="<%= ResolveClientUrl("~/bower_components/datatables-responsive/css/dataTables.responsive.css") %>" rel="stylesheet">
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentSection" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Registro de Preguntas</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>            
            <div class="row">
                <div class="col-lg-12">
                    
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Registro de Preguntas
                        </div>
                        <div class="panel-body">
							<div>
								<asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="cabecera" 
                                    DisplayMode="BulletList" ShowMessageBox="True" 
									class="alert alert-danger" CssClass="alert alert-danger"
							        HeaderText="Debe considerar las siguientes observaciones antes de continuar" />                                
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <!-- Botones de Accion -->
                                        <asp:LinkButton ID="btnAtras" runat="server" Text="<i class='fa fa-arrow-left'></i>" ToolTip="Atras"
                                            CssClass="btn btn-primary btn-circle" class="btn btn-default btn-circle" OnClick="btnAtras_Click"/>
                                    
                                        <asp:LinkButton ID="btnGuardar" runat="server" Text="<i class='fa fa-save'></i>" ToolTip="Guardar registro"
                                            CssClass="btn btn-primary btn-circle" class="btn btn-default btn-circle" 
                                            OnClick="btnRegistrar_Click" OnClientClick="return confirmarRegistro();"/>
                                    
                                        <asp:LinkButton ID="btnGuardarNuevo" runat="server" Text="<i class='fa fa-repeat'></i>" ToolTip="Guardar y Nuevo registro"
                                            CssClass="btn btn-primary btn-circle" class="btn btn-default btn-circle" OnClick="btnRegistrarNuevo_Click"/>
                                    </div>

                                    <div class="form-group">
										<label id="lblid_seccion" runat="server">Secci&oacute;n:</label>
                                        <asp:DropDownList ID="ddlid_seccion" runat="server" class="form-control" CssClass="form-control"></asp:DropDownList>
									</div>
                                    
                                    <div class="form-group">
										<label id="lblcodigo_pregunta" runat="server">C&oacute;digo Pregunta:</label>
										<asp:TextBox id="txtcodigo_pregunta" Class="form-control" CssClass="form-control" runat="server"></asp:TextBox>
									</div>

                                    <div class="form-group">
                                        <label id="lblPregunta" runat="server">Pregunta:</label>
                                        <CKEditor:CKEditorControl ID="txtPregunta" BasePath="../Content/ckeditor/" 
                                            Skin="kama" runat="server" 
                                            Toolbar="Source
                                            Undo|Redo|-|Find|Replace|-|SelectAll|RemoveFormat
                                            Bold|Italic|Underline|Strike|-|Subscript|Superscript
                                            TextColor
                                            NumberedList|BulletedList" ></CKEditor:CKEditorControl>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label id="Label1" runat="server">Instrucci&oacute;n:</label>
                                        <CKEditor:CKEditorControl ID="txtInstruccion" BasePath="../Content/ckeditor/" 
                                            Skin="kama" runat="server" 
                                            Toolbar="Source
                                            Undo|Redo|-|Find|Replace|-|SelectAll|RemoveFormat
                                            Bold|Italic|Underline|Strike|-|Subscript|Superscript
                                            TextColor
                                            NumberedList|BulletedList" ></CKEditor:CKEditorControl>
                                    </div>
									
                                    <div class="form-group">
                                        <label id="lblvariable" runat="server">Variable:</label>
                                        <asp:DropDownList ID="ddlvariable" runat="server" class="form-control" 
                                            CssClass="form-control" AutoPostBack="True"></asp:DropDownList>
                                    </div>

                                    <div class="form-group">
										<label id="lblid_tipo_pregunta" runat="server">Tipo de Pregunta:</label>
                                        <asp:DropDownList ID="ddlid_tipo_pregunta" runat="server" class="form-control" 
                                            CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="ddlid_tipo_pregunta_SelectedIndexChanged"></asp:DropDownList>
									</div>
                                    
                                    <div class="form-group">
										<label id="lblminimo" runat="server">Valor M&iacute;nimo:</label>
										<asp:TextBox id="txtminimo" Class="form-control" CssClass="form-control" runat="server" MaxLength="3" TextMode="Number"></asp:TextBox>
									</div>

                                    <div class="form-group">
										<label id="lblmaximo" runat="server">Valor M&aacute;ximo:</label>
										<asp:TextBox id="txtmaximo" Class="form-control" CssClass="form-control" runat="server" MaxLength="3" TextMode="Number"></asp:TextBox>
									</div>
                                    
                                    <div class="form-group">
										<label id="lbllongitud" runat="server">Longitud:</label>
										<asp:TextBox id="txtlongitud" Class="form-control" CssClass="form-control" runat="server" MaxLength="3" TextMode="Number" ></asp:TextBox>
									</div>
                                    
                                    <div class="form-group">
										<label id="lblrespuestas" runat="server">Respuestas:</label>
										<asp:TextBox id="txtrespuestas" Class="form-control" CssClass="form-control" runat="server" TextMode="MultiLine" ></asp:TextBox>
									</div>
                                    
                                    <div class="form-group">
                                        <label id="lblmostrar_ventana" runat="server">Mostrar en Ventana PopUp al inicio del Nivel:</label>                                     
                                        <div class="checkbox checkbox-slider--b">
	                                        <label>
	                                            <input ID="chkMostrarVentana" runat="server" type="checkbox"/><span>Mostrar Ventana</span>
	                                        </label>
                                        </div>
                                    </div>
                                </div>
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>
			<asp:HiddenField ID="hdnIdDatos" runat="server" />
        </ContentTemplate>	        
    </asp:UpdatePanel>
    
    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="server">
    <script src="<%= ResolveClientUrl("~/bower_components/datatables/media/js/jquery.dataTables.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js") %>"></script>
	
	<script>
	    var confirmarRegistro = function () {
	        BootstrapDialog.confirm({
	            title: 'Confirmar acci&oacute;n',
	            message: '¿Est&aacute; seguro que desea guardar la informaci&oacute;n?',
	            type: BootstrapDialog.TYPE_PRIMARY, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
	            draggable: true, // <-- Default value is false
	            btnCancelLabel: 'No estoy seguro', // <-- Default value is 'Cancel',
	            btnOKLabel: 'De acuerdo', // <-- Default value is 'OK',
	            btnOKClass: 'btn-success', // <-- If you didn't specify it, dialog type will be used,
	            callback: function (result) {
	                // result will be true if button was click, while it will be false if users close the dialog directly.
	                if (result) {
	                    <%=Page.ClientScript.GetPostBackEventReference(btnGuardar, string.Empty)%>;
	                } else {
	                    return false;
	                }
	            }
	        });

	    };        
	</script> 
</asp:Content>

