<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="docEncFlujo.aspx.cs" Inherits="ReAl.Opinion.Web.ENC.docEncFlujo" Title="docEncPregunta" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %> 

<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="server">
    <link href="<%= ResolveClientUrl("~/Content/bootstrap-table/bootstrap-table.min.css") %>" rel="stylesheet">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentSection" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Registro de Saltos</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>            
            <div class="row">
                <div class="col-lg-12">                    
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Registro de Saltos
                        </div>
                        <div class="panel-body">                            
							<div class="row">
                                <div>
								    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="cabecera" 
                                        DisplayMode="BulletList" ShowMessageBox="True" 
									    class="alert alert-danger" CssClass="alert alert-danger"
							            HeaderText="Debe considerar las siguientes observaciones antes de continuar" />                                
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <!-- Botones de Accion -->
                                        <asp:LinkButton ID="btnAtras" runat="server" Text="<i class='fa fa-arrow-left'></i>" ToolTip="Atras"
                                            CssClass="btn btn-primary btn-circle" class="btn btn-default btn-circle" OnClick="btnAtras_Click"/>
                                    
                                        <asp:LinkButton ID="btnGuardar" runat="server" Text="<i class='fa fa-save'></i>" ToolTip="Guardar registro"
                                            CssClass="btn btn-primary btn-circle" class="btn btn-default btn-circle" 
                                            OnClick="btnRegistrar_Click" OnClientClick="return confirmarRegistro();"/>
                                    
                                        <asp:LinkButton ID="btnGuardarNuevo" runat="server" Text="<i class='fa fa-repeat'></i>" ToolTip="Guardar y Nuevo registro"
                                            CssClass="btn btn-primary btn-circle" class="btn btn-default btn-circle" OnClick="btnRegistrarNuevo_Click"/>
                                    </div>

                                    <div class="form-group">
                                        <label id="lblorigen" runat="server">Origen:</label>
                                        <asp:DropDownList ID="ddlorigen" runat="server" class="form-control" 
                                            CssClass="form-control" ></asp:DropDownList>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label id="lbldestino" runat="server">Origen:</label>
                                        <asp:DropDownList ID="ddldestino" runat="server" class="form-control" 
                                            CssClass="form-control" ></asp:DropDownList>
                                    </div>

                                    <div class="form-group">
										<label id="lblregla" runat="server">Regla:</label>
										<asp:TextBox id="txtregla" Class="form-control" CssClass="form-control" runat="server"  ></asp:TextBox>
									</div>
                                    
									

                                </div>
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-lg-12">  
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Preguntas de la Boleta
                        </div>
                        <div class="panel-body">                            
					        <div class="dataTable_wrapper">
                                <asp:GridView ID="dtgList" runat="server" AutoGenerateColumns="False"
							data-toggle="table" data-show-columns="true" data-pagination="true" 
                            data-search="true" data-show-toggle="true" data-sortable="true" 
                            data-page-size="10" data-pagination-v-align="both"
							DataKeyNames="" 
                            CssClass="table table-striped table-bordered table-hover" >
                                    <Columns>
                                        <asp:BoundField ReadOnly="True" DataField="id_nivel" HeaderText="Nivel" ShowHeader="false" HtmlEncode="False" >
									        <ItemStyle HorizontalAlign="Justify" ></ItemStyle>
									        <HeaderStyle HorizontalAlign="Center" ></HeaderStyle>
								        </asp:BoundField>
                                
                                        <asp:BoundField ReadOnly="True" DataField="codigo_pregunta" HeaderText="Codigo" ShowHeader="false" HtmlEncode="False" >
									        <ItemStyle HorizontalAlign="Justify" ></ItemStyle>
									        <HeaderStyle HorizontalAlign="Center" ></HeaderStyle>
								        </asp:BoundField>
                                
                                        <asp:BoundField ReadOnly="True" DataField="pregunta" HeaderText="Pregunta" ShowHeader="false" HtmlEncode="False" >
									        <ItemStyle HorizontalAlign="Justify" ></ItemStyle>
									        <HeaderStyle HorizontalAlign="Center" ></HeaderStyle>
								        </asp:BoundField>
                                
                                        <asp:BoundField ReadOnly="True" DataField="instruccion" HeaderText="Instruccion" ShowHeader="false" HtmlEncode="False" >
									        <ItemStyle HorizontalAlign="Justify" ></ItemStyle>
									        <HeaderStyle HorizontalAlign="Center" ></HeaderStyle>
								        </asp:BoundField>
                                
                                        <asp:BoundField ReadOnly="True" DataField="tipo_pregunta" HeaderText="Tipo" ShowHeader="false" HtmlEncode="False" >
									        <ItemStyle HorizontalAlign="Justify" ></ItemStyle>
									        <HeaderStyle HorizontalAlign="Center" ></HeaderStyle>
								        </asp:BoundField>
                                
                                        <asp:BoundField ReadOnly="True" DataField="respuestas" HeaderText="Respuestas" ShowHeader="false" HtmlEncode="False" >
									        <ItemStyle HorizontalAlign="Justify" ></ItemStyle>
									        <HeaderStyle HorizontalAlign="Center" ></HeaderStyle>
								        </asp:BoundField>
                                
                                        <asp:BoundField ReadOnly="True" DataField="consistencia" HeaderText="Consistencia" ShowHeader="false" HtmlEncode="False" >
									        <ItemStyle HorizontalAlign="Justify" ></ItemStyle>
									        <HeaderStyle HorizontalAlign="Center" ></HeaderStyle>
								        </asp:BoundField>
                                
                                        <asp:BoundField ReadOnly="True" DataField="mensaje" HeaderText="Alerta" ShowHeader="false" HtmlEncode="False" >
									        <ItemStyle HorizontalAlign="Justify" ></ItemStyle>
									        <HeaderStyle HorizontalAlign="Center" ></HeaderStyle>
								        </asp:BoundField>
                                
                                        <asp:BoundField ReadOnly="True" DataField="flujo" HeaderText="Saltos" ShowHeader="false" HtmlEncode="False" >
									        <ItemStyle HorizontalAlign="Justify" ></ItemStyle>
									        <HeaderStyle HorizontalAlign="Center" ></HeaderStyle>
								        </asp:BoundField>


                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<asp:HiddenField ID="hdnIdDatos" runat="server" />
        </ContentTemplate>	        
    </asp:UpdatePanel>
    
    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="server">
    <script src="<%= ResolveClientUrl("~/Scripts/bootstrap-table/bootstrap-table.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Scripts/bootstrap-table/locale/bootstrap-table-es-MX.min.js") %>"></script>	
	<script>
        var confirmarRegistro = function () {
            BootstrapDialog.confirm({
                title: 'Confirmar acci&oacute;n',
                message: '¿Est&aacute; seguro que desea guardar la informaci&oacute;n?',
                type: BootstrapDialog.TYPE_PRIMARY, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                draggable: true, // <-- Default value is false
                btnCancelLabel: 'No estoy seguro', // <-- Default value is 'Cancel',
                btnOKLabel: 'De acuerdo', // <-- Default value is 'OK',
                btnOKClass: 'btn-success', // <-- If you didn't specify it, dialog type will be used,
                callback: function (result) {
                    // result will be true if button was click, while it will be false if users close the dialog directly.
                    if (result) {
                        <%=Page.ClientScript.GetPostBackEventReference(btnGuardar, string.Empty)%>;
                    } else {
                        return false;
                    }
                }
            });

        };        
    </script> 
</asp:Content>

