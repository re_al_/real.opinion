﻿#region 
/***********************************************************************************************************
	NOMBRE:       lstEncPregunta
	DESCRIPCION:
		Clase que define los metodos y propiedades de la página docEncPregunta

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        09/07/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion

#region

using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReAl.Opinion.Dal.Entidades;
using ReAl.Opinion.Dal.Modelo;
using ReAl.Opinion.Web.App_Class;
using Microsoft.Ajax.Utilities;


#endregion

namespace ReAl.Opinion.Web.ENC
{
    public partial class docEncPreguntaModificar : Page
    {
		#region Parametros de llegada
        public int localid
        {
            get
            {
                if (Request["id"] == null)
                {
                    return 0;
                }
                return int.Parse(Request["id"].ToString());
            }
        }
        #endregion

		protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //Titulo de Pagina
                this.Title = cParametrosWeb.strNombrePagina;

                //Otorgamos valores a los Labels
                //Colocamos los mensajes de Error
                
                if (localid > 0)
                {
                    llenarControles();
                }	
            }
            
        }



		protected void llenarControles()
        {
			try
			{
				//Funcion que inicializa los controles
				rnEncPregunta rnModificar = new rnEncPregunta();
				entEncPregunta objPag = rnModificar.ObtenerObjeto(localid);

				if(objPag != null)
				{
                    //Asignamos los valores
                    txtPregunta.Text = objPag.pregunta;
                    txtInstruccion.Text = objPag.instruccion;
				}
			}
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }
        }

        protected void limpiarControles()
        {
            //Funcion que inicializa los controles
            txtPregunta.Text = "";
            txtInstruccion.Text = "";
        }

        protected void btnAtras_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/ENC/lstEncPregunta.aspx");
        }
        
        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            limpiarControles();
        }
        
		protected void btnRegistrar_Click(object sender, EventArgs e)
        {
			Boolean bProcede = false;
            
			try
			{
				Page.Validate("cabecera");
                if (!Page.IsValid) return;
			
				
				if (localid > 0)
				{
                    rnEncPregunta rnModificar = new rnEncPregunta();
				    entEncPregunta objPag = rnModificar.ObtenerObjeto(localid);
                    cSessionHandler miSesion = new cSessionHandler();

                    //Apropiamos los valores para MODIFICAR
                    objPag.pregunta = Server.HtmlDecode(txtPregunta.Text).Replace("\n", "").Replace("\t", "").Replace("<div>", "").Replace("</div>", "").Replace("span style=\"color:", "font color=\"").Replace("</span>", "</font>").Replace(";", "").Replace("<p>","").Replace("</p>","<br />");
                    objPag.instruccion = Server.HtmlDecode(txtInstruccion.Text).Replace("\n", "").Replace("\t", "").Replace("<div>", "").Replace("</div>", "").Replace("span style=\"color:", "font color=\"").Replace("</span>", "</font>").Replace(";", "").Replace("<p>", "").Replace("</p>", "<br />");

                    objPag.usumod = miSesion.appUsuario.login;
                    rnModificar.Update(objPag);
                }				
	            
				limpiarControles();
				bProcede = true;
			}
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this,exp);
            }
			if (bProcede)
                Response.Redirect("~/ENC/lstEncPregunta.aspx");
        }
    }
}
