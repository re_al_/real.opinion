﻿#region 
/***********************************************************************************************************
	NOMBRE:       lstEncPregunta
	DESCRIPCION:
		Clase que define los metodos y propiedades de la página lstEncPregunta

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        09/07/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion

#region

using System;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReAl.Opinion.Dal.Entidades;
using ReAl.Opinion.Dal.Modelo;
using ReAl.Opinion.Web.App_Class;

#endregion

namespace ReAl.Opinion.Web.ENC
{
    public partial class lstEncRespuesta : Page
    {
        
         #region Parametros de llegada

        public lstEncRespuesta()
        {
            dsReporte = null;
        }

        public DataSet dsReporte { set; get; }

        #endregion


		protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Validamos el salto directo
                cSessionHandler miSesion = new cSessionHandler();
                if (miSesion.arrMenu != null)
                    if (!miSesion.arrMenu.Contains(Request.AppRelativeCurrentExecutionFilePath))
                        Server.Transfer("~/Template/Dashboard.aspx");


                //Titulo de Pagina
                this.Title = cParametrosWeb.strNombrePagina;

                if (!Page.IsPostBack)
                    CargarDdlPregunta();

                String strNombreVista = entEncRespuesta.strNombreTabla;

                ArrayList arrNomParam = new ArrayList();
                arrNomParam.Add(entEncRespuesta.Fields.id_pregunta.ToString());
                ArrayList arrParam = new ArrayList();
                arrParam.Add(ddlid_pregunta.SelectedValue);
                

                ArrayList arrHide = new ArrayList();
                
                cReportBuilder cRpt = new cReportBuilder();
                this.dsReporte = cRpt.obtenerReporteVista(strNombreVista, arrNomParam, arrParam, arrHide);

                cargarListado();
                
            }
            catch (Exception exp)
            {

                ((Main)this.Master).mostrarPopUp(this, exp);
            }            
        }

        private void CargarDdlPregunta()
        {
            try
            {
                //Funcion que inicializa los controles
                rnVista rn = new rnVista();
                cSessionHandler miSession = new cSessionHandler();
                DataTable dt = rn.ObtenerDatos("vw_enc_pregunta_dropdownlist");
                DataView dv = dt.DefaultView;
                dv.RowFilter = "id_proyecto = " + miSession.appRestriccion.id_proyecto;

                ddlid_pregunta.DataValueField = entEncPregunta.Fields.id_pregunta.ToString();
                ddlid_pregunta.DataTextField = entEncPregunta.Fields.pregunta.ToString();
                ddlid_pregunta.DataSource = dv.ToTable();
                ddlid_pregunta.DataBind();
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp);
            }
        }

        private void cargarListado()
        {
			try
			{
                DataView dv = this.dsReporte.Tables[1].DefaultView;
			    dv.Sort = "codigo";

                DataTable listTable = dv.ToTable();
                dtgListado.DataSource = listTable;
				dtgListado.DataBind();

			    if (listTable.Rows.Count > 0)
			    {
                    dtgListado.HeaderRow.TableSection = TableRowSection.TableHeader;

                    for (int i = 0; i < dtgListado.Columns.Count; i++)
                        dtgListado.HeaderRow.Cells[i].Attributes.Add("data-sortable", "true");
                }                
			}
			catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp);
            }
        }
        
        protected void dtgListado_RowDataBound(object sender, GridViewRowEventArgs e)
        {			    
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                
            }
        }

		protected void dtgListado_RowCommand(object sender, GridViewCommandEventArgs e)
		{
		    String strRedireccion = "";
            try
            {
                if (e.CommandName.Equals("detalles"))
                {
                    string strId = e.CommandArgument.ToString();

                    //Filtramos el Dataset
                    DataView dv = this.dsReporte.Tables[0].DefaultView;
                    dv.RowFilter = "id = " + strId;

                    DataTable detailTable = dv.ToTable();


                    dtgDetalles.DataSource = detailTable;
                    dtgDetalles.DataBind();
                    dtgDetalles.HeaderRow.TableSection = TableRowSection.TableHeader;

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#currentdetail').appendTo('body').modal('show');", true);
                }
                else if (e.CommandName.Equals("modificar"))
                {
                    string strId = e.CommandArgument.ToString();
                    rnEncRespuesta rnModificar = new rnEncRespuesta();
                    entEncRespuesta objPag = rnModificar.ObtenerObjeto(int.Parse(strId));
                    strRedireccion = "~/ENC/docEncRespuesta.aspx?id=" + objPag.id_respuesta;
                }
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }

            if (!strRedireccion.IsNullOrEmpty())
                Response.Redirect(strRedireccion);
        }

		protected void ImbEliminar_Click(object sender, EventArgs e)
        {
            try
            {                
                int index = ((GridViewRow)(((ImageButton)sender).Parent.Parent)).RowIndex;
                //Obtenemos las Claves para cada registro

                //Eliminamos el objeto
                rnEncPregunta rnModificar = new rnEncPregunta();
                entEncPregunta objPag = new entEncPregunta();                
                
				
                rnModificar.Delete(objPag);

                //Actualizamos el Grid
                cargarListado();
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }
        }
        
        protected void btnAtras_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Template/Dashboard.aspx");
        }

        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/ENC/docEncRespuesta.aspx");
        }
    }
}
