#region 
/***********************************************************************************************************
	NOMBRE:       lstEncPregunta
	DESCRIPCION:
		Clase que define los metodos y propiedades de la página docEncPregunta

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        09/07/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion

#region

using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;


using ReAl.Opinion.Dal.Entidades;
using ReAl.Opinion.Dal.Modelo;
using ReAl.Opinion.Web.App_Class;
using Microsoft.Ajax.Utilities;


#endregion

namespace ReAl.Opinion.Web.ENC
{
    public partial class docEncSeccion : Page
    {
		#region Parametros de llegada
        public int localid
        {
            get
            {
                if (Request["id"] == null)
                {
                    return 0;
                }
                return int.Parse(Request["id"].ToString());
            }
        }
        #endregion

		protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //Titulo de Pagina
                this.Title = cParametrosWeb.strNombrePagina;

                //Otorgamos valores a los Labels
                CargarDdlNivel();
                //Colocamos los mensajes de Error
                
                if (localid > 0)
                {
                    llenarControles();
                }	
            }
            
        }
        
        private void CargarDdlNivel()
        {
            try
            {
                //Funcion que inicializa los controles
                DataTable dt = new DataTable();
                dt.Columns.Add(new DataColumn("id_nivel", typeof(Int16)));
                dt.Columns.Add(new DataColumn("nivel", typeof(String)));

                DataRow dr1 = dt.NewRow();
                dr1["id_nivel"] = 1;
                dr1["nivel"] = "Nivel 1";
                dt.Rows.Add(dr1);
                DataRow dr2 = dt.NewRow();
                dr2["id_nivel"] = 2;
                dr2["nivel"] = "Nivel 2";
                dt.Rows.Add(dr2);

                ddlid_nivel.DataValueField = "id_nivel";
                ddlid_nivel.DataTextField = "nivel";
                ddlid_nivel.DataSource = dt;
                ddlid_nivel.DataBind();
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }
        }

		protected void llenarControles()
        {
			try
			{
				//Funcion que inicializa los controles
				rnEncSeccion rnModificar = new rnEncSeccion();
                entEncSeccion objPag = rnModificar.ObtenerObjeto(localid);

				if(objPag != null)
				{
                    //Asignamos los valores
				    ddlid_nivel.SelectedValue = objPag.id_nivel.ToString();
                    txtcodigo.Text = objPag.codigo;
                    txtseccion.Text = objPag.seccion;
                }
			}
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }
        }

        protected void limpiarControles()
        {
            //Funcion que inicializa los controles
            
        }

        

        protected void btnAtras_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/ENC/lstEncSeccion.aspx");
        }
        
        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            limpiarControles();
        }
        
		protected void btnRegistrar_Click(object sender, EventArgs e)
        {
            Boolean bProcede = false;

            try
            {
                Page.Validate("cabecera");
                if (!Page.IsValid) return;

                rnEncSeccion rnModificar = new rnEncSeccion();
                entEncSeccion objPag = new entEncSeccion();
                cSessionHandler miSesion = new cSessionHandler();

                if (localid == 0)
                {
                    //Apropiamos los valores para INSERTAR
                    objPag = new entEncSeccion();


                    objPag.id_nivel = rnModificar.GetColumnType(ddlid_nivel.SelectedValue, entEncSeccion.Fields.id_nivel);
                    objPag.codigo = txtcodigo.Text;
                    objPag.seccion = txtseccion.Text;
                    objPag.id_proyecto = miSesion.appRestriccion.id_proyecto;
                    objPag.apiestado = "ELABORADO";
                    objPag.usucre = miSesion.appUsuario.login;

                    rnModificar.Insert(objPag);


                }
                else
                {
                    //Apropiamos los valores para MODIFICAR
                    objPag = rnModificar.ObtenerObjeto(localid);
                    objPag.id_nivel = rnModificar.GetColumnType(ddlid_nivel.SelectedValue, entEncSeccion.Fields.id_nivel);
                    objPag.codigo = txtcodigo.Text;
                    objPag.seccion = txtseccion.Text;
                    objPag.apiestado = "ELABORADO";
                    objPag.usumod = miSesion.appUsuario.login;

                    rnModificar.Update(objPag);
                }

                limpiarControles();
                bProcede = true;
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp);
            }
            if (bProcede)
                Response.Redirect("~/ENC/lstEncSeccion.aspx");
        }

        protected void btnRegistrarNuevo_Click(object sender, EventArgs e)
        {
                      
        }

        protected void ddlid_tipo_pregunta_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }        
    }
}
