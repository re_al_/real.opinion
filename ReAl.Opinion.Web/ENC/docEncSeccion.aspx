<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="docEncSeccion.aspx.cs" Inherits="ReAl.Opinion.Web.ENC.docEncSeccion" Title="docEncSeccion" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %> 

<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="server">
    <link href="<%= ResolveClientUrl("~/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css") %>" rel="stylesheet">
    <link href="<%= ResolveClientUrl("~/bower_components/datatables-responsive/css/dataTables.responsive.css") %>" rel="stylesheet">
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentSection" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Registro de Secciones</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>            
            <div class="row">
                <div class="col-lg-12">
                    
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Registro de Secciones
                        </div>
                        <div class="panel-body">
							<div>
								<asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="cabecera" 
                                    DisplayMode="BulletList" ShowMessageBox="True" 
									class="alert alert-danger" CssClass="alert alert-danger"
							        HeaderText="Debe considerar las siguientes observaciones antes de continuar" />                                
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <!-- Botones de Accion -->
                                        <asp:LinkButton ID="btnAtras" runat="server" Text="<i class='fa fa-arrow-left'></i>" ToolTip="Atras"
                                            CssClass="btn btn-primary btn-circle" class="btn btn-default btn-circle" OnClick="btnAtras_Click"/>
                                    
                                        <asp:LinkButton ID="btnGuardar" runat="server" Text="<i class='fa fa-save'></i>" ToolTip="Guardar registro"
                                            CssClass="btn btn-primary btn-circle" class="btn btn-default btn-circle" OnClick="btnRegistrar_Click"/>
                                    </div>

                                    <div class="form-group">
										<label id="lblid_nivel" runat="server">Nivel:</label>
                                        <asp:DropDownList ID="ddlid_nivel" runat="server" class="form-control" CssClass="form-control"></asp:DropDownList>
									</div>
                                    
                                    <div class="form-group">
										<label id="lblcodigo" runat="server">C&oacute;digo Secci&oacute;n:</label>
										<asp:TextBox id="txtcodigo" Class="form-control" CssClass="form-control" runat="server"></asp:TextBox>
									</div>
                                    
                                    <div class="form-group">
										<label id="lblseccion" runat="server">Secci&oacute;n:</label>
										<asp:TextBox id="txtseccion" Class="form-control" CssClass="form-control" runat="server"></asp:TextBox>
									</div>
                                    
                                    <div class="form-group">
                                        <label id="lblmostrar_ventana" runat="server">Secci&oacute;n abierta:</label>
                                        <div class="checkbox checkbox-slider--b">
	                                        <label>
	                                            <input ID="chkabierta" runat="server" type="checkbox"/><span>Se puede acceder a la Seccion en cualquier momento</span>
	                                        </label>
                                        </div>
                                    </div>
                                </div>
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>
			<asp:HiddenField ID="hdnIdDatos" runat="server" />
        </ContentTemplate>	        
    </asp:UpdatePanel>
    
    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="server">
    <script src="<%= ResolveClientUrl("~/bower_components/datatables/media/js/jquery.dataTables.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js") %>"></script>
	
	<script>
	    var confirmarRegistro = function () {
	        BootstrapDialog.confirm({
	            title: 'Confirmar acci&oacute;n',
	            message: '¿Est&aacute; seguro que desea guardar la informaci&oacute;n?',
	            type: BootstrapDialog.TYPE_PRIMARY, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
	            draggable: true, // <-- Default value is false
	            btnCancelLabel: 'No estoy seguro', // <-- Default value is 'Cancel',
	            btnOKLabel: 'De acuerdo', // <-- Default value is 'OK',
	            btnOKClass: 'btn-success', // <-- If you didn't specify it, dialog type will be used,
	            callback: function (result) {
	                // result will be true if button was click, while it will be false if users close the dialog directly.
	                if (result) {
	                    <%=Page.ClientScript.GetPostBackEventReference(btnGuardar, string.Empty)%>;
	                } else {
	                    return false;
	                }
	            }
	        });

	    };        
	</script> 
</asp:Content>

