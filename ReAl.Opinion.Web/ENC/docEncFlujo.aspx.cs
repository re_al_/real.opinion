﻿#region 
/***********************************************************************************************************
	NOMBRE:       lstEncPregunta
	DESCRIPCION:
		Clase que define los metodos y propiedades de la página docEncPregunta

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        09/07/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion

#region

using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;


using ReAl.Opinion.Dal.Entidades;
using ReAl.Opinion.Dal.Modelo;
using ReAl.Opinion.Web.App_Class;

#endregion

namespace ReAl.Opinion.Web.ENC
{
    public partial class docEncFlujo : Page
    {
		#region Parametros de llegada
        public int localid
        {
            get
            {
                if (Request["id"] == null)
                {
                    return 0;
                }
                return int.Parse(Request["id"].ToString());
            }
        }
        #endregion

		protected void Page_Load(object sender, EventArgs e)
        {
            

            if (!Page.IsPostBack)
            {                

                //Titulo de Pagina
                this.Title = cParametrosWeb.strNombrePagina;

                //Otorgamos valores a los Labels
                CargarDdlOrigenDestino();

				//Colocamos los mensajes de Error

				if (localid > 0)
                {
                    llenarControles();
                }	
            }
            else
            {
                if (dtgList.HeaderRow != null)
                    dtgList.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            cargarListado();

        }

        private void cargarListado()
        {
            try
            {
                cSessionHandler miSesion = new cSessionHandler();
                rnVista rn = new rnVista();

                ArrayList arrNomParam = new ArrayList();
                arrNomParam.Add("pid_proyecto");
                ArrayList arrParam = new ArrayList();
                arrParam.Add(miSesion.appRestriccion.id_proyecto);

                DataTable dtTemp = rn.ObtenerDatosProcAlm("fn_reporte_resumen_boleta", arrNomParam, arrParam);

                //Ejecutamos el query
                DataTable dtReporte = rn.CargarDataTableDesdeQuery(dtTemp.Rows[0][0].ToString());

                dtgList.DataSource = dtReporte;
                dtgList.DataBind();
                if (dtReporte.Rows.Count > 0)
                {
                    dtgList.HeaderRow.TableSection = TableRowSection.TableHeader;
                    dtgList.Attributes.Add("data-page-size", "10");

                    for (int i = 0; i < dtgList.Columns.Count; i++)
                    {
                        dtgList.HeaderRow.Cells[i].Attributes.Add("data-sortable", "true");
                    }
                }
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }
        }

        private void CargarDdlOrigenDestino()
        {
            try
            {
                //Funcion que inicializa los controles
                rnVista rn = new rnVista();
                cSessionHandler miSession = new cSessionHandler();
                DataTable dt = rn.ObtenerDatos("vw_enc_pregunta_dropdownlist");
                DataView dv = dt.DefaultView;
                dv.RowFilter = "id_proyecto = " + miSession.appRestriccion.id_proyecto;

                ddlorigen.DataValueField = entEncPregunta.Fields.id_pregunta.ToString();
                ddlorigen.DataTextField = entEncPregunta.Fields.pregunta.ToString();
                ddlorigen.DataSource = dv.ToTable();
                ddlorigen.DataBind();

                DataTable dtDestino = dv.ToTable();
                DataRow dr1 = dtDestino.NewRow();
                dr1["id_pregunta"] = -1;
                dr1["pregunta"] = " --FIN DE SECCION-- ";
                dtDestino.Rows.Add(dr1);
                DataRow dr2 = dtDestino.NewRow();
                dr2["id_pregunta"] = -2;
                dr2["pregunta"] = " --FIN DE BOLETA-- ";
                dtDestino.Rows.Add(dr2);

                ddldestino.DataValueField = entEncPregunta.Fields.id_pregunta.ToString();
                ddldestino.DataTextField = entEncPregunta.Fields.pregunta.ToString();
                ddldestino.DataSource = dtDestino;
                ddldestino.DataBind();
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp);
            }
        }

        protected void llenarControles()
        {
			try
			{
				//Funcion que inicializa los controles
				rnEncPregunta rnModificar = new rnEncPregunta();
				entEncPregunta objPag = rnModificar.ObtenerObjeto(localid);

				if(objPag != null)
				{
					//Asignamos los valores
				}
			}
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }
        }

        protected void limpiarControles()
        {
            //Funcion que inicializa los controles
            txtregla.Text = "";
            
        }

        protected void btnAtras_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/ENC/lstEncFlujo.aspx");
        }
        
        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            limpiarControles();
        }
        
		protected void btnRegistrar_Click(object sender, EventArgs e)
        {
			Boolean bProcede = false;
            
			try
			{
				Page.Validate("cabecera");
                if (!Page.IsValid) return;

                rnEncFlujo rnModificar = new rnEncFlujo();
                entEncFlujo objPag = new entEncFlujo();
                cSessionHandler miSesion = new cSessionHandler();

                if (localid == 0)
				{
					//Apropiamos los valores para INSERTAR
                    objPag = new entEncFlujo();

                    objPag.id_pregunta = rnModificar.GetColumnType(ddlorigen.SelectedValue, entEncFlujo.Fields.id_pregunta); ;
                    objPag.id_pregunta_destino = rnModificar.GetColumnType(ddldestino.SelectedValue, entEncFlujo.Fields.id_pregunta_destino); ;
				    objPag.regla = txtregla.Text.Trim();
				    objPag.id_proyecto = miSesion.appRestriccion.id_proyecto;
                    objPag.apiestado = "ELABORADO";
				
					//objPag.usucre = cParametrosWeb._miUsuario.loginsus;
					rnModificar.Insert(objPag);
                    
				}
				else
				{
					//Apropiamos los valores para MODIFICAR
					objPag = rnModificar.ObtenerObjeto(localid);
                    objPag.id_pregunta = rnModificar.GetColumnType(ddlorigen.SelectedValue, entEncFlujo.Fields.id_pregunta); ;
                    objPag.id_pregunta_destino = rnModificar.GetColumnType(ddldestino.SelectedValue, entEncFlujo.Fields.id_pregunta_destino); ;
                    objPag.regla = txtregla.Text.Trim();
				
					//objPag.usumod = cParametrosWeb._miUsuario.loginsus;
					//rnModificar.Update(objPag);
				}				
	            
				limpiarControles();
				bProcede = true;
			}
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this,exp);
            }
			//if (bProcede)
                //Response.Redirect("~/ENC/lstEncPregunta.aspx");
        }

        protected void btnRegistrarNuevo_Click(object sender, EventArgs e)
        {
            Boolean bProcede = false;
            try
            {
                Page.Validate("cabecera");
                if (!Page.IsValid) return;

                rnEncFlujo rnModificar = new rnEncFlujo();
                entEncFlujo objPag = new entEncFlujo();
                cSessionHandler miSesion = new cSessionHandler();

                if (localid == 0)
                {
                    //Apropiamos los valores para INSERTAR
                    objPag = new entEncFlujo();

                    objPag.id_pregunta = rnModificar.GetColumnType(ddlorigen.SelectedValue, entEncFlujo.Fields.id_pregunta); ;
                    objPag.id_pregunta_destino = rnModificar.GetColumnType(ddldestino.SelectedValue, entEncFlujo.Fields.id_pregunta_destino); ;
                    objPag.regla = txtregla.Text.Trim();
                    objPag.id_proyecto = miSesion.appRestriccion.id_proyecto;
                    objPag.apiestado = "ELABORADO";

                    //objPag.usucre = cParametrosWeb._miUsuario.loginsus;
                    rnModificar.Insert(objPag);

                }
                else
                {
                    //Apropiamos los valores para MODIFICAR
                    objPag = rnModificar.ObtenerObjeto(localid);
                    objPag.id_pregunta = rnModificar.GetColumnType(ddlorigen.SelectedValue, entEncFlujo.Fields.id_pregunta); ;
                    objPag.id_pregunta_destino = rnModificar.GetColumnType(ddldestino.SelectedValue, entEncFlujo.Fields.id_pregunta_destino); ;
                    objPag.regla = txtregla.Text.Trim();

                    //objPag.usumod = cParametrosWeb._miUsuario.loginsus;
                    //rnModificar.Update(objPag);
                }

                limpiarControles();
                bProcede = true;
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp);
            }            
        }

        
    }
}
