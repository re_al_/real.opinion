#region 
/***********************************************************************************************************
	NOMBRE:       lstEncPregunta
	DESCRIPCION:
		Clase que define los metodos y propiedades de la página lstEncPregunta

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        09/07/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion

#region

using System;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReAl.Opinion.Dal;
using ReAl.Opinion.Dal.Modelo;
using ReAl.Opinion.Dal.Entidades;
using ReAl.Opinion.Web.App_Class;

#endregion

namespace ReAl.Opinion.Web.ENC
{
    public partial class lstEncFlujo : Page
    {
        
         #region Parametros de llegada

        public lstEncFlujo()
        {
            dsReporte = null;
        }

        public DataSet dsReporte { set; get; }

        #endregion


		protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Validamos el salto directo
                cSessionHandler miSesion = new cSessionHandler();
                if (miSesion.arrMenu != null)
                    if (!miSesion.arrMenu.Contains(Request.AppRelativeCurrentExecutionFilePath))
                        Server.Transfer("~/Template/Dashboard.aspx");


                //Titulo de Pagina
                this.Title = cParametrosWeb.strNombrePagina;

                String strNombreVista = "vw_enc_flujo_lis";

                ArrayList arrNomParam = new ArrayList();
                arrNomParam.Add(entSegProyecto.Fields.id_proyecto.ToString());
                ArrayList arrParam = new ArrayList();
                arrParam.Add(miSesion.appRestriccion.id_proyecto);

                ArrayList arrHide = new ArrayList();
                arrHide.Add("pregunta_origen");
                arrHide.Add("pregunta_destino");

                /*
                arrHide.Add(entEncPregunta.Fields.apiestado.ToString());
                arrHide.Add(entEncPregunta.Fields.usucre.ToString());
                arrHide.Add(entEncPregunta.Fields.feccre.ToString());
                arrHide.Add(entEncPregunta.Fields.usumod.ToString());
                arrHide.Add(entEncPregunta.Fields.fecmod.ToString());
                 * */


                cReportBuilder cRpt = new cReportBuilder();
                this.dsReporte = cRpt.obtenerReporteVista(strNombreVista, arrNomParam, arrParam, arrHide);

                if (!Page.IsPostBack)
                {
                    cargarListado();

                }
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp);
            }
            
        }
       
        private void cargarListado()
        {
			try
			{
                DataView dv = this.dsReporte.Tables[1].DefaultView;
                dv.Sort = "origen";

                DataTable listTable = dv.ToTable();
                dtgListado.DataSource = listTable;
				dtgListado.DataBind();

			    if (dtgListado.Rows.Count > 0)
			    {
			        dtgListado.HeaderRow.TableSection = TableRowSection.TableHeader;

			        for (int i = 0; i < dtgListado.Columns.Count; i++)
			            dtgListado.HeaderRow.Cells[i].Attributes.Add("data-sortable", "true");
			    }
			}
			catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp);
            }
        }
        
        protected void dtgListado_RowDataBound(object sender, GridViewRowEventArgs e)
        {			    
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                
            }
        }

		protected void dtgListado_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("detalles"))
                {
                    string strId = e.CommandArgument.ToString();

                    //Filtramos el Dataset
                    DataView dv = this.dsReporte.Tables[0].DefaultView;
                    dv.RowFilter = "id = " + strId;

                    DataTable detailTable = dv.ToTable();


                    dtgDetalles.DataSource = detailTable;
                    dtgDetalles.DataBind();
                    dtgDetalles.HeaderRow.TableSection = TableRowSection.TableHeader;

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#currentdetail').appendTo('body').modal('show');", true);
                }
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }
        }

		protected void ImbEliminar_Click(object sender, EventArgs e)
        {
            try
            {                
                int index = ((GridViewRow)(((ImageButton)sender).Parent.Parent)).RowIndex;
                //Obtenemos las Claves para cada registro

                //Eliminamos el objeto
                rnEncPregunta rnModificar = new rnEncPregunta();
                entEncPregunta objPag = new entEncPregunta();                
                
				
                rnModificar.Delete(objPag);

                //Actualizamos el Grid
                cargarListado();
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }
        }
        
        protected void ImbModificar_Click(object sender, EventArgs e)
        {
			entEncPregunta objPag = new entEncPregunta();
			Boolean bProcede = false;
            try
            {
                int index = ((GridViewRow)(((ImageButton)sender).Parent.Parent)).RowIndex;				
                //Obtenemos las Claves para cada registro

                //Obtenemos el Objeto
				rnEncPregunta rnModificar = new rnEncPregunta();                
				

				bProcede = true;
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }
			//if (bProcede)
            //    Response.Redirect("~...?id=" + objPag.);
        }

        protected void btnAtras_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Template/Dashboard.aspx");
        }
        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/ENC/docEncFlujo.aspx");
        }  
    }
}
