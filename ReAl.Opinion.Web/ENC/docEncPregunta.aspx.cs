﻿#region 
/***********************************************************************************************************
	NOMBRE:       lstEncPregunta
	DESCRIPCION:
		Clase que define los metodos y propiedades de la página docEncPregunta

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        09/07/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion

#region

using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;


using ReAl.Opinion.Dal.Entidades;
using ReAl.Opinion.Dal.Modelo;
using ReAl.Opinion.Web.App_Class;
using Microsoft.Ajax.Utilities;


#endregion

namespace ReAl.Opinion.Web.ENC
{
    public partial class docEncPregunta : Page
    {
		#region Parametros de llegada
        public int localid
        {
            get
            {
                if (Request["id"] == null)
                {
                    return 0;
                }
                return int.Parse(Request["id"].ToString());
            }
        }
        #endregion

		protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //Titulo de Pagina
                this.Title = cParametrosWeb.strNombrePagina;

                //Otorgamos valores a los Labels
                CargarDdlSeccion();
                CargarDdlTipoPregunta();
                CargarDdlVariable();
                HabilitarOpcionesPregunta(ddlid_tipo_pregunta.SelectedValue);
                //Colocamos los mensajes de Error
                
                if (localid > 0)
                {
                    llenarControles();
                }	
            }
            
        }

        private void CargarDdlVariable()
        {
            try
            {
                //Funcion que inicializa los controles
                rnVista rn = new rnVista();
                cSessionHandler miSession = new cSessionHandler();
                DataTable dt = rn.ObtenerDatos("vw_enc_pregunta_dropdownlist");
                DataView dv = dt.DefaultView;
                dv.RowFilter = "id_proyecto = " + miSession.appRestriccion.id_proyecto;

                DataTable dtDestino = dv.ToTable();
                DataRow dr1 = dtDestino.NewRow();
                dr1["codigo_pregunta"] = "-1";
                dr1["pregunta"] = " --SIN VARIABLE-- ";
                dtDestino.Rows.Add(dr1);

                ddlvariable.DataValueField = entEncPregunta.Fields.codigo_pregunta.ToString();
                ddlvariable.DataTextField = entEncPregunta.Fields.pregunta.ToString();
                ddlvariable.DataSource = dtDestino;
                ddlvariable.DataBind();
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp);
            }
        }

        private void CargarDdlSeccion()
        {
            try
            {
                //Funcion que inicializa los controles
                rnEncSeccion rn = new rnEncSeccion();
                cSessionHandler miSesion = new cSessionHandler();
                DataTable dt = rn.ObtenerDataTable(entEncSeccion.Fields.id_proyecto,miSesion.appRestriccion.id_proyecto);
                DataView dv = dt.DefaultView;
                dv.Sort = entEncSeccion.Fields.codigo.ToString();
                
                ddlid_seccion.DataValueField = entEncSeccion.Fields.id_seccion.ToString();
                ddlid_seccion.DataTextField = entEncSeccion.Fields.codigo.ToString();
                ddlid_seccion.DataSource = dv.ToTable();
                ddlid_seccion.DataBind();
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }
        }

        private void CargarDdlTipoPregunta()
        {
            try
            {
                //Funcion que inicializa los controles
                rnCatTipopregunta rn = new rnCatTipopregunta();
                DataTable dt = rn.ObtenerDataTable();
                DataView dv = dt.DefaultView;
                dv.Sort = entCatTipopregunta.Fields.tipo_pregunta.ToString();

                ddlid_tipo_pregunta.DataValueField = entCatTipopregunta.Fields.id_tipo_pregunta.ToString();
                ddlid_tipo_pregunta.DataTextField = entCatTipopregunta.Fields.tipo_pregunta.ToString();
                ddlid_tipo_pregunta.DataSource = dv.ToTable();
                ddlid_tipo_pregunta.DataBind();
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }
        }

		protected void llenarControles()
        {
			try
			{
				//Funcion que inicializa los controles
				rnEncPregunta rnModificar = new rnEncPregunta();
				entEncPregunta objPag = rnModificar.ObtenerObjeto(localid);

				if(objPag != null)
				{
					//Asignamos los valores
				}
			}
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }
        }

        protected void limpiarControles()
        {
            //Funcion que inicializa los controles
            txtcodigo_pregunta.Text = "";
            txtPregunta.Text = "";
            txtminimo.Text = "0";
            txtmaximo.Text = "0";
            txtlongitud.Text = "0";
            txtrespuestas.Text = "";
            chkMostrarVentana.Checked = false;
        }

        public  void HabilitarOpcionesPregunta(string strTipoPregunta)
        {
            try
            {

                switch (strTipoPregunta)
                {
                    case "0":
                        txtminimo.Text = "0";
                        txtminimo.Enabled = false;
                        txtmaximo.Text = "0";
                        txtmaximo.Enabled = false;
                        txtlongitud.Text = "512";
                        txtlongitud.Enabled = true;
                        txtrespuestas.Enabled = false;
                        txtrespuestas.Text = "";
                        break;
                    case "5":
                    case "6":
                    case "8":
                    case "9":
                        txtminimo.Text = "0";
                        txtminimo.Enabled = false;
                        txtmaximo.Text = "0";
                        txtmaximo.Enabled = false;
                        txtlongitud.Text = "0";
                        txtlongitud.Enabled = false;
                        txtrespuestas.Enabled = false;
                        txtrespuestas.Text = "";
                        break;
                    case "1":
                        txtminimo.Text = "0";
                        txtminimo.Enabled = false;
                        txtmaximo.Text = "0";
                        txtmaximo.Enabled = false;
                        txtlongitud.Text = "0";
                        txtlongitud.Enabled = false;
                        txtrespuestas.Enabled = true;
                        break;
                    case "2":
                    case "3":
                    case "4":
                        txtminimo.Text = "1";
                        txtminimo.Enabled = true;
                        txtmaximo.Text = "0";
                        txtmaximo.Enabled = true;
                        txtlongitud.Text = "0";
                        txtlongitud.Enabled = false;
                        txtrespuestas.Enabled = true;
                        break;
                    }
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }
        }

        protected void btnAtras_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/ENC/lstEncPregunta.aspx");
        }
        
        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            limpiarControles();
        }
        
		protected void btnRegistrar_Click(object sender, EventArgs e)
        {
			Boolean bProcede = false;
            
			try
			{
				Page.Validate("cabecera");
                if (!Page.IsValid) return;

                if (localid == 0)
				{
                    guardarRegistro();
                }
				
				limpiarControles();
				bProcede = true;
			}
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this,exp);
            }
			if (bProcede)
                Response.Redirect("~/ENC/lstEncPregunta.aspx");
        }

        protected void btnRegistrarNuevo_Click(object sender, EventArgs e)
        {
            Boolean bProcede = false;
            try
            {
                Page.Validate("cabecera");
                if (!Page.IsValid) return;



                if (localid == 0)
                {
                    guardarRegistro();
                }
                
                limpiarControles();
                bProcede = true;
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp);
            }            
        }

        protected void ddlid_tipo_pregunta_SelectedIndexChanged(object sender, EventArgs e)
        {
            HabilitarOpcionesPregunta(ddlid_tipo_pregunta.SelectedValue);
        }

        private void guardarRegistro()
        {
            rnEncPregunta rnModificar = new rnEncPregunta();
            entEncPregunta objPag = new entEncPregunta();
            cSessionHandler miSesion = new cSessionHandler();

            //Apropiamos los valores para INSERTAR
            objPag = new entEncPregunta();

            //Obtenemos el nivel segun la seccion
            rnEncSeccion rnSec = new rnEncSeccion();
            entEncSeccion objSec = rnSec.ObtenerObjeto(rnSec.GetColumnType(ddlid_seccion.SelectedValue, entEncSeccion.Fields.id_seccion));


            objPag.codigo_pregunta = ddlid_seccion.Items[ddlid_seccion.SelectedIndex].Text + txtcodigo_pregunta.Text.Trim();
            objPag.pregunta = Server.HtmlDecode(txtPregunta.Text).Replace("\n", "").Replace("\t", "").Replace("<p>", "").Replace("</p>", "").Replace("<div>", "").Replace("</div>", "").Replace("span style=\"color:", "font color=\"").Replace("</span>", "</font>").Replace(";", "").Replace("<p>", "").Replace("</p>", "<br />");
            objPag.instruccion = Server.HtmlDecode(txtInstruccion.Text).Replace("\n", "").Replace("\t", "").Replace("<p>", "").Replace("</p>", "").Replace("<div>", "").Replace("</div>", "").Replace("span style=\"color:", "font color=\"").Replace("</span>", "</font>").Replace(";", "").Replace("<p>", "").Replace("</p>", "<br />");
            objPag.ayuda = "";
            objPag.catalogo = "--";
            objPag.codigo_especifique = "0";
            objPag.regla = "";
            objPag.rpn = "";
            objPag.mensaje = "";
            objPag.revision = "";
            objPag.variable = int.Parse(ddlvariable.SelectedValue.ToString()) <= 0 ? "" : ddlvariable.SelectedItem.ToString();
            objPag.variable_bucle = "";
            objPag.formula = "";
            objPag.rpn_formula = "";


            objPag.id_proyecto = miSesion.appRestriccion.id_proyecto;
            objPag.id_nivel = objSec.id_nivel;
            objPag.id_seccion = rnModificar.GetColumnType(ddlid_seccion.SelectedValue, entEncPregunta.Fields.id_seccion);
            objPag.id_tipo_pregunta = rnModificar.GetColumnType(ddlid_tipo_pregunta.SelectedValue, entEncPregunta.Fields.id_tipo_pregunta);
            objPag.minimo = int.Parse(txtminimo.Text);
            objPag.maximo = int.Parse(txtmaximo.Text);
            objPag.longitud = int.Parse(txtlongitud.Text);
            objPag.mostrar_ventana = chkMostrarVentana.Checked ? 1 : 0;
            objPag.apiestado = "ELABORADO";

            //objPag.usucre = cParametrosWeb._miUsuario.loginsus;
            rnModificar.Insert(objPag);

            //Verificamos las respuestas
            if (txtrespuestas.Enabled && txtrespuestas.Text.Trim() != "")
            {
                //Obtenemos el idPregunta
                int idPregunta = rnModificar.FuncionesMax(entEncPregunta.Fields.id_pregunta);
                rnEncRespuesta rnResp = new rnEncRespuesta();

                using (StringReader reader = new StringReader(txtrespuestas.Text))
                {
                    int i = 1;
                    string line = "";
                    while ((line = reader.ReadLine()) != null)
                    {
                        // Guardamos las respuestas
                        entEncRespuesta objRes = new entEncRespuesta();
                        objRes.codigo = i < 10 ? "0" + i.ToString() : i.ToString();
                        if (line.Contains("="))
                            objRes.respuesta = line.Substring(0, line.LastIndexOf('=')).Trim();
                        else
                            objRes.respuesta = line.Trim();
                        objRes.id_pregunta = idPregunta;
                        objRes.apiestado = "ELABORADO";
                        objRes.usucre = "rvera";
                        rnResp.Insert(objRes);

                        i++;
                    }
                }
            }
        }  
    }
}
