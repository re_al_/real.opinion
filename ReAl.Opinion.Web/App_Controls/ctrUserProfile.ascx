﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctrUserProfile.ascx.cs" Inherits="ReAl.Opinion.Web.App_Controls.ctrUserProfile" %>


<div class="modal-dialog">
    <asp:UpdatePanel ID="updModale" runat="server">
        <ContentTemplate>
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Perfil de Usuario</h4>
                </div>
                <div class="modal-body">
                   <div class="form-group">
                        <label id="lblPassActual" runat="server">Contraseña actual:</label>
                        <asp:TextBox id="txtPassActual" runat="server"
                            Class="form-control" CssClass="form-control" ></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label id="lblNewPass" runat="server">Contraseña actual:</label>
                        <asp:TextBox id="txtNewPass" runat="server" Text="prueba"
                            Class="form-control" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label id="lblRepeatPass" runat="server">Contraseña actual:</label>
                        <asp:TextBox id="txtRepeatPass" runat="server"
                            Class="form-control" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="alert alert-info alert-dismissable" runat="server" id="divError" Visible="False">
                        <a class="panel-close close" data-dismiss="alert">×</a> 
                        <i class="fa fa-coffee"></i>
                        <asp:Label ID="lblError" runat="server" Text="Label"></asp:Label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <asp:Button ID="btnCambiarPass" runat="server" Text="Cambiar contraseña" 
                        CssClass="btn btn-primary" Class="btn btn-primary" 
                        UseSubmitBehavior="false" OnClick="btnCambiarPass_Click" />
                </div>
            </div>
            <!-- /.modal-content -->
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
<!-- /.modal-dialog -->
