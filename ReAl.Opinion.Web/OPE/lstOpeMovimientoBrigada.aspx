<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="lstOpeMovimientoBrigada.aspx.cs" Inherits="ReAl.Opinion.Web.OPE.lstOpeMovimientoBrigada" Title="lstOpeMovimientoBrigada" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="server">
    <link href="<%= ResolveClientUrl("~/Content/bootstrap-table/bootstrap-table.min.css") %>" rel="stylesheet">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentSection" runat="server">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Carga de trabajo por Brigada</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>            
    <div class="row">
        <div class="col-lg-12">
                    <%--  --%>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Carga de trabajo por Brigada
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <!-- Botones de Accion -->
                                <asp:LinkButton ID="btnAtras" runat="server" Text="<i class='fa fa-arrow-left'></i>" ToolTip="Atras"
                                    CssClass="btn btn-primary btn-circle" class="btn btn-default btn-circle" OnClick="btnAtras_Click"/>
                                
                                <asp:LinkButton ID="btnGuardar" runat="server" Text="<i class='fa fa-save'></i>" ToolTip="Guardar registro"
                                    CssClass="btn btn-primary btn-circle" class="btn btn-default btn-circle" 
                                    OnClick="btnRegistrar_Click" OnClientClick="return confirmarRegistro();"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
								<label id="lblid_departamento" runat="server">Departamento:</label>
                                <asp:DropDownList ID="ddlid_departamento" runat="server" class="form-control" 
                                    CssClass="form-control" AutoPostBack="True"
                                    OnSelectedIndexChanged="ddlid_departamento_SelectedIndexChanged"></asp:DropDownList>
							</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Brigadas
                                </div>
                                <div class="panel-body">
                                    <div class="dataTable_wrapper">
                                        <asp:GridView ID="dtgListadoBrigada" runat="server" AutoGenerateColumns="False"
							                data-toggle="table" data-sortable="true" data-search="true" 
							                DataKeyNames=""
                                            CssClass="table table-striped table-bordered table-hover" >
                                    
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:RadioButton ID="rdbRow" runat="server" GroupName="brigadas" 
                                                            AutoPostBack="True" OnCheckedChanged="rb_CheckedChanged" />
                                                        <asp:HiddenField ID="hddid" runat="server" Value='<%# Eval("id") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                                                
                                                <asp:BoundField ReadOnly="True" DataField="codigo" HeaderText="Brigada" ShowHeader="false" HtmlEncode="False" >
									                <ItemStyle HorizontalAlign="Justify" ></ItemStyle>
									                <HeaderStyle HorizontalAlign="Center" ></HeaderStyle>
								                </asp:BoundField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        
                        <div class="col-lg-8">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    UPMs
                                </div>
                                <div class="panel-body">
                                    <div class="dataTable_wrapper">
                                        <asp:GridView ID="dtgListadoUpm" runat="server" AutoGenerateColumns="False"
							                data-toggle="table" data-sortable="true" data-search="true" 
							                DataKeyNames=""
                                            CssClass="table table-striped table-bordered table-hover" >
                                    
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkRow" runat="server" />
                                                        <asp:HiddenField ID="hddid" runat="server" Value='<%# Eval("id") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                
                                                <asp:BoundField ReadOnly="True" DataField="codigo" HeaderText="UPM" ShowHeader="false" HtmlEncode="False" >
									                <ItemStyle HorizontalAlign="Justify" ></ItemStyle>
									                <HeaderStyle HorizontalAlign="Center" ></HeaderStyle>
								                </asp:BoundField>

                                                <asp:BoundField ReadOnly="True" DataField="nombre" HeaderText="Descripci&oacute;n" ShowHeader="false" HtmlEncode="False" >
									                <ItemStyle HorizontalAlign="Justify" ></ItemStyle>
									                <HeaderStyle HorizontalAlign="Center" ></HeaderStyle>
								                </asp:BoundField> 
                                
                                                <asp:BoundField ReadOnly="True" DataField="fecinicio" HeaderText="Fecha" ShowHeader="false" >
									                <ItemStyle HorizontalAlign="Justify" ></ItemStyle>
									                <HeaderStyle HorizontalAlign="Center" ></HeaderStyle>
								                </asp:BoundField> 
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
					
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Operativo de campo planificado
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="dataTable_wrapper">
                                <asp:GridView ID="dtgListado" runat="server" AutoGenerateColumns="False"
							        data-toggle="table" data-show-columns="true" data-pagination="true" 
                                    data-search="true" data-show-toggle="true" data-sortable="true" 
                                    data-page-size="100" data-pagination-v-align="both"
							        DataKeyNames="" OnRowCommand="dtgListado_RowCommand"
                                    CssClass="table table-striped table-bordered table-hover" >
                                    
                                    <Columns>
                                        <asp:BoundField ReadOnly="True" DataField="id" HeaderText="ID" ShowHeader="false" >
									        <ItemStyle HorizontalAlign="Justify" ></ItemStyle>
									        <HeaderStyle HorizontalAlign="Center" ></HeaderStyle>
								        </asp:BoundField> 
                                
                                        <asp:BoundField ReadOnly="True" DataField="brigada" HeaderText="Brigada" ShowHeader="false" >
									        <ItemStyle HorizontalAlign="Justify" ></ItemStyle>
									        <HeaderStyle HorizontalAlign="Center" ></HeaderStyle>
								        </asp:BoundField> 
													
                                        <asp:BoundField ReadOnly="True" DataField="upm" HeaderText="C&oacute;digo UPM" ShowHeader="false" HtmlEncode="False" >
									        <ItemStyle HorizontalAlign="Justify" ></ItemStyle>
									        <HeaderStyle HorizontalAlign="Center" ></HeaderStyle>
								        </asp:BoundField> 					
                                
                                
                                        <asp:BoundField ReadOnly="True" DataField="nombre" HeaderText="Descripci&oacute;n UPM" ShowHeader="false" HtmlEncode="False" >
									        <ItemStyle HorizontalAlign="Justify" ></ItemStyle>
									        <HeaderStyle HorizontalAlign="Center" ></HeaderStyle>
								        </asp:BoundField> 
                                                                        
                                        <asp:TemplateField HeaderText="Acciones">
									        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
									        <HeaderStyle></HeaderStyle>
									        <ItemTemplate>
										        <asp:LinkButton ID="ImbEliminar" CommandName="eliminar" CommandArgument='<%# Bind("id") %>' runat="server" 
                                                    ToolTip="Modificar el registro"
                                                    CssClass="btn btn-danger btn-circle" Text="<i class='fa fa-eraser'></i>"/>										
									        </ItemTemplate>
								        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

	<asp:HiddenField ID="hdnIdDatos" runat="server" />


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="server">
    <script src="<%= ResolveClientUrl("~/Scripts/bootstrap-table/bootstrap-table.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Scripts/bootstrap-table/locale/bootstrap-table-es-MX.min.js") %>"></script>
    
    <script>
        var confirmarRegistro = function () {
            BootstrapDialog.confirm({
                title: 'Confirmar accion',
                message: '�Est� seguro que desea guardar la informacion?',
                type: BootstrapDialog.TYPE_PRIMARY, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                draggable: true, // <-- Default value is false
                btnCancelLabel: 'No estoy seguro', // <-- Default value is 'Cancel',
                btnOKLabel: 'De acuerdo', // <-- Default value is 'OK',
                btnOKClass: 'btn-success', // <-- If you didn't specify it, dialog type will be used,
                callback: function (result) {
                    // result will be true if button was click, while it will be false if users close the dialog directly.
                    if (result) {
                        <%=Page.ClientScript.GetPostBackEventReference(btnGuardar, string.Empty)%>;
                    } else {
                        return false;
                    }
                }
            });

        };        
    </script>

    <script>
        // tooltip demo
        $('.tooltip-demo').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });

        // popover demo
        $("[data-toggle=popover]")
            .popover();
    </script>
</asp:Content>

