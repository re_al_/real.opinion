﻿#region 
/***********************************************************************************************************
	NOMBRE:       lstEncPregunta
	DESCRIPCION:
		Clase que define los metodos y propiedades de la página docEncPregunta

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        09/07/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion

#region

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClosedXML.Excel;

using ReAl.Opinion.Dal.Entidades;
using ReAl.Opinion.Dal.Modelo;
using ReAl.Opinion.PgConn;
using ReAl.Opinion.Utils;
using ReAl.Opinion.Web.App_Class;
using Microsoft.Ajax.Utilities;
using Npgsql;
using NpgsqlTypes;

#endregion

namespace ReAl.Opinion.Web.OPE
{
    public partial class pagOpeListadoSubir : Page
    {
        #region Parametros de llegada

        public int localid
        {
            get
            {
                if (Request["id"] == null)
                {
                    return 0;
                }
                return int.Parse(Request["id"].ToString());
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            //Validamos el salto directo
            cSessionHandler miSesion = new cSessionHandler();
            if (miSesion.arrMenu != null)
                if (!miSesion.arrMenu.Contains(Request.AppRelativeCurrentExecutionFilePath))
                    Server.Transfer("~/Template/Dashboard.aspx");


            if (!Page.IsPostBack)
            {
                //Titulo de Pagina
                this.Title = cParametrosWeb.strNombrePagina;

            }

        }

        protected void btnAtras_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Template/Dashboard.aspx");
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {

        }

        protected void btnRegistrar_Click(object sender, EventArgs e)
        {
            Boolean bProcede = false;

            try
            {
                Page.Validate("cabecera");
                if (!Page.IsValid) return;
                string strMensaje = "";
                //Subimos el archivo
                if (!FileUpload1.HasFile)
                {
                    // No file uploaded!
                    strMensaje = "Please first select a file to upload...";
                }
                else
                {
                    String strNombreArchivo = "LV-" + DateTime.Now.ToString("yyyy-MM-dd HH-mm") + ".xlsx";
                    cSessionHandler miSesion = new cSessionHandler();                    
                    // Display the uploaded file's details
                    strMensaje = string.Format(
                        @"Uploaded file: {0}<br />
                  File size (in bytes): {1:N0}<br />
                  Content-type: {2}",
                        FileUpload1.FileName,
                        FileUpload1.FileBytes.Length,
                        FileUpload1.PostedFile.ContentType);
                    string filePath = Server.MapPath("~/uploads/" + miSesion.appUsuario.login + "/lvs/" + strNombreArchivo);

                    if(!Directory.Exists(Server.MapPath("~/uploads/" + miSesion.appUsuario.login + "/")))
                        Directory.CreateDirectory(Server.MapPath("~/uploads/" + miSesion.appUsuario.login + "/"));
                    if (!Directory.Exists(Server.MapPath("~/uploads/" + miSesion.appUsuario.login + "/lvs")))
                        Directory.CreateDirectory(Server.MapPath("~/uploads/" + miSesion.appUsuario.login + "/lvs"));

                    FileUpload1.SaveAs(filePath);

                    //Validamos el archivo excel
                    cOpeListadoUpload cLv = new cOpeListadoUpload();
                    bProcede = cLv.validarExcel(filePath);
                }
            }
            catch (Exception exp)
            {
                ((Main) this.Master).mostrarPopUp(this, exp);
            }
            if (bProcede)
                Response.Redirect("~/Template/Dashboard.aspx?msg=Se ha cargado los LVs del archivo");
        }

        protected void btnDescargar_Click(object sender, EventArgs e)
        {
            String strNombreReporte = "PlantillaLVs";
            cSessionHandler miSesion = new cSessionHandler();
            rnVista rn = new rnVista();
            DataTable dtReporte = rn.ObtenerDatos("vw_ope_listado_excel");

            //Para las UPMs
            String strNombreVista = "vw_cat_upm_lis";
            ArrayList arrCols = new ArrayList();
            arrCols.Add(entCatUpm.Fields.codigo.ToString());
            ArrayList arrNomParam = new ArrayList();
            arrNomParam.Add(entCatUpm.Fields.id_proyecto.ToString());
            ArrayList arrParam = new ArrayList();
            arrParam.Add(miSesion.appRestriccion.id_proyecto);
            DataTable dtUpm = rn.ObtenerDatos(strNombreVista, arrCols, arrNomParam, arrParam,
                " AND id_departamento IN (" + miSesion.appRestriccion.departamentos + ")");

            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dtReporte,"LVs");
                wb.Worksheets.Add(dtUpm, "UPMs");


                //Validaciones
                //UPM
                //wb.Worksheets.Worksheet(1).Column(1).SetDataValidation().AllowedValues = XLAllowedValues.TextLength;
                wb.Worksheets.Worksheet(1).Column(1).SetDataValidation().List(wb.Worksheets.Worksheet(2).Range("A2:A20"));

                //Manzanas
                wb.Worksheets.Worksheet(1).Column(2).SetDataValidation().TextLength.Between(1, 40);

                //avenida_calle
                wb.Worksheets.Worksheet(1).Column(3).SetDataValidation().TextLength.Between(1, 50);

                //Nro Predio
                wb.Worksheets.Worksheet(1).Column(4).SetDataValidation().AllowedValues = XLAllowedValues.WholeNumber;
                wb.Worksheets.Worksheet(1).Column(4).SetDataValidation().Decimal.Between(0, 50);
                //Nro Hogares
                wb.Worksheets.Worksheet(1).Column(10).SetDataValidation().AllowedValues = XLAllowedValues.WholeNumber;
                wb.Worksheets.Worksheet(1).Column(10).SetDataValidation().Decimal.Between(0, 10);
                //Nro Hombres
                wb.Worksheets.Worksheet(1).Column(11).SetDataValidation().AllowedValues = XLAllowedValues.WholeNumber;
                wb.Worksheets.Worksheet(1).Column(11).SetDataValidation().Decimal.Between(0, 50);
                //Nro Mujeres
                wb.Worksheets.Worksheet(1).Column(12).SetDataValidation().AllowedValues = XLAllowedValues.WholeNumber;
                wb.Worksheets.Worksheet(1).Column(12).SetDataValidation().Decimal.Between(0, 50);
                //Nro VOE
                wb.Worksheets.Worksheet(1).Column(14).SetDataValidation().AllowedValues = XLAllowedValues.WholeNumber;
                wb.Worksheets.Worksheet(1).Column(14).SetDataValidation().Decimal.Between(0, 100);

                wb.Worksheets.Worksheet(2).Hide();

                wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                wb.Style.Font.Bold = true;

                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;filename= " + strNombreReporte + ".xlsx");

                using (MemoryStream MyMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(MyMemoryStream);
                    MyMemoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
            
        }
    }
}
