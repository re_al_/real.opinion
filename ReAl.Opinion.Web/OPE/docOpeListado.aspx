<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="docOpeListado.aspx.cs" Inherits="ReAl.Opinion.Web.OPE.docOpeListado" Title="docOpeListado" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %> 

<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="server">
    <link href="<%= ResolveClientUrl("~/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css") %>" rel="stylesheet">
    <link href="<%= ResolveClientUrl("~/bower_components/datatables-responsive/css/dataTables.responsive.css") %>" rel="stylesheet">
    <link href="<%= ResolveClientUrl("~/Content/bootstrap-datetimepicker.css") %>" rel="stylesheet">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentSection" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Registro de LVs</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>            
            <div class="row">
                <div class="col-lg-12">
                    
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Registro de LVs
                        </div>
                        <div class="panel-body">
							<div>
								<asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="cabecera" 
                                    DisplayMode="BulletList" ShowMessageBox="True" 
									class="alert alert-danger" CssClass="alert alert-danger"
							        HeaderText="Debe considerar las siguientes observaciones antes de continuar" />                                
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <!-- Botones de Accion -->
                                        <asp:LinkButton ID="btnAtras" runat="server" Text="<i class='fa fa-arrow-left'></i>" ToolTip="Atras"
                                            CssClass="btn btn-primary btn-circle" class="btn btn-default btn-circle" OnClick="btnAtras_Click"/>
                                                                        
                                        <asp:LinkButton ID="btnGuardar" runat="server" Text="<i class='fa fa-save'></i>" ToolTip="Guardar registro"
                                            CssClass="btn btn-primary btn-circle" class="btn btn-default btn-circle" 
                                            OnClick="btnRegistrar_Click" OnClientClick="return confirmarRegistro();" />
                                        
                                        <asp:LinkButton ID="btnGuardarNuevo" runat="server" Text="<i class='fa fa-repeat'></i>" ToolTip="Guardar y Nuevo registro"
                                            CssClass="btn btn-primary btn-circle" class="btn btn-default btn-circle" OnClick="btnRegistrarNuevo_Click"/>
                                    </div>

                                    <div class="form-group">
										<label id="lblid_departamento" runat="server">Upm:</label>
                                        <asp:DropDownList ID="ddlid_upm" runat="server" class="form-control" CssClass="form-control"></asp:DropDownList>
									</div>
                                    
                                    <div class="form-group">
										<label id="lblcodigo" runat="server">C&oacute;digo Manzana / Comunidad:</label>
										<asp:TextBox id="txtcodigo_manzana_comunidad" Class="form-control" CssClass="form-control" runat="server"></asp:TextBox>
									</div>
                                    
                                    <div class="form-group">
										<label id="lblnombre" runat="server">Nombre de Avenida / Calle / Callejon:</label>
                                        <label id="Label1" runat="server">Nombre de la Comunidad o Localidad:</label>
										<asp:TextBox id="txtavenida_calle" Class="form-control" CssClass="form-control" runat="server"></asp:TextBox>
									</div>
                                    
                                    <div class="form-group">
										<label id="lblnro_predio" runat="server">Nro. de Orden de Predio:</label>
										<asp:TextBox id="txtnro_predio" Class="form-control" CssClass="form-control" runat="server"></asp:TextBox>
									</div>
                                    
                                    <div class="form-group">
										<label id="lblnro_puerta" runat="server">Nro. de Puerta Predio (AREA AMANZANADA):</label>
										<asp:TextBox id="txtnro_puerta" Class="form-control" CssClass="form-control" runat="server"></asp:TextBox>
									</div>
                                    
                                    <div class="form-group">
										<label id="lblnro_orden_vivienda" runat="server">Nro. de Orden de Vivienda:</label>
										<asp:TextBox id="txtnro_orden_vivienda" Class="form-control" CssClass="form-control" runat="server"></asp:TextBox>
									</div>
                                    
                                    <div class="form-group">
										<label id="lblnro_piso" runat="server">Nro. de Piso (AREA AMANZANADA):</label>
										<asp:TextBox id="txtnro_piso" Class="form-control" CssClass="form-control" runat="server"></asp:TextBox>
									</div>
                                    
                                    <div class="form-group">
										<label id="lblnro_depto" runat="server">Nro. de Depto. (AREA AMANZANADA):</label>
										<asp:TextBox id="txtnro_depto" Class="form-control" CssClass="form-control" runat="server"></asp:TextBox>
									</div>
                                    
                                    <div class="form-group">
										<label id="lbluso_vivienda" runat="server">Uso de la Vivienda:</label>
										<asp:TextBox id="txtuso_vivienda" Class="form-control" CssClass="form-control" runat="server"></asp:TextBox>
									</div>
                                    
                                    <div class="form-group">
										<label id="lblnro_hogares" runat="server">Nro de Hogares en la Vivienda:</label>
										<asp:TextBox id="txtnro_hogares" Class="form-control" CssClass="form-control" runat="server"></asp:TextBox>
									</div>
                                    
                                    <div class="form-group">
										<label id="lblnro_hombres" runat="server">Nro de Hombres:</label>
										<asp:TextBox id="txtnro_hombres" Class="form-control" CssClass="form-control" runat="server"></asp:TextBox>
									</div>
                                    
                                    <div class="form-group">
										<label id="lblnro_mujeres" runat="server">Nro de Mujeres:</label>
										<asp:TextBox id="txtnro_mujeres" Class="form-control" CssClass="form-control" runat="server"></asp:TextBox>
									</div>
                                    
                                    <div class="form-group">
										<label id="lblnombre_jefe" runat="server">Nombre y apellido del Jefe o Jefa del Hogar:</label>
										<asp:TextBox id="txtnombre_jefe" Class="form-control" CssClass="form-control" runat="server"></asp:TextBox>
									</div>

                                    <div class="form-group">
										<label id="lblnro_voe" runat="server">Nro. de Orden de Vivienda Objeto de Estudio:</label>
										<asp:TextBox id="txtnro_voe" Class="form-control" CssClass="form-control" runat="server"></asp:TextBox>
									</div>
                                </div>
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>
			<asp:HiddenField ID="hdnIdDatos" runat="server" />
        </ContentTemplate>	        
    </asp:UpdatePanel>
    
    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="server">
    <script src="<%= ResolveClientUrl("~/bower_components/datatables/media/js/jquery.dataTables.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js") %>"></script>    
    
    <script type="text/javascript" src="<%= ResolveClientUrl("~/Scripts/moment.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("~/Scripts/bootstrap-datetimepicker.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("~/Scripts/bootstrap-datetimepicker.min.js") %>"></script>

	<script>
	    var confirmarRegistro = function () {
	        BootstrapDialog.confirm({
	            title: 'Confirmar acci&oacute;n',
	            message: '¿Est&aacute; seguro que desea guardar la informaci&oacute;n?',
	            type: BootstrapDialog.TYPE_PRIMARY, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
	            draggable: true, // <-- Default value is false
	            btnCancelLabel: 'No estoy seguro', // <-- Default value is 'Cancel',
	            btnOKLabel: 'De acuerdo', // <-- Default value is 'OK',
	            btnOKClass: 'btn-success', // <-- If you didn't specify it, dialog type will be used,
	            callback: function (result) {
	                // result will be true if button was click, while it will be false if users close the dialog directly.
	                if (result) {
	                    <%=Page.ClientScript.GetPostBackEventReference(btnGuardar, string.Empty)%>;
	                } else {
	                    return false;
	                }
	            }
	        });
	    };
	</script> 
     <script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });
    </script>
</asp:Content>

