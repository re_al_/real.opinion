﻿#region 
/***********************************************************************************************************
	NOMBRE:       lstEncPregunta
	DESCRIPCION:
		Clase que define los metodos y propiedades de la página docEncPregunta

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        09/07/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion

#region

using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReAl.Opinion.Dal.Entidades;
using ReAl.Opinion.Dal.Modelo;
using ReAl.Opinion.Web.App_Class;
using Microsoft.Ajax.Utilities;


#endregion

namespace ReAl.Opinion.Web.OPE
{
    public partial class docOpeBrigada : Page
    {
		#region Parametros de llegada
        public int localid
        {
            get
            {
                if (Request["id"] == null)
                {
                    return 0;
                }
                return int.Parse(Request["id"].ToString());
            }
        }
        #endregion

		protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //Titulo de Pagina
                this.Title = cParametrosWeb.strNombrePagina;

                //Otorgamos valores a los Labels
                CargarDdlDepartamento();
                //Colocamos los mensajes de Error
                
                if (localid > 0)
                {
                    llenarControles();
                }	
            }
            
        }
        
        private void CargarDdlDepartamento()
        {
            try
            {
                //Funcion que inicializa los controles
                cSessionHandler miSesion = new cSessionHandler();
                DataTable dt = cUsuarioRestriccion.ObtenerDataTableDepto(miSesion.appUsuario.login);

                ddlid_departamento.DataValueField = entCatDepartamento.Fields.id_departamento.ToString();
                ddlid_departamento.DataTextField = entCatDepartamento.Fields.nombre.ToString();
                ddlid_departamento.DataSource = dt;
                ddlid_departamento.DataBind();
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }
        }

		protected void llenarControles()
        {
			try
			{
				//Funcion que inicializa los controles
				rnOpeBrigada rnModificar = new rnOpeBrigada();
                entOpeBrigada objPag = rnModificar.ObtenerObjeto(localid);

				if(objPag != null)
				{
					//Asignamos los valores
				    ddlid_departamento.SelectedValue = objPag.id_departamento.ToString();
				    txtcodigo.Text = objPag.codigo_brigada;
				}
			}
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }
        }

        protected void limpiarControles()
        {
            //Funcion que inicializa los controles
            txtcodigo.Text = "";
        }

        protected void btnAtras_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/OPE/lstOpeBrigada.aspx");
        }
        
        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            limpiarControles();
        }
        
		protected void btnRegistrar_Click(object sender, EventArgs e)
        {
			Boolean bProcede = false;
            
			try
			{
				Page.Validate("cabecera");
                if (!Page.IsValid) return;
			
				rnOpeBrigada rnModificar = new rnOpeBrigada();
                entOpeBrigada objPag = new entOpeBrigada();
                cSessionHandler miSesion = new cSessionHandler();

                if (localid == 0)
				{
					//Apropiamos los valores para INSERTAR
                    objPag = new entOpeBrigada();

                    
                    objPag.id_departamento = rnModificar.GetColumnType(ddlid_departamento.SelectedValue, entOpeBrigada.Fields.id_departamento);
                    objPag.codigo_brigada = txtcodigo.Text;
                    objPag.apiestado = "ELABORADO";
				    objPag.usucre = miSesion.appUsuario.login;

					rnModificar.Insert(objPag);

				    
				}
				else
				{
					//Apropiamos los valores para MODIFICAR
					objPag = rnModificar.ObtenerObjeto(localid);
                    objPag.id_departamento = rnModificar.GetColumnType(ddlid_departamento.SelectedValue, entOpeBrigada.Fields.id_departamento);
                    objPag.codigo_brigada = txtcodigo.Text;
                    objPag.apiestado = "ELABORADO";
                    objPag.usumod = miSesion.appUsuario.login; 
                    
                    rnModificar.Update(objPag);
                }				
	            
				limpiarControles();
				bProcede = true;
			}
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this,exp);
            }
			if (bProcede)
                Response.Redirect("~/OPE/lstOpeBrigada.aspx");
        }      
    }
}
