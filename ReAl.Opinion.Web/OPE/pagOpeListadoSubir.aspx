<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="pagOpeListadoSubir.aspx.cs" Inherits="ReAl.Opinion.Web.OPE.pagOpeListadoSubir" Title="pagOpeListadoSubir" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="server">
    <link href="<%= ResolveClientUrl("~/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css") %>" rel="stylesheet">
    <link href="<%= ResolveClientUrl("~/bower_components/datatables-responsive/css/dataTables.responsive.css") %>" rel="stylesheet">
    <link href="<%= ResolveClientUrl("~/Content/bootstrap-fileinput/css/fileinput.min.css") %>" rel="stylesheet">
    <link href="<%= ResolveClientUrl("~/Content/bootstrap-datetimepicker.css") %>" rel="stylesheet">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentSection" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Subir Listado Viviendas</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>            
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Descargar Plantilla Listado de Viviendas
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <asp:Button ID="btnDescargar" runat="server" CssClass="btn btn-success" 
										CausesValidation="true" UseSubmitBehavior="False" AccessKey="R" 
										Text="Decargar Plantilla LVs" onclick="btnDescargar_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Subir Archivo con Listado de Viviendas
                        </div>
                        <div class="panel-body">
							<div>
								<asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="cabecera" 
                                    DisplayMode="BulletList" ShowMessageBox="True" 
									class="alert alert-danger" CssClass="alert alert-danger"
							        HeaderText="Debe considerar las siguientes observaciones antes de continuar" />                                
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label id="Label1" runat="server">Archivo:</label>

                                        <asp:FileUpload ID="FileUpload1" runat="server" 
                                            CssClass="file text-center center-block well well-sm" 
                                            Class="file text-center center-block well well-sm"
										    multiple="false"
                                            data-show-upload="false" data-show-caption="true"/>
										
									</div>
                                    <div class="form-group">
                                        <!-- Botones de Accion -->
                                        <asp:Button ID="btnGuardar" runat="server" CssClass="btn btn-success" 
										    CausesValidation="true" UseSubmitBehavior="False" AccessKey="R" 
										    Text="Subir Archivo con LVs" onclick="btnRegistrar_Click" />
                                    </div>

                                </div>
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>
			<asp:HiddenField ID="hdnIdDatos" runat="server" />
        </ContentTemplate>	   
        <Triggers>
                <asp:PostBackTrigger ControlID="btnGuardar" />
            <asp:PostBackTrigger ControlID="btnDescargar" />
        </Triggers>     
    </asp:UpdatePanel>
    
    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="server">
    <script src="<%= ResolveClientUrl("~/bower_components/datatables/media/js/jquery.dataTables.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js") %>"></script>    
    <script src="<%= ResolveClientUrl("~/Scripts/fileinput.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Scripts/fileinput_locale_es.js") %>"></script>

    <script type="text/javascript" src="<%= ResolveClientUrl("~/Scripts/moment.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("~/Scripts/bootstrap-datetimepicker.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("~/Scripts/bootstrap-datetimepicker.min.js") %>"></script>
    
    <script>
	    var confirmarRegistro = function () {
	        BootstrapDialog.confirm({
	            title: 'Confirmar acci&oacute;n',
	            message: '¿Est&aacute; seguro que desea guardar la informaci&oacute;n?',
	            type: BootstrapDialog.TYPE_PRIMARY, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
	            draggable: true, // <-- Default value is false
	            btnCancelLabel: 'No estoy seguro', // <-- Default value is 'Cancel',
	            btnOKLabel: 'De acuerdo', // <-- Default value is 'OK',
	            btnOKClass: 'btn-success', // <-- If you didn't specify it, dialog type will be used,
	            callback: function (result) {
	                // result will be true if button was click, while it will be false if users close the dialog directly.
	                if (result) {
	                    <%=Page.ClientScript.GetPostBackEventReference(btnGuardar, string.Empty)%>;
	                } else {
	                    return false;
	                }
	                return false;
	            }
	        });
	    };
	</script> 
     <script type="text/javascript">
        $(function () {
            $("#datetimepicker1").datetimepicker({
                format:"DD/MM/YYYY"
            });
        });
    </script>
</asp:Content>

