﻿#region 
/***********************************************************************************************************
	NOMBRE:       lstEncPregunta
	DESCRIPCION:
		Clase que define los metodos y propiedades de la página docEncPregunta

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        09/07/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion

#region

using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReAl.Opinion.Dal.Entidades;
using ReAl.Opinion.Dal.Modelo;
using ReAl.Opinion.Web.App_Class;
using Microsoft.Ajax.Utilities;


#endregion

namespace ReAl.Opinion.Web.OPE
{
    public partial class docOpeAsignacion : Page
    {
		#region Parametros de llegada
        public int localid
        {
            get
            {
                if (Request["id"] == null)
                {
                    return 0;
                }
                return int.Parse(Request["id"].ToString());
            }
        }
        #endregion

		protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //Titulo de Pagina
                this.Title = cParametrosWeb.strNombrePagina;

                //Otorgamos valores a los Labels
                CargarDdlDepartamento();
                CargarDdlBrigada();
                CargarDdlUsuario();
                CargarDdlTablet();

                //Colocamos los mensajes de Error

                if (localid > 0)
                {
                    llenarControles();
                }	
            }
            
        }
        
        private void CargarDdlDepartamento()
        {
            try
            {
                //Funcion que inicializa los controles
                cSessionHandler miSesion = new cSessionHandler();
                DataTable dt = cUsuarioRestriccion.ObtenerDataTableDepto(miSesion.appUsuario.login);

                ddlid_departamento.DataValueField = entCatDepartamento.Fields.id_departamento.ToString();
                ddlid_departamento.DataTextField = entCatDepartamento.Fields.nombre.ToString();
                ddlid_departamento.DataSource = dt;
                ddlid_departamento.DataBind();
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }
        }

        private void CargarDdlUsuario()
        {
            try
            {
                //Funcion que inicializa los controles
                rnVista rn = new rnVista();
                cSessionHandler miSesion = new cSessionHandler();
                ArrayList arrNomParam = new ArrayList();
                arrNomParam.Add("pid_proyecto");
                arrNomParam.Add("pid_departamento");
                ArrayList arrParam = new ArrayList();
                arrParam.Add(miSesion.appRestriccion.id_proyecto);
                arrParam.Add(int.Parse(ddlid_departamento.SelectedValue));

                DataTable dt = rn.ObtenerDatosProcAlm("fn_ope_asignacion_usu", arrNomParam, arrParam);

                ddlid_usuario.DataValueField = entSegUsuario.Fields.id_usuario.ToString();
                ddlid_usuario.DataTextField = entSegUsuario.Fields.nombre.ToString();
                ddlid_usuario.DataSource = dt;
                ddlid_usuario.DataBind();
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }
        }

        private void CargarDdlBrigada()
        {
            try
            {
                //Funcion que inicializa los controles
                rnOpeBrigada rn = new rnOpeBrigada();
                DataTable dt = rn.ObtenerDataTable(entOpeBrigada.Fields.id_departamento, ddlid_departamento.SelectedValue);

                ddlid_brigada.DataValueField = entOpeBrigada.Fields.id_brigada.ToString();
                ddlid_brigada.DataTextField = entOpeBrigada.Fields.codigo_brigada.ToString();
                ddlid_brigada.DataSource = dt;
                ddlid_brigada.DataBind();
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }
        }

        private void CargarDdlTablet()
        {
            try
            {
                //Funcion que inicializa los controles
                rnVista rn = new rnVista();

                ArrayList arrNomParam = new ArrayList();
                arrNomParam.Add("pid_proyecto");
                ArrayList arrParam = new ArrayList();
                arrParam.Add(1);
                DataTable dt = rn.ObtenerDatosProcAlm("fn_ope_asignacion_tablet", arrNomParam, arrParam);

                ddlid_tablet.DataValueField = entOpeTablet.Fields.id_tablet.ToString();
                ddlid_tablet.DataTextField = entOpeTablet.Fields.imei.ToString();
                ddlid_tablet.DataSource = dt;
                ddlid_tablet.DataBind();
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }
        }

        protected void llenarControles()
        {
			try
			{
				//Funcion que inicializa los controles
				rnOpeAsignacion rnModificar = new rnOpeAsignacion();
                entOpeAsignacion objPag = rnModificar.ObtenerObjeto(localid);

				if(objPag != null)
				{
                    //Asignamos los valores
                    ddlid_brigada.SelectedValue = objPag.id_brigada.ToString();
                    ddlid_usuario.SelectedValue = objPag.id_usuario.ToString();
                    ddlid_tablet.SelectedValue = objPag.id_tablet.ToString();
                    
                }
			}
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }
        }

        protected void limpiarControles()
        {
            //Funcion que inicializa los controles
            //txtcodigo.Text = "";
        }

        protected void btnAtras_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/OPE/lstOpeAsignacion.aspx");
        }
        
        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            limpiarControles();
        }
        
		protected void btnRegistrar_Click(object sender, EventArgs e)
        {
			Boolean bProcede = false;
            
			try
			{
				Page.Validate("cabecera");
                if (!Page.IsValid) return;
			
				rnOpeAsignacion rnModificar = new rnOpeAsignacion();
                entOpeAsignacion objPag = new entOpeAsignacion();
                cSessionHandler miSesion = new cSessionHandler();

                if (localid == 0)
				{
					//Apropiamos los valores para INSERTAR
                    objPag = new entOpeAsignacion();

                    
                    objPag.id_brigada = rnModificar.GetColumnType(ddlid_brigada.SelectedValue, entOpeAsignacion.Fields.id_brigada);
                    objPag.id_usuario = rnModificar.GetColumnType(ddlid_usuario.SelectedValue, entOpeAsignacion.Fields.id_usuario);
                    objPag.id_tablet = rnModificar.GetColumnType(ddlid_tablet.SelectedValue, entOpeAsignacion.Fields.id_tablet);
				    objPag.id_proyecto = miSesion.appRestriccion.id_proyecto;
                    objPag.apiestado = "ELABORADO";
				    objPag.usucre = miSesion.appUsuario.login;

					rnModificar.Insert(objPag);

				    
				}
				else
				{
					//Apropiamos los valores para MODIFICAR
					objPag = rnModificar.ObtenerObjeto(localid);
                    objPag.id_brigada = rnModificar.GetColumnType(ddlid_brigada.SelectedValue, entOpeAsignacion.Fields.id_brigada);
                    objPag.id_usuario = rnModificar.GetColumnType(ddlid_usuario.SelectedValue, entOpeAsignacion.Fields.id_usuario);
                    objPag.id_tablet = rnModificar.GetColumnType(ddlid_tablet.SelectedValue, entOpeAsignacion.Fields.id_tablet);
                    objPag.id_proyecto = miSesion.appRestriccion.id_proyecto;
                    objPag.apiestado = "ELABORADO";
                    objPag.usumod = miSesion.appUsuario.login; 
                    
                    rnModificar.Update(objPag);
                }				
	            
				limpiarControles();
				bProcede = true;
			}
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this,exp);
            }
			if (bProcede)
                Response.Redirect("~/OPE/lstOpeAsignacion.aspx");
        }

        protected void ddlid_departamento_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargarDdlBrigada();
            CargarDdlUsuario();
            CargarDdlTablet();
        }
    }
}
