﻿#region 
/***********************************************************************************************************
	NOMBRE:       lstEncPregunta
	DESCRIPCION:
		Clase que define los metodos y propiedades de la página docEncPregunta

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        09/07/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion

#region

using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReAl.Opinion.Dal.Entidades;
using ReAl.Opinion.Dal.Modelo;
using ReAl.Opinion.Web.App_Class;
using Microsoft.Ajax.Utilities;


#endregion

namespace ReAl.Opinion.Web.OPE
{
    public partial class docOpeListado : Page
    {
		#region Parametros de llegada
        public int localid
        {
            get
            {
                if (Request["id"] == null)
                {
                    return 0;
                }
                return int.Parse(Request["id"].ToString());
            }
        }
        #endregion

		protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //Titulo de Pagina
                this.Title = cParametrosWeb.strNombrePagina;

                //Otorgamos valores a los Labels
                CargarDdlUpm();
                //Colocamos los mensajes de Error
                
                if (localid > 0)
                {
                    llenarControles();
                }	
            }
            
        }
        
        private void CargarDdlUpm()
        {
            try
            {
                //Funcion que inicializa los controles
                cSessionHandler miSesion = new cSessionHandler();
                DataTable dt = cUsuarioRestriccion.ObtenerDataTableUpm(miSesion.appUsuario.login, miSesion.appRestriccion.id_proyecto, miSesion.appRestriccion.departamentos);
                DataView dv = dt.DefaultView;
                dv.Sort = entCatUpm.Fields.codigo.ToString();

                ddlid_upm.DataValueField = entCatUpm.Fields.id_upm.ToString();
                ddlid_upm.DataTextField = entCatUpm.Fields.codigo.ToString();
                ddlid_upm.DataSource = dv.Table;
                ddlid_upm.DataBind();
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }
        }

		protected void llenarControles()
        {
			try
			{
				//Funcion que inicializa los controles
				rnOpeListado rnModificar = new rnOpeListado();
				entOpeListado objPag = rnModificar.ObtenerObjeto(localid);

				if(objPag != null)
				{
                    //Asignamos los valores
				    ddlid_upm.SelectedValue = objPag.id_upm.ToString();
                    txtcodigo_manzana_comunidad.Text = objPag.codigo_manzana_comunidad;
                    txtavenida_calle.Text = objPag.avenida_calle;
                    txtnro_predio.Text = objPag.nro_predio.ToString();
                    txtnro_puerta.Text = objPag.nro_puerta;
                    txtnro_orden_vivienda.Text = objPag.nro_orden_vivienda.ToString();
                    txtnro_piso.Text = objPag.nro_piso;
                    txtnro_depto.Text = objPag.nro_depto;
                    txtuso_vivienda.Text = objPag.uso_vivienda;
                    txtnro_hogares.Text = objPag.nro_hogares.ToString();
                    txtnro_hombres.Text = objPag.nro_hombres.ToString();
                    txtnro_mujeres.Text = objPag.nro_mujeres.ToString();
                    txtnombre_jefe.Text = objPag.nombre_jefe.ToString();
                    txtnro_voe.Text = objPag.nro_voe.ToString();
                }
			}
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }
        }

        protected void limpiarControles()
        {
            //Funcion que inicializa los controles
            //txtcodigo_manzana_comunidad.Text = "";
            txtavenida_calle.Text = "";
            txtnro_predio.Text = "";
            txtnro_puerta.Text = "";
            txtnro_orden_vivienda.Text = "";
            txtnro_piso.Text = "";
            txtnro_depto.Text = "";
            txtuso_vivienda.Text = "";
            txtnro_hogares.Text = "";
            txtnro_hombres.Text = "";
            txtnro_mujeres.Text = "";
            txtnombre_jefe.Text = "";
            txtnro_voe.Text = "";
        }

        protected void btnAtras_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/OPE/lstOpeListado.aspx");
        }
        
        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            limpiarControles();
        }
        
		protected void btnRegistrar_Click(object sender, EventArgs e)
        {
			Boolean bProcede = false;

		    try
		    {
                Page.Validate("cabecera");
                if (!Page.IsValid) return;

                bProcede = guardarRegistro();
		    }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }

            if (bProcede)
                Response.Redirect("~/OPE/lstOpeListado.aspx");
        }

        protected void btnRegistrarNuevo_Click(object sender, EventArgs e)
        {
            Boolean bProcede = false;
            try
            {
                Page.Validate("cabecera");
                if (!Page.IsValid) return;

                bProcede = guardarRegistro();
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }

            if (bProcede)
                limpiarControles();
        }

        private Boolean guardarRegistro()
        {
            Boolean bProcede = false;
            
            
            rnOpeListado rnModificar = new rnOpeListado();
            entOpeListado objPag = new entOpeListado();
            cSessionHandler miSesion = new cSessionHandler();

            if (localid == 0)
            {
                //Apropiamos los valores para INSERTAR
                objPag = new entOpeListado();
                objPag.id_upm = rnModificar.GetColumnType(ddlid_upm.SelectedValue, entOpeListado.Fields.id_upm);
                objPag.codigo_manzana_comunidad = txtcodigo_manzana_comunidad.Text;
                objPag.avenida_calle = txtavenida_calle.Text;
                objPag.nro_predio = txtnro_predio.Text.IsNullOrEmptyAfterTrimmed() ? 0 : int.Parse(txtnro_predio.Text);
                objPag.nro_puerta = txtnro_puerta.Text;
                objPag.nro_orden_vivienda = txtnro_orden_vivienda.Text.IsNullOrEmptyAfterTrimmed() ? 0 : int.Parse(txtnro_orden_vivienda.Text);
                objPag.nro_piso = txtnro_piso.Text;
                objPag.nro_depto = txtnro_depto.Text;
                objPag.uso_vivienda = txtuso_vivienda.Text;
                objPag.nro_hogares = txtnro_hogares.Text.IsNullOrEmptyAfterTrimmed() ? 0 : int.Parse(txtnro_hogares.Text);
                objPag.nro_hombres = txtnro_hombres.Text.IsNullOrEmptyAfterTrimmed() ? 0 : int.Parse(txtnro_hombres.Text);
                objPag.nro_mujeres = txtnro_mujeres.Text.IsNullOrEmptyAfterTrimmed() ? 0 : int.Parse(txtnro_mujeres.Text);
                objPag.nombre_jefe = txtnombre_jefe.Text;
                objPag.nro_voe = txtnro_voe.Text.IsNullOrEmptyAfterTrimmed() ? 0 : int.Parse(txtnro_voe.Text);

                objPag.apiestado = "ELABORADO";
                objPag.usucre = miSesion.appUsuario.login;

                rnModificar.Insert(objPag);


            }
            else
            {
                //Apropiamos los valores para MODIFICAR
                objPag = rnModificar.ObtenerObjeto(localid);
                objPag.codigo_manzana_comunidad = txtcodigo_manzana_comunidad.Text;
                objPag.avenida_calle = txtavenida_calle.Text;
                objPag.nro_predio = txtnro_predio.Text.IsNullOrEmptyAfterTrimmed() ? 0 : int.Parse(txtnro_predio.Text);
                objPag.nro_puerta = txtnro_puerta.Text;
                objPag.nro_orden_vivienda = txtnro_orden_vivienda.Text.IsNullOrEmptyAfterTrimmed() ? 0 : int.Parse(txtnro_orden_vivienda.Text);
                objPag.nro_piso = txtnro_piso.Text;
                objPag.nro_depto = txtnro_depto.Text;
                objPag.uso_vivienda = txtuso_vivienda.Text;
                objPag.nro_hogares = txtnro_hogares.Text.IsNullOrEmptyAfterTrimmed() ? 0 : int.Parse(txtnro_hogares.Text);
                objPag.nro_hombres = txtnro_hombres.Text.IsNullOrEmptyAfterTrimmed() ? 0 : int.Parse(txtnro_hombres.Text);
                objPag.nro_mujeres = txtnro_mujeres.Text.IsNullOrEmptyAfterTrimmed() ? 0 : int.Parse(txtnro_mujeres.Text);
                objPag.nombre_jefe = txtnombre_jefe.Text;
                objPag.nro_voe = txtnro_voe.Text.IsNullOrEmptyAfterTrimmed() ? 0 : int.Parse(txtnro_voe.Text);
                objPag.apiestado = "ELABORADO";
                objPag.usumod = miSesion.appUsuario.login;

                rnModificar.Update(objPag);
            }

            limpiarControles();
            bProcede = true;
            
            return bProcede;
        }
    }
}
