﻿#region 
/***********************************************************************************************************
	NOMBRE:       lstEncPregunta
	DESCRIPCION:
		Clase que define los metodos y propiedades de la página lstEncPregunta

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        09/07/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion

#region

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReAl.Opinion.Dal.Entidades;
using ReAl.Opinion.Dal.Modelo;
using ReAl.Opinion.Web.App_Class;

#endregion

namespace ReAl.Opinion.Web.OPE
{
    public partial class lstOpeMovimiento : Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Validamos el salto directo
                cSessionHandler miSesion = new cSessionHandler();
                if (miSesion.arrMenu != null)
                    if (!miSesion.arrMenu.Contains(Request.AppRelativeCurrentExecutionFilePath))
                        Server.Transfer("~/Template/Dashboard.aspx");


                //Titulo de Pagina
                this.Title = cParametrosWeb.strNombrePagina;

                if (!Page.IsPostBack)
                {
                    CargarDdlDepartamento();
                    cargarListadoTecnicos();
                    cargarListadoUpm();
                    cargarAsignacion();
                }


                if (dtgListadoEncuestador.Rows.Count > 0)
                {
                    dtgListadoEncuestador.HeaderRow.TableSection = TableRowSection.TableHeader;

                    for (int i = 0; i < dtgListadoEncuestador.Columns.Count; i++)
                        dtgListadoEncuestador.HeaderRow.Cells[i].Attributes.Add("data-sortable", "true");
                }

                if (dtgListadoUpm.Rows.Count > 0)
                {
                    dtgListadoUpm.HeaderRow.TableSection = TableRowSection.TableHeader;

                    for (int i = 0; i < dtgListadoUpm.Columns.Count; i++)
                        dtgListadoUpm.HeaderRow.Cells[i].Attributes.Add("data-sortable", "true");
                }

                if (dtgListado.Rows.Count > 0)
                {
                    dtgListado.HeaderRow.TableSection = TableRowSection.TableHeader;

                    for (int i = 0; i < dtgListado.Columns.Count; i++)
                        dtgListado.HeaderRow.Cells[i].Attributes.Add("data-sortable", "true");
                }
            }
            catch (Exception exp)
            {

                ((Main)this.Master).mostrarPopUp(this, exp);
            }            
        }

        private void CargarDdlDepartamento()
        {
            try
            {
                //Funcion que inicializa los controles
                cSessionHandler miSesion = new cSessionHandler();
                DataTable dt = cUsuarioRestriccion.ObtenerDataTableDepto(miSesion.appUsuario.login);

                ddlid_departamento.DataValueField = entCatDepartamento.Fields.id_departamento.ToString();
                ddlid_departamento.DataTextField = entCatDepartamento.Fields.nombre.ToString();
                ddlid_departamento.DataSource = dt;
                ddlid_departamento.DataBind();
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }
        }

        private void cargarListadoTecnicos()
        {
			try
			{
                cSessionHandler miSesion = new cSessionHandler();
                String strNombreVista = "vw_ope_encuestadores_lis";

                ArrayList arrNomParam = new ArrayList();
                arrNomParam.Add(entCatUpm.Fields.id_departamento.ToString());
                arrNomParam.Add(entCatUpm.Fields.id_proyecto.ToString());
                ArrayList arrParam = new ArrayList();
                arrParam.Add(ddlid_departamento.SelectedValue);
                arrParam.Add(miSesion.appRestriccion.id_proyecto);

                rnVista rn = new rnVista();
			    DataTable listTable = rn.ObtenerDatos(strNombreVista, arrNomParam, arrParam);
                listTable.Columns[0].ColumnName = "id";
                dtgListadoEncuestador.DataSource = listTable;
                dtgListadoEncuestador.DataBind();

			    if (listTable.Rows.Count > 0)
			    {
                    dtgListadoEncuestador.HeaderRow.TableSection = TableRowSection.TableHeader;

                    for (int i = 0; i < dtgListadoEncuestador.Columns.Count; i++)
                        dtgListadoEncuestador.HeaderRow.Cells[i].Attributes.Add("data-sortable", "true");
                }
			}
			catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp);
            }
        }

        private void cargarListadoUpm()
        {
            try
            {
                cSessionHandler miSesion = new cSessionHandler();
                String strNombreVista = "vw_cat_upm_lis";

                ArrayList arrNomParam = new ArrayList();
                arrNomParam.Add(entCatUpm.Fields.id_departamento.ToString());
                arrNomParam.Add(entCatUpm.Fields.id_proyecto.ToString());
                ArrayList arrParam = new ArrayList();
                arrParam.Add(ddlid_departamento.SelectedValue);
                arrParam.Add(miSesion.appRestriccion.id_proyecto);

                rnVista rn = new rnVista();
                DataTable listTable = rn.ObtenerDatos(strNombreVista, arrNomParam, arrParam);
                listTable.Columns[0].ColumnName = "id";
                dtgListadoUpm.DataSource = listTable;
                dtgListadoUpm.DataBind();

                if (listTable.Rows.Count > 0)
                {
                    dtgListadoUpm.HeaderRow.TableSection = TableRowSection.TableHeader;

                    for (int i = 0; i < dtgListadoUpm.Columns.Count; i++)
                        dtgListadoUpm.HeaderRow.Cells[i].Attributes.Add("data-sortable", "true");
                }
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp);
            }
        }

        private void cargarAsignacion()
        {
            try
            {
                cSessionHandler miSesion = new cSessionHandler();
                String strNombreVista = "vw_ope_movimiento_personal_lis";

                ArrayList arrNomParam = new ArrayList();
                arrNomParam.Add(entCatUpm.Fields.id_departamento.ToString());
                arrNomParam.Add(entCatUpm.Fields.id_proyecto.ToString());
                ArrayList arrParam = new ArrayList();
                arrParam.Add(ddlid_departamento.SelectedValue);
                arrParam.Add(miSesion.appRestriccion.id_proyecto);

                rnVista rn = new rnVista();
                DataTable listTable = rn.ObtenerDatos(strNombreVista, arrNomParam, arrParam);
                listTable.Columns[0].ColumnName = "id";
                dtgListado.DataSource = listTable;
                dtgListado.DataBind();

                if (listTable.Rows.Count > 0)
                {
                    dtgListado.HeaderRow.TableSection = TableRowSection.TableHeader;

                    for (int i = 0; i < dtgListado.Columns.Count; i++)
                        dtgListado.HeaderRow.Cells[i].Attributes.Add("data-sortable", "true");
                }
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp);
            }
        }


        protected void btnAtras_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Template/Dashboard.aspx");
        }

        protected void btnRegistrar_Click(object sender, EventArgs e)
        {
            try
            {
                cSessionHandler miSesion = new cSessionHandler();
                //Iteramos las Brigadas
                foreach (GridViewRow rowTec in dtgListadoEncuestador.Rows)
                {
                    if (rowTec.RowType == DataControlRowType.DataRow)
                    {
                        RadioButton rdbRow = (rowTec.Cells[0].FindControl("rdbRow") as RadioButton);
                        if (rdbRow.Checked)
                        {
                            //string idBrigada = rowBri.Cells[1].Text;
                            string idAsignacion = (rowTec.Cells[0].FindControl("hddid") as HiddenField).Value;
                            //Iteramos las UPMs                    

                            foreach (GridViewRow rowUpm in dtgListadoUpm.Rows)
                            {
                                if (rowUpm.RowType == DataControlRowType.DataRow)
                                {
                                    CheckBox chkRow = (rowUpm.Cells[0].FindControl("chkRow") as CheckBox);
                                    if (chkRow.Checked)
                                    {
                                        //string idUpm = rowUpm.Cells[1].Text;
                                        string idUpm = (rowUpm.Cells[0].FindControl("hddid") as HiddenField).Value;
                                        //Insertamos la asignacion para cada Usuario de la Brigada
                                        rnOpeMovimiento rnMov = new rnOpeMovimiento();

                                        //Verificamos si existe el par asignado en estado elaborado
                                        ArrayList arrColWhereMov = new ArrayList();
                                        arrColWhereMov.Add(entOpeMovimiento.Fields.id_asignacion.ToString());
                                        arrColWhereMov.Add(entOpeMovimiento.Fields.id_upm.ToString());                                            
                                        ArrayList arrValWhereMov = new ArrayList();
                                        arrValWhereMov.Add(rnMov.GetColumnType(idAsignacion, entOpeMovimiento.Fields.id_asignacion));
                                        arrValWhereMov.Add(int.Parse(idUpm));
                                        
                                        entOpeMovimiento objMov = rnMov.ObtenerObjeto(arrColWhereMov, arrValWhereMov);
                                        if (objMov == null)
                                        {
                                            objMov = new entOpeMovimiento();
                                            objMov.id_asignacion = rnMov.GetColumnType(idAsignacion, entOpeMovimiento.Fields.id_asignacion );
                                            objMov.id_upm = int.Parse(idUpm);
                                            objMov.gestion = DateTime.Now.Year;
                                            objMov.mes = DateTime.Now.Month;
                                            objMov.apiestado = "ELABORADO";
                                            objMov.usucre = miSesion.appUsuario.login;                                                

                                            rnMov.Insert(objMov);
                                        }
                                        else
                                        {
                                            if (objMov.apiestado == "ELIMINADO")
                                            {
                                                objMov.apiestado = "ELABORADO";
                                                objMov.gestion = DateTime.Now.Year;
                                                objMov.mes = DateTime.Now.Month;
                                                objMov.usucre = miSesion.appUsuario.login;
                                                rnMov.Update(objMov);
                                            }
                                        }
                                        
                                    }
                                }
                            }

                        }
                    }
                }
                cargarListadoTecnicos();
                cargarListadoUpm();
                cargarAsignacion();
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp);
            }
        }

        protected void dtgListado_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            String strRedireccion = "";
            try
            {
                if (e.CommandName.Equals("eliminar"))
                {
                    string idMovimiento = e.CommandArgument.ToString();
                    rnOpeMovimiento rnModificar = new rnOpeMovimiento();
                    cSessionHandler miSesion = new cSessionHandler();

                    //Partimos el id segun el -                    
                    entOpeMovimiento objMov = rnModificar.ObtenerObjeto(int.Parse(idMovimiento));
                    objMov.apiestado = "ELIMINADO";
                    objMov.usucre = miSesion.appUsuario.login;
                    rnModificar.Update(objMov);
                    
                    cargarListadoTecnicos();
                    cargarListadoUpm();
                    cargarAsignacion();
                }
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }

            if (!strRedireccion.IsNullOrEmpty())
                Response.Redirect(strRedireccion);
        }

        protected void ddlid_departamento_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargarListadoTecnicos();
            cargarListadoUpm();
            cargarAsignacion();
        }

        protected void rb_CheckedChanged(object sender, EventArgs e)
        {
            GridViewRow row = (GridViewRow)((RadioButton)sender).NamingContainer;
            foreach (GridViewRow r in dtgListadoEncuestador.Rows)
            {
                if (r != row)
                    ((RadioButton)r.FindControl("rdbRow")).Checked = false;
            }
        }
    }
}
