﻿#region 
/***********************************************************************************************************
	NOMBRE:       lstEncPregunta
	DESCRIPCION:
		Clase que define los metodos y propiedades de la página docEncPregunta

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        09/07/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion

#region

using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReAl.Opinion.Dal.Entidades;
using ReAl.Opinion.Dal.Modelo;
using ReAl.Opinion.Web.App_Class;
using Microsoft.Ajax.Utilities;


#endregion

namespace ReAl.Opinion.Web.OPE
{
    public partial class docOpeTablet : Page
    {
		#region Parametros de llegada
        public int localid
        {
            get
            {
                if (Request["id"] == null)
                {
                    return 0;
                }
                return int.Parse(Request["id"].ToString());
            }
        }
        #endregion

		protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //Titulo de Pagina
                this.Title = cParametrosWeb.strNombrePagina;

                //Otorgamos valores a los Labels
                //Colocamos los mensajes de Error
                
                if (localid > 0)
                {
                    llenarControles();
                }	
            }
            
        }        

		protected void llenarControles()
        {
			try
			{
				//Funcion que inicializa los controles
				rnOpeTablet rnModificar = new rnOpeTablet();
                entOpeTablet objPag = rnModificar.ObtenerObjeto(localid);

				if(objPag != null)
				{
					//Asignamos los valores
				    txtimie.Text = objPag.imei;
                    txtmarca.Text = objPag.marca;
                    txtmodelo.Text = objPag.modelo;
                }
			}
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }
        }

        protected void limpiarControles()
        {
            //Funcion que inicializa los controles
            txtimie.Text = "";
            txtmarca.Text = "";
            txtmodelo.Text = "";
        }

        protected void btnAtras_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/OPE/lstOpeTablet.aspx");
        }
        
        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            limpiarControles();
        }
        
		protected void btnRegistrar_Click(object sender, EventArgs e)
        {
			Boolean bProcede = false;
            
			try
			{
				Page.Validate("cabecera");
                if (!Page.IsValid) return;
			
				rnOpeTablet rnModificar = new rnOpeTablet();
                entOpeTablet objPag = new entOpeTablet();
                cSessionHandler miSesion = new cSessionHandler();

                if (localid == 0)
				{
					//Apropiamos los valores para INSERTAR
                    objPag = new entOpeTablet();
                    
                    objPag.imei = txtimie.Text;
                    objPag.marca = txtmarca.Text;
                    objPag.modelo = txtmodelo.Text;
                    objPag.apiestado = "ELABORADO";
				    objPag.usucre = miSesion.appUsuario.login;

					rnModificar.Insert(objPag);

				    
				}
				else
				{
					//Apropiamos los valores para MODIFICAR
					objPag = rnModificar.ObtenerObjeto(localid);
                    objPag.imei = txtimie.Text;
                    objPag.marca = txtmarca.Text;
                    objPag.modelo = txtmodelo.Text;
                    objPag.usumod = miSesion.appUsuario.login; 
                    
                    rnModificar.Update(objPag);
                }				
	            
				limpiarControles();
				bProcede = true;
			}
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this,exp);
            }
			if (bProcede)
                Response.Redirect("~/OPE/lstOpeTablet.aspx");
        }      
    }
}
