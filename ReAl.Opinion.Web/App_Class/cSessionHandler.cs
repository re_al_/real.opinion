﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using ReAl.Opinion.Dal.Entidades;

namespace ReAl.Opinion.Web.App_Class
{
    public class cSessionHandler : System.Web.UI.Page
    {
        public DataTable dtMenu
        {
            get
            {
                DataTable sessionData = (DataTable)Session["dtMenu"];
                return sessionData;
            }
            set
            {
                Session["dtMenu"] = value;
            }
        }

        public ArrayList arrMenu
        {
            get
            {
                ArrayList sessionData = (ArrayList)Session["arrMenu"];
                return sessionData;
            }
            set
            {
                Session["arrMenu"] = value;
            }
        }

        public entSegUsuario appUsuario
        {
            get
            {
                entSegUsuario sessionData = (entSegUsuario)Session["SegUsuario"];
                return sessionData;
            }
            set
            {
                Session["SegUsuario"] = value;
            }
        }

        public entSegUsuariorestriccion appRestriccion
        {
            get
            {
                entSegUsuariorestriccion sessionData = (entSegUsuariorestriccion)Session["SegUsuariorestriccion"];
                return sessionData;
            }
            set
            {
                Session["SegUsuariorestriccion"] = value;
            }
        }

        public entSegRol appRol
        {
            get
            {
                entSegRol sessionData = (entSegRol)Session["SegRol"];
                return sessionData;
            }
            set
            {
                Session["SegRol"] = value;
            }
        }

        public String appUploadPath
        {
            get
            {
                String sessionData = Session["UploadPath"].ToString();
                return sessionData;
            }
            set
            {
                Session["UploadPath"] = value;
            }
        }
    }
}