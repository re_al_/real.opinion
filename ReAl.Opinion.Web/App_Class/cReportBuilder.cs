﻿using System.Collections;
using System.Data;
using ReAl.Opinion.Dal.Modelo;

namespace ReAl.Opinion.Web.App_Class
{
    public class cReportBuilder
    {
        public DataSet obtenerReporteSp(string strProcAlm, ArrayList arrNomParam, ArrayList arrParam, ArrayList arrHide)
        {
            rnVista rn = new rnVista();
            DataTable dtPopUp = rn.ObtenerDatosProcAlm(strProcAlm, arrNomParam, arrParam);
            dtPopUp.Columns[0].ColumnName = "id";

            DataTable dtListado = dtPopUp.Copy();


            foreach (string strColumna in arrHide)
            {
                dtListado.Columns.Remove(strColumna);
            }

            DataSet dsRpt = new DataSet();
            dsRpt.Tables.Add(dtPopUp);
            dsRpt.Tables.Add(dtListado);

            return dsRpt;
        }

        public DataSet obtenerReporteVista(string strVista, ArrayList arrNomParam, ArrayList arrParam, ArrayList arrHide)
        {
            rnVista rn = new rnVista();
            DataTable dtPopUp = rn.ObtenerDatos(strVista, arrNomParam, arrParam);
            dtPopUp.Columns[0].ColumnName = "id";

            DataTable dtListado = dtPopUp.Copy();


            foreach (string strColumna in arrHide)
            {
                dtListado.Columns.Remove(strColumna);
            }

            DataSet dsRpt = new DataSet();
            dsRpt.Tables.Add(dtPopUp);
            dsRpt.Tables.Add(dtListado);

            return dsRpt;
        }

        public DataSet obtenerReporteVista(DataTable dtPopUp, ArrayList arrHide)
        {
            DataTable dtListado = dtPopUp.Copy();


            foreach (string strColumna in arrHide)
            {
                dtListado.Columns.Remove(strColumna);
            }

            DataSet dsRpt = new DataSet();
            dsRpt.Tables.Add(dtPopUp);
            dsRpt.Tables.Add(dtListado);

            return dsRpt;
        }
    }
}