﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Web;
using ReAl.Opinion.Dal.Modelo;
using ReAl.Opinion.PgConn;
using ReAl.Opinion.Utils;
using Npgsql;
using NpgsqlTypes;

namespace ReAl.Opinion.Web.App_Class
{
    public class cConsolidacion
    {
        public Boolean ConsolidarZip(string strPath, string strUsuario, string strNombreArchivo)
        {
            string fileDest = strPath + strUsuario + "/";
            if (!Directory.Exists(fileDest))
                Directory.CreateDirectory(fileDest);

            string filePath = fileDest + strNombreArchivo;

            string strDataBaseName = "database" + DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ssss") + ".db";
            //Descomprimimos el archivo
            cFuncionesZip zip = cFuncionesZip.Open(filePath, FileAccess.Read);
            List<cFuncionesZip.ZipFileEntry> dir = zip.ReadCentralDir();
            foreach (cFuncionesZip.ZipFileEntry entry in dir)
            {
                //zip.ExtractFile(entry, fileDest + entry.FilenameInZip);
                zip.ExtractFile(entry, fileDest + strDataBaseName);
                break;
            }
            zip.Close();


            SQLiteConnection sql_con = new SQLiteConnection("Data Source=" + fileDest + strDataBaseName + ";Version=3;New=False;Compress=True;");

            //Actualizamos el id_tab del informante
            string txtSQLQueryInformante = "UPDATE enc_informante SET id_tab_informante = id_informante;";
            if (sql_con.State == ConnectionState.Closed)
                sql_con.Open();
            SQLiteCommand cmdUpdateInformante = new SQLiteCommand(txtSQLQueryInformante, sql_con);
            cmdUpdateInformante.ExecuteNonQuery();
            cmdUpdateInformante.Dispose();
            sql_con.Close();

            //Actualizamos el id_tab de la encuesta
            string txtSQLQuery = "UPDATE enc_encuesta SET id_tab_encuesta = id_encuesta;";
            if (sql_con.State == ConnectionState.Closed)
                sql_con.Open();
            SQLiteCommand cmdUpdate = new SQLiteCommand(txtSQLQuery, sql_con);
            cmdUpdate.ExecuteNonQuery();
            cmdUpdate.Dispose();
            sql_con.Close();

            //Leemos la BD SqLite                    
            if (sql_con.State == ConnectionState.Closed)
                sql_con.Open();
            string queryInformante = "SELECT * FROM enc_informante";
            SQLiteDataAdapter daInformante = new SQLiteDataAdapter(queryInformante, sql_con);
            DataTable dtEncInformante = new DataTable();
            daInformante.Fill(dtEncInformante);
            sql_con.Close();

            rnVista rn = new rnVista();
            foreach (DataRow row in dtEncInformante.Rows)
            {
                //Leemos la Encuesta del Informante
                if (sql_con.State == ConnectionState.Closed)
                    sql_con.Open();
                string queryEncuesta = " SELECT   id_encuesta, id_encuesta as id_tab_encuesta, id_informante, " +
                                       " id_movimiento, id_pregunta, id_respuesta, " +
                                       " codigo_respuesta, respuesta, fila, " +
                                       " latitud, longitud, COALESCE(observacion,'') as observacion, " +
                                       " id_last, apiestado, usucre, " +
                                       " feccre, usumod, COALESCE(fecmod,0) as fecmod " +
                                       " FROM enc_encuesta WHERE id_informante = " + row["id_informante"];
                SQLiteDataAdapter daEncuesta = new SQLiteDataAdapter(queryEncuesta, sql_con);
                DataTable dtEncEncuesta = new DataTable();
                daEncuesta.Fill(dtEncEncuesta);
                sql_con.Close();

                //Serializamos la encuesta
                string strEncuestaJson = cFuncionesJson.DataTableToJSON(dtEncEncuesta);

                //Insertamos en la BD
                cConn miCon = new cConn();
                NpgsqlCommand command = new NpgsqlCommand();
                command.CommandText = "fn_consolidar_informante";
                command.CommandType = CommandType.StoredProcedure;

                //Los parametros
                command.Parameters.Add(new NpgsqlParameter("ver_catalogo", ""));
                command.Parameters["ver_catalogo"].NpgsqlDbType = NpgsqlDbType.Text;

                command.Parameters.Add(new NpgsqlParameter("ver_app", ""));
                command.Parameters["ver_app"].NpgsqlDbType = NpgsqlDbType.Text;

                command.Parameters.Add(new NpgsqlParameter("id_informante", row["id_informante"]));
                command.Parameters["id_informante"].NpgsqlDbType = NpgsqlDbType.Integer;

                command.Parameters.Add(new NpgsqlParameter("id_informante_padre", row["id_informante_padre"].ToString().IsNullOrEmptyAfterTrimmed() ? 0 : row["id_informante_padre"]));
                command.Parameters["id_informante_padre"].NpgsqlDbType = NpgsqlDbType.Integer;

                command.Parameters.Add(new NpgsqlParameter("id_upm", row["id_upm"].ToString().IsNullOrEmptyAfterTrimmed() ? 0 : row["id_upm"]));
                command.Parameters["id_upm"].NpgsqlDbType = NpgsqlDbType.Integer;

                command.Parameters.Add(new NpgsqlParameter("id_nivel", row["id_nivel"]));
                command.Parameters["id_nivel"].NpgsqlDbType = NpgsqlDbType.Integer;

                command.Parameters.Add(new NpgsqlParameter("id_movimiento", row["id_movimiento"]));
                command.Parameters["id_movimiento"].NpgsqlDbType = NpgsqlDbType.Integer;

                command.Parameters.Add(new NpgsqlParameter("latitud", row["latitud"]));
                command.Parameters["latitud"].NpgsqlDbType = NpgsqlDbType.Numeric;

                command.Parameters.Add(new NpgsqlParameter("longitud", row["longitud"]));
                command.Parameters["longitud"].NpgsqlDbType = NpgsqlDbType.Numeric;

                command.Parameters.Add(new NpgsqlParameter("codigo", row["codigo"]));
                command.Parameters["codigo"].NpgsqlDbType = NpgsqlDbType.Text;

                command.Parameters.Add(new NpgsqlParameter("descripcion", row["descripcion"]));
                command.Parameters["descripcion"].NpgsqlDbType = NpgsqlDbType.Text;

                command.Parameters.Add(new NpgsqlParameter("apiestado", row["apiestado"]));
                command.Parameters["apiestado"].NpgsqlDbType = NpgsqlDbType.Text;

                command.Parameters.Add(new NpgsqlParameter("usucre", row["usucre"]));
                command.Parameters["usucre"].NpgsqlDbType = NpgsqlDbType.Text;

                command.Parameters.Add(new NpgsqlParameter("feccre", row["feccre"]));
                command.Parameters["feccre"].NpgsqlDbType = NpgsqlDbType.Integer;

                command.Parameters.Add(new NpgsqlParameter("usumod", row["usumod"]));
                command.Parameters["usumod"].NpgsqlDbType = NpgsqlDbType.Text;

                command.Parameters.Add(new NpgsqlParameter("fecmod", row["fecmod"] ?? 0));
                command.Parameters["fecmod"].NpgsqlDbType = NpgsqlDbType.Integer;

                command.Parameters.Add(new NpgsqlParameter("encuesta", strEncuestaJson));
                command.Parameters["encuesta"].NpgsqlDbType = NpgsqlDbType.Json;

                command.Connection = miCon.conexionBD;

                command.Connection.Open();
                int intRes = command.ExecuteNonQuery();

                command.Connection.Close();
                command.Dispose();
            }

            return true;
        }

    }
}