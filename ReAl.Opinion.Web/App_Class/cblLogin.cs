﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ReAl.Opinion.Dal.Entidades;
using ReAl.Opinion.Dal.Modelo;
using ReAl.Opinion.Utils;


namespace ReAl.Opinion.Web.App_Class
{
    public static class cblLogin
    {
        
        public static bool WebValidarUsuario(ref string strValidacion, ref System.Web.UI.WebControls.TextBox txtUsuario, ref System.Web.UI.WebControls.TextBox txtPass)
        {
            bool bProcede = true;
            bool pTieneFoco = false;

            //Validamos el Login
            if (string.IsNullOrEmpty(txtUsuario.Text))
            {
                strValidacion = Environment.NewLine + "Debe ingresar el Nombre de Usuario";
                bProcede = false;
                if (!pTieneFoco) txtUsuario.Focus();
            }
            //Validamos el Password
            if (string.IsNullOrEmpty(txtPass.Text))
            {
                strValidacion = Environment.NewLine + "Debe ingresar su Contraseña";
                bProcede = false;
                if (!pTieneFoco) txtPass.Focus();
            }

            return bProcede;
        }

        public static bool AutenticarUsuario(ref string strValidacion, string strUsuaario, string strPass, ref entSegUsuario miUsuario, ref entSegUsuariorestriccion miUsuarioRestriccion, ref entSegRol miRol)
        {
            bool bProcede = false;
            try
            {
                rnSegUsuario rnUsuarioVerificacion = new rnSegUsuario();

                ArrayList arrNombreParametros = new ArrayList();
                arrNombreParametros.Add("UPPER(" + entSegUsuario.Fields.login + ")");
                ArrayList arrParametros = new ArrayList();
                arrParametros.Add("UPPER('" + strUsuaario + "')");

                entSegUsuario eUsuario = rnUsuarioVerificacion.ObtenerObjeto(arrNombreParametros, arrParametros);

                if (eUsuario != null)
                {
                    if (eUsuario.password.ToUpper() == cFuncionesEncriptacion.generarMD5(strPass).ToUpper())
                    {
                        if (1 == 1)
                        {
                            DateTime dtFechaVigente = (eUsuario.fecha_vigente == null
                                                           ? DateTime.Now
                                                           : (DateTime)eUsuario.fecha_vigente);
                            if (cFuncionesFechas.dateDiff(dtFechaVigente, DateTime.Now, "dd") >= 0)
                            {

                                //Obtenemos la lista de grupos o roles                    
                                rnSegUsuariorestriccion rnRestriccion = new rnSegUsuariorestriccion();
                                rnSegRol rnRol = new rnSegRol();

                                List<entSegUsuariorestriccion> listaRoles =
                                    rnRestriccion.ObtenerLista(
                                        entSegUsuariorestriccion.Fields.id_usuario, "'" + eUsuario.id_usuario + "'");

                                //Verificamos que existan roles
                                if (listaRoles.Count > 0)
                                {
                                    
                                    foreach (entSegUsuariorestriccion rolRestriccion in listaRoles)
                                    {
                                        miUsuarioRestriccion = rolRestriccion;
                                        miRol = rnRol.ObtenerObjeto(rolRestriccion.id_rol);
                                    }

                                    //Verificamos que exista un Rol Activo
                                    if (miRol == null)
                                    {
                                        strValidacion = "No existe un Rol Activo para este usuario";
                                        bProcede = false;
                                    }
                                    else
                                    {
                                        //Para la conexion
                                        //cParametros.user = eUsuario.login;
                                        //cParametros.pass = eUsuario.password;

                                        miUsuario = eUsuario;
                                        //Redireccionamos
                                        bProcede = true;
                                    }
                                }
                                else
                                {
                                    strValidacion = "No existen Roles para este Usuario";
                                }
                            }
                            else
                            {
                                strValidacion = "El Usuario ya no está activo.";
                            }
                        }
                        else
                        {
                            strValidacion = "El Usuario ya no está activo.";
                        }
                    }
                    else
                    {
                        strValidacion = "La contraseña ingresada no es correcta.";
                    }
                }
                else
                {
                    strValidacion = "Verifique Nombre de Usuario y Contraseña.";
                }
            }
            catch (Exception exp)
            {
                strValidacion = exp.Message;
                //errores_control1.cargar("Error", UtilErrores.Tipos.critico, exp);;
            }

            return bProcede;
        }
    }
}
