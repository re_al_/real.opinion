﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Text;
using System.IO;
using ReAl.Opinion.Dal.Modelo;

namespace ReAl.Opinion.Web.App_Class
{
    public static class cExportarCsv
    {
        private static string crearCsv(DataTable dt, string strNombre)
        {            
            StringBuilder sb = new StringBuilder();
            IEnumerable<string> columnNames = dt.Columns.Cast<DataColumn>().
                                              Select(column => column.ColumnName);
            //sb.AppendLine(string.Join(",", columnNames));
            foreach (DataRow row in dt.Rows)
            {
                IEnumerable<string> fields = row.ItemArray.Select(field => field.ToString());
                sb.AppendLine(string.Join(",", fields));
            }
            //File.WriteAllText(strNombre, sb.ToString());

            return sb.ToString();
        }

        public static string csvSegProyecto(string strPathFile)
        {
            string strQuery = "WITH a AS(SELECT 1 ord,'id_proyecto|nombre|codigo|descripcion|fecinicio|fecfin|apiestado|usucre|feccre|usumod|fecmod' cont " +
            " UNION " +
            " SELECT 2 ord,id_proyecto || '|' || coalesce(nombre::Text, '') || '|' || coalesce(codigo::Text, '') || '|' || coalesce(descripcion::Text, '') || '|' || extract(epoch FROM fecinicio)::Int || '|' || extract(epoch FROM fecfin)::Int || '|' || apiestado || '|' || usucre || '|' || extract(epoch FROM feccre)::Int || '|' || coalesce(usumod::Text, '') || '|' || coalesce(extract(epoch FROM fecmod)::Int::Text, '') cont " +
            " FROM seg_proyecto) " +
            " SELECT string_agg(cont, chr(13) || chr(10) ORDER BY ord) FROM a";

            rnVista rn = new rnVista();
            DataTable dt = rn.CargarDataTableDesdeQuery(strQuery);
            return crearCsv(dt, strPathFile);
        }

        public static string csvSegUsuario(string strPathFile)
        {
            string strQuery = "WITH a AS(SELECT 1 ord,'id_usuario|login|password|carnet|nombre|paterno|materno|direccion|telefono|fecha_vigente|foto|id_departamento|apiestado|usucre|feccre|usumod|fecmod' cont " +
            " UNION " +
            " SELECT 2 ord,id_usuario || '|' || coalesce(login::Text, '') || '|' || coalesce(password::Text, '') || '|' || coalesce(carnet::Text, '') || '|' || coalesce(nombre::Text, '') || '|' || coalesce(paterno::Text, '') || '|' || coalesce(materno::Text, '') || '|' || coalesce(direccion::Text, '') || '|' || coalesce(telefono::Text, '') || '|' || coalesce(extract(epoch FROM fecha_vigente)::Int::Text, '') || '|' || coalesce(foto::Text, '') || '|' || coalesce(id_departamento::Text, '') || '|' || apiestado || '|' || usucre || '|' || extract(epoch FROM feccre)::Int || '|' || coalesce(usumod::Text, '') || '|' || coalesce(extract(epoch FROM fecmod)::Int::Text, '') cont " +
            " FROM seg_usuario " +
            " WHERE apiestado = 'ELABORADO') " +
            " SELECT string_agg(cont, chr(13) || chr(10) ORDER BY ord) FROM a";

            rnVista rn = new rnVista();
            DataTable dt = rn.CargarDataTableDesdeQuery(strQuery);
            return crearCsv(dt, strPathFile);
        }

        public static string csvSegRol(string strPathFile)
        {
            string strQuery = "WITH a AS(SELECT 1 ord,'id_rol|sigla|descripcion|apiestado|usucre|feccre|usumod|fecmod' cont " +
            " UNION " +
            " SELECT 2 ord,id_rol || '|' || sigla || '|' || descripcion || '|' || apiestado || '|' || usucre || '|' || extract(epoch FROM feccre)::Int || '|' || coalesce(usumod::Text, '') || '|' || coalesce(extract(epoch FROM fecmod)::Int::Text, '') " +
            " FROM seg_rol) " +
            " SELECT string_agg(cont, chr(13) || chr(10) ORDER BY ord) FROM a";

            rnVista rn = new rnVista();
            DataTable dt = rn.CargarDataTableDesdeQuery(strQuery);
            return crearCsv(dt, strPathFile);
        }

        public static string csvSegUsuarioRol(string strPathFile)
        {
            string strQuery = "WITH a AS(SELECT 1 ord,'id_usurestriccion|id_proyecto|id_usuario|id_rol|apiestado|usucre|feccre|usumod|fecmod' cont " +
            " UNION " +
            " SELECT 2 ord,id_usurestriccion || '|' || id_proyecto || '|' || id_usuario || '|' || id_rol || '|' || apiestado || '|' || usucre || '|' || extract(epoch FROM feccre)::Int || '|' || coalesce(usumod::Text, '') || '|' || coalesce(extract(epoch FROM fecmod)::Int::Text, '') cont " +
            " FROM seg_usuariorestriccion " +
            " WHERE apiestado = 'ELABORADO') " +
            " SELECT string_agg(cont, chr(13) || chr(10) ORDER BY ord) FROM a";

            rnVista rn = new rnVista();
            DataTable dt = rn.CargarDataTableDesdeQuery(strQuery);
            return crearCsv(dt, strPathFile);
        }

        public static string csvOpeTablet(string strPathFile)
        {
            string strQuery = "WITH a AS(SELECT 1 ord,'id_tablet|imei|marca|modelo|apiestado|usucre|feccre|usumod|fecmod' cont " +
            " UNION " +
            " SELECT 2 ord,id_tablet || '|' || imei || '|' || marca || '|' || coalesce(modelo::Text, '') || '|' || apiestado || '|' || usucre || '|' || extract(epoch FROM feccre)::Int || '|' || coalesce(usumod::Text, '') || '|' || coalesce(extract(epoch FROM fecmod)::Int::Text, '') cont " +
            " FROM ope_tablet " +
            " WHERE apiestado = 'ELABORADO') " +
            " SELECT string_agg(cont, chr(13) || chr(10) ORDER BY ord) FROM a";

            rnVista rn = new rnVista();
            DataTable dt = rn.CargarDataTableDesdeQuery(strQuery);
            return crearCsv(dt, strPathFile);
        }

        public static string csvOpeBrigada(string strPathFile)
        {
            string strQuery = "WITH a AS(SELECT 1 ord,'id_brigada|id_departamento|codigo_brigada|apiestado|usucre|feccre|usumod|fecmod' cont " +
            " UNION " +
            " SELECT 2 ord,id_brigada || '|' || id_departamento || '|' || codigo_brigada || '|' || apiestado || '|' || usucre || '|' || extract(epoch FROM feccre)::Int || '|' || coalesce(usumod::Text, '') || '|' || coalesce(extract(epoch FROM fecmod)::Int::Text, '') cont " +
            " FROM ope_brigada " +
            " WHERE apiestado = 'ELABORADO') " +
            " SELECT string_agg(cont, chr(13) || chr(10) ORDER BY ord) FROM a";

            rnVista rn = new rnVista();
            DataTable dt = rn.CargarDataTableDesdeQuery(strQuery);
            return crearCsv(dt, strPathFile);
        }

        public static string csvCatUpm(string strPathFile, string strIdProy)
        {
            string strQuery = "WITH a AS(SELECT 1 ord,'id_upm|id_proyecto|id_departamento|codigo|nombre|fecinicio|latitud|longitud|apiestado|usucre|feccre|usumod|fecmod' cont " +
            " UNION " +
            " SELECT 2 ord,id_upm || '|' || id_proyecto || '|' || coalesce(id_departamento::Text, '') || '|' || coalesce(codigo::Text, '') || '|' || coalesce(nombre::Text, '') || '|' || extract(epoch FROM fecinicio)::Int || '|' || latitud || '|' || longitud || '|' || apiestado || '|' || usucre || '|' || extract(epoch FROM feccre)::Int || '|' || coalesce(usumod::Text, '') || '|' || coalesce(extract(epoch FROM fecmod)::Int::Text, '') cont " +
            " FROM cat_upm " +
            " WHERE id_proyecto = "+ strIdProy + ") " +
            " SELECT string_agg(cont, chr(13) || chr(10) ORDER BY ord) FROM a";

            rnVista rn = new rnVista();
            DataTable dt = rn.CargarDataTableDesdeQuery(strQuery);
            return crearCsv(dt, strPathFile);
        }

        public static string csvCatCatalogo(string strPathFile)
        {
            string strQuery = "WITH a AS(SELECT 1 ord,'id_catalogo|catalogo|codigo|descripcion' cont " +
            " UNION " +
            " SELECT 2 ord, id_catalogo || '|' || catalogo || '|' || codigo || '|' || descripcion cont " +
            " FROM cat_catalogo) " +
            " SELECT string_agg(cont, chr(13) || chr(10) ORDER BY ord) FROM a";

            rnVista rn = new rnVista();
            DataTable dt = rn.CargarDataTableDesdeQuery(strQuery);
            return crearCsv(dt, strPathFile);
        }

        public static string csvCatTipoPregunta(string strPathFile)
        {
            string strQuery = "WITH a AS(SELECT 1 ord,'id_tipo_pregunta|tipo_pregunta|descripcion|respuesta_valor|apiestado|usucre|feccre|usumod|fecmod' cont " +
            " UNION " +
            " SELECT 2 ord,id_tipo_pregunta || '|' || coalesce(tipo_pregunta::Text, '') || '|' || coalesce(descripcion::Text, '') || '|' || coalesce(respuesta_valor::Text, '') || '|' || apiestado || '|' || usucre || '|' || extract(epoch FROM feccre)::Int || '|' || coalesce(usumod::Text, '') || '|' || coalesce(extract(epoch FROM fecmod)::Int::Text, '') cont " +
            " FROM cat_tipo_pregunta) " +
            " SELECT string_agg(cont, chr(13) || chr(10) ORDER BY ord) FROM a";

            rnVista rn = new rnVista();
            DataTable dt = rn.CargarDataTableDesdeQuery(strQuery);
            return crearCsv(dt, strPathFile);
        }

        public static string csvOpeAsignacion(string strPathFile, string strIdProy)
        {
            string strQuery = "WITH a AS(SELECT 1 ord,'id_asignacion|id_usuario|id_proyecto|id_brigada|id_tablet|apiestado|usucre|feccre|usumod|fecmod' cont " +
            " UNION " +
            " SELECT 2 ord,id_asignacion || '|' || coalesce(id_usuario::Text, '') || '|' || coalesce(id_proyecto::Text, '') || '|' || coalesce(id_brigada::Text, '') || '|' || coalesce(id_tablet::Text, '') || '|' || apiestado || '|' || usucre || '|' || extract(epoch FROM feccre)::Int || '|' || coalesce(usumod::Text, '') || '|' || coalesce(extract(epoch FROM fecmod)::Int::Text, '') cont " +
            " FROM ope_asignacion " +
            " WHERE id_proyecto = "+ strIdProy +" " +
            " AND apiestado = 'ELABORADO') " +
            " SELECT string_agg(cont, chr(13) || chr(10) ORDER BY ord) FROM a";

            rnVista rn = new rnVista();
            DataTable dt = rn.CargarDataTableDesdeQuery(strQuery);
            return crearCsv(dt, strPathFile);
        }

        public static string csvOpeMovimiento(string strPathFile, string strIdProy)
        {
            string strQuery = "WITH a AS(SELECT 1 ord,'id_movimiento|id_asignacion|id_upm|gestion|mes|apiestado|usucre|feccre|usumod|fecmod' cont " +
            " UNION " +
            " SELECT 2 ord,id_movimiento || '|' || id_asignacion || '|' || coalesce(id_upm::Text, '') || '|' || coalesce(gestion::Text, '') || '|' || coalesce(mes::Text, '') || '|' || apiestado || '|' || usucre || '|' || extract(epoch FROM feccre)::Int || '|' || coalesce(usumod::Text, '') || '|' || coalesce(extract(epoch FROM fecmod)::Int::Text, '') cont " +
            " FROM ope_movimiento " +
            " WHERE exportar = 1 AND id_upm IN(SELECT id_upm FROM cat_upm WHERE id_proyecto = "+ strIdProy +") and apiestado = 'ELABORADO') " +
            " SELECT string_agg(cont, chr(13) || chr(10) ORDER BY ord) FROM a";

            rnVista rn = new rnVista();
            DataTable dt = rn.CargarDataTableDesdeQuery(strQuery);
            return crearCsv(dt, strPathFile);
        }

        public static string csvEncSeccion(string strPathFile, string strIdProy)
        {
            string strQuery = "WITH a AS(SELECT 1 ord,'id_seccion|id_proyecto|id_nivel|codigo|seccion|abierta|apiestado|usucre|feccre|usumod|fecmod' cont " +
            " UNION " +
            " SELECT 2 ord,id_seccion || '|' || id_proyecto || '|' || id_nivel || '|' || coalesce(codigo::Text, '') || '|' || coalesce(seccion::Text, '') || '|' || abierta || '|' || apiestado || '|' || usucre || '|' || extract(epoch FROM feccre)::Int || '|' || coalesce(usumod::Text, '') || '|' || coalesce(extract(epoch FROM fecmod)::Int::Text, '') cont " +
            " FROM enc_seccion " +
            " WHERE id_proyecto = " + strIdProy +") " +
            " SELECT string_agg(cont, chr(13) || chr(10) ORDER BY ord) FROM a";

            rnVista rn = new rnVista();
            DataTable dt = rn.CargarDataTableDesdeQuery(strQuery);
            return crearCsv(dt, strPathFile);
        }

        public static string csvEncPregunta(string strPathFile, string strIdProy)
        {
            string strQuery = "WITH a AS(SELECT 1 ord,'id_pregunta|id_proyecto|id_nivel|id_seccion|codigo_pregunta|pregunta|ayuda|instruccion|id_tipo_pregunta|minimo|maximo|catalogo|longitud|bucle|variable_bucle|codigo_especifique|mostrar_ventana|variable|codigo_especial|formula|rpn_formula|regla|rpn|mensaje|revision|apiestado|usucre|feccre|usumod|fecmod' cont " +
            " UNION " +
            " SELECT 2 ord,id_pregunta || '|' || id_proyecto || '|' || id_nivel || '|' || coalesce(id_seccion::Text, '') || '|' || codigo_pregunta || '|' || pregunta || '|' || ayuda || '|' || instruccion || '|' || id_tipo_pregunta || '|' || minimo || '|' || maximo || '|' || catalogo || '|' || coalesce(longitud::Text, '') || '|' || bucle || '|' || variable_bucle || '|' || codigo_especifique || '|' || mostrar_ventana || '|' || variable || '|' || codigo_especial || '|' || replace(formula, '|', ':') || '|' || replace(rpn_formula, '|', ':') || '|' || replace(regla, '|', ':') || '|' || replace(rpn, '|', ':') || '|' || mensaje || '|' || revision || '|' || apiestado || '|' || usucre || '|' || extract(epoch FROM feccre)::Int || '|' || coalesce(usumod::Text, '') || '|' || coalesce(extract(epoch FROM fecmod)::Int::Text, '') cont " +
            " FROM enc_pregunta " +
            " WHERE id_proyecto = "+ strIdProy +" " +
            " AND apiestado = 'ELABORADO') " +
            " SELECT string_agg(cont, chr(13) || chr(10) ORDER BY ord) FROM a";

            rnVista rn = new rnVista();
            DataTable dt = rn.CargarDataTableDesdeQuery(strQuery);
            return crearCsv(dt, strPathFile);
        }

        public static string csvEncRespuesta(string strPathFile)
        {
            string strQuery = "WITH a AS(SELECT 1 ord,'id_respuesta|id_pregunta|codigo|respuesta|apiestado|usucre|feccre|usumod|fecmod|factor' cont " +
            " UNION " +
            " SELECT 2 ord,id_respuesta || '|' || coalesce(id_pregunta::Text, '') || '|' || codigo || '|' || respuesta || '|' || apiestado || '|' || usucre || '|' || extract(epoch FROM feccre)::BigInt || '|' || coalesce(usumod::Text, '') || '|' || coalesce(extract(epoch FROM fecmod)::Int::Text, '') || '|' || factor cont " +
            " FROM enc_respuesta " +
            " WHERE apiestado = 'ELABORADO') " +
            " SELECT string_agg(cont, chr(13) || chr(10) ORDER BY ord) FROM a";

            rnVista rn = new rnVista();
            DataTable dt = rn.CargarDataTableDesdeQuery(strQuery);
            return crearCsv(dt, strPathFile);
        }

        public static string csvEncFlujo(string strPathFile, string strIdProy)
        {
            string strQuery = "WITH a AS(SELECT 1 ord,'id_flujo|id_proyecto|id_pregunta|id_pregunta_destino|orden|regla|rpn|apiestado|usucre|feccre|usumod|fecmod' cont " +
            " UNION " +
            " SELECT 2 ord,id_flujo || '|' || coalesce(id_proyecto::Text, '') || '|' || id_pregunta || '|' || id_pregunta_destino || '|' || orden || '|' || replace(regla, '|', ':') || '|' || replace(rpn, '|', ':') || '|' || apiestado || '|' || usucre || '|' || extract(epoch FROM feccre)::Int || '|' || coalesce(usumod::Text, '') || '|' || coalesce(extract(epoch FROM fecmod)::Int::Text, '') cont " +
            " FROM enc_flujo " +
            " WHERE id_proyecto = "+ strIdProy +") " +
            " SELECT string_agg(cont, chr(13) || chr(10) ORDER BY ord) FROM a"; 

            rnVista rn = new rnVista();
            DataTable dt = rn.CargarDataTableDesdeQuery(strQuery);
            return crearCsv(dt, strPathFile);
        }
    }
}