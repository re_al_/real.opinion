﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using ReAl.Opinion.Dal.Entidades;
using ReAl.Opinion.Dal.Modelo;

namespace ReAl.Opinion.Web.App_Class
{
    public static class cUsuarioRestriccion
    {
        public static DataTable ObtenerDataTableDepto(string strUsuario)
        {
            rnVista rn = new rnVista();
            ArrayList arrColWhere = new ArrayList();
            arrColWhere.Add("plogin");
            ArrayList arrValWhere = new ArrayList();
            arrValWhere.Add(strUsuario);

            DataTable dt = rn.ObtenerDatosProcAlm("vw_cat_departamento_dropdown", arrColWhere, arrValWhere);
            return dt;
        }

        public static DataTable ObtenerDataTableUsuario(string strUsuario)
        {
            rnSegUsuario rn = new rnSegUsuario();
            DataTable dt = rn.ObtenerDataTable();
            return dt;
        }

        public static DataTable ObtenerDataTableUpm(string strUsuario, int idProyecto, string idDepartamentos)
        {
            rnVista rn = new rnVista();
            ArrayList arrColWhere = new ArrayList();
            arrColWhere.Add(entSegProyecto.Fields.id_proyecto.ToString());
            ArrayList arrValWhere = new ArrayList();
            arrValWhere.Add(idProyecto);

            DataTable dt = rn.ObtenerDatos("vw_cat_upm_dropdown", arrColWhere, arrValWhere);
            DataView dv = dt.DefaultView;
            dv.RowFilter = "id_departamento IN ("+ idDepartamentos +")";

            return dv.ToTable();
        }

        public static DataTable ObtenerDataTableBrigada(string strUsuario)
        {
            rnCatDepartamento rn = new rnCatDepartamento();
            DataTable dt = rn.ObtenerDataTable();
            return dt;
        }
    }
}
