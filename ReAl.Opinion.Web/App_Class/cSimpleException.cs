﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ReAl.Opinion.Web.App_Class
{
    [Serializable]
    public class cSimpleException : Exception
    {
        public cSimpleException()
        : base() { }

        public cSimpleException(string message)
        : base(message) { }

        public cSimpleException(string format, params object[] args)
        : base(string.Format(format, args)) { }

        public cSimpleException(string message, Exception innerException)
        : base(message, innerException) { }

        public cSimpleException(string format, Exception innerException, params object[] args)
        : base(string.Format(format, args), innerException) { }

        protected cSimpleException(SerializationInfo info, StreamingContext context)
        : base(info, context) { }
    }
}