﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using ReAl.Opinion.Dal.Entidades;
using ReAl.Opinion.Dal.Modelo;

namespace ReAl.Opinion.Web.App_Class
{
    public class cSegRolPagina : System.Web.UI.Page
    {
        public DataTable ObtenerMenu()
        {
            rnVista rn = new rnVista();
            cSessionHandler miSesion = new cSessionHandler();

            ArrayList arrColWhere = new ArrayList();
            arrColWhere.Add(entSegRol.Fields.id_rol.ToString());
            ArrayList arrValWhere = new ArrayList();
            arrValWhere.Add(miSesion.appRol.id_rol);

            DataTable dtMenu = rn.ObtenerDatos("vw_seg_menu_lis", arrColWhere, arrValWhere);

            //Creamos el diccionario con las reglas
            ArrayList arrPags = new ArrayList();
            foreach (DataRow dr in dtMenu.Rows)
            {
                arrPags.Add(dr["enlace"].ToString());
            }

            miSesion.dtMenu = dtMenu;
            miSesion.arrMenu = arrPags;
            return dtMenu;

        }
    }
}