﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Globalization;
using System.Linq;
using System.Web;
using ReAl.Opinion.Dal.Entidades;
using ReAl.Opinion.Dal.Modelo;
using ReAl.Opinion.PgConn;

namespace ReAl.Opinion.Web.App_Class
{
    public class cOpeListadoUpload
    {
        public Boolean validarExcel(string strFilePath)
        {
            DataTable dt = exceldata(strFilePath);
            Boolean bProcede = false;
            if (dt == null)
            {
                throw new cSimpleException("El documento que se ha seleccionado no corresponde al formato prediseñado. Por favor descargue la \"Plantilla LVs\" e intente de nuevo.");
            }
            else
            {
                cSessionHandler miSesion = new cSessionHandler();
                rnOpeListado rnLv = new rnOpeListado();
                rnCatUpm rnUpm = new rnCatUpm();
                entOpeListado obj = new entOpeListado();
                entCatUpm objUpm = new entCatUpm();
                //Iteramos el DataTable

                cTrans miTrans = new cTrans();
                try
                {
                    ArrayList arrColWhere = new ArrayList();
                    arrColWhere.Add(entCatUpm.Fields.id_proyecto.ToString());
                    arrColWhere.Add(entCatUpm.Fields.codigo.ToString());

                    ArrayList arrValWhere = new ArrayList();
                    foreach (DataRow row in dt.Rows)
                    {
                        if (!row.Table.Columns.Contains("upm"))
                            throw new cSimpleException("No se ha encontrado la columna \"UPM\".\r\n\r\n¿Está usando la \"Plantilla LVs\"?");

                        obj = new entOpeListado();
                        //Obtenemos el id_upm
                        arrValWhere = new ArrayList();
                        arrValWhere.Add(miSesion.appRestriccion.id_proyecto);
                        arrValWhere.Add("'" + row["upm"].ToString() + "'");
                        objUpm = rnUpm.ObtenerObjeto(arrColWhere, arrValWhere);

                        if (objUpm == null)
                        {
                            //No existe la UPM
                            throw new cSimpleException("La UPM \"" + row["upm"].ToString() + "\" no se encuentra en el Listado de UPMs registradas en el Sistema.\r\n\r\nVerifique que el código de la UPM este bien transcrito.");
                        }
                        else
                        {
                            obj.id_upm = objUpm.id_upm;
                            //Apropiamos los codigos
                            if (row.Table.Columns.Contains(entOpeListado.Fields.codigo_manzana_comunidad.ToString()))
                                obj.codigo_manzana_comunidad = rnLv.GetColumnType(row[entOpeListado.Fields.codigo_manzana_comunidad.ToString()], entOpeListado.Fields.codigo_manzana_comunidad);
                            if (row.Table.Columns.Contains(entOpeListado.Fields.avenida_calle.ToString()))
                                obj.avenida_calle = rnLv.GetColumnType(row[entOpeListado.Fields.avenida_calle.ToString()], entOpeListado.Fields.avenida_calle);
                            if (row.Table.Columns.Contains(entOpeListado.Fields.nro_predio.ToString()))
                                obj.nro_predio = rnLv.GetColumnType(row[entOpeListado.Fields.nro_predio.ToString()], entOpeListado.Fields.nro_predio);
                            if (row.Table.Columns.Contains(entOpeListado.Fields.nro_puerta.ToString()))
                                obj.nro_puerta = rnLv.GetColumnType(row[entOpeListado.Fields.nro_puerta.ToString()], entOpeListado.Fields.nro_puerta);
                            if (row.Table.Columns.Contains(entOpeListado.Fields.nro_orden_vivienda.ToString()))
                                obj.nro_orden_vivienda = rnLv.GetColumnType(row[entOpeListado.Fields.nro_orden_vivienda.ToString()], entOpeListado.Fields.nro_orden_vivienda);
                            if (row.Table.Columns.Contains(entOpeListado.Fields.nro_piso.ToString()))
                                obj.nro_piso = rnLv.GetColumnType(row[entOpeListado.Fields.nro_piso.ToString()], entOpeListado.Fields.nro_piso);
                            if (row.Table.Columns.Contains(entOpeListado.Fields.nro_depto.ToString()))
                                obj.nro_depto = rnLv.GetColumnType(row[entOpeListado.Fields.nro_depto.ToString()], entOpeListado.Fields.nro_depto);
                            if (row.Table.Columns.Contains(entOpeListado.Fields.uso_vivienda.ToString()))
                                obj.uso_vivienda = rnLv.GetColumnType(row[entOpeListado.Fields.uso_vivienda.ToString()], entOpeListado.Fields.uso_vivienda);
                            if (row.Table.Columns.Contains(entOpeListado.Fields.nro_hogares.ToString()))
                                obj.nro_hogares = rnLv.GetColumnType(row[entOpeListado.Fields.nro_hogares.ToString()], entOpeListado.Fields.nro_hogares);
                            if (row.Table.Columns.Contains(entOpeListado.Fields.nro_hombres.ToString()))
                                obj.nro_hombres = rnLv.GetColumnType(row[entOpeListado.Fields.nro_hombres.ToString()], entOpeListado.Fields.nro_hombres);
                            if (row.Table.Columns.Contains(entOpeListado.Fields.nro_mujeres.ToString()))
                                obj.nro_mujeres = rnLv.GetColumnType(row[entOpeListado.Fields.nro_mujeres.ToString()], entOpeListado.Fields.nro_mujeres);
                            if (row.Table.Columns.Contains(entOpeListado.Fields.nombre_jefe.ToString()))
                                obj.nombre_jefe = rnLv.GetColumnType(row[entOpeListado.Fields.nombre_jefe.ToString()], entOpeListado.Fields.nombre_jefe);
                            if (row.Table.Columns.Contains(entOpeListado.Fields.nro_voe.ToString()))
                                obj.nro_voe = rnLv.GetColumnType(row[entOpeListado.Fields.nro_voe.ToString()], entOpeListado.Fields.nro_voe);

                            obj.usucre = miSesion.appUsuario.login;

                            rnLv.Insert(obj, ref miTrans);
                        }
                    }

                    miTrans.ConfirmarTransaccion();
                    bProcede = true;
                }
                catch (Exception)
                {
                    miTrans.AnularTransaccion();
                    throw;
                }
                return bProcede;
            }

        }

        //http://stackoverflow.com/questions/24857876/how-to-import-data-from-excel-sheet-to-data-table-in-c
        private  DataTable exceldata(string filePath)
        {
            try
            {
                DataTable dtexcel = new DataTable();
                bool hasHeaders = false;
                string HDR = hasHeaders ? "Yes" : "No";
                string strConn;
                if (filePath.Substring(filePath.LastIndexOf('.')).ToLower() == ".xlsx")
                    strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=0\"";
                else
                    strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filePath + ";Extended Properties=\"Excel 8.0;HDR=" + HDR + ";IMEX=0\"";
                OleDbConnection conn = new OleDbConnection(strConn);
                conn.Open();
                DataTable schemaTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                //Looping Total Sheet of Xl File
                /*foreach (DataRow schemaRow in schemaTable.Rows)
                {
                }*/
                //Looping a first Sheet of Xl File
                DataRow schemaRow = schemaTable.Rows[0];
                string sheet = schemaRow["TABLE_NAME"].ToString();
                if (!sheet.EndsWith("_"))
                {
                    string query = "SELECT  * FROM [" + sheet + "]";
                    OleDbDataAdapter daexcel = new OleDbDataAdapter(query, conn);
                    dtexcel.Locale = CultureInfo.CurrentCulture;
                    daexcel.Fill(dtexcel);
                }
                conn.Close();
                return dtexcel;
            }
            catch (Exception exp)
            {
                return null;
            }
        }
    }
}