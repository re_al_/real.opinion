﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ReAl.Opinion.Web.App_Class;

namespace ReAl.Opinion.Web
{
    public partial class siceConsolidacion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string summary = "Consolidacion SICE...<br />";
            Boolean bProcede = false;
            try
            {

                string strUsuario = Request["username"];
                string strPassword = Request["password"];

                if (strUsuario == null || strPassword == null)
                    throw new Exception("No existe el Usuario y/o la Contraseña");

                //Verificamos si el usuario existe
                Boolean bUsuarioExiste = true;

                if (bUsuarioExiste && strPassword == "Mw52udZdisjr9fhS")
                {
                    HttpFileCollection uploadFiles = Request.Files;

                    summary += "Archivos subidos: " + uploadFiles.Count + "<br />";

                    //GUardamos el archivo recibido
                    HttpPostedFile postedFile = uploadFiles[0];

                    System.IO.Stream inStream = postedFile.InputStream;
                    byte[] fileData = new byte[postedFile.ContentLength];
                    inStream.Read(fileData, 0, postedFile.ContentLength);

                    //Verify Directory exists
                    if (!System.IO.Directory.Exists(Server.MapPath("~/uploads/" + strUsuario + "/")))
                        System.IO.Directory.CreateDirectory(Server.MapPath("~/uploads/" + strUsuario + "/"));

                    // Save the posted file in our "data" virtual directory.
                    string filePath = Server.MapPath("~/uploads/" + strUsuario + "/" + postedFile.FileName);
                    postedFile.SaveAs(filePath);

                    summary += "Archivo guardado satisfactoriamente: " + postedFile.FileName + "<br />";
                    //Consolidamos el Archivo ZIP
                    cConsolidacion miCon = new cConsolidacion();
                    bProcede = miCon.ConsolidarZip(Server.MapPath("~/uploads/"), strUsuario, postedFile.FileName);

                    summary += "Resultado de la Consolidacion: " + bProcede + "<br />";
                }
            }
            catch (Exception exp)
            {
                summary += exp.Message + "<br />";
                summary += exp.StackTrace + "<br />";
            }
            finally
            {
                if (bProcede)
                    lblRes.Text = "OK";
                else
                {
                    lblRes.Text = summary;
                }
            }
        }
    }
}