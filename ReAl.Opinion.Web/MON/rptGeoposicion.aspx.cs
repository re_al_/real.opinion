﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClosedXML.Excel;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;

using ReAl.Opinion.Dal.Entidades;
using ReAl.Opinion.Dal.Modelo;
using ReAl.Opinion.Web.App_Class;
using Subgurim.Controles;

namespace ReAl.Opinion.Web.MON
{
    public partial class rptGeoposicion : System.Web.UI.Page
    {
        #region Parametros de llegada

        public rptGeoposicion()
        {
            dsReporte = null;
        }

        public DataSet dsReporte { set; get; }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            //Validamos el salto directo
            cSessionHandler miSesion = new cSessionHandler();
            if (miSesion.arrMenu != null)
                if (!miSesion.arrMenu.Contains(Request.AppRelativeCurrentExecutionFilePath))
                    Server.Transfer("~/Template/Dashboard.aspx");


            if (!Page.IsPostBack)
            {
                //Titulo de Pagina
                this.Title = cParametrosWeb.strNombrePagina;
                CargarDdlPregunta();
            }
            else
            {
                if (dtgList.HeaderRow != null)
                    dtgList.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            if (ddlid_pregunta.Items.Count > 0)
                cargarReporte();
        }

        private void CargarDdlPregunta()
        {
            try
            {
                //Funcion que inicializa los 
                rnVista rn = new rnVista();
                cSessionHandler miSession = new cSessionHandler();
                DataTable dt = rn.ObtenerDatos("vw_enc_pregunta_dropdownlist");
                DataView dv = dt.DefaultView;
                dv.RowFilter = "id_proyecto = " + miSession.appRestriccion.id_proyecto + " AND  id_tipo_pregunta = 8"; //Solo preguntas GPS

                ddlid_pregunta.DataValueField = entEncPregunta.Fields.id_pregunta.ToString();
                ddlid_pregunta.DataTextField = entEncPregunta.Fields.pregunta.ToString();
                ddlid_pregunta.DataSource = dv.ToTable();
                ddlid_pregunta.DataBind();
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }
        }

        private void cargarReporte()
        {
            try
            {
                rnVista rn = new rnVista();
                cSessionHandler miSesion = new cSessionHandler();
                ArrayList arrNomParam = new ArrayList();
                arrNomParam.Add("pid_proyecto");
                arrNomParam.Add("pid_pregunta");
                ArrayList arrParam = new ArrayList();
                arrParam.Add(miSesion.appRestriccion.id_proyecto);
                arrParam.Add( int.Parse(ddlid_pregunta.SelectedValue));

                DataTable dtTemp = rn.ObtenerDatosProcAlm("fn_reporte_geoposicion", arrNomParam, arrParam);
                
                //Ejecutamos el query
                DataTable dtReporte = rn.CargarDataTableDesdeQuery(dtTemp.Rows[0][0].ToString());
                
                ArrayList arrHide = new ArrayList();

                cReportBuilder cRpt = new cReportBuilder();
                this.dsReporte = cRpt.obtenerReporteVista(dtReporte, arrHide);
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }
        }

        private void cargarListado()
        {
            try
            {
                dtgList.DataSource = this.dsReporte.Tables[1];
                dtgList.DataBind();

                if (this.dsReporte.Tables[1].Rows.Count > 0)
                {
                    dtgList.HeaderRow.TableSection = TableRowSection.TableHeader;
                    dtgList.Attributes.Add("data-page-size", "25");

                    for (int i = 0; i < dtgList.Columns.Count; i++)
                        dtgList.HeaderRow.Cells[i].Attributes.Add("data-sortable", "true");
                }
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }
        }

        protected void btnVerReporte_Click(object sender, EventArgs e)
        {
            cargarListado();
        }

        protected void btnExportarExcel_Click(object sender, EventArgs e)
        {
            try
            {
                String strNombreReporte = "Reporte-" + DateTime.Now.ToString("yyyy-MM-dd HH-mm");
                using (XLWorkbook wb = new XLWorkbook())
                {
                    wb.Worksheets.Add(this.dsReporte.Tables[0]);
                    wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    wb.Style.Font.Bold = true;

                    Response.Clear();
                    Response.Buffer = true;
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment;filename= " + strNombreReporte + ".xlsx");

                    using (MemoryStream MyMemoryStream = new MemoryStream())
                    {
                        wb.SaveAs(MyMemoryStream);
                        MyMemoryStream.WriteTo(Response.OutputStream);
                        Response.Flush();
                        Response.End();
                    }
                }
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }

        }

        protected void btnExportarCsv_Click(object sender, EventArgs e)
        {
            try
            {
                String strNombreReporte = "Reporte-" + DateTime.Now.ToString("yyyy-MM-dd HH-mm");

                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=" + strNombreReporte + ".txt");
                Response.Charset = "";
                Response.ContentType = "application/text";

                DataTable dtReporte = this.dsReporte.Tables[0];

                StringBuilder sb = new StringBuilder();
                for (int k = 0; k < dtReporte.Columns.Count; k++)
                {
                    //add separator
                    sb.Append(dtReporte.Columns[k].ColumnName + '|');
                }
                //append new line
                sb.Append("\r\n");
                for (int i = 0; i < dtReporte.Rows.Count; i++)
                {
                    for (int k = 0; k < dtReporte.Columns.Count; k++)
                    {
                        //add separator
                        sb.Append(dtReporte.Rows[i][k].ToString() + '|');
                    }
                    //append new line
                    sb.Append("\r\n");
                }
                Response.Output.Write(sb.ToString());
                Response.Flush();
                Response.End();
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }

        }

        public override void VerifyRenderingInServerForm(Control control)
        {

            /* Verifies that the control is rendered */

        }

        protected void btnExportarPdf_Click(object sender, EventArgs e)
        {
            String strNombreReporte = "Reporte-" + DateTime.Now.ToString("yyyy-MM-dd HH-mm");
            Document pdfDoc = new Document(iTextSharp.text.PageSize.LETTER.Rotate(), 10, 10, 10, 10);

            try
            {
                PdfWriter.GetInstance(pdfDoc, System.Web.HttpContext.Current.Response.OutputStream);
                pdfDoc.Open();
                Paragraph p = new Paragraph();
                p.Alignment = Element.ALIGN_CENTER;
                pdfDoc.Add(p);

                Font font8 = FontFactory.GetFont("ARIAL", 7);
                DataTable dt = this.dsReporte.Tables[0];
                if (dt != null)
                {
                    //Craete instance of the pdf table and set the number of column in that table  
                    PdfPTable PdfTable = new PdfPTable(dt.Columns.Count);
                    PdfPCell PdfPCell = null;

                    //Primero la cabecera
                    for (int k = 0; k < dt.Columns.Count; k++)
                    {
                        //add separator
                        PdfPCell = new PdfPCell(new Phrase(new Chunk(dt.Columns[k].ColumnName, font8)));
                        PdfTable.AddCell(PdfPCell);
                    }

                    //Ahora los datos
                    for (int rows = 0; rows < dt.Rows.Count; rows++)
                    {
                        for (int column = 0; column < dt.Columns.Count; column++)
                        {
                            PdfPCell = new PdfPCell(new Phrase(new Chunk(dt.Rows[rows][column].ToString(), font8)));
                            PdfTable.AddCell(PdfPCell);
                        }
                    }
                    //PdfTable.SpacingBefore = 15f; // Give some space after the text or it may overlap the table            
                    pdfDoc.Add(PdfTable); // add pdf table to the document   
                }
                pdfDoc.Close();
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment; filename=" + strNombreReporte + ".pdf");
                HttpContext.Current.Response.Write(pdfDoc);
                Response.Flush();
                Response.End();
                //HttpContext.Current.ApplicationInstance.CompleteRequest();  
            }
            catch (DocumentException de)
            {
                ((Main)this.Master).mostrarPopUp(this, de); ;
            }
            catch (IOException ioEx)
            {
                ((Main)this.Master).mostrarPopUp(this, ioEx); ;
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }
        }

        protected void dtgListado_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            String strRedireccion = "";
            try
            {
                if (e.CommandName.Equals("mapa"))
                {
                    string strId = e.CommandArgument.ToString();

                    //Filtramos el Dataset
                    DataView dv = this.dsReporte.Tables[0].DefaultView;
                    dv.RowFilter = "id = " + strId;

                    DataTable detailTable = dv.ToTable();

                    double latitud = double.Parse(detailTable.Rows[0]["latitud"].ToString().Replace(".",","));
                    double longitud = double.Parse(detailTable.Rows[0]["longitud"].ToString().Replace(".", ","));

                    GMap1.enableDragging = true;
                    GMap1.enableDoubleClickZoom = true;
                    GMap1.enableServerEvents = true;
                    GMap1.Language = "es";
                    GMap1.BackColor = System.Drawing.Color.White;
                    GLatLng latlng = new GLatLng(latitud, longitud);

                    GMap1.setCenter(latlng, 10);

                    //Para los controles de navegacion
                    GMap1.Add(new GControl(GControl.preBuilt.LargeMapControl));

                    //Para los markers
                    GMarker marker = new GMarker(latlng);
                    GInfoWindow window1 = new GInfoWindow(marker, "<center><b>" + detailTable.Rows[0][1].ToString() + "</b></center>", false);
                    GMap1.Add(window1);
                    //mManager.Add(window1,2);
                    

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#currentdetail').appendTo('body').modal('show');", true);
                }
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }

            if (!strRedireccion.IsNullOrEmpty())
                Response.Redirect(strRedireccion);
        }
    }
}