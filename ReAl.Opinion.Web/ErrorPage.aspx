﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ErrorPage.aspx.cs" Inherits="ReAl.Opinion.Web.ErrorPage" %>

<%@ Import Namespace="System.Web.Optimization" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="R. Alonzo Vera Arias">

    <title>SB Admin 2 - Bootstrap Admin Theme</title>

    <%-- Style Section --%>
    <%: Styles.Render("~/bundles/BootstrapCss")  %>
    
    <!-- Custom Fonts -->
    <link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">        
</head>
<body>
    <!-- Login Screen -->
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <form id="frmLogin" runat="server" enctype="multipart/form-data" method="post" role="form">
                    <div class="login-panel panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <asp:Label ID="lblTitulo" runat="server" Text="Error"></asp:Label>
                            </h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group" align="center">
                                <img src="images/logo-1-peq.png" class="img-responsive" alt="Responsive image">
                            </div>
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-danger">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Descripcion del Error</a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <asp:Label ID="FriendlyErrorMsg" runat="server" Font-Size="Small" /><br />
                                        </div>
                                    </div>
                                </div>
                                <asp:Panel ID="pnlDet2" runat="server" Visible="False" class="panel panel-danger">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Detalle del Error</a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <asp:Label ID="ErrorDetailedMsg" runat="server" Font-Size="Small" /><br />
                                        </div>
                                    </div>
                                </asp:Panel>
                                <asp:Panel ID="pnlDet3" runat="server" Visible="False" class="panel panel-danger">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Error Handler</a>
                                        </h4>
                                    </div>
                                    <div id="collapseThree" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <asp:Label ID="ErrorHandler" runat="server" Font-Size="Small" /><br />
                                        </div>
                                    </div>
                                </asp:Panel>
                                <asp:Panel ID="pnlDet4" runat="server" Visible="False" class="panel panel-danger">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">Mensaje</a>
                                        </h4>
                                    </div>
                                    <div id="collapseFour" class="panel-collapse collapse">
                                        <div class="panel-body">                                            
                                            <p>
                                                <asp:Label ID="InnerMessage" runat="server" Font-Size="Small" /><br />
                                            </p>
                                            <p>
                                                <asp:Label ID="InnerTrace" runat="server"  />
                                            </p>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>                            
                            <a class="btn btn-lg btn-success btn-block" href="<%= ResolveClientUrl("~/Template/Dashboard.aspx") %>"><i class="fa fa-home"></i> Ir al Dashboard</a>                    
                        </div>                    
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End Login Screen -->
  </body>
    
    <!-- jQuery -->
    <script src="bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="bower_components/dist/js/sb-admin-2.js"></script>
</html>
