﻿using System;
using System.Collections;
using System.Web;
using System.Data;
using System.Web.Security;

using ReAl.Opinion.Dal.Entidades;
using ReAl.Opinion.Dal.Modelo;
using ReAl.Opinion.Web.App_Class;

namespace ReAl.Opinion.Web
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //Titulo de Pagina
                this.Title = cParametrosWeb.strPantallaInicio;
                this.lblTitulo.Text = cParametrosWeb.strPantallaInicio;
                txtUsuario.Focus();
            }
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            string returnUrl = "";
            bool bProcede = false;
            string strTextoValidacion = "";

            entSegUsuario miUsuario = new entSegUsuario();
            entSegUsuariorestriccion miUsuarioRestriccion = new entSegUsuariorestriccion();
            entSegRol miRol = new entSegRol();
            if (cblLogin.WebValidarUsuario(ref strTextoValidacion, ref txtUsuario, ref txtPass) && cblLogin.AutenticarUsuario(ref strTextoValidacion, txtUsuario.Text, txtPass.Text, ref miUsuario, ref miUsuarioRestriccion, ref miRol))
            {
                
                cSessionHandler miSesion = new cSessionHandler();
                miSesion.appUsuario = miUsuario;
                miSesion.appRestriccion = miUsuarioRestriccion;
                miSesion.appRol = miRol;
                miSesion.appUploadPath = Server.MapPath("~/uploads/");

                int minutosSession = 1200;
                string sKey = txtUsuario.Text;
                string roles = miRol.sigla;

                HttpContext.Current.Cache.Insert(sKey, sKey);

                // Se crea el ticket de autenticación
                FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(2,             //Version del Ticket
                                                        txtUsuario.Text,         //ID de usuario asociado al ticket
                                                        DateTime.Now,                           //fecha de creacion del ticket
                                                        DateTime.Now.AddMinutes(minutosSession),            //expiracion de la cookie
                                                        false,                   // Si el usuario cliquó en "Recuérdame" la cookie no expira.
                                                        roles,                                  // Almacena datos del usuario, en este caso los roles
                                                        FormsAuthentication.FormsCookiePath);   // El path de la cookie especificado en el Web.Config

                // Se encripta el ticket para añadir más seguridad
                string cticket = FormsAuthentication.Encrypt(ticket);

                //Creamos una cookie con el ticket encriptado
                HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, cticket);

                //Adicionamos la cookie creada al cliente
                Response.Cookies.Add(cookie);

                //Culture es-BO
                Session.LCID = 16394;

                returnUrl = Request.QueryString[ "ReturnUrl" ];

                if (returnUrl == null)
                    returnUrl = "~/Template/Dashboard.aspx";

                if (returnUrl.Contains("Login"))
                    returnUrl = "~/Template/Dashboard.aspx";

                if (returnUrl.Equals("/"))
                    returnUrl = "~/Template/Dashboard.aspx";

                //Cargamos el Menu
                cSegRolPagina menu = new cSegRolPagina();
                DataTable dtMenu = menu.ObtenerMenu();
                
                bProcede = true;
            }
            else
            {
                pnlError.Visible = true;
                lblError.Text = strTextoValidacion;
            }

            if (bProcede)
            {
                if (returnUrl == null) returnUrl = "~/Template/Dashboard.aspx";
                Response.Redirect(returnUrl);
            }
                
        }

        
    }
}