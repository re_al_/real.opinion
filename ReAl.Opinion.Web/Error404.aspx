﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Error404.aspx.cs" Inherits="ReAl.Opinion.Web.Error404" %>

<%@ Import Namespace="System.Web.Optimization" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="R. Alonzo Vera Arias">

    <title>SB Admin 2 - Bootstrap Admin Theme</title>

    <%-- Style Section --%>
    <%: Styles.Render("~/bundles/BootstrapCss")  %>
    
    <!-- Custom Fonts -->
    <link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">        
</head>
<body>
    <!-- Login Screen -->
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-danger">
                    <div class="panel-heading">
                        <h3 class="panel-title">Error 404</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group" align="center">
                            <img src="images/logo-1-peq.png" class="img-responsive" alt="Responsive image">
                        </div>
                        <h2>
                            Oops!!<br/>Parece que te has perdido
                        </h2>
                          <a class="btn btn-lg btn-success btn-block" href="<%= ResolveClientUrl("~/Template/Dashboard.aspx") %>"><i class="fa fa-home"></i> Ir al Dashboard</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Login Screen -->
  </body>
    
    <!-- jQuery -->
    <script src="bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="bower_components/dist/js/sb-admin-2.js"></script>
</html>
