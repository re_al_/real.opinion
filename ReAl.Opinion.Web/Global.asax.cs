﻿#region librerias

using System;
using System.Globalization;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Optimization;
using System.Web.Security;

#endregion

namespace ReAl.Opinion.Web
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            WebApiConfig.Register(GlobalConfiguration.Configuration);
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated)
            {
                System.Web.Security.FormsAuthentication.SignOut();
                System.Web.Security.FormsAuthentication.RedirectToLoginPage();
            }
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            try
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo(Request.UserLanguages[0], false);
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture
                    (Thread.CurrentThread.CurrentCulture.Name);
            }
            catch (Exception)
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("es-ES");
            }
            finally
            {
                Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
            }
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            // Si se envía alguna información acerca del usuario
            if (HttpContext.Current.User != null)
            {
                // se comprueba que el usuario esté autenticado
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    // Se comprueba si el usuario está autenticado por formulario
                    if (HttpContext.Current.User.Identity is FormsIdentity)
                    {
                        // Se recupera el ticket del usuario
                        FormsAuthenticationTicket tkt;
                        tkt = FormsAuthentication.Decrypt(Request.Cookies[FormsAuthentication.FormsCookieName].Value);

                        // Se recupera la información acerca del usuario, donde metimos la información de los roles
                        string[] roles = tkt.UserData.Split(';');


                        // Se recupera la identidad del usuario para recuperar sus roles
                        FormsIdentity id = (FormsIdentity)HttpContext.Current.User.Identity;

                        // Se crea un usuario con dichos roles
                        HttpContext.Current.User = new GenericPrincipal(id, roles);
                    }
                }
            }
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs.
            if (1 == 2)
            {
                // Get last error from the server
                Exception exc = Server.GetLastError();

                if (exc is HttpUnhandledException)
                {
                    if (exc.InnerException != null)
                    {
                        exc = new Exception(exc.InnerException.Message);
                        Server.Transfer("ErrorPage.aspx?handler=Application_Error%20-%20Global.asax", true);
                    }
                }
            }
        }

        protected void Session_End(object sender, EventArgs e)
        {
        }

        protected void Application_End(object sender, EventArgs e)
        {
        }
    }
}