﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="D3-js.aspx.cs" Inherits="ReAl.Opinion.Web.Template.D3_js" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="server">
    <link href="<%= ResolveClientUrl("~/bower_components/font-awesome/css/font-awesome.min.css") %>" rel="stylesheet">
    <style>
        .liquidFillGaugeText { font-family: Helvetica; font-weight: bold; }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentSection" runat="server">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Morris.js Charts</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    D3 Liquid Fill Gauge
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <svg id="fillgauge1" width="97%" height="250" onclick="gauge1.update(NewValue());"></svg>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-6 -->
        <div class="col-lg-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    D3 Liquid Fill Gauge
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <svg id="fillgauge3" width="97%" height="250" onclick="gauge3.update(NewValue());"></svg>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-6 -->
    </div>
    <!-- /.row -->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="server">
    <script src="<%= ResolveClientUrl("~/Content/d3/d3.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Content/d3/liquidFillGauge.js") %>"></script>
    
    <script language="JavaScript">
        var gauge1 = loadLiquidFillGauge("fillgauge1", 55);
        var config1 = liquidFillGaugeDefaultSettings();
        config1.circleColor = "#FF7777";
        config1.textColor = "#FF4444";
        config1.waveTextColor = "#FFAAAA";
        config1.waveColor = "#FFDDDD";
        config1.circleThickness = 0.2;
        config1.textVertPosition = 0.2;
        config1.waveAnimateTime = 1000;


        var config2 = liquidFillGaugeDefaultSettings();
        config2.circleColor = "#FF7777";
        config2.textColor = "#FF4444";
        config2.waveTextColor = "#FFAAAA";
        config2.waveColor = "#FFDDDD";
        config2.circleThickness = 0.1;
        config2.circleFillGap = 0.2;
        config2.textVertPosition = 0.8;
        config2.waveAnimateTime = 2000;
        config2.waveHeight = 0.3;
        config2.waveCount = 1;
        var gauge3 = loadLiquidFillGauge("fillgauge3", 60.1, config2);


        function NewValue() {
            if (Math.random() > .5) {
                return Math.round(Math.random() * 100);
            } else {
                return (Math.random() * 100).toFixed(1);
            }
        }
    </script>
</asp:Content>
