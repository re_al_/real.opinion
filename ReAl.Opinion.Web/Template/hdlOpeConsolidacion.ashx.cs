﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Script.Serialization;
using ReAl.Opinion.Dal.Entidades;
using ReAl.Opinion.Dal.Modelo;
using ReAl.Opinion.Web.App_Class;

namespace ReAl.Opinion.Web.Template
{
    /// <summary>
    /// Descripción breve de hdlOpeConsolidacion
    /// </summary>
    public class hdlOpeConsolidacion : IHttpHandler
    {

        public class GraphData
        {
            public string periodo { get; set; }
            public string cantidad { get; set; }
        }

        public class GraphDataList
        {
            public List<GraphData> ListOfGraphData { get; set; }
        }

        public void ProcessRequest(HttpContext context)
        {
            //Obtenemos el id
            int localid = 0;
            if (context.Request["id"] == null)
                localid = 0;
            else
                localid = int.Parse(context.Request["id"].ToString());

            System.Collections.Specialized.NameValueCollection forms = context.Request.Form;
            
            rnVista rn = new rnVista();
            ArrayList arrColWhere = new ArrayList();
            arrColWhere.Add(entSegProyecto.Fields.id_proyecto.ToString());
            ArrayList arrValWhere = new ArrayList();
            arrValWhere.Add(localid);

            DataTable dtUsuario = rn.ObtenerDatos("vw_ope_consolidacion_dashboard", arrColWhere, arrValWhere);

            GraphDataList graphList = new GraphDataList();
            graphList.ListOfGraphData = new List<GraphData>();

            for (int i = 0; i < dtUsuario.Rows.Count; i++)
            {
                GraphData obj = new GraphData();
                obj.periodo = dtUsuario.Rows[i]["feccre"].ToString();
                obj.cantidad = dtUsuario.Rows[i]["total"].ToString();
                graphList.ListOfGraphData.Add(obj);
            }


            int programID = 1;

            if (!string.IsNullOrEmpty("Consolidaciones"))
            {
                //oper = null which means its first load.
                var jsonSerializer = new JavaScriptSerializer();
                string data = jsonSerializer.Serialize(graphList.ListOfGraphData);
                context.Response.Write(data);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}