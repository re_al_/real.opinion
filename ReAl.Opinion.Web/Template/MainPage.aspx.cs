﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Subgurim.Controles;
using System.Drawing;
using System.Text;

using ReAl.Opinion.Dal.Entidades;
using ReAl.Opinion.Dal.Modelo;
using ReAl.Opinion.Utils;
using Ionic.Zip;
using ReAl.Opinion.Web.App_Class;

namespace ReAl.Opinion.Web.Template
{
    public partial class MainPage : System.Web.UI.Page
    {
        private MarkerManager mManager = new MarkerManager();


        protected void Page_Load(object sender, EventArgs e)
        {
            //Titulo de Pagina
            this.Title = cParametrosWeb.strNombrePagina;

            // GMap1.Version = "2.43";
            MostrarRecorrido();

            ConfigurarLeerMapa();     
            
            
        }

        private void ExportarCsv()
        {
            try
            {
                cFuncionesCsv.DefaultCsvDefinition.FieldSeparator = '|';

                rnRptAgropecuario rn = new rnRptAgropecuario();
                List<entRptAgropecuario> miFlujo = rn.ObtenerLista();
                miFlujo.ToCsv("/carga/flujo.csv");


                Response.Clear();
                Response.BufferOutput = false; // for large files...
                string archiveName = "carga";
                Response.ContentType = "application/zip";
                Response.AddHeader("content-disposition", "filename=" + archiveName);

                using (ZipFile zip = new ZipFile())
                {
                    // filesToInclude is an IEnumerable<String>, like String[] or List<String>
                    zip.AddFile("/carga/flujo.csv","");

                    // Add a file from a string                
                    zip.Save(Response.OutputStream);
                }
                // Response.End();  // no! See http://stackoverflow.com/questions/1087777
                Response.Close();
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }
        }

        private void ConfigurarLeerMapa()
        {
            //KB: http://codedbot.com/questions/2952270/subgurim-component-clearoverlays-in-google-maps-v3-how-to-delete-markers
            StringBuilder sb = new StringBuilder();

            sb.Append("var markersArray=[];");
            sb.Append("function clearOverlays() {");
            sb.Append("   for (var i = 0; i < markersArray.length; i++ ) {");
            sb.Append("     markersArray[i].setMap(null);");
            sb.Append("   }");
            sb.Append("   markersArray = [];");
            sb.Append("}");

            GMap2.Add(sb.ToString());
        }


        private void MostrarRecorrido()
        {
            //Google API for javascript 
            //KB: https://console.developers.google.com/project/ine-sice-ipp/apiui/credential
            //KB: http://googlemaps.subgurim.net/

            try
            {
                GMap1.enableDragging = true;
                GMap1.enableDoubleClickZoom = true;
                GMap1.enableServerEvents = true;
                GMap1.Language = "es";
                GMap1.BackColor = Color.White;
                GLatLng latlng = new GLatLng(46, 21);

                GMap1.setCenter(latlng, 3);

                //Para los controles de navegacion
                GMap1.Add(new GControl(GControl.preBuilt.LargeMapControl));

                //Para los marcadores
                MarkerManager mManager = new MarkerManager();

                //Para los markers
                GMarker marker = new GMarker(latlng);
                GInfoWindow window1 = new GInfoWindow(marker, "<center><b>GoogleMaps.Subgurim.NET</b></center>", false);
                GMap1.Add(window1);
                //mManager.Add(window1,2);

                //Para las lineas
                List<GLatLng> puntos3 = new List<GLatLng>();
                puntos3.Add(latlng + new GLatLng(5, -20));
                puntos3.Add(latlng + new GLatLng(5, 20));
                GPolyline linea3 = new GPolyline(puntos3, Color.DarkViolet, 4);
                GInfoWindow window2 = new GInfoWindow();
                window2.sourceEvent = GListener.Event.click;
                window2.gPolyline = linea3;
                window2.html = "<center>Recorrido</center>";
                GMap1.Add(window2);
                //mManager.Add(window2, 2);
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }
            

        }

        protected string GMap2_Click(object s, GAjaxServerEventArgs e)
        {
            GMarker marker = new GMarker(e.point);
            string strInfoWindow = string.Format(
                                             @"point<br />lat = {0}<br/>lng = {1}",
                                             e.point.lat,
                                             e.point.lng);
            GInfoWindow window = new GInfoWindow(marker,
                                                 strInfoWindow,
                                                 true);
            return
                   "clearOverlays();" +
                   window.ToString(e.map) +
                   "markersArray.push(" + GMap2.getGMapElementById(marker.ID) + ");";
        }
    }
}