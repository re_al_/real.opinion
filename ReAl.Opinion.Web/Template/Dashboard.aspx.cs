﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Subgurim.Controles;
using System.Drawing;
using System.Text;
using DocumentFormat.OpenXml.Drawing.Charts;

using ReAl.Opinion.Dal.Entidades;
using ReAl.Opinion.Dal.Modelo;
using ReAl.Opinion.Utils;
using ReAl.Opinion.Web.App_Class;
using Ionic.Zip;
using ReAl.Opinion.Web.App_Class;

namespace ReAl.Opinion.Web.Template
{
    public partial class Dashboard : System.Web.UI.Page
    {
        #region Parametros de llegada
        public string localmsg
        {
            get
            {
                if (Request["msg"] == null)
                {
                    return "";
                }
                return Request["msg"];
            }
        }

        public string localerror
        {
            get
            {
                if (Request["error"] == null)
                {
                    return "";
                }
                return Request["error"];
            }
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Titulo de Pagina
                this.Title = cParametrosWeb.strNombrePagina;
                cSessionHandler miSesion = new cSessionHandler();

                //Contamos EncInformante y EncEncuesta
                rnVista rn = new rnVista();
                ArrayList arrColWhere = new ArrayList();
                arrColWhere.Add(entSegProyecto.Fields.id_proyecto.ToString());
                ArrayList arrValWhere = new ArrayList();
                arrValWhere.Add(miSesion.appRestriccion.id_proyecto);
                System.Data.DataTable dt = rn.ObtenerDatos("vw_dashboard", arrColWhere, arrValWhere);

                lblEncuesta.Text = "0";
                lblInformante.Text = "0";
                foreach (System.Data.DataRow row in dt.Rows)
                {
                    if (row["tipo"].ToString() == "enc_encuesta")
                        lblEncuesta.Text = row["total"].ToString();

                    if (row["tipo"].ToString() == "enc_informante")
                        lblInformante.Text = row["total"].ToString();
                }


                //Los mensajes de llegada
                if (!localmsg.IsNullOrEmpty())
                {
                    pnlMsg.Visible = true;
                    lblmsg.Text = localmsg;
                }
                if (!localerror.IsNullOrEmpty())
                {
                    pnlError.Visible = true;
                    lblError.Text = localerror;
                }
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp);
            }
            

            
            
        }

        private void ExportarCsv()
        {
            try
            {
                cFuncionesCsv.DefaultCsvDefinition.FieldSeparator = '|';

                rnRptAgropecuario rn = new rnRptAgropecuario();
                List<entRptAgropecuario> miFlujo = rn.ObtenerLista();
                miFlujo.ToCsv("/carga/flujo.csv");
                
                Response.Clear();
                Response.BufferOutput = false; // for large files...
                string archiveName = "carga";
                Response.ContentType = "application/zip";
                Response.AddHeader("content-disposition", "filename=" + archiveName);

                using (ZipFile zip = new ZipFile())
                {
                    // filesToInclude is an IEnumerable<String>, like String[] or List<String>
                    zip.AddFile("/carga/flujo.csv","");

                    // Add a file from a string                
                    zip.Save(Response.OutputStream);
                }
                // Response.End();  // no! See http://stackoverflow.com/questions/1087777
                Response.Close();
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }
        }

        protected void lnkAndroidApk_Click(object sender, EventArgs e)
        {
            //Response.ContentType = "image/jpeg";
            Response.AppendHeader("Content-Disposition", "attachment; filename=ReAl.Opinion.apk");
            Response.TransmitFile(Server.MapPath("~/apk/ReAl.Opinion.apk"));
            Response.End();
        }
    }
}