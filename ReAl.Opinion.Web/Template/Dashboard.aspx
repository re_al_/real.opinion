﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="ReAl.Opinion.Web.Template.Dashboard"  %>
<%@ Import Namespace="ReAl.Opinion.Web.App_Class" %>
<%@ register assembly="GMaps" namespace="Subgurim.Controles" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="server">
    <link href="<%= ResolveClientUrl("~/bower_components/font-awesome/css/font-awesome.min.css") %>" rel="stylesheet">
    <link href="<%= ResolveClientUrl("~/bower_components/morrisjs/morris.css") %>" rel="stylesheet">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentSection" runat="server">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Dashboard</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <asp:Panel ID="pnlMsg" runat="server" Visible="False"
                class="alert alert-success alert-dismissable" 
                CssClass="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <asp:Label ID="lblmsg" runat="server" Text="Label"></asp:Label>
            </asp:Panel>
            <asp:Panel ID="pnlError" runat="server" Visible="False"
                class="alert alert-danger alert-dismissable" 
                CssClass="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <asp:Label ID="lblError" runat="server" Text="Label"></asp:Label>
            </asp:Panel>
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-android fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">v.1.3.2</div>
                            <div>Nueva Aplicacion Android!</div>
                        </div>
                    </div>
                </div>
                <a href="<%= ResolveClientUrl("~/apk/EncuestaPercepcion.apk") %>">
                    <div class="panel-footer">
                        <span class="pull-left">Descargar</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>

        <div class="col-lg-4 col-md-6">
            <div class="panel panel-red">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-tasks fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">
                                <asp:Label ID="lblInformante" runat="server" Text="Label"></asp:Label>
                            </div>
                            <div>Informantes registrados!</div>
                        </div>
                    </div>
                </div>
                <a href="#">
                    <div class="panel-footer">
                        <span class="pull-left">Ver Detalles</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>

        <div class="col-lg-4 col-md-6">
            <div class="panel panel-yellow">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-shopping-cart fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">
                                <asp:Label ID="lblEncuesta" runat="server" Text="Label"></asp:Label>
                            </div>
                            <div>Preguntas respondidas</div>
                        </div>
                    </div>
                </div>
                <a href="#">
                    <div class="panel-footer">
                        <span class="pull-left">Ver Detalles</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>

        
    </div>
    <!-- /.row -->
    
    
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Proceso de Consolidaci&oacute;n
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div id="morris-area-bar-consolidacion"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>     
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="server">
    <!-- Flot Charts JavaScript -->
    <script src="<%= ResolveClientUrl("~/bower_components/raphael/raphael-min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/bower_components/morrisjs/morris.min.js") %>"></script>    

    <script>
        $(document).ready(function() {
            $("#morris-area-bar-consolidacion").empty();
            Morris.Line({
                element: 'morris-area-bar-consolidacion',
                data: GraphConsolidacion(),
                xkey: 'periodo',
                ykeys: ['cantidad'],
                labels: ['Consolidaciones'],
                hideHover: 'auto',
                parseTime: false,
                resize: true
            });
        });


        function GraphConsolidacion() {
            var data = "";

            $.ajax({
                type: 'GET',
                url: "<%= ResolveClientUrl("~/Template/hdlOpeConsolidacion.ashx") %>",
                data: { 'id': '<%= (((new cSessionHandler()).appRestriccion == null) ? 1 : (new cSessionHandler()).appRestriccion.id_proyecto) %>' },
                dataType: 'json',
                async: false,
                contentType: "application/json; charset=utf-8",
                success: function (result) {
                    data = result;
                },
                error: function (xhr, status, error) {
                    alert(error);
                }
            });

            return data;
        }
    </script>
</asp:Content>
