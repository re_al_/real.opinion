#region 
/***********************************************************************************************************
	NOMBRE:       lstSegUsuario
	DESCRIPCION:
		Clase que define los metodos y propiedades de la página docSegUsuario

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        17/06/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion

#region

using System;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReAl.Opinion.Dal;
using ReAl.Opinion.Dal.Modelo;
using ReAl.Opinion.Dal.Entidades;
using ReAl.Opinion.PgConn;
using ReAl.Opinion.Utils;
using ReAl.Opinion.Web.App_Class;
using ReAl.Opinion.Dal.Entidades;
using ReAl.Opinion.Dal.Modelo;
using ReAl.Opinion.PgConn;
using ReAl.Opinion.Utils;
using ReAl.Opinion.Web.App_Class;

#endregion

namespace ReAl.Opinion.Web.SEG
{
    public partial class docSegUsuario : Page
    {
        #region Parametros de llegada
        public int localid
        {
            get
            {
                if (Request["id"] == null)
                {
                    return 0;
                }
                return int.Parse(Request["id"].ToString());
            }
        }
        #endregion

        protected string _miFoto = "http://lorempixel.com/200/200/people/9/";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //Titulo de Pagina
                this.Title = cParametrosWeb.strNombrePagina;
                CargarDdlDepartamento();
                CargarDdlRol();

                //Ponemos valores por defecto
                txtfecha_vigente.Text = "31/12/" + DateTime.Now.Year.ToString();
                txtfec_nacimiento.Text = DateTime.Now.ToString(cParametros.parFormatoFecha);

                //Colocamos los mensajes de Error
                if (localid > 0)
                    llenarControles();
                else
                {
                    btnSubirFoto.Visible = false;
                    updMiFoto.Enabled = false;
                }
            }
            if (localid > 0)
            {
                rnSegUsuario rnModificar = new rnSegUsuario();
                entSegUsuario objPag = rnModificar.ObtenerObjeto(localid);
                if (!objPag.foto.IsNullOrEmpty())
                    _miFoto = ResolveClientUrl("~/images/profiles/" + objPag.foto);
            }
                
        }

        private void CargarDdlRol()
        {
            try
            {
                //Funcion que inicializa los controles
                rnSegRol rn = new rnSegRol();
                DataTable dt = rn.ObtenerDataTable();

                DataView dv = dt.DefaultView;
                if (localid > 0)
                    dv.RowFilter = "id_rol > 0";
                else
                    dv.RowFilter = "id_rol = 9";
                dv.Sort = "id_rol";

                ddlid_rol.DataValueField = entSegRol.Fields.id_rol.ToString();
                ddlid_rol.DataTextField = entSegRol.Fields.descripcion.ToString();
                ddlid_rol.DataSource = dv.ToTable();
                ddlid_rol.DataBind();
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }
        }

        private void CargarDdlDepartamento()
        {
            try
            {
                cSessionHandler miSesion = new cSessionHandler();
                //Funcion que inicializa los controles
                DataTable dt = cUsuarioRestriccion.ObtenerDataTableDepto(miSesion.appUsuario.login);

                ddlid_departamento.DataValueField = entCatDepartamento.Fields.id_departamento.ToString();
                ddlid_departamento.DataTextField = entCatDepartamento.Fields.nombre.ToString();
                ddlid_departamento.DataSource = dt;
                ddlid_departamento.DataBind();
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }
        }

        protected void llenarControles()
        {
            try
            {
                rnSegUsuario rnModificar = new rnSegUsuario();
                entSegUsuario objPag = rnModificar.ObtenerObjeto(localid);

                if (objPag != null)
                {
                    //Asignamos los valores
                    txtlogin.Text = objPag.login;
                    txtcarnet.Text = objPag.carnet.ToString();
                    txtnombre.Text = objPag.nombre;
                    txtpaterno.Text = objPag.paterno;
                    txtmaterno.Text = objPag.materno;
                    txtdireccion.Text = objPag.direccion;
                    txttelefono.Text = objPag.telefono.ToString();
                    txtfecha_vigente.Text = objPag.fecha_vigente.ToString();
                    ddlid_departamento.SelectedValue = objPag.id_departamento.ToString();
                    

                    //Buscamos el Rol
                    rnSegUsuariorestriccion rnSur = new rnSegUsuariorestriccion();

                    ArrayList arrColWhere = new ArrayList();
                    arrColWhere.Add(entSegUsuariorestriccion.Fields.id_usuario.ToString());
                    arrColWhere.Add(entSegUsuariorestriccion.Fields.apiestado.ToString());
                    ArrayList arrValWhere = new ArrayList();
                    arrValWhere.Add(objPag.id_usuario);
                    arrValWhere.Add("'ELABORADO'");

                    entSegUsuariorestriccion objSur = rnSur.ObtenerObjeto(arrColWhere, arrValWhere);
                    ddlid_rol.SelectedValue = objSur.id_rol.ToString();

                    //Restringimos la modificacion
                    txtlogin.ReadOnly = true;
                    txtfecha_vigente.ReadOnly = true;
                    ddlid_departamento.Enabled = false;
                    ddlid_rol.Enabled = false;
                }
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }



        }

        protected void limpiarControles()
        {
            //Funcion que inicializa los controles
            try
            {
                txtlogin.Text = "";
                txtcarnet.Text = "";
                txtnombre.Text = "";
                txtpaterno.Text = "";
                txtmaterno.Text = "";
                txtdireccion.Text = "";
                txttelefono.Text = "";
                txtfecha_vigente.Text = "";
                txtfec_nacimiento.Text = "";
                txtcorreo.Text = "";

                throw new Exception("Se ha limpiado los controles");
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }
        }



        protected void imgAtras_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            limpiarControles();
        }

        protected void btnSubirFoto_Click(object sender, EventArgs e)
        {
            Boolean bProcede = false;

            try
            {
                rnSegUsuario rnModificar = new rnSegUsuario();
                entSegUsuario objPag = new entSegUsuario();
                cSessionHandler miSesion = new cSessionHandler();

                if (localid > 0)
                {
                    objPag = rnModificar.ObtenerObjeto(localid);

                    //Subimos el archivo
                    if (updMiFoto.HasFile)
                    {
                        string[] validFileTypes = { "png", "jpg", "jpeg" };
                        string strExtension = System.IO.Path.GetExtension(updMiFoto.FileName);
                        bool isValidFile = false;
                        for (int i = 0; i < validFileTypes.Length; i++)
                        {
                            if (strExtension == "." + validFileTypes[i])
                            {
                                isValidFile = true;
                                break;
                            }
                        }

                        if (isValidFile)
                        {
                            string strFile = objPag.login + strExtension;
                            string filePath = Server.MapPath("~/images/profiles/" + strFile);
                            updMiFoto.SaveAs(filePath);

                            //Actualizamos el registro
                            objPag.foto = strFile;
                            objPag.usumod = miSesion.appUsuario.login;
                            rnModificar.Update(objPag);

                            bProcede = true;
                        }
                        else
                        {
                            throw new cSimpleException("Solo se permite archivos de tipo " + string.Join(",", validFileTypes));
                        }     
                    }
                }
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp);
            }
            if (bProcede)
                Response.Redirect(Request.RawUrl);
        }

        protected void btnRegistrar_Click(object sender, EventArgs e)
        {
            Boolean bProcede = false;

            try
            {
                Page.Validate("cabecera");
                if (!Page.IsValid) return;

                rnSegUsuario rnModificar = new rnSegUsuario();
                entSegUsuario objPag = new entSegUsuario();
                cSessionHandler miSesion = new cSessionHandler();

                if (localid == 0)
                {
                    //Apropiamos los valores
                    objPag = new entSegUsuario();
                    objPag.login = rnModificar.GetColumnType(txtlogin.Text, entSegUsuario.Fields.login);
                    objPag.password = cFuncionesEncriptacion.generarMD5("12345678").ToLower();
                    objPag.id_departamento = rnModificar.GetColumnType(ddlid_departamento.SelectedValue, entSegUsuario.Fields.id_departamento);
                    objPag.fecha_vigente = rnModificar.GetColumnType(txtfecha_vigente.Text, entSegUsuario.Fields.fecha_vigente);
                    objPag.fec_nacimiento = rnModificar.GetColumnType(txtfec_nacimiento.Text, entSegUsuario.Fields.fec_nacimiento);

                    objPag.carnet = rnModificar.GetColumnType(txtcarnet.Text, entSegUsuario.Fields.carnet);
                    objPag.nombre = rnModificar.GetColumnType(txtnombre.Text, entSegUsuario.Fields.nombre);
                    objPag.paterno = rnModificar.GetColumnType(txtpaterno.Text, entSegUsuario.Fields.paterno);
                    objPag.materno = rnModificar.GetColumnType(txtmaterno.Text, entSegUsuario.Fields.materno);
                    objPag.direccion = rnModificar.GetColumnType(txtdireccion.Text, entSegUsuario.Fields.direccion);
                    objPag.telefono = rnModificar.GetColumnType(txttelefono.Text, entSegUsuario.Fields.telefono);                    
                    objPag.usucre = miSesion.appUsuario.login;

                    rnModificar.Insert(objPag);

                    //Obtenemos el ID insertado
                    entSegUsuario objUsuIns = rnModificar.ObtenerObjeto(entSegUsuario.Fields.login, "'"+ objPag.login + "'");

                    //Insertamos el usuario restriccion
                    rnSegUsuariorestriccion rnSur = new rnSegUsuariorestriccion();
                    entSegUsuariorestriccion objSur = new entSegUsuariorestriccion();
                    objSur.id_proyecto = miSesion.appRestriccion.id_proyecto;
                    objSur.id_rol = rnSur.GetColumnType(ddlid_rol.SelectedValue, entSegUsuariorestriccion.Fields.id_rol);
                    objSur.id_usuario = objUsuIns.id_usuario;
                    objSur.departamentos = ddlid_departamento.SelectedValue;
                    rnSur.Insert(objSur);
                }
                else
                {
                    objPag = rnModificar.ObtenerObjeto(localid);
                    objPag.carnet = rnModificar.GetColumnType(txtcarnet.Text, entSegUsuario.Fields.carnet);
                    objPag.nombre = rnModificar.GetColumnType(txtnombre.Text, entSegUsuario.Fields.nombre);
                    objPag.paterno = rnModificar.GetColumnType(txtpaterno.Text, entSegUsuario.Fields.paterno);
                    objPag.materno = rnModificar.GetColumnType(txtmaterno.Text, entSegUsuario.Fields.materno);
                    objPag.direccion = rnModificar.GetColumnType(txtdireccion.Text, entSegUsuario.Fields.direccion);
                    objPag.telefono = rnModificar.GetColumnType(txttelefono.Text, entSegUsuario.Fields.telefono);
                    objPag.fec_nacimiento = rnModificar.GetColumnType(txtfec_nacimiento.Text, entSegUsuario.Fields.fec_nacimiento);
                    objPag.usumod = miSesion.appUsuario.login;

                    rnModificar.Update(objPag);
                }
                
                limpiarControles();
                bProcede = true;
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp);
            }
            if (bProcede)
                Response.Redirect("~/SEG/lstSegUsuario.aspx");
        }
    }
}

