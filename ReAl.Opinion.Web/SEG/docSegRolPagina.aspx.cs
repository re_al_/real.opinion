﻿#region 
/***********************************************************************************************************
	NOMBRE:       lstEncPregunta
	DESCRIPCION:
		Clase que define los metodos y propiedades de la página docEncPregunta

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        09/07/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion

#region

using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReAl.Opinion.Dal.Entidades;
using ReAl.Opinion.Dal.Modelo;
using ReAl.Opinion.Web.App_Class;
using Microsoft.Ajax.Utilities;


#endregion

namespace ReAl.Opinion.Web.SEG
{
    public partial class docSegRolPagina : Page
    {
		#region Parametros de llegada
        public int localid
        {
            get
            {
                if (Request["id"] == null)
                {
                    return 0;
                }
                return int.Parse(Request["id"].ToString());
            }
        }
        #endregion

		protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //Titulo de Pagina
                this.Title = cParametrosWeb.strNombrePagina;

                //Otorgamos valores a los Labels
                CargarDdlRol();
                CargarDdlPagina();
                //Colocamos los mensajes de Error

                if (localid > 0)
                {
                    llenarControles();
                }	
            }            
        }
        
        
        private void CargarDdlRol()
        {
            try
            {
                //Funcion que inicializa los controles
                rnSegRol rn = new rnSegRol();
                DataTable dt = rn.ObtenerDataTable();

                ddlid_rol.DataValueField = entSegRol.Fields.id_rol.ToString();
                ddlid_rol.DataTextField = entSegRol.Fields.sigla.ToString();
                ddlid_rol.DataSource = dt;
                ddlid_rol.DataBind();
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }
        }

        private void CargarDdlPagina()
        {
            try
            {
                //Funcion que inicializa los controles
                rnVista rn = new rnVista();                

                DataView dv = rn.ObtenerDatos("vw_seg_pagina_lis").DefaultView;
                dv.Sort = entSegPagina.Fields.nombre.ToString();

                DataTable listTable = dv.ToTable();

                ddlid_pagina.DataValueField = entSegPagina.Fields.id_pagina.ToString();
                ddlid_pagina.DataTextField = entSegPagina.Fields.nombre.ToString();
                ddlid_pagina.DataSource = listTable;
                ddlid_pagina.DataBind();
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }
        }        

        protected void llenarControles()
        {
			try
			{
				//Funcion que inicializa los controles
				rnSegRolpagina rnModificar = new rnSegRolpagina();
                entSegRolpagina objPag = rnModificar.ObtenerObjeto(localid);

				if(objPag != null)
				{
                    //Asignamos los valores
                    ddlid_rol.SelectedValue = objPag.id_rol.ToString();
                    ddlid_pagina.SelectedValue = objPag.id_pagina.ToString();                    
                    
                }
			}
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }
        }

        protected void limpiarControles()
        {
            //Funcion que inicializa los controles
            //txtcodigo.Text = "";
        }

        protected void btnAtras_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/SEG/lstSegRolPagina.aspx");
        }
        
        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            limpiarControles();
        }
        
		protected void btnRegistrar_Click(object sender, EventArgs e)
        {
			Boolean bProcede = false;
            
			try
			{
				Page.Validate("cabecera");
                if (!Page.IsValid) return;
			
				rnSegRolpagina rnModificar = new rnSegRolpagina();
                entSegRolpagina objPag = new entSegRolpagina();
                cSessionHandler miSesion = new cSessionHandler();

                if (localid == 0)
				{
					//Apropiamos los valores para INSERTAR
                    objPag = new entSegRolpagina();

                    objPag.id_pagina = rnModificar.GetColumnType(ddlid_pagina.SelectedValue, entSegRolpagina.Fields.id_pagina);
                    objPag.id_rol = rnModificar.GetColumnType(ddlid_rol.SelectedValue, entSegRolpagina.Fields.id_rol);                    
                    objPag.apiestado = "ELABORADO";
				    objPag.usucre = miSesion.appUsuario.login;

					rnModificar.Insert(objPag);

				    
				}
				else
				{
					//Apropiamos los valores para MODIFICAR
					objPag = rnModificar.ObtenerObjeto(localid);
                    objPag.id_pagina = rnModificar.GetColumnType(ddlid_pagina.SelectedValue, entSegRolpagina.Fields.id_pagina);
                    objPag.id_rol = rnModificar.GetColumnType(ddlid_rol.SelectedValue, entSegRolpagina.Fields.id_rol);
                    objPag.apiestado = "ELABORADO";
                    objPag.usumod = miSesion.appUsuario.login; 
                    
                    rnModificar.Update(objPag);
                }				
	            
				limpiarControles();
				bProcede = true;
			}
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this,exp);
            }
			if (bProcede)
                Response.Redirect("~/SEG/lstSegRolPagina.aspx");
        }

        protected void btnRegistrarNuevo_Click(object sender, EventArgs e)
        {
            Boolean bProcede = false;

            try
            {
                Page.Validate("cabecera");
                if (!Page.IsValid) return;

                rnSegRolpagina rnModificar = new rnSegRolpagina();
                entSegRolpagina objPag = new entSegRolpagina();
                cSessionHandler miSesion = new cSessionHandler();

                if (localid == 0)
                {
                    //Apropiamos los valores para INSERTAR
                    objPag = new entSegRolpagina();

                    objPag.id_pagina = rnModificar.GetColumnType(ddlid_pagina.SelectedValue, entSegRolpagina.Fields.id_pagina);
                    objPag.id_rol = rnModificar.GetColumnType(ddlid_rol.SelectedValue, entSegRolpagina.Fields.id_rol);
                    objPag.apiestado = "ELABORADO";
                    objPag.usucre = miSesion.appUsuario.login;

                    rnModificar.Insert(objPag);


                }
                else
                {
                    //Apropiamos los valores para MODIFICAR
                    objPag = rnModificar.ObtenerObjeto(localid);
                    objPag.id_pagina = rnModificar.GetColumnType(ddlid_pagina.SelectedValue, entSegRolpagina.Fields.id_pagina);
                    objPag.id_rol = rnModificar.GetColumnType(ddlid_rol.SelectedValue, entSegRolpagina.Fields.id_rol);
                    objPag.apiestado = "ELABORADO";
                    objPag.usumod = miSesion.appUsuario.login;

                    rnModificar.Update(objPag);
                }

                limpiarControles();
                bProcede = true;
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp);
            }
            if (bProcede)
                limpiarControles();
        }
    }
}
