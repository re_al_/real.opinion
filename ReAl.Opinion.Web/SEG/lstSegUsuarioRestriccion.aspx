<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="lstSegUsuarioRestriccion.aspx.cs" Inherits="ReAl.Opinion.Web.SEG.lstSegUsuarioRestriccion" Title="lstSegUsuario" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="server">   
    <link href="<%= ResolveClientUrl("~/Content/bootstrap-table/bootstrap-table.min.css") %>" rel="stylesheet">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentSection" runat="server">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Cambiar Restriccion de Usuario</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>            
    <div class="row">
        <div class="col-lg-12">
                    
            <div class="panel panel-default">
                <div class="panel-heading">
                    Cambiar Restriccion de Usuario
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <!-- Botones de Accion -->
                                <asp:LinkButton ID="btnAtras" runat="server" Text="<i class='fa fa-arrow-left'></i>" ToolTip="Atras"
                                    CssClass="btn btn-primary btn-circle" class="btn btn-default btn-circle" OnClick="btnAtras_Click"/>
                                <asp:LinkButton ID="btnNuevo" runat="server" Text="<i class='fa fa-file-o'></i>" ToolTip="Nuevo registro"
                                    CssClass="btn btn-primary btn-circle" class="btn btn-default btn-circle" OnClick="btnNuevo_Click"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
								<label id="lblid_proyecto" runat="server">Proyecto:</label>
                                <asp:DropDownList ID="ddlid_proyecto" runat="server" class="form-control" 
                                    CssClass="form-control"></asp:DropDownList>
							</div>
                        </div>
                    </div>
					<div class="dataTable_wrapper">
                        <asp:GridView ID="dtgListado" runat="server" AutoGenerateColumns="False" 
                            data-toggle="table" data-show-columns="true" data-pagination="true" 
                            data-search="true" data-show-toggle="true" data-sortable="true" 
                            data-page-size="25" data-pagination-v-align="both"
                            DataKeyNames="" OnRowCommand="dtgListado_RowCommand"
                            CssClass="table table-striped table-bordered table-hover" >
                            
                            <Columns>
								<asp:BoundField ReadOnly="True" DataField="id" HeaderText="C&#243;digo" ShowHeader="false" >
									<ItemStyle HorizontalAlign="Justify" ></ItemStyle>
									<HeaderStyle HorizontalAlign="Center" ></HeaderStyle>
								</asp:BoundField>
								
                                <asp:BoundField ReadOnly="True" DataField="login" HeaderText="Usuario" ShowHeader="false" >
									<ItemStyle HorizontalAlign="Justify"></ItemStyle>
									<HeaderStyle HorizontalAlign="Center" ></HeaderStyle>
								</asp:BoundField>

                                <asp:BoundField ReadOnly="True" DataField="proyecto" HeaderText="Proyecto" ShowHeader="false" >
									<ItemStyle HorizontalAlign="Justify"></ItemStyle>
									<HeaderStyle HorizontalAlign="Center" ></HeaderStyle>
								</asp:BoundField>

                                <asp:BoundField ReadOnly="True" DataField="rol" HeaderText="Rol" ShowHeader="false" >
									<ItemStyle HorizontalAlign="Justify"></ItemStyle>
									<HeaderStyle HorizontalAlign="Center" ></HeaderStyle>
								</asp:BoundField>

								<asp:BoundField ReadOnly="True" DataField="departamentos" HeaderText="Departamentos" ShowHeader="false" >
									<ItemStyle HorizontalAlign="Justify"></ItemStyle>
									<HeaderStyle HorizontalAlign="Center" ></HeaderStyle>
								</asp:BoundField>
                                
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="datos" runat="server"/>
    
    <!-- Detail Modal -->
    <div class="modal fade" id="currentdetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
                    
            <asp:UpdatePanel ID="updModale" runat="server">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Detalle del registro</h4>
                        </div>
                        <div class="modal-body">
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <asp:DetailsView ID="dtgDetalles" runat="server"
                                            CssClass="table table-striped table-bordered table-hover"
                                            FieldHeaderStyle-Font-Bold="true" AutoGenerateRows="True">
                                        <Fields>

                                        </Fields>
                                    </asp:DetailsView>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="dtgListado" EventName="RowCommand" />
                                </Triggers>
                            </asp:UpdatePanel>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </ContentTemplate>
            </asp:UpdatePanel> 

        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="server">
    <script src="<%= ResolveClientUrl("~/Scripts/bootstrap-table/bootstrap-table.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Scripts/bootstrap-table/locale/bootstrap-table-es-MX.min.js") %>"></script>

    <script>
        // tooltip demo
        $('.tooltip-demo').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });

        // popover demo
        $("[data-toggle=popover]")
            .popover();
    </script>
</asp:Content>

