﻿#region 
/***********************************************************************************************************
	NOMBRE:       lstEncPregunta
	DESCRIPCION:
		Clase que define los metodos y propiedades de la página docEncPregunta

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        09/07/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion

#region

using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReAl.Opinion.Dal.Entidades;
using ReAl.Opinion.Dal.Modelo;
using ReAl.Opinion.Web.App_Class;
using Microsoft.Ajax.Utilities;


#endregion

namespace ReAl.Opinion.Web.SEG
{
    public partial class docSegPagina : Page
    {
		#region Parametros de llegada
        public int localid
        {
            get
            {
                if (Request["id"] == null)
                {
                    return 0;
                }
                return int.Parse(Request["id"].ToString());
            }
        }
        #endregion

		protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //Titulo de Pagina
                this.Title = cParametrosWeb.strNombrePagina;

                //Otorgamos valores a los Labels
                //Colocamos los mensajes de Error
                
                if (localid > 0)
                {
                    llenarControles();
                }	
            }
            
        }        

		protected void llenarControles()
        {
			try
			{
				//Funcion que inicializa los controles
				rnSegPagina rnModificar = new rnSegPagina();
                entSegPagina objPag = rnModificar.ObtenerObjeto(localid);

				if(objPag != null)
				{
					//Asignamos los valores
				    ddlid_pagina_padre.SelectedValue = objPag.id_pagina_padre.ToString();
				    txtenlace.Text = objPag.enlace;
                    txticono.Text = objPag.icono;
                    txtnombre.Text = objPag.nombre;
                    txtorden.Text = objPag.orden.ToString();
                }
			}
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }
        }

        protected void limpiarControles()
        {
            //Funcion que inicializa los controles
            txtenlace.Text = "";
            txticono.Text = "";
            txtnombre.Text = "";
            txtorden.Text = "";
        }

        protected void btnAtras_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/SEG/lstSegPagina.aspx");
        }
        
        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            limpiarControles();
        }
        
		protected void btnRegistrar_Click(object sender, EventArgs e)
        {
			Boolean bProcede = false;
            
			try
			{
				Page.Validate("cabecera");
                if (!Page.IsValid) return;
			
				rnSegPagina rnModificar = new rnSegPagina();
                entSegPagina objPag = new entSegPagina();
                cSessionHandler miSesion = new cSessionHandler();

                if (localid == 0)
				{
					//Apropiamos los valores para INSERTAR
                    objPag = new entSegPagina();

                    objPag.id_pagina_padre = rnModificar.GetColumnType(ddlid_pagina_padre.SelectedValue, entSegPagina.Fields.id_pagina_padre);
                    objPag.enlace = txtenlace.Text;
                    objPag.icono = txticono.Text;
                    objPag.nombre = txtnombre.Text;
				    objPag.orden = int.Parse(txtorden.Text);
                    objPag.apiestado = "ELABORADO";
				    objPag.usucre = miSesion.appUsuario.login;

					rnModificar.Insert(objPag);   
				}
				else
				{
					//Apropiamos los valores para MODIFICAR
					objPag = rnModificar.ObtenerObjeto(localid);
                    objPag.id_pagina_padre = rnModificar.GetColumnType(ddlid_pagina_padre.SelectedValue, entSegPagina.Fields.id_pagina_padre);
                    objPag.enlace = txtenlace.Text;
                    objPag.icono = txticono.Text;
                    objPag.nombre = txtnombre.Text;
                    objPag.orden = int.Parse(txtorden.Text);
                    objPag.usumod = miSesion.appUsuario.login; 
                    
                    rnModificar.Update(objPag);
                }				
	            
				limpiarControles();
				bProcede = true;
			}
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this,exp);
            }
			if (bProcede)
                Response.Redirect("~/SEG/lstSegPagina.aspx");
        }      
    }
}
