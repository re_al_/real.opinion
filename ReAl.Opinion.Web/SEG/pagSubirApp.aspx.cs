﻿#region 
/***********************************************************************************************************
	NOMBRE:       lstEncPregunta
	DESCRIPCION:
		Clase que define los metodos y propiedades de la página docEncPregunta

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        09/07/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion

#region

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClosedXML.Excel;

using ReAl.Opinion.Dal.Entidades;
using ReAl.Opinion.Dal.Modelo;
using ReAl.Opinion.PgConn;
using ReAl.Opinion.Utils;
using ReAl.Opinion.Web.App_Class;
using Microsoft.Ajax.Utilities;
using Npgsql;
using NpgsqlTypes;

#endregion

namespace ReAl.Opinion.Web.SEG
{
    public partial class pagSubirApp : Page
    {
        #region Parametros de llegada

        public int localid
        {
            get
            {
                if (Request["id"] == null)
                {
                    return 0;
                }
                return int.Parse(Request["id"].ToString());
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            //Validamos el salto directo
            cSessionHandler miSesion = new cSessionHandler();
            if (miSesion.arrMenu != null)
                if (!miSesion.arrMenu.Contains(Request.AppRelativeCurrentExecutionFilePath))
                    Server.Transfer("~/Template/Dashboard.aspx");


            if (!Page.IsPostBack)
            {
                //Titulo de Pagina
                this.Title = cParametrosWeb.strNombrePagina;

            }

        }

        protected void btnAtras_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Template/Dashboard.aspx");
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {

        }

        protected void btnRegistrar_Click(object sender, EventArgs e)
        {
            Boolean bProcede = false;

            try
            {
                Page.Validate("cabecera");
                if (!Page.IsValid) return;
                string strMensaje = "";
                //Subimos el archivo
                if (FileUpload1.HasFile)
                {
                    String strNombreArchivo = "ine.sice.ece.apk";
                    string filePath = Server.MapPath("~/apk/" + strNombreArchivo);

                    FileUpload1.SaveAs(filePath);
                    bProcede = true;
                }
            }
            catch (Exception exp)
            {
                ((Main) this.Master).mostrarPopUp(this, exp);
            }
            if (bProcede)
                Response.Redirect("~/Template/Dashboard.aspx?msg=Se ha actualizado el Archivo APK de la Aplicación Móvil");
        }
    }
}
