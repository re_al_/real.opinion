<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="docSegUsuarioPassword.aspx.cs" Inherits="ReAl.Opinion.Web.SEG.docSegUsuarioPassword" Title="docSegUsuarioPassword" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="server">
    <link href="<%= ResolveClientUrl("~/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css") %>" rel="stylesheet">
    <link href="<%= ResolveClientUrl("~/bower_components/datatables-responsive/css/dataTables.responsive.css") %>" rel="stylesheet">
    <link href="<%= ResolveClientUrl("~/Content/bootstrap-fileinput/css/fileinput.min.css") %>" rel="stylesheet">
    <link href="<%= ResolveClientUrl("~/Content/bootstrap-datetimepicker.css") %>" rel="stylesheet">    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentSection" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Cambio de Password</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>            
            <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Cambio de Password
                        </div>
                        <div class="panel-body">
                            <div>
								<asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="cabecera" 
                                    DisplayMode="BulletList" ShowMessageBox="True" 
                                    class="alert alert-danger" CssClass="alert alert-danger"
                                    HeaderText="Debe considerar las siguientes observaciones antes de continuar" />                                
                            </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <!-- Botones de Accion -->
                                            <asp:LinkButton ID="btnAtras" runat="server" Text="<i class='fa fa-arrow-left'></i>" ToolTip="Atras"
                                                CssClass="btn btn-primary btn-circle" class="btn btn-default btn-circle" OnClick="btnAtras_Click"/>
                                                                        
                                            <asp:LinkButton ID="btnGuardar" runat="server" Text="<i class='fa fa-save'></i>" ToolTip="Guardar registro"
                                                CssClass="btn btn-primary btn-circle" class="btn btn-default btn-circle"
                                                OnClick="btnRegistrar_Click" OnClientClick="return confirmarRegistro();"/>
                                        </div>
                                        <div class="form-group">
                                            <label id="lblPassActual" runat="server">Contrase�a actual:</label>
                                            <asp:TextBox id="txtActual" runat="server" TextMode="Password"
                                                Class="form-control" CssClass="form-control" ></asp:TextBox>
                                        </div>
                                        <div class="form-group">
                                            <label id="lblNewPass" runat="server">Contrase�a actual:</label>
                                            <asp:TextBox id="txtNew" runat="server" TextMode="Password"
                                                Class="form-control" CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <div class="form-group">
                                            <label id="lblRepeatPass" runat="server">Contrase�a actual:</label>
                                            <asp:TextBox id="txtRepeat" runat="server" TextMode="Password"
                                                Class="form-control" CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <div class="alert alert-info alert-dismissable" runat="server" id="divError" Visible="False">
                                            <a class="panel-close close" data-dismiss="alert">�</a> 
                                            <i class="fa fa-coffee"></i>
                                            <asp:Label ID="lblError" runat="server" Text="Label"></asp:Label>
                                        </div>
                                    </div>
                                </div>                            
                            </div>
                        </div>
                    </div>
                </div>
            
            <asp:HiddenField ID="hdnIdDatos" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="server">
    <script src="<%= ResolveClientUrl("~/bower_components/datatables/media/js/jquery.dataTables.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Scripts/fileinput.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Scripts/fileinput_locale_es.js") %>"></script>

    <script type="text/javascript" src="<%= ResolveClientUrl("~/Scripts/moment.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("~/Scripts/bootstrap-datetimepicker.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("~/Scripts/bootstrap-datetimepicker.min.js") %>"></script>

    <script>
        var confirmarRegistro = function () {
            BootstrapDialog.confirm({
                title: 'Confirmar accion',
                message: '�Est� seguro que desea guardar la informacion?',
                type: BootstrapDialog.TYPE_PRIMARY, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                draggable: true, // <-- Default value is false
                btnCancelLabel: 'No estoy seguro', // <-- Default value is 'Cancel',
                btnOKLabel: 'De acuerdo', // <-- Default value is 'OK',
                btnOKClass: 'btn-success', // <-- If you didn't specify it, dialog type will be used,
                callback: function (result) {
                    // result will be true if button was click, while it will be false if users close the dialog directly.
                    if (result) {
                        <%=Page.ClientScript.GetPostBackEventReference(btnGuardar, string.Empty)%>;
                    } else {
                        return false;
                    }
                }
            });

        };        
    </script> 
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('#datetimepicker2').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });
    </script>
</asp:Content>