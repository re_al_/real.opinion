<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="docSegUsuario.aspx.cs" Inherits="ReAl.Opinion.Web.SEG.docSegUsuario" Title="docSegUsuario" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="server">
    <link href="<%= ResolveClientUrl("~/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css") %>" rel="stylesheet">
    <link href="<%= ResolveClientUrl("~/bower_components/datatables-responsive/css/dataTables.responsive.css") %>" rel="stylesheet">
    <link href="<%= ResolveClientUrl("~/Content/bootstrap-fileinput/css/fileinput.min.css") %>" rel="stylesheet">
    <link href="<%= ResolveClientUrl("~/Content/bootstrap-datetimepicker.css") %>" rel="stylesheet">    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentSection" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Listado de Usuarios</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>            
            <div class="row">
                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Mi presentaci�n
                        </div>
                        <div class="panel-body">
							<div class="row">
                                <div class="col-lg-12">
                                    <div class="text-center">
                                        <img src="<%= _miFoto %>" class="avatar img-circle img-thumbnail" alt="avatar">
                                        <div class="form-group">
                                            <h6>
                                                <label id="lblfoto" runat="server">foto:</label>
                                            </h6>											    
                                            <asp:FileUpload ID="updMiFoto" runat="server" 
                                                CssClass="file text-center center-block well well-sm" 
                                                Class="file text-center center-block well well-sm"
										        multiple="false"
                                                data-show-upload="false" data-show-caption="true"/>
										</div>                                        
                                        <asp:Button ID="btnSubirFoto" runat="server" CssClass="btn btn-success" 
										    CausesValidation="true" UseSubmitBehavior="False" AccessKey="R"   
										    Text="Cambiar fotograf�a" onclick="btnSubirFoto_Click" />
                                    </div>
                                </div>
                                    
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Datos Personales
                        </div>
                        <div class="panel-body">
                            <div>
								<asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="cabecera" 
                                    DisplayMode="BulletList" ShowMessageBox="True" 
                                    class="alert alert-danger" CssClass="alert alert-danger"
                                    HeaderText="Debe considerar las siguientes observaciones antes de continuar" />                                
                            </div>
                                <div class="row">
                                    <div class="col-lg-12">                                    
                                        <div class="form-group">
								            <label id="Label1" runat="server">Departamento:</label>
                                            <asp:DropDownList ID="ddlid_departamento" runat="server" class="form-control" 
                                                CssClass="form-control" ></asp:DropDownList>
							            </div>
                                        
                                        <div class="form-group">
								            <label id="Label2" runat="server">Rol:</label>
                                            <asp:DropDownList ID="ddlid_rol" runat="server" class="form-control" 
                                                CssClass="form-control" ></asp:DropDownList>
							            </div>

										<div class="form-group">
											<label id="lbllogin" runat="server">Usuario:</label>
											<asp:TextBox id="txtlogin" Class="form-control" CssClass="form-control" runat="server" MaxLength="20" ></asp:TextBox>
										</div>

                                        <div class="form-group">
                                            <label id="lblfecha_vigente" runat="server">Vigente hasta:</label>
                                            <div class='input-group date' id='datetimepicker1'>
                                                <asp:TextBox id="txtfecha_vigente" Class="form-control" CssClass="form-control" runat="server"></asp:TextBox>
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>

										<div class="form-group">
											<label id="lblcarnet" runat="server">Carnet:</label>
											<asp:TextBox id="txtcarnet" Class="form-control" CssClass="form-control" runat="server" MaxLength="9" TextMode="Number" ></asp:TextBox>
										</div>

										<div class="form-group">
											<label id="lblnombre" runat="server">Nombres:</label>
											<asp:TextBox id="txtnombre" Class="form-control" CssClass="form-control" runat="server" MaxLength="40" ></asp:TextBox>
										</div>

										<div class="form-group">
											<label id="lblpaterno" runat="server">Apellido Paterno:</label>
											<asp:TextBox id="txtpaterno" Class="form-control" CssClass="form-control" runat="server" MaxLength="40" ></asp:TextBox>
										</div>

										<div class="form-group">
											<label id="lblmaterno" runat="server">Apellido Materno:</label>
											<asp:TextBox id="txtmaterno" Class="form-control" CssClass="form-control" runat="server" MaxLength="40" ></asp:TextBox>
										</div>

										<div class="form-group">
											<label id="lbldireccion" runat="server">Direccion:</label>
											<asp:TextBox id="txtdireccion" Class="form-control" CssClass="form-control" runat="server" MaxLength="120" ></asp:TextBox>
										</div>

										<div class="form-group">
											<label id="lbltelefono" runat="server">Telefono:</label>
											<asp:TextBox id="txttelefono" Class="form-control" CssClass="form-control" runat="server" MaxLength="10" TextMode="Number" ></asp:TextBox>
										</div>

										<div class="form-group">
                                            <label id="lblfec_nacimiento" runat="server">Fecha Nacimiento:</label>
                                            <div class='input-group date' id='datetimepicker2'>
                                                <asp:TextBox id="txtfec_nacimiento" Class="form-control" CssClass="form-control" runat="server"></asp:TextBox>
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>

										<div class="form-group">
											<label id="lblcorreo" runat="server">Correo:</label>
											<asp:TextBox id="txtcorreo" Class="form-control" CssClass="form-control" runat="server" MaxLength="0" TextMode="Email" ></asp:TextBox>
										</div>

                                        <asp:Button ID="btnGuardar" runat="server" CssClass="btn btn-success" 
										    CausesValidation="true" UseSubmitBehavior="False" AccessKey="R"   
                                            OnClientClick="return confirmarRegistro();"
										    Text="Guardar" onclick="btnRegistrar_Click" />
                                    </div>
                                </div>                            
                            </div>
                        </div>
                    </div>
                </div>
            
            <asp:HiddenField ID="hdnIdDatos" runat="server" />
        </ContentTemplate>		
        <Triggers>
            <asp:PostBackTrigger ControlID="btnGuardar" />
            <asp:PostBackTrigger ControlID="btnSubirFoto" />
            
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="server">
    <script src="<%= ResolveClientUrl("~/bower_components/datatables/media/js/jquery.dataTables.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Scripts/fileinput.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Scripts/fileinput_locale_es.js") %>"></script>

    <script type="text/javascript" src="<%= ResolveClientUrl("~/Scripts/moment.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("~/Scripts/bootstrap-datetimepicker.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("~/Scripts/bootstrap-datetimepicker.min.js") %>"></script>

    <script>
        var confirmarRegistro = function () {
            BootstrapDialog.confirm({
                title: 'Confirmar accion',
                message: '�Est� seguro que desea guardar la informacion?',
                type: BootstrapDialog.TYPE_PRIMARY, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                draggable: true, // <-- Default value is false
                btnCancelLabel: 'No estoy seguro', // <-- Default value is 'Cancel',
                btnOKLabel: 'De acuerdo', // <-- Default value is 'OK',
                btnOKClass: 'btn-success', // <-- If you didn't specify it, dialog type will be used,
                callback: function (result) {
                    // result will be true if button was click, while it will be false if users close the dialog directly.
                    if (result) {
                        <%=Page.ClientScript.GetPostBackEventReference(btnGuardar, string.Empty)%>;
                    } else {
                        return false;
                    }
                }
            });

        };        
    </script> 
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('#datetimepicker2').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });
    </script>
</asp:Content>

