#region 
/***********************************************************************************************************
	NOMBRE:       lstSegUsuario
	DESCRIPCION:
		Clase que define los metodos y propiedades de la página lstSegUsuario

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        17/06/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion

#region

using System;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Ajax.Utilities;
using ReAl.Opinion.Dal;
using ReAl.Opinion.Dal.Modelo;
using ReAl.Opinion.Dal.Entidades;
using ReAl.Opinion.Web.App_Class;

#endregion

namespace ReAl.Opinion.Web.SEG
{
    public partial class lstSegProyecto : Page
    {

        #region Parametros de llegada

        public lstSegProyecto()
        {
            dsReporte = null;
        }

        public DataSet dsReporte { set; get; }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            //Validamos el salto directo
            cSessionHandler miSesion = new cSessionHandler();
            if (miSesion.arrMenu != null)
                if (!miSesion.arrMenu.Contains(Request.AppRelativeCurrentExecutionFilePath))
                    Server.Transfer("~/Template/Dashboard.aspx");


            String strNombreVista = "seg_proyecto";

            ArrayList arrNomParam = new ArrayList();
            arrNomParam.Add(1);
            ArrayList arrParam = new ArrayList();
            arrParam.Add(1);

            ArrayList arrHide = new ArrayList();            

            cReportBuilder cRpt = new cReportBuilder();
            this.dsReporte = cRpt.obtenerReporteVista(strNombreVista, arrNomParam, arrParam, arrHide);

            cargarListado();            
        }

        private void cargarListado()
        {
            try
            {
                DataView dv = this.dsReporte.Tables[1].DefaultView;
                dv.Sort = "codigo";

                DataTable listTable = dv.ToTable();
                dtgListado.DataSource = listTable;
                dtgListado.DataBind();

                if (listTable.Rows.Count > 0)
                {
                    dtgListado.HeaderRow.TableSection = TableRowSection.TableHeader;

                    for (int i = 0; i < dtgListado.Columns.Count; i++)
                        dtgListado.HeaderRow.Cells[i].Attributes.Add("data-sortable", "true");
                }                   
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }
        }

        protected void dtgListado_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                
                Button miBoton1 = (Button)e.Row.Cells[4].FindControl("btn0");
                if (miBoton1 != null)
                    miBoton1.BackColor = System.Drawing.ColorTranslator.FromHtml(miBoton1.Text);

                Button miBoton2 = (Button)e.Row.Cells[5].FindControl("btn1");
                if (miBoton2 != null)
                    miBoton2.BackColor = System.Drawing.ColorTranslator.FromHtml(miBoton2.Text);

                Button miBoton3 = (Button)e.Row.Cells[6].FindControl("btn2");
                if (miBoton3 != null)
                    miBoton3.BackColor = System.Drawing.ColorTranslator.FromHtml(miBoton3.Text);
            }
        }

        protected void dtgListado_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            String strRedireccion = "";
            try
            {
                if (e.CommandName.Equals("detalles"))
                {
                    string strId = e.CommandArgument.ToString();

                    //Filtramos el Dataset
                    DataView dv = this.dsReporte.Tables[0].DefaultView;
                    dv.RowFilter = "id = " + strId;

                    DataTable detailTable = dv.ToTable();


                    dtgDetalles.DataSource = detailTable;
                    dtgDetalles.DataBind();
                    dtgDetalles.HeaderRow.TableSection = TableRowSection.TableHeader;

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#currentdetail').appendTo('body').modal('show');", true);
                }
                else if (e.CommandName.Equals("modificar"))
                {
                    string strId = e.CommandArgument.ToString();
                    rnSegProyecto rnModificar = new rnSegProyecto();
                    entSegProyecto objPag = rnModificar.ObtenerObjeto(int.Parse(strId));
                    strRedireccion = "~/SEG/docSegProyecto.aspx?id=" + objPag.id_proyecto;
                }
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }

            if (!strRedireccion.IsNullOrEmpty())
                Response.Redirect(strRedireccion);
        }

        protected void imgAtras_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }

        protected void btnAtras_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Template/Dashboard.aspx");
        }

        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/SEG/docSegProyecto.aspx");
        }

    }
}
