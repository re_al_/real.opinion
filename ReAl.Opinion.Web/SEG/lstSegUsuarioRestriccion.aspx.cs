#region 
/***********************************************************************************************************
	NOMBRE:       lstSegUsuario
	DESCRIPCION:
		Clase que define los metodos y propiedades de la página lstSegUsuario

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        17/06/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion

#region

using System;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

using Microsoft.Ajax.Utilities;
using ReAl.Opinion.Dal;
using ReAl.Opinion.Dal.Modelo;
using ReAl.Opinion.Dal.Entidades;
using ReAl.Opinion.Web.App_Class;

#endregion

namespace ReAl.Opinion.Web.SEG
{
    public partial class lstSegUsuarioRestriccion : Page
    {

        #region Parametros de llegada

        public lstSegUsuarioRestriccion()
        {
            dsReporte = null;
        }

        public DataSet dsReporte { set; get; }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Validamos el salto directo
                cSessionHandler miSesion = new cSessionHandler();
                if (miSesion.arrMenu != null)
                    if (!miSesion.arrMenu.Contains(Request.AppRelativeCurrentExecutionFilePath))
                        Server.Transfer("~/Template/Dashboard.aspx");

                //Titulo de Pagina
                this.Title = cParametrosWeb.strNombrePagina;

                if (!Page.IsPostBack)
                    CargarDdlRol();

                String strNombreVista = "vw_seg_usuariorestriccion_lis";

                ArrayList arrNomParam = new ArrayList();
                arrNomParam.Add(entSegUsuariorestriccion.Fields.id_usuario.ToString());
                ArrayList arrParam = new ArrayList();
                arrParam.Add(miSesion.appUsuario.id_usuario);

                ArrayList arrHide = new ArrayList();

                cReportBuilder cRpt = new cReportBuilder();
                this.dsReporte = cRpt.obtenerReporteVista(strNombreVista, arrNomParam, arrParam, arrHide);

                cargarListado();
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }
        }

        private void CargarDdlRol()
        {
            try
            {
                //Funcion que inicializa los controles
                rnSegProyecto rn = new rnSegProyecto();
                DataTable dt = rn.ObtenerDataTable();

                ddlid_proyecto.DataValueField = entSegProyecto.Fields.id_proyecto.ToString();
                ddlid_proyecto.DataTextField = entSegProyecto.Fields.codigo.ToString();
                ddlid_proyecto.DataSource = dt;
                ddlid_proyecto.DataBind();
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }
        }

        private void cargarListado()
        {
            try
            {
                DataView dv = this.dsReporte.Tables[1].DefaultView;
                dv.Sort = "proyecto";
                

                DataTable listTable = dv.ToTable();
                dtgListado.DataSource = listTable;
                dtgListado.DataBind();

                if (listTable.Rows.Count > 0)
                {
                    dtgListado.HeaderRow.TableSection = TableRowSection.TableHeader;

                    for (int i = 0; i < dtgListado.Columns.Count; i++)
                        dtgListado.HeaderRow.Cells[i].Attributes.Add("data-sortable", "true");
                }                   
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }
        }

        protected void dtgListado_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

            }
        }

        protected void dtgListado_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            String strRedireccion = "";
            try
            {
                if (e.CommandName.Equals("detalles"))
                {
                    string strId = e.CommandArgument.ToString();

                    //Filtramos el Dataset
                    DataView dv = this.dsReporte.Tables[0].DefaultView;
                    dv.RowFilter = "id = " + strId;

                    DataTable detailTable = dv.ToTable();


                    dtgDetalles.DataSource = detailTable;
                    dtgDetalles.DataBind();
                    dtgDetalles.HeaderRow.TableSection = TableRowSection.TableHeader;

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#currentdetail').appendTo('body').modal('show');", true);
                }
                else if (e.CommandName.Equals("modificar"))
                {
                    string strId = e.CommandArgument.ToString();
                    rnSegPagina rnModificar = new rnSegPagina();
                    entSegPagina objPag = rnModificar.ObtenerObjeto(int.Parse(strId));
                    strRedireccion = "~/SEG/docSegUsuario.aspx?id=" + objPag.id_pagina;
                }
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp); ;
            }

            if (!strRedireccion.IsNullOrEmpty())
                Response.Redirect(strRedireccion);
        }

        protected void imgAtras_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }

        protected void btnAtras_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Template/Dashboard.aspx");
        }

        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            //cambiamos el proyecto
            Boolean bProcede = false;

            try
            {
                Page.Validate("cabecera");
                if (!Page.IsValid) return;

                rnSegUsuariorestriccion rnModificar = new rnSegUsuariorestriccion();
                entSegUsuariorestriccion objPag = new entSegUsuariorestriccion();
                cSessionHandler miSesion = new cSessionHandler();

                
                //Apropiamos los valores para MODIFICAR
                objPag = rnModificar.ObtenerObjeto(miSesion.appRestriccion.id_usurestriccion);
                objPag.id_proyecto = rnModificar.GetColumnType(ddlid_proyecto.SelectedValue, entSegUsuariorestriccion.Fields.id_proyecto);
                objPag.apiestado = "ELABORADO";
                objPag.usumod = miSesion.appUsuario.login;

                rnModificar.Update(objPag);


                //Actualizamos el menu y la variable de session
                miSesion.appRestriccion = objPag;
                cSegRolPagina menu = new cSegRolPagina();
                DataTable dtMenu = menu.ObtenerMenu();

                bProcede = true;
            }
            catch (Exception exp)
            {
                ((Main)this.Master).mostrarPopUp(this, exp);
            }
            if (bProcede)
                Response.Redirect("~/Template/Dashboard.aspx?msg=Se ha cambiado el Proyecto activo");
        }

    }
}
