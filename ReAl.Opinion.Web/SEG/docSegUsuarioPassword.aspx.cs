#region 
/***********************************************************************************************************
	NOMBRE:       lstSegUsuario
	DESCRIPCION:
		Clase que define los metodos y propiedades de la página docSegUsuario

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        17/06/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion

#region

using System;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReAl.Opinion.Dal;
using ReAl.Opinion.Dal.Modelo;
using ReAl.Opinion.Dal.Entidades;
using ReAl.Opinion.Utils;
using ReAl.Opinion.Web.App_Class;

#endregion

namespace ReAl.Opinion.Web.SEG
{
    public partial class docSegUsuarioPassword : Page
    {
        #region Parametros de llegada
        public int localid
        {
            get
            {
                if (Request["id"] == null)
                {
                    return 0;
                }
                return int.Parse(Request["id"].ToString());
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //Titulo de Pagina
                this.Title = cParametrosWeb.strNombrePagina;
            }

        }

        protected void btnAtras_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Template/Dashboard.aspx");
        }
        
        protected void btnRegistrar_Click(object sender, EventArgs e)
        {
            Boolean bProcede = false;
            try
            {
                if (txtNew.Text == txtRepeat.Text)
                {
                    rnSegUsuario rn = new rnSegUsuario();
                    cSessionHandler miSesion = new cSessionHandler();
                    entSegUsuario obj = rn.ObtenerObjeto(miSesion.appUsuario.id_usuario);

                    if (obj != null)
                    {
                        if (obj.password == cFuncionesEncriptacion.generarMD5(txtActual.Text).ToLower())
                        {
                            //Cambiamos el Password
                            obj.password = cFuncionesEncriptacion.generarMD5(txtNew.Text).ToLower();
                            obj.usumod = miSesion.appUsuario.login;

                            rn.Update(obj);
                            bProcede = true;
                        }
                        else
                        {
                            divError.Visible = true;
                            lblError.Text = "Su password actual no coincide";
                        }
                    }
                }
                else
                {
                    divError.Visible = true;
                    lblError.Text = "Los nuevos passwords no son iguales";
                }
            }
            catch (Exception exp)
            {
                ((Main)this.Parent.Page.Master).mostrarPopUp(this.Parent.Page, exp); ;
            }
            if (bProcede)
                Response.Redirect("~/Template/Dashboard.aspx");
        }
    }
}

