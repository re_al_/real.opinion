﻿#region librerias

using System.Web.Optimization;

#endregion

namespace ReAl.Opinion.Web
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254726
        public static void RegisterBundles(BundleCollection bundles)
        {
            // Order of CSS or Js
            // 1. JQuery
            // 2. Bootstrap

            #region JS / SCRIPT

            bundles.Add(new ScriptBundle("~/bundles/jQuery").Include(
                  //"~/Scripts/bootstrap.min.js"
                  "~/Scripts/bootstrap.min.js"
                , "~/Scripts/bootstrap-dialog.js"
                , "~/Scripts/jquery-2.1.4.js"
                //, "~/bower_components/jquery/dist/jquery.js"
                , "~/bower_components/metisMenu/dist/metisMenu.min.js"
                , "~/bower_components/raphael/raphael-min.js"
                , "~/bower_components/morrisjs/morris.min.js"
                , "~/bower_components/dist/js/sb-admin-2.js"
                ));

            #endregion

            #region CSS / STYLE

            bundles.Add(new StyleBundle("~/bundles/BootstrapCss").Include(
                  "~/Content/bootstrap.min.css"
                , "~/Content/bootstrap.min.css"
                , "~/Content/bootstrap-dialog.css"
                , "~/Content/TiTaToggle/titatoggle-dist-min.css"
                , "~/bower_components/metisMenu/dist/metisMenu.min.css"
                , "~/bower_components/dist/css/timeline.css"
                , "~/bower_components/dist/css/sb-admin-2.css"
                , "~/bower_components/morrisjs/morris.css"
                ));

            #endregion

            // Enable bundle optimization.
            BundleTable.EnableOptimizations = true;
        }
    }
}