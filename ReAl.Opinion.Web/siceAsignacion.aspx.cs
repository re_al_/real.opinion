﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ionic.Zip;
using ReAl.Opinion.Dal.Entidades;
using ReAl.Opinion.Dal.Modelo;
using ReAl.Opinion.Utils;
using ReAl.Opinion.Web.App_Class;

namespace ReAl.Opinion.Web
{
    public partial class siceAsignacion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string summary = "Descargando Asignacion...<br />";
            Boolean bProcede = false;
            try
            {
                //Obtenemos la serie                
                string strExtension = "";
                string strSerie = Request["serie"];
                if (strSerie.IsNullOrEmpty())
                    throw new cSimpleException("No se ha recibido el parametro correcto");
                string strPath = Server.MapPath("~/carga/" + strSerie + "/");
                if (!System.IO.Directory.Exists(strPath))
                    System.IO.Directory.CreateDirectory(strPath);

                //Verificamos si la serie esta asignada:
                ArrayList arrColWhereTab = new ArrayList();
                arrColWhereTab.Add(entOpeTablet.Fields.imei.ToString());
                arrColWhereTab.Add(entOpeTablet.Fields.apiestado.ToString());
                ArrayList arrValWhereTab = new ArrayList();
                arrValWhereTab.Add("'" + strSerie + "'");
                arrValWhereTab.Add("'ELABORADO'");
                entOpeTablet objTab = (new rnOpeTablet()).ObtenerObjeto(arrColWhereTab, arrValWhereTab);

                if (objTab == null)
                    throw new cSimpleException("El Dispositivo Móvil con serie " + strSerie + " no se ha registrado");

                //Obtenemos la asignacion
                ArrayList arrColWhere = new ArrayList();
                arrColWhere.Add(entOpeAsignacion.Fields.id_tablet.ToString());
                arrColWhere.Add(entOpeAsignacion.Fields.apiestado.ToString());
                ArrayList arrValWhere = new ArrayList();
                arrValWhere.Add(objTab.id_tablet);
                arrValWhere.Add("'ELABORADO'");
                entOpeAsignacion objAsig = (new rnOpeAsignacion()).ObtenerObjeto(arrColWhere, arrValWhere);

                if (objAsig == null)
                    throw new cSimpleException("El Dispositivo Móvil con serie " + strSerie + " no se ha Asignado a ningun proyecto");

                var bytesMov =
                    Encoding.UTF8
                        .GetBytes(cExportarCsv.csvOpeMovimiento(strPath + "ope_movimiento" + strExtension,
                            objAsig.id_proyecto.ToString()));

                var bytesUpm =
                    Encoding.UTF8
                        .GetBytes(cExportarCsv.csvCatUpm(strPath + "cat_upm" + strExtension,
                            objAsig.id_proyecto.ToString()));


                Response.Clear();
                Response.BufferOutput = false; // for large files...
                string archiveName = "CSV_" + strSerie +".zip";
                Response.ContentType = "application/zip";
                Response.AddHeader("content-disposition", "filename=" + archiveName);

                using (ZipFile zip = new ZipFile())
                {
                    // filesToInclude is an IEnumerable<String>, like String[] or List<String>
                    zip.AddEntry("ope_movimiento" + strExtension, bytesMov);
                    zip.AddEntry("cat_upm" + strExtension, bytesUpm);

                    // Add a file from a string                
                    zip.Save(Response.OutputStream);
                }
                // Response.End();  // no! See http://stackoverflow.com/questions/1087777
                Response.Close();
            }
            catch (Exception exp)
            {
                if (exp.GetType() == typeof (cSimpleException))
                    summary += exp.Message + "<br />";
                else
                {
                    summary += exp.Message + "<br />";
                    summary += exp.StackTrace + "<br />";
                }
            }
            finally
            {
                if (bProcede)
                    lblRes.Text = "OK";
                else
                    lblRes.Text = summary;
            }
        }
    }
}