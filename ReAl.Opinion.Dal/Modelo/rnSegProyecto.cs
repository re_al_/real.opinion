#region 
/***********************************************************************************************************
	NOMBRE:       rnSegProyecto
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla seg_proyecto

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        12/10/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using ReAl.Opinion.Dal;
using ReAl.Opinion.PgConn;
using ReAl.Opinion.Dal.Entidades;
using ReAl.Opinion.Dal.Interface;
using System.Web.UI.WebControls;
#endregion

namespace ReAl.Opinion.Dal.Modelo
{
	public partial class rnSegProyecto: inSegProyecto
	{
		//Debe implementar la Interface (Alt + Shift + F10)

		#region inSegProyecto Members

		#region Reflection

		/// <summary>
		/// Metodo que devuelve el Script SQL de la Tabla
		/// </summary>
		/// <returns>Script SQL</returns>
		public string getTableScript()
		{
			TableClass tabla = new TableClass(typeof(entSegProyecto));
			return tabla.CreateTableScript();
		}
		
		/// <summary>
		/// Metodo para castear Dinamicamente un Tipo
		/// <summary>
		/// <param name="valor">Tipo a ser casteado</param>
		/// <param name="myField">Enum de la columna</param>
		/// <returns>Devuelve un objeto del Tipo de la columna especificada en el Enum</returns>
		public dynamic GetColumnType(object valor, entSegProyecto.Fields myField)
		{
			if (DBNull.Value.Equals(valor)) 
				return null;
			Type destino = typeof(entSegProyecto).GetProperty(myField.ToString()).PropertyType;
			var miTipo = Nullable.GetUnderlyingType(destino) ?? destino;
			return Convert.ChangeType(valor, miTipo);
		}

		/// <summary>
		/// 	 Funcion que obtiene la llave primaria unica de la tabla seg_proyecto a partir de una cadena
		/// </summary>
		/// <param name="args" type="string[]">
		///     <para>
		/// 		 Cadena desde la que se construye el identificador unico de la tabla seg_proyecto
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Identificador unico de la tabla seg_proyecto
		/// </returns>
		public string CreatePK(string[] args)
		{
			return args[0];
		}
		
		#endregion 

		#region ObtenerObjeto

		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo entSegProyecto que cumple con los filtros de los parametros
		/// </returns>
		public entSegProyecto ObtenerObjeto(int intid_proyecto)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(entSegProyecto.Fields.id_proyecto.ToString());
		
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(intid_proyecto);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegProyecto que cumple con los filtros de los parametros
		/// </returns>
		public entSegProyecto ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegProyecto que cumple con los filtros de los parametros
		/// </returns>
		public entSegProyecto ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegProyecto.Fields.id_proyecto.ToString());
				arrColumnas.Add(entSegProyecto.Fields.nombre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.codigo.ToString());
				arrColumnas.Add(entSegProyecto.Fields.descripcion.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecinicio.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecfin.ToString());
				arrColumnas.Add(entSegProyecto.Fields.apiestado.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usucre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.feccre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usumod.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecmod.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_web.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_movil.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_font.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entSegProyecto.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count == 1)
				{
					entSegProyecto obj = new entSegProyecto();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegProyecto que cumple con los filtros de los parametros
		/// </returns>
		public entSegProyecto ObtenerObjeto(Hashtable htbFiltro)
		{
			return ObtenerObjeto(htbFiltro, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegProyecto que cumple con los filtros de los parametros
		/// </returns>
		public entSegProyecto ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegProyecto.Fields.id_proyecto.ToString());
				arrColumnas.Add(entSegProyecto.Fields.nombre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.codigo.ToString());
				arrColumnas.Add(entSegProyecto.Fields.descripcion.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecinicio.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecfin.ToString());
				arrColumnas.Add(entSegProyecto.Fields.apiestado.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usucre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.feccre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usumod.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecmod.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_web.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_movil.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_font.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entSegProyecto.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count == 1)
				{
					entSegProyecto obj = new entSegProyecto();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegProyecto que cumple con los filtros de los parametros
		/// </returns>
		public entSegProyecto ObtenerObjeto(entSegProyecto.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegProyecto que cumple con los filtros de los parametros
		/// </returns>
		public entSegProyecto ObtenerObjeto(entSegProyecto.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un Business Object del Tipo entSegProyecto a partir de su llave promaria
		/// </summary>
		/// <returns>
		/// 	Objeto del Tipo entSegProyecto
		/// </returns>
		public entSegProyecto ObtenerObjeto(int intid_proyecto, ref cTrans localTrans )
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(entSegProyecto.Fields.id_proyecto.ToString());
		
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(intid_proyecto);
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_proyecto
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegProyecto que cumple con los filtros de los parametros
		/// </returns>
		public entSegProyecto ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_proyecto
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegProyecto que cumple con los filtros de los parametros
		/// </returns>
		public entSegProyecto ObtenerObjeto(Hashtable htbFiltro, ref cTrans localTrans)
		{
			return ObtenerObjeto(htbFiltro, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_proyecto
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegProyecto que cumple con los filtros de los parametros
		/// </returns>
		public entSegProyecto ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales,  ref cTrans localTrans)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_proyecto
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegProyecto que cumple con los filtros de los parametros
		/// </returns>
		public entSegProyecto ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegProyecto.Fields.id_proyecto.ToString());
				arrColumnas.Add(entSegProyecto.Fields.nombre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.codigo.ToString());
				arrColumnas.Add(entSegProyecto.Fields.descripcion.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecinicio.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecfin.ToString());
				arrColumnas.Add(entSegProyecto.Fields.apiestado.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usucre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.feccre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usumod.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecmod.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_web.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_movil.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_font.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entSegProyecto.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count == 1)
				{
					entSegProyecto obj = new entSegProyecto();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_proyecto
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegProyecto que cumple con los filtros de los parametros
		/// </returns>
		public entSegProyecto ObtenerObjeto(entSegProyecto.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_proyecto
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegProyecto que cumple con los filtros de los parametros
		/// </returns>
		public entSegProyecto ObtenerObjeto(entSegProyecto.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion

		#region ObtenerLista

		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegProyecto> ObtenerLista()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegProyecto> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegProyecto> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegProyecto.Fields.id_proyecto.ToString());
				arrColumnas.Add(entSegProyecto.Fields.nombre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.codigo.ToString());
				arrColumnas.Add(entSegProyecto.Fields.descripcion.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecinicio.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecfin.ToString());
				arrColumnas.Add(entSegProyecto.Fields.apiestado.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usucre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.feccre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usumod.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecmod.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_web.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_movil.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_font.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entSegProyecto.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<entSegProyecto>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegProyecto> ObtenerLista(entSegProyecto.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegProyecto> ObtenerLista(entSegProyecto.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_proyecto
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegProyecto> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_proyecto
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegProyecto> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegProyecto.Fields.id_proyecto.ToString());
				arrColumnas.Add(entSegProyecto.Fields.nombre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.codigo.ToString());
				arrColumnas.Add(entSegProyecto.Fields.descripcion.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecinicio.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecfin.ToString());
				arrColumnas.Add(entSegProyecto.Fields.apiestado.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usucre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.feccre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usumod.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecmod.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_web.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_movil.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_font.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entSegProyecto.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<entSegProyecto>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_proyecto
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegProyecto> ObtenerLista(entSegProyecto.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_proyecto
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegProyecto> ObtenerLista(entSegProyecto.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegProyecto> ObtenerLista(Hashtable htbFiltro)
		{
			return ObtenerLista(htbFiltro, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegProyecto> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegProyecto.Fields.id_proyecto.ToString());
				arrColumnas.Add(entSegProyecto.Fields.nombre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.codigo.ToString());
				arrColumnas.Add(entSegProyecto.Fields.descripcion.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecinicio.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecfin.ToString());
				arrColumnas.Add(entSegProyecto.Fields.apiestado.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usucre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.feccre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usumod.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecmod.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_web.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_movil.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_font.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entSegProyecto.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<entSegProyecto>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_proyecto
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegProyecto> ObtenerLista(Hashtable htbFiltro, ref cTrans localTrans)
		{
			return ObtenerLista(htbFiltro, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_proyecto
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegProyecto> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegProyecto.Fields.id_proyecto.ToString());
				arrColumnas.Add(entSegProyecto.Fields.nombre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.codigo.ToString());
				arrColumnas.Add(entSegProyecto.Fields.descripcion.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecinicio.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecfin.ToString());
				arrColumnas.Add(entSegProyecto.Fields.apiestado.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usucre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.feccre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usumod.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecmod.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_web.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_movil.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_font.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entSegProyecto.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<entSegProyecto>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegProyecto> ObtenerListaDesdeVista(String strVista)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegProyecto> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegProyecto> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<entSegProyecto>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegProyecto> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro)
		{
			return ObtenerListaDesdeVista(strVista, htbFiltro, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegProyecto> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<entSegProyecto>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegProyecto> ObtenerListaDesdeVista(String strVista, entSegProyecto.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegProyecto> ObtenerListaDesdeVista(String strVista, entSegProyecto.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_proyecto
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegProyecto> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_proyecto
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegProyecto> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<entSegProyecto>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_proyecto
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegProyecto> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref cTrans localTrans)
		{
			return ObtenerListaDesdeVista(strVista, htbFiltro, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_proyecto
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegProyecto> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<entSegProyecto>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_proyecto
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegProyecto> ObtenerListaDesdeVista(String strVista, entSegProyecto.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_proyecto
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegProyecto> ObtenerListaDesdeVista(String strVista, entSegProyecto.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion 

		#region ObtenerCola y Obtener Pila

		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entSegProyecto> ObtenerCola()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entSegProyecto> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entSegProyecto> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegProyecto.Fields.id_proyecto.ToString());
				arrColumnas.Add(entSegProyecto.Fields.nombre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.codigo.ToString());
				arrColumnas.Add(entSegProyecto.Fields.descripcion.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecinicio.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecfin.ToString());
				arrColumnas.Add(entSegProyecto.Fields.apiestado.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usucre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.feccre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usumod.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecmod.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_web.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_movil.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_font.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entSegProyecto.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearCola(table);
				}
				else
					return new Queue<entSegProyecto>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entSegProyecto> ObtenerCola(entSegProyecto.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entSegProyecto> ObtenerCola(entSegProyecto.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_proyecto
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entSegProyecto> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_proyecto
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entSegProyecto> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegProyecto.Fields.id_proyecto.ToString());
				arrColumnas.Add(entSegProyecto.Fields.nombre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.codigo.ToString());
				arrColumnas.Add(entSegProyecto.Fields.descripcion.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecinicio.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecfin.ToString());
				arrColumnas.Add(entSegProyecto.Fields.apiestado.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usucre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.feccre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usumod.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecmod.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_web.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_movil.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_font.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entSegProyecto.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearCola(table);
				}
				else
					return new Queue<entSegProyecto>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_proyecto
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entSegProyecto> ObtenerCola(entSegProyecto.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_proyecto
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entSegProyecto> ObtenerCola(entSegProyecto.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entSegProyecto> ObtenerPila()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entSegProyecto> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entSegProyecto> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegProyecto.Fields.id_proyecto.ToString());
				arrColumnas.Add(entSegProyecto.Fields.nombre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.codigo.ToString());
				arrColumnas.Add(entSegProyecto.Fields.descripcion.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecinicio.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecfin.ToString());
				arrColumnas.Add(entSegProyecto.Fields.apiestado.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usucre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.feccre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usumod.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecmod.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_web.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_movil.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_font.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entSegProyecto.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearPila(table);
				}
				else
					return new Stack<entSegProyecto>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entSegProyecto> ObtenerPila(entSegProyecto.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entSegProyecto> ObtenerPila(entSegProyecto.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_proyecto
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entSegProyecto> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_proyecto
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entSegProyecto> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegProyecto.Fields.id_proyecto.ToString());
				arrColumnas.Add(entSegProyecto.Fields.nombre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.codigo.ToString());
				arrColumnas.Add(entSegProyecto.Fields.descripcion.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecinicio.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecfin.ToString());
				arrColumnas.Add(entSegProyecto.Fields.apiestado.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usucre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.feccre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usumod.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecmod.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_web.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_movil.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_font.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entSegProyecto.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearPila(table);
				}
				else
					return new Stack<entSegProyecto>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_proyecto
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entSegProyecto> ObtenerPila(entSegProyecto.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_proyecto
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entSegProyecto> ObtenerPila(entSegProyecto.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion 

		#region ObtenerDataTable

		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla seg_proyecto
		/// </summary>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de seg_proyecto
		/// </returns>
		public DataTable NuevoDataTable()
		{
			try
			{
				DataTable table = new DataTable ();
				DataColumn dc;
				dc = new DataColumn(entSegProyecto.Fields.id_proyecto.ToString(),typeof(entSegProyecto).GetProperty(entSegProyecto.Fields.id_proyecto.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entSegProyecto.Fields.nombre.ToString(),typeof(entSegProyecto).GetProperty(entSegProyecto.Fields.nombre.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entSegProyecto.Fields.codigo.ToString(),typeof(entSegProyecto).GetProperty(entSegProyecto.Fields.codigo.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entSegProyecto.Fields.descripcion.ToString(),typeof(entSegProyecto).GetProperty(entSegProyecto.Fields.descripcion.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entSegProyecto.Fields.fecinicio.ToString(),typeof(entSegProyecto).GetProperty(entSegProyecto.Fields.fecinicio.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entSegProyecto.Fields.fecfin.ToString(),typeof(entSegProyecto).GetProperty(entSegProyecto.Fields.fecfin.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entSegProyecto.Fields.apiestado.ToString(),typeof(entSegProyecto).GetProperty(entSegProyecto.Fields.apiestado.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entSegProyecto.Fields.usucre.ToString(),typeof(entSegProyecto).GetProperty(entSegProyecto.Fields.usucre.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entSegProyecto.Fields.feccre.ToString(),typeof(entSegProyecto).GetProperty(entSegProyecto.Fields.feccre.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entSegProyecto.Fields.usumod.ToString(),typeof(entSegProyecto).GetProperty(entSegProyecto.Fields.usumod.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entSegProyecto.Fields.fecmod.ToString(),typeof(entSegProyecto).GetProperty(entSegProyecto.Fields.fecmod.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entSegProyecto.Fields.color_web.ToString(),typeof(entSegProyecto).GetProperty(entSegProyecto.Fields.color_web.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entSegProyecto.Fields.color_movil.ToString(),typeof(entSegProyecto).GetProperty(entSegProyecto.Fields.color_movil.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entSegProyecto.Fields.color_font.ToString(),typeof(entSegProyecto).GetProperty(entSegProyecto.Fields.color_font.ToString()).PropertyType);
				table.Columns.Add(dc);

				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que genera un DataTable con determinadas columnas de una seg_proyecto
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de seg_proyecto
		/// </returns>
		public DataTable NuevoDataTable(ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'2'");
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entSegProyecto.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla seg_proyecto
		/// </summary>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de seg_proyecto
		/// </returns>
		public DataTable ObtenerDataTable()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDataTable(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla y n condicion WHERE seg_proyecto
		/// </summary>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de seg_proyecto
		/// </returns>
		public DataTable ObtenerDataTable(String strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla seg_proyecto
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de seg_proyecto
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla seg_proyecto
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de seg_proyecto
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla seg_proyecto
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de seg_proyecto
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla seg_proyecto
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de seg_proyecto
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro)
		{
			try
			{
				return ObtenerDataTable(arrColumnas, htbFiltro, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla seg_proyecto
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de seg_proyecto
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla seg_proyecto
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de seg_proyecto
		/// </returns>
		public DataTable ObtenerDataTable(Hashtable htbFiltro)
		{
			try
			{
				return ObtenerDataTable(htbFiltro, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla seg_proyecto
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de seg_proyecto
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entSegProyecto.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla seg_proyecto
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de seg_proyecto
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entSegProyecto.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla seg_proyecto
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de seg_proyecto
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegProyecto.Fields.id_proyecto.ToString());
				arrColumnas.Add(entSegProyecto.Fields.nombre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.codigo.ToString());
				arrColumnas.Add(entSegProyecto.Fields.descripcion.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecinicio.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecfin.ToString());
				arrColumnas.Add(entSegProyecto.Fields.apiestado.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usucre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.feccre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usumod.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecmod.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_web.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_movil.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_font.ToString());
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla seg_proyecto
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de seg_proyecto
		/// </returns>
		public DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegProyecto.Fields.id_proyecto.ToString());
				arrColumnas.Add(entSegProyecto.Fields.nombre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.codigo.ToString());
				arrColumnas.Add(entSegProyecto.Fields.descripcion.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecinicio.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecfin.ToString());
				arrColumnas.Add(entSegProyecto.Fields.apiestado.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usucre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.feccre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usumod.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecmod.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_web.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_movil.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_font.ToString());
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla seg_proyecto
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de seg_proyecto
		/// </returns>
		public DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegProyecto.Fields.id_proyecto.ToString());
				arrColumnas.Add(entSegProyecto.Fields.nombre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.codigo.ToString());
				arrColumnas.Add(entSegProyecto.Fields.descripcion.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecinicio.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecfin.ToString());
				arrColumnas.Add(entSegProyecto.Fields.apiestado.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usucre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.feccre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usumod.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecmod.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_web.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_movil.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_font.ToString());
				return ObtenerDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla seg_proyecto
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de seg_proyecto
		/// </returns>
		public DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return ObtenerDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla seg_proyecto
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de seg_proyecto
		/// </returns>
		public DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DataTable table = local.cargarDataTableOr(cParametros.schema + entSegProyecto.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="searchField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(entSegProyecto.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="searchField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(entSegProyecto.Fields searchField, object searchValue, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, entSegProyecto.Fields searchField, object searchValue, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, entSegProyecto.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		

		#endregion 

		#region ObtenerDiccionario

		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entSegProyecto> ObtenerDiccionario()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entSegProyecto> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entSegProyecto> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegProyecto.Fields.id_proyecto.ToString());
				arrColumnas.Add(entSegProyecto.Fields.nombre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.codigo.ToString());
				arrColumnas.Add(entSegProyecto.Fields.descripcion.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecinicio.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecfin.ToString());
				arrColumnas.Add(entSegProyecto.Fields.apiestado.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usucre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.feccre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usumod.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecmod.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_web.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_movil.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_font.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entSegProyecto.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearDiccionario(table);
				}
				else
					return new Dictionary<string, entSegProyecto>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entSegProyecto> ObtenerDiccionario(entSegProyecto.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entSegProyecto> ObtenerDiccionario(entSegProyecto.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_proyecto
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entSegProyecto> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_proyecto
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entSegProyecto> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegProyecto.Fields.id_proyecto.ToString());
				arrColumnas.Add(entSegProyecto.Fields.nombre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.codigo.ToString());
				arrColumnas.Add(entSegProyecto.Fields.descripcion.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecinicio.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecfin.ToString());
				arrColumnas.Add(entSegProyecto.Fields.apiestado.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usucre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.feccre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usumod.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecmod.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_web.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_movil.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_font.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entSegProyecto.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearDiccionario(table);
				}
				else
					return new Dictionary<string, entSegProyecto>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_proyecto
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entSegProyecto> ObtenerDiccionario(entSegProyecto.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegProyecto a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_proyecto
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entSegProyecto> ObtenerDiccionario(entSegProyecto.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion 

		#region FuncionesAgregadas

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegProyecto que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesCount(entSegProyecto.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegProyecto que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesCount(entSegProyecto.Fields refField, entSegProyecto.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegProyecto que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesCount(entSegProyecto.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("count(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegProyecto que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMin(entSegProyecto.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegProyecto que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMin(entSegProyecto.Fields refField, entSegProyecto.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegProyecto que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMin(entSegProyecto.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("min(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegProyecto que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMax(entSegProyecto.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegProyecto que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMax(entSegProyecto.Fields refField, entSegProyecto.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegProyecto que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMax(entSegProyecto.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("max(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegProyecto que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesSum(entSegProyecto.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegProyecto que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesSum(entSegProyecto.Fields refField, entSegProyecto.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegProyecto que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesSum(entSegProyecto.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("sum(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegProyecto que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesAvg(entSegProyecto.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegProyecto que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesAvg(entSegProyecto.Fields refField, entSegProyecto.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegProyecto que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesAvg(entSegProyecto.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("avg(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion 

		#region ABMs

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla seg_proyecto a partir de una clase del tipo Eseg_proyecto
		/// </summary>
		/// <param name="obj" type="Entidades.entSegProyecto">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla seg_proyectoseg_proyecto
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionseg_proyecto
		/// </returns>
		public bool Insert(entSegProyecto obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(entSegProyecto.Fields.id_proyecto.ToString());
				arrColumnas.Add(entSegProyecto.Fields.nombre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.codigo.ToString());
				arrColumnas.Add(entSegProyecto.Fields.descripcion.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecinicio.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecfin.ToString());
				arrColumnas.Add(entSegProyecto.Fields.apiestado.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usucre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_web.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_movil.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_font.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.id_proyecto);
				arrValores.Add(obj.nombre == null ? null : "'" + obj.nombre + "'");
				arrValores.Add(obj.codigo == null ? null : "'" + obj.codigo + "'");
				arrValores.Add(obj.descripcion == null ? null : "'" + obj.descripcion + "'");
				arrValores.Add(obj.fecinicio == null ? null : "'" + Convert.ToDateTime(obj.fecinicio).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.fecfin == null ? null : "'" + Convert.ToDateTime(obj.fecfin).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.color_web == null ? null : "'" + obj.color_web + "'");
				arrValores.Add(obj.color_movil == null ? null : "'" + obj.color_movil + "'");
				arrValores.Add(obj.color_font == null ? null : "'" + obj.color_font + "'");

			
				cConn local = new cConn();
				return local.insertBD(entSegProyecto.strNombreTabla, arrColumnas, arrValores);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla seg_proyecto a partir de una clase del tipo Eseg_proyecto
		/// </summary>
		/// <param name="obj" type="Entidades.entSegProyecto">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla seg_proyectoseg_proyecto
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_proyecto
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionseg_proyecto
		/// </returns>
		public bool Insert(entSegProyecto obj, ref cTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(entSegProyecto.Fields.id_proyecto.ToString());
				arrColumnas.Add(entSegProyecto.Fields.nombre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.codigo.ToString());
				arrColumnas.Add(entSegProyecto.Fields.descripcion.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecinicio.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecfin.ToString());
				arrColumnas.Add(entSegProyecto.Fields.apiestado.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usucre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_web.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_movil.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_font.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.id_proyecto);
				arrValores.Add(obj.nombre == null ? null : "'" + obj.nombre + "'");
				arrValores.Add(obj.codigo == null ? null : "'" + obj.codigo + "'");
				arrValores.Add(obj.descripcion == null ? null : "'" + obj.descripcion + "'");
				arrValores.Add(obj.fecinicio == null ? null : "'" + Convert.ToDateTime(obj.fecinicio).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.fecfin == null ? null : "'" + Convert.ToDateTime(obj.fecfin).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.color_web == null ? null : "'" + obj.color_web + "'");
				arrValores.Add(obj.color_movil == null ? null : "'" + obj.color_movil + "'");
				arrValores.Add(obj.color_font == null ? null : "'" + obj.color_font + "'");

			
				cConn local = new cConn();
				return local.insertBD(entSegProyecto.strNombreTabla, arrColumnas, arrValores, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla seg_proyecto a partir de una clase del tipo Eseg_proyecto
		/// </summary>
		/// <param name="obj" type="Entidades.entSegProyecto">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla seg_proyectoseg_proyecto
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionseg_proyecto
		/// </returns>
		public bool InsertIdentity(entSegProyecto obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(entSegProyecto.Fields.id_proyecto.ToString());
				arrColumnas.Add(entSegProyecto.Fields.nombre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.codigo.ToString());
				arrColumnas.Add(entSegProyecto.Fields.descripcion.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecinicio.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecfin.ToString());
				arrColumnas.Add(entSegProyecto.Fields.apiestado.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usucre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_web.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_movil.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_font.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.id_proyecto);
				arrValores.Add(obj.nombre == null ? null : "'" + obj.nombre + "'");
				arrValores.Add(obj.codigo == null ? null : "'" + obj.codigo + "'");
				arrValores.Add(obj.descripcion == null ? null : "'" + obj.descripcion + "'");
				arrValores.Add(obj.fecinicio == null ? null : "'" + Convert.ToDateTime(obj.fecinicio).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.fecfin == null ? null : "'" + Convert.ToDateTime(obj.fecfin).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.color_web == null ? null : "'" + obj.color_web + "'");
				arrValores.Add(obj.color_movil == null ? null : "'" + obj.color_movil + "'");
				arrValores.Add(obj.color_font == null ? null : "'" + obj.color_font + "'");

			
				cConn local = new cConn();
				int intIdentidad = -1;
				bool res = local.insertBD(entSegProyecto.strNombreTabla, arrColumnas, arrValores, ref intIdentidad);
				obj.id_proyecto = intIdentidad;
				return res;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla seg_proyecto a partir de una clase del tipo Eseg_proyecto
		/// </summary>
		/// <param name="obj" type="Entidades.entSegProyecto">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla seg_proyectoseg_proyecto
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_proyecto
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionseg_proyecto
		/// </returns>
		public bool InsertIdentity(entSegProyecto obj, ref cTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(entSegProyecto.Fields.id_proyecto.ToString());
				arrColumnas.Add(entSegProyecto.Fields.nombre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.codigo.ToString());
				arrColumnas.Add(entSegProyecto.Fields.descripcion.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecinicio.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecfin.ToString());
				arrColumnas.Add(entSegProyecto.Fields.apiestado.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usucre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_web.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_movil.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_font.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.id_proyecto);
				arrValores.Add(obj.nombre == null ? null : "'" + obj.nombre + "'");
				arrValores.Add(obj.codigo == null ? null : "'" + obj.codigo + "'");
				arrValores.Add(obj.descripcion == null ? null : "'" + obj.descripcion + "'");
				arrValores.Add(obj.fecinicio == null ? null : "'" + Convert.ToDateTime(obj.fecinicio).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.fecfin == null ? null : "'" + Convert.ToDateTime(obj.fecfin).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.color_web == null ? null : "'" + obj.color_web + "'");
				arrValores.Add(obj.color_movil == null ? null : "'" + obj.color_movil + "'");
				arrValores.Add(obj.color_font == null ? null : "'" + obj.color_font + "'");

			
				cConn local = new cConn();
				int intIdentidad = -1;
				bool res = local.insertBD(entSegProyecto.strNombreTabla, arrColumnas, arrValores, ref intIdentidad, ref localTrans);
				obj.id_proyecto = intIdentidad;
				return res;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla seg_proyecto a partir de una clase del tipo Eseg_proyecto
		/// </summary>
		/// <param name="obj" type="Entidades.entSegProyecto">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla seg_proyecto
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionseg_proyecto
		/// </returns>
		public int Update(entSegProyecto obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegProyecto.Fields.nombre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.codigo.ToString());
				arrColumnas.Add(entSegProyecto.Fields.descripcion.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecinicio.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecfin.ToString());
				arrColumnas.Add(entSegProyecto.Fields.apiestado.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usumod.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_web.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_movil.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_font.ToString());
			
				ArrayList arrValores = new ArrayList();
				arrValores.Add(obj.nombre == null ? null : "'" + obj.nombre + "'");
				arrValores.Add(obj.codigo == null ? null : "'" + obj.codigo + "'");
				arrValores.Add(obj.descripcion == null ? null : "'" + obj.descripcion + "'");
				arrValores.Add(obj.fecinicio == null ? null : "'" + Convert.ToDateTime(obj.fecinicio).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.fecfin == null ? null : "'" + Convert.ToDateTime(obj.fecfin).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");
				arrValores.Add(obj.color_web == null ? null : "'" + obj.color_web + "'");
				arrValores.Add(obj.color_movil == null ? null : "'" + obj.color_movil + "'");
				arrValores.Add(obj.color_font == null ? null : "'" + obj.color_font + "'");

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(entSegProyecto.Fields.id_proyecto.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.id_proyecto);

			
				cConn local = new cConn();
				return local.updateBD(entSegProyecto.strNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla seg_proyecto a partir de una clase del tipo eseg_proyecto
		/// </summary>
		/// <param name="obj" type="Entidades.entSegProyecto">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla seg_proyecto
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_proyecto
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacion
		/// </returns>
		public int Update(entSegProyecto obj, ref cTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegProyecto.Fields.nombre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.codigo.ToString());
				arrColumnas.Add(entSegProyecto.Fields.descripcion.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecinicio.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecfin.ToString());
				arrColumnas.Add(entSegProyecto.Fields.apiestado.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usumod.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_web.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_movil.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_font.ToString());
			
				ArrayList arrValores = new ArrayList();
				arrValores.Add(obj.nombre == null ? null : "'" + obj.nombre + "'");
				arrValores.Add(obj.codigo == null ? null : "'" + obj.codigo + "'");
				arrValores.Add(obj.descripcion == null ? null : "'" + obj.descripcion + "'");
				arrValores.Add(obj.fecinicio == null ? null : "'" + Convert.ToDateTime(obj.fecinicio).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.fecfin == null ? null : "'" + Convert.ToDateTime(obj.fecfin).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");
				arrValores.Add(obj.color_web == null ? null : "'" + obj.color_web + "'");
				arrValores.Add(obj.color_movil == null ? null : "'" + obj.color_movil + "'");
				arrValores.Add(obj.color_font == null ? null : "'" + obj.color_font + "'");

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(entSegProyecto.Fields.id_proyecto.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.id_proyecto);

			
				cConn local = new cConn();
				return local.updateBD(entSegProyecto.strNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla seg_proyecto a partir de una clase del tipo entSegProyecto y su respectiva PK
		/// </summary>
		/// <param name="obj" type="Entidades.entSegProyecto">
		///     <para>
		/// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla seg_proyecto
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionseg_proyecto
		/// </returns>
		public int Delete(entSegProyecto obj)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(entSegProyecto.Fields.id_proyecto.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.id_proyecto);

			
				cConn local = new cConn();
				return local.deleteBD(entSegProyecto.strNombreTabla, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla seg_proyecto a partir de una clase del tipo entSegProyecto y su PK
		/// </summary>
		/// <param name="obj" type="Entidades.eseg_proyecto">
		///     <para>
		/// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla seg_proyecto
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_proyecto
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacionseg_proyecto
		/// </returns>
		public int Delete(entSegProyecto obj, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(entSegProyecto.Fields.id_proyecto.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.id_proyecto);

			
				cConn local = new cConn();
				return local.deleteBD(entSegProyecto.strNombreTabla, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla seg_proyecto a partir de una clase del tipo eseg_proyecto
		/// </summary>
		/// <param name="arrColumnasWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Valores para cada una de las columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionseg_proyecto
		/// </returns>
		public int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				cConn local = new cConn();
				return local.deleteBD("seg_proyecto", arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla seg_proyecto a partir de una clase del tipo eseg_proyecto
		/// </summary>
		/// <param name="arrColumnasWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Valores para cada una de las columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_proyecto
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacionseg_proyecto
		/// </returns>
		public int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			try
			{
				cConn local = new cConn();
				return local.deleteBD(entSegProyecto.strNombreTabla, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		

		#endregion 

		#region Llenado de elementos

		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla seg_proyecto
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla seg_proyectoseg_proyecto
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarDropDownList(ref cmb, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla seg_proyecto
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla seg_proyectoseg_proyecto
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarDropDownList(ref cmb, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla seg_proyecto
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla seg_proyectoseg_proyecto
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarDropDownList(ref cmb, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla seg_proyecto
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla seg_proyectoseg_proyecto
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegProyecto.Fields.id_proyecto.ToString());
				arrColumnas.Add(entSegProyecto.Fields.nombre.ToString());
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla seg_proyecto
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla seg_proyectoseg_proyecto
		///     </para>
		/// </param>
		/// <param name="valueField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, entSegProyecto.Fields valueField, entSegProyecto.Fields textField)
		{
			try
			{
				CargarDropDownList(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla seg_proyecto
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla seg_proyectoseg_proyecto
		///     </para>
		/// </param>
		/// <param name="valueField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, entSegProyecto.Fields valueField, entSegProyecto.Fields textField, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla seg_proyecto
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla seg_proyectoseg_proyecto
		///     </para>
		/// </param>
		/// <param name="valueField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, entSegProyecto.Fields valueField, String textField)
		{
			try
			{
				CargarDropDownList(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla seg_proyecto
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla seg_proyectoseg_proyecto
		///     </para>
		/// </param>
		/// <param name="valueField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, entSegProyecto.Fields valueField, String textField, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla seg_proyecto
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla seg_proyectoseg_proyecto
		///     </para>
		/// </param>
		/// <param name="valueField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, entSegProyecto.Fields valueField, entSegProyecto.Fields textField, entSegProyecto.Fields searchField, object searchValue)
		{
			try
			{
				CargarDropDownList(ref cmb, valueField, textField.ToString(), searchField, searchValue);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla seg_proyecto
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla seg_proyectoseg_proyecto
		///     </para>
		/// </param>
		/// <param name="valueField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, entSegProyecto.Fields valueField, String textField, entSegProyecto.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla seg_proyecto
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla seg_proyectoseg_proyecto
		///     </para>
		/// </param>
		/// <param name="valueField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, entSegProyecto.Fields valueField, entSegProyecto.Fields textField, entSegProyecto.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla seg_proyecto
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla seg_proyectoseg_proyecto
		///     </para>
		/// </param>
		/// <param name="valueField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, entSegProyecto.Fields valueField, String textField, entSegProyecto.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla seg_proyecto
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla seg_proyectoseg_proyecto
		///     </para>
		/// </param>
		/// <param name="valueField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, entSegProyecto.Fields valueField, entSegProyecto.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla seg_proyecto
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla seg_proyectoseg_proyecto
		///     </para>
		/// </param>
		/// <param name="valueField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, entSegProyecto.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla seg_proyecto
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla seg_proyectoseg_proyecto
		///     </para>
		/// </param>
		/// <param name="valueField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, entSegProyecto.Fields valueField, entSegProyecto.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla seg_proyecto
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla seg_proyectoseg_proyecto
		///     </para>
		/// </param>
		/// <param name="valueField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, entSegProyecto.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla seg_proyecto
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla seg_proyectoseg_proyecto
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entSegProyecto.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Columns.Count > 0)
				{
					cmb.DataValueField = table.Columns[0].ColumnName;
					cmb.DataTextField = table.Columns[1].ColumnName;
					cmb.DataSource = table;
					cmb.DataBind();

				}
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla seg_proyecto
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegProyecto.Fields.id_proyecto.ToString());
				arrColumnas.Add(entSegProyecto.Fields.nombre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.codigo.ToString());
				arrColumnas.Add(entSegProyecto.Fields.descripcion.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecinicio.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecfin.ToString());
				arrColumnas.Add(entSegProyecto.Fields.apiestado.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usucre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.feccre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usumod.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecmod.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_web.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_movil.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_font.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla seg_proyecto
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		  Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegProyecto.Fields.id_proyecto.ToString());
				arrColumnas.Add(entSegProyecto.Fields.nombre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.codigo.ToString());
				arrColumnas.Add(entSegProyecto.Fields.descripcion.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecinicio.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecfin.ToString());
				arrColumnas.Add(entSegProyecto.Fields.apiestado.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usucre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.feccre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usumod.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecmod.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_web.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_movil.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_font.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla seg_proyecto
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla seg_proyecto
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla seg_proyecto
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegProyecto.Fields.id_proyecto.ToString());
				arrColumnas.Add(entSegProyecto.Fields.nombre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.codigo.ToString());
				arrColumnas.Add(entSegProyecto.Fields.descripcion.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecinicio.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecfin.ToString());
				arrColumnas.Add(entSegProyecto.Fields.apiestado.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usucre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.feccre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usumod.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecmod.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_web.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_movil.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_font.ToString());
			
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla seg_proyecto
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegProyecto.Fields.id_proyecto.ToString());
				arrColumnas.Add(entSegProyecto.Fields.nombre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.codigo.ToString());
				arrColumnas.Add(entSegProyecto.Fields.descripcion.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecinicio.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecfin.ToString());
				arrColumnas.Add(entSegProyecto.Fields.apiestado.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usucre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.feccre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usumod.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecmod.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_web.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_movil.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_font.ToString());
			
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla seg_proyecto
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla seg_proyecto
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DbDataReader dsReader = local.cargarDataReaderAnd(cParametros.schema + entSegProyecto.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				dtg.DataSource = dsReader;
				dtg.DataBind();

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla seg_proyecto
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, entSegProyecto.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegProyecto.Fields.id_proyecto.ToString());
				arrColumnas.Add(entSegProyecto.Fields.nombre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.codigo.ToString());
				arrColumnas.Add(entSegProyecto.Fields.descripcion.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecinicio.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecfin.ToString());
				arrColumnas.Add(entSegProyecto.Fields.apiestado.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usucre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.feccre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usumod.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecmod.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_web.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_movil.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_font.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla seg_proyecto
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, entSegProyecto.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegProyecto.Fields.id_proyecto.ToString());
				arrColumnas.Add(entSegProyecto.Fields.nombre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.codigo.ToString());
				arrColumnas.Add(entSegProyecto.Fields.descripcion.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecinicio.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecfin.ToString());
				arrColumnas.Add(entSegProyecto.Fields.apiestado.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usucre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.feccre.ToString());
				arrColumnas.Add(entSegProyecto.Fields.usumod.ToString());
				arrColumnas.Add(entSegProyecto.Fields.fecmod.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_web.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_movil.ToString());
				arrColumnas.Add(entSegProyecto.Fields.color_font.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla seg_proyecto
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, entSegProyecto.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla seg_proyecto
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, entSegProyecto.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla seg_proyecto
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarGridViewOr(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla seg_proyecto
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DbDataReader dsReader = local.cargarDataReaderOr(cParametros.schema + entSegProyecto.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				dtg.DataSource = dsReader;
				dtg.DataBind();

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion 


		#endregion

		#region Funciones Internas

		/// <summary>
		/// 	 Funcion que devuelve un objeto a partir de un DataRow
		/// </summary>
		/// <param name="row" type="System.Data.DataRow">
		///     <para>
		/// 		 DataRow con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Objeto seg_proyecto
		/// </returns>
		internal entSegProyecto crearObjeto(DataRow row)
		{
			entSegProyecto obj = new entSegProyecto();
			obj.id_proyecto = GetColumnType(row[entSegProyecto.Fields.id_proyecto.ToString()], entSegProyecto.Fields.id_proyecto);
			obj.nombre = GetColumnType(row[entSegProyecto.Fields.nombre.ToString()], entSegProyecto.Fields.nombre);
			obj.codigo = GetColumnType(row[entSegProyecto.Fields.codigo.ToString()], entSegProyecto.Fields.codigo);
			obj.descripcion = GetColumnType(row[entSegProyecto.Fields.descripcion.ToString()], entSegProyecto.Fields.descripcion);
			obj.fecinicio = GetColumnType(row[entSegProyecto.Fields.fecinicio.ToString()], entSegProyecto.Fields.fecinicio);
			obj.fecfin = GetColumnType(row[entSegProyecto.Fields.fecfin.ToString()], entSegProyecto.Fields.fecfin);
			obj.apiestado = GetColumnType(row[entSegProyecto.Fields.apiestado.ToString()], entSegProyecto.Fields.apiestado);
			obj.usucre = GetColumnType(row[entSegProyecto.Fields.usucre.ToString()], entSegProyecto.Fields.usucre);
			obj.feccre = GetColumnType(row[entSegProyecto.Fields.feccre.ToString()], entSegProyecto.Fields.feccre);
			obj.usumod = GetColumnType(row[entSegProyecto.Fields.usumod.ToString()], entSegProyecto.Fields.usumod);
			obj.fecmod = GetColumnType(row[entSegProyecto.Fields.fecmod.ToString()], entSegProyecto.Fields.fecmod);
			obj.color_web = GetColumnType(row[entSegProyecto.Fields.color_web.ToString()], entSegProyecto.Fields.color_web);
			obj.color_movil = GetColumnType(row[entSegProyecto.Fields.color_movil.ToString()], entSegProyecto.Fields.color_movil);
			obj.color_font = GetColumnType(row[entSegProyecto.Fields.color_font.ToString()], entSegProyecto.Fields.color_font);
			return obj;
		}

		/// <summary>
		/// 	 Funcion que devuelve un objeto a partir de un DataRow
		/// </summary>
		/// <param name="row" type="System.Data.DataRow">
		///     <para>
		/// 		 DataRow con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Objeto seg_proyecto
		/// </returns>
		internal entSegProyecto crearObjetoRevisado(DataRow row)
		{
			entSegProyecto obj = new entSegProyecto();
			if (row.Table.Columns.Contains(entSegProyecto.Fields.id_proyecto.ToString()))
				obj.id_proyecto = GetColumnType(row[entSegProyecto.Fields.id_proyecto.ToString()], entSegProyecto.Fields.id_proyecto);
			if (row.Table.Columns.Contains(entSegProyecto.Fields.nombre.ToString()))
				obj.nombre = GetColumnType(row[entSegProyecto.Fields.nombre.ToString()], entSegProyecto.Fields.nombre);
			if (row.Table.Columns.Contains(entSegProyecto.Fields.codigo.ToString()))
				obj.codigo = GetColumnType(row[entSegProyecto.Fields.codigo.ToString()], entSegProyecto.Fields.codigo);
			if (row.Table.Columns.Contains(entSegProyecto.Fields.descripcion.ToString()))
				obj.descripcion = GetColumnType(row[entSegProyecto.Fields.descripcion.ToString()], entSegProyecto.Fields.descripcion);
			if (row.Table.Columns.Contains(entSegProyecto.Fields.fecinicio.ToString()))
				obj.fecinicio = GetColumnType(row[entSegProyecto.Fields.fecinicio.ToString()], entSegProyecto.Fields.fecinicio);
			if (row.Table.Columns.Contains(entSegProyecto.Fields.fecfin.ToString()))
				obj.fecfin = GetColumnType(row[entSegProyecto.Fields.fecfin.ToString()], entSegProyecto.Fields.fecfin);
			if (row.Table.Columns.Contains(entSegProyecto.Fields.apiestado.ToString()))
				obj.apiestado = GetColumnType(row[entSegProyecto.Fields.apiestado.ToString()], entSegProyecto.Fields.apiestado);
			if (row.Table.Columns.Contains(entSegProyecto.Fields.usucre.ToString()))
				obj.usucre = GetColumnType(row[entSegProyecto.Fields.usucre.ToString()], entSegProyecto.Fields.usucre);
			if (row.Table.Columns.Contains(entSegProyecto.Fields.feccre.ToString()))
				obj.feccre = GetColumnType(row[entSegProyecto.Fields.feccre.ToString()], entSegProyecto.Fields.feccre);
			if (row.Table.Columns.Contains(entSegProyecto.Fields.usumod.ToString()))
				obj.usumod = GetColumnType(row[entSegProyecto.Fields.usumod.ToString()], entSegProyecto.Fields.usumod);
			if (row.Table.Columns.Contains(entSegProyecto.Fields.fecmod.ToString()))
				obj.fecmod = GetColumnType(row[entSegProyecto.Fields.fecmod.ToString()], entSegProyecto.Fields.fecmod);
			if (row.Table.Columns.Contains(entSegProyecto.Fields.color_web.ToString()))
				obj.color_web = GetColumnType(row[entSegProyecto.Fields.color_web.ToString()], entSegProyecto.Fields.color_web);
			if (row.Table.Columns.Contains(entSegProyecto.Fields.color_movil.ToString()))
				obj.color_movil = GetColumnType(row[entSegProyecto.Fields.color_movil.ToString()], entSegProyecto.Fields.color_movil);
			if (row.Table.Columns.Contains(entSegProyecto.Fields.color_font.ToString()))
				obj.color_font = GetColumnType(row[entSegProyecto.Fields.color_font.ToString()], entSegProyecto.Fields.color_font);
			return obj;
		}

		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtseg_proyecto" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Lista de Objetos seg_proyecto
		/// </returns>
		internal List<entSegProyecto> crearLista(DataTable dtseg_proyecto)
		{
			List<entSegProyecto> list = new List<entSegProyecto>();
			
			entSegProyecto obj = new entSegProyecto();
			foreach (DataRow row in dtseg_proyecto.Rows)
			{
				obj = crearObjeto(row);
				list.Add(obj);
			}
			return list;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable y con solo algunas columnas
		/// </summary>
		/// <param name="dtseg_proyecto" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Lista de Objetos seg_proyecto
		/// </returns>
		internal List<entSegProyecto> crearListaRevisada(DataTable dtseg_proyecto)
		{
			List<entSegProyecto> list = new List<entSegProyecto>();
			
			entSegProyecto obj = new entSegProyecto();
			foreach (DataRow row in dtseg_proyecto.Rows)
			{
				obj = crearObjetoRevisado(row);
				list.Add(obj);
			}
			return list;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtseg_proyecto" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cola de Objetos seg_proyecto
		/// </returns>
		internal Queue<entSegProyecto> crearCola(DataTable dtseg_proyecto)
		{
			Queue<entSegProyecto> cola = new Queue<entSegProyecto>();
			
			entSegProyecto obj = new entSegProyecto();
			foreach (DataRow row in dtseg_proyecto.Rows)
			{
				obj = crearObjeto(row);
				cola.Enqueue(obj);
			}
			return cola;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtseg_proyecto" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Pila de Objetos seg_proyecto
		/// </returns>
		internal Stack<entSegProyecto> crearPila(DataTable dtseg_proyecto)
		{
			Stack<entSegProyecto> pila = new Stack<entSegProyecto>();
			
			entSegProyecto obj = new entSegProyecto();
			foreach (DataRow row in dtseg_proyecto.Rows)
			{
				obj = crearObjeto(row);
				pila.Push(obj);
			}
			return pila;
		}
		
		/// <summary>
		/// 	 Funcion que crea un Dicionario a partir de un DataTable
		/// </summary>
		/// <param name="dtseg_proyecto" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Diccionario de Objetos seg_proyecto
		/// </returns>
		internal Dictionary<String, entSegProyecto> crearDiccionario(DataTable dtseg_proyecto)
		{
			Dictionary<String, entSegProyecto>  miDic = new Dictionary<String, entSegProyecto>();
			
			entSegProyecto obj = new entSegProyecto();
			foreach (DataRow row in dtseg_proyecto.Rows)
			{
				obj = crearObjeto(row);
				miDic.Add(obj.id_proyecto.ToString(), obj);
			}
			return miDic;
		}
		
		/// <summary>
		/// 	 Funcion que crea un Dicionario a partir de un DataTable
		/// </summary>
		/// <param name="dtseg_proyecto" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 HashTable de Objetos seg_proyecto
		/// </returns>
		internal Hashtable crearHashTable(DataTable dtseg_proyecto)
		{
			Hashtable miTabla = new Hashtable();
			
			entSegProyecto obj = new entSegProyecto();
			foreach (DataRow row in dtseg_proyecto.Rows)
			{
				obj = crearObjeto(row);
				miTabla.Add(obj.id_proyecto.ToString(), obj);
			}
			return miTabla;
		}
		
		/// <summary>
		/// 	 Funcion que crea un Dicionario a partir de un DataTable y solo con columnas existentes
		/// </summary>
		/// <param name="dtseg_proyecto" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Diccionario de Objetos seg_proyecto
		/// </returns>
		internal Dictionary<String, entSegProyecto> crearDiccionarioRevisado(DataTable dtseg_proyecto)
		{
			Dictionary<String, entSegProyecto>  miDic = new Dictionary<String, entSegProyecto>();
			
			entSegProyecto obj = new entSegProyecto();
			foreach (DataRow row in dtseg_proyecto.Rows)
			{
				obj = crearObjetoRevisado(row);
				miDic.Add(obj.id_proyecto.ToString(), obj);
			}
			return miDic;
		}
		
		public void Dispose()
		{
			GC.SuppressFinalize(this);
		}
		
		protected void Finalize()
		{
			Dispose();
		}
		#endregion

	}
}

