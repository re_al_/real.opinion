#region 
/***********************************************************************************************************
	NOMBRE:       rnCatTipopregunta
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla cat_tipo_pregunta

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using ReAl.Opinion.Dal;
using ReAl.Opinion.PgConn;
using ReAl.Opinion.Dal.Entidades;
using ReAl.Opinion.Dal.Interface;
using System.Windows.Forms;
#endregion

namespace ReAl.Opinion.Dal.Modelo
{
	public partial class rnCatTipopregunta: inCatTipopregunta
	{
		//Debe implementar la Interface (Alt + Shift + F10)

		#region inCatTipopregunta Members

		#region Reflection

		/// <summary>
		/// Metodo que devuelve el Script SQL de la Tabla
		/// </summary>
		/// <returns>Script SQL</returns>
		public string getTableScript()
		{
			TableClass tabla = new TableClass(typeof(entCatTipopregunta));
			return tabla.CreateTableScript();
		}
		
		/// <summary>
		/// Metodo para castear Dinamicamente un Tipo
		/// <summary>
		/// <param name="valor">Tipo a ser casteado</param>
		/// <param name="myField">Enum de la columna</param>
		/// <returns>Devuelve un objeto del Tipo de la columna especificada en el Enum</returns>
		public dynamic GetColumnType(object valor, entCatTipopregunta.Fields myField)
		{
			if (DBNull.Value.Equals(valor)) 
				return null;
			Type destino = typeof(entCatTipopregunta).GetProperty(myField.ToString()).PropertyType;
			var miTipo = Nullable.GetUnderlyingType(destino) ?? destino;
			return Convert.ChangeType(valor, miTipo);
		}

		/// <summary>
		/// 	 Funcion que obtiene la llave primaria unica de la tabla cat_tipo_pregunta a partir de una cadena
		/// </summary>
		/// <param name="args" type="string[]">
		///     <para>
		/// 		 Cadena desde la que se construye el identificador unico de la tabla cat_tipo_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Identificador unico de la tabla cat_tipo_pregunta
		/// </returns>
		public string CreatePK(string[] args)
		{
			return args[0];
		}
		
		#endregion 

		#region ObtenerObjeto

		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo entCatTipopregunta que cumple con los filtros de los parametros
		/// </returns>
		public entCatTipopregunta ObtenerObjeto(int intid_tipo_pregunta)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(entCatTipopregunta.Fields.id_tipo_pregunta.ToString());
		
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(intid_tipo_pregunta);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entCatTipopregunta que cumple con los filtros de los parametros
		/// </returns>
		public entCatTipopregunta ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entCatTipopregunta que cumple con los filtros de los parametros
		/// </returns>
		public entCatTipopregunta ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entCatTipopregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.descripcion.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.respuesta_valor.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usucre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.feccre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usumod.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.fecmod.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.codigo_activo.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.exportar_codigo.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entCatTipopregunta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count == 1)
				{
					entCatTipopregunta obj = new entCatTipopregunta();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entCatTipopregunta que cumple con los filtros de los parametros
		/// </returns>
		public entCatTipopregunta ObtenerObjeto(Hashtable htbFiltro)
		{
			return ObtenerObjeto(htbFiltro, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entCatTipopregunta que cumple con los filtros de los parametros
		/// </returns>
		public entCatTipopregunta ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entCatTipopregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.descripcion.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.respuesta_valor.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usucre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.feccre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usumod.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.fecmod.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.codigo_activo.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.exportar_codigo.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entCatTipopregunta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count == 1)
				{
					entCatTipopregunta obj = new entCatTipopregunta();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entCatTipopregunta que cumple con los filtros de los parametros
		/// </returns>
		public entCatTipopregunta ObtenerObjeto(entCatTipopregunta.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entCatTipopregunta que cumple con los filtros de los parametros
		/// </returns>
		public entCatTipopregunta ObtenerObjeto(entCatTipopregunta.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un Business Object del Tipo entCatTipopregunta a partir de su llave promaria
		/// </summary>
		/// <returns>
		/// 	Objeto del Tipo entCatTipopregunta
		/// </returns>
		public entCatTipopregunta ObtenerObjeto(int intid_tipo_pregunta, ref cTrans localTrans )
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(entCatTipopregunta.Fields.id_tipo_pregunta.ToString());
		
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(intid_tipo_pregunta);
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion cat_tipo_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entCatTipopregunta que cumple con los filtros de los parametros
		/// </returns>
		public entCatTipopregunta ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion cat_tipo_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entCatTipopregunta que cumple con los filtros de los parametros
		/// </returns>
		public entCatTipopregunta ObtenerObjeto(Hashtable htbFiltro, ref cTrans localTrans)
		{
			return ObtenerObjeto(htbFiltro, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion cat_tipo_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entCatTipopregunta que cumple con los filtros de los parametros
		/// </returns>
		public entCatTipopregunta ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales,  ref cTrans localTrans)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion cat_tipo_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entCatTipopregunta que cumple con los filtros de los parametros
		/// </returns>
		public entCatTipopregunta ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entCatTipopregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.descripcion.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.respuesta_valor.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usucre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.feccre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usumod.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.fecmod.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.codigo_activo.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.exportar_codigo.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entCatTipopregunta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count == 1)
				{
					entCatTipopregunta obj = new entCatTipopregunta();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion cat_tipo_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entCatTipopregunta que cumple con los filtros de los parametros
		/// </returns>
		public entCatTipopregunta ObtenerObjeto(entCatTipopregunta.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion cat_tipo_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entCatTipopregunta que cumple con los filtros de los parametros
		/// </returns>
		public entCatTipopregunta ObtenerObjeto(entCatTipopregunta.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion

		#region ObtenerLista

		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entCatTipopregunta> ObtenerLista()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entCatTipopregunta> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entCatTipopregunta> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entCatTipopregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.descripcion.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.respuesta_valor.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usucre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.feccre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usumod.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.fecmod.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.codigo_activo.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.exportar_codigo.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entCatTipopregunta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<entCatTipopregunta>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entCatTipopregunta> ObtenerLista(entCatTipopregunta.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entCatTipopregunta> ObtenerLista(entCatTipopregunta.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion cat_tipo_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entCatTipopregunta> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion cat_tipo_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entCatTipopregunta> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entCatTipopregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.descripcion.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.respuesta_valor.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usucre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.feccre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usumod.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.fecmod.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.codigo_activo.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.exportar_codigo.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entCatTipopregunta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<entCatTipopregunta>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion cat_tipo_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entCatTipopregunta> ObtenerLista(entCatTipopregunta.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion cat_tipo_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entCatTipopregunta> ObtenerLista(entCatTipopregunta.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entCatTipopregunta> ObtenerLista(Hashtable htbFiltro)
		{
			return ObtenerLista(htbFiltro, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entCatTipopregunta> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entCatTipopregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.descripcion.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.respuesta_valor.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usucre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.feccre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usumod.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.fecmod.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.codigo_activo.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.exportar_codigo.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entCatTipopregunta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<entCatTipopregunta>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion cat_tipo_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entCatTipopregunta> ObtenerLista(Hashtable htbFiltro, ref cTrans localTrans)
		{
			return ObtenerLista(htbFiltro, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion cat_tipo_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entCatTipopregunta> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entCatTipopregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.descripcion.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.respuesta_valor.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usucre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.feccre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usumod.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.fecmod.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.codigo_activo.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.exportar_codigo.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entCatTipopregunta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<entCatTipopregunta>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entCatTipopregunta> ObtenerListaDesdeVista(String strVista)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entCatTipopregunta> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entCatTipopregunta> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<entCatTipopregunta>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entCatTipopregunta> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro)
		{
			return ObtenerListaDesdeVista(strVista, htbFiltro, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entCatTipopregunta> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<entCatTipopregunta>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entCatTipopregunta> ObtenerListaDesdeVista(String strVista, entCatTipopregunta.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entCatTipopregunta> ObtenerListaDesdeVista(String strVista, entCatTipopregunta.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion cat_tipo_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entCatTipopregunta> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion cat_tipo_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entCatTipopregunta> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<entCatTipopregunta>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion cat_tipo_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entCatTipopregunta> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref cTrans localTrans)
		{
			return ObtenerListaDesdeVista(strVista, htbFiltro, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion cat_tipo_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entCatTipopregunta> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<entCatTipopregunta>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion cat_tipo_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entCatTipopregunta> ObtenerListaDesdeVista(String strVista, entCatTipopregunta.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion cat_tipo_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entCatTipopregunta> ObtenerListaDesdeVista(String strVista, entCatTipopregunta.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion 

		#region ObtenerCola y Obtener Pila

		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entCatTipopregunta> ObtenerCola()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entCatTipopregunta> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entCatTipopregunta> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entCatTipopregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.descripcion.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.respuesta_valor.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usucre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.feccre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usumod.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.fecmod.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.codigo_activo.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.exportar_codigo.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entCatTipopregunta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearCola(table);
				}
				else
					return new Queue<entCatTipopregunta>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entCatTipopregunta> ObtenerCola(entCatTipopregunta.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entCatTipopregunta> ObtenerCola(entCatTipopregunta.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion cat_tipo_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entCatTipopregunta> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion cat_tipo_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entCatTipopregunta> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entCatTipopregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.descripcion.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.respuesta_valor.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usucre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.feccre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usumod.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.fecmod.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.codigo_activo.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.exportar_codigo.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entCatTipopregunta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearCola(table);
				}
				else
					return new Queue<entCatTipopregunta>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion cat_tipo_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entCatTipopregunta> ObtenerCola(entCatTipopregunta.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion cat_tipo_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entCatTipopregunta> ObtenerCola(entCatTipopregunta.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entCatTipopregunta> ObtenerPila()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entCatTipopregunta> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entCatTipopregunta> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entCatTipopregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.descripcion.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.respuesta_valor.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usucre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.feccre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usumod.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.fecmod.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.codigo_activo.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.exportar_codigo.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entCatTipopregunta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearPila(table);
				}
				else
					return new Stack<entCatTipopregunta>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entCatTipopregunta> ObtenerPila(entCatTipopregunta.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entCatTipopregunta> ObtenerPila(entCatTipopregunta.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion cat_tipo_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entCatTipopregunta> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion cat_tipo_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entCatTipopregunta> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entCatTipopregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.descripcion.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.respuesta_valor.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usucre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.feccre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usumod.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.fecmod.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.codigo_activo.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.exportar_codigo.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entCatTipopregunta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearPila(table);
				}
				else
					return new Stack<entCatTipopregunta>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion cat_tipo_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entCatTipopregunta> ObtenerPila(entCatTipopregunta.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion cat_tipo_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entCatTipopregunta> ObtenerPila(entCatTipopregunta.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion 

		#region ObtenerDataTable

		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla cat_tipo_pregunta
		/// </summary>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de cat_tipo_pregunta
		/// </returns>
		public DataTable NuevoDataTable()
		{
			try
			{
				DataTable table = new DataTable ();
				DataColumn dc;
				dc = new DataColumn(entCatTipopregunta.Fields.id_tipo_pregunta.ToString(),typeof(entCatTipopregunta).GetProperty(entCatTipopregunta.Fields.id_tipo_pregunta.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entCatTipopregunta.Fields.tipo_pregunta.ToString(),typeof(entCatTipopregunta).GetProperty(entCatTipopregunta.Fields.tipo_pregunta.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entCatTipopregunta.Fields.descripcion.ToString(),typeof(entCatTipopregunta).GetProperty(entCatTipopregunta.Fields.descripcion.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entCatTipopregunta.Fields.respuesta_valor.ToString(),typeof(entCatTipopregunta).GetProperty(entCatTipopregunta.Fields.respuesta_valor.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entCatTipopregunta.Fields.apiestado.ToString(),typeof(entCatTipopregunta).GetProperty(entCatTipopregunta.Fields.apiestado.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entCatTipopregunta.Fields.usucre.ToString(),typeof(entCatTipopregunta).GetProperty(entCatTipopregunta.Fields.usucre.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entCatTipopregunta.Fields.feccre.ToString(),typeof(entCatTipopregunta).GetProperty(entCatTipopregunta.Fields.feccre.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entCatTipopregunta.Fields.usumod.ToString(),typeof(entCatTipopregunta).GetProperty(entCatTipopregunta.Fields.usumod.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entCatTipopregunta.Fields.fecmod.ToString(),typeof(entCatTipopregunta).GetProperty(entCatTipopregunta.Fields.fecmod.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entCatTipopregunta.Fields.codigo_activo.ToString(),typeof(entCatTipopregunta).GetProperty(entCatTipopregunta.Fields.codigo_activo.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entCatTipopregunta.Fields.exportar_codigo.ToString(),typeof(entCatTipopregunta).GetProperty(entCatTipopregunta.Fields.exportar_codigo.ToString()).PropertyType);
				table.Columns.Add(dc);

				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que genera un DataTable con determinadas columnas de una cat_tipo_pregunta
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de cat_tipo_pregunta
		/// </returns>
		public DataTable NuevoDataTable(ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'2'");
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entCatTipopregunta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla cat_tipo_pregunta
		/// </summary>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de cat_tipo_pregunta
		/// </returns>
		public DataTable ObtenerDataTable()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDataTable(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla y n condicion WHERE cat_tipo_pregunta
		/// </summary>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de cat_tipo_pregunta
		/// </returns>
		public DataTable ObtenerDataTable(String strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla cat_tipo_pregunta
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de cat_tipo_pregunta
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla cat_tipo_pregunta
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de cat_tipo_pregunta
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla cat_tipo_pregunta
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de cat_tipo_pregunta
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla cat_tipo_pregunta
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de cat_tipo_pregunta
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro)
		{
			try
			{
				return ObtenerDataTable(arrColumnas, htbFiltro, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla cat_tipo_pregunta
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de cat_tipo_pregunta
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla cat_tipo_pregunta
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de cat_tipo_pregunta
		/// </returns>
		public DataTable ObtenerDataTable(Hashtable htbFiltro)
		{
			try
			{
				return ObtenerDataTable(htbFiltro, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla cat_tipo_pregunta
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de cat_tipo_pregunta
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entCatTipopregunta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla cat_tipo_pregunta
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de cat_tipo_pregunta
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entCatTipopregunta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla cat_tipo_pregunta
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de cat_tipo_pregunta
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entCatTipopregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.descripcion.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.respuesta_valor.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usucre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.feccre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usumod.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.fecmod.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.codigo_activo.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.exportar_codigo.ToString());
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla cat_tipo_pregunta
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de cat_tipo_pregunta
		/// </returns>
		public DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entCatTipopregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.descripcion.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.respuesta_valor.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usucre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.feccre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usumod.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.fecmod.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.codigo_activo.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.exportar_codigo.ToString());
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla cat_tipo_pregunta
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de cat_tipo_pregunta
		/// </returns>
		public DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entCatTipopregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.descripcion.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.respuesta_valor.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usucre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.feccre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usumod.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.fecmod.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.codigo_activo.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.exportar_codigo.ToString());
				return ObtenerDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla cat_tipo_pregunta
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de cat_tipo_pregunta
		/// </returns>
		public DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return ObtenerDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla cat_tipo_pregunta
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de cat_tipo_pregunta
		/// </returns>
		public DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DataTable table = local.cargarDataTableOr(cParametros.schema + entCatTipopregunta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="searchField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(entCatTipopregunta.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="searchField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(entCatTipopregunta.Fields searchField, object searchValue, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, entCatTipopregunta.Fields searchField, object searchValue, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, entCatTipopregunta.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		

		#endregion 

		#region ObtenerDiccionario

		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entCatTipopregunta> ObtenerDiccionario()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entCatTipopregunta> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entCatTipopregunta> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entCatTipopregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.descripcion.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.respuesta_valor.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usucre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.feccre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usumod.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.fecmod.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.codigo_activo.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.exportar_codigo.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entCatTipopregunta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearDiccionario(table);
				}
				else
					return new Dictionary<string, entCatTipopregunta>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entCatTipopregunta> ObtenerDiccionario(entCatTipopregunta.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entCatTipopregunta> ObtenerDiccionario(entCatTipopregunta.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion cat_tipo_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entCatTipopregunta> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion cat_tipo_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entCatTipopregunta> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entCatTipopregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.descripcion.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.respuesta_valor.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usucre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.feccre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usumod.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.fecmod.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.codigo_activo.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.exportar_codigo.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entCatTipopregunta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearDiccionario(table);
				}
				else
					return new Dictionary<string, entCatTipopregunta>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion cat_tipo_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entCatTipopregunta> ObtenerDiccionario(entCatTipopregunta.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entCatTipopregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion cat_tipo_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entCatTipopregunta> ObtenerDiccionario(entCatTipopregunta.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion 

		#region FuncionesAgregadas

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entCatTipopregunta que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesCount(entCatTipopregunta.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entCatTipopregunta que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesCount(entCatTipopregunta.Fields refField, entCatTipopregunta.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entCatTipopregunta que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesCount(entCatTipopregunta.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("count(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entCatTipopregunta que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMin(entCatTipopregunta.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entCatTipopregunta que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMin(entCatTipopregunta.Fields refField, entCatTipopregunta.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entCatTipopregunta que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMin(entCatTipopregunta.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("min(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entCatTipopregunta que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMax(entCatTipopregunta.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entCatTipopregunta que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMax(entCatTipopregunta.Fields refField, entCatTipopregunta.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entCatTipopregunta que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMax(entCatTipopregunta.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("max(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entCatTipopregunta que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesSum(entCatTipopregunta.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entCatTipopregunta que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesSum(entCatTipopregunta.Fields refField, entCatTipopregunta.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entCatTipopregunta que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesSum(entCatTipopregunta.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("sum(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entCatTipopregunta que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesAvg(entCatTipopregunta.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entCatTipopregunta que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesAvg(entCatTipopregunta.Fields refField, entCatTipopregunta.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entCatTipopregunta que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesAvg(entCatTipopregunta.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("avg(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion 

		#region ABMs

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla cat_tipo_pregunta a partir de una clase del tipo Ecat_tipo_pregunta
		/// </summary>
		/// <param name="obj" type="Entidades.entCatTipopregunta">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla cat_tipo_preguntacat_tipo_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacioncat_tipo_pregunta
		/// </returns>
		public bool Insert(entCatTipopregunta obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(entCatTipopregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.descripcion.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.respuesta_valor.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usucre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.feccre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.codigo_activo.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.exportar_codigo.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.id_tipo_pregunta);
				arrValores.Add(obj.tipo_pregunta == null ? null : "'" + obj.tipo_pregunta + "'");
				arrValores.Add(obj.descripcion == null ? null : "'" + obj.descripcion + "'");
				arrValores.Add(obj.respuesta_valor == null ? null : "'" + obj.respuesta_valor + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.codigo_activo == null ? null : "'" + obj.codigo_activo + "'");
				arrValores.Add(obj.exportar_codigo);

			
				cConn local = new cConn();
				return local.insertBD(entCatTipopregunta.strNombreTabla, arrColumnas, arrValores);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla cat_tipo_pregunta a partir de una clase del tipo Ecat_tipo_pregunta
		/// </summary>
		/// <param name="obj" type="Entidades.entCatTipopregunta">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla cat_tipo_preguntacat_tipo_pregunta
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion cat_tipo_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacioncat_tipo_pregunta
		/// </returns>
		public bool Insert(entCatTipopregunta obj, ref cTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(entCatTipopregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.descripcion.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.respuesta_valor.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usucre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.feccre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.codigo_activo.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.exportar_codigo.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.id_tipo_pregunta);
				arrValores.Add(obj.tipo_pregunta == null ? null : "'" + obj.tipo_pregunta + "'");
				arrValores.Add(obj.descripcion == null ? null : "'" + obj.descripcion + "'");
				arrValores.Add(obj.respuesta_valor == null ? null : "'" + obj.respuesta_valor + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.codigo_activo == null ? null : "'" + obj.codigo_activo + "'");
				arrValores.Add(obj.exportar_codigo);

			
				cConn local = new cConn();
				return local.insertBD(entCatTipopregunta.strNombreTabla, arrColumnas, arrValores, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla cat_tipo_pregunta a partir de una clase del tipo Ecat_tipo_pregunta
		/// </summary>
		/// <param name="obj" type="Entidades.entCatTipopregunta">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla cat_tipo_preguntacat_tipo_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacioncat_tipo_pregunta
		/// </returns>
		public bool InsertIdentity(entCatTipopregunta obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(entCatTipopregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.descripcion.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.respuesta_valor.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usucre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.feccre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.codigo_activo.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.exportar_codigo.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.id_tipo_pregunta);
				arrValores.Add(obj.tipo_pregunta == null ? null : "'" + obj.tipo_pregunta + "'");
				arrValores.Add(obj.descripcion == null ? null : "'" + obj.descripcion + "'");
				arrValores.Add(obj.respuesta_valor == null ? null : "'" + obj.respuesta_valor + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.codigo_activo == null ? null : "'" + obj.codigo_activo + "'");
				arrValores.Add(obj.exportar_codigo);

			
				cConn local = new cConn();
				int intIdentidad = -1;
				bool res = local.insertBD(entCatTipopregunta.strNombreTabla, arrColumnas, arrValores, ref intIdentidad);
				obj.id_tipo_pregunta = intIdentidad;
				return res;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla cat_tipo_pregunta a partir de una clase del tipo Ecat_tipo_pregunta
		/// </summary>
		/// <param name="obj" type="Entidades.entCatTipopregunta">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla cat_tipo_preguntacat_tipo_pregunta
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion cat_tipo_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacioncat_tipo_pregunta
		/// </returns>
		public bool InsertIdentity(entCatTipopregunta obj, ref cTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(entCatTipopregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.descripcion.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.respuesta_valor.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usucre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.feccre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.codigo_activo.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.exportar_codigo.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.id_tipo_pregunta);
				arrValores.Add(obj.tipo_pregunta == null ? null : "'" + obj.tipo_pregunta + "'");
				arrValores.Add(obj.descripcion == null ? null : "'" + obj.descripcion + "'");
				arrValores.Add(obj.respuesta_valor == null ? null : "'" + obj.respuesta_valor + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.codigo_activo == null ? null : "'" + obj.codigo_activo + "'");
				arrValores.Add(obj.exportar_codigo);

			
				cConn local = new cConn();
				int intIdentidad = -1;
				bool res = local.insertBD(entCatTipopregunta.strNombreTabla, arrColumnas, arrValores, ref intIdentidad, ref localTrans);
				obj.id_tipo_pregunta = intIdentidad;
				return res;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla cat_tipo_pregunta a partir de una clase del tipo Ecat_tipo_pregunta
		/// </summary>
		/// <param name="obj" type="Entidades.entCatTipopregunta">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla cat_tipo_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacioncat_tipo_pregunta
		/// </returns>
		public int Update(entCatTipopregunta obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entCatTipopregunta.Fields.tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.descripcion.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.respuesta_valor.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usucre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.feccre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.codigo_activo.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.exportar_codigo.ToString());
			
				ArrayList arrValores = new ArrayList();
				arrValores.Add(obj.tipo_pregunta == null ? null : "'" + obj.tipo_pregunta + "'");
				arrValores.Add(obj.descripcion == null ? null : "'" + obj.descripcion + "'");
				arrValores.Add(obj.respuesta_valor == null ? null : "'" + obj.respuesta_valor + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.codigo_activo == null ? null : "'" + obj.codigo_activo + "'");
				arrValores.Add(obj.exportar_codigo);

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(entCatTipopregunta.Fields.id_tipo_pregunta.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.id_tipo_pregunta);

			
				cConn local = new cConn();
				return local.updateBD(entCatTipopregunta.strNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla cat_tipo_pregunta a partir de una clase del tipo ecat_tipo_pregunta
		/// </summary>
		/// <param name="obj" type="Entidades.entCatTipopregunta">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla cat_tipo_pregunta
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion cat_tipo_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacion
		/// </returns>
		public int Update(entCatTipopregunta obj, ref cTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entCatTipopregunta.Fields.tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.descripcion.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.respuesta_valor.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usucre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.feccre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.codigo_activo.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.exportar_codigo.ToString());
			
				ArrayList arrValores = new ArrayList();
				arrValores.Add(obj.tipo_pregunta == null ? null : "'" + obj.tipo_pregunta + "'");
				arrValores.Add(obj.descripcion == null ? null : "'" + obj.descripcion + "'");
				arrValores.Add(obj.respuesta_valor == null ? null : "'" + obj.respuesta_valor + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.codigo_activo == null ? null : "'" + obj.codigo_activo + "'");
				arrValores.Add(obj.exportar_codigo);

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(entCatTipopregunta.Fields.id_tipo_pregunta.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.id_tipo_pregunta);

			
				cConn local = new cConn();
				return local.updateBD(entCatTipopregunta.strNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla cat_tipo_pregunta a partir de una clase del tipo entCatTipopregunta y su respectiva PK
		/// </summary>
		/// <param name="obj" type="Entidades.entCatTipopregunta">
		///     <para>
		/// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla cat_tipo_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacioncat_tipo_pregunta
		/// </returns>
		public int Delete(entCatTipopregunta obj)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(entCatTipopregunta.Fields.id_tipo_pregunta.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.id_tipo_pregunta);

			
				cConn local = new cConn();
				return local.deleteBD(entCatTipopregunta.strNombreTabla, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla cat_tipo_pregunta a partir de una clase del tipo entCatTipopregunta y su PK
		/// </summary>
		/// <param name="obj" type="Entidades.ecat_tipo_pregunta">
		///     <para>
		/// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla cat_tipo_pregunta
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion cat_tipo_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacioncat_tipo_pregunta
		/// </returns>
		public int Delete(entCatTipopregunta obj, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(entCatTipopregunta.Fields.id_tipo_pregunta.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.id_tipo_pregunta);

			
				cConn local = new cConn();
				return local.deleteBD(entCatTipopregunta.strNombreTabla, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla cat_tipo_pregunta a partir de una clase del tipo ecat_tipo_pregunta
		/// </summary>
		/// <param name="arrColumnasWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Valores para cada una de las columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacioncat_tipo_pregunta
		/// </returns>
		public int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				cConn local = new cConn();
				return local.deleteBD("cat_tipo_pregunta", arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla cat_tipo_pregunta a partir de una clase del tipo ecat_tipo_pregunta
		/// </summary>
		/// <param name="arrColumnasWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Valores para cada una de las columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion cat_tipo_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacioncat_tipo_pregunta
		/// </returns>
		public int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			try
			{
				cConn local = new cConn();
				return local.deleteBD(entCatTipopregunta.strNombreTabla, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		

		#endregion 

		#region Llenado de elementos

		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla cat_tipo_pregunta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla cat_tipo_preguntacat_tipo_pregunta
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla cat_tipo_pregunta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla cat_tipo_preguntacat_tipo_pregunta
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla cat_tipo_pregunta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla cat_tipo_preguntacat_tipo_pregunta
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla cat_tipo_pregunta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla cat_tipo_preguntacat_tipo_pregunta
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entCatTipopregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.tipo_pregunta.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla cat_tipo_pregunta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla cat_tipo_preguntacat_tipo_pregunta
		///     </para>
		/// </param>
		/// <param name="valueField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entCatTipopregunta.Fields valueField, entCatTipopregunta.Fields textField)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla cat_tipo_pregunta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla cat_tipo_preguntacat_tipo_pregunta
		///     </para>
		/// </param>
		/// <param name="valueField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entCatTipopregunta.Fields valueField, entCatTipopregunta.Fields textField, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla cat_tipo_pregunta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla cat_tipo_preguntacat_tipo_pregunta
		///     </para>
		/// </param>
		/// <param name="valueField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entCatTipopregunta.Fields valueField, String textField)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla cat_tipo_pregunta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla cat_tipo_preguntacat_tipo_pregunta
		///     </para>
		/// </param>
		/// <param name="valueField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entCatTipopregunta.Fields valueField, String textField, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla cat_tipo_pregunta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla cat_tipo_preguntacat_tipo_pregunta
		///     </para>
		/// </param>
		/// <param name="valueField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entCatTipopregunta.Fields valueField, entCatTipopregunta.Fields textField, entCatTipopregunta.Fields searchField, object searchValue)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField.ToString(), searchField, searchValue);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla cat_tipo_pregunta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla cat_tipo_preguntacat_tipo_pregunta
		///     </para>
		/// </param>
		/// <param name="valueField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entCatTipopregunta.Fields valueField, String textField, entCatTipopregunta.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla cat_tipo_pregunta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla cat_tipo_preguntacat_tipo_pregunta
		///     </para>
		/// </param>
		/// <param name="valueField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entCatTipopregunta.Fields valueField, entCatTipopregunta.Fields textField, entCatTipopregunta.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla cat_tipo_pregunta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla cat_tipo_preguntacat_tipo_pregunta
		///     </para>
		/// </param>
		/// <param name="valueField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entCatTipopregunta.Fields valueField, String textField, entCatTipopregunta.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla cat_tipo_pregunta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla cat_tipo_preguntacat_tipo_pregunta
		///     </para>
		/// </param>
		/// <param name="valueField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entCatTipopregunta.Fields valueField, entCatTipopregunta.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla cat_tipo_pregunta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla cat_tipo_preguntacat_tipo_pregunta
		///     </para>
		/// </param>
		/// <param name="valueField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entCatTipopregunta.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla cat_tipo_pregunta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla cat_tipo_preguntacat_tipo_pregunta
		///     </para>
		/// </param>
		/// <param name="valueField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entCatTipopregunta.Fields valueField, entCatTipopregunta.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla cat_tipo_pregunta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla cat_tipo_preguntacat_tipo_pregunta
		///     </para>
		/// </param>
		/// <param name="valueField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entCatTipopregunta.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla cat_tipo_pregunta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla cat_tipo_preguntacat_tipo_pregunta
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entCatTipopregunta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Columns.Count > 0)
				{
					cmb.ValueMember = table.Columns[0].ColumnName;
					cmb.DisplayMember = table.Columns[1].ColumnName;
					cmb.DataSource = table;

				}
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla cat_tipo_pregunta
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entCatTipopregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.descripcion.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.respuesta_valor.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usucre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.feccre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usumod.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.fecmod.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.codigo_activo.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.exportar_codigo.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla cat_tipo_pregunta
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		  Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entCatTipopregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.descripcion.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.respuesta_valor.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usucre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.feccre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usumod.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.fecmod.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.codigo_activo.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.exportar_codigo.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla cat_tipo_pregunta
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla cat_tipo_pregunta
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla cat_tipo_pregunta
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entCatTipopregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.descripcion.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.respuesta_valor.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usucre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.feccre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usumod.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.fecmod.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.codigo_activo.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.exportar_codigo.ToString());
			
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla cat_tipo_pregunta
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entCatTipopregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.descripcion.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.respuesta_valor.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usucre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.feccre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usumod.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.fecmod.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.codigo_activo.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.exportar_codigo.ToString());
			
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla cat_tipo_pregunta
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla cat_tipo_pregunta
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DbDataReader dsReader = local.cargarDataReaderAnd(cParametros.schema + entCatTipopregunta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				dtg.DataSource = dsReader;

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla cat_tipo_pregunta
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, entCatTipopregunta.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entCatTipopregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.descripcion.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.respuesta_valor.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usucre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.feccre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usumod.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.fecmod.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.codigo_activo.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.exportar_codigo.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla cat_tipo_pregunta
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, entCatTipopregunta.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entCatTipopregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.tipo_pregunta.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.descripcion.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.respuesta_valor.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usucre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.feccre.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.usumod.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.fecmod.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.codigo_activo.ToString());
				arrColumnas.Add(entCatTipopregunta.Fields.exportar_codigo.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla cat_tipo_pregunta
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entCatTipopregunta.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla cat_tipo_pregunta
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entCatTipopregunta.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla cat_tipo_pregunta
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarDataGridOr(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla cat_tipo_pregunta
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DbDataReader dsReader = local.cargarDataReaderOr(cParametros.schema + entCatTipopregunta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				dtg.DataSource = dsReader;

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion 


		#endregion

		#region Funciones Internas

		/// <summary>
		/// 	 Funcion que devuelve un objeto a partir de un DataRow
		/// </summary>
		/// <param name="row" type="System.Data.DataRow">
		///     <para>
		/// 		 DataRow con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Objeto cat_tipo_pregunta
		/// </returns>
		internal entCatTipopregunta crearObjeto(DataRow row)
		{
			entCatTipopregunta obj = new entCatTipopregunta();
			obj.id_tipo_pregunta = GetColumnType(row[entCatTipopregunta.Fields.id_tipo_pregunta.ToString()], entCatTipopregunta.Fields.id_tipo_pregunta);
			obj.tipo_pregunta = GetColumnType(row[entCatTipopregunta.Fields.tipo_pregunta.ToString()], entCatTipopregunta.Fields.tipo_pregunta);
			obj.descripcion = GetColumnType(row[entCatTipopregunta.Fields.descripcion.ToString()], entCatTipopregunta.Fields.descripcion);
			obj.respuesta_valor = GetColumnType(row[entCatTipopregunta.Fields.respuesta_valor.ToString()], entCatTipopregunta.Fields.respuesta_valor);
			obj.apiestado = GetColumnType(row[entCatTipopregunta.Fields.apiestado.ToString()], entCatTipopregunta.Fields.apiestado);
			obj.usucre = GetColumnType(row[entCatTipopregunta.Fields.usucre.ToString()], entCatTipopregunta.Fields.usucre);
			obj.feccre = GetColumnType(row[entCatTipopregunta.Fields.feccre.ToString()], entCatTipopregunta.Fields.feccre);
			obj.usumod = GetColumnType(row[entCatTipopregunta.Fields.usumod.ToString()], entCatTipopregunta.Fields.usumod);
			obj.fecmod = GetColumnType(row[entCatTipopregunta.Fields.fecmod.ToString()], entCatTipopregunta.Fields.fecmod);
			obj.codigo_activo = GetColumnType(row[entCatTipopregunta.Fields.codigo_activo.ToString()], entCatTipopregunta.Fields.codigo_activo);
			obj.exportar_codigo = GetColumnType(row[entCatTipopregunta.Fields.exportar_codigo.ToString()], entCatTipopregunta.Fields.exportar_codigo);
			return obj;
		}

		/// <summary>
		/// 	 Funcion que devuelve un objeto a partir de un DataRow
		/// </summary>
		/// <param name="row" type="System.Data.DataRow">
		///     <para>
		/// 		 DataRow con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Objeto cat_tipo_pregunta
		/// </returns>
		internal entCatTipopregunta crearObjetoRevisado(DataRow row)
		{
			entCatTipopregunta obj = new entCatTipopregunta();
			if (row.Table.Columns.Contains(entCatTipopregunta.Fields.id_tipo_pregunta.ToString()))
				obj.id_tipo_pregunta = GetColumnType(row[entCatTipopregunta.Fields.id_tipo_pregunta.ToString()], entCatTipopregunta.Fields.id_tipo_pregunta);
			if (row.Table.Columns.Contains(entCatTipopregunta.Fields.tipo_pregunta.ToString()))
				obj.tipo_pregunta = GetColumnType(row[entCatTipopregunta.Fields.tipo_pregunta.ToString()], entCatTipopregunta.Fields.tipo_pregunta);
			if (row.Table.Columns.Contains(entCatTipopregunta.Fields.descripcion.ToString()))
				obj.descripcion = GetColumnType(row[entCatTipopregunta.Fields.descripcion.ToString()], entCatTipopregunta.Fields.descripcion);
			if (row.Table.Columns.Contains(entCatTipopregunta.Fields.respuesta_valor.ToString()))
				obj.respuesta_valor = GetColumnType(row[entCatTipopregunta.Fields.respuesta_valor.ToString()], entCatTipopregunta.Fields.respuesta_valor);
			if (row.Table.Columns.Contains(entCatTipopregunta.Fields.apiestado.ToString()))
				obj.apiestado = GetColumnType(row[entCatTipopregunta.Fields.apiestado.ToString()], entCatTipopregunta.Fields.apiestado);
			if (row.Table.Columns.Contains(entCatTipopregunta.Fields.usucre.ToString()))
				obj.usucre = GetColumnType(row[entCatTipopregunta.Fields.usucre.ToString()], entCatTipopregunta.Fields.usucre);
			if (row.Table.Columns.Contains(entCatTipopregunta.Fields.feccre.ToString()))
				obj.feccre = GetColumnType(row[entCatTipopregunta.Fields.feccre.ToString()], entCatTipopregunta.Fields.feccre);
			if (row.Table.Columns.Contains(entCatTipopregunta.Fields.usumod.ToString()))
				obj.usumod = GetColumnType(row[entCatTipopregunta.Fields.usumod.ToString()], entCatTipopregunta.Fields.usumod);
			if (row.Table.Columns.Contains(entCatTipopregunta.Fields.fecmod.ToString()))
				obj.fecmod = GetColumnType(row[entCatTipopregunta.Fields.fecmod.ToString()], entCatTipopregunta.Fields.fecmod);
			if (row.Table.Columns.Contains(entCatTipopregunta.Fields.codigo_activo.ToString()))
				obj.codigo_activo = GetColumnType(row[entCatTipopregunta.Fields.codigo_activo.ToString()], entCatTipopregunta.Fields.codigo_activo);
			if (row.Table.Columns.Contains(entCatTipopregunta.Fields.exportar_codigo.ToString()))
				obj.exportar_codigo = GetColumnType(row[entCatTipopregunta.Fields.exportar_codigo.ToString()], entCatTipopregunta.Fields.exportar_codigo);
			return obj;
		}

		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtcat_tipo_pregunta" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Lista de Objetos cat_tipo_pregunta
		/// </returns>
		internal List<entCatTipopregunta> crearLista(DataTable dtcat_tipo_pregunta)
		{
			List<entCatTipopregunta> list = new List<entCatTipopregunta>();
			
			entCatTipopregunta obj = new entCatTipopregunta();
			foreach (DataRow row in dtcat_tipo_pregunta.Rows)
			{
				obj = crearObjeto(row);
				list.Add(obj);
			}
			return list;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable y con solo algunas columnas
		/// </summary>
		/// <param name="dtcat_tipo_pregunta" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Lista de Objetos cat_tipo_pregunta
		/// </returns>
		internal List<entCatTipopregunta> crearListaRevisada(DataTable dtcat_tipo_pregunta)
		{
			List<entCatTipopregunta> list = new List<entCatTipopregunta>();
			
			entCatTipopregunta obj = new entCatTipopregunta();
			foreach (DataRow row in dtcat_tipo_pregunta.Rows)
			{
				obj = crearObjetoRevisado(row);
				list.Add(obj);
			}
			return list;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtcat_tipo_pregunta" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cola de Objetos cat_tipo_pregunta
		/// </returns>
		internal Queue<entCatTipopregunta> crearCola(DataTable dtcat_tipo_pregunta)
		{
			Queue<entCatTipopregunta> cola = new Queue<entCatTipopregunta>();
			
			entCatTipopregunta obj = new entCatTipopregunta();
			foreach (DataRow row in dtcat_tipo_pregunta.Rows)
			{
				obj = crearObjeto(row);
				cola.Enqueue(obj);
			}
			return cola;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtcat_tipo_pregunta" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Pila de Objetos cat_tipo_pregunta
		/// </returns>
		internal Stack<entCatTipopregunta> crearPila(DataTable dtcat_tipo_pregunta)
		{
			Stack<entCatTipopregunta> pila = new Stack<entCatTipopregunta>();
			
			entCatTipopregunta obj = new entCatTipopregunta();
			foreach (DataRow row in dtcat_tipo_pregunta.Rows)
			{
				obj = crearObjeto(row);
				pila.Push(obj);
			}
			return pila;
		}
		
		/// <summary>
		/// 	 Funcion que crea un Dicionario a partir de un DataTable
		/// </summary>
		/// <param name="dtcat_tipo_pregunta" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Diccionario de Objetos cat_tipo_pregunta
		/// </returns>
		internal Dictionary<String, entCatTipopregunta> crearDiccionario(DataTable dtcat_tipo_pregunta)
		{
			Dictionary<String, entCatTipopregunta>  miDic = new Dictionary<String, entCatTipopregunta>();
			
			entCatTipopregunta obj = new entCatTipopregunta();
			foreach (DataRow row in dtcat_tipo_pregunta.Rows)
			{
				obj = crearObjeto(row);
				miDic.Add(obj.id_tipo_pregunta.ToString(), obj);
			}
			return miDic;
		}
		
		/// <summary>
		/// 	 Funcion que crea un Dicionario a partir de un DataTable
		/// </summary>
		/// <param name="dtcat_tipo_pregunta" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 HashTable de Objetos cat_tipo_pregunta
		/// </returns>
		internal Hashtable crearHashTable(DataTable dtcat_tipo_pregunta)
		{
			Hashtable miTabla = new Hashtable();
			
			entCatTipopregunta obj = new entCatTipopregunta();
			foreach (DataRow row in dtcat_tipo_pregunta.Rows)
			{
				obj = crearObjeto(row);
				miTabla.Add(obj.id_tipo_pregunta.ToString(), obj);
			}
			return miTabla;
		}
		
		/// <summary>
		/// 	 Funcion que crea un Dicionario a partir de un DataTable y solo con columnas existentes
		/// </summary>
		/// <param name="dtcat_tipo_pregunta" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Diccionario de Objetos cat_tipo_pregunta
		/// </returns>
		internal Dictionary<String, entCatTipopregunta> crearDiccionarioRevisado(DataTable dtcat_tipo_pregunta)
		{
			Dictionary<String, entCatTipopregunta>  miDic = new Dictionary<String, entCatTipopregunta>();
			
			entCatTipopregunta obj = new entCatTipopregunta();
			foreach (DataRow row in dtcat_tipo_pregunta.Rows)
			{
				obj = crearObjetoRevisado(row);
				miDic.Add(obj.id_tipo_pregunta.ToString(), obj);
			}
			return miDic;
		}
		
		public void Dispose()
		{
			GC.SuppressFinalize(this);
		}
		
		protected void Finalize()
		{
			Dispose();
		}
		#endregion

	}
}

