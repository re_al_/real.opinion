#region 
/***********************************************************************************************************
	NOMBRE:       rnSegUsuario
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla seg_usuario

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using ReAl.Opinion.Dal;
using ReAl.Opinion.PgConn;
using ReAl.Opinion.Dal.Entidades;
using ReAl.Opinion.Dal.Interface;
using System.Windows.Forms;
#endregion

namespace ReAl.Opinion.Dal.Modelo
{
	public partial class rnSegUsuario: inSegUsuario
	{
		//Debe implementar la Interface (Alt + Shift + F10)

		#region inSegUsuario Members

		#region Reflection

		/// <summary>
		/// Metodo que devuelve el Script SQL de la Tabla
		/// </summary>
		/// <returns>Script SQL</returns>
		public string getTableScript()
		{
			TableClass tabla = new TableClass(typeof(entSegUsuario));
			return tabla.CreateTableScript();
		}
		
		/// <summary>
		/// Metodo para castear Dinamicamente un Tipo
		/// <summary>
		/// <param name="valor">Tipo a ser casteado</param>
		/// <param name="myField">Enum de la columna</param>
		/// <returns>Devuelve un objeto del Tipo de la columna especificada en el Enum</returns>
		public dynamic GetColumnType(object valor, entSegUsuario.Fields myField)
		{
			if (DBNull.Value.Equals(valor)) 
				return null;
			Type destino = typeof(entSegUsuario).GetProperty(myField.ToString()).PropertyType;
			var miTipo = Nullable.GetUnderlyingType(destino) ?? destino;
			return Convert.ChangeType(valor, miTipo);
		}

		/// <summary>
		/// 	 Funcion que obtiene la llave primaria unica de la tabla seg_usuario a partir de una cadena
		/// </summary>
		/// <param name="args" type="string[]">
		///     <para>
		/// 		 Cadena desde la que se construye el identificador unico de la tabla seg_usuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Identificador unico de la tabla seg_usuario
		/// </returns>
		public string CreatePK(string[] args)
		{
			return args[0];
		}
		
		#endregion 

		#region ObtenerObjeto

		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo entSegUsuario que cumple con los filtros de los parametros
		/// </returns>
		public entSegUsuario ObtenerObjeto(int intid_usuario)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(entSegUsuario.Fields.id_usuario.ToString());
		
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(intid_usuario);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegUsuario que cumple con los filtros de los parametros
		/// </returns>
		public entSegUsuario ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegUsuario que cumple con los filtros de los parametros
		/// </returns>
		public entSegUsuario ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegUsuario.Fields.id_usuario.ToString());
				arrColumnas.Add(entSegUsuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.login.ToString());
				arrColumnas.Add(entSegUsuario.Fields.password.ToString());
				arrColumnas.Add(entSegUsuario.Fields.carnet.ToString());
				arrColumnas.Add(entSegUsuario.Fields.nombre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.paterno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.materno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.direccion.ToString());
				arrColumnas.Add(entSegUsuario.Fields.telefono.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecha_vigente.ToString());
				arrColumnas.Add(entSegUsuario.Fields.foto.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fec_nacimiento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.correo.ToString());
				arrColumnas.Add(entSegUsuario.Fields.remember_token.ToString());
				arrColumnas.Add(entSegUsuario.Fields.apiestado.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usucre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.feccre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usumod.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecmod.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entSegUsuario.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count == 1)
				{
					entSegUsuario obj = new entSegUsuario();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegUsuario que cumple con los filtros de los parametros
		/// </returns>
		public entSegUsuario ObtenerObjeto(Hashtable htbFiltro)
		{
			return ObtenerObjeto(htbFiltro, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegUsuario que cumple con los filtros de los parametros
		/// </returns>
		public entSegUsuario ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegUsuario.Fields.id_usuario.ToString());
				arrColumnas.Add(entSegUsuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.login.ToString());
				arrColumnas.Add(entSegUsuario.Fields.password.ToString());
				arrColumnas.Add(entSegUsuario.Fields.carnet.ToString());
				arrColumnas.Add(entSegUsuario.Fields.nombre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.paterno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.materno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.direccion.ToString());
				arrColumnas.Add(entSegUsuario.Fields.telefono.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecha_vigente.ToString());
				arrColumnas.Add(entSegUsuario.Fields.foto.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fec_nacimiento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.correo.ToString());
				arrColumnas.Add(entSegUsuario.Fields.remember_token.ToString());
				arrColumnas.Add(entSegUsuario.Fields.apiestado.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usucre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.feccre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usumod.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entSegUsuario.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count == 1)
				{
					entSegUsuario obj = new entSegUsuario();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegUsuario que cumple con los filtros de los parametros
		/// </returns>
		public entSegUsuario ObtenerObjeto(entSegUsuario.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegUsuario que cumple con los filtros de los parametros
		/// </returns>
		public entSegUsuario ObtenerObjeto(entSegUsuario.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un Business Object del Tipo entSegUsuario a partir de su llave promaria
		/// </summary>
		/// <returns>
		/// 	Objeto del Tipo entSegUsuario
		/// </returns>
		public entSegUsuario ObtenerObjeto(int intid_usuario, ref cTrans localTrans )
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(entSegUsuario.Fields.id_usuario.ToString());
		
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(intid_usuario);
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_usuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegUsuario que cumple con los filtros de los parametros
		/// </returns>
		public entSegUsuario ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_usuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegUsuario que cumple con los filtros de los parametros
		/// </returns>
		public entSegUsuario ObtenerObjeto(Hashtable htbFiltro, ref cTrans localTrans)
		{
			return ObtenerObjeto(htbFiltro, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_usuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegUsuario que cumple con los filtros de los parametros
		/// </returns>
		public entSegUsuario ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales,  ref cTrans localTrans)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_usuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegUsuario que cumple con los filtros de los parametros
		/// </returns>
		public entSegUsuario ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegUsuario.Fields.id_usuario.ToString());
				arrColumnas.Add(entSegUsuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.login.ToString());
				arrColumnas.Add(entSegUsuario.Fields.password.ToString());
				arrColumnas.Add(entSegUsuario.Fields.carnet.ToString());
				arrColumnas.Add(entSegUsuario.Fields.nombre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.paterno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.materno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.direccion.ToString());
				arrColumnas.Add(entSegUsuario.Fields.telefono.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecha_vigente.ToString());
				arrColumnas.Add(entSegUsuario.Fields.foto.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fec_nacimiento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.correo.ToString());
				arrColumnas.Add(entSegUsuario.Fields.remember_token.ToString());
				arrColumnas.Add(entSegUsuario.Fields.apiestado.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usucre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.feccre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usumod.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entSegUsuario.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count == 1)
				{
					entSegUsuario obj = new entSegUsuario();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_usuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegUsuario que cumple con los filtros de los parametros
		/// </returns>
		public entSegUsuario ObtenerObjeto(entSegUsuario.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_usuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegUsuario que cumple con los filtros de los parametros
		/// </returns>
		public entSegUsuario ObtenerObjeto(entSegUsuario.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion

		#region ObtenerLista

		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegUsuario> ObtenerLista()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegUsuario> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegUsuario> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegUsuario.Fields.id_usuario.ToString());
				arrColumnas.Add(entSegUsuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.login.ToString());
				arrColumnas.Add(entSegUsuario.Fields.password.ToString());
				arrColumnas.Add(entSegUsuario.Fields.carnet.ToString());
				arrColumnas.Add(entSegUsuario.Fields.nombre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.paterno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.materno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.direccion.ToString());
				arrColumnas.Add(entSegUsuario.Fields.telefono.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecha_vigente.ToString());
				arrColumnas.Add(entSegUsuario.Fields.foto.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fec_nacimiento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.correo.ToString());
				arrColumnas.Add(entSegUsuario.Fields.remember_token.ToString());
				arrColumnas.Add(entSegUsuario.Fields.apiestado.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usucre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.feccre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usumod.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecmod.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entSegUsuario.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<entSegUsuario>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegUsuario> ObtenerLista(entSegUsuario.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegUsuario> ObtenerLista(entSegUsuario.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_usuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegUsuario> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_usuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegUsuario> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegUsuario.Fields.id_usuario.ToString());
				arrColumnas.Add(entSegUsuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.login.ToString());
				arrColumnas.Add(entSegUsuario.Fields.password.ToString());
				arrColumnas.Add(entSegUsuario.Fields.carnet.ToString());
				arrColumnas.Add(entSegUsuario.Fields.nombre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.paterno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.materno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.direccion.ToString());
				arrColumnas.Add(entSegUsuario.Fields.telefono.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecha_vigente.ToString());
				arrColumnas.Add(entSegUsuario.Fields.foto.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fec_nacimiento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.correo.ToString());
				arrColumnas.Add(entSegUsuario.Fields.remember_token.ToString());
				arrColumnas.Add(entSegUsuario.Fields.apiestado.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usucre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.feccre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usumod.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecmod.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entSegUsuario.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<entSegUsuario>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_usuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegUsuario> ObtenerLista(entSegUsuario.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_usuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegUsuario> ObtenerLista(entSegUsuario.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegUsuario> ObtenerLista(Hashtable htbFiltro)
		{
			return ObtenerLista(htbFiltro, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegUsuario> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegUsuario.Fields.id_usuario.ToString());
				arrColumnas.Add(entSegUsuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.login.ToString());
				arrColumnas.Add(entSegUsuario.Fields.password.ToString());
				arrColumnas.Add(entSegUsuario.Fields.carnet.ToString());
				arrColumnas.Add(entSegUsuario.Fields.nombre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.paterno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.materno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.direccion.ToString());
				arrColumnas.Add(entSegUsuario.Fields.telefono.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecha_vigente.ToString());
				arrColumnas.Add(entSegUsuario.Fields.foto.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fec_nacimiento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.correo.ToString());
				arrColumnas.Add(entSegUsuario.Fields.remember_token.ToString());
				arrColumnas.Add(entSegUsuario.Fields.apiestado.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usucre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.feccre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usumod.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entSegUsuario.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<entSegUsuario>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_usuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegUsuario> ObtenerLista(Hashtable htbFiltro, ref cTrans localTrans)
		{
			return ObtenerLista(htbFiltro, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_usuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegUsuario> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegUsuario.Fields.id_usuario.ToString());
				arrColumnas.Add(entSegUsuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.login.ToString());
				arrColumnas.Add(entSegUsuario.Fields.password.ToString());
				arrColumnas.Add(entSegUsuario.Fields.carnet.ToString());
				arrColumnas.Add(entSegUsuario.Fields.nombre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.paterno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.materno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.direccion.ToString());
				arrColumnas.Add(entSegUsuario.Fields.telefono.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecha_vigente.ToString());
				arrColumnas.Add(entSegUsuario.Fields.foto.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fec_nacimiento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.correo.ToString());
				arrColumnas.Add(entSegUsuario.Fields.remember_token.ToString());
				arrColumnas.Add(entSegUsuario.Fields.apiestado.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usucre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.feccre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usumod.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entSegUsuario.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<entSegUsuario>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegUsuario> ObtenerListaDesdeVista(String strVista)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegUsuario> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegUsuario> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<entSegUsuario>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegUsuario> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro)
		{
			return ObtenerListaDesdeVista(strVista, htbFiltro, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegUsuario> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<entSegUsuario>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegUsuario> ObtenerListaDesdeVista(String strVista, entSegUsuario.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegUsuario> ObtenerListaDesdeVista(String strVista, entSegUsuario.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_usuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegUsuario> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_usuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegUsuario> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<entSegUsuario>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_usuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegUsuario> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref cTrans localTrans)
		{
			return ObtenerListaDesdeVista(strVista, htbFiltro, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_usuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegUsuario> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<entSegUsuario>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_usuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegUsuario> ObtenerListaDesdeVista(String strVista, entSegUsuario.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_usuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entSegUsuario> ObtenerListaDesdeVista(String strVista, entSegUsuario.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion 

		#region ObtenerCola y Obtener Pila

		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entSegUsuario> ObtenerCola()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entSegUsuario> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entSegUsuario> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegUsuario.Fields.id_usuario.ToString());
				arrColumnas.Add(entSegUsuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.login.ToString());
				arrColumnas.Add(entSegUsuario.Fields.password.ToString());
				arrColumnas.Add(entSegUsuario.Fields.carnet.ToString());
				arrColumnas.Add(entSegUsuario.Fields.nombre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.paterno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.materno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.direccion.ToString());
				arrColumnas.Add(entSegUsuario.Fields.telefono.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecha_vigente.ToString());
				arrColumnas.Add(entSegUsuario.Fields.foto.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fec_nacimiento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.correo.ToString());
				arrColumnas.Add(entSegUsuario.Fields.remember_token.ToString());
				arrColumnas.Add(entSegUsuario.Fields.apiestado.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usucre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.feccre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usumod.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecmod.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entSegUsuario.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearCola(table);
				}
				else
					return new Queue<entSegUsuario>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entSegUsuario> ObtenerCola(entSegUsuario.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entSegUsuario> ObtenerCola(entSegUsuario.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_usuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entSegUsuario> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_usuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entSegUsuario> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegUsuario.Fields.id_usuario.ToString());
				arrColumnas.Add(entSegUsuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.login.ToString());
				arrColumnas.Add(entSegUsuario.Fields.password.ToString());
				arrColumnas.Add(entSegUsuario.Fields.carnet.ToString());
				arrColumnas.Add(entSegUsuario.Fields.nombre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.paterno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.materno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.direccion.ToString());
				arrColumnas.Add(entSegUsuario.Fields.telefono.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecha_vigente.ToString());
				arrColumnas.Add(entSegUsuario.Fields.foto.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fec_nacimiento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.correo.ToString());
				arrColumnas.Add(entSegUsuario.Fields.remember_token.ToString());
				arrColumnas.Add(entSegUsuario.Fields.apiestado.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usucre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.feccre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usumod.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecmod.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entSegUsuario.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearCola(table);
				}
				else
					return new Queue<entSegUsuario>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_usuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entSegUsuario> ObtenerCola(entSegUsuario.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_usuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entSegUsuario> ObtenerCola(entSegUsuario.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entSegUsuario> ObtenerPila()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entSegUsuario> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entSegUsuario> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegUsuario.Fields.id_usuario.ToString());
				arrColumnas.Add(entSegUsuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.login.ToString());
				arrColumnas.Add(entSegUsuario.Fields.password.ToString());
				arrColumnas.Add(entSegUsuario.Fields.carnet.ToString());
				arrColumnas.Add(entSegUsuario.Fields.nombre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.paterno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.materno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.direccion.ToString());
				arrColumnas.Add(entSegUsuario.Fields.telefono.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecha_vigente.ToString());
				arrColumnas.Add(entSegUsuario.Fields.foto.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fec_nacimiento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.correo.ToString());
				arrColumnas.Add(entSegUsuario.Fields.remember_token.ToString());
				arrColumnas.Add(entSegUsuario.Fields.apiestado.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usucre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.feccre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usumod.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecmod.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entSegUsuario.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearPila(table);
				}
				else
					return new Stack<entSegUsuario>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entSegUsuario> ObtenerPila(entSegUsuario.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entSegUsuario> ObtenerPila(entSegUsuario.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_usuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entSegUsuario> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_usuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entSegUsuario> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegUsuario.Fields.id_usuario.ToString());
				arrColumnas.Add(entSegUsuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.login.ToString());
				arrColumnas.Add(entSegUsuario.Fields.password.ToString());
				arrColumnas.Add(entSegUsuario.Fields.carnet.ToString());
				arrColumnas.Add(entSegUsuario.Fields.nombre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.paterno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.materno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.direccion.ToString());
				arrColumnas.Add(entSegUsuario.Fields.telefono.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecha_vigente.ToString());
				arrColumnas.Add(entSegUsuario.Fields.foto.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fec_nacimiento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.correo.ToString());
				arrColumnas.Add(entSegUsuario.Fields.remember_token.ToString());
				arrColumnas.Add(entSegUsuario.Fields.apiestado.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usucre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.feccre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usumod.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecmod.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entSegUsuario.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearPila(table);
				}
				else
					return new Stack<entSegUsuario>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_usuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entSegUsuario> ObtenerPila(entSegUsuario.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_usuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entSegUsuario> ObtenerPila(entSegUsuario.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion 

		#region ObtenerDataTable

		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla seg_usuario
		/// </summary>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de seg_usuario
		/// </returns>
		public DataTable NuevoDataTable()
		{
			try
			{
				DataTable table = new DataTable ();
				DataColumn dc;
				dc = new DataColumn(entSegUsuario.Fields.id_usuario.ToString(),typeof(entSegUsuario).GetProperty(entSegUsuario.Fields.id_usuario.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entSegUsuario.Fields.id_departamento.ToString(),typeof(entSegUsuario).GetProperty(entSegUsuario.Fields.id_departamento.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entSegUsuario.Fields.login.ToString(),typeof(entSegUsuario).GetProperty(entSegUsuario.Fields.login.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entSegUsuario.Fields.password.ToString(),typeof(entSegUsuario).GetProperty(entSegUsuario.Fields.password.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entSegUsuario.Fields.carnet.ToString(),typeof(entSegUsuario).GetProperty(entSegUsuario.Fields.carnet.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entSegUsuario.Fields.nombre.ToString(),typeof(entSegUsuario).GetProperty(entSegUsuario.Fields.nombre.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entSegUsuario.Fields.paterno.ToString(),typeof(entSegUsuario).GetProperty(entSegUsuario.Fields.paterno.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entSegUsuario.Fields.materno.ToString(),typeof(entSegUsuario).GetProperty(entSegUsuario.Fields.materno.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entSegUsuario.Fields.direccion.ToString(),typeof(entSegUsuario).GetProperty(entSegUsuario.Fields.direccion.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entSegUsuario.Fields.telefono.ToString(),typeof(entSegUsuario).GetProperty(entSegUsuario.Fields.telefono.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entSegUsuario.Fields.fecha_vigente.ToString(),typeof(entSegUsuario).GetProperty(entSegUsuario.Fields.fecha_vigente.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entSegUsuario.Fields.foto.ToString(),typeof(entSegUsuario).GetProperty(entSegUsuario.Fields.foto.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entSegUsuario.Fields.fec_nacimiento.ToString(),typeof(entSegUsuario).GetProperty(entSegUsuario.Fields.fec_nacimiento.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entSegUsuario.Fields.correo.ToString(),typeof(entSegUsuario).GetProperty(entSegUsuario.Fields.correo.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entSegUsuario.Fields.remember_token.ToString(),typeof(entSegUsuario).GetProperty(entSegUsuario.Fields.remember_token.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entSegUsuario.Fields.apiestado.ToString(),typeof(entSegUsuario).GetProperty(entSegUsuario.Fields.apiestado.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entSegUsuario.Fields.usucre.ToString(),typeof(entSegUsuario).GetProperty(entSegUsuario.Fields.usucre.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entSegUsuario.Fields.feccre.ToString(),typeof(entSegUsuario).GetProperty(entSegUsuario.Fields.feccre.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entSegUsuario.Fields.usumod.ToString(),typeof(entSegUsuario).GetProperty(entSegUsuario.Fields.usumod.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entSegUsuario.Fields.fecmod.ToString(),typeof(entSegUsuario).GetProperty(entSegUsuario.Fields.fecmod.ToString()).PropertyType);
				table.Columns.Add(dc);

				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que genera un DataTable con determinadas columnas de una seg_usuario
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de seg_usuario
		/// </returns>
		public DataTable NuevoDataTable(ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'2'");
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entSegUsuario.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla seg_usuario
		/// </summary>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de seg_usuario
		/// </returns>
		public DataTable ObtenerDataTable()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDataTable(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla y n condicion WHERE seg_usuario
		/// </summary>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de seg_usuario
		/// </returns>
		public DataTable ObtenerDataTable(String strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla seg_usuario
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de seg_usuario
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla seg_usuario
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de seg_usuario
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla seg_usuario
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de seg_usuario
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla seg_usuario
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de seg_usuario
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro)
		{
			try
			{
				return ObtenerDataTable(arrColumnas, htbFiltro, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla seg_usuario
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de seg_usuario
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla seg_usuario
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de seg_usuario
		/// </returns>
		public DataTable ObtenerDataTable(Hashtable htbFiltro)
		{
			try
			{
				return ObtenerDataTable(htbFiltro, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla seg_usuario
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de seg_usuario
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entSegUsuario.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla seg_usuario
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de seg_usuario
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entSegUsuario.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla seg_usuario
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de seg_usuario
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegUsuario.Fields.id_usuario.ToString());
				arrColumnas.Add(entSegUsuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.login.ToString());
				arrColumnas.Add(entSegUsuario.Fields.password.ToString());
				arrColumnas.Add(entSegUsuario.Fields.carnet.ToString());
				arrColumnas.Add(entSegUsuario.Fields.nombre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.paterno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.materno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.direccion.ToString());
				arrColumnas.Add(entSegUsuario.Fields.telefono.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecha_vigente.ToString());
				arrColumnas.Add(entSegUsuario.Fields.foto.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fec_nacimiento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.correo.ToString());
				arrColumnas.Add(entSegUsuario.Fields.remember_token.ToString());
				arrColumnas.Add(entSegUsuario.Fields.apiestado.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usucre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.feccre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usumod.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecmod.ToString());
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla seg_usuario
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de seg_usuario
		/// </returns>
		public DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegUsuario.Fields.id_usuario.ToString());
				arrColumnas.Add(entSegUsuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.login.ToString());
				arrColumnas.Add(entSegUsuario.Fields.password.ToString());
				arrColumnas.Add(entSegUsuario.Fields.carnet.ToString());
				arrColumnas.Add(entSegUsuario.Fields.nombre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.paterno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.materno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.direccion.ToString());
				arrColumnas.Add(entSegUsuario.Fields.telefono.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecha_vigente.ToString());
				arrColumnas.Add(entSegUsuario.Fields.foto.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fec_nacimiento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.correo.ToString());
				arrColumnas.Add(entSegUsuario.Fields.remember_token.ToString());
				arrColumnas.Add(entSegUsuario.Fields.apiestado.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usucre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.feccre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usumod.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecmod.ToString());
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla seg_usuario
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de seg_usuario
		/// </returns>
		public DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegUsuario.Fields.id_usuario.ToString());
				arrColumnas.Add(entSegUsuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.login.ToString());
				arrColumnas.Add(entSegUsuario.Fields.password.ToString());
				arrColumnas.Add(entSegUsuario.Fields.carnet.ToString());
				arrColumnas.Add(entSegUsuario.Fields.nombre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.paterno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.materno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.direccion.ToString());
				arrColumnas.Add(entSegUsuario.Fields.telefono.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecha_vigente.ToString());
				arrColumnas.Add(entSegUsuario.Fields.foto.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fec_nacimiento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.correo.ToString());
				arrColumnas.Add(entSegUsuario.Fields.remember_token.ToString());
				arrColumnas.Add(entSegUsuario.Fields.apiestado.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usucre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.feccre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usumod.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecmod.ToString());
				return ObtenerDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla seg_usuario
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de seg_usuario
		/// </returns>
		public DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return ObtenerDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla seg_usuario
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de seg_usuario
		/// </returns>
		public DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DataTable table = local.cargarDataTableOr(cParametros.schema + entSegUsuario.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="searchField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(entSegUsuario.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="searchField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(entSegUsuario.Fields searchField, object searchValue, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, entSegUsuario.Fields searchField, object searchValue, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, entSegUsuario.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		

		#endregion 

		#region ObtenerDiccionario

		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entSegUsuario> ObtenerDiccionario()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entSegUsuario> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entSegUsuario> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegUsuario.Fields.id_usuario.ToString());
				arrColumnas.Add(entSegUsuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.login.ToString());
				arrColumnas.Add(entSegUsuario.Fields.password.ToString());
				arrColumnas.Add(entSegUsuario.Fields.carnet.ToString());
				arrColumnas.Add(entSegUsuario.Fields.nombre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.paterno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.materno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.direccion.ToString());
				arrColumnas.Add(entSegUsuario.Fields.telefono.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecha_vigente.ToString());
				arrColumnas.Add(entSegUsuario.Fields.foto.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fec_nacimiento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.correo.ToString());
				arrColumnas.Add(entSegUsuario.Fields.remember_token.ToString());
				arrColumnas.Add(entSegUsuario.Fields.apiestado.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usucre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.feccre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usumod.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecmod.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entSegUsuario.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearDiccionario(table);
				}
				else
					return new Dictionary<string, entSegUsuario>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entSegUsuario> ObtenerDiccionario(entSegUsuario.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entSegUsuario> ObtenerDiccionario(entSegUsuario.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_usuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entSegUsuario> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_usuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entSegUsuario> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegUsuario.Fields.id_usuario.ToString());
				arrColumnas.Add(entSegUsuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.login.ToString());
				arrColumnas.Add(entSegUsuario.Fields.password.ToString());
				arrColumnas.Add(entSegUsuario.Fields.carnet.ToString());
				arrColumnas.Add(entSegUsuario.Fields.nombre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.paterno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.materno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.direccion.ToString());
				arrColumnas.Add(entSegUsuario.Fields.telefono.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecha_vigente.ToString());
				arrColumnas.Add(entSegUsuario.Fields.foto.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fec_nacimiento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.correo.ToString());
				arrColumnas.Add(entSegUsuario.Fields.remember_token.ToString());
				arrColumnas.Add(entSegUsuario.Fields.apiestado.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usucre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.feccre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usumod.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecmod.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entSegUsuario.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearDiccionario(table);
				}
				else
					return new Dictionary<string, entSegUsuario>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_usuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entSegUsuario> ObtenerDiccionario(entSegUsuario.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entSegUsuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_usuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entSegUsuario> ObtenerDiccionario(entSegUsuario.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion 

		#region FuncionesAgregadas

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegUsuario que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesCount(entSegUsuario.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegUsuario que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesCount(entSegUsuario.Fields refField, entSegUsuario.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegUsuario que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesCount(entSegUsuario.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("count(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegUsuario que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMin(entSegUsuario.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegUsuario que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMin(entSegUsuario.Fields refField, entSegUsuario.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegUsuario que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMin(entSegUsuario.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("min(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegUsuario que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMax(entSegUsuario.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegUsuario que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMax(entSegUsuario.Fields refField, entSegUsuario.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegUsuario que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMax(entSegUsuario.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("max(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegUsuario que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesSum(entSegUsuario.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegUsuario que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesSum(entSegUsuario.Fields refField, entSegUsuario.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegUsuario que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesSum(entSegUsuario.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("sum(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegUsuario que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesAvg(entSegUsuario.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegUsuario que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesAvg(entSegUsuario.Fields refField, entSegUsuario.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entSegUsuario que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesAvg(entSegUsuario.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("avg(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion 

		#region ABMs

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla seg_usuario a partir de una clase del tipo Eseg_usuario
		/// </summary>
		/// <param name="obj" type="Entidades.entSegUsuario">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla seg_usuarioseg_usuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionseg_usuario
		/// </returns>
		public bool Insert(entSegUsuario obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(entSegUsuario.Fields.id_usuario.ToString());
				arrColumnas.Add(entSegUsuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.login.ToString());
				arrColumnas.Add(entSegUsuario.Fields.password.ToString());
				arrColumnas.Add(entSegUsuario.Fields.carnet.ToString());
				arrColumnas.Add(entSegUsuario.Fields.nombre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.paterno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.materno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.direccion.ToString());
				arrColumnas.Add(entSegUsuario.Fields.telefono.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecha_vigente.ToString());
				arrColumnas.Add(entSegUsuario.Fields.foto.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fec_nacimiento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.correo.ToString());
				arrColumnas.Add(entSegUsuario.Fields.remember_token.ToString());
				arrColumnas.Add(entSegUsuario.Fields.apiestado.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usucre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.feccre.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.id_usuario);
				arrValores.Add(obj.id_departamento);
				arrValores.Add(obj.login == null ? null : "'" + obj.login + "'");
				arrValores.Add(obj.password == null ? null : "'" + obj.password + "'");
				arrValores.Add(obj.carnet);
				arrValores.Add(obj.nombre == null ? null : "'" + obj.nombre + "'");
				arrValores.Add(obj.paterno == null ? null : "'" + obj.paterno + "'");
				arrValores.Add(obj.materno == null ? null : "'" + obj.materno + "'");
				arrValores.Add(obj.direccion == null ? null : "'" + obj.direccion + "'");
				arrValores.Add(obj.telefono);
				arrValores.Add(obj.fecha_vigente == null ? null : "'" + Convert.ToDateTime(obj.fecha_vigente).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.foto == null ? null : "'" + obj.foto + "'");
				arrValores.Add(obj.fec_nacimiento == null ? null : "'" + Convert.ToDateTime(obj.fec_nacimiento).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.correo == null ? null : "'" + obj.correo + "'");
				arrValores.Add(obj.remember_token == null ? null : "'" + obj.remember_token + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(cParametros.parFormatoFechaHora) + "'");

			
				cConn local = new cConn();
				return local.insertBD(entSegUsuario.strNombreTabla, arrColumnas, arrValores);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla seg_usuario a partir de una clase del tipo Eseg_usuario
		/// </summary>
		/// <param name="obj" type="Entidades.entSegUsuario">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla seg_usuarioseg_usuario
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_usuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionseg_usuario
		/// </returns>
		public bool Insert(entSegUsuario obj, ref cTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(entSegUsuario.Fields.id_usuario.ToString());
				arrColumnas.Add(entSegUsuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.login.ToString());
				arrColumnas.Add(entSegUsuario.Fields.password.ToString());
				arrColumnas.Add(entSegUsuario.Fields.carnet.ToString());
				arrColumnas.Add(entSegUsuario.Fields.nombre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.paterno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.materno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.direccion.ToString());
				arrColumnas.Add(entSegUsuario.Fields.telefono.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecha_vigente.ToString());
				arrColumnas.Add(entSegUsuario.Fields.foto.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fec_nacimiento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.correo.ToString());
				arrColumnas.Add(entSegUsuario.Fields.remember_token.ToString());
				arrColumnas.Add(entSegUsuario.Fields.apiestado.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usucre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.feccre.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.id_usuario);
				arrValores.Add(obj.id_departamento);
				arrValores.Add(obj.login == null ? null : "'" + obj.login + "'");
				arrValores.Add(obj.password == null ? null : "'" + obj.password + "'");
				arrValores.Add(obj.carnet);
				arrValores.Add(obj.nombre == null ? null : "'" + obj.nombre + "'");
				arrValores.Add(obj.paterno == null ? null : "'" + obj.paterno + "'");
				arrValores.Add(obj.materno == null ? null : "'" + obj.materno + "'");
				arrValores.Add(obj.direccion == null ? null : "'" + obj.direccion + "'");
				arrValores.Add(obj.telefono);
				arrValores.Add(obj.fecha_vigente == null ? null : "'" + Convert.ToDateTime(obj.fecha_vigente).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.foto == null ? null : "'" + obj.foto + "'");
				arrValores.Add(obj.fec_nacimiento == null ? null : "'" + Convert.ToDateTime(obj.fec_nacimiento).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.correo == null ? null : "'" + obj.correo + "'");
				arrValores.Add(obj.remember_token == null ? null : "'" + obj.remember_token + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(cParametros.parFormatoFechaHora) + "'");

			
				cConn local = new cConn();
				return local.insertBD(entSegUsuario.strNombreTabla, arrColumnas, arrValores, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla seg_usuario a partir de una clase del tipo Eseg_usuario
		/// </summary>
		/// <param name="obj" type="Entidades.entSegUsuario">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla seg_usuarioseg_usuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionseg_usuario
		/// </returns>
		public bool InsertIdentity(entSegUsuario obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(entSegUsuario.Fields.id_usuario.ToString());
				arrColumnas.Add(entSegUsuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.login.ToString());
				arrColumnas.Add(entSegUsuario.Fields.password.ToString());
				arrColumnas.Add(entSegUsuario.Fields.carnet.ToString());
				arrColumnas.Add(entSegUsuario.Fields.nombre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.paterno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.materno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.direccion.ToString());
				arrColumnas.Add(entSegUsuario.Fields.telefono.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecha_vigente.ToString());
				arrColumnas.Add(entSegUsuario.Fields.foto.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fec_nacimiento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.correo.ToString());
				arrColumnas.Add(entSegUsuario.Fields.remember_token.ToString());
				arrColumnas.Add(entSegUsuario.Fields.apiestado.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usucre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.feccre.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.id_usuario);
				arrValores.Add(obj.id_departamento);
				arrValores.Add(obj.login == null ? null : "'" + obj.login + "'");
				arrValores.Add(obj.password == null ? null : "'" + obj.password + "'");
				arrValores.Add(obj.carnet);
				arrValores.Add(obj.nombre == null ? null : "'" + obj.nombre + "'");
				arrValores.Add(obj.paterno == null ? null : "'" + obj.paterno + "'");
				arrValores.Add(obj.materno == null ? null : "'" + obj.materno + "'");
				arrValores.Add(obj.direccion == null ? null : "'" + obj.direccion + "'");
				arrValores.Add(obj.telefono);
				arrValores.Add(obj.fecha_vigente == null ? null : "'" + Convert.ToDateTime(obj.fecha_vigente).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.foto == null ? null : "'" + obj.foto + "'");
				arrValores.Add(obj.fec_nacimiento == null ? null : "'" + Convert.ToDateTime(obj.fec_nacimiento).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.correo == null ? null : "'" + obj.correo + "'");
				arrValores.Add(obj.remember_token == null ? null : "'" + obj.remember_token + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(cParametros.parFormatoFechaHora) + "'");

			
				cConn local = new cConn();
				int intIdentidad = -1;
				bool res = local.insertBD(entSegUsuario.strNombreTabla, arrColumnas, arrValores, ref intIdentidad);
				obj.id_usuario = intIdentidad;
				return res;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla seg_usuario a partir de una clase del tipo Eseg_usuario
		/// </summary>
		/// <param name="obj" type="Entidades.entSegUsuario">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla seg_usuarioseg_usuario
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_usuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionseg_usuario
		/// </returns>
		public bool InsertIdentity(entSegUsuario obj, ref cTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(entSegUsuario.Fields.id_usuario.ToString());
				arrColumnas.Add(entSegUsuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.login.ToString());
				arrColumnas.Add(entSegUsuario.Fields.password.ToString());
				arrColumnas.Add(entSegUsuario.Fields.carnet.ToString());
				arrColumnas.Add(entSegUsuario.Fields.nombre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.paterno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.materno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.direccion.ToString());
				arrColumnas.Add(entSegUsuario.Fields.telefono.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecha_vigente.ToString());
				arrColumnas.Add(entSegUsuario.Fields.foto.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fec_nacimiento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.correo.ToString());
				arrColumnas.Add(entSegUsuario.Fields.remember_token.ToString());
				arrColumnas.Add(entSegUsuario.Fields.apiestado.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usucre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.feccre.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.id_usuario);
				arrValores.Add(obj.id_departamento);
				arrValores.Add(obj.login == null ? null : "'" + obj.login + "'");
				arrValores.Add(obj.password == null ? null : "'" + obj.password + "'");
				arrValores.Add(obj.carnet);
				arrValores.Add(obj.nombre == null ? null : "'" + obj.nombre + "'");
				arrValores.Add(obj.paterno == null ? null : "'" + obj.paterno + "'");
				arrValores.Add(obj.materno == null ? null : "'" + obj.materno + "'");
				arrValores.Add(obj.direccion == null ? null : "'" + obj.direccion + "'");
				arrValores.Add(obj.telefono);
				arrValores.Add(obj.fecha_vigente == null ? null : "'" + Convert.ToDateTime(obj.fecha_vigente).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.foto == null ? null : "'" + obj.foto + "'");
				arrValores.Add(obj.fec_nacimiento == null ? null : "'" + Convert.ToDateTime(obj.fec_nacimiento).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.correo == null ? null : "'" + obj.correo + "'");
				arrValores.Add(obj.remember_token == null ? null : "'" + obj.remember_token + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(cParametros.parFormatoFechaHora) + "'");

			
				cConn local = new cConn();
				int intIdentidad = -1;
				bool res = local.insertBD(entSegUsuario.strNombreTabla, arrColumnas, arrValores, ref intIdentidad, ref localTrans);
				obj.id_usuario = intIdentidad;
				return res;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla seg_usuario a partir de una clase del tipo Eseg_usuario
		/// </summary>
		/// <param name="obj" type="Entidades.entSegUsuario">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla seg_usuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionseg_usuario
		/// </returns>
		public int Update(entSegUsuario obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegUsuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.login.ToString());
				arrColumnas.Add(entSegUsuario.Fields.password.ToString());
				arrColumnas.Add(entSegUsuario.Fields.carnet.ToString());
				arrColumnas.Add(entSegUsuario.Fields.nombre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.paterno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.materno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.direccion.ToString());
				arrColumnas.Add(entSegUsuario.Fields.telefono.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecha_vigente.ToString());
				arrColumnas.Add(entSegUsuario.Fields.foto.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fec_nacimiento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.correo.ToString());
				arrColumnas.Add(entSegUsuario.Fields.remember_token.ToString());
				arrColumnas.Add(entSegUsuario.Fields.apiestado.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usucre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.feccre.ToString());
			
				ArrayList arrValores = new ArrayList();
				arrValores.Add(obj.id_departamento);
				arrValores.Add(obj.login == null ? null : "'" + obj.login + "'");
				arrValores.Add(obj.password == null ? null : "'" + obj.password + "'");
				arrValores.Add(obj.carnet);
				arrValores.Add(obj.nombre == null ? null : "'" + obj.nombre + "'");
				arrValores.Add(obj.paterno == null ? null : "'" + obj.paterno + "'");
				arrValores.Add(obj.materno == null ? null : "'" + obj.materno + "'");
				arrValores.Add(obj.direccion == null ? null : "'" + obj.direccion + "'");
				arrValores.Add(obj.telefono);
				arrValores.Add(obj.fecha_vigente == null ? null : "'" + Convert.ToDateTime(obj.fecha_vigente).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.foto == null ? null : "'" + obj.foto + "'");
				arrValores.Add(obj.fec_nacimiento == null ? null : "'" + Convert.ToDateTime(obj.fec_nacimiento).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.correo == null ? null : "'" + obj.correo + "'");
				arrValores.Add(obj.remember_token == null ? null : "'" + obj.remember_token + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(cParametros.parFormatoFechaHora) + "'");

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(entSegUsuario.Fields.id_usuario.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.id_usuario);

			
				cConn local = new cConn();
				return local.updateBD(entSegUsuario.strNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla seg_usuario a partir de una clase del tipo eseg_usuario
		/// </summary>
		/// <param name="obj" type="Entidades.entSegUsuario">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla seg_usuario
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_usuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacion
		/// </returns>
		public int Update(entSegUsuario obj, ref cTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegUsuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.login.ToString());
				arrColumnas.Add(entSegUsuario.Fields.password.ToString());
				arrColumnas.Add(entSegUsuario.Fields.carnet.ToString());
				arrColumnas.Add(entSegUsuario.Fields.nombre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.paterno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.materno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.direccion.ToString());
				arrColumnas.Add(entSegUsuario.Fields.telefono.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecha_vigente.ToString());
				arrColumnas.Add(entSegUsuario.Fields.foto.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fec_nacimiento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.correo.ToString());
				arrColumnas.Add(entSegUsuario.Fields.remember_token.ToString());
				arrColumnas.Add(entSegUsuario.Fields.apiestado.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usucre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.feccre.ToString());
			
				ArrayList arrValores = new ArrayList();
				arrValores.Add(obj.id_departamento);
				arrValores.Add(obj.login == null ? null : "'" + obj.login + "'");
				arrValores.Add(obj.password == null ? null : "'" + obj.password + "'");
				arrValores.Add(obj.carnet);
				arrValores.Add(obj.nombre == null ? null : "'" + obj.nombre + "'");
				arrValores.Add(obj.paterno == null ? null : "'" + obj.paterno + "'");
				arrValores.Add(obj.materno == null ? null : "'" + obj.materno + "'");
				arrValores.Add(obj.direccion == null ? null : "'" + obj.direccion + "'");
				arrValores.Add(obj.telefono);
				arrValores.Add(obj.fecha_vigente == null ? null : "'" + Convert.ToDateTime(obj.fecha_vigente).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.foto == null ? null : "'" + obj.foto + "'");
				arrValores.Add(obj.fec_nacimiento == null ? null : "'" + Convert.ToDateTime(obj.fec_nacimiento).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.correo == null ? null : "'" + obj.correo + "'");
				arrValores.Add(obj.remember_token == null ? null : "'" + obj.remember_token + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(cParametros.parFormatoFechaHora) + "'");

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(entSegUsuario.Fields.id_usuario.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.id_usuario);

			
				cConn local = new cConn();
				return local.updateBD(entSegUsuario.strNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla seg_usuario a partir de una clase del tipo entSegUsuario y su respectiva PK
		/// </summary>
		/// <param name="obj" type="Entidades.entSegUsuario">
		///     <para>
		/// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla seg_usuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionseg_usuario
		/// </returns>
		public int Delete(entSegUsuario obj)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(entSegUsuario.Fields.id_usuario.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.id_usuario);

			
				cConn local = new cConn();
				return local.deleteBD(entSegUsuario.strNombreTabla, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla seg_usuario a partir de una clase del tipo entSegUsuario y su PK
		/// </summary>
		/// <param name="obj" type="Entidades.eseg_usuario">
		///     <para>
		/// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla seg_usuario
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_usuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacionseg_usuario
		/// </returns>
		public int Delete(entSegUsuario obj, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(entSegUsuario.Fields.id_usuario.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.id_usuario);

			
				cConn local = new cConn();
				return local.deleteBD(entSegUsuario.strNombreTabla, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla seg_usuario a partir de una clase del tipo eseg_usuario
		/// </summary>
		/// <param name="arrColumnasWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Valores para cada una de las columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionseg_usuario
		/// </returns>
		public int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				cConn local = new cConn();
				return local.deleteBD("seg_usuario", arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla seg_usuario a partir de una clase del tipo eseg_usuario
		/// </summary>
		/// <param name="arrColumnasWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Valores para cada una de las columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion seg_usuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacionseg_usuario
		/// </returns>
		public int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			try
			{
				cConn local = new cConn();
				return local.deleteBD(entSegUsuario.strNombreTabla, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		

		#endregion 

		#region Llenado de elementos

		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla seg_usuario
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla seg_usuarioseg_usuario
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla seg_usuario
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla seg_usuarioseg_usuario
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla seg_usuario
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla seg_usuarioseg_usuario
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla seg_usuario
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla seg_usuarioseg_usuario
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegUsuario.Fields.id_usuario.ToString());
				arrColumnas.Add(entSegUsuario.Fields.id_departamento.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla seg_usuario
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla seg_usuarioseg_usuario
		///     </para>
		/// </param>
		/// <param name="valueField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entSegUsuario.Fields valueField, entSegUsuario.Fields textField)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla seg_usuario
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla seg_usuarioseg_usuario
		///     </para>
		/// </param>
		/// <param name="valueField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entSegUsuario.Fields valueField, entSegUsuario.Fields textField, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla seg_usuario
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla seg_usuarioseg_usuario
		///     </para>
		/// </param>
		/// <param name="valueField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entSegUsuario.Fields valueField, String textField)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla seg_usuario
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla seg_usuarioseg_usuario
		///     </para>
		/// </param>
		/// <param name="valueField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entSegUsuario.Fields valueField, String textField, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla seg_usuario
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla seg_usuarioseg_usuario
		///     </para>
		/// </param>
		/// <param name="valueField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entSegUsuario.Fields valueField, entSegUsuario.Fields textField, entSegUsuario.Fields searchField, object searchValue)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField.ToString(), searchField, searchValue);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla seg_usuario
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla seg_usuarioseg_usuario
		///     </para>
		/// </param>
		/// <param name="valueField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entSegUsuario.Fields valueField, String textField, entSegUsuario.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla seg_usuario
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla seg_usuarioseg_usuario
		///     </para>
		/// </param>
		/// <param name="valueField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entSegUsuario.Fields valueField, entSegUsuario.Fields textField, entSegUsuario.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla seg_usuario
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla seg_usuarioseg_usuario
		///     </para>
		/// </param>
		/// <param name="valueField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entSegUsuario.Fields valueField, String textField, entSegUsuario.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla seg_usuario
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla seg_usuarioseg_usuario
		///     </para>
		/// </param>
		/// <param name="valueField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entSegUsuario.Fields valueField, entSegUsuario.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla seg_usuario
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla seg_usuarioseg_usuario
		///     </para>
		/// </param>
		/// <param name="valueField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entSegUsuario.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla seg_usuario
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla seg_usuarioseg_usuario
		///     </para>
		/// </param>
		/// <param name="valueField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entSegUsuario.Fields valueField, entSegUsuario.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla seg_usuario
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla seg_usuarioseg_usuario
		///     </para>
		/// </param>
		/// <param name="valueField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entSegUsuario.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla seg_usuario
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla seg_usuarioseg_usuario
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entSegUsuario.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Columns.Count > 0)
				{
					cmb.ValueMember = table.Columns[0].ColumnName;
					cmb.DisplayMember = table.Columns[1].ColumnName;
					cmb.DataSource = table;

				}
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla seg_usuario
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegUsuario.Fields.id_usuario.ToString());
				arrColumnas.Add(entSegUsuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.login.ToString());
				arrColumnas.Add(entSegUsuario.Fields.password.ToString());
				arrColumnas.Add(entSegUsuario.Fields.carnet.ToString());
				arrColumnas.Add(entSegUsuario.Fields.nombre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.paterno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.materno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.direccion.ToString());
				arrColumnas.Add(entSegUsuario.Fields.telefono.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecha_vigente.ToString());
				arrColumnas.Add(entSegUsuario.Fields.foto.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fec_nacimiento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.correo.ToString());
				arrColumnas.Add(entSegUsuario.Fields.remember_token.ToString());
				arrColumnas.Add(entSegUsuario.Fields.apiestado.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usucre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.feccre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usumod.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecmod.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla seg_usuario
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		  Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegUsuario.Fields.id_usuario.ToString());
				arrColumnas.Add(entSegUsuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.login.ToString());
				arrColumnas.Add(entSegUsuario.Fields.password.ToString());
				arrColumnas.Add(entSegUsuario.Fields.carnet.ToString());
				arrColumnas.Add(entSegUsuario.Fields.nombre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.paterno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.materno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.direccion.ToString());
				arrColumnas.Add(entSegUsuario.Fields.telefono.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecha_vigente.ToString());
				arrColumnas.Add(entSegUsuario.Fields.foto.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fec_nacimiento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.correo.ToString());
				arrColumnas.Add(entSegUsuario.Fields.remember_token.ToString());
				arrColumnas.Add(entSegUsuario.Fields.apiestado.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usucre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.feccre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usumod.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecmod.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla seg_usuario
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla seg_usuario
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla seg_usuario
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegUsuario.Fields.id_usuario.ToString());
				arrColumnas.Add(entSegUsuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.login.ToString());
				arrColumnas.Add(entSegUsuario.Fields.password.ToString());
				arrColumnas.Add(entSegUsuario.Fields.carnet.ToString());
				arrColumnas.Add(entSegUsuario.Fields.nombre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.paterno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.materno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.direccion.ToString());
				arrColumnas.Add(entSegUsuario.Fields.telefono.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecha_vigente.ToString());
				arrColumnas.Add(entSegUsuario.Fields.foto.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fec_nacimiento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.correo.ToString());
				arrColumnas.Add(entSegUsuario.Fields.remember_token.ToString());
				arrColumnas.Add(entSegUsuario.Fields.apiestado.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usucre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.feccre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usumod.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecmod.ToString());
			
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla seg_usuario
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegUsuario.Fields.id_usuario.ToString());
				arrColumnas.Add(entSegUsuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.login.ToString());
				arrColumnas.Add(entSegUsuario.Fields.password.ToString());
				arrColumnas.Add(entSegUsuario.Fields.carnet.ToString());
				arrColumnas.Add(entSegUsuario.Fields.nombre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.paterno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.materno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.direccion.ToString());
				arrColumnas.Add(entSegUsuario.Fields.telefono.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecha_vigente.ToString());
				arrColumnas.Add(entSegUsuario.Fields.foto.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fec_nacimiento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.correo.ToString());
				arrColumnas.Add(entSegUsuario.Fields.remember_token.ToString());
				arrColumnas.Add(entSegUsuario.Fields.apiestado.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usucre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.feccre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usumod.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecmod.ToString());
			
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla seg_usuario
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla seg_usuario
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DbDataReader dsReader = local.cargarDataReaderAnd(cParametros.schema + entSegUsuario.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				dtg.DataSource = dsReader;

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla seg_usuario
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, entSegUsuario.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegUsuario.Fields.id_usuario.ToString());
				arrColumnas.Add(entSegUsuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.login.ToString());
				arrColumnas.Add(entSegUsuario.Fields.password.ToString());
				arrColumnas.Add(entSegUsuario.Fields.carnet.ToString());
				arrColumnas.Add(entSegUsuario.Fields.nombre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.paterno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.materno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.direccion.ToString());
				arrColumnas.Add(entSegUsuario.Fields.telefono.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecha_vigente.ToString());
				arrColumnas.Add(entSegUsuario.Fields.foto.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fec_nacimiento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.correo.ToString());
				arrColumnas.Add(entSegUsuario.Fields.remember_token.ToString());
				arrColumnas.Add(entSegUsuario.Fields.apiestado.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usucre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.feccre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usumod.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla seg_usuario
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, entSegUsuario.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entSegUsuario.Fields.id_usuario.ToString());
				arrColumnas.Add(entSegUsuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.login.ToString());
				arrColumnas.Add(entSegUsuario.Fields.password.ToString());
				arrColumnas.Add(entSegUsuario.Fields.carnet.ToString());
				arrColumnas.Add(entSegUsuario.Fields.nombre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.paterno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.materno.ToString());
				arrColumnas.Add(entSegUsuario.Fields.direccion.ToString());
				arrColumnas.Add(entSegUsuario.Fields.telefono.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecha_vigente.ToString());
				arrColumnas.Add(entSegUsuario.Fields.foto.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fec_nacimiento.ToString());
				arrColumnas.Add(entSegUsuario.Fields.correo.ToString());
				arrColumnas.Add(entSegUsuario.Fields.remember_token.ToString());
				arrColumnas.Add(entSegUsuario.Fields.apiestado.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usucre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.feccre.ToString());
				arrColumnas.Add(entSegUsuario.Fields.usumod.ToString());
				arrColumnas.Add(entSegUsuario.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla seg_usuario
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entSegUsuario.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla seg_usuario
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entSegUsuario.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla seg_usuario
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarDataGridOr(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla seg_usuario
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DbDataReader dsReader = local.cargarDataReaderOr(cParametros.schema + entSegUsuario.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				dtg.DataSource = dsReader;

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion 


		#endregion

		#region Funciones Internas

		/// <summary>
		/// 	 Funcion que devuelve un objeto a partir de un DataRow
		/// </summary>
		/// <param name="row" type="System.Data.DataRow">
		///     <para>
		/// 		 DataRow con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Objeto seg_usuario
		/// </returns>
		internal entSegUsuario crearObjeto(DataRow row)
		{
			entSegUsuario obj = new entSegUsuario();
			obj.id_usuario = GetColumnType(row[entSegUsuario.Fields.id_usuario.ToString()], entSegUsuario.Fields.id_usuario);
			obj.id_departamento = GetColumnType(row[entSegUsuario.Fields.id_departamento.ToString()], entSegUsuario.Fields.id_departamento);
			obj.login = GetColumnType(row[entSegUsuario.Fields.login.ToString()], entSegUsuario.Fields.login);
			obj.password = GetColumnType(row[entSegUsuario.Fields.password.ToString()], entSegUsuario.Fields.password);
			obj.carnet = GetColumnType(row[entSegUsuario.Fields.carnet.ToString()], entSegUsuario.Fields.carnet);
			obj.nombre = GetColumnType(row[entSegUsuario.Fields.nombre.ToString()], entSegUsuario.Fields.nombre);
			obj.paterno = GetColumnType(row[entSegUsuario.Fields.paterno.ToString()], entSegUsuario.Fields.paterno);
			obj.materno = GetColumnType(row[entSegUsuario.Fields.materno.ToString()], entSegUsuario.Fields.materno);
			obj.direccion = GetColumnType(row[entSegUsuario.Fields.direccion.ToString()], entSegUsuario.Fields.direccion);
			obj.telefono = GetColumnType(row[entSegUsuario.Fields.telefono.ToString()], entSegUsuario.Fields.telefono);
			obj.fecha_vigente = GetColumnType(row[entSegUsuario.Fields.fecha_vigente.ToString()], entSegUsuario.Fields.fecha_vigente);
			obj.foto = GetColumnType(row[entSegUsuario.Fields.foto.ToString()], entSegUsuario.Fields.foto);
			obj.fec_nacimiento = GetColumnType(row[entSegUsuario.Fields.fec_nacimiento.ToString()], entSegUsuario.Fields.fec_nacimiento);
			obj.correo = GetColumnType(row[entSegUsuario.Fields.correo.ToString()], entSegUsuario.Fields.correo);
			obj.remember_token = GetColumnType(row[entSegUsuario.Fields.remember_token.ToString()], entSegUsuario.Fields.remember_token);
			obj.apiestado = GetColumnType(row[entSegUsuario.Fields.apiestado.ToString()], entSegUsuario.Fields.apiestado);
			obj.usucre = GetColumnType(row[entSegUsuario.Fields.usucre.ToString()], entSegUsuario.Fields.usucre);
			obj.feccre = GetColumnType(row[entSegUsuario.Fields.feccre.ToString()], entSegUsuario.Fields.feccre);
			obj.usumod = GetColumnType(row[entSegUsuario.Fields.usumod.ToString()], entSegUsuario.Fields.usumod);
			obj.fecmod = GetColumnType(row[entSegUsuario.Fields.fecmod.ToString()], entSegUsuario.Fields.fecmod);
			return obj;
		}

		/// <summary>
		/// 	 Funcion que devuelve un objeto a partir de un DataRow
		/// </summary>
		/// <param name="row" type="System.Data.DataRow">
		///     <para>
		/// 		 DataRow con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Objeto seg_usuario
		/// </returns>
		internal entSegUsuario crearObjetoRevisado(DataRow row)
		{
			entSegUsuario obj = new entSegUsuario();
			if (row.Table.Columns.Contains(entSegUsuario.Fields.id_usuario.ToString()))
				obj.id_usuario = GetColumnType(row[entSegUsuario.Fields.id_usuario.ToString()], entSegUsuario.Fields.id_usuario);
			if (row.Table.Columns.Contains(entSegUsuario.Fields.id_departamento.ToString()))
				obj.id_departamento = GetColumnType(row[entSegUsuario.Fields.id_departamento.ToString()], entSegUsuario.Fields.id_departamento);
			if (row.Table.Columns.Contains(entSegUsuario.Fields.login.ToString()))
				obj.login = GetColumnType(row[entSegUsuario.Fields.login.ToString()], entSegUsuario.Fields.login);
			if (row.Table.Columns.Contains(entSegUsuario.Fields.password.ToString()))
				obj.password = GetColumnType(row[entSegUsuario.Fields.password.ToString()], entSegUsuario.Fields.password);
			if (row.Table.Columns.Contains(entSegUsuario.Fields.carnet.ToString()))
				obj.carnet = GetColumnType(row[entSegUsuario.Fields.carnet.ToString()], entSegUsuario.Fields.carnet);
			if (row.Table.Columns.Contains(entSegUsuario.Fields.nombre.ToString()))
				obj.nombre = GetColumnType(row[entSegUsuario.Fields.nombre.ToString()], entSegUsuario.Fields.nombre);
			if (row.Table.Columns.Contains(entSegUsuario.Fields.paterno.ToString()))
				obj.paterno = GetColumnType(row[entSegUsuario.Fields.paterno.ToString()], entSegUsuario.Fields.paterno);
			if (row.Table.Columns.Contains(entSegUsuario.Fields.materno.ToString()))
				obj.materno = GetColumnType(row[entSegUsuario.Fields.materno.ToString()], entSegUsuario.Fields.materno);
			if (row.Table.Columns.Contains(entSegUsuario.Fields.direccion.ToString()))
				obj.direccion = GetColumnType(row[entSegUsuario.Fields.direccion.ToString()], entSegUsuario.Fields.direccion);
			if (row.Table.Columns.Contains(entSegUsuario.Fields.telefono.ToString()))
				obj.telefono = GetColumnType(row[entSegUsuario.Fields.telefono.ToString()], entSegUsuario.Fields.telefono);
			if (row.Table.Columns.Contains(entSegUsuario.Fields.fecha_vigente.ToString()))
				obj.fecha_vigente = GetColumnType(row[entSegUsuario.Fields.fecha_vigente.ToString()], entSegUsuario.Fields.fecha_vigente);
			if (row.Table.Columns.Contains(entSegUsuario.Fields.foto.ToString()))
				obj.foto = GetColumnType(row[entSegUsuario.Fields.foto.ToString()], entSegUsuario.Fields.foto);
			if (row.Table.Columns.Contains(entSegUsuario.Fields.fec_nacimiento.ToString()))
				obj.fec_nacimiento = GetColumnType(row[entSegUsuario.Fields.fec_nacimiento.ToString()], entSegUsuario.Fields.fec_nacimiento);
			if (row.Table.Columns.Contains(entSegUsuario.Fields.correo.ToString()))
				obj.correo = GetColumnType(row[entSegUsuario.Fields.correo.ToString()], entSegUsuario.Fields.correo);
			if (row.Table.Columns.Contains(entSegUsuario.Fields.remember_token.ToString()))
				obj.remember_token = GetColumnType(row[entSegUsuario.Fields.remember_token.ToString()], entSegUsuario.Fields.remember_token);
			if (row.Table.Columns.Contains(entSegUsuario.Fields.apiestado.ToString()))
				obj.apiestado = GetColumnType(row[entSegUsuario.Fields.apiestado.ToString()], entSegUsuario.Fields.apiestado);
			if (row.Table.Columns.Contains(entSegUsuario.Fields.usucre.ToString()))
				obj.usucre = GetColumnType(row[entSegUsuario.Fields.usucre.ToString()], entSegUsuario.Fields.usucre);
			if (row.Table.Columns.Contains(entSegUsuario.Fields.feccre.ToString()))
				obj.feccre = GetColumnType(row[entSegUsuario.Fields.feccre.ToString()], entSegUsuario.Fields.feccre);
			if (row.Table.Columns.Contains(entSegUsuario.Fields.usumod.ToString()))
				obj.usumod = GetColumnType(row[entSegUsuario.Fields.usumod.ToString()], entSegUsuario.Fields.usumod);
			if (row.Table.Columns.Contains(entSegUsuario.Fields.fecmod.ToString()))
				obj.fecmod = GetColumnType(row[entSegUsuario.Fields.fecmod.ToString()], entSegUsuario.Fields.fecmod);
			return obj;
		}

		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtseg_usuario" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Lista de Objetos seg_usuario
		/// </returns>
		internal List<entSegUsuario> crearLista(DataTable dtseg_usuario)
		{
			List<entSegUsuario> list = new List<entSegUsuario>();
			
			entSegUsuario obj = new entSegUsuario();
			foreach (DataRow row in dtseg_usuario.Rows)
			{
				obj = crearObjeto(row);
				list.Add(obj);
			}
			return list;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable y con solo algunas columnas
		/// </summary>
		/// <param name="dtseg_usuario" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Lista de Objetos seg_usuario
		/// </returns>
		internal List<entSegUsuario> crearListaRevisada(DataTable dtseg_usuario)
		{
			List<entSegUsuario> list = new List<entSegUsuario>();
			
			entSegUsuario obj = new entSegUsuario();
			foreach (DataRow row in dtseg_usuario.Rows)
			{
				obj = crearObjetoRevisado(row);
				list.Add(obj);
			}
			return list;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtseg_usuario" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cola de Objetos seg_usuario
		/// </returns>
		internal Queue<entSegUsuario> crearCola(DataTable dtseg_usuario)
		{
			Queue<entSegUsuario> cola = new Queue<entSegUsuario>();
			
			entSegUsuario obj = new entSegUsuario();
			foreach (DataRow row in dtseg_usuario.Rows)
			{
				obj = crearObjeto(row);
				cola.Enqueue(obj);
			}
			return cola;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtseg_usuario" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Pila de Objetos seg_usuario
		/// </returns>
		internal Stack<entSegUsuario> crearPila(DataTable dtseg_usuario)
		{
			Stack<entSegUsuario> pila = new Stack<entSegUsuario>();
			
			entSegUsuario obj = new entSegUsuario();
			foreach (DataRow row in dtseg_usuario.Rows)
			{
				obj = crearObjeto(row);
				pila.Push(obj);
			}
			return pila;
		}
		
		/// <summary>
		/// 	 Funcion que crea un Dicionario a partir de un DataTable
		/// </summary>
		/// <param name="dtseg_usuario" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Diccionario de Objetos seg_usuario
		/// </returns>
		internal Dictionary<String, entSegUsuario> crearDiccionario(DataTable dtseg_usuario)
		{
			Dictionary<String, entSegUsuario>  miDic = new Dictionary<String, entSegUsuario>();
			
			entSegUsuario obj = new entSegUsuario();
			foreach (DataRow row in dtseg_usuario.Rows)
			{
				obj = crearObjeto(row);
				miDic.Add(obj.id_usuario.ToString(), obj);
			}
			return miDic;
		}
		
		/// <summary>
		/// 	 Funcion que crea un Dicionario a partir de un DataTable
		/// </summary>
		/// <param name="dtseg_usuario" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 HashTable de Objetos seg_usuario
		/// </returns>
		internal Hashtable crearHashTable(DataTable dtseg_usuario)
		{
			Hashtable miTabla = new Hashtable();
			
			entSegUsuario obj = new entSegUsuario();
			foreach (DataRow row in dtseg_usuario.Rows)
			{
				obj = crearObjeto(row);
				miTabla.Add(obj.id_usuario.ToString(), obj);
			}
			return miTabla;
		}
		
		/// <summary>
		/// 	 Funcion que crea un Dicionario a partir de un DataTable y solo con columnas existentes
		/// </summary>
		/// <param name="dtseg_usuario" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Diccionario de Objetos seg_usuario
		/// </returns>
		internal Dictionary<String, entSegUsuario> crearDiccionarioRevisado(DataTable dtseg_usuario)
		{
			Dictionary<String, entSegUsuario>  miDic = new Dictionary<String, entSegUsuario>();
			
			entSegUsuario obj = new entSegUsuario();
			foreach (DataRow row in dtseg_usuario.Rows)
			{
				obj = crearObjetoRevisado(row);
				miDic.Add(obj.id_usuario.ToString(), obj);
			}
			return miDic;
		}
		
		public void Dispose()
		{
			GC.SuppressFinalize(this);
		}
		
		protected void Finalize()
		{
			Dispose();
		}
		#endregion

	}
}

