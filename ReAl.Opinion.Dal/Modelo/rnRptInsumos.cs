#region 
/***********************************************************************************************************
	NOMBRE:       rnRptInsumos
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla rpt_insumos

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        22/10/2014  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using ReAl.Opinion.PgConn;
using ReAl.Opinion.Dal.Entidades;
using ReAl.Opinion.Dal.Interface;
using System.Windows.Forms;
#endregion

namespace ReAl.Opinion.Dal.Modelo
{
	public partial class rnRptInsumos: inRptInsumos
	{
		//Debe implementar la Interface (Alt + Shift + F10)

		#region inRptInsumos Members

		/// <summary>
		/// Metodo que devuelve el Script SQL de la Tabla
		/// </summary>
		/// <returns>Script SQL</returns>
		public string getTableScript()
		{
			TableClass tabla = new TableClass(typeof(entRptInsumos));
			return tabla.CreateTableScript();
		}
		
		/// <summary>
		/// Metodo para castear Dinamicamente un Tipo
		/// <summary>
		/// <param name="valor">Tipo a ser casteado</param>
		/// <param name="myField">Enum de la columna</param>
		/// <returns>Devuelve un objeto del Tipo de la columna especificada en el Enum</returns>
		public dynamic GetColumnType(object valor, entRptInsumos.Fields myField)
		{
			if (DBNull.Value.Equals(valor)) 
				return null;
			Type destino = typeof(entRptInsumos).GetProperty(myField.ToString()).PropertyType;
			var miTipo = Nullable.GetUnderlyingType(destino) ?? destino;
			return Convert.ChangeType(valor, miTipo);
		}

		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entRptInsumos a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo entRptInsumos que cumple con los filtros de los parametros
		/// </returns>
		public entRptInsumos ObtenerObjeto(int intgestion)
		{
			ArrayList arrColumnasWhere = new ArrayList();
		    arrColumnasWhere.Add(entRptInsumos.Fields.gestion.ToString());
		
			ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(intgestion);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entRptInsumos a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entRptInsumos que cumple con los filtros de los parametros
		/// </returns>
		public entRptInsumos ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entRptInsumos a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entRptInsumos que cumple con los filtros de los parametros
		/// </returns>
		public entRptInsumos ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entRptInsumos.Fields.gestion.ToString());
				arrColumnas.Add(entRptInsumos.Fields.mes.ToString());
				arrColumnas.Add(entRptInsumos.Fields.foto.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_departamento.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_upmipp.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_provincia.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_municipio.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_listado.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_boleta.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_persona.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_movimiento.ToString());
				arrColumnas.Add(entRptInsumos.Fields.control.ToString());
				arrColumnas.Add(entRptInsumos.Fields.minimo.ToString());
				arrColumnas.Add(entRptInsumos.Fields.maximo.ToString());
				arrColumnas.Add(entRptInsumos.Fields.departamento.ToString());
				arrColumnas.Add(entRptInsumos.Fields.provincia.ToString());
				arrColumnas.Add(entRptInsumos.Fields.municipio.ToString());
				arrColumnas.Add(entRptInsumos.Fields.comunidad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.login.ToString());
				arrColumnas.Add(entRptInsumos.Fields.producto_boleta.ToString());
				arrColumnas.Add(entRptInsumos.Fields.codigo_producto.ToString());
				arrColumnas.Add(entRptInsumos.Fields.codigo_ccp.ToString());
				arrColumnas.Add(entRptInsumos.Fields.informante.ToString());
				arrColumnas.Add(entRptInsumos.Fields.telefono.ToString());
				arrColumnas.Add(entRptInsumos.Fields.direccion.ToString());
				arrColumnas.Add(entRptInsumos.Fields.producto.ToString());
				arrColumnas.Add(entRptInsumos.Fields.caracteristicas.ToString());
				arrColumnas.Add(entRptInsumos.Fields.variedad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.origen.ToString());
				arrColumnas.Add(entRptInsumos.Fields.cantidad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.unidad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.precio.ToString());
				arrColumnas.Add(entRptInsumos.Fields.superficie.ToString());
				arrColumnas.Add(entRptInsumos.Fields.observaciones.ToString());
				arrColumnas.Add(entRptInsumos.Fields.minfec.ToString());
				arrColumnas.Add(entRptInsumos.Fields.maxfec.ToString());
				arrColumnas.Add(entRptInsumos.Fields.duracion.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entRptInsumos.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count == 1)
				{
					entRptInsumos obj = new entRptInsumos();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entRptInsumos a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entRptInsumos que cumple con los filtros de los parametros
		/// </returns>
		public entRptInsumos ObtenerObjeto(entRptInsumos.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entRptInsumos a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entRptInsumos que cumple con los filtros de los parametros
		/// </returns>
		public entRptInsumos ObtenerObjeto(entRptInsumos.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un Business Object del Tipo entRptInsumos a partir de su llave promaria
		/// </summary>
		/// <returns>
		/// 	Objeto del Tipo entRptInsumos
		/// </returns>
		public entRptInsumos ObtenerObjeto(int intgestion, ref cTrans localTrans )
		{
			ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(entRptInsumos.Fields.gestion.ToString());
		
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(intgestion);
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entRptInsumos a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion rpt_insumos
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entRptInsumos que cumple con los filtros de los parametros
		/// </returns>
		public entRptInsumos ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entRptInsumos a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion rpt_insumos
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entRptInsumos que cumple con los filtros de los parametros
		/// </returns>
		public entRptInsumos ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entRptInsumos.Fields.gestion.ToString());
				arrColumnas.Add(entRptInsumos.Fields.mes.ToString());
				arrColumnas.Add(entRptInsumos.Fields.foto.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_departamento.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_upmipp.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_provincia.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_municipio.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_listado.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_boleta.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_persona.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_movimiento.ToString());
				arrColumnas.Add(entRptInsumos.Fields.control.ToString());
				arrColumnas.Add(entRptInsumos.Fields.minimo.ToString());
				arrColumnas.Add(entRptInsumos.Fields.maximo.ToString());
				arrColumnas.Add(entRptInsumos.Fields.departamento.ToString());
				arrColumnas.Add(entRptInsumos.Fields.provincia.ToString());
				arrColumnas.Add(entRptInsumos.Fields.municipio.ToString());
				arrColumnas.Add(entRptInsumos.Fields.comunidad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.login.ToString());
				arrColumnas.Add(entRptInsumos.Fields.producto_boleta.ToString());
				arrColumnas.Add(entRptInsumos.Fields.codigo_producto.ToString());
				arrColumnas.Add(entRptInsumos.Fields.codigo_ccp.ToString());
				arrColumnas.Add(entRptInsumos.Fields.informante.ToString());
				arrColumnas.Add(entRptInsumos.Fields.telefono.ToString());
				arrColumnas.Add(entRptInsumos.Fields.direccion.ToString());
				arrColumnas.Add(entRptInsumos.Fields.producto.ToString());
				arrColumnas.Add(entRptInsumos.Fields.caracteristicas.ToString());
				arrColumnas.Add(entRptInsumos.Fields.variedad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.origen.ToString());
				arrColumnas.Add(entRptInsumos.Fields.cantidad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.unidad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.precio.ToString());
				arrColumnas.Add(entRptInsumos.Fields.superficie.ToString());
				arrColumnas.Add(entRptInsumos.Fields.observaciones.ToString());
				arrColumnas.Add(entRptInsumos.Fields.minfec.ToString());
				arrColumnas.Add(entRptInsumos.Fields.maxfec.ToString());
				arrColumnas.Add(entRptInsumos.Fields.duracion.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entRptInsumos.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count == 1)
				{
					entRptInsumos obj = new entRptInsumos();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entRptInsumos a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion rpt_insumos
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entRptInsumos que cumple con los filtros de los parametros
		/// </returns>
		public entRptInsumos ObtenerObjeto(entRptInsumos.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entRptInsumos a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion rpt_insumos
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entRptInsumos que cumple con los filtros de los parametros
		/// </returns>
		public entRptInsumos ObtenerObjeto(entRptInsumos.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptInsumos a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo List<entRptInsumos> que cumple con los filtros de los parametros
		/// </returns>
		public List<entRptInsumos> ObtenerLista()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptInsumos a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo List<entRptInsumos> que cumple con los filtros de los parametros
		/// </returns>
		public List<entRptInsumos> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptInsumos a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo List<entRptInsumos> que cumple con los filtros de los parametros
		/// </returns>
		public List<entRptInsumos> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entRptInsumos.Fields.gestion.ToString());
				arrColumnas.Add(entRptInsumos.Fields.mes.ToString());
				arrColumnas.Add(entRptInsumos.Fields.foto.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_departamento.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_upmipp.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_provincia.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_municipio.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_listado.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_boleta.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_persona.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_movimiento.ToString());
				arrColumnas.Add(entRptInsumos.Fields.control.ToString());
				arrColumnas.Add(entRptInsumos.Fields.minimo.ToString());
				arrColumnas.Add(entRptInsumos.Fields.maximo.ToString());
				arrColumnas.Add(entRptInsumos.Fields.departamento.ToString());
				arrColumnas.Add(entRptInsumos.Fields.provincia.ToString());
				arrColumnas.Add(entRptInsumos.Fields.municipio.ToString());
				arrColumnas.Add(entRptInsumos.Fields.comunidad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.login.ToString());
				arrColumnas.Add(entRptInsumos.Fields.producto_boleta.ToString());
				arrColumnas.Add(entRptInsumos.Fields.codigo_producto.ToString());
				arrColumnas.Add(entRptInsumos.Fields.codigo_ccp.ToString());
				arrColumnas.Add(entRptInsumos.Fields.informante.ToString());
				arrColumnas.Add(entRptInsumos.Fields.telefono.ToString());
				arrColumnas.Add(entRptInsumos.Fields.direccion.ToString());
				arrColumnas.Add(entRptInsumos.Fields.producto.ToString());
				arrColumnas.Add(entRptInsumos.Fields.caracteristicas.ToString());
				arrColumnas.Add(entRptInsumos.Fields.variedad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.origen.ToString());
				arrColumnas.Add(entRptInsumos.Fields.cantidad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.unidad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.precio.ToString());
				arrColumnas.Add(entRptInsumos.Fields.superficie.ToString());
				arrColumnas.Add(entRptInsumos.Fields.observaciones.ToString());
				arrColumnas.Add(entRptInsumos.Fields.minfec.ToString());
				arrColumnas.Add(entRptInsumos.Fields.maxfec.ToString());
				arrColumnas.Add(entRptInsumos.Fields.duracion.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entRptInsumos.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<entRptInsumos>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptInsumos a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo List<entRptInsumos> que cumple con los filtros de los parametros
		/// </returns>
		public List<entRptInsumos> ObtenerLista(entRptInsumos.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptInsumos a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo List<entRptInsumos> que cumple con los filtros de los parametros
		/// </returns>
		public List<entRptInsumos> ObtenerLista(entRptInsumos.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptInsumos a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion rpt_insumos
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo List<entRptInsumos> que cumple con los filtros de los parametros
		/// </returns>
		public List<entRptInsumos> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptInsumos a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion rpt_insumos
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo List<entRptInsumos> que cumple con los filtros de los parametros
		/// </returns>
		public List<entRptInsumos> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entRptInsumos.Fields.gestion.ToString());
				arrColumnas.Add(entRptInsumos.Fields.mes.ToString());
				arrColumnas.Add(entRptInsumos.Fields.foto.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_departamento.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_upmipp.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_provincia.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_municipio.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_listado.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_boleta.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_persona.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_movimiento.ToString());
				arrColumnas.Add(entRptInsumos.Fields.control.ToString());
				arrColumnas.Add(entRptInsumos.Fields.minimo.ToString());
				arrColumnas.Add(entRptInsumos.Fields.maximo.ToString());
				arrColumnas.Add(entRptInsumos.Fields.departamento.ToString());
				arrColumnas.Add(entRptInsumos.Fields.provincia.ToString());
				arrColumnas.Add(entRptInsumos.Fields.municipio.ToString());
				arrColumnas.Add(entRptInsumos.Fields.comunidad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.login.ToString());
				arrColumnas.Add(entRptInsumos.Fields.producto_boleta.ToString());
				arrColumnas.Add(entRptInsumos.Fields.codigo_producto.ToString());
				arrColumnas.Add(entRptInsumos.Fields.codigo_ccp.ToString());
				arrColumnas.Add(entRptInsumos.Fields.informante.ToString());
				arrColumnas.Add(entRptInsumos.Fields.telefono.ToString());
				arrColumnas.Add(entRptInsumos.Fields.direccion.ToString());
				arrColumnas.Add(entRptInsumos.Fields.producto.ToString());
				arrColumnas.Add(entRptInsumos.Fields.caracteristicas.ToString());
				arrColumnas.Add(entRptInsumos.Fields.variedad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.origen.ToString());
				arrColumnas.Add(entRptInsumos.Fields.cantidad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.unidad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.precio.ToString());
				arrColumnas.Add(entRptInsumos.Fields.superficie.ToString());
				arrColumnas.Add(entRptInsumos.Fields.observaciones.ToString());
				arrColumnas.Add(entRptInsumos.Fields.minfec.ToString());
				arrColumnas.Add(entRptInsumos.Fields.maxfec.ToString());
				arrColumnas.Add(entRptInsumos.Fields.duracion.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entRptInsumos.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<entRptInsumos>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptInsumos a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion rpt_insumos
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo List<entRptInsumos> que cumple con los filtros de los parametros
		/// </returns>
		public List<entRptInsumos> ObtenerLista(entRptInsumos.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptInsumos a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion rpt_insumos
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo List<entRptInsumos> que cumple con los filtros de los parametros
		/// </returns>
		public List<entRptInsumos> ObtenerLista(entRptInsumos.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptInsumos a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo Dictionary<String, entRptInsumos> que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entRptInsumos> ObtenerDiccionario()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptInsumos a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Dictionary<String, entRptInsumos> que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entRptInsumos> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptInsumos a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Dictionary<String, entRptInsumos> que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entRptInsumos> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entRptInsumos.Fields.gestion.ToString());
				arrColumnas.Add(entRptInsumos.Fields.mes.ToString());
				arrColumnas.Add(entRptInsumos.Fields.foto.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_departamento.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_upmipp.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_provincia.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_municipio.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_listado.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_boleta.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_persona.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_movimiento.ToString());
				arrColumnas.Add(entRptInsumos.Fields.control.ToString());
				arrColumnas.Add(entRptInsumos.Fields.minimo.ToString());
				arrColumnas.Add(entRptInsumos.Fields.maximo.ToString());
				arrColumnas.Add(entRptInsumos.Fields.departamento.ToString());
				arrColumnas.Add(entRptInsumos.Fields.provincia.ToString());
				arrColumnas.Add(entRptInsumos.Fields.municipio.ToString());
				arrColumnas.Add(entRptInsumos.Fields.comunidad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.login.ToString());
				arrColumnas.Add(entRptInsumos.Fields.producto_boleta.ToString());
				arrColumnas.Add(entRptInsumos.Fields.codigo_producto.ToString());
				arrColumnas.Add(entRptInsumos.Fields.codigo_ccp.ToString());
				arrColumnas.Add(entRptInsumos.Fields.informante.ToString());
				arrColumnas.Add(entRptInsumos.Fields.telefono.ToString());
				arrColumnas.Add(entRptInsumos.Fields.direccion.ToString());
				arrColumnas.Add(entRptInsumos.Fields.producto.ToString());
				arrColumnas.Add(entRptInsumos.Fields.caracteristicas.ToString());
				arrColumnas.Add(entRptInsumos.Fields.variedad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.origen.ToString());
				arrColumnas.Add(entRptInsumos.Fields.cantidad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.unidad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.precio.ToString());
				arrColumnas.Add(entRptInsumos.Fields.superficie.ToString());
				arrColumnas.Add(entRptInsumos.Fields.observaciones.ToString());
				arrColumnas.Add(entRptInsumos.Fields.minfec.ToString());
				arrColumnas.Add(entRptInsumos.Fields.maxfec.ToString());
				arrColumnas.Add(entRptInsumos.Fields.duracion.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entRptInsumos.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearDiccionario(table);
				}
				else
					return new Dictionary<string, entRptInsumos>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptInsumos a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Dictionary<String, entRptInsumos> que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entRptInsumos> ObtenerDiccionario(entRptInsumos.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptInsumos a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Dictionary<String, entRptInsumos> que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entRptInsumos> ObtenerDiccionario(entRptInsumos.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptInsumos a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion rpt_insumos
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Dictionary<String, entRptInsumos> que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entRptInsumos> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptInsumos a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion rpt_insumos
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Dictionary<String, entRptInsumos> que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entRptInsumos> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entRptInsumos.Fields.gestion.ToString());
				arrColumnas.Add(entRptInsumos.Fields.mes.ToString());
				arrColumnas.Add(entRptInsumos.Fields.foto.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_departamento.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_upmipp.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_provincia.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_municipio.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_listado.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_boleta.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_persona.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_movimiento.ToString());
				arrColumnas.Add(entRptInsumos.Fields.control.ToString());
				arrColumnas.Add(entRptInsumos.Fields.minimo.ToString());
				arrColumnas.Add(entRptInsumos.Fields.maximo.ToString());
				arrColumnas.Add(entRptInsumos.Fields.departamento.ToString());
				arrColumnas.Add(entRptInsumos.Fields.provincia.ToString());
				arrColumnas.Add(entRptInsumos.Fields.municipio.ToString());
				arrColumnas.Add(entRptInsumos.Fields.comunidad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.login.ToString());
				arrColumnas.Add(entRptInsumos.Fields.producto_boleta.ToString());
				arrColumnas.Add(entRptInsumos.Fields.codigo_producto.ToString());
				arrColumnas.Add(entRptInsumos.Fields.codigo_ccp.ToString());
				arrColumnas.Add(entRptInsumos.Fields.informante.ToString());
				arrColumnas.Add(entRptInsumos.Fields.telefono.ToString());
				arrColumnas.Add(entRptInsumos.Fields.direccion.ToString());
				arrColumnas.Add(entRptInsumos.Fields.producto.ToString());
				arrColumnas.Add(entRptInsumos.Fields.caracteristicas.ToString());
				arrColumnas.Add(entRptInsumos.Fields.variedad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.origen.ToString());
				arrColumnas.Add(entRptInsumos.Fields.cantidad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.unidad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.precio.ToString());
				arrColumnas.Add(entRptInsumos.Fields.superficie.ToString());
				arrColumnas.Add(entRptInsumos.Fields.observaciones.ToString());
				arrColumnas.Add(entRptInsumos.Fields.minfec.ToString());
				arrColumnas.Add(entRptInsumos.Fields.maxfec.ToString());
				arrColumnas.Add(entRptInsumos.Fields.duracion.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entRptInsumos.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearDiccionario(table);
				}
				else
					return new Dictionary<string, entRptInsumos>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptInsumos a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion rpt_insumos
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Dictionary<String, entRptInsumos> que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entRptInsumos> ObtenerDiccionario(entRptInsumos.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptInsumos a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion rpt_insumos
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Dictionary<String, entRptInsumos> que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entRptInsumos> ObtenerDiccionario(entRptInsumos.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	 Funcion que obtiene la llave primaria unica de la tabla rpt_insumos a partir de una cadena
		/// </summary>
		/// <param name="cod" type="string">
		///     <para>
		/// 		 Cadena desde la que se construye el identificador unico de la tabla rpt_insumos
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Identificador unico de la tabla rpt_insumos
		/// </returns>
		public string CreatePK(string[] args)
		{
			return args[0];
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla rpt_insumos a partir de una clase del tipo Erpt_insumos
		/// </summary>
		/// <param name="obj" type="Entidades.entRptInsumos">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla rpt_insumosrpt_insumos
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionrpt_insumos
		/// </returns>
		public bool Insert(entRptInsumos obj)
		{
			throw new Exception("No existe el Procedimiento Almacenado Sprpt_insumosIns.");
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla rpt_insumos a partir de una clase del tipo Erpt_insumos
		/// </summary>
		/// <param name="obj" type="Entidades.entRptInsumos">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla rpt_insumosrpt_insumos
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion rpt_insumos
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionrpt_insumos
		/// </returns>
		public bool Insert(entRptInsumos obj, ref cTrans localTrans)
		{
			throw new Exception("No existe el Procedimiento Almacenado Sprpt_insumosIns.");
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla rpt_insumos a partir de una clase del tipo Erpt_insumos
		/// </summary>
		/// <param name="obj" type="Entidades.entRptInsumos">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla rpt_insumosrpt_insumos
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionrpt_insumos
		/// </returns>
		public int Update(entRptInsumos obj)
		{
			throw new Exception("No existe el Procedimiento Almacenado Sprpt_insumosUpd.");
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla rpt_insumos a partir de una clase del tipo Erpt_insumos
		/// </summary>
		/// <param name="obj" type="Entidades.entRptInsumos">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla rpt_insumosrpt_insumos
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion rpt_insumos
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionrpt_insumos
		/// </returns>
		public int Update(entRptInsumos obj, ref cTrans localTrans)
		{
			throw new Exception("No existe el Procedimiento Almacenado Sprpt_insumosUpd.");
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla rpt_insumos a partir de una clase del tipo Erpt_insumos
		/// </summary>
		/// <param name="obj" type="Entidades.entRptInsumos">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla rpt_insumosrpt_insumos
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionrpt_insumos
		/// </returns>
		public int Delete(entRptInsumos obj)
		{
			throw new Exception("No existe el Procedimiento Almacenado Sprpt_insumosDel.");
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla rpt_insumos a partir de una clase del tipo Erpt_insumos
		/// </summary>
		/// <param name="obj" type="Entidades.entRptInsumos">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla rpt_insumosrpt_insumos
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion rpt_insumos
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionrpt_insumos
		/// </returns>
		public int Delete(entRptInsumos obj, ref cTrans localTrans)
		{
			throw new Exception("No existe el Procedimiento Almacenado Sprpt_insumosDel.");
		}

		/// <summary>
		/// 	Funcion que inserta o actualiza un registro un nuevo registro en la tabla rpt_insumos a partir de una clase del tipo Erpt_insumos
		/// </summary>
		/// <param name="obj" type="Entidades.entRptInsumos">
		///     <para>
		/// 		 Clase desde la que se van a insertar o actualizar los valores a la tabla rpt_insumosrpt_insumos
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionrpt_insumos
		/// </returns>
		public int InsertUpdate(entRptInsumos obj)
		{
			try
			{
				bool esInsertar = true;
				
				
				if (esInsertar)
					return Insert(obj) ? 1 : 0;
				else
					return Update(obj);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta o actualiza un registro un nuevo registro en la tabla rpt_insumos a partir de una clase del tipo Erpt_insumos
		/// </summary>
		/// <param name="obj" type="Entidades.entRptInsumos">
		///     <para>
		/// 		 Clase desde la que se van a insertar o actualizar los valores a la tabla rpt_insumosrpt_insumos
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion rpt_insumos
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionrpt_insumos
		/// </returns>
		public int InsertUpdate(entRptInsumos obj, ref cTrans localTrans)
		{
			try
			{
				bool esInsertar = false;
				
				
				if (esInsertar)
					return Insert(obj, ref localTrans) ? 1 : 0;
				else
					return Update(obj, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla rpt_insumos
		/// </summary>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de rpt_insumos
		/// </returns>
		public DataTable NuevoDataTable()
		{
			try
			{
				DataTable table = new DataTable ();
				DataColumn dc;
				dc = new DataColumn(entRptInsumos.Fields.gestion.ToString(),typeof(entRptInsumos).GetProperty(entRptInsumos.Fields.gestion.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptInsumos.Fields.mes.ToString(),typeof(entRptInsumos).GetProperty(entRptInsumos.Fields.mes.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptInsumos.Fields.foto.ToString(),typeof(entRptInsumos).GetProperty(entRptInsumos.Fields.foto.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptInsumos.Fields.id_departamento.ToString(),typeof(entRptInsumos).GetProperty(entRptInsumos.Fields.id_departamento.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptInsumos.Fields.id_upmipp.ToString(),typeof(entRptInsumos).GetProperty(entRptInsumos.Fields.id_upmipp.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptInsumos.Fields.id_provincia.ToString(),typeof(entRptInsumos).GetProperty(entRptInsumos.Fields.id_provincia.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptInsumos.Fields.id_municipio.ToString(),typeof(entRptInsumos).GetProperty(entRptInsumos.Fields.id_municipio.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptInsumos.Fields.id_listado.ToString(),typeof(entRptInsumos).GetProperty(entRptInsumos.Fields.id_listado.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptInsumos.Fields.id_boleta.ToString(),typeof(entRptInsumos).GetProperty(entRptInsumos.Fields.id_boleta.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptInsumos.Fields.id_persona.ToString(),typeof(entRptInsumos).GetProperty(entRptInsumos.Fields.id_persona.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptInsumos.Fields.id_movimiento.ToString(),typeof(entRptInsumos).GetProperty(entRptInsumos.Fields.id_movimiento.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptInsumos.Fields.control.ToString(),typeof(entRptInsumos).GetProperty(entRptInsumos.Fields.control.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptInsumos.Fields.minimo.ToString(),typeof(entRptInsumos).GetProperty(entRptInsumos.Fields.minimo.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptInsumos.Fields.maximo.ToString(),typeof(entRptInsumos).GetProperty(entRptInsumos.Fields.maximo.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptInsumos.Fields.departamento.ToString(),typeof(entRptInsumos).GetProperty(entRptInsumos.Fields.departamento.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptInsumos.Fields.provincia.ToString(),typeof(entRptInsumos).GetProperty(entRptInsumos.Fields.provincia.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptInsumos.Fields.municipio.ToString(),typeof(entRptInsumos).GetProperty(entRptInsumos.Fields.municipio.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptInsumos.Fields.comunidad.ToString(),typeof(entRptInsumos).GetProperty(entRptInsumos.Fields.comunidad.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptInsumos.Fields.login.ToString(),typeof(entRptInsumos).GetProperty(entRptInsumos.Fields.login.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptInsumos.Fields.producto_boleta.ToString(),typeof(entRptInsumos).GetProperty(entRptInsumos.Fields.producto_boleta.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptInsumos.Fields.codigo_producto.ToString(),typeof(entRptInsumos).GetProperty(entRptInsumos.Fields.codigo_producto.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptInsumos.Fields.codigo_ccp.ToString(),typeof(entRptInsumos).GetProperty(entRptInsumos.Fields.codigo_ccp.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptInsumos.Fields.informante.ToString(),typeof(entRptInsumos).GetProperty(entRptInsumos.Fields.informante.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptInsumos.Fields.telefono.ToString(),typeof(entRptInsumos).GetProperty(entRptInsumos.Fields.telefono.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptInsumos.Fields.direccion.ToString(),typeof(entRptInsumos).GetProperty(entRptInsumos.Fields.direccion.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptInsumos.Fields.producto.ToString(),typeof(entRptInsumos).GetProperty(entRptInsumos.Fields.producto.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptInsumos.Fields.caracteristicas.ToString(),typeof(entRptInsumos).GetProperty(entRptInsumos.Fields.caracteristicas.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptInsumos.Fields.variedad.ToString(),typeof(entRptInsumos).GetProperty(entRptInsumos.Fields.variedad.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptInsumos.Fields.origen.ToString(),typeof(entRptInsumos).GetProperty(entRptInsumos.Fields.origen.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptInsumos.Fields.cantidad.ToString(),typeof(entRptInsumos).GetProperty(entRptInsumos.Fields.cantidad.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptInsumos.Fields.unidad.ToString(),typeof(entRptInsumos).GetProperty(entRptInsumos.Fields.unidad.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptInsumos.Fields.precio.ToString(),typeof(entRptInsumos).GetProperty(entRptInsumos.Fields.precio.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptInsumos.Fields.superficie.ToString(),typeof(entRptInsumos).GetProperty(entRptInsumos.Fields.superficie.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptInsumos.Fields.observaciones.ToString(),typeof(entRptInsumos).GetProperty(entRptInsumos.Fields.observaciones.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptInsumos.Fields.minfec.ToString(),typeof(entRptInsumos).GetProperty(entRptInsumos.Fields.minfec.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptInsumos.Fields.maxfec.ToString(),typeof(entRptInsumos).GetProperty(entRptInsumos.Fields.maxfec.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptInsumos.Fields.duracion.ToString(),typeof(entRptInsumos).GetProperty(entRptInsumos.Fields.duracion.ToString()).PropertyType);
				table.Columns.Add(dc);

				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que genera un DataTable con determinadas columnas de una rpt_insumos
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de rpt_insumos
		/// </returns>
		public DataTable NuevoDataTable(ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'2'");
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entRptInsumos.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla rpt_insumos
		/// </summary>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de rpt_insumos
		/// </returns>
		public DataTable CargarDataTable()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return CargarDataTable(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla y n condicion WHERE rpt_insumos
		/// </summary>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de rpt_insumos
		/// </returns>
		public DataTable CargarDataTable(String strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return CargarDataTable(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla rpt_insumos
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de rpt_insumos
		/// </returns>
		public DataTable CargarDataTable(ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				return CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla rpt_insumos
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de rpt_insumos
		/// </returns>
		public DataTable CargarDataTable(ArrayList arrColumnas, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				return CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla rpt_insumos
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de rpt_insumos
		/// </returns>
		public DataTable CargarDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla rpt_insumos
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de rpt_insumos
		/// </returns>
		public DataTable CargarDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return CargarDataTable(arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla rpt_insumos
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de rpt_insumos
		/// </returns>
		public DataTable CargarDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entRptInsumos.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla rpt_insumos
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de rpt_insumos
		/// </returns>
		public DataTable CargarDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entRptInsumos.Fields.gestion.ToString());
				arrColumnas.Add(entRptInsumos.Fields.mes.ToString());
				arrColumnas.Add(entRptInsumos.Fields.foto.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_departamento.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_upmipp.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_provincia.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_municipio.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_listado.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_boleta.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_persona.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_movimiento.ToString());
				arrColumnas.Add(entRptInsumos.Fields.control.ToString());
				arrColumnas.Add(entRptInsumos.Fields.minimo.ToString());
				arrColumnas.Add(entRptInsumos.Fields.maximo.ToString());
				arrColumnas.Add(entRptInsumos.Fields.departamento.ToString());
				arrColumnas.Add(entRptInsumos.Fields.provincia.ToString());
				arrColumnas.Add(entRptInsumos.Fields.municipio.ToString());
				arrColumnas.Add(entRptInsumos.Fields.comunidad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.login.ToString());
				arrColumnas.Add(entRptInsumos.Fields.producto_boleta.ToString());
				arrColumnas.Add(entRptInsumos.Fields.codigo_producto.ToString());
				arrColumnas.Add(entRptInsumos.Fields.codigo_ccp.ToString());
				arrColumnas.Add(entRptInsumos.Fields.informante.ToString());
				arrColumnas.Add(entRptInsumos.Fields.telefono.ToString());
				arrColumnas.Add(entRptInsumos.Fields.direccion.ToString());
				arrColumnas.Add(entRptInsumos.Fields.producto.ToString());
				arrColumnas.Add(entRptInsumos.Fields.caracteristicas.ToString());
				arrColumnas.Add(entRptInsumos.Fields.variedad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.origen.ToString());
				arrColumnas.Add(entRptInsumos.Fields.cantidad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.unidad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.precio.ToString());
				arrColumnas.Add(entRptInsumos.Fields.superficie.ToString());
				arrColumnas.Add(entRptInsumos.Fields.observaciones.ToString());
				arrColumnas.Add(entRptInsumos.Fields.minfec.ToString());
				arrColumnas.Add(entRptInsumos.Fields.maxfec.ToString());
				arrColumnas.Add(entRptInsumos.Fields.duracion.ToString());
				
				return CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="searchField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable CargarDataTable(entRptInsumos.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return CargarDataTable(arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="searchField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable CargarDataTable(entRptInsumos.Fields searchField, object searchValue, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return CargarDataTable(arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable CargarDataTable(ArrayList arrColumnas, entRptInsumos.Fields searchField, object searchValue, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable CargarDataTable(ArrayList arrColumnas, entRptInsumos.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla rpt_insumos
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de rpt_insumos
		/// </returns>
		public DataTable CargarDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entRptInsumos.Fields.gestion.ToString());
				arrColumnas.Add(entRptInsumos.Fields.mes.ToString());
				arrColumnas.Add(entRptInsumos.Fields.foto.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_departamento.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_upmipp.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_provincia.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_municipio.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_listado.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_boleta.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_persona.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_movimiento.ToString());
				arrColumnas.Add(entRptInsumos.Fields.control.ToString());
				arrColumnas.Add(entRptInsumos.Fields.minimo.ToString());
				arrColumnas.Add(entRptInsumos.Fields.maximo.ToString());
				arrColumnas.Add(entRptInsumos.Fields.departamento.ToString());
				arrColumnas.Add(entRptInsumos.Fields.provincia.ToString());
				arrColumnas.Add(entRptInsumos.Fields.municipio.ToString());
				arrColumnas.Add(entRptInsumos.Fields.comunidad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.login.ToString());
				arrColumnas.Add(entRptInsumos.Fields.producto_boleta.ToString());
				arrColumnas.Add(entRptInsumos.Fields.codigo_producto.ToString());
				arrColumnas.Add(entRptInsumos.Fields.codigo_ccp.ToString());
				arrColumnas.Add(entRptInsumos.Fields.informante.ToString());
				arrColumnas.Add(entRptInsumos.Fields.telefono.ToString());
				arrColumnas.Add(entRptInsumos.Fields.direccion.ToString());
				arrColumnas.Add(entRptInsumos.Fields.producto.ToString());
				arrColumnas.Add(entRptInsumos.Fields.caracteristicas.ToString());
				arrColumnas.Add(entRptInsumos.Fields.variedad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.origen.ToString());
				arrColumnas.Add(entRptInsumos.Fields.cantidad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.unidad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.precio.ToString());
				arrColumnas.Add(entRptInsumos.Fields.superficie.ToString());
				arrColumnas.Add(entRptInsumos.Fields.observaciones.ToString());
				arrColumnas.Add(entRptInsumos.Fields.minfec.ToString());
				arrColumnas.Add(entRptInsumos.Fields.maxfec.ToString());
				arrColumnas.Add(entRptInsumos.Fields.duracion.ToString());
				return CargarDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla rpt_insumos
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de rpt_insumos
		/// </returns>
		public DataTable CargarDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return CargarDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla rpt_insumos
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de rpt_insumos
		/// </returns>
		public DataTable CargarDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DataTable table = local.cargarDataTableOr(cParametros.schema + entRptInsumos.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla rpt_insumos
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla rpt_insumosrpt_insumos
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla rpt_insumos
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla rpt_insumosrpt_insumos
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla rpt_insumos
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla rpt_insumosrpt_insumos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla rpt_insumos
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla rpt_insumosrpt_insumos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entRptInsumos.Fields.gestion.ToString());
				arrColumnas.Add(entRptInsumos.Fields.mes.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla rpt_insumos
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla rpt_insumosrpt_insumos
		///     </para>
		/// </param>
		/// <param name="valueField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entRptInsumos.Fields valueField, entRptInsumos.Fields textField)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla rpt_insumos
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla rpt_insumosrpt_insumos
		///     </para>
		/// </param>
		/// <param name="valueField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entRptInsumos.Fields valueField, entRptInsumos.Fields textField, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla rpt_insumos
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla rpt_insumosrpt_insumos
		///     </para>
		/// </param>
		/// <param name="valueField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entRptInsumos.Fields valueField, String textField)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla rpt_insumos
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla rpt_insumosrpt_insumos
		///     </para>
		/// </param>
		/// <param name="valueField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entRptInsumos.Fields valueField, String textField, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla rpt_insumos
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla rpt_insumosrpt_insumos
		///     </para>
		/// </param>
		/// <param name="valueField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entRptInsumos.Fields valueField, entRptInsumos.Fields textField, entRptInsumos.Fields searchField, object searchValue)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField.ToString(), searchField, searchValue);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla rpt_insumos
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla rpt_insumosrpt_insumos
		///     </para>
		/// </param>
		/// <param name="valueField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entRptInsumos.Fields valueField, String textField, entRptInsumos.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla rpt_insumos
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla rpt_insumosrpt_insumos
		///     </para>
		/// </param>
		/// <param name="valueField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entRptInsumos.Fields valueField, entRptInsumos.Fields textField, entRptInsumos.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla rpt_insumos
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla rpt_insumosrpt_insumos
		///     </para>
		/// </param>
		/// <param name="valueField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entRptInsumos.Fields valueField, String textField, entRptInsumos.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla rpt_insumos
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla rpt_insumosrpt_insumos
		///     </para>
		/// </param>
		/// <param name="valueField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entRptInsumos.Fields valueField, entRptInsumos.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla rpt_insumos
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla rpt_insumosrpt_insumos
		///     </para>
		/// </param>
		/// <param name="valueField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entRptInsumos.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla rpt_insumos
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla rpt_insumosrpt_insumos
		///     </para>
		/// </param>
		/// <param name="valueField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entRptInsumos.Fields valueField, entRptInsumos.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla rpt_insumos
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla rpt_insumosrpt_insumos
		///     </para>
		/// </param>
		/// <param name="valueField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entRptInsumos.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla rpt_insumos
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla rpt_insumosrpt_insumos
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entRptInsumos.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Columns.Count > 0)
				{
					cmb.ValueMember = table.Columns[0].ColumnName;
					cmb.DisplayMember = table.Columns[1].ColumnName;
					cmb.DataSource = table;

				}
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla rpt_insumos
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entRptInsumos.Fields.gestion.ToString());
				arrColumnas.Add(entRptInsumos.Fields.mes.ToString());
				arrColumnas.Add(entRptInsumos.Fields.foto.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_departamento.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_upmipp.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_provincia.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_municipio.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_listado.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_boleta.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_persona.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_movimiento.ToString());
				arrColumnas.Add(entRptInsumos.Fields.control.ToString());
				arrColumnas.Add(entRptInsumos.Fields.minimo.ToString());
				arrColumnas.Add(entRptInsumos.Fields.maximo.ToString());
				arrColumnas.Add(entRptInsumos.Fields.departamento.ToString());
				arrColumnas.Add(entRptInsumos.Fields.provincia.ToString());
				arrColumnas.Add(entRptInsumos.Fields.municipio.ToString());
				arrColumnas.Add(entRptInsumos.Fields.comunidad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.login.ToString());
				arrColumnas.Add(entRptInsumos.Fields.producto_boleta.ToString());
				arrColumnas.Add(entRptInsumos.Fields.codigo_producto.ToString());
				arrColumnas.Add(entRptInsumos.Fields.codigo_ccp.ToString());
				arrColumnas.Add(entRptInsumos.Fields.informante.ToString());
				arrColumnas.Add(entRptInsumos.Fields.telefono.ToString());
				arrColumnas.Add(entRptInsumos.Fields.direccion.ToString());
				arrColumnas.Add(entRptInsumos.Fields.producto.ToString());
				arrColumnas.Add(entRptInsumos.Fields.caracteristicas.ToString());
				arrColumnas.Add(entRptInsumos.Fields.variedad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.origen.ToString());
				arrColumnas.Add(entRptInsumos.Fields.cantidad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.unidad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.precio.ToString());
				arrColumnas.Add(entRptInsumos.Fields.superficie.ToString());
				arrColumnas.Add(entRptInsumos.Fields.observaciones.ToString());
				arrColumnas.Add(entRptInsumos.Fields.minfec.ToString());
				arrColumnas.Add(entRptInsumos.Fields.maxfec.ToString());
				arrColumnas.Add(entRptInsumos.Fields.duracion.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla rpt_insumos
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		  Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entRptInsumos.Fields.gestion.ToString());
				arrColumnas.Add(entRptInsumos.Fields.mes.ToString());
				arrColumnas.Add(entRptInsumos.Fields.foto.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_departamento.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_upmipp.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_provincia.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_municipio.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_listado.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_boleta.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_persona.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_movimiento.ToString());
				arrColumnas.Add(entRptInsumos.Fields.control.ToString());
				arrColumnas.Add(entRptInsumos.Fields.minimo.ToString());
				arrColumnas.Add(entRptInsumos.Fields.maximo.ToString());
				arrColumnas.Add(entRptInsumos.Fields.departamento.ToString());
				arrColumnas.Add(entRptInsumos.Fields.provincia.ToString());
				arrColumnas.Add(entRptInsumos.Fields.municipio.ToString());
				arrColumnas.Add(entRptInsumos.Fields.comunidad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.login.ToString());
				arrColumnas.Add(entRptInsumos.Fields.producto_boleta.ToString());
				arrColumnas.Add(entRptInsumos.Fields.codigo_producto.ToString());
				arrColumnas.Add(entRptInsumos.Fields.codigo_ccp.ToString());
				arrColumnas.Add(entRptInsumos.Fields.informante.ToString());
				arrColumnas.Add(entRptInsumos.Fields.telefono.ToString());
				arrColumnas.Add(entRptInsumos.Fields.direccion.ToString());
				arrColumnas.Add(entRptInsumos.Fields.producto.ToString());
				arrColumnas.Add(entRptInsumos.Fields.caracteristicas.ToString());
				arrColumnas.Add(entRptInsumos.Fields.variedad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.origen.ToString());
				arrColumnas.Add(entRptInsumos.Fields.cantidad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.unidad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.precio.ToString());
				arrColumnas.Add(entRptInsumos.Fields.superficie.ToString());
				arrColumnas.Add(entRptInsumos.Fields.observaciones.ToString());
				arrColumnas.Add(entRptInsumos.Fields.minfec.ToString());
				arrColumnas.Add(entRptInsumos.Fields.maxfec.ToString());
				arrColumnas.Add(entRptInsumos.Fields.duracion.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla rpt_insumos
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla rpt_insumos
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla rpt_insumos
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entRptInsumos.Fields.gestion.ToString());
				arrColumnas.Add(entRptInsumos.Fields.mes.ToString());
				arrColumnas.Add(entRptInsumos.Fields.foto.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_departamento.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_upmipp.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_provincia.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_municipio.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_listado.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_boleta.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_persona.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_movimiento.ToString());
				arrColumnas.Add(entRptInsumos.Fields.control.ToString());
				arrColumnas.Add(entRptInsumos.Fields.minimo.ToString());
				arrColumnas.Add(entRptInsumos.Fields.maximo.ToString());
				arrColumnas.Add(entRptInsumos.Fields.departamento.ToString());
				arrColumnas.Add(entRptInsumos.Fields.provincia.ToString());
				arrColumnas.Add(entRptInsumos.Fields.municipio.ToString());
				arrColumnas.Add(entRptInsumos.Fields.comunidad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.login.ToString());
				arrColumnas.Add(entRptInsumos.Fields.producto_boleta.ToString());
				arrColumnas.Add(entRptInsumos.Fields.codigo_producto.ToString());
				arrColumnas.Add(entRptInsumos.Fields.codigo_ccp.ToString());
				arrColumnas.Add(entRptInsumos.Fields.informante.ToString());
				arrColumnas.Add(entRptInsumos.Fields.telefono.ToString());
				arrColumnas.Add(entRptInsumos.Fields.direccion.ToString());
				arrColumnas.Add(entRptInsumos.Fields.producto.ToString());
				arrColumnas.Add(entRptInsumos.Fields.caracteristicas.ToString());
				arrColumnas.Add(entRptInsumos.Fields.variedad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.origen.ToString());
				arrColumnas.Add(entRptInsumos.Fields.cantidad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.unidad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.precio.ToString());
				arrColumnas.Add(entRptInsumos.Fields.superficie.ToString());
				arrColumnas.Add(entRptInsumos.Fields.observaciones.ToString());
				arrColumnas.Add(entRptInsumos.Fields.minfec.ToString());
				arrColumnas.Add(entRptInsumos.Fields.maxfec.ToString());
				arrColumnas.Add(entRptInsumos.Fields.duracion.ToString());
			
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla rpt_insumos
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entRptInsumos.Fields.gestion.ToString());
				arrColumnas.Add(entRptInsumos.Fields.mes.ToString());
				arrColumnas.Add(entRptInsumos.Fields.foto.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_departamento.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_upmipp.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_provincia.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_municipio.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_listado.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_boleta.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_persona.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_movimiento.ToString());
				arrColumnas.Add(entRptInsumos.Fields.control.ToString());
				arrColumnas.Add(entRptInsumos.Fields.minimo.ToString());
				arrColumnas.Add(entRptInsumos.Fields.maximo.ToString());
				arrColumnas.Add(entRptInsumos.Fields.departamento.ToString());
				arrColumnas.Add(entRptInsumos.Fields.provincia.ToString());
				arrColumnas.Add(entRptInsumos.Fields.municipio.ToString());
				arrColumnas.Add(entRptInsumos.Fields.comunidad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.login.ToString());
				arrColumnas.Add(entRptInsumos.Fields.producto_boleta.ToString());
				arrColumnas.Add(entRptInsumos.Fields.codigo_producto.ToString());
				arrColumnas.Add(entRptInsumos.Fields.codigo_ccp.ToString());
				arrColumnas.Add(entRptInsumos.Fields.informante.ToString());
				arrColumnas.Add(entRptInsumos.Fields.telefono.ToString());
				arrColumnas.Add(entRptInsumos.Fields.direccion.ToString());
				arrColumnas.Add(entRptInsumos.Fields.producto.ToString());
				arrColumnas.Add(entRptInsumos.Fields.caracteristicas.ToString());
				arrColumnas.Add(entRptInsumos.Fields.variedad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.origen.ToString());
				arrColumnas.Add(entRptInsumos.Fields.cantidad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.unidad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.precio.ToString());
				arrColumnas.Add(entRptInsumos.Fields.superficie.ToString());
				arrColumnas.Add(entRptInsumos.Fields.observaciones.ToString());
				arrColumnas.Add(entRptInsumos.Fields.minfec.ToString());
				arrColumnas.Add(entRptInsumos.Fields.maxfec.ToString());
				arrColumnas.Add(entRptInsumos.Fields.duracion.ToString());
			
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla rpt_insumos
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla rpt_insumos
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DbDataReader dsReader = local.cargarDataReaderAnd(cParametros.schema + entRptInsumos.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				dtg.DataSource = dsReader;

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla rpt_insumos
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, entRptInsumos.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entRptInsumos.Fields.gestion.ToString());
				arrColumnas.Add(entRptInsumos.Fields.mes.ToString());
				arrColumnas.Add(entRptInsumos.Fields.foto.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_departamento.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_upmipp.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_provincia.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_municipio.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_listado.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_boleta.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_persona.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_movimiento.ToString());
				arrColumnas.Add(entRptInsumos.Fields.control.ToString());
				arrColumnas.Add(entRptInsumos.Fields.minimo.ToString());
				arrColumnas.Add(entRptInsumos.Fields.maximo.ToString());
				arrColumnas.Add(entRptInsumos.Fields.departamento.ToString());
				arrColumnas.Add(entRptInsumos.Fields.provincia.ToString());
				arrColumnas.Add(entRptInsumos.Fields.municipio.ToString());
				arrColumnas.Add(entRptInsumos.Fields.comunidad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.login.ToString());
				arrColumnas.Add(entRptInsumos.Fields.producto_boleta.ToString());
				arrColumnas.Add(entRptInsumos.Fields.codigo_producto.ToString());
				arrColumnas.Add(entRptInsumos.Fields.codigo_ccp.ToString());
				arrColumnas.Add(entRptInsumos.Fields.informante.ToString());
				arrColumnas.Add(entRptInsumos.Fields.telefono.ToString());
				arrColumnas.Add(entRptInsumos.Fields.direccion.ToString());
				arrColumnas.Add(entRptInsumos.Fields.producto.ToString());
				arrColumnas.Add(entRptInsumos.Fields.caracteristicas.ToString());
				arrColumnas.Add(entRptInsumos.Fields.variedad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.origen.ToString());
				arrColumnas.Add(entRptInsumos.Fields.cantidad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.unidad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.precio.ToString());
				arrColumnas.Add(entRptInsumos.Fields.superficie.ToString());
				arrColumnas.Add(entRptInsumos.Fields.observaciones.ToString());
				arrColumnas.Add(entRptInsumos.Fields.minfec.ToString());
				arrColumnas.Add(entRptInsumos.Fields.maxfec.ToString());
				arrColumnas.Add(entRptInsumos.Fields.duracion.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla rpt_insumos
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, entRptInsumos.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entRptInsumos.Fields.gestion.ToString());
				arrColumnas.Add(entRptInsumos.Fields.mes.ToString());
				arrColumnas.Add(entRptInsumos.Fields.foto.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_departamento.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_upmipp.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_provincia.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_municipio.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_listado.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_boleta.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_persona.ToString());
				arrColumnas.Add(entRptInsumos.Fields.id_movimiento.ToString());
				arrColumnas.Add(entRptInsumos.Fields.control.ToString());
				arrColumnas.Add(entRptInsumos.Fields.minimo.ToString());
				arrColumnas.Add(entRptInsumos.Fields.maximo.ToString());
				arrColumnas.Add(entRptInsumos.Fields.departamento.ToString());
				arrColumnas.Add(entRptInsumos.Fields.provincia.ToString());
				arrColumnas.Add(entRptInsumos.Fields.municipio.ToString());
				arrColumnas.Add(entRptInsumos.Fields.comunidad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.login.ToString());
				arrColumnas.Add(entRptInsumos.Fields.producto_boleta.ToString());
				arrColumnas.Add(entRptInsumos.Fields.codigo_producto.ToString());
				arrColumnas.Add(entRptInsumos.Fields.codigo_ccp.ToString());
				arrColumnas.Add(entRptInsumos.Fields.informante.ToString());
				arrColumnas.Add(entRptInsumos.Fields.telefono.ToString());
				arrColumnas.Add(entRptInsumos.Fields.direccion.ToString());
				arrColumnas.Add(entRptInsumos.Fields.producto.ToString());
				arrColumnas.Add(entRptInsumos.Fields.caracteristicas.ToString());
				arrColumnas.Add(entRptInsumos.Fields.variedad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.origen.ToString());
				arrColumnas.Add(entRptInsumos.Fields.cantidad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.unidad.ToString());
				arrColumnas.Add(entRptInsumos.Fields.precio.ToString());
				arrColumnas.Add(entRptInsumos.Fields.superficie.ToString());
				arrColumnas.Add(entRptInsumos.Fields.observaciones.ToString());
				arrColumnas.Add(entRptInsumos.Fields.minfec.ToString());
				arrColumnas.Add(entRptInsumos.Fields.maxfec.ToString());
				arrColumnas.Add(entRptInsumos.Fields.duracion.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla rpt_insumos
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entRptInsumos.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla rpt_insumos
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entRptInsumos.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla rpt_insumos
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarDataGridOr(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla rpt_insumos
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DbDataReader dsReader = local.cargarDataReaderOr(cParametros.schema + entRptInsumos.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				dtg.DataSource = dsReader;

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entRptInsumos que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesCount(entRptInsumos.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entRptInsumos que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesCount(entRptInsumos.Fields refField, entRptInsumos.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entRptInsumos que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesCount(entRptInsumos.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("count(" + refField + ")");
				DataTable dtTemp = CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return dtTemp.Rows[0][0];
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entRptInsumos que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesMin(entRptInsumos.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entRptInsumos que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesMin(entRptInsumos.Fields refField, entRptInsumos.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entRptInsumos que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesMin(entRptInsumos.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("min(" + refField + ")");
				DataTable dtTemp = CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return dtTemp.Rows[0][0];
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entRptInsumos que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesMax(entRptInsumos.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entRptInsumos que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesMax(entRptInsumos.Fields refField, entRptInsumos.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entRptInsumos que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesMax(entRptInsumos.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("max(" + refField + ")");
				DataTable dtTemp = CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return dtTemp.Rows[0][0];
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entRptInsumos que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesSum(entRptInsumos.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entRptInsumos que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesSum(entRptInsumos.Fields refField, entRptInsumos.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entRptInsumos que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesSum(entRptInsumos.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("sum(" + refField + ")");
				DataTable dtTemp = CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return dtTemp.Rows[0][0];
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entRptInsumos que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesAvg(entRptInsumos.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entRptInsumos que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesAvg(entRptInsumos.Fields refField, entRptInsumos.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entRptInsumos que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesAvg(entRptInsumos.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("avg(" + refField + ")");
				DataTable dtTemp = CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return dtTemp.Rows[0][0];
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion

		/// <summary>
		/// 	 Funcion que devuelve un objeto a partir de un DataRow
		/// </summary>
		/// <param name="row" type="System.Data.DataRow">
		///     <para>
		/// 		 DataRow con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Objeto rpt_insumos
		/// </returns>
		internal entRptInsumos crearObjeto(DataRow row)
		{
			entRptInsumos obj = new entRptInsumos();
			obj.gestion = GetColumnType(row[entRptInsumos.Fields.gestion.ToString()], entRptInsumos.Fields.gestion);
			obj.mes = GetColumnType(row[entRptInsumos.Fields.mes.ToString()], entRptInsumos.Fields.mes);
			obj.foto = GetColumnType(row[entRptInsumos.Fields.foto.ToString()], entRptInsumos.Fields.foto);
			obj.id_departamento = GetColumnType(row[entRptInsumos.Fields.id_departamento.ToString()], entRptInsumos.Fields.id_departamento);
			obj.id_upmipp = GetColumnType(row[entRptInsumos.Fields.id_upmipp.ToString()], entRptInsumos.Fields.id_upmipp);
			obj.id_provincia = GetColumnType(row[entRptInsumos.Fields.id_provincia.ToString()], entRptInsumos.Fields.id_provincia);
			obj.id_municipio = GetColumnType(row[entRptInsumos.Fields.id_municipio.ToString()], entRptInsumos.Fields.id_municipio);
			obj.id_listado = GetColumnType(row[entRptInsumos.Fields.id_listado.ToString()], entRptInsumos.Fields.id_listado);
			obj.id_boleta = GetColumnType(row[entRptInsumos.Fields.id_boleta.ToString()], entRptInsumos.Fields.id_boleta);
			obj.id_persona = GetColumnType(row[entRptInsumos.Fields.id_persona.ToString()], entRptInsumos.Fields.id_persona);
			obj.id_movimiento = GetColumnType(row[entRptInsumos.Fields.id_movimiento.ToString()], entRptInsumos.Fields.id_movimiento);
			obj.control = GetColumnType(row[entRptInsumos.Fields.control.ToString()], entRptInsumos.Fields.control);
			obj.minimo = GetColumnType(row[entRptInsumos.Fields.minimo.ToString()], entRptInsumos.Fields.minimo);
			obj.maximo = GetColumnType(row[entRptInsumos.Fields.maximo.ToString()], entRptInsumos.Fields.maximo);
			obj.departamento = GetColumnType(row[entRptInsumos.Fields.departamento.ToString()], entRptInsumos.Fields.departamento);
			obj.provincia = GetColumnType(row[entRptInsumos.Fields.provincia.ToString()], entRptInsumos.Fields.provincia);
			obj.municipio = GetColumnType(row[entRptInsumos.Fields.municipio.ToString()], entRptInsumos.Fields.municipio);
			obj.comunidad = GetColumnType(row[entRptInsumos.Fields.comunidad.ToString()], entRptInsumos.Fields.comunidad);
			obj.login = GetColumnType(row[entRptInsumos.Fields.login.ToString()], entRptInsumos.Fields.login);
			obj.producto_boleta = GetColumnType(row[entRptInsumos.Fields.producto_boleta.ToString()], entRptInsumos.Fields.producto_boleta);
			obj.codigo_producto = GetColumnType(row[entRptInsumos.Fields.codigo_producto.ToString()], entRptInsumos.Fields.codigo_producto);
			obj.codigo_ccp = GetColumnType(row[entRptInsumos.Fields.codigo_ccp.ToString()], entRptInsumos.Fields.codigo_ccp);
			obj.informante = GetColumnType(row[entRptInsumos.Fields.informante.ToString()], entRptInsumos.Fields.informante);
			obj.telefono = GetColumnType(row[entRptInsumos.Fields.telefono.ToString()], entRptInsumos.Fields.telefono);
			obj.direccion = GetColumnType(row[entRptInsumos.Fields.direccion.ToString()], entRptInsumos.Fields.direccion);
			obj.producto = GetColumnType(row[entRptInsumos.Fields.producto.ToString()], entRptInsumos.Fields.producto);
			obj.caracteristicas = GetColumnType(row[entRptInsumos.Fields.caracteristicas.ToString()], entRptInsumos.Fields.caracteristicas);
			obj.variedad = GetColumnType(row[entRptInsumos.Fields.variedad.ToString()], entRptInsumos.Fields.variedad);
			obj.origen = GetColumnType(row[entRptInsumos.Fields.origen.ToString()], entRptInsumos.Fields.origen);
			obj.cantidad = GetColumnType(row[entRptInsumos.Fields.cantidad.ToString()], entRptInsumos.Fields.cantidad);
			obj.unidad = GetColumnType(row[entRptInsumos.Fields.unidad.ToString()], entRptInsumos.Fields.unidad);
			obj.precio = GetColumnType(row[entRptInsumos.Fields.precio.ToString()], entRptInsumos.Fields.precio);
			obj.superficie = GetColumnType(row[entRptInsumos.Fields.superficie.ToString()], entRptInsumos.Fields.superficie);
			obj.observaciones = GetColumnType(row[entRptInsumos.Fields.observaciones.ToString()], entRptInsumos.Fields.observaciones);
			obj.minfec = GetColumnType(row[entRptInsumos.Fields.minfec.ToString()], entRptInsumos.Fields.minfec);
			obj.maxfec = GetColumnType(row[entRptInsumos.Fields.maxfec.ToString()], entRptInsumos.Fields.maxfec);
			obj.duracion = GetColumnType(row[entRptInsumos.Fields.duracion.ToString()], entRptInsumos.Fields.duracion);
			return obj;
		}

		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtrpt_insumos" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Lista de Objetos rpt_insumos
		/// </returns>
		internal List<entRptInsumos> crearLista(DataTable dtrpt_insumos)
		{
			List<entRptInsumos> list = new List<entRptInsumos>();
			
			entRptInsumos obj = new entRptInsumos();
			foreach (DataRow row in dtrpt_insumos.Rows)
			{
				obj = crearObjeto(row);
				list.Add(obj);
			}
			return list;
		}
		
		/// <summary>
		/// 	 FUncion que crea un Dicionario a partir de un DataTable
		/// </summary>
		/// <param name="dtrpt_insumos" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Diccionario de Objetos rpt_insumos
		/// </returns>
		internal Dictionary<String, entRptInsumos> crearDiccionario(DataTable dtrpt_insumos)
		{
			Dictionary<String, entRptInsumos>  miDic = new Dictionary<String, entRptInsumos>();
			
			entRptInsumos obj = new entRptInsumos();
			foreach (DataRow row in dtrpt_insumos.Rows)
			{
				obj = crearObjeto(row);
				miDic.Add(obj.GetHashCode().ToString(), obj);
			}
			return miDic;
		}
		
		public void Dispose()
		{
			GC.SuppressFinalize(this);
		}
		
		protected void Finalize()
		{
			Dispose();
		}
	}
}

