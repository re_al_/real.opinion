#region 
/***********************************************************************************************************
	NOMBRE:       rnEncEncuesta
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla enc_encuesta

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using ReAl.Opinion.Dal;
using ReAl.Opinion.PgConn;
using ReAl.Opinion.Dal.Entidades;
using ReAl.Opinion.Dal.Interface;
using System.Windows.Forms;
#endregion

namespace ReAl.Opinion.Dal.Modelo
{
	public partial class rnEncEncuesta: inEncEncuesta
	{
		//Debe implementar la Interface (Alt + Shift + F10)

		#region inEncEncuesta Members

		#region Reflection

		/// <summary>
		/// Metodo que devuelve el Script SQL de la Tabla
		/// </summary>
		/// <returns>Script SQL</returns>
		public string getTableScript()
		{
			TableClass tabla = new TableClass(typeof(entEncEncuesta));
			return tabla.CreateTableScript();
		}
		
		/// <summary>
		/// Metodo para castear Dinamicamente un Tipo
		/// <summary>
		/// <param name="valor">Tipo a ser casteado</param>
		/// <param name="myField">Enum de la columna</param>
		/// <returns>Devuelve un objeto del Tipo de la columna especificada en el Enum</returns>
		public dynamic GetColumnType(object valor, entEncEncuesta.Fields myField)
		{
			if (DBNull.Value.Equals(valor)) 
				return null;
			Type destino = typeof(entEncEncuesta).GetProperty(myField.ToString()).PropertyType;
			var miTipo = Nullable.GetUnderlyingType(destino) ?? destino;
			return Convert.ChangeType(valor, miTipo);
		}

		/// <summary>
		/// 	 Funcion que obtiene la llave primaria unica de la tabla enc_encuesta a partir de una cadena
		/// </summary>
		/// <param name="args" type="string[]">
		///     <para>
		/// 		 Cadena desde la que se construye el identificador unico de la tabla enc_encuesta
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Identificador unico de la tabla enc_encuesta
		/// </returns>
		public string CreatePK(string[] args)
		{
			return args[0];
		}
		
		#endregion 

		#region ObtenerObjeto

		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo entEncEncuesta que cumple con los filtros de los parametros
		/// </returns>
		public entEncEncuesta ObtenerObjeto(Int64 Int64id_encuesta)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(entEncEncuesta.Fields.id_encuesta.ToString());
		
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'" + Int64id_encuesta + "'");
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncEncuesta que cumple con los filtros de los parametros
		/// </returns>
		public entEncEncuesta ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncEncuesta que cumple con los filtros de los parametros
		/// </returns>
		public entEncEncuesta ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncEncuesta.Fields.id_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_tab_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_informante.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_movimiento.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.codigo_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.observacion.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.latitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.longitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_last.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fila.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usucre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.feccre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usumod.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fecmod.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entEncEncuesta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count == 1)
				{
					entEncEncuesta obj = new entEncEncuesta();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncEncuesta que cumple con los filtros de los parametros
		/// </returns>
		public entEncEncuesta ObtenerObjeto(Hashtable htbFiltro)
		{
			return ObtenerObjeto(htbFiltro, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncEncuesta que cumple con los filtros de los parametros
		/// </returns>
		public entEncEncuesta ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncEncuesta.Fields.id_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_tab_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_informante.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_movimiento.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.codigo_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.observacion.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.latitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.longitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_last.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fila.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usucre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.feccre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usumod.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entEncEncuesta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count == 1)
				{
					entEncEncuesta obj = new entEncEncuesta();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncEncuesta que cumple con los filtros de los parametros
		/// </returns>
		public entEncEncuesta ObtenerObjeto(entEncEncuesta.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncEncuesta que cumple con los filtros de los parametros
		/// </returns>
		public entEncEncuesta ObtenerObjeto(entEncEncuesta.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un Business Object del Tipo entEncEncuesta a partir de su llave promaria
		/// </summary>
		/// <returns>
		/// 	Objeto del Tipo entEncEncuesta
		/// </returns>
		public entEncEncuesta ObtenerObjeto(Int64 Int64id_encuesta, ref cTrans localTrans )
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(entEncEncuesta.Fields.id_encuesta.ToString());
		
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'" + Int64id_encuesta + "'");
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_encuesta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncEncuesta que cumple con los filtros de los parametros
		/// </returns>
		public entEncEncuesta ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_encuesta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncEncuesta que cumple con los filtros de los parametros
		/// </returns>
		public entEncEncuesta ObtenerObjeto(Hashtable htbFiltro, ref cTrans localTrans)
		{
			return ObtenerObjeto(htbFiltro, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_encuesta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncEncuesta que cumple con los filtros de los parametros
		/// </returns>
		public entEncEncuesta ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales,  ref cTrans localTrans)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_encuesta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncEncuesta que cumple con los filtros de los parametros
		/// </returns>
		public entEncEncuesta ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncEncuesta.Fields.id_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_tab_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_informante.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_movimiento.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.codigo_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.observacion.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.latitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.longitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_last.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fila.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usucre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.feccre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usumod.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entEncEncuesta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count == 1)
				{
					entEncEncuesta obj = new entEncEncuesta();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_encuesta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncEncuesta que cumple con los filtros de los parametros
		/// </returns>
		public entEncEncuesta ObtenerObjeto(entEncEncuesta.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_encuesta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncEncuesta que cumple con los filtros de los parametros
		/// </returns>
		public entEncEncuesta ObtenerObjeto(entEncEncuesta.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion

		#region ObtenerLista

		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncEncuesta> ObtenerLista()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncEncuesta> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncEncuesta> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncEncuesta.Fields.id_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_tab_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_informante.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_movimiento.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.codigo_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.observacion.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.latitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.longitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_last.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fila.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usucre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.feccre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usumod.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fecmod.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entEncEncuesta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<entEncEncuesta>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncEncuesta> ObtenerLista(entEncEncuesta.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncEncuesta> ObtenerLista(entEncEncuesta.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_encuesta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncEncuesta> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_encuesta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncEncuesta> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncEncuesta.Fields.id_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_tab_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_informante.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_movimiento.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.codigo_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.observacion.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.latitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.longitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_last.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fila.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usucre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.feccre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usumod.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fecmod.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entEncEncuesta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<entEncEncuesta>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_encuesta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncEncuesta> ObtenerLista(entEncEncuesta.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_encuesta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncEncuesta> ObtenerLista(entEncEncuesta.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncEncuesta> ObtenerLista(Hashtable htbFiltro)
		{
			return ObtenerLista(htbFiltro, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncEncuesta> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncEncuesta.Fields.id_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_tab_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_informante.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_movimiento.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.codigo_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.observacion.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.latitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.longitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_last.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fila.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usucre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.feccre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usumod.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entEncEncuesta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<entEncEncuesta>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_encuesta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncEncuesta> ObtenerLista(Hashtable htbFiltro, ref cTrans localTrans)
		{
			return ObtenerLista(htbFiltro, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_encuesta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncEncuesta> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncEncuesta.Fields.id_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_tab_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_informante.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_movimiento.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.codigo_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.observacion.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.latitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.longitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_last.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fila.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usucre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.feccre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usumod.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entEncEncuesta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<entEncEncuesta>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncEncuesta> ObtenerListaDesdeVista(String strVista)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncEncuesta> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncEncuesta> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<entEncEncuesta>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncEncuesta> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro)
		{
			return ObtenerListaDesdeVista(strVista, htbFiltro, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncEncuesta> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<entEncEncuesta>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncEncuesta> ObtenerListaDesdeVista(String strVista, entEncEncuesta.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncEncuesta> ObtenerListaDesdeVista(String strVista, entEncEncuesta.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_encuesta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncEncuesta> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_encuesta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncEncuesta> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<entEncEncuesta>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_encuesta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncEncuesta> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref cTrans localTrans)
		{
			return ObtenerListaDesdeVista(strVista, htbFiltro, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_encuesta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncEncuesta> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<entEncEncuesta>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_encuesta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncEncuesta> ObtenerListaDesdeVista(String strVista, entEncEncuesta.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_encuesta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncEncuesta> ObtenerListaDesdeVista(String strVista, entEncEncuesta.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion 

		#region ObtenerCola y Obtener Pila

		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entEncEncuesta> ObtenerCola()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entEncEncuesta> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entEncEncuesta> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncEncuesta.Fields.id_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_tab_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_informante.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_movimiento.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.codigo_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.observacion.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.latitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.longitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_last.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fila.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usucre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.feccre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usumod.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fecmod.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entEncEncuesta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearCola(table);
				}
				else
					return new Queue<entEncEncuesta>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entEncEncuesta> ObtenerCola(entEncEncuesta.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entEncEncuesta> ObtenerCola(entEncEncuesta.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_encuesta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entEncEncuesta> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_encuesta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entEncEncuesta> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncEncuesta.Fields.id_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_tab_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_informante.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_movimiento.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.codigo_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.observacion.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.latitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.longitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_last.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fila.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usucre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.feccre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usumod.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fecmod.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entEncEncuesta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearCola(table);
				}
				else
					return new Queue<entEncEncuesta>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_encuesta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entEncEncuesta> ObtenerCola(entEncEncuesta.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_encuesta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entEncEncuesta> ObtenerCola(entEncEncuesta.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entEncEncuesta> ObtenerPila()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entEncEncuesta> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entEncEncuesta> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncEncuesta.Fields.id_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_tab_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_informante.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_movimiento.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.codigo_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.observacion.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.latitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.longitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_last.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fila.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usucre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.feccre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usumod.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fecmod.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entEncEncuesta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearPila(table);
				}
				else
					return new Stack<entEncEncuesta>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entEncEncuesta> ObtenerPila(entEncEncuesta.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entEncEncuesta> ObtenerPila(entEncEncuesta.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_encuesta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entEncEncuesta> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_encuesta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entEncEncuesta> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncEncuesta.Fields.id_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_tab_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_informante.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_movimiento.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.codigo_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.observacion.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.latitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.longitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_last.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fila.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usucre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.feccre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usumod.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fecmod.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entEncEncuesta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearPila(table);
				}
				else
					return new Stack<entEncEncuesta>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_encuesta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entEncEncuesta> ObtenerPila(entEncEncuesta.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_encuesta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entEncEncuesta> ObtenerPila(entEncEncuesta.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion 

		#region ObtenerDataTable

		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla enc_encuesta
		/// </summary>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de enc_encuesta
		/// </returns>
		public DataTable NuevoDataTable()
		{
			try
			{
				DataTable table = new DataTable ();
				DataColumn dc;
				dc = new DataColumn(entEncEncuesta.Fields.id_encuesta.ToString(),typeof(entEncEncuesta).GetProperty(entEncEncuesta.Fields.id_encuesta.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entEncEncuesta.Fields.id_tab_encuesta.ToString(),typeof(entEncEncuesta).GetProperty(entEncEncuesta.Fields.id_tab_encuesta.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entEncEncuesta.Fields.id_informante.ToString(),typeof(entEncEncuesta).GetProperty(entEncEncuesta.Fields.id_informante.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entEncEncuesta.Fields.id_movimiento.ToString(),typeof(entEncEncuesta).GetProperty(entEncEncuesta.Fields.id_movimiento.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entEncEncuesta.Fields.id_pregunta.ToString(),typeof(entEncEncuesta).GetProperty(entEncEncuesta.Fields.id_pregunta.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entEncEncuesta.Fields.id_respuesta.ToString(),typeof(entEncEncuesta).GetProperty(entEncEncuesta.Fields.id_respuesta.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entEncEncuesta.Fields.codigo_respuesta.ToString(),typeof(entEncEncuesta).GetProperty(entEncEncuesta.Fields.codigo_respuesta.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entEncEncuesta.Fields.respuesta.ToString(),typeof(entEncEncuesta).GetProperty(entEncEncuesta.Fields.respuesta.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entEncEncuesta.Fields.observacion.ToString(),typeof(entEncEncuesta).GetProperty(entEncEncuesta.Fields.observacion.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entEncEncuesta.Fields.latitud.ToString(),typeof(entEncEncuesta).GetProperty(entEncEncuesta.Fields.latitud.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entEncEncuesta.Fields.longitud.ToString(),typeof(entEncEncuesta).GetProperty(entEncEncuesta.Fields.longitud.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entEncEncuesta.Fields.id_last.ToString(),typeof(entEncEncuesta).GetProperty(entEncEncuesta.Fields.id_last.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entEncEncuesta.Fields.fila.ToString(),typeof(entEncEncuesta).GetProperty(entEncEncuesta.Fields.fila.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entEncEncuesta.Fields.apiestado.ToString(),typeof(entEncEncuesta).GetProperty(entEncEncuesta.Fields.apiestado.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entEncEncuesta.Fields.usucre.ToString(),typeof(entEncEncuesta).GetProperty(entEncEncuesta.Fields.usucre.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entEncEncuesta.Fields.feccre.ToString(),typeof(entEncEncuesta).GetProperty(entEncEncuesta.Fields.feccre.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entEncEncuesta.Fields.usumod.ToString(),typeof(entEncEncuesta).GetProperty(entEncEncuesta.Fields.usumod.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entEncEncuesta.Fields.fecmod.ToString(),typeof(entEncEncuesta).GetProperty(entEncEncuesta.Fields.fecmod.ToString()).PropertyType);
				table.Columns.Add(dc);

				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que genera un DataTable con determinadas columnas de una enc_encuesta
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de enc_encuesta
		/// </returns>
		public DataTable NuevoDataTable(ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'2'");
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entEncEncuesta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla enc_encuesta
		/// </summary>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de enc_encuesta
		/// </returns>
		public DataTable ObtenerDataTable()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDataTable(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla y n condicion WHERE enc_encuesta
		/// </summary>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de enc_encuesta
		/// </returns>
		public DataTable ObtenerDataTable(String strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla enc_encuesta
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de enc_encuesta
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla enc_encuesta
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de enc_encuesta
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla enc_encuesta
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de enc_encuesta
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla enc_encuesta
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de enc_encuesta
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro)
		{
			try
			{
				return ObtenerDataTable(arrColumnas, htbFiltro, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla enc_encuesta
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de enc_encuesta
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla enc_encuesta
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de enc_encuesta
		/// </returns>
		public DataTable ObtenerDataTable(Hashtable htbFiltro)
		{
			try
			{
				return ObtenerDataTable(htbFiltro, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla enc_encuesta
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de enc_encuesta
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entEncEncuesta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla enc_encuesta
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de enc_encuesta
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entEncEncuesta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla enc_encuesta
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de enc_encuesta
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncEncuesta.Fields.id_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_tab_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_informante.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_movimiento.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.codigo_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.observacion.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.latitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.longitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_last.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fila.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usucre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.feccre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usumod.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fecmod.ToString());
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla enc_encuesta
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de enc_encuesta
		/// </returns>
		public DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncEncuesta.Fields.id_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_tab_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_informante.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_movimiento.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.codigo_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.observacion.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.latitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.longitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_last.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fila.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usucre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.feccre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usumod.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fecmod.ToString());
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla enc_encuesta
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de enc_encuesta
		/// </returns>
		public DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncEncuesta.Fields.id_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_tab_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_informante.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_movimiento.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.codigo_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.observacion.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.latitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.longitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_last.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fila.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usucre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.feccre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usumod.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fecmod.ToString());
				return ObtenerDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla enc_encuesta
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de enc_encuesta
		/// </returns>
		public DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return ObtenerDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla enc_encuesta
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de enc_encuesta
		/// </returns>
		public DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DataTable table = local.cargarDataTableOr(cParametros.schema + entEncEncuesta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="searchField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(entEncEncuesta.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="searchField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(entEncEncuesta.Fields searchField, object searchValue, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, entEncEncuesta.Fields searchField, object searchValue, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, entEncEncuesta.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		

		#endregion 

		#region ObtenerDiccionario

		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entEncEncuesta> ObtenerDiccionario()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entEncEncuesta> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entEncEncuesta> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncEncuesta.Fields.id_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_tab_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_informante.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_movimiento.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.codigo_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.observacion.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.latitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.longitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_last.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fila.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usucre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.feccre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usumod.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fecmod.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entEncEncuesta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearDiccionario(table);
				}
				else
					return new Dictionary<string, entEncEncuesta>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entEncEncuesta> ObtenerDiccionario(entEncEncuesta.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entEncEncuesta> ObtenerDiccionario(entEncEncuesta.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_encuesta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entEncEncuesta> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_encuesta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entEncEncuesta> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncEncuesta.Fields.id_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_tab_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_informante.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_movimiento.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.codigo_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.observacion.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.latitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.longitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_last.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fila.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usucre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.feccre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usumod.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fecmod.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entEncEncuesta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearDiccionario(table);
				}
				else
					return new Dictionary<string, entEncEncuesta>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_encuesta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entEncEncuesta> ObtenerDiccionario(entEncEncuesta.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncEncuesta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_encuesta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entEncEncuesta> ObtenerDiccionario(entEncEncuesta.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion 

		#region FuncionesAgregadas

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncEncuesta que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesCount(entEncEncuesta.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncEncuesta que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesCount(entEncEncuesta.Fields refField, entEncEncuesta.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncEncuesta que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesCount(entEncEncuesta.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("count(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncEncuesta que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMin(entEncEncuesta.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncEncuesta que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMin(entEncEncuesta.Fields refField, entEncEncuesta.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncEncuesta que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMin(entEncEncuesta.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("min(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncEncuesta que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMax(entEncEncuesta.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncEncuesta que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMax(entEncEncuesta.Fields refField, entEncEncuesta.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncEncuesta que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMax(entEncEncuesta.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("max(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncEncuesta que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesSum(entEncEncuesta.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncEncuesta que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesSum(entEncEncuesta.Fields refField, entEncEncuesta.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncEncuesta que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesSum(entEncEncuesta.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("sum(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncEncuesta que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesAvg(entEncEncuesta.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncEncuesta que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesAvg(entEncEncuesta.Fields refField, entEncEncuesta.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncEncuesta que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesAvg(entEncEncuesta.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("avg(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion 

		#region ABMs

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla enc_encuesta a partir de una clase del tipo Eenc_encuesta
		/// </summary>
		/// <param name="obj" type="Entidades.entEncEncuesta">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla enc_encuestaenc_encuesta
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionenc_encuesta
		/// </returns>
		public bool Insert(entEncEncuesta obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(entEncEncuesta.Fields.id_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_tab_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_informante.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_movimiento.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.codigo_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.observacion.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.latitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.longitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_last.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fila.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usucre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.feccre.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.id_encuesta == null ? null : "'" + obj.id_encuesta + "'");
				arrValores.Add(obj.id_tab_encuesta);
				arrValores.Add(obj.id_informante == null ? null : "'" + obj.id_informante + "'");
				arrValores.Add(obj.id_movimiento);
				arrValores.Add(obj.id_pregunta);
				arrValores.Add(obj.id_respuesta);
				arrValores.Add(obj.codigo_respuesta == null ? null : "'" + obj.codigo_respuesta + "'");
				arrValores.Add(obj.respuesta == null ? null : "'" + obj.respuesta + "'");
				arrValores.Add(obj.observacion == null ? null : "'" + obj.observacion + "'");
				arrValores.Add(obj.latitud);
				arrValores.Add(obj.longitud);
				arrValores.Add(obj.id_last);
				arrValores.Add(obj.fila);
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(cParametros.parFormatoFechaHora) + "'");

			
				cConn local = new cConn();
				return local.insertBD(entEncEncuesta.strNombreTabla, arrColumnas, arrValores);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla enc_encuesta a partir de una clase del tipo Eenc_encuesta
		/// </summary>
		/// <param name="obj" type="Entidades.entEncEncuesta">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla enc_encuestaenc_encuesta
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_encuesta
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionenc_encuesta
		/// </returns>
		public bool Insert(entEncEncuesta obj, ref cTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(entEncEncuesta.Fields.id_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_tab_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_informante.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_movimiento.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.codigo_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.observacion.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.latitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.longitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_last.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fila.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usucre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.feccre.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.id_encuesta == null ? null : "'" + obj.id_encuesta + "'");
				arrValores.Add(obj.id_tab_encuesta);
				arrValores.Add(obj.id_informante == null ? null : "'" + obj.id_informante + "'");
				arrValores.Add(obj.id_movimiento);
				arrValores.Add(obj.id_pregunta);
				arrValores.Add(obj.id_respuesta);
				arrValores.Add(obj.codigo_respuesta == null ? null : "'" + obj.codigo_respuesta + "'");
				arrValores.Add(obj.respuesta == null ? null : "'" + obj.respuesta + "'");
				arrValores.Add(obj.observacion == null ? null : "'" + obj.observacion + "'");
				arrValores.Add(obj.latitud);
				arrValores.Add(obj.longitud);
				arrValores.Add(obj.id_last);
				arrValores.Add(obj.fila);
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(cParametros.parFormatoFechaHora) + "'");

			
				cConn local = new cConn();
				return local.insertBD(entEncEncuesta.strNombreTabla, arrColumnas, arrValores, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla enc_encuesta a partir de una clase del tipo Eenc_encuesta
		/// </summary>
		/// <param name="obj" type="Entidades.entEncEncuesta">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla enc_encuestaenc_encuesta
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionenc_encuesta
		/// </returns>
		public bool InsertIdentity(entEncEncuesta obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(entEncEncuesta.Fields.id_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_tab_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_informante.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_movimiento.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.codigo_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.observacion.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.latitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.longitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_last.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fila.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usucre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.feccre.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.id_encuesta == null ? null : "'" + obj.id_encuesta + "'");
				arrValores.Add(obj.id_tab_encuesta);
				arrValores.Add(obj.id_informante == null ? null : "'" + obj.id_informante + "'");
				arrValores.Add(obj.id_movimiento);
				arrValores.Add(obj.id_pregunta);
				arrValores.Add(obj.id_respuesta);
				arrValores.Add(obj.codigo_respuesta == null ? null : "'" + obj.codigo_respuesta + "'");
				arrValores.Add(obj.respuesta == null ? null : "'" + obj.respuesta + "'");
				arrValores.Add(obj.observacion == null ? null : "'" + obj.observacion + "'");
				arrValores.Add(obj.latitud);
				arrValores.Add(obj.longitud);
				arrValores.Add(obj.id_last);
				arrValores.Add(obj.fila);
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(cParametros.parFormatoFechaHora) + "'");

			
				cConn local = new cConn();
				int intIdentidad = -1;
				bool res = local.insertBD(entEncEncuesta.strNombreTabla, arrColumnas, arrValores, ref intIdentidad);
				obj.id_encuesta = intIdentidad;
				return res;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla enc_encuesta a partir de una clase del tipo Eenc_encuesta
		/// </summary>
		/// <param name="obj" type="Entidades.entEncEncuesta">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla enc_encuestaenc_encuesta
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_encuesta
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionenc_encuesta
		/// </returns>
		public bool InsertIdentity(entEncEncuesta obj, ref cTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(entEncEncuesta.Fields.id_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_tab_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_informante.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_movimiento.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.codigo_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.observacion.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.latitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.longitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_last.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fila.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usucre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.feccre.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.id_encuesta == null ? null : "'" + obj.id_encuesta + "'");
				arrValores.Add(obj.id_tab_encuesta);
				arrValores.Add(obj.id_informante == null ? null : "'" + obj.id_informante + "'");
				arrValores.Add(obj.id_movimiento);
				arrValores.Add(obj.id_pregunta);
				arrValores.Add(obj.id_respuesta);
				arrValores.Add(obj.codigo_respuesta == null ? null : "'" + obj.codigo_respuesta + "'");
				arrValores.Add(obj.respuesta == null ? null : "'" + obj.respuesta + "'");
				arrValores.Add(obj.observacion == null ? null : "'" + obj.observacion + "'");
				arrValores.Add(obj.latitud);
				arrValores.Add(obj.longitud);
				arrValores.Add(obj.id_last);
				arrValores.Add(obj.fila);
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(cParametros.parFormatoFechaHora) + "'");

			
				cConn local = new cConn();
				int intIdentidad = -1;
				bool res = local.insertBD(entEncEncuesta.strNombreTabla, arrColumnas, arrValores, ref intIdentidad, ref localTrans);
				obj.id_encuesta = intIdentidad;
				return res;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla enc_encuesta a partir de una clase del tipo Eenc_encuesta
		/// </summary>
		/// <param name="obj" type="Entidades.entEncEncuesta">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla enc_encuesta
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionenc_encuesta
		/// </returns>
		public int Update(entEncEncuesta obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncEncuesta.Fields.id_tab_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_informante.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_movimiento.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.codigo_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.observacion.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.latitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.longitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_last.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fila.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usucre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.feccre.ToString());
			
				ArrayList arrValores = new ArrayList();
				arrValores.Add(obj.id_tab_encuesta);
				arrValores.Add(obj.id_informante == null ? null : "'" + obj.id_informante + "'");
				arrValores.Add(obj.id_movimiento);
				arrValores.Add(obj.id_pregunta);
				arrValores.Add(obj.id_respuesta);
				arrValores.Add(obj.codigo_respuesta == null ? null : "'" + obj.codigo_respuesta + "'");
				arrValores.Add(obj.respuesta == null ? null : "'" + obj.respuesta + "'");
				arrValores.Add(obj.observacion == null ? null : "'" + obj.observacion + "'");
				arrValores.Add(obj.latitud);
				arrValores.Add(obj.longitud);
				arrValores.Add(obj.id_last);
				arrValores.Add(obj.fila);
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(cParametros.parFormatoFechaHora) + "'");

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(entEncEncuesta.Fields.id_encuesta.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.id_encuesta == null ? null : "'" + obj.id_encuesta + "'");

			
				cConn local = new cConn();
				return local.updateBD(entEncEncuesta.strNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla enc_encuesta a partir de una clase del tipo eenc_encuesta
		/// </summary>
		/// <param name="obj" type="Entidades.entEncEncuesta">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla enc_encuesta
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_encuesta
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacion
		/// </returns>
		public int Update(entEncEncuesta obj, ref cTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncEncuesta.Fields.id_tab_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_informante.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_movimiento.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.codigo_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.observacion.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.latitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.longitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_last.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fila.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usucre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.feccre.ToString());
			
				ArrayList arrValores = new ArrayList();
				arrValores.Add(obj.id_tab_encuesta);
				arrValores.Add(obj.id_informante == null ? null : "'" + obj.id_informante + "'");
				arrValores.Add(obj.id_movimiento);
				arrValores.Add(obj.id_pregunta);
				arrValores.Add(obj.id_respuesta);
				arrValores.Add(obj.codigo_respuesta == null ? null : "'" + obj.codigo_respuesta + "'");
				arrValores.Add(obj.respuesta == null ? null : "'" + obj.respuesta + "'");
				arrValores.Add(obj.observacion == null ? null : "'" + obj.observacion + "'");
				arrValores.Add(obj.latitud);
				arrValores.Add(obj.longitud);
				arrValores.Add(obj.id_last);
				arrValores.Add(obj.fila);
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(cParametros.parFormatoFechaHora) + "'");

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(entEncEncuesta.Fields.id_encuesta.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.id_encuesta == null ? null : "'" + obj.id_encuesta + "'");

			
				cConn local = new cConn();
				return local.updateBD(entEncEncuesta.strNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla enc_encuesta a partir de una clase del tipo entEncEncuesta y su respectiva PK
		/// </summary>
		/// <param name="obj" type="Entidades.entEncEncuesta">
		///     <para>
		/// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla enc_encuesta
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionenc_encuesta
		/// </returns>
		public int Delete(entEncEncuesta obj)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(entEncEncuesta.Fields.id_encuesta.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.id_encuesta == null ? null : "'" + obj.id_encuesta + "'");

			
				cConn local = new cConn();
				return local.deleteBD(entEncEncuesta.strNombreTabla, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla enc_encuesta a partir de una clase del tipo entEncEncuesta y su PK
		/// </summary>
		/// <param name="obj" type="Entidades.eenc_encuesta">
		///     <para>
		/// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla enc_encuesta
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_encuesta
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacionenc_encuesta
		/// </returns>
		public int Delete(entEncEncuesta obj, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(entEncEncuesta.Fields.id_encuesta.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.id_encuesta == null ? null : "'" + obj.id_encuesta + "'");

			
				cConn local = new cConn();
				return local.deleteBD(entEncEncuesta.strNombreTabla, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla enc_encuesta a partir de una clase del tipo eenc_encuesta
		/// </summary>
		/// <param name="arrColumnasWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Valores para cada una de las columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionenc_encuesta
		/// </returns>
		public int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				cConn local = new cConn();
				return local.deleteBD("enc_encuesta", arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla enc_encuesta a partir de una clase del tipo eenc_encuesta
		/// </summary>
		/// <param name="arrColumnasWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Valores para cada una de las columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_encuesta
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacionenc_encuesta
		/// </returns>
		public int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			try
			{
				cConn local = new cConn();
				return local.deleteBD(entEncEncuesta.strNombreTabla, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		

		#endregion 

		#region Llenado de elementos

		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla enc_encuesta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla enc_encuestaenc_encuesta
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla enc_encuesta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla enc_encuestaenc_encuesta
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla enc_encuesta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla enc_encuestaenc_encuesta
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla enc_encuesta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla enc_encuestaenc_encuesta
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncEncuesta.Fields.id_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_tab_encuesta.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla enc_encuesta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla enc_encuestaenc_encuesta
		///     </para>
		/// </param>
		/// <param name="valueField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entEncEncuesta.Fields valueField, entEncEncuesta.Fields textField)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla enc_encuesta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla enc_encuestaenc_encuesta
		///     </para>
		/// </param>
		/// <param name="valueField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entEncEncuesta.Fields valueField, entEncEncuesta.Fields textField, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla enc_encuesta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla enc_encuestaenc_encuesta
		///     </para>
		/// </param>
		/// <param name="valueField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entEncEncuesta.Fields valueField, String textField)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla enc_encuesta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla enc_encuestaenc_encuesta
		///     </para>
		/// </param>
		/// <param name="valueField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entEncEncuesta.Fields valueField, String textField, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla enc_encuesta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla enc_encuestaenc_encuesta
		///     </para>
		/// </param>
		/// <param name="valueField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entEncEncuesta.Fields valueField, entEncEncuesta.Fields textField, entEncEncuesta.Fields searchField, object searchValue)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField.ToString(), searchField, searchValue);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla enc_encuesta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla enc_encuestaenc_encuesta
		///     </para>
		/// </param>
		/// <param name="valueField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entEncEncuesta.Fields valueField, String textField, entEncEncuesta.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla enc_encuesta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla enc_encuestaenc_encuesta
		///     </para>
		/// </param>
		/// <param name="valueField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entEncEncuesta.Fields valueField, entEncEncuesta.Fields textField, entEncEncuesta.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla enc_encuesta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla enc_encuestaenc_encuesta
		///     </para>
		/// </param>
		/// <param name="valueField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entEncEncuesta.Fields valueField, String textField, entEncEncuesta.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla enc_encuesta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla enc_encuestaenc_encuesta
		///     </para>
		/// </param>
		/// <param name="valueField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entEncEncuesta.Fields valueField, entEncEncuesta.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla enc_encuesta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla enc_encuestaenc_encuesta
		///     </para>
		/// </param>
		/// <param name="valueField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entEncEncuesta.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla enc_encuesta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla enc_encuestaenc_encuesta
		///     </para>
		/// </param>
		/// <param name="valueField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entEncEncuesta.Fields valueField, entEncEncuesta.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla enc_encuesta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla enc_encuestaenc_encuesta
		///     </para>
		/// </param>
		/// <param name="valueField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entEncEncuesta.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla enc_encuesta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla enc_encuestaenc_encuesta
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entEncEncuesta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Columns.Count > 0)
				{
					cmb.ValueMember = table.Columns[0].ColumnName;
					cmb.DisplayMember = table.Columns[1].ColumnName;
					cmb.DataSource = table;

				}
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla enc_encuesta
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncEncuesta.Fields.id_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_tab_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_informante.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_movimiento.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.codigo_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.observacion.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.latitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.longitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_last.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fila.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usucre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.feccre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usumod.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fecmod.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla enc_encuesta
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		  Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncEncuesta.Fields.id_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_tab_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_informante.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_movimiento.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.codigo_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.observacion.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.latitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.longitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_last.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fila.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usucre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.feccre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usumod.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fecmod.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla enc_encuesta
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla enc_encuesta
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla enc_encuesta
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncEncuesta.Fields.id_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_tab_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_informante.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_movimiento.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.codigo_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.observacion.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.latitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.longitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_last.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fila.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usucre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.feccre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usumod.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fecmod.ToString());
			
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla enc_encuesta
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncEncuesta.Fields.id_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_tab_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_informante.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_movimiento.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.codigo_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.observacion.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.latitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.longitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_last.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fila.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usucre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.feccre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usumod.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fecmod.ToString());
			
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla enc_encuesta
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla enc_encuesta
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DbDataReader dsReader = local.cargarDataReaderAnd(cParametros.schema + entEncEncuesta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				dtg.DataSource = dsReader;

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla enc_encuesta
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, entEncEncuesta.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncEncuesta.Fields.id_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_tab_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_informante.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_movimiento.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.codigo_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.observacion.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.latitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.longitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_last.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fila.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usucre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.feccre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usumod.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla enc_encuesta
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, entEncEncuesta.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncEncuesta.Fields.id_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_tab_encuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_informante.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_movimiento.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.codigo_respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.respuesta.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.observacion.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.latitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.longitud.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.id_last.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fila.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usucre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.feccre.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.usumod.ToString());
				arrColumnas.Add(entEncEncuesta.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla enc_encuesta
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entEncEncuesta.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla enc_encuesta
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entEncEncuesta.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla enc_encuesta
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarDataGridOr(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla enc_encuesta
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DbDataReader dsReader = local.cargarDataReaderOr(cParametros.schema + entEncEncuesta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				dtg.DataSource = dsReader;

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion 


		#endregion

		#region Funciones Internas

		/// <summary>
		/// 	 Funcion que devuelve un objeto a partir de un DataRow
		/// </summary>
		/// <param name="row" type="System.Data.DataRow">
		///     <para>
		/// 		 DataRow con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Objeto enc_encuesta
		/// </returns>
		internal entEncEncuesta crearObjeto(DataRow row)
		{
			entEncEncuesta obj = new entEncEncuesta();
			obj.id_encuesta = GetColumnType(row[entEncEncuesta.Fields.id_encuesta.ToString()], entEncEncuesta.Fields.id_encuesta);
			obj.id_tab_encuesta = GetColumnType(row[entEncEncuesta.Fields.id_tab_encuesta.ToString()], entEncEncuesta.Fields.id_tab_encuesta);
			obj.id_informante = GetColumnType(row[entEncEncuesta.Fields.id_informante.ToString()], entEncEncuesta.Fields.id_informante);
			obj.id_movimiento = GetColumnType(row[entEncEncuesta.Fields.id_movimiento.ToString()], entEncEncuesta.Fields.id_movimiento);
			obj.id_pregunta = GetColumnType(row[entEncEncuesta.Fields.id_pregunta.ToString()], entEncEncuesta.Fields.id_pregunta);
			obj.id_respuesta = GetColumnType(row[entEncEncuesta.Fields.id_respuesta.ToString()], entEncEncuesta.Fields.id_respuesta);
			obj.codigo_respuesta = GetColumnType(row[entEncEncuesta.Fields.codigo_respuesta.ToString()], entEncEncuesta.Fields.codigo_respuesta);
			obj.respuesta = GetColumnType(row[entEncEncuesta.Fields.respuesta.ToString()], entEncEncuesta.Fields.respuesta);
			obj.observacion = GetColumnType(row[entEncEncuesta.Fields.observacion.ToString()], entEncEncuesta.Fields.observacion);
			obj.latitud = GetColumnType(row[entEncEncuesta.Fields.latitud.ToString()], entEncEncuesta.Fields.latitud);
			obj.longitud = GetColumnType(row[entEncEncuesta.Fields.longitud.ToString()], entEncEncuesta.Fields.longitud);
			obj.id_last = GetColumnType(row[entEncEncuesta.Fields.id_last.ToString()], entEncEncuesta.Fields.id_last);
			obj.fila = GetColumnType(row[entEncEncuesta.Fields.fila.ToString()], entEncEncuesta.Fields.fila);
			obj.apiestado = GetColumnType(row[entEncEncuesta.Fields.apiestado.ToString()], entEncEncuesta.Fields.apiestado);
			obj.usucre = GetColumnType(row[entEncEncuesta.Fields.usucre.ToString()], entEncEncuesta.Fields.usucre);
			obj.feccre = GetColumnType(row[entEncEncuesta.Fields.feccre.ToString()], entEncEncuesta.Fields.feccre);
			obj.usumod = GetColumnType(row[entEncEncuesta.Fields.usumod.ToString()], entEncEncuesta.Fields.usumod);
			obj.fecmod = GetColumnType(row[entEncEncuesta.Fields.fecmod.ToString()], entEncEncuesta.Fields.fecmod);
			return obj;
		}

		/// <summary>
		/// 	 Funcion que devuelve un objeto a partir de un DataRow
		/// </summary>
		/// <param name="row" type="System.Data.DataRow">
		///     <para>
		/// 		 DataRow con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Objeto enc_encuesta
		/// </returns>
		internal entEncEncuesta crearObjetoRevisado(DataRow row)
		{
			entEncEncuesta obj = new entEncEncuesta();
			if (row.Table.Columns.Contains(entEncEncuesta.Fields.id_encuesta.ToString()))
				obj.id_encuesta = GetColumnType(row[entEncEncuesta.Fields.id_encuesta.ToString()], entEncEncuesta.Fields.id_encuesta);
			if (row.Table.Columns.Contains(entEncEncuesta.Fields.id_tab_encuesta.ToString()))
				obj.id_tab_encuesta = GetColumnType(row[entEncEncuesta.Fields.id_tab_encuesta.ToString()], entEncEncuesta.Fields.id_tab_encuesta);
			if (row.Table.Columns.Contains(entEncEncuesta.Fields.id_informante.ToString()))
				obj.id_informante = GetColumnType(row[entEncEncuesta.Fields.id_informante.ToString()], entEncEncuesta.Fields.id_informante);
			if (row.Table.Columns.Contains(entEncEncuesta.Fields.id_movimiento.ToString()))
				obj.id_movimiento = GetColumnType(row[entEncEncuesta.Fields.id_movimiento.ToString()], entEncEncuesta.Fields.id_movimiento);
			if (row.Table.Columns.Contains(entEncEncuesta.Fields.id_pregunta.ToString()))
				obj.id_pregunta = GetColumnType(row[entEncEncuesta.Fields.id_pregunta.ToString()], entEncEncuesta.Fields.id_pregunta);
			if (row.Table.Columns.Contains(entEncEncuesta.Fields.id_respuesta.ToString()))
				obj.id_respuesta = GetColumnType(row[entEncEncuesta.Fields.id_respuesta.ToString()], entEncEncuesta.Fields.id_respuesta);
			if (row.Table.Columns.Contains(entEncEncuesta.Fields.codigo_respuesta.ToString()))
				obj.codigo_respuesta = GetColumnType(row[entEncEncuesta.Fields.codigo_respuesta.ToString()], entEncEncuesta.Fields.codigo_respuesta);
			if (row.Table.Columns.Contains(entEncEncuesta.Fields.respuesta.ToString()))
				obj.respuesta = GetColumnType(row[entEncEncuesta.Fields.respuesta.ToString()], entEncEncuesta.Fields.respuesta);
			if (row.Table.Columns.Contains(entEncEncuesta.Fields.observacion.ToString()))
				obj.observacion = GetColumnType(row[entEncEncuesta.Fields.observacion.ToString()], entEncEncuesta.Fields.observacion);
			if (row.Table.Columns.Contains(entEncEncuesta.Fields.latitud.ToString()))
				obj.latitud = GetColumnType(row[entEncEncuesta.Fields.latitud.ToString()], entEncEncuesta.Fields.latitud);
			if (row.Table.Columns.Contains(entEncEncuesta.Fields.longitud.ToString()))
				obj.longitud = GetColumnType(row[entEncEncuesta.Fields.longitud.ToString()], entEncEncuesta.Fields.longitud);
			if (row.Table.Columns.Contains(entEncEncuesta.Fields.id_last.ToString()))
				obj.id_last = GetColumnType(row[entEncEncuesta.Fields.id_last.ToString()], entEncEncuesta.Fields.id_last);
			if (row.Table.Columns.Contains(entEncEncuesta.Fields.fila.ToString()))
				obj.fila = GetColumnType(row[entEncEncuesta.Fields.fila.ToString()], entEncEncuesta.Fields.fila);
			if (row.Table.Columns.Contains(entEncEncuesta.Fields.apiestado.ToString()))
				obj.apiestado = GetColumnType(row[entEncEncuesta.Fields.apiestado.ToString()], entEncEncuesta.Fields.apiestado);
			if (row.Table.Columns.Contains(entEncEncuesta.Fields.usucre.ToString()))
				obj.usucre = GetColumnType(row[entEncEncuesta.Fields.usucre.ToString()], entEncEncuesta.Fields.usucre);
			if (row.Table.Columns.Contains(entEncEncuesta.Fields.feccre.ToString()))
				obj.feccre = GetColumnType(row[entEncEncuesta.Fields.feccre.ToString()], entEncEncuesta.Fields.feccre);
			if (row.Table.Columns.Contains(entEncEncuesta.Fields.usumod.ToString()))
				obj.usumod = GetColumnType(row[entEncEncuesta.Fields.usumod.ToString()], entEncEncuesta.Fields.usumod);
			if (row.Table.Columns.Contains(entEncEncuesta.Fields.fecmod.ToString()))
				obj.fecmod = GetColumnType(row[entEncEncuesta.Fields.fecmod.ToString()], entEncEncuesta.Fields.fecmod);
			return obj;
		}

		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtenc_encuesta" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Lista de Objetos enc_encuesta
		/// </returns>
		internal List<entEncEncuesta> crearLista(DataTable dtenc_encuesta)
		{
			List<entEncEncuesta> list = new List<entEncEncuesta>();
			
			entEncEncuesta obj = new entEncEncuesta();
			foreach (DataRow row in dtenc_encuesta.Rows)
			{
				obj = crearObjeto(row);
				list.Add(obj);
			}
			return list;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable y con solo algunas columnas
		/// </summary>
		/// <param name="dtenc_encuesta" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Lista de Objetos enc_encuesta
		/// </returns>
		internal List<entEncEncuesta> crearListaRevisada(DataTable dtenc_encuesta)
		{
			List<entEncEncuesta> list = new List<entEncEncuesta>();
			
			entEncEncuesta obj = new entEncEncuesta();
			foreach (DataRow row in dtenc_encuesta.Rows)
			{
				obj = crearObjetoRevisado(row);
				list.Add(obj);
			}
			return list;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtenc_encuesta" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cola de Objetos enc_encuesta
		/// </returns>
		internal Queue<entEncEncuesta> crearCola(DataTable dtenc_encuesta)
		{
			Queue<entEncEncuesta> cola = new Queue<entEncEncuesta>();
			
			entEncEncuesta obj = new entEncEncuesta();
			foreach (DataRow row in dtenc_encuesta.Rows)
			{
				obj = crearObjeto(row);
				cola.Enqueue(obj);
			}
			return cola;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtenc_encuesta" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Pila de Objetos enc_encuesta
		/// </returns>
		internal Stack<entEncEncuesta> crearPila(DataTable dtenc_encuesta)
		{
			Stack<entEncEncuesta> pila = new Stack<entEncEncuesta>();
			
			entEncEncuesta obj = new entEncEncuesta();
			foreach (DataRow row in dtenc_encuesta.Rows)
			{
				obj = crearObjeto(row);
				pila.Push(obj);
			}
			return pila;
		}
		
		/// <summary>
		/// 	 Funcion que crea un Dicionario a partir de un DataTable
		/// </summary>
		/// <param name="dtenc_encuesta" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Diccionario de Objetos enc_encuesta
		/// </returns>
		internal Dictionary<String, entEncEncuesta> crearDiccionario(DataTable dtenc_encuesta)
		{
			Dictionary<String, entEncEncuesta>  miDic = new Dictionary<String, entEncEncuesta>();
			
			entEncEncuesta obj = new entEncEncuesta();
			foreach (DataRow row in dtenc_encuesta.Rows)
			{
				obj = crearObjeto(row);
				miDic.Add(obj.id_encuesta.ToString(), obj);
			}
			return miDic;
		}
		
		/// <summary>
		/// 	 Funcion que crea un Dicionario a partir de un DataTable
		/// </summary>
		/// <param name="dtenc_encuesta" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 HashTable de Objetos enc_encuesta
		/// </returns>
		internal Hashtable crearHashTable(DataTable dtenc_encuesta)
		{
			Hashtable miTabla = new Hashtable();
			
			entEncEncuesta obj = new entEncEncuesta();
			foreach (DataRow row in dtenc_encuesta.Rows)
			{
				obj = crearObjeto(row);
				miTabla.Add(obj.id_encuesta.ToString(), obj);
			}
			return miTabla;
		}
		
		/// <summary>
		/// 	 Funcion que crea un Dicionario a partir de un DataTable y solo con columnas existentes
		/// </summary>
		/// <param name="dtenc_encuesta" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Diccionario de Objetos enc_encuesta
		/// </returns>
		internal Dictionary<String, entEncEncuesta> crearDiccionarioRevisado(DataTable dtenc_encuesta)
		{
			Dictionary<String, entEncEncuesta>  miDic = new Dictionary<String, entEncEncuesta>();
			
			entEncEncuesta obj = new entEncEncuesta();
			foreach (DataRow row in dtenc_encuesta.Rows)
			{
				obj = crearObjetoRevisado(row);
				miDic.Add(obj.id_encuesta.ToString(), obj);
			}
			return miDic;
		}
		
		public void Dispose()
		{
			GC.SuppressFinalize(this);
		}
		
		protected void Finalize()
		{
			Dispose();
		}
		#endregion

	}
}

