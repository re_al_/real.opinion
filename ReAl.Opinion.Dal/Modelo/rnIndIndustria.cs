#region 
/***********************************************************************************************************
	NOMBRE:       rnIndIndustria
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla ind_industria

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/05/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using ReAl.Opinion.Dal;
using ReAl.Opinion.Dal.Entidades;
using ReAl.Opinion.Dal.Interface;
using System.Windows.Forms;
using ReAl.Opinion.PgConn;

#endregion

namespace ReAl.Opinion.Dal.Modelo
{
	public partial class rnIndIndustria: inIndIndustria
	{
		//Debe implementar la Interface (Alt + Shift + F10)

		#region inIndIndustria Members

		/// <summary>
		/// Metodo que devuelve el Script SQL de la Tabla
		/// </summary>
		/// <returns>Script SQL</returns>
		public string getTableScript()
		{
			TableClass tabla = new TableClass(typeof(entIndIndustria));
			return tabla.CreateTableScript();
		}
		
		/// <summary>
		/// Metodo para castear Dinamicamente un Tipo
		/// <summary>
		/// <param name="valor">Tipo a ser casteado</param>
		/// <param name="myField">Enum de la columna</param>
		/// <returns>Devuelve un objeto del Tipo de la columna especificada en el Enum</returns>
		public dynamic GetColumnType(object valor, entIndIndustria.Fields myField)
		{
			if (DBNull.Value.Equals(valor)) 
				return null;
			Type destino = typeof(entIndIndustria).GetProperty(myField.ToString()).PropertyType;
			var miTipo = Nullable.GetUnderlyingType(destino) ?? destino;
			return Convert.ChangeType(valor, miTipo);
		}

		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo entIndIndustria que cumple con los filtros de los parametros
		/// </returns>
		public entIndIndustria ObtenerObjeto(int intid_industria)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(entIndIndustria.Fields.id_industria.ToString());
		
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(intid_industria);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entIndIndustria que cumple con los filtros de los parametros
		/// </returns>
		public entIndIndustria ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entIndIndustria que cumple con los filtros de los parametros
		/// </returns>
		public entIndIndustria ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustria.Fields.id_industria.ToString());
				arrColumnas.Add(entIndIndustria.Fields.id_departamento.ToString());
				arrColumnas.Add(entIndIndustria.Fields.razon_social.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nombre_comercial.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nit.ToString());
				arrColumnas.Add(entIndIndustria.Fields.regine.ToString());
				arrColumnas.Add(entIndIndustria.Fields.situacion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.direccion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.numero.ToString());
				arrColumnas.Add(entIndIndustria.Fields.edificio.ToString());
				arrColumnas.Add(entIndIndustria.Fields.piso.ToString());
				arrColumnas.Add(entIndIndustria.Fields.oficina.ToString());
				arrColumnas.Add(entIndIndustria.Fields.zona.ToString());
				arrColumnas.Add(entIndIndustria.Fields.entre_calles.ToString());
				arrColumnas.Add(entIndIndustria.Fields.referencia.ToString());
				arrColumnas.Add(entIndIndustria.Fields.telefono.ToString());
				arrColumnas.Add(entIndIndustria.Fields.fax.ToString());
				arrColumnas.Add(entIndIndustria.Fields.casilla.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nombre_informante.ToString());
				arrColumnas.Add(entIndIndustria.Fields.e_mail.ToString());
				arrColumnas.Add(entIndIndustria.Fields.pagina_web.ToString());
				arrColumnas.Add(entIndIndustria.Fields.apiestado.ToString());
				arrColumnas.Add(entIndIndustria.Fields.apitransaccion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usucre.ToString());
				arrColumnas.Add(entIndIndustria.Fields.feccre.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usumod.ToString());
				arrColumnas.Add(entIndIndustria.Fields.fecmod.ToString());
				arrColumnas.Add(entIndIndustria.Fields.geoposicion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usugeo.ToString());
				arrColumnas.Add(entIndIndustria.Fields.geoposicion_fabrica.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usuge_fabrica.ToString());
				arrColumnas.Add(entIndIndustria.Fields.gps_informante.ToString());
				arrColumnas.Add(entIndIndustria.Fields.gps_fabrica.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entIndIndustria.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count == 1)
				{
					entIndIndustria obj = new entIndIndustria();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entIndIndustria que cumple con los filtros de los parametros
		/// </returns>
		public entIndIndustria ObtenerObjeto(entIndIndustria.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entIndIndustria que cumple con los filtros de los parametros
		/// </returns>
		public entIndIndustria ObtenerObjeto(entIndIndustria.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un Business Object del Tipo entIndIndustria a partir de su llave promaria
		/// </summary>
		/// <returns>
		/// 	Objeto del Tipo entIndIndustria
		/// </returns>
		public entIndIndustria ObtenerObjeto(int intid_industria, ref cTrans localTrans )
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(entIndIndustria.Fields.id_industria.ToString());
		
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(intid_industria);
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industria
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entIndIndustria que cumple con los filtros de los parametros
		/// </returns>
		public entIndIndustria ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industria
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entIndIndustria que cumple con los filtros de los parametros
		/// </returns>
		public entIndIndustria ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustria.Fields.id_industria.ToString());
				arrColumnas.Add(entIndIndustria.Fields.id_departamento.ToString());
				arrColumnas.Add(entIndIndustria.Fields.razon_social.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nombre_comercial.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nit.ToString());
				arrColumnas.Add(entIndIndustria.Fields.regine.ToString());
				arrColumnas.Add(entIndIndustria.Fields.situacion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.direccion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.numero.ToString());
				arrColumnas.Add(entIndIndustria.Fields.edificio.ToString());
				arrColumnas.Add(entIndIndustria.Fields.piso.ToString());
				arrColumnas.Add(entIndIndustria.Fields.oficina.ToString());
				arrColumnas.Add(entIndIndustria.Fields.zona.ToString());
				arrColumnas.Add(entIndIndustria.Fields.entre_calles.ToString());
				arrColumnas.Add(entIndIndustria.Fields.referencia.ToString());
				arrColumnas.Add(entIndIndustria.Fields.telefono.ToString());
				arrColumnas.Add(entIndIndustria.Fields.fax.ToString());
				arrColumnas.Add(entIndIndustria.Fields.casilla.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nombre_informante.ToString());
				arrColumnas.Add(entIndIndustria.Fields.e_mail.ToString());
				arrColumnas.Add(entIndIndustria.Fields.pagina_web.ToString());
				arrColumnas.Add(entIndIndustria.Fields.apiestado.ToString());
				arrColumnas.Add(entIndIndustria.Fields.apitransaccion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usucre.ToString());
				arrColumnas.Add(entIndIndustria.Fields.feccre.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usumod.ToString());
				arrColumnas.Add(entIndIndustria.Fields.fecmod.ToString());
				arrColumnas.Add(entIndIndustria.Fields.geoposicion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usugeo.ToString());
				arrColumnas.Add(entIndIndustria.Fields.geoposicion_fabrica.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usuge_fabrica.ToString());
				arrColumnas.Add(entIndIndustria.Fields.gps_informante.ToString());
				arrColumnas.Add(entIndIndustria.Fields.gps_fabrica.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entIndIndustria.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count == 1)
				{
					entIndIndustria obj = new entIndIndustria();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industria
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entIndIndustria que cumple con los filtros de los parametros
		/// </returns>
		public entIndIndustria ObtenerObjeto(entIndIndustria.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industria
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entIndIndustria que cumple con los filtros de los parametros
		/// </returns>
		public entIndIndustria ObtenerObjeto(entIndIndustria.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entIndIndustria> ObtenerLista()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entIndIndustria> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entIndIndustria> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustria.Fields.id_industria.ToString());
				arrColumnas.Add(entIndIndustria.Fields.id_departamento.ToString());
				arrColumnas.Add(entIndIndustria.Fields.razon_social.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nombre_comercial.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nit.ToString());
				arrColumnas.Add(entIndIndustria.Fields.regine.ToString());
				arrColumnas.Add(entIndIndustria.Fields.situacion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.direccion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.numero.ToString());
				arrColumnas.Add(entIndIndustria.Fields.edificio.ToString());
				arrColumnas.Add(entIndIndustria.Fields.piso.ToString());
				arrColumnas.Add(entIndIndustria.Fields.oficina.ToString());
				arrColumnas.Add(entIndIndustria.Fields.zona.ToString());
				arrColumnas.Add(entIndIndustria.Fields.entre_calles.ToString());
				arrColumnas.Add(entIndIndustria.Fields.referencia.ToString());
				arrColumnas.Add(entIndIndustria.Fields.telefono.ToString());
				arrColumnas.Add(entIndIndustria.Fields.fax.ToString());
				arrColumnas.Add(entIndIndustria.Fields.casilla.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nombre_informante.ToString());
				arrColumnas.Add(entIndIndustria.Fields.e_mail.ToString());
				arrColumnas.Add(entIndIndustria.Fields.pagina_web.ToString());
				arrColumnas.Add(entIndIndustria.Fields.apiestado.ToString());
				arrColumnas.Add(entIndIndustria.Fields.apitransaccion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usucre.ToString());
				arrColumnas.Add(entIndIndustria.Fields.feccre.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usumod.ToString());
				arrColumnas.Add(entIndIndustria.Fields.fecmod.ToString());
				arrColumnas.Add(entIndIndustria.Fields.geoposicion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usugeo.ToString());
				arrColumnas.Add(entIndIndustria.Fields.geoposicion_fabrica.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usuge_fabrica.ToString());
				arrColumnas.Add(entIndIndustria.Fields.gps_informante.ToString());
				arrColumnas.Add(entIndIndustria.Fields.gps_fabrica.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entIndIndustria.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<entIndIndustria>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entIndIndustria> ObtenerLista(entIndIndustria.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entIndIndustria> ObtenerLista(entIndIndustria.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industria
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entIndIndustria> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industria
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entIndIndustria> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustria.Fields.id_industria.ToString());
				arrColumnas.Add(entIndIndustria.Fields.id_departamento.ToString());
				arrColumnas.Add(entIndIndustria.Fields.razon_social.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nombre_comercial.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nit.ToString());
				arrColumnas.Add(entIndIndustria.Fields.regine.ToString());
				arrColumnas.Add(entIndIndustria.Fields.situacion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.direccion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.numero.ToString());
				arrColumnas.Add(entIndIndustria.Fields.edificio.ToString());
				arrColumnas.Add(entIndIndustria.Fields.piso.ToString());
				arrColumnas.Add(entIndIndustria.Fields.oficina.ToString());
				arrColumnas.Add(entIndIndustria.Fields.zona.ToString());
				arrColumnas.Add(entIndIndustria.Fields.entre_calles.ToString());
				arrColumnas.Add(entIndIndustria.Fields.referencia.ToString());
				arrColumnas.Add(entIndIndustria.Fields.telefono.ToString());
				arrColumnas.Add(entIndIndustria.Fields.fax.ToString());
				arrColumnas.Add(entIndIndustria.Fields.casilla.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nombre_informante.ToString());
				arrColumnas.Add(entIndIndustria.Fields.e_mail.ToString());
				arrColumnas.Add(entIndIndustria.Fields.pagina_web.ToString());
				arrColumnas.Add(entIndIndustria.Fields.apiestado.ToString());
				arrColumnas.Add(entIndIndustria.Fields.apitransaccion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usucre.ToString());
				arrColumnas.Add(entIndIndustria.Fields.feccre.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usumod.ToString());
				arrColumnas.Add(entIndIndustria.Fields.fecmod.ToString());
				arrColumnas.Add(entIndIndustria.Fields.geoposicion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usugeo.ToString());
				arrColumnas.Add(entIndIndustria.Fields.geoposicion_fabrica.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usuge_fabrica.ToString());
				arrColumnas.Add(entIndIndustria.Fields.gps_informante.ToString());
				arrColumnas.Add(entIndIndustria.Fields.gps_fabrica.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entIndIndustria.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<entIndIndustria>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industria
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entIndIndustria> ObtenerLista(entIndIndustria.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industria
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entIndIndustria> ObtenerLista(entIndIndustria.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entIndIndustria> ObtenerListaDesdeVista(String strVista)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entIndIndustria> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entIndIndustria> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<entIndIndustria>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entIndIndustria> ObtenerListaDesdeVista(String strVista, entIndIndustria.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entIndIndustria> ObtenerListaDesdeVista(String strVista, entIndIndustria.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industria
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entIndIndustria> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industria
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entIndIndustria> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<entIndIndustria>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industria
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entIndIndustria> ObtenerListaDesdeVista(String strVista, entIndIndustria.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industria
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entIndIndustria> ObtenerListaDesdeVista(String strVista, entIndIndustria.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entIndIndustria> ObtenerCola()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entIndIndustria> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entIndIndustria> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustria.Fields.id_industria.ToString());
				arrColumnas.Add(entIndIndustria.Fields.id_departamento.ToString());
				arrColumnas.Add(entIndIndustria.Fields.razon_social.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nombre_comercial.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nit.ToString());
				arrColumnas.Add(entIndIndustria.Fields.regine.ToString());
				arrColumnas.Add(entIndIndustria.Fields.situacion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.direccion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.numero.ToString());
				arrColumnas.Add(entIndIndustria.Fields.edificio.ToString());
				arrColumnas.Add(entIndIndustria.Fields.piso.ToString());
				arrColumnas.Add(entIndIndustria.Fields.oficina.ToString());
				arrColumnas.Add(entIndIndustria.Fields.zona.ToString());
				arrColumnas.Add(entIndIndustria.Fields.entre_calles.ToString());
				arrColumnas.Add(entIndIndustria.Fields.referencia.ToString());
				arrColumnas.Add(entIndIndustria.Fields.telefono.ToString());
				arrColumnas.Add(entIndIndustria.Fields.fax.ToString());
				arrColumnas.Add(entIndIndustria.Fields.casilla.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nombre_informante.ToString());
				arrColumnas.Add(entIndIndustria.Fields.e_mail.ToString());
				arrColumnas.Add(entIndIndustria.Fields.pagina_web.ToString());
				arrColumnas.Add(entIndIndustria.Fields.apiestado.ToString());
				arrColumnas.Add(entIndIndustria.Fields.apitransaccion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usucre.ToString());
				arrColumnas.Add(entIndIndustria.Fields.feccre.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usumod.ToString());
				arrColumnas.Add(entIndIndustria.Fields.fecmod.ToString());
				arrColumnas.Add(entIndIndustria.Fields.geoposicion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usugeo.ToString());
				arrColumnas.Add(entIndIndustria.Fields.geoposicion_fabrica.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usuge_fabrica.ToString());
				arrColumnas.Add(entIndIndustria.Fields.gps_informante.ToString());
				arrColumnas.Add(entIndIndustria.Fields.gps_fabrica.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entIndIndustria.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearCola(table);
				}
				else
					return new Queue<entIndIndustria>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entIndIndustria> ObtenerCola(entIndIndustria.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entIndIndustria> ObtenerCola(entIndIndustria.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industria
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entIndIndustria> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industria
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entIndIndustria> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustria.Fields.id_industria.ToString());
				arrColumnas.Add(entIndIndustria.Fields.id_departamento.ToString());
				arrColumnas.Add(entIndIndustria.Fields.razon_social.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nombre_comercial.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nit.ToString());
				arrColumnas.Add(entIndIndustria.Fields.regine.ToString());
				arrColumnas.Add(entIndIndustria.Fields.situacion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.direccion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.numero.ToString());
				arrColumnas.Add(entIndIndustria.Fields.edificio.ToString());
				arrColumnas.Add(entIndIndustria.Fields.piso.ToString());
				arrColumnas.Add(entIndIndustria.Fields.oficina.ToString());
				arrColumnas.Add(entIndIndustria.Fields.zona.ToString());
				arrColumnas.Add(entIndIndustria.Fields.entre_calles.ToString());
				arrColumnas.Add(entIndIndustria.Fields.referencia.ToString());
				arrColumnas.Add(entIndIndustria.Fields.telefono.ToString());
				arrColumnas.Add(entIndIndustria.Fields.fax.ToString());
				arrColumnas.Add(entIndIndustria.Fields.casilla.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nombre_informante.ToString());
				arrColumnas.Add(entIndIndustria.Fields.e_mail.ToString());
				arrColumnas.Add(entIndIndustria.Fields.pagina_web.ToString());
				arrColumnas.Add(entIndIndustria.Fields.apiestado.ToString());
				arrColumnas.Add(entIndIndustria.Fields.apitransaccion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usucre.ToString());
				arrColumnas.Add(entIndIndustria.Fields.feccre.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usumod.ToString());
				arrColumnas.Add(entIndIndustria.Fields.fecmod.ToString());
				arrColumnas.Add(entIndIndustria.Fields.geoposicion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usugeo.ToString());
				arrColumnas.Add(entIndIndustria.Fields.geoposicion_fabrica.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usuge_fabrica.ToString());
				arrColumnas.Add(entIndIndustria.Fields.gps_informante.ToString());
				arrColumnas.Add(entIndIndustria.Fields.gps_fabrica.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entIndIndustria.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearCola(table);
				}
				else
					return new Queue<entIndIndustria>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industria
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entIndIndustria> ObtenerCola(entIndIndustria.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industria
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entIndIndustria> ObtenerCola(entIndIndustria.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entIndIndustria> ObtenerPila()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entIndIndustria> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entIndIndustria> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustria.Fields.id_industria.ToString());
				arrColumnas.Add(entIndIndustria.Fields.id_departamento.ToString());
				arrColumnas.Add(entIndIndustria.Fields.razon_social.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nombre_comercial.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nit.ToString());
				arrColumnas.Add(entIndIndustria.Fields.regine.ToString());
				arrColumnas.Add(entIndIndustria.Fields.situacion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.direccion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.numero.ToString());
				arrColumnas.Add(entIndIndustria.Fields.edificio.ToString());
				arrColumnas.Add(entIndIndustria.Fields.piso.ToString());
				arrColumnas.Add(entIndIndustria.Fields.oficina.ToString());
				arrColumnas.Add(entIndIndustria.Fields.zona.ToString());
				arrColumnas.Add(entIndIndustria.Fields.entre_calles.ToString());
				arrColumnas.Add(entIndIndustria.Fields.referencia.ToString());
				arrColumnas.Add(entIndIndustria.Fields.telefono.ToString());
				arrColumnas.Add(entIndIndustria.Fields.fax.ToString());
				arrColumnas.Add(entIndIndustria.Fields.casilla.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nombre_informante.ToString());
				arrColumnas.Add(entIndIndustria.Fields.e_mail.ToString());
				arrColumnas.Add(entIndIndustria.Fields.pagina_web.ToString());
				arrColumnas.Add(entIndIndustria.Fields.apiestado.ToString());
				arrColumnas.Add(entIndIndustria.Fields.apitransaccion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usucre.ToString());
				arrColumnas.Add(entIndIndustria.Fields.feccre.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usumod.ToString());
				arrColumnas.Add(entIndIndustria.Fields.fecmod.ToString());
				arrColumnas.Add(entIndIndustria.Fields.geoposicion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usugeo.ToString());
				arrColumnas.Add(entIndIndustria.Fields.geoposicion_fabrica.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usuge_fabrica.ToString());
				arrColumnas.Add(entIndIndustria.Fields.gps_informante.ToString());
				arrColumnas.Add(entIndIndustria.Fields.gps_fabrica.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entIndIndustria.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearPila(table);
				}
				else
					return new Stack<entIndIndustria>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entIndIndustria> ObtenerPila(entIndIndustria.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entIndIndustria> ObtenerPila(entIndIndustria.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industria
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entIndIndustria> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industria
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entIndIndustria> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustria.Fields.id_industria.ToString());
				arrColumnas.Add(entIndIndustria.Fields.id_departamento.ToString());
				arrColumnas.Add(entIndIndustria.Fields.razon_social.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nombre_comercial.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nit.ToString());
				arrColumnas.Add(entIndIndustria.Fields.regine.ToString());
				arrColumnas.Add(entIndIndustria.Fields.situacion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.direccion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.numero.ToString());
				arrColumnas.Add(entIndIndustria.Fields.edificio.ToString());
				arrColumnas.Add(entIndIndustria.Fields.piso.ToString());
				arrColumnas.Add(entIndIndustria.Fields.oficina.ToString());
				arrColumnas.Add(entIndIndustria.Fields.zona.ToString());
				arrColumnas.Add(entIndIndustria.Fields.entre_calles.ToString());
				arrColumnas.Add(entIndIndustria.Fields.referencia.ToString());
				arrColumnas.Add(entIndIndustria.Fields.telefono.ToString());
				arrColumnas.Add(entIndIndustria.Fields.fax.ToString());
				arrColumnas.Add(entIndIndustria.Fields.casilla.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nombre_informante.ToString());
				arrColumnas.Add(entIndIndustria.Fields.e_mail.ToString());
				arrColumnas.Add(entIndIndustria.Fields.pagina_web.ToString());
				arrColumnas.Add(entIndIndustria.Fields.apiestado.ToString());
				arrColumnas.Add(entIndIndustria.Fields.apitransaccion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usucre.ToString());
				arrColumnas.Add(entIndIndustria.Fields.feccre.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usumod.ToString());
				arrColumnas.Add(entIndIndustria.Fields.fecmod.ToString());
				arrColumnas.Add(entIndIndustria.Fields.geoposicion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usugeo.ToString());
				arrColumnas.Add(entIndIndustria.Fields.geoposicion_fabrica.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usuge_fabrica.ToString());
				arrColumnas.Add(entIndIndustria.Fields.gps_informante.ToString());
				arrColumnas.Add(entIndIndustria.Fields.gps_fabrica.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entIndIndustria.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearPila(table);
				}
				else
					return new Stack<entIndIndustria>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industria
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entIndIndustria> ObtenerPila(entIndIndustria.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industria
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entIndIndustria> ObtenerPila(entIndIndustria.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entIndIndustria> ObtenerDiccionario()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entIndIndustria> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entIndIndustria> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustria.Fields.id_industria.ToString());
				arrColumnas.Add(entIndIndustria.Fields.id_departamento.ToString());
				arrColumnas.Add(entIndIndustria.Fields.razon_social.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nombre_comercial.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nit.ToString());
				arrColumnas.Add(entIndIndustria.Fields.regine.ToString());
				arrColumnas.Add(entIndIndustria.Fields.situacion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.direccion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.numero.ToString());
				arrColumnas.Add(entIndIndustria.Fields.edificio.ToString());
				arrColumnas.Add(entIndIndustria.Fields.piso.ToString());
				arrColumnas.Add(entIndIndustria.Fields.oficina.ToString());
				arrColumnas.Add(entIndIndustria.Fields.zona.ToString());
				arrColumnas.Add(entIndIndustria.Fields.entre_calles.ToString());
				arrColumnas.Add(entIndIndustria.Fields.referencia.ToString());
				arrColumnas.Add(entIndIndustria.Fields.telefono.ToString());
				arrColumnas.Add(entIndIndustria.Fields.fax.ToString());
				arrColumnas.Add(entIndIndustria.Fields.casilla.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nombre_informante.ToString());
				arrColumnas.Add(entIndIndustria.Fields.e_mail.ToString());
				arrColumnas.Add(entIndIndustria.Fields.pagina_web.ToString());
				arrColumnas.Add(entIndIndustria.Fields.apiestado.ToString());
				arrColumnas.Add(entIndIndustria.Fields.apitransaccion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usucre.ToString());
				arrColumnas.Add(entIndIndustria.Fields.feccre.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usumod.ToString());
				arrColumnas.Add(entIndIndustria.Fields.fecmod.ToString());
				arrColumnas.Add(entIndIndustria.Fields.geoposicion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usugeo.ToString());
				arrColumnas.Add(entIndIndustria.Fields.geoposicion_fabrica.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usuge_fabrica.ToString());
				arrColumnas.Add(entIndIndustria.Fields.gps_informante.ToString());
				arrColumnas.Add(entIndIndustria.Fields.gps_fabrica.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entIndIndustria.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearDiccionario(table);
				}
				else
					return new Dictionary<string, entIndIndustria>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entIndIndustria> ObtenerDiccionario(entIndIndustria.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entIndIndustria> ObtenerDiccionario(entIndIndustria.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industria
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entIndIndustria> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industria
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entIndIndustria> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustria.Fields.id_industria.ToString());
				arrColumnas.Add(entIndIndustria.Fields.id_departamento.ToString());
				arrColumnas.Add(entIndIndustria.Fields.razon_social.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nombre_comercial.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nit.ToString());
				arrColumnas.Add(entIndIndustria.Fields.regine.ToString());
				arrColumnas.Add(entIndIndustria.Fields.situacion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.direccion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.numero.ToString());
				arrColumnas.Add(entIndIndustria.Fields.edificio.ToString());
				arrColumnas.Add(entIndIndustria.Fields.piso.ToString());
				arrColumnas.Add(entIndIndustria.Fields.oficina.ToString());
				arrColumnas.Add(entIndIndustria.Fields.zona.ToString());
				arrColumnas.Add(entIndIndustria.Fields.entre_calles.ToString());
				arrColumnas.Add(entIndIndustria.Fields.referencia.ToString());
				arrColumnas.Add(entIndIndustria.Fields.telefono.ToString());
				arrColumnas.Add(entIndIndustria.Fields.fax.ToString());
				arrColumnas.Add(entIndIndustria.Fields.casilla.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nombre_informante.ToString());
				arrColumnas.Add(entIndIndustria.Fields.e_mail.ToString());
				arrColumnas.Add(entIndIndustria.Fields.pagina_web.ToString());
				arrColumnas.Add(entIndIndustria.Fields.apiestado.ToString());
				arrColumnas.Add(entIndIndustria.Fields.apitransaccion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usucre.ToString());
				arrColumnas.Add(entIndIndustria.Fields.feccre.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usumod.ToString());
				arrColumnas.Add(entIndIndustria.Fields.fecmod.ToString());
				arrColumnas.Add(entIndIndustria.Fields.geoposicion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usugeo.ToString());
				arrColumnas.Add(entIndIndustria.Fields.geoposicion_fabrica.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usuge_fabrica.ToString());
				arrColumnas.Add(entIndIndustria.Fields.gps_informante.ToString());
				arrColumnas.Add(entIndIndustria.Fields.gps_fabrica.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entIndIndustria.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearDiccionario(table);
				}
				else
					return new Dictionary<string, entIndIndustria>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industria
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entIndIndustria> ObtenerDiccionario(entIndIndustria.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustria a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industria
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entIndIndustria> ObtenerDiccionario(entIndIndustria.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	 Funcion que obtiene la llave primaria unica de la tabla ind_industria a partir de una cadena
		/// </summary>
		/// <param name="args" type="string[]">
		///     <para>
		/// 		 Cadena desde la que se construye el identificador unico de la tabla ind_industria
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Identificador unico de la tabla ind_industria
		/// </returns>
		public string CreatePK(string[] args)
		{
			return args[0];
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla ind_industria a partir de una clase del tipo Eind_industria
		/// </summary>
		/// <param name="obj" type="Entidades.entIndIndustria">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla ind_industriaind_industria
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionind_industria
		/// </returns>
		public bool Insert(entIndIndustria obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustria.Fields.id_industria.ToString());
				arrColumnas.Add(entIndIndustria.Fields.id_departamento.ToString());
				arrColumnas.Add(entIndIndustria.Fields.razon_social.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nombre_comercial.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nit.ToString());
				arrColumnas.Add(entIndIndustria.Fields.regine.ToString());
				arrColumnas.Add(entIndIndustria.Fields.situacion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.direccion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.numero.ToString());
				arrColumnas.Add(entIndIndustria.Fields.edificio.ToString());
				arrColumnas.Add(entIndIndustria.Fields.piso.ToString());
				arrColumnas.Add(entIndIndustria.Fields.oficina.ToString());
				arrColumnas.Add(entIndIndustria.Fields.zona.ToString());
				arrColumnas.Add(entIndIndustria.Fields.entre_calles.ToString());
				arrColumnas.Add(entIndIndustria.Fields.referencia.ToString());
				arrColumnas.Add(entIndIndustria.Fields.telefono.ToString());
				arrColumnas.Add(entIndIndustria.Fields.fax.ToString());
				arrColumnas.Add(entIndIndustria.Fields.casilla.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nombre_informante.ToString());
				arrColumnas.Add(entIndIndustria.Fields.e_mail.ToString());
				arrColumnas.Add(entIndIndustria.Fields.pagina_web.ToString());
				arrColumnas.Add(entIndIndustria.Fields.apiestado.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usucre.ToString());
				arrColumnas.Add(entIndIndustria.Fields.feccre.ToString());
				arrColumnas.Add(entIndIndustria.Fields.geoposicion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usugeo.ToString());
				arrColumnas.Add(entIndIndustria.Fields.geoposicion_fabrica.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usuge_fabrica.ToString());
				arrColumnas.Add(entIndIndustria.Fields.gps_informante.ToString());
				arrColumnas.Add(entIndIndustria.Fields.gps_fabrica.ToString());
				
				ArrayList arrValores = new ArrayList();
				arrValores.Add(obj.id_industria);
				arrValores.Add(obj.id_departamento);
				arrValores.Add(obj.razon_social == null ? null : "'" + obj.razon_social + "'");
				arrValores.Add(obj.nombre_comercial == null ? null : "'" + obj.nombre_comercial + "'");
				arrValores.Add(obj.nit == null ? null : "'" + obj.nit + "'");
				arrValores.Add(obj.regine == null ? null : "'" + obj.regine + "'");
				arrValores.Add(obj.situacion == null ? null : "'" + obj.situacion + "'");
				arrValores.Add(obj.direccion == null ? null : "'" + obj.direccion + "'");
				arrValores.Add(obj.numero == null ? null : "'" + obj.numero + "'");
				arrValores.Add(obj.edificio == null ? null : "'" + obj.edificio + "'");
				arrValores.Add(obj.piso == null ? null : "'" + obj.piso + "'");
				arrValores.Add(obj.oficina == null ? null : "'" + obj.oficina + "'");
				arrValores.Add(obj.zona == null ? null : "'" + obj.zona + "'");
				arrValores.Add(obj.entre_calles == null ? null : "'" + obj.entre_calles + "'");
				arrValores.Add(obj.referencia == null ? null : "'" + obj.referencia + "'");
				arrValores.Add(obj.telefono == null ? null : "'" + obj.telefono + "'");
				arrValores.Add(obj.fax == null ? null : "'" + obj.fax + "'");
				arrValores.Add(obj.casilla == null ? null : "'" + obj.casilla + "'");
				arrValores.Add(obj.nombre_informante == null ? null : "'" + obj.nombre_informante + "'");
				arrValores.Add(obj.e_mail == null ? null : "'" + obj.e_mail + "'");
				arrValores.Add(obj.pagina_web == null ? null : "'" + obj.pagina_web + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.geoposicion == null ? null : "'" + obj.geoposicion + "'");
				arrValores.Add(obj.usugeo == null ? null : "'" + obj.usugeo + "'");
				arrValores.Add(obj.geoposicion_fabrica == null ? null : "'" + obj.geoposicion_fabrica + "'");
				arrValores.Add(obj.usuge_fabrica == null ? null : "'" + obj.usuge_fabrica + "'");
				arrValores.Add(obj.gps_informante == null ? null : "'" + obj.gps_informante + "'");
				arrValores.Add(obj.gps_fabrica == null ? null : "'" + obj.gps_fabrica + "'");

			
				cConn local = new cConn();
				return local.insertBD(entIndIndustria.strNombreTabla, arrColumnas, arrValores);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla ind_industria a partir de una clase del tipo Eind_industria
		/// </summary>
		/// <param name="obj" type="Entidades.entIndIndustria">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla ind_industriaind_industria
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industria
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionind_industria
		/// </returns>
		public bool Insert(entIndIndustria obj, ref cTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustria.Fields.id_industria.ToString());
				arrColumnas.Add(entIndIndustria.Fields.id_departamento.ToString());
				arrColumnas.Add(entIndIndustria.Fields.razon_social.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nombre_comercial.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nit.ToString());
				arrColumnas.Add(entIndIndustria.Fields.regine.ToString());
				arrColumnas.Add(entIndIndustria.Fields.situacion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.direccion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.numero.ToString());
				arrColumnas.Add(entIndIndustria.Fields.edificio.ToString());
				arrColumnas.Add(entIndIndustria.Fields.piso.ToString());
				arrColumnas.Add(entIndIndustria.Fields.oficina.ToString());
				arrColumnas.Add(entIndIndustria.Fields.zona.ToString());
				arrColumnas.Add(entIndIndustria.Fields.entre_calles.ToString());
				arrColumnas.Add(entIndIndustria.Fields.referencia.ToString());
				arrColumnas.Add(entIndIndustria.Fields.telefono.ToString());
				arrColumnas.Add(entIndIndustria.Fields.fax.ToString());
				arrColumnas.Add(entIndIndustria.Fields.casilla.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nombre_informante.ToString());
				arrColumnas.Add(entIndIndustria.Fields.e_mail.ToString());
				arrColumnas.Add(entIndIndustria.Fields.pagina_web.ToString());
				arrColumnas.Add(entIndIndustria.Fields.apiestado.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usucre.ToString());
				arrColumnas.Add(entIndIndustria.Fields.feccre.ToString());
				arrColumnas.Add(entIndIndustria.Fields.geoposicion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usugeo.ToString());
				arrColumnas.Add(entIndIndustria.Fields.geoposicion_fabrica.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usuge_fabrica.ToString());
				arrColumnas.Add(entIndIndustria.Fields.gps_informante.ToString());
				arrColumnas.Add(entIndIndustria.Fields.gps_fabrica.ToString());
				
				ArrayList arrValores = new ArrayList();
				arrValores.Add(obj.id_industria);
				arrValores.Add(obj.id_departamento);
				arrValores.Add(obj.razon_social == null ? null : "'" + obj.razon_social + "'");
				arrValores.Add(obj.nombre_comercial == null ? null : "'" + obj.nombre_comercial + "'");
				arrValores.Add(obj.nit == null ? null : "'" + obj.nit + "'");
				arrValores.Add(obj.regine == null ? null : "'" + obj.regine + "'");
				arrValores.Add(obj.situacion == null ? null : "'" + obj.situacion + "'");
				arrValores.Add(obj.direccion == null ? null : "'" + obj.direccion + "'");
				arrValores.Add(obj.numero == null ? null : "'" + obj.numero + "'");
				arrValores.Add(obj.edificio == null ? null : "'" + obj.edificio + "'");
				arrValores.Add(obj.piso == null ? null : "'" + obj.piso + "'");
				arrValores.Add(obj.oficina == null ? null : "'" + obj.oficina + "'");
				arrValores.Add(obj.zona == null ? null : "'" + obj.zona + "'");
				arrValores.Add(obj.entre_calles == null ? null : "'" + obj.entre_calles + "'");
				arrValores.Add(obj.referencia == null ? null : "'" + obj.referencia + "'");
				arrValores.Add(obj.telefono == null ? null : "'" + obj.telefono + "'");
				arrValores.Add(obj.fax == null ? null : "'" + obj.fax + "'");
				arrValores.Add(obj.casilla == null ? null : "'" + obj.casilla + "'");
				arrValores.Add(obj.nombre_informante == null ? null : "'" + obj.nombre_informante + "'");
				arrValores.Add(obj.e_mail == null ? null : "'" + obj.e_mail + "'");
				arrValores.Add(obj.pagina_web == null ? null : "'" + obj.pagina_web + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.geoposicion == null ? null : "'" + obj.geoposicion + "'");
				arrValores.Add(obj.usugeo == null ? null : "'" + obj.usugeo + "'");
				arrValores.Add(obj.geoposicion_fabrica == null ? null : "'" + obj.geoposicion_fabrica + "'");
				arrValores.Add(obj.usuge_fabrica == null ? null : "'" + obj.usuge_fabrica + "'");
				arrValores.Add(obj.gps_informante == null ? null : "'" + obj.gps_informante + "'");
				arrValores.Add(obj.gps_fabrica == null ? null : "'" + obj.gps_fabrica + "'");

			
				cConn local = new cConn();
				return local.insertBD(entIndIndustria.strNombreTabla, arrColumnas, arrValores, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla ind_industria a partir de una clase del tipo Eind_industria
		/// </summary>
		/// <param name="obj" type="Entidades.entIndIndustria">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla ind_industriaind_industria
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionind_industria
		/// </returns>
		public bool InsertIdentity(entIndIndustria obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustria.Fields.id_industria.ToString());
				arrColumnas.Add(entIndIndustria.Fields.id_departamento.ToString());
				arrColumnas.Add(entIndIndustria.Fields.razon_social.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nombre_comercial.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nit.ToString());
				arrColumnas.Add(entIndIndustria.Fields.regine.ToString());
				arrColumnas.Add(entIndIndustria.Fields.situacion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.direccion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.numero.ToString());
				arrColumnas.Add(entIndIndustria.Fields.edificio.ToString());
				arrColumnas.Add(entIndIndustria.Fields.piso.ToString());
				arrColumnas.Add(entIndIndustria.Fields.oficina.ToString());
				arrColumnas.Add(entIndIndustria.Fields.zona.ToString());
				arrColumnas.Add(entIndIndustria.Fields.entre_calles.ToString());
				arrColumnas.Add(entIndIndustria.Fields.referencia.ToString());
				arrColumnas.Add(entIndIndustria.Fields.telefono.ToString());
				arrColumnas.Add(entIndIndustria.Fields.fax.ToString());
				arrColumnas.Add(entIndIndustria.Fields.casilla.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nombre_informante.ToString());
				arrColumnas.Add(entIndIndustria.Fields.e_mail.ToString());
				arrColumnas.Add(entIndIndustria.Fields.pagina_web.ToString());
				arrColumnas.Add(entIndIndustria.Fields.apiestado.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usucre.ToString());
				arrColumnas.Add(entIndIndustria.Fields.feccre.ToString());
				arrColumnas.Add(entIndIndustria.Fields.geoposicion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usugeo.ToString());
				arrColumnas.Add(entIndIndustria.Fields.geoposicion_fabrica.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usuge_fabrica.ToString());
				arrColumnas.Add(entIndIndustria.Fields.gps_informante.ToString());
				arrColumnas.Add(entIndIndustria.Fields.gps_fabrica.ToString());
				
				ArrayList arrValores = new ArrayList();
				arrValores.Add(obj.id_industria);
				arrValores.Add(obj.id_departamento);
				arrValores.Add(obj.razon_social == null ? null : "'" + obj.razon_social + "'");
				arrValores.Add(obj.nombre_comercial == null ? null : "'" + obj.nombre_comercial + "'");
				arrValores.Add(obj.nit == null ? null : "'" + obj.nit + "'");
				arrValores.Add(obj.regine == null ? null : "'" + obj.regine + "'");
				arrValores.Add(obj.situacion == null ? null : "'" + obj.situacion + "'");
				arrValores.Add(obj.direccion == null ? null : "'" + obj.direccion + "'");
				arrValores.Add(obj.numero == null ? null : "'" + obj.numero + "'");
				arrValores.Add(obj.edificio == null ? null : "'" + obj.edificio + "'");
				arrValores.Add(obj.piso == null ? null : "'" + obj.piso + "'");
				arrValores.Add(obj.oficina == null ? null : "'" + obj.oficina + "'");
				arrValores.Add(obj.zona == null ? null : "'" + obj.zona + "'");
				arrValores.Add(obj.entre_calles == null ? null : "'" + obj.entre_calles + "'");
				arrValores.Add(obj.referencia == null ? null : "'" + obj.referencia + "'");
				arrValores.Add(obj.telefono == null ? null : "'" + obj.telefono + "'");
				arrValores.Add(obj.fax == null ? null : "'" + obj.fax + "'");
				arrValores.Add(obj.casilla == null ? null : "'" + obj.casilla + "'");
				arrValores.Add(obj.nombre_informante == null ? null : "'" + obj.nombre_informante + "'");
				arrValores.Add(obj.e_mail == null ? null : "'" + obj.e_mail + "'");
				arrValores.Add(obj.pagina_web == null ? null : "'" + obj.pagina_web + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.geoposicion == null ? null : "'" + obj.geoposicion + "'");
				arrValores.Add(obj.usugeo == null ? null : "'" + obj.usugeo + "'");
				arrValores.Add(obj.geoposicion_fabrica == null ? null : "'" + obj.geoposicion_fabrica + "'");
				arrValores.Add(obj.usuge_fabrica == null ? null : "'" + obj.usuge_fabrica + "'");
				arrValores.Add(obj.gps_informante == null ? null : "'" + obj.gps_informante + "'");
				arrValores.Add(obj.gps_fabrica == null ? null : "'" + obj.gps_fabrica + "'");

			
				cConn local = new cConn();
				int intIdentidad = -1;
				bool res = local.insertBD(entIndIndustria.strNombreTabla, arrColumnas, arrValores, ref intIdentidad);
				obj.id_industria = intIdentidad;
				return res;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla ind_industria a partir de una clase del tipo Eind_industria
		/// </summary>
		/// <param name="obj" type="Entidades.entIndIndustria">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla ind_industriaind_industria
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industria
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionind_industria
		/// </returns>
		public bool InsertIdentity(entIndIndustria obj, ref cTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustria.Fields.id_industria.ToString());
				arrColumnas.Add(entIndIndustria.Fields.id_departamento.ToString());
				arrColumnas.Add(entIndIndustria.Fields.razon_social.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nombre_comercial.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nit.ToString());
				arrColumnas.Add(entIndIndustria.Fields.regine.ToString());
				arrColumnas.Add(entIndIndustria.Fields.situacion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.direccion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.numero.ToString());
				arrColumnas.Add(entIndIndustria.Fields.edificio.ToString());
				arrColumnas.Add(entIndIndustria.Fields.piso.ToString());
				arrColumnas.Add(entIndIndustria.Fields.oficina.ToString());
				arrColumnas.Add(entIndIndustria.Fields.zona.ToString());
				arrColumnas.Add(entIndIndustria.Fields.entre_calles.ToString());
				arrColumnas.Add(entIndIndustria.Fields.referencia.ToString());
				arrColumnas.Add(entIndIndustria.Fields.telefono.ToString());
				arrColumnas.Add(entIndIndustria.Fields.fax.ToString());
				arrColumnas.Add(entIndIndustria.Fields.casilla.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nombre_informante.ToString());
				arrColumnas.Add(entIndIndustria.Fields.e_mail.ToString());
				arrColumnas.Add(entIndIndustria.Fields.pagina_web.ToString());
				arrColumnas.Add(entIndIndustria.Fields.apiestado.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usucre.ToString());
				arrColumnas.Add(entIndIndustria.Fields.feccre.ToString());
				arrColumnas.Add(entIndIndustria.Fields.geoposicion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usugeo.ToString());
				arrColumnas.Add(entIndIndustria.Fields.geoposicion_fabrica.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usuge_fabrica.ToString());
				arrColumnas.Add(entIndIndustria.Fields.gps_informante.ToString());
				arrColumnas.Add(entIndIndustria.Fields.gps_fabrica.ToString());
				
				ArrayList arrValores = new ArrayList();
				arrValores.Add(obj.id_industria);
				arrValores.Add(obj.id_departamento);
				arrValores.Add(obj.razon_social == null ? null : "'" + obj.razon_social + "'");
				arrValores.Add(obj.nombre_comercial == null ? null : "'" + obj.nombre_comercial + "'");
				arrValores.Add(obj.nit == null ? null : "'" + obj.nit + "'");
				arrValores.Add(obj.regine == null ? null : "'" + obj.regine + "'");
				arrValores.Add(obj.situacion == null ? null : "'" + obj.situacion + "'");
				arrValores.Add(obj.direccion == null ? null : "'" + obj.direccion + "'");
				arrValores.Add(obj.numero == null ? null : "'" + obj.numero + "'");
				arrValores.Add(obj.edificio == null ? null : "'" + obj.edificio + "'");
				arrValores.Add(obj.piso == null ? null : "'" + obj.piso + "'");
				arrValores.Add(obj.oficina == null ? null : "'" + obj.oficina + "'");
				arrValores.Add(obj.zona == null ? null : "'" + obj.zona + "'");
				arrValores.Add(obj.entre_calles == null ? null : "'" + obj.entre_calles + "'");
				arrValores.Add(obj.referencia == null ? null : "'" + obj.referencia + "'");
				arrValores.Add(obj.telefono == null ? null : "'" + obj.telefono + "'");
				arrValores.Add(obj.fax == null ? null : "'" + obj.fax + "'");
				arrValores.Add(obj.casilla == null ? null : "'" + obj.casilla + "'");
				arrValores.Add(obj.nombre_informante == null ? null : "'" + obj.nombre_informante + "'");
				arrValores.Add(obj.e_mail == null ? null : "'" + obj.e_mail + "'");
				arrValores.Add(obj.pagina_web == null ? null : "'" + obj.pagina_web + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.geoposicion == null ? null : "'" + obj.geoposicion + "'");
				arrValores.Add(obj.usugeo == null ? null : "'" + obj.usugeo + "'");
				arrValores.Add(obj.geoposicion_fabrica == null ? null : "'" + obj.geoposicion_fabrica + "'");
				arrValores.Add(obj.usuge_fabrica == null ? null : "'" + obj.usuge_fabrica + "'");
				arrValores.Add(obj.gps_informante == null ? null : "'" + obj.gps_informante + "'");
				arrValores.Add(obj.gps_fabrica == null ? null : "'" + obj.gps_fabrica + "'");

			
				cConn local = new cConn();
				int intIdentidad = -1;
				bool res = local.insertBD(entIndIndustria.strNombreTabla, arrColumnas, arrValores, ref intIdentidad, ref localTrans);
				obj.id_industria = intIdentidad;
				return res;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla ind_industria a partir de una clase del tipo Eind_industria
		/// </summary>
		/// <param name="obj" type="Entidades.entIndIndustria">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla ind_industria
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionind_industria
		/// </returns>
		public int Update(entIndIndustria obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustria.Fields.id_departamento.ToString());
				arrColumnas.Add(entIndIndustria.Fields.razon_social.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nombre_comercial.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nit.ToString());
				arrColumnas.Add(entIndIndustria.Fields.regine.ToString());
				arrColumnas.Add(entIndIndustria.Fields.situacion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.direccion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.numero.ToString());
				arrColumnas.Add(entIndIndustria.Fields.edificio.ToString());
				arrColumnas.Add(entIndIndustria.Fields.piso.ToString());
				arrColumnas.Add(entIndIndustria.Fields.oficina.ToString());
				arrColumnas.Add(entIndIndustria.Fields.zona.ToString());
				arrColumnas.Add(entIndIndustria.Fields.entre_calles.ToString());
				arrColumnas.Add(entIndIndustria.Fields.referencia.ToString());
				arrColumnas.Add(entIndIndustria.Fields.telefono.ToString());
				arrColumnas.Add(entIndIndustria.Fields.fax.ToString());
				arrColumnas.Add(entIndIndustria.Fields.casilla.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nombre_informante.ToString());
				arrColumnas.Add(entIndIndustria.Fields.e_mail.ToString());
				arrColumnas.Add(entIndIndustria.Fields.pagina_web.ToString());
				arrColumnas.Add(entIndIndustria.Fields.apiestado.ToString());
				arrColumnas.Add(entIndIndustria.Fields.apitransaccion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usucre.ToString());
				arrColumnas.Add(entIndIndustria.Fields.feccre.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usumod.ToString());
			
				ArrayList arrValores = new ArrayList();
				arrValores.Add(obj.id_departamento);
				arrValores.Add(obj.razon_social == null ? null : "'" + obj.razon_social + "'");
				arrValores.Add(obj.nombre_comercial == null ? null : "'" + obj.nombre_comercial + "'");
				arrValores.Add(obj.nit == null ? null : "'" + obj.nit + "'");
				arrValores.Add(obj.regine == null ? null : "'" + obj.regine + "'");
				arrValores.Add(obj.situacion == null ? null : "'" + obj.situacion + "'");
				arrValores.Add(obj.direccion == null ? null : "'" + obj.direccion + "'");
				arrValores.Add(obj.numero == null ? null : "'" + obj.numero + "'");
				arrValores.Add(obj.edificio == null ? null : "'" + obj.edificio + "'");
				arrValores.Add(obj.piso == null ? null : "'" + obj.piso + "'");
				arrValores.Add(obj.oficina == null ? null : "'" + obj.oficina + "'");
				arrValores.Add(obj.zona == null ? null : "'" + obj.zona + "'");
				arrValores.Add(obj.entre_calles == null ? null : "'" + obj.entre_calles + "'");
				arrValores.Add(obj.referencia == null ? null : "'" + obj.referencia + "'");
				arrValores.Add(obj.telefono == null ? null : "'" + obj.telefono + "'");
				arrValores.Add(obj.fax == null ? null : "'" + obj.fax + "'");
				arrValores.Add(obj.casilla == null ? null : "'" + obj.casilla + "'");
				arrValores.Add(obj.nombre_informante == null ? null : "'" + obj.nombre_informante + "'");
				arrValores.Add(obj.e_mail == null ? null : "'" + obj.e_mail + "'");
				arrValores.Add(obj.pagina_web == null ? null : "'" + obj.pagina_web + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.geoposicion == null ? null : "'" + obj.geoposicion + "'");
				arrValores.Add(obj.usugeo == null ? null : "'" + obj.usugeo + "'");
				arrValores.Add(obj.geoposicion_fabrica == null ? null : "'" + obj.geoposicion_fabrica + "'");
				arrValores.Add(obj.usuge_fabrica == null ? null : "'" + obj.usuge_fabrica + "'");
				arrValores.Add(obj.gps_informante == null ? null : "'" + obj.gps_informante + "'");
				arrValores.Add(obj.gps_fabrica == null ? null : "'" + obj.gps_fabrica + "'");

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(entIndIndustria.Fields.id_industria.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.id_industria);

			
				cConn local = new cConn();
				return local.updateBD(entIndIndustria.strNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla ind_industria a partir de una clase del tipo eind_industria
		/// </summary>
		/// <param name="obj" type="Entidades.entIndIndustria">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla ind_industria
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industria
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacion
		/// </returns>
		public int Update(entIndIndustria obj, ref cTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustria.Fields.id_departamento.ToString());
				arrColumnas.Add(entIndIndustria.Fields.razon_social.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nombre_comercial.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nit.ToString());
				arrColumnas.Add(entIndIndustria.Fields.regine.ToString());
				arrColumnas.Add(entIndIndustria.Fields.situacion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.direccion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.numero.ToString());
				arrColumnas.Add(entIndIndustria.Fields.edificio.ToString());
				arrColumnas.Add(entIndIndustria.Fields.piso.ToString());
				arrColumnas.Add(entIndIndustria.Fields.oficina.ToString());
				arrColumnas.Add(entIndIndustria.Fields.zona.ToString());
				arrColumnas.Add(entIndIndustria.Fields.entre_calles.ToString());
				arrColumnas.Add(entIndIndustria.Fields.referencia.ToString());
				arrColumnas.Add(entIndIndustria.Fields.telefono.ToString());
				arrColumnas.Add(entIndIndustria.Fields.fax.ToString());
				arrColumnas.Add(entIndIndustria.Fields.casilla.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nombre_informante.ToString());
				arrColumnas.Add(entIndIndustria.Fields.e_mail.ToString());
				arrColumnas.Add(entIndIndustria.Fields.pagina_web.ToString());
				arrColumnas.Add(entIndIndustria.Fields.apiestado.ToString());
				arrColumnas.Add(entIndIndustria.Fields.apitransaccion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usucre.ToString());
				arrColumnas.Add(entIndIndustria.Fields.feccre.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usumod.ToString());
			
				ArrayList arrValores = new ArrayList();
				arrValores.Add(obj.id_departamento);
				arrValores.Add(obj.razon_social == null ? null : "'" + obj.razon_social + "'");
				arrValores.Add(obj.nombre_comercial == null ? null : "'" + obj.nombre_comercial + "'");
				arrValores.Add(obj.nit == null ? null : "'" + obj.nit + "'");
				arrValores.Add(obj.regine == null ? null : "'" + obj.regine + "'");
				arrValores.Add(obj.situacion == null ? null : "'" + obj.situacion + "'");
				arrValores.Add(obj.direccion == null ? null : "'" + obj.direccion + "'");
				arrValores.Add(obj.numero == null ? null : "'" + obj.numero + "'");
				arrValores.Add(obj.edificio == null ? null : "'" + obj.edificio + "'");
				arrValores.Add(obj.piso == null ? null : "'" + obj.piso + "'");
				arrValores.Add(obj.oficina == null ? null : "'" + obj.oficina + "'");
				arrValores.Add(obj.zona == null ? null : "'" + obj.zona + "'");
				arrValores.Add(obj.entre_calles == null ? null : "'" + obj.entre_calles + "'");
				arrValores.Add(obj.referencia == null ? null : "'" + obj.referencia + "'");
				arrValores.Add(obj.telefono == null ? null : "'" + obj.telefono + "'");
				arrValores.Add(obj.fax == null ? null : "'" + obj.fax + "'");
				arrValores.Add(obj.casilla == null ? null : "'" + obj.casilla + "'");
				arrValores.Add(obj.nombre_informante == null ? null : "'" + obj.nombre_informante + "'");
				arrValores.Add(obj.e_mail == null ? null : "'" + obj.e_mail + "'");
				arrValores.Add(obj.pagina_web == null ? null : "'" + obj.pagina_web + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.geoposicion == null ? null : "'" + obj.geoposicion + "'");
				arrValores.Add(obj.usugeo == null ? null : "'" + obj.usugeo + "'");
				arrValores.Add(obj.geoposicion_fabrica == null ? null : "'" + obj.geoposicion_fabrica + "'");
				arrValores.Add(obj.usuge_fabrica == null ? null : "'" + obj.usuge_fabrica + "'");
				arrValores.Add(obj.gps_informante == null ? null : "'" + obj.gps_informante + "'");
				arrValores.Add(obj.gps_fabrica == null ? null : "'" + obj.gps_fabrica + "'");

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(entIndIndustria.Fields.id_industria.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.id_industria);

			
				cConn local = new cConn();
				return local.updateBD(entIndIndustria.strNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla ind_industria a partir de una clase del tipo entIndIndustria y su respectiva PK
		/// </summary>
		/// <param name="obj" type="Entidades.entIndIndustria">
		///     <para>
		/// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla ind_industria
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionind_industria
		/// </returns>
		public int Delete(entIndIndustria obj)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(entIndIndustria.Fields.id_industria.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.id_industria);

			
				cConn local = new cConn();
				return local.deleteBD(entIndIndustria.strNombreTabla, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla ind_industria a partir de una clase del tipo entIndIndustria y su PK
		/// </summary>
		/// <param name="obj" type="Entidades.eind_industria">
		///     <para>
		/// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla ind_industria
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industria
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacionind_industria
		/// </returns>
		public int Delete(entIndIndustria obj, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(entIndIndustria.Fields.id_industria.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.id_industria);

			
				cConn local = new cConn();
				return local.deleteBD(entIndIndustria.strNombreTabla, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla ind_industria a partir de una clase del tipo eind_industria
		/// </summary>
		/// <param name="arrColumnasWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Valores para cada una de las columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionind_industria
		/// </returns>
		public int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				cConn local = new cConn();
				return local.deleteBD("ind_industria", arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla ind_industria a partir de una clase del tipo eind_industria
		/// </summary>
		/// <param name="arrColumnasWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Valores para cada una de las columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industria
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacionind_industria
		/// </returns>
		public int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			try
			{
				cConn local = new cConn();
				return local.deleteBD(entIndIndustria.strNombreTabla, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		

		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla ind_industria
		/// </summary>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ind_industria
		/// </returns>
		public DataTable NuevoDataTable()
		{
			try
			{
				DataTable table = new DataTable ();
				DataColumn dc;
				dc = new DataColumn(entIndIndustria.Fields.id_industria.ToString(),typeof(entIndIndustria).GetProperty(entIndIndustria.Fields.id_industria.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entIndIndustria.Fields.id_departamento.ToString(),typeof(entIndIndustria).GetProperty(entIndIndustria.Fields.id_departamento.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entIndIndustria.Fields.razon_social.ToString(),typeof(entIndIndustria).GetProperty(entIndIndustria.Fields.razon_social.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entIndIndustria.Fields.nombre_comercial.ToString(),typeof(entIndIndustria).GetProperty(entIndIndustria.Fields.nombre_comercial.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entIndIndustria.Fields.nit.ToString(),typeof(entIndIndustria).GetProperty(entIndIndustria.Fields.nit.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entIndIndustria.Fields.regine.ToString(),typeof(entIndIndustria).GetProperty(entIndIndustria.Fields.regine.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entIndIndustria.Fields.situacion.ToString(),typeof(entIndIndustria).GetProperty(entIndIndustria.Fields.situacion.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entIndIndustria.Fields.direccion.ToString(),typeof(entIndIndustria).GetProperty(entIndIndustria.Fields.direccion.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entIndIndustria.Fields.numero.ToString(),typeof(entIndIndustria).GetProperty(entIndIndustria.Fields.numero.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entIndIndustria.Fields.edificio.ToString(),typeof(entIndIndustria).GetProperty(entIndIndustria.Fields.edificio.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entIndIndustria.Fields.piso.ToString(),typeof(entIndIndustria).GetProperty(entIndIndustria.Fields.piso.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entIndIndustria.Fields.oficina.ToString(),typeof(entIndIndustria).GetProperty(entIndIndustria.Fields.oficina.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entIndIndustria.Fields.zona.ToString(),typeof(entIndIndustria).GetProperty(entIndIndustria.Fields.zona.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entIndIndustria.Fields.entre_calles.ToString(),typeof(entIndIndustria).GetProperty(entIndIndustria.Fields.entre_calles.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entIndIndustria.Fields.referencia.ToString(),typeof(entIndIndustria).GetProperty(entIndIndustria.Fields.referencia.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entIndIndustria.Fields.telefono.ToString(),typeof(entIndIndustria).GetProperty(entIndIndustria.Fields.telefono.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entIndIndustria.Fields.fax.ToString(),typeof(entIndIndustria).GetProperty(entIndIndustria.Fields.fax.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entIndIndustria.Fields.casilla.ToString(),typeof(entIndIndustria).GetProperty(entIndIndustria.Fields.casilla.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entIndIndustria.Fields.nombre_informante.ToString(),typeof(entIndIndustria).GetProperty(entIndIndustria.Fields.nombre_informante.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entIndIndustria.Fields.e_mail.ToString(),typeof(entIndIndustria).GetProperty(entIndIndustria.Fields.e_mail.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entIndIndustria.Fields.pagina_web.ToString(),typeof(entIndIndustria).GetProperty(entIndIndustria.Fields.pagina_web.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entIndIndustria.Fields.apiestado.ToString(),typeof(entIndIndustria).GetProperty(entIndIndustria.Fields.apiestado.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entIndIndustria.Fields.apitransaccion.ToString(),typeof(entIndIndustria).GetProperty(entIndIndustria.Fields.apitransaccion.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entIndIndustria.Fields.usucre.ToString(),typeof(entIndIndustria).GetProperty(entIndIndustria.Fields.usucre.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entIndIndustria.Fields.feccre.ToString(),typeof(entIndIndustria).GetProperty(entIndIndustria.Fields.feccre.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entIndIndustria.Fields.usumod.ToString(),typeof(entIndIndustria).GetProperty(entIndIndustria.Fields.usumod.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entIndIndustria.Fields.fecmod.ToString(),typeof(entIndIndustria).GetProperty(entIndIndustria.Fields.fecmod.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entIndIndustria.Fields.geoposicion.ToString(),typeof(entIndIndustria).GetProperty(entIndIndustria.Fields.geoposicion.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entIndIndustria.Fields.usugeo.ToString(),typeof(entIndIndustria).GetProperty(entIndIndustria.Fields.usugeo.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entIndIndustria.Fields.geoposicion_fabrica.ToString(),typeof(entIndIndustria).GetProperty(entIndIndustria.Fields.geoposicion_fabrica.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entIndIndustria.Fields.usuge_fabrica.ToString(),typeof(entIndIndustria).GetProperty(entIndIndustria.Fields.usuge_fabrica.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entIndIndustria.Fields.gps_informante.ToString(),typeof(entIndIndustria).GetProperty(entIndIndustria.Fields.gps_informante.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entIndIndustria.Fields.gps_fabrica.ToString(),typeof(entIndIndustria).GetProperty(entIndIndustria.Fields.gps_fabrica.ToString()).PropertyType);
				table.Columns.Add(dc);

				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que genera un DataTable con determinadas columnas de una ind_industria
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ind_industria
		/// </returns>
		public DataTable NuevoDataTable(ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'2'");
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entIndIndustria.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla ind_industria
		/// </summary>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ind_industria
		/// </returns>
		public DataTable CargarDataTable()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return CargarDataTable(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla y n condicion WHERE ind_industria
		/// </summary>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ind_industria
		/// </returns>
		public DataTable CargarDataTable(String strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return CargarDataTable(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla ind_industria
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ind_industria
		/// </returns>
		public DataTable CargarDataTable(ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				return CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla ind_industria
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ind_industria
		/// </returns>
		public DataTable CargarDataTable(ArrayList arrColumnas, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				return CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla ind_industria
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ind_industria
		/// </returns>
		public DataTable CargarDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla ind_industria
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ind_industria
		/// </returns>
		public DataTable CargarDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return CargarDataTable(arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla ind_industria
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ind_industria
		/// </returns>
		public DataTable CargarDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entIndIndustria.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla ind_industria
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ind_industria
		/// </returns>
		public DataTable CargarDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustria.Fields.id_industria.ToString());
				arrColumnas.Add(entIndIndustria.Fields.id_departamento.ToString());
				arrColumnas.Add(entIndIndustria.Fields.razon_social.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nombre_comercial.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nit.ToString());
				arrColumnas.Add(entIndIndustria.Fields.regine.ToString());
				arrColumnas.Add(entIndIndustria.Fields.situacion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.direccion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.numero.ToString());
				arrColumnas.Add(entIndIndustria.Fields.edificio.ToString());
				arrColumnas.Add(entIndIndustria.Fields.piso.ToString());
				arrColumnas.Add(entIndIndustria.Fields.oficina.ToString());
				arrColumnas.Add(entIndIndustria.Fields.zona.ToString());
				arrColumnas.Add(entIndIndustria.Fields.entre_calles.ToString());
				arrColumnas.Add(entIndIndustria.Fields.referencia.ToString());
				arrColumnas.Add(entIndIndustria.Fields.telefono.ToString());
				arrColumnas.Add(entIndIndustria.Fields.fax.ToString());
				arrColumnas.Add(entIndIndustria.Fields.casilla.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nombre_informante.ToString());
				arrColumnas.Add(entIndIndustria.Fields.e_mail.ToString());
				arrColumnas.Add(entIndIndustria.Fields.pagina_web.ToString());
				arrColumnas.Add(entIndIndustria.Fields.apiestado.ToString());
				arrColumnas.Add(entIndIndustria.Fields.apitransaccion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usucre.ToString());
				arrColumnas.Add(entIndIndustria.Fields.feccre.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usumod.ToString());
				arrColumnas.Add(entIndIndustria.Fields.fecmod.ToString());
				arrColumnas.Add(entIndIndustria.Fields.geoposicion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usugeo.ToString());
				arrColumnas.Add(entIndIndustria.Fields.geoposicion_fabrica.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usuge_fabrica.ToString());
				arrColumnas.Add(entIndIndustria.Fields.gps_informante.ToString());
				arrColumnas.Add(entIndIndustria.Fields.gps_fabrica.ToString());
				
				return CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="searchField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable CargarDataTable(entIndIndustria.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return CargarDataTable(arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="searchField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable CargarDataTable(entIndIndustria.Fields searchField, object searchValue, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return CargarDataTable(arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable CargarDataTable(ArrayList arrColumnas, entIndIndustria.Fields searchField, object searchValue, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable CargarDataTable(ArrayList arrColumnas, entIndIndustria.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla ind_industria
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ind_industria
		/// </returns>
		public DataTable CargarDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustria.Fields.id_industria.ToString());
				arrColumnas.Add(entIndIndustria.Fields.id_departamento.ToString());
				arrColumnas.Add(entIndIndustria.Fields.razon_social.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nombre_comercial.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nit.ToString());
				arrColumnas.Add(entIndIndustria.Fields.regine.ToString());
				arrColumnas.Add(entIndIndustria.Fields.situacion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.direccion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.numero.ToString());
				arrColumnas.Add(entIndIndustria.Fields.edificio.ToString());
				arrColumnas.Add(entIndIndustria.Fields.piso.ToString());
				arrColumnas.Add(entIndIndustria.Fields.oficina.ToString());
				arrColumnas.Add(entIndIndustria.Fields.zona.ToString());
				arrColumnas.Add(entIndIndustria.Fields.entre_calles.ToString());
				arrColumnas.Add(entIndIndustria.Fields.referencia.ToString());
				arrColumnas.Add(entIndIndustria.Fields.telefono.ToString());
				arrColumnas.Add(entIndIndustria.Fields.fax.ToString());
				arrColumnas.Add(entIndIndustria.Fields.casilla.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nombre_informante.ToString());
				arrColumnas.Add(entIndIndustria.Fields.e_mail.ToString());
				arrColumnas.Add(entIndIndustria.Fields.pagina_web.ToString());
				arrColumnas.Add(entIndIndustria.Fields.apiestado.ToString());
				arrColumnas.Add(entIndIndustria.Fields.apitransaccion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usucre.ToString());
				arrColumnas.Add(entIndIndustria.Fields.feccre.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usumod.ToString());
				arrColumnas.Add(entIndIndustria.Fields.fecmod.ToString());
				arrColumnas.Add(entIndIndustria.Fields.geoposicion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usugeo.ToString());
				arrColumnas.Add(entIndIndustria.Fields.geoposicion_fabrica.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usuge_fabrica.ToString());
				arrColumnas.Add(entIndIndustria.Fields.gps_informante.ToString());
				arrColumnas.Add(entIndIndustria.Fields.gps_fabrica.ToString());
				return CargarDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla ind_industria
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ind_industria
		/// </returns>
		public DataTable CargarDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return CargarDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla ind_industria
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ind_industria
		/// </returns>
		public DataTable CargarDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DataTable table = local.cargarDataTableOr(cParametros.schema + entIndIndustria.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ind_industria
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla ind_industriaind_industria
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ind_industria
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla ind_industriaind_industria
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ind_industria
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla ind_industriaind_industria
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ind_industria
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla ind_industriaind_industria
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustria.Fields.id_industria.ToString());
				arrColumnas.Add(entIndIndustria.Fields.id_departamento.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ind_industria
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ind_industriaind_industria
		///     </para>
		/// </param>
		/// <param name="valueField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entIndIndustria.Fields valueField, entIndIndustria.Fields textField)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ind_industria
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ind_industriaind_industria
		///     </para>
		/// </param>
		/// <param name="valueField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entIndIndustria.Fields valueField, entIndIndustria.Fields textField, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ind_industria
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ind_industriaind_industria
		///     </para>
		/// </param>
		/// <param name="valueField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entIndIndustria.Fields valueField, String textField)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ind_industria
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ind_industriaind_industria
		///     </para>
		/// </param>
		/// <param name="valueField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entIndIndustria.Fields valueField, String textField, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ind_industria
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ind_industriaind_industria
		///     </para>
		/// </param>
		/// <param name="valueField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entIndIndustria.Fields valueField, entIndIndustria.Fields textField, entIndIndustria.Fields searchField, object searchValue)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField.ToString(), searchField, searchValue);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ind_industria
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ind_industriaind_industria
		///     </para>
		/// </param>
		/// <param name="valueField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entIndIndustria.Fields valueField, String textField, entIndIndustria.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ind_industria
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ind_industriaind_industria
		///     </para>
		/// </param>
		/// <param name="valueField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entIndIndustria.Fields valueField, entIndIndustria.Fields textField, entIndIndustria.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ind_industria
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ind_industriaind_industria
		///     </para>
		/// </param>
		/// <param name="valueField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entIndIndustria.Fields valueField, String textField, entIndIndustria.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ind_industria
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ind_industriaind_industria
		///     </para>
		/// </param>
		/// <param name="valueField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entIndIndustria.Fields valueField, entIndIndustria.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ind_industria
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ind_industriaind_industria
		///     </para>
		/// </param>
		/// <param name="valueField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entIndIndustria.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ind_industria
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ind_industriaind_industria
		///     </para>
		/// </param>
		/// <param name="valueField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entIndIndustria.Fields valueField, entIndIndustria.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ind_industria
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ind_industriaind_industria
		///     </para>
		/// </param>
		/// <param name="valueField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entIndIndustria.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ind_industria
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ind_industriaind_industria
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entIndIndustria.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Columns.Count > 0)
				{
					cmb.ValueMember = table.Columns[0].ColumnName;
					cmb.DisplayMember = table.Columns[1].ColumnName;
					cmb.DataSource = table;

				}
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla ind_industria
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustria.Fields.id_industria.ToString());
				arrColumnas.Add(entIndIndustria.Fields.id_departamento.ToString());
				arrColumnas.Add(entIndIndustria.Fields.razon_social.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nombre_comercial.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nit.ToString());
				arrColumnas.Add(entIndIndustria.Fields.regine.ToString());
				arrColumnas.Add(entIndIndustria.Fields.situacion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.direccion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.numero.ToString());
				arrColumnas.Add(entIndIndustria.Fields.edificio.ToString());
				arrColumnas.Add(entIndIndustria.Fields.piso.ToString());
				arrColumnas.Add(entIndIndustria.Fields.oficina.ToString());
				arrColumnas.Add(entIndIndustria.Fields.zona.ToString());
				arrColumnas.Add(entIndIndustria.Fields.entre_calles.ToString());
				arrColumnas.Add(entIndIndustria.Fields.referencia.ToString());
				arrColumnas.Add(entIndIndustria.Fields.telefono.ToString());
				arrColumnas.Add(entIndIndustria.Fields.fax.ToString());
				arrColumnas.Add(entIndIndustria.Fields.casilla.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nombre_informante.ToString());
				arrColumnas.Add(entIndIndustria.Fields.e_mail.ToString());
				arrColumnas.Add(entIndIndustria.Fields.pagina_web.ToString());
				arrColumnas.Add(entIndIndustria.Fields.apiestado.ToString());
				arrColumnas.Add(entIndIndustria.Fields.apitransaccion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usucre.ToString());
				arrColumnas.Add(entIndIndustria.Fields.feccre.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usumod.ToString());
				arrColumnas.Add(entIndIndustria.Fields.fecmod.ToString());
				arrColumnas.Add(entIndIndustria.Fields.geoposicion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usugeo.ToString());
				arrColumnas.Add(entIndIndustria.Fields.geoposicion_fabrica.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usuge_fabrica.ToString());
				arrColumnas.Add(entIndIndustria.Fields.gps_informante.ToString());
				arrColumnas.Add(entIndIndustria.Fields.gps_fabrica.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla ind_industria
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		  Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustria.Fields.id_industria.ToString());
				arrColumnas.Add(entIndIndustria.Fields.id_departamento.ToString());
				arrColumnas.Add(entIndIndustria.Fields.razon_social.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nombre_comercial.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nit.ToString());
				arrColumnas.Add(entIndIndustria.Fields.regine.ToString());
				arrColumnas.Add(entIndIndustria.Fields.situacion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.direccion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.numero.ToString());
				arrColumnas.Add(entIndIndustria.Fields.edificio.ToString());
				arrColumnas.Add(entIndIndustria.Fields.piso.ToString());
				arrColumnas.Add(entIndIndustria.Fields.oficina.ToString());
				arrColumnas.Add(entIndIndustria.Fields.zona.ToString());
				arrColumnas.Add(entIndIndustria.Fields.entre_calles.ToString());
				arrColumnas.Add(entIndIndustria.Fields.referencia.ToString());
				arrColumnas.Add(entIndIndustria.Fields.telefono.ToString());
				arrColumnas.Add(entIndIndustria.Fields.fax.ToString());
				arrColumnas.Add(entIndIndustria.Fields.casilla.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nombre_informante.ToString());
				arrColumnas.Add(entIndIndustria.Fields.e_mail.ToString());
				arrColumnas.Add(entIndIndustria.Fields.pagina_web.ToString());
				arrColumnas.Add(entIndIndustria.Fields.apiestado.ToString());
				arrColumnas.Add(entIndIndustria.Fields.apitransaccion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usucre.ToString());
				arrColumnas.Add(entIndIndustria.Fields.feccre.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usumod.ToString());
				arrColumnas.Add(entIndIndustria.Fields.fecmod.ToString());
				arrColumnas.Add(entIndIndustria.Fields.geoposicion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usugeo.ToString());
				arrColumnas.Add(entIndIndustria.Fields.geoposicion_fabrica.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usuge_fabrica.ToString());
				arrColumnas.Add(entIndIndustria.Fields.gps_informante.ToString());
				arrColumnas.Add(entIndIndustria.Fields.gps_fabrica.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla ind_industria
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla ind_industria
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla ind_industria
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustria.Fields.id_industria.ToString());
				arrColumnas.Add(entIndIndustria.Fields.id_departamento.ToString());
				arrColumnas.Add(entIndIndustria.Fields.razon_social.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nombre_comercial.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nit.ToString());
				arrColumnas.Add(entIndIndustria.Fields.regine.ToString());
				arrColumnas.Add(entIndIndustria.Fields.situacion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.direccion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.numero.ToString());
				arrColumnas.Add(entIndIndustria.Fields.edificio.ToString());
				arrColumnas.Add(entIndIndustria.Fields.piso.ToString());
				arrColumnas.Add(entIndIndustria.Fields.oficina.ToString());
				arrColumnas.Add(entIndIndustria.Fields.zona.ToString());
				arrColumnas.Add(entIndIndustria.Fields.entre_calles.ToString());
				arrColumnas.Add(entIndIndustria.Fields.referencia.ToString());
				arrColumnas.Add(entIndIndustria.Fields.telefono.ToString());
				arrColumnas.Add(entIndIndustria.Fields.fax.ToString());
				arrColumnas.Add(entIndIndustria.Fields.casilla.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nombre_informante.ToString());
				arrColumnas.Add(entIndIndustria.Fields.e_mail.ToString());
				arrColumnas.Add(entIndIndustria.Fields.pagina_web.ToString());
				arrColumnas.Add(entIndIndustria.Fields.apiestado.ToString());
				arrColumnas.Add(entIndIndustria.Fields.apitransaccion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usucre.ToString());
				arrColumnas.Add(entIndIndustria.Fields.feccre.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usumod.ToString());
				arrColumnas.Add(entIndIndustria.Fields.fecmod.ToString());
				arrColumnas.Add(entIndIndustria.Fields.geoposicion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usugeo.ToString());
				arrColumnas.Add(entIndIndustria.Fields.geoposicion_fabrica.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usuge_fabrica.ToString());
				arrColumnas.Add(entIndIndustria.Fields.gps_informante.ToString());
				arrColumnas.Add(entIndIndustria.Fields.gps_fabrica.ToString());
			
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla ind_industria
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustria.Fields.id_industria.ToString());
				arrColumnas.Add(entIndIndustria.Fields.id_departamento.ToString());
				arrColumnas.Add(entIndIndustria.Fields.razon_social.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nombre_comercial.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nit.ToString());
				arrColumnas.Add(entIndIndustria.Fields.regine.ToString());
				arrColumnas.Add(entIndIndustria.Fields.situacion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.direccion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.numero.ToString());
				arrColumnas.Add(entIndIndustria.Fields.edificio.ToString());
				arrColumnas.Add(entIndIndustria.Fields.piso.ToString());
				arrColumnas.Add(entIndIndustria.Fields.oficina.ToString());
				arrColumnas.Add(entIndIndustria.Fields.zona.ToString());
				arrColumnas.Add(entIndIndustria.Fields.entre_calles.ToString());
				arrColumnas.Add(entIndIndustria.Fields.referencia.ToString());
				arrColumnas.Add(entIndIndustria.Fields.telefono.ToString());
				arrColumnas.Add(entIndIndustria.Fields.fax.ToString());
				arrColumnas.Add(entIndIndustria.Fields.casilla.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nombre_informante.ToString());
				arrColumnas.Add(entIndIndustria.Fields.e_mail.ToString());
				arrColumnas.Add(entIndIndustria.Fields.pagina_web.ToString());
				arrColumnas.Add(entIndIndustria.Fields.apiestado.ToString());
				arrColumnas.Add(entIndIndustria.Fields.apitransaccion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usucre.ToString());
				arrColumnas.Add(entIndIndustria.Fields.feccre.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usumod.ToString());
				arrColumnas.Add(entIndIndustria.Fields.fecmod.ToString());
				arrColumnas.Add(entIndIndustria.Fields.geoposicion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usugeo.ToString());
				arrColumnas.Add(entIndIndustria.Fields.geoposicion_fabrica.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usuge_fabrica.ToString());
				arrColumnas.Add(entIndIndustria.Fields.gps_informante.ToString());
				arrColumnas.Add(entIndIndustria.Fields.gps_fabrica.ToString());
			
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla ind_industria
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla ind_industria
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DbDataReader dsReader = local.cargarDataReaderAnd(cParametros.schema + entIndIndustria.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				dtg.DataSource = dsReader;

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla ind_industria
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, entIndIndustria.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustria.Fields.id_industria.ToString());
				arrColumnas.Add(entIndIndustria.Fields.id_departamento.ToString());
				arrColumnas.Add(entIndIndustria.Fields.razon_social.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nombre_comercial.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nit.ToString());
				arrColumnas.Add(entIndIndustria.Fields.regine.ToString());
				arrColumnas.Add(entIndIndustria.Fields.situacion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.direccion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.numero.ToString());
				arrColumnas.Add(entIndIndustria.Fields.edificio.ToString());
				arrColumnas.Add(entIndIndustria.Fields.piso.ToString());
				arrColumnas.Add(entIndIndustria.Fields.oficina.ToString());
				arrColumnas.Add(entIndIndustria.Fields.zona.ToString());
				arrColumnas.Add(entIndIndustria.Fields.entre_calles.ToString());
				arrColumnas.Add(entIndIndustria.Fields.referencia.ToString());
				arrColumnas.Add(entIndIndustria.Fields.telefono.ToString());
				arrColumnas.Add(entIndIndustria.Fields.fax.ToString());
				arrColumnas.Add(entIndIndustria.Fields.casilla.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nombre_informante.ToString());
				arrColumnas.Add(entIndIndustria.Fields.e_mail.ToString());
				arrColumnas.Add(entIndIndustria.Fields.pagina_web.ToString());
				arrColumnas.Add(entIndIndustria.Fields.apiestado.ToString());
				arrColumnas.Add(entIndIndustria.Fields.apitransaccion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usucre.ToString());
				arrColumnas.Add(entIndIndustria.Fields.feccre.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usumod.ToString());
				arrColumnas.Add(entIndIndustria.Fields.fecmod.ToString());
				arrColumnas.Add(entIndIndustria.Fields.geoposicion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usugeo.ToString());
				arrColumnas.Add(entIndIndustria.Fields.geoposicion_fabrica.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usuge_fabrica.ToString());
				arrColumnas.Add(entIndIndustria.Fields.gps_informante.ToString());
				arrColumnas.Add(entIndIndustria.Fields.gps_fabrica.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla ind_industria
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, entIndIndustria.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustria.Fields.id_industria.ToString());
				arrColumnas.Add(entIndIndustria.Fields.id_departamento.ToString());
				arrColumnas.Add(entIndIndustria.Fields.razon_social.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nombre_comercial.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nit.ToString());
				arrColumnas.Add(entIndIndustria.Fields.regine.ToString());
				arrColumnas.Add(entIndIndustria.Fields.situacion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.direccion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.numero.ToString());
				arrColumnas.Add(entIndIndustria.Fields.edificio.ToString());
				arrColumnas.Add(entIndIndustria.Fields.piso.ToString());
				arrColumnas.Add(entIndIndustria.Fields.oficina.ToString());
				arrColumnas.Add(entIndIndustria.Fields.zona.ToString());
				arrColumnas.Add(entIndIndustria.Fields.entre_calles.ToString());
				arrColumnas.Add(entIndIndustria.Fields.referencia.ToString());
				arrColumnas.Add(entIndIndustria.Fields.telefono.ToString());
				arrColumnas.Add(entIndIndustria.Fields.fax.ToString());
				arrColumnas.Add(entIndIndustria.Fields.casilla.ToString());
				arrColumnas.Add(entIndIndustria.Fields.nombre_informante.ToString());
				arrColumnas.Add(entIndIndustria.Fields.e_mail.ToString());
				arrColumnas.Add(entIndIndustria.Fields.pagina_web.ToString());
				arrColumnas.Add(entIndIndustria.Fields.apiestado.ToString());
				arrColumnas.Add(entIndIndustria.Fields.apitransaccion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usucre.ToString());
				arrColumnas.Add(entIndIndustria.Fields.feccre.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usumod.ToString());
				arrColumnas.Add(entIndIndustria.Fields.fecmod.ToString());
				arrColumnas.Add(entIndIndustria.Fields.geoposicion.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usugeo.ToString());
				arrColumnas.Add(entIndIndustria.Fields.geoposicion_fabrica.ToString());
				arrColumnas.Add(entIndIndustria.Fields.usuge_fabrica.ToString());
				arrColumnas.Add(entIndIndustria.Fields.gps_informante.ToString());
				arrColumnas.Add(entIndIndustria.Fields.gps_fabrica.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla ind_industria
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entIndIndustria.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla ind_industria
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entIndIndustria.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla ind_industria
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarDataGridOr(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla ind_industria
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DbDataReader dsReader = local.cargarDataReaderOr(cParametros.schema + entIndIndustria.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				dtg.DataSource = dsReader;

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entIndIndustria que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesCount(entIndIndustria.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entIndIndustria que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesCount(entIndIndustria.Fields refField, entIndIndustria.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entIndIndustria que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesCount(entIndIndustria.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("count(" + refField + ")");
				DataTable dtTemp = CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return dtTemp.Rows[0][0];
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entIndIndustria que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesMin(entIndIndustria.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entIndIndustria que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesMin(entIndIndustria.Fields refField, entIndIndustria.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entIndIndustria que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesMin(entIndIndustria.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("min(" + refField + ")");
				DataTable dtTemp = CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return dtTemp.Rows[0][0];
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entIndIndustria que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesMax(entIndIndustria.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entIndIndustria que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesMax(entIndIndustria.Fields refField, entIndIndustria.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entIndIndustria que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesMax(entIndIndustria.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("max(" + refField + ")");
				DataTable dtTemp = CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return dtTemp.Rows[0][0];
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entIndIndustria que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesSum(entIndIndustria.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entIndIndustria que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesSum(entIndIndustria.Fields refField, entIndIndustria.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entIndIndustria que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesSum(entIndIndustria.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("sum(" + refField + ")");
				DataTable dtTemp = CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return dtTemp.Rows[0][0];
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entIndIndustria que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesAvg(entIndIndustria.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entIndIndustria que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesAvg(entIndIndustria.Fields refField, entIndIndustria.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entIndIndustria que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesAvg(entIndIndustria.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("avg(" + refField + ")");
				DataTable dtTemp = CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return dtTemp.Rows[0][0];
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion

		/// <summary>
		/// 	 Funcion que devuelve un objeto a partir de un DataRow
		/// </summary>
		/// <param name="row" type="System.Data.DataRow">
		///     <para>
		/// 		 DataRow con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Objeto ind_industria
		/// </returns>
		internal entIndIndustria crearObjeto(DataRow row)
		{
			entIndIndustria obj = new entIndIndustria();
			obj.id_industria = GetColumnType(row[entIndIndustria.Fields.id_industria.ToString()], entIndIndustria.Fields.id_industria);
			obj.id_departamento = GetColumnType(row[entIndIndustria.Fields.id_departamento.ToString()], entIndIndustria.Fields.id_departamento);
			obj.razon_social = GetColumnType(row[entIndIndustria.Fields.razon_social.ToString()], entIndIndustria.Fields.razon_social);
			obj.nombre_comercial = GetColumnType(row[entIndIndustria.Fields.nombre_comercial.ToString()], entIndIndustria.Fields.nombre_comercial);
			obj.nit = GetColumnType(row[entIndIndustria.Fields.nit.ToString()], entIndIndustria.Fields.nit);
			obj.regine = GetColumnType(row[entIndIndustria.Fields.regine.ToString()], entIndIndustria.Fields.regine);
			obj.situacion = GetColumnType(row[entIndIndustria.Fields.situacion.ToString()], entIndIndustria.Fields.situacion);
			obj.direccion = GetColumnType(row[entIndIndustria.Fields.direccion.ToString()], entIndIndustria.Fields.direccion);
			obj.numero = GetColumnType(row[entIndIndustria.Fields.numero.ToString()], entIndIndustria.Fields.numero);
			obj.edificio = GetColumnType(row[entIndIndustria.Fields.edificio.ToString()], entIndIndustria.Fields.edificio);
			obj.piso = GetColumnType(row[entIndIndustria.Fields.piso.ToString()], entIndIndustria.Fields.piso);
			obj.oficina = GetColumnType(row[entIndIndustria.Fields.oficina.ToString()], entIndIndustria.Fields.oficina);
			obj.zona = GetColumnType(row[entIndIndustria.Fields.zona.ToString()], entIndIndustria.Fields.zona);
			obj.entre_calles = GetColumnType(row[entIndIndustria.Fields.entre_calles.ToString()], entIndIndustria.Fields.entre_calles);
			obj.referencia = GetColumnType(row[entIndIndustria.Fields.referencia.ToString()], entIndIndustria.Fields.referencia);
			obj.telefono = GetColumnType(row[entIndIndustria.Fields.telefono.ToString()], entIndIndustria.Fields.telefono);
			obj.fax = GetColumnType(row[entIndIndustria.Fields.fax.ToString()], entIndIndustria.Fields.fax);
			obj.casilla = GetColumnType(row[entIndIndustria.Fields.casilla.ToString()], entIndIndustria.Fields.casilla);
			obj.nombre_informante = GetColumnType(row[entIndIndustria.Fields.nombre_informante.ToString()], entIndIndustria.Fields.nombre_informante);
			obj.e_mail = GetColumnType(row[entIndIndustria.Fields.e_mail.ToString()], entIndIndustria.Fields.e_mail);
			obj.pagina_web = GetColumnType(row[entIndIndustria.Fields.pagina_web.ToString()], entIndIndustria.Fields.pagina_web);
			obj.apiestado = GetColumnType(row[entIndIndustria.Fields.apiestado.ToString()], entIndIndustria.Fields.apiestado);
			obj.apitransaccion = GetColumnType(row[entIndIndustria.Fields.apitransaccion.ToString()], entIndIndustria.Fields.apitransaccion);
			obj.usucre = GetColumnType(row[entIndIndustria.Fields.usucre.ToString()], entIndIndustria.Fields.usucre);
			obj.feccre = GetColumnType(row[entIndIndustria.Fields.feccre.ToString()], entIndIndustria.Fields.feccre);
			obj.usumod = GetColumnType(row[entIndIndustria.Fields.usumod.ToString()], entIndIndustria.Fields.usumod);
			obj.fecmod = GetColumnType(row[entIndIndustria.Fields.fecmod.ToString()], entIndIndustria.Fields.fecmod);
			obj.geoposicion = GetColumnType(row[entIndIndustria.Fields.geoposicion.ToString()], entIndIndustria.Fields.geoposicion);
			obj.usugeo = GetColumnType(row[entIndIndustria.Fields.usugeo.ToString()], entIndIndustria.Fields.usugeo);
			obj.geoposicion_fabrica = GetColumnType(row[entIndIndustria.Fields.geoposicion_fabrica.ToString()], entIndIndustria.Fields.geoposicion_fabrica);
			obj.usuge_fabrica = GetColumnType(row[entIndIndustria.Fields.usuge_fabrica.ToString()], entIndIndustria.Fields.usuge_fabrica);
			obj.gps_informante = GetColumnType(row[entIndIndustria.Fields.gps_informante.ToString()], entIndIndustria.Fields.gps_informante);
			obj.gps_fabrica = GetColumnType(row[entIndIndustria.Fields.gps_fabrica.ToString()], entIndIndustria.Fields.gps_fabrica);
			return obj;
		}

		/// <summary>
		/// 	 Funcion que devuelve un objeto a partir de un DataRow
		/// </summary>
		/// <param name="row" type="System.Data.DataRow">
		///     <para>
		/// 		 DataRow con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Objeto ind_industria
		/// </returns>
		internal entIndIndustria crearObjetoRevisado(DataRow row)
		{
			entIndIndustria obj = new entIndIndustria();
			if (row.Table.Columns.Contains(entIndIndustria.Fields.id_industria.ToString()))
				obj.id_industria = GetColumnType(row[entIndIndustria.Fields.id_industria.ToString()], entIndIndustria.Fields.id_industria);
			if (row.Table.Columns.Contains(entIndIndustria.Fields.id_departamento.ToString()))
				obj.id_departamento = GetColumnType(row[entIndIndustria.Fields.id_departamento.ToString()], entIndIndustria.Fields.id_departamento);
			if (row.Table.Columns.Contains(entIndIndustria.Fields.razon_social.ToString()))
				obj.razon_social = GetColumnType(row[entIndIndustria.Fields.razon_social.ToString()], entIndIndustria.Fields.razon_social);
			if (row.Table.Columns.Contains(entIndIndustria.Fields.nombre_comercial.ToString()))
				obj.nombre_comercial = GetColumnType(row[entIndIndustria.Fields.nombre_comercial.ToString()], entIndIndustria.Fields.nombre_comercial);
			if (row.Table.Columns.Contains(entIndIndustria.Fields.nit.ToString()))
				obj.nit = GetColumnType(row[entIndIndustria.Fields.nit.ToString()], entIndIndustria.Fields.nit);
			if (row.Table.Columns.Contains(entIndIndustria.Fields.regine.ToString()))
				obj.regine = GetColumnType(row[entIndIndustria.Fields.regine.ToString()], entIndIndustria.Fields.regine);
			if (row.Table.Columns.Contains(entIndIndustria.Fields.situacion.ToString()))
				obj.situacion = GetColumnType(row[entIndIndustria.Fields.situacion.ToString()], entIndIndustria.Fields.situacion);
			if (row.Table.Columns.Contains(entIndIndustria.Fields.direccion.ToString()))
				obj.direccion = GetColumnType(row[entIndIndustria.Fields.direccion.ToString()], entIndIndustria.Fields.direccion);
			if (row.Table.Columns.Contains(entIndIndustria.Fields.numero.ToString()))
				obj.numero = GetColumnType(row[entIndIndustria.Fields.numero.ToString()], entIndIndustria.Fields.numero);
			if (row.Table.Columns.Contains(entIndIndustria.Fields.edificio.ToString()))
				obj.edificio = GetColumnType(row[entIndIndustria.Fields.edificio.ToString()], entIndIndustria.Fields.edificio);
			if (row.Table.Columns.Contains(entIndIndustria.Fields.piso.ToString()))
				obj.piso = GetColumnType(row[entIndIndustria.Fields.piso.ToString()], entIndIndustria.Fields.piso);
			if (row.Table.Columns.Contains(entIndIndustria.Fields.oficina.ToString()))
				obj.oficina = GetColumnType(row[entIndIndustria.Fields.oficina.ToString()], entIndIndustria.Fields.oficina);
			if (row.Table.Columns.Contains(entIndIndustria.Fields.zona.ToString()))
				obj.zona = GetColumnType(row[entIndIndustria.Fields.zona.ToString()], entIndIndustria.Fields.zona);
			if (row.Table.Columns.Contains(entIndIndustria.Fields.entre_calles.ToString()))
				obj.entre_calles = GetColumnType(row[entIndIndustria.Fields.entre_calles.ToString()], entIndIndustria.Fields.entre_calles);
			if (row.Table.Columns.Contains(entIndIndustria.Fields.referencia.ToString()))
				obj.referencia = GetColumnType(row[entIndIndustria.Fields.referencia.ToString()], entIndIndustria.Fields.referencia);
			if (row.Table.Columns.Contains(entIndIndustria.Fields.telefono.ToString()))
				obj.telefono = GetColumnType(row[entIndIndustria.Fields.telefono.ToString()], entIndIndustria.Fields.telefono);
			if (row.Table.Columns.Contains(entIndIndustria.Fields.fax.ToString()))
				obj.fax = GetColumnType(row[entIndIndustria.Fields.fax.ToString()], entIndIndustria.Fields.fax);
			if (row.Table.Columns.Contains(entIndIndustria.Fields.casilla.ToString()))
				obj.casilla = GetColumnType(row[entIndIndustria.Fields.casilla.ToString()], entIndIndustria.Fields.casilla);
			if (row.Table.Columns.Contains(entIndIndustria.Fields.nombre_informante.ToString()))
				obj.nombre_informante = GetColumnType(row[entIndIndustria.Fields.nombre_informante.ToString()], entIndIndustria.Fields.nombre_informante);
			if (row.Table.Columns.Contains(entIndIndustria.Fields.e_mail.ToString()))
				obj.e_mail = GetColumnType(row[entIndIndustria.Fields.e_mail.ToString()], entIndIndustria.Fields.e_mail);
			if (row.Table.Columns.Contains(entIndIndustria.Fields.pagina_web.ToString()))
				obj.pagina_web = GetColumnType(row[entIndIndustria.Fields.pagina_web.ToString()], entIndIndustria.Fields.pagina_web);
			if (row.Table.Columns.Contains(entIndIndustria.Fields.apiestado.ToString()))
				obj.apiestado = GetColumnType(row[entIndIndustria.Fields.apiestado.ToString()], entIndIndustria.Fields.apiestado);
			if (row.Table.Columns.Contains(entIndIndustria.Fields.apitransaccion.ToString()))
				obj.apitransaccion = GetColumnType(row[entIndIndustria.Fields.apitransaccion.ToString()], entIndIndustria.Fields.apitransaccion);
			if (row.Table.Columns.Contains(entIndIndustria.Fields.usucre.ToString()))
				obj.usucre = GetColumnType(row[entIndIndustria.Fields.usucre.ToString()], entIndIndustria.Fields.usucre);
			if (row.Table.Columns.Contains(entIndIndustria.Fields.feccre.ToString()))
				obj.feccre = GetColumnType(row[entIndIndustria.Fields.feccre.ToString()], entIndIndustria.Fields.feccre);
			if (row.Table.Columns.Contains(entIndIndustria.Fields.usumod.ToString()))
				obj.usumod = GetColumnType(row[entIndIndustria.Fields.usumod.ToString()], entIndIndustria.Fields.usumod);
			if (row.Table.Columns.Contains(entIndIndustria.Fields.fecmod.ToString()))
				obj.fecmod = GetColumnType(row[entIndIndustria.Fields.fecmod.ToString()], entIndIndustria.Fields.fecmod);
			if (row.Table.Columns.Contains(entIndIndustria.Fields.geoposicion.ToString()))
				obj.geoposicion = GetColumnType(row[entIndIndustria.Fields.geoposicion.ToString()], entIndIndustria.Fields.geoposicion);
			if (row.Table.Columns.Contains(entIndIndustria.Fields.usugeo.ToString()))
				obj.usugeo = GetColumnType(row[entIndIndustria.Fields.usugeo.ToString()], entIndIndustria.Fields.usugeo);
			if (row.Table.Columns.Contains(entIndIndustria.Fields.geoposicion_fabrica.ToString()))
				obj.geoposicion_fabrica = GetColumnType(row[entIndIndustria.Fields.geoposicion_fabrica.ToString()], entIndIndustria.Fields.geoposicion_fabrica);
			if (row.Table.Columns.Contains(entIndIndustria.Fields.usuge_fabrica.ToString()))
				obj.usuge_fabrica = GetColumnType(row[entIndIndustria.Fields.usuge_fabrica.ToString()], entIndIndustria.Fields.usuge_fabrica);
			if (row.Table.Columns.Contains(entIndIndustria.Fields.gps_informante.ToString()))
				obj.gps_informante = GetColumnType(row[entIndIndustria.Fields.gps_informante.ToString()], entIndIndustria.Fields.gps_informante);
			if (row.Table.Columns.Contains(entIndIndustria.Fields.gps_fabrica.ToString()))
				obj.gps_fabrica = GetColumnType(row[entIndIndustria.Fields.gps_fabrica.ToString()], entIndIndustria.Fields.gps_fabrica);
			return obj;
		}

		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtind_industria" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Lista de Objetos ind_industria
		/// </returns>
		internal List<entIndIndustria> crearLista(DataTable dtind_industria)
		{
			List<entIndIndustria> list = new List<entIndIndustria>();
			
			entIndIndustria obj = new entIndIndustria();
			foreach (DataRow row in dtind_industria.Rows)
			{
				obj = crearObjeto(row);
				list.Add(obj);
			}
			return list;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable y con solo algunas columnas
		/// </summary>
		/// <param name="dtind_industria" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Lista de Objetos ind_industria
		/// </returns>
		internal List<entIndIndustria> crearListaRevisada(DataTable dtind_industria)
		{
			List<entIndIndustria> list = new List<entIndIndustria>();
			
			entIndIndustria obj = new entIndIndustria();
			foreach (DataRow row in dtind_industria.Rows)
			{
				obj = crearObjetoRevisado(row);
				list.Add(obj);
			}
			return list;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtind_industria" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cola de Objetos ind_industria
		/// </returns>
		internal Queue<entIndIndustria> crearCola(DataTable dtind_industria)
		{
			Queue<entIndIndustria> cola = new Queue<entIndIndustria>();
			
			entIndIndustria obj = new entIndIndustria();
			foreach (DataRow row in dtind_industria.Rows)
			{
				obj = crearObjeto(row);
				cola.Enqueue(obj);
			}
			return cola;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtind_industria" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Pila de Objetos ind_industria
		/// </returns>
		internal Stack<entIndIndustria> crearPila(DataTable dtind_industria)
		{
			Stack<entIndIndustria> pila = new Stack<entIndIndustria>();
			
			entIndIndustria obj = new entIndIndustria();
			foreach (DataRow row in dtind_industria.Rows)
			{
				obj = crearObjeto(row);
				pila.Push(obj);
			}
			return pila;
		}
		
		/// <summary>
		/// 	 Funcion que crea un Dicionario a partir de un DataTable
		/// </summary>
		/// <param name="dtind_industria" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Diccionario de Objetos ind_industria
		/// </returns>
		internal Dictionary<String, entIndIndustria> crearDiccionario(DataTable dtind_industria)
		{
			Dictionary<String, entIndIndustria>  miDic = new Dictionary<String, entIndIndustria>();
			
			entIndIndustria obj = new entIndIndustria();
			foreach (DataRow row in dtind_industria.Rows)
			{
				obj = crearObjeto(row);
				miDic.Add(obj.id_industria.ToString(), obj);
			}
			return miDic;
		}
		
		/// <summary>
		/// 	 Funcion que crea un Dicionario a partir de un DataTable y solo con columnas existentes
		/// </summary>
		/// <param name="dtind_industria" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Diccionario de Objetos ind_industria
		/// </returns>
		internal Dictionary<String, entIndIndustria> crearDiccionarioRevisado(DataTable dtind_industria)
		{
			Dictionary<String, entIndIndustria>  miDic = new Dictionary<String, entIndIndustria>();
			
			entIndIndustria obj = new entIndIndustria();
			foreach (DataRow row in dtind_industria.Rows)
			{
				obj = crearObjetoRevisado(row);
				miDic.Add(obj.id_industria.ToString(), obj);
			}
			return miDic;
		}
		
		public void Dispose()
		{
			GC.SuppressFinalize(this);
		}
		
		protected void Finalize()
		{
			Dispose();
		}
	}
}

