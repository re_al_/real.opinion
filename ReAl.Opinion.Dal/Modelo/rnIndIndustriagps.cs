#region 
/***********************************************************************************************************
	NOMBRE:       rnIndIndustriagps
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla ind_industriagps

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/05/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

using ReAl.Opinion.Dal;
using ReAl.Opinion.Dal.Entidades;
using ReAl.Opinion.Dal.Interface;
using System.Windows.Forms;
using ReAl.Opinion.PgConn;

#endregion

namespace ReAl.Opinion.Dal.Modelo
{
	public partial class rnIndIndustriagps: inIndIndustriagps
	{
		//Debe implementar la Interface (Alt + Shift + F10)

		#region inIndIndustriagps Members

		/// <summary>
		/// Metodo que devuelve el Script SQL de la Tabla
		/// </summary>
		/// <returns>Script SQL</returns>
		public string getTableScript()
		{
			TableClass tabla = new TableClass(typeof(entIndIndustriagps));
			return tabla.CreateTableScript();
		}
		
		/// <summary>
		/// Metodo para castear Dinamicamente un Tipo
		/// <summary>
		/// <param name="valor">Tipo a ser casteado</param>
		/// <param name="myField">Enum de la columna</param>
		/// <returns>Devuelve un objeto del Tipo de la columna especificada en el Enum</returns>
		public dynamic GetColumnType(object valor, entIndIndustriagps.Fields myField)
		{
			if (DBNull.Value.Equals(valor)) 
				return null;
			Type destino = typeof(entIndIndustriagps).GetProperty(myField.ToString()).PropertyType;
			var miTipo = Nullable.GetUnderlyingType(destino) ?? destino;
			return Convert.ChangeType(valor, miTipo);
		}

		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo entIndIndustriagps que cumple con los filtros de los parametros
		/// </returns>
		public entIndIndustriagps ObtenerObjeto(int intgestion, int intmes, int intid_gps, String Stringusucre)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(entIndIndustriagps.Fields.gestion.ToString());
			arrColumnasWhere.Add(entIndIndustriagps.Fields.mes.ToString());
			arrColumnasWhere.Add(entIndIndustriagps.Fields.id_gps.ToString());
			arrColumnasWhere.Add(entIndIndustriagps.Fields.usucre.ToString());
		
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(intgestion);
			arrValoresWhere.Add(intmes);
			arrValoresWhere.Add(intid_gps);
			arrValoresWhere.Add("'" + Stringusucre + "'");
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entIndIndustriagps que cumple con los filtros de los parametros
		/// </returns>
		public entIndIndustriagps ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entIndIndustriagps que cumple con los filtros de los parametros
		/// </returns>
		public entIndIndustriagps ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustriagps.Fields.gestion.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.mes.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_gps.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_industria.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.latitud.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.longitud.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.hora_control.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.apiestado.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.apitransaccion.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usucre.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.feccre.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usumod.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.fecmod.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.codigo_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usu_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.fec_manzana.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entIndIndustriagps.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count == 1)
				{
					entIndIndustriagps obj = new entIndIndustriagps();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entIndIndustriagps que cumple con los filtros de los parametros
		/// </returns>
		public entIndIndustriagps ObtenerObjeto(entIndIndustriagps.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entIndIndustriagps que cumple con los filtros de los parametros
		/// </returns>
		public entIndIndustriagps ObtenerObjeto(entIndIndustriagps.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un Business Object del Tipo entIndIndustriagps a partir de su llave promaria
		/// </summary>
		/// <returns>
		/// 	Objeto del Tipo entIndIndustriagps
		/// </returns>
		public entIndIndustriagps ObtenerObjeto(int intgestion, int intmes, int intid_gps, String Stringusucre, ref cTrans localTrans )
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(entIndIndustriagps.Fields.gestion.ToString());
			arrColumnasWhere.Add(entIndIndustriagps.Fields.mes.ToString());
			arrColumnasWhere.Add(entIndIndustriagps.Fields.id_gps.ToString());
			arrColumnasWhere.Add(entIndIndustriagps.Fields.usucre.ToString());
		
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(intgestion);
			arrValoresWhere.Add(intmes);
			arrValoresWhere.Add(intid_gps);
			arrValoresWhere.Add("'" + Stringusucre + "'");
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industriagps
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entIndIndustriagps que cumple con los filtros de los parametros
		/// </returns>
		public entIndIndustriagps ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industriagps
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entIndIndustriagps que cumple con los filtros de los parametros
		/// </returns>
		public entIndIndustriagps ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustriagps.Fields.gestion.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.mes.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_gps.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_industria.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.latitud.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.longitud.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.hora_control.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.apiestado.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.apitransaccion.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usucre.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.feccre.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usumod.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.fecmod.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.codigo_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usu_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.fec_manzana.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entIndIndustriagps.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count == 1)
				{
					entIndIndustriagps obj = new entIndIndustriagps();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industriagps
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entIndIndustriagps que cumple con los filtros de los parametros
		/// </returns>
		public entIndIndustriagps ObtenerObjeto(entIndIndustriagps.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industriagps
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entIndIndustriagps que cumple con los filtros de los parametros
		/// </returns>
		public entIndIndustriagps ObtenerObjeto(entIndIndustriagps.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entIndIndustriagps> ObtenerLista()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entIndIndustriagps> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entIndIndustriagps> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustriagps.Fields.gestion.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.mes.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_gps.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_industria.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.latitud.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.longitud.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.hora_control.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.apiestado.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.apitransaccion.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usucre.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.feccre.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usumod.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.fecmod.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.codigo_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usu_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.fec_manzana.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entIndIndustriagps.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<entIndIndustriagps>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entIndIndustriagps> ObtenerLista(entIndIndustriagps.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entIndIndustriagps> ObtenerLista(entIndIndustriagps.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industriagps
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entIndIndustriagps> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industriagps
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entIndIndustriagps> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustriagps.Fields.gestion.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.mes.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_gps.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_industria.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.latitud.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.longitud.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.hora_control.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.apiestado.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.apitransaccion.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usucre.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.feccre.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usumod.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.fecmod.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.codigo_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usu_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.fec_manzana.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entIndIndustriagps.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<entIndIndustriagps>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industriagps
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entIndIndustriagps> ObtenerLista(entIndIndustriagps.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industriagps
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entIndIndustriagps> ObtenerLista(entIndIndustriagps.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entIndIndustriagps> ObtenerListaDesdeVista(String strVista)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entIndIndustriagps> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entIndIndustriagps> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<entIndIndustriagps>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entIndIndustriagps> ObtenerListaDesdeVista(String strVista, entIndIndustriagps.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entIndIndustriagps> ObtenerListaDesdeVista(String strVista, entIndIndustriagps.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industriagps
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entIndIndustriagps> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industriagps
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entIndIndustriagps> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<entIndIndustriagps>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industriagps
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entIndIndustriagps> ObtenerListaDesdeVista(String strVista, entIndIndustriagps.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industriagps
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entIndIndustriagps> ObtenerListaDesdeVista(String strVista, entIndIndustriagps.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entIndIndustriagps> ObtenerCola()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entIndIndustriagps> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entIndIndustriagps> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustriagps.Fields.gestion.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.mes.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_gps.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_industria.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.latitud.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.longitud.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.hora_control.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.apiestado.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.apitransaccion.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usucre.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.feccre.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usumod.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.fecmod.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.codigo_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usu_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.fec_manzana.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entIndIndustriagps.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearCola(table);
				}
				else
					return new Queue<entIndIndustriagps>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entIndIndustriagps> ObtenerCola(entIndIndustriagps.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entIndIndustriagps> ObtenerCola(entIndIndustriagps.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industriagps
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entIndIndustriagps> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industriagps
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entIndIndustriagps> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustriagps.Fields.gestion.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.mes.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_gps.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_industria.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.latitud.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.longitud.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.hora_control.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.apiestado.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.apitransaccion.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usucre.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.feccre.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usumod.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.fecmod.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.codigo_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usu_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.fec_manzana.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entIndIndustriagps.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearCola(table);
				}
				else
					return new Queue<entIndIndustriagps>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industriagps
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entIndIndustriagps> ObtenerCola(entIndIndustriagps.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industriagps
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entIndIndustriagps> ObtenerCola(entIndIndustriagps.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entIndIndustriagps> ObtenerPila()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entIndIndustriagps> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entIndIndustriagps> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustriagps.Fields.gestion.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.mes.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_gps.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_industria.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.latitud.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.longitud.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.hora_control.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.apiestado.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.apitransaccion.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usucre.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.feccre.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usumod.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.fecmod.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.codigo_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usu_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.fec_manzana.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entIndIndustriagps.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearPila(table);
				}
				else
					return new Stack<entIndIndustriagps>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entIndIndustriagps> ObtenerPila(entIndIndustriagps.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entIndIndustriagps> ObtenerPila(entIndIndustriagps.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industriagps
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entIndIndustriagps> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industriagps
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entIndIndustriagps> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustriagps.Fields.gestion.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.mes.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_gps.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_industria.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.latitud.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.longitud.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.hora_control.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.apiestado.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.apitransaccion.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usucre.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.feccre.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usumod.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.fecmod.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.codigo_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usu_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.fec_manzana.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entIndIndustriagps.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearPila(table);
				}
				else
					return new Stack<entIndIndustriagps>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industriagps
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entIndIndustriagps> ObtenerPila(entIndIndustriagps.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industriagps
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entIndIndustriagps> ObtenerPila(entIndIndustriagps.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entIndIndustriagps> ObtenerDiccionario()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entIndIndustriagps> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entIndIndustriagps> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustriagps.Fields.gestion.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.mes.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_gps.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_industria.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.latitud.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.longitud.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.hora_control.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.apiestado.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.apitransaccion.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usucre.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.feccre.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usumod.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.fecmod.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.codigo_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usu_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.fec_manzana.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entIndIndustriagps.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearDiccionario(table);
				}
				else
					return new Dictionary<string, entIndIndustriagps>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entIndIndustriagps> ObtenerDiccionario(entIndIndustriagps.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entIndIndustriagps> ObtenerDiccionario(entIndIndustriagps.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industriagps
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entIndIndustriagps> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industriagps
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entIndIndustriagps> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustriagps.Fields.gestion.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.mes.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_gps.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_industria.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.latitud.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.longitud.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.hora_control.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.apiestado.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.apitransaccion.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usucre.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.feccre.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usumod.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.fecmod.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.codigo_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usu_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.fec_manzana.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entIndIndustriagps.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearDiccionario(table);
				}
				else
					return new Dictionary<string, entIndIndustriagps>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industriagps
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entIndIndustriagps> ObtenerDiccionario(entIndIndustriagps.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entIndIndustriagps a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industriagps
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entIndIndustriagps> ObtenerDiccionario(entIndIndustriagps.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	 Funcion que obtiene la llave primaria unica de la tabla ind_industriagps a partir de una cadena
		/// </summary>
		/// <param name="args" type="string[]">
		///     <para>
		/// 		 Cadena desde la que se construye el identificador unico de la tabla ind_industriagps
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Identificador unico de la tabla ind_industriagps
		/// </returns>
		public string CreatePK(string[] args)
		{
			return args[0];
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla ind_industriagps a partir de una clase del tipo Eind_industriagps
		/// </summary>
		/// <param name="obj" type="Entidades.entIndIndustriagps">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla ind_industriagpsind_industriagps
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionind_industriagps
		/// </returns>
		public bool Insert(entIndIndustriagps obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustriagps.Fields.gestion.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.mes.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_gps.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_industria.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.latitud.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.longitud.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.hora_control.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.apiestado.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usucre.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.feccre.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.codigo_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usu_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.fec_manzana.ToString());
				
				ArrayList arrValores = new ArrayList();
				arrValores.Add(obj.gestion);
				arrValores.Add(obj.mes);
				arrValores.Add(obj.id_gps);
				arrValores.Add(obj.id_industria);
				arrValores.Add(obj.latitud);
				arrValores.Add(obj.longitud);
				arrValores.Add(obj.hora_control == null ? null : "'" + Convert.ToDateTime(obj.hora_control).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.id_manzana == null ? null : "'" + obj.id_manzana + "'");
				arrValores.Add(obj.codigo_manzana == null ? null : "'" + obj.codigo_manzana + "'");
				arrValores.Add(obj.usu_manzana == null ? null : "'" + obj.usu_manzana + "'");
				arrValores.Add(obj.fec_manzana == null ? null : "'" + Convert.ToDateTime(obj.fec_manzana).ToString(cParametros.parFormatoFechaHora) + "'");

			
				cConn local = new cConn();
				return local.insertBD(entIndIndustriagps.strNombreTabla, arrColumnas, arrValores);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla ind_industriagps a partir de una clase del tipo Eind_industriagps
		/// </summary>
		/// <param name="obj" type="Entidades.entIndIndustriagps">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla ind_industriagpsind_industriagps
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industriagps
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionind_industriagps
		/// </returns>
		public bool Insert(entIndIndustriagps obj, ref cTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustriagps.Fields.gestion.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.mes.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_gps.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_industria.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.latitud.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.longitud.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.hora_control.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.apiestado.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usucre.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.feccre.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.codigo_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usu_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.fec_manzana.ToString());
				
				ArrayList arrValores = new ArrayList();
				arrValores.Add(obj.gestion);
				arrValores.Add(obj.mes);
				arrValores.Add(obj.id_gps);
				arrValores.Add(obj.id_industria);
				arrValores.Add(obj.latitud);
				arrValores.Add(obj.longitud);
				arrValores.Add(obj.hora_control == null ? null : "'" + Convert.ToDateTime(obj.hora_control).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.id_manzana == null ? null : "'" + obj.id_manzana + "'");
				arrValores.Add(obj.codigo_manzana == null ? null : "'" + obj.codigo_manzana + "'");
				arrValores.Add(obj.usu_manzana == null ? null : "'" + obj.usu_manzana + "'");
				arrValores.Add(obj.fec_manzana == null ? null : "'" + Convert.ToDateTime(obj.fec_manzana).ToString(cParametros.parFormatoFechaHora) + "'");

			
				cConn local = new cConn();
				return local.insertBD(entIndIndustriagps.strNombreTabla, arrColumnas, arrValores, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla ind_industriagps a partir de una clase del tipo Eind_industriagps
		/// </summary>
		/// <param name="obj" type="Entidades.entIndIndustriagps">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla ind_industriagpsind_industriagps
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionind_industriagps
		/// </returns>
		public bool InsertIdentity(entIndIndustriagps obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustriagps.Fields.gestion.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.mes.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_gps.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_industria.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.latitud.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.longitud.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.hora_control.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.apiestado.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usucre.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.feccre.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.codigo_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usu_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.fec_manzana.ToString());
				
				ArrayList arrValores = new ArrayList();
				arrValores.Add(obj.gestion);
				arrValores.Add(obj.mes);
				arrValores.Add(obj.id_gps);
				arrValores.Add(obj.id_industria);
				arrValores.Add(obj.latitud);
				arrValores.Add(obj.longitud);
				arrValores.Add(obj.hora_control == null ? null : "'" + Convert.ToDateTime(obj.hora_control).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.id_manzana == null ? null : "'" + obj.id_manzana + "'");
				arrValores.Add(obj.codigo_manzana == null ? null : "'" + obj.codigo_manzana + "'");
				arrValores.Add(obj.usu_manzana == null ? null : "'" + obj.usu_manzana + "'");
				arrValores.Add(obj.fec_manzana == null ? null : "'" + Convert.ToDateTime(obj.fec_manzana).ToString(cParametros.parFormatoFechaHora) + "'");

			
				cConn local = new cConn();
				int intIdentidad = -1;
				bool res = local.insertBD(entIndIndustriagps.strNombreTabla, arrColumnas, arrValores, ref intIdentidad);
				obj.gestion = intIdentidad;
				return res;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla ind_industriagps a partir de una clase del tipo Eind_industriagps
		/// </summary>
		/// <param name="obj" type="Entidades.entIndIndustriagps">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla ind_industriagpsind_industriagps
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industriagps
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionind_industriagps
		/// </returns>
		public bool InsertIdentity(entIndIndustriagps obj, ref cTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustriagps.Fields.gestion.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.mes.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_gps.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_industria.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.latitud.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.longitud.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.hora_control.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.apiestado.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usucre.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.feccre.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.codigo_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usu_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.fec_manzana.ToString());
				
				ArrayList arrValores = new ArrayList();
				arrValores.Add(obj.gestion);
				arrValores.Add(obj.mes);
				arrValores.Add(obj.id_gps);
				arrValores.Add(obj.id_industria);
				arrValores.Add(obj.latitud);
				arrValores.Add(obj.longitud);
				arrValores.Add(obj.hora_control == null ? null : "'" + Convert.ToDateTime(obj.hora_control).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.id_manzana == null ? null : "'" + obj.id_manzana + "'");
				arrValores.Add(obj.codigo_manzana == null ? null : "'" + obj.codigo_manzana + "'");
				arrValores.Add(obj.usu_manzana == null ? null : "'" + obj.usu_manzana + "'");
				arrValores.Add(obj.fec_manzana == null ? null : "'" + Convert.ToDateTime(obj.fec_manzana).ToString(cParametros.parFormatoFechaHora) + "'");

			
				cConn local = new cConn();
				int intIdentidad = -1;
				bool res = local.insertBD(entIndIndustriagps.strNombreTabla, arrColumnas, arrValores, ref intIdentidad, ref localTrans);
				obj.gestion = intIdentidad;
				return res;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla ind_industriagps a partir de una clase del tipo Eind_industriagps
		/// </summary>
		/// <param name="obj" type="Entidades.entIndIndustriagps">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla ind_industriagps
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionind_industriagps
		/// </returns>
		public int Update(entIndIndustriagps obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustriagps.Fields.id_industria.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.latitud.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.longitud.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.hora_control.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.apiestado.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.apitransaccion.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.feccre.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usumod.ToString());
			
				ArrayList arrValores = new ArrayList();
				arrValores.Add(obj.id_industria);
				arrValores.Add(obj.latitud);
				arrValores.Add(obj.longitud);
				arrValores.Add(obj.hora_control == null ? null : "'" + Convert.ToDateTime(obj.hora_control).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
				arrValores.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.id_manzana == null ? null : "'" + obj.id_manzana + "'");
				arrValores.Add(obj.codigo_manzana == null ? null : "'" + obj.codigo_manzana + "'");
				arrValores.Add(obj.usu_manzana == null ? null : "'" + obj.usu_manzana + "'");
				arrValores.Add(obj.fec_manzana == null ? null : "'" + Convert.ToDateTime(obj.fec_manzana).ToString(cParametros.parFormatoFechaHora) + "'");

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(entIndIndustriagps.Fields.gestion.ToString());
				arrColumnasWhere.Add(entIndIndustriagps.Fields.mes.ToString());
				arrColumnasWhere.Add(entIndIndustriagps.Fields.id_gps.ToString());
				arrColumnasWhere.Add(entIndIndustriagps.Fields.usucre.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.gestion);
				arrValoresWhere.Add(obj.mes);
				arrValoresWhere.Add(obj.id_gps);
				arrValoresWhere.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");

			
				cConn local = new cConn();
				return local.updateBD(entIndIndustriagps.strNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla ind_industriagps a partir de una clase del tipo eind_industriagps
		/// </summary>
		/// <param name="obj" type="Entidades.entIndIndustriagps">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla ind_industriagps
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industriagps
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacion
		/// </returns>
		public int Update(entIndIndustriagps obj, ref cTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustriagps.Fields.id_industria.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.latitud.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.longitud.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.hora_control.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.apiestado.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.apitransaccion.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.feccre.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usumod.ToString());
			
				ArrayList arrValores = new ArrayList();
				arrValores.Add(obj.id_industria);
				arrValores.Add(obj.latitud);
				arrValores.Add(obj.longitud);
				arrValores.Add(obj.hora_control == null ? null : "'" + Convert.ToDateTime(obj.hora_control).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
				arrValores.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.id_manzana == null ? null : "'" + obj.id_manzana + "'");
				arrValores.Add(obj.codigo_manzana == null ? null : "'" + obj.codigo_manzana + "'");
				arrValores.Add(obj.usu_manzana == null ? null : "'" + obj.usu_manzana + "'");
				arrValores.Add(obj.fec_manzana == null ? null : "'" + Convert.ToDateTime(obj.fec_manzana).ToString(cParametros.parFormatoFechaHora) + "'");

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(entIndIndustriagps.Fields.gestion.ToString());
				arrColumnasWhere.Add(entIndIndustriagps.Fields.mes.ToString());
				arrColumnasWhere.Add(entIndIndustriagps.Fields.id_gps.ToString());
				arrColumnasWhere.Add(entIndIndustriagps.Fields.usucre.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.gestion);
				arrValoresWhere.Add(obj.mes);
				arrValoresWhere.Add(obj.id_gps);
				arrValoresWhere.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");

			
				cConn local = new cConn();
				return local.updateBD(entIndIndustriagps.strNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla ind_industriagps a partir de una clase del tipo entIndIndustriagps y su respectiva PK
		/// </summary>
		/// <param name="obj" type="Entidades.entIndIndustriagps">
		///     <para>
		/// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla ind_industriagps
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionind_industriagps
		/// </returns>
		public int Delete(entIndIndustriagps obj)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(entIndIndustriagps.Fields.gestion.ToString());
				arrColumnasWhere.Add(entIndIndustriagps.Fields.mes.ToString());
				arrColumnasWhere.Add(entIndIndustriagps.Fields.id_gps.ToString());
				arrColumnasWhere.Add(entIndIndustriagps.Fields.usucre.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.gestion);
				arrValoresWhere.Add(obj.mes);
				arrValoresWhere.Add(obj.id_gps);
				arrValoresWhere.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");

			
				cConn local = new cConn();
				return local.deleteBD(entIndIndustriagps.strNombreTabla, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla ind_industriagps a partir de una clase del tipo entIndIndustriagps y su PK
		/// </summary>
		/// <param name="obj" type="Entidades.eind_industriagps">
		///     <para>
		/// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla ind_industriagps
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industriagps
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacionind_industriagps
		/// </returns>
		public int Delete(entIndIndustriagps obj, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(entIndIndustriagps.Fields.gestion.ToString());
				arrColumnasWhere.Add(entIndIndustriagps.Fields.mes.ToString());
				arrColumnasWhere.Add(entIndIndustriagps.Fields.id_gps.ToString());
				arrColumnasWhere.Add(entIndIndustriagps.Fields.usucre.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.gestion);
				arrValoresWhere.Add(obj.mes);
				arrValoresWhere.Add(obj.id_gps);
				arrValoresWhere.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");

			
				cConn local = new cConn();
				return local.deleteBD(entIndIndustriagps.strNombreTabla, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla ind_industriagps a partir de una clase del tipo eind_industriagps
		/// </summary>
		/// <param name="arrColumnasWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Valores para cada una de las columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionind_industriagps
		/// </returns>
		public int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				cConn local = new cConn();
				return local.deleteBD("ind_industriagps", arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla ind_industriagps a partir de una clase del tipo eind_industriagps
		/// </summary>
		/// <param name="arrColumnasWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Valores para cada una de las columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ind_industriagps
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacionind_industriagps
		/// </returns>
		public int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			try
			{
				cConn local = new cConn();
				return local.deleteBD(entIndIndustriagps.strNombreTabla, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		

		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla ind_industriagps
		/// </summary>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ind_industriagps
		/// </returns>
		public DataTable NuevoDataTable()
		{
			try
			{
				DataTable table = new DataTable ();
				DataColumn dc;
				dc = new DataColumn(entIndIndustriagps.Fields.gestion.ToString(),typeof(entIndIndustriagps).GetProperty(entIndIndustriagps.Fields.gestion.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entIndIndustriagps.Fields.mes.ToString(),typeof(entIndIndustriagps).GetProperty(entIndIndustriagps.Fields.mes.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entIndIndustriagps.Fields.id_gps.ToString(),typeof(entIndIndustriagps).GetProperty(entIndIndustriagps.Fields.id_gps.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entIndIndustriagps.Fields.id_industria.ToString(),typeof(entIndIndustriagps).GetProperty(entIndIndustriagps.Fields.id_industria.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entIndIndustriagps.Fields.latitud.ToString(),typeof(entIndIndustriagps).GetProperty(entIndIndustriagps.Fields.latitud.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entIndIndustriagps.Fields.longitud.ToString(),typeof(entIndIndustriagps).GetProperty(entIndIndustriagps.Fields.longitud.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entIndIndustriagps.Fields.hora_control.ToString(),typeof(entIndIndustriagps).GetProperty(entIndIndustriagps.Fields.hora_control.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entIndIndustriagps.Fields.apiestado.ToString(),typeof(entIndIndustriagps).GetProperty(entIndIndustriagps.Fields.apiestado.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entIndIndustriagps.Fields.apitransaccion.ToString(),typeof(entIndIndustriagps).GetProperty(entIndIndustriagps.Fields.apitransaccion.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entIndIndustriagps.Fields.usucre.ToString(),typeof(entIndIndustriagps).GetProperty(entIndIndustriagps.Fields.usucre.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entIndIndustriagps.Fields.feccre.ToString(),typeof(entIndIndustriagps).GetProperty(entIndIndustriagps.Fields.feccre.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entIndIndustriagps.Fields.usumod.ToString(),typeof(entIndIndustriagps).GetProperty(entIndIndustriagps.Fields.usumod.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entIndIndustriagps.Fields.fecmod.ToString(),typeof(entIndIndustriagps).GetProperty(entIndIndustriagps.Fields.fecmod.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entIndIndustriagps.Fields.id_manzana.ToString(),typeof(entIndIndustriagps).GetProperty(entIndIndustriagps.Fields.id_manzana.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entIndIndustriagps.Fields.codigo_manzana.ToString(),typeof(entIndIndustriagps).GetProperty(entIndIndustriagps.Fields.codigo_manzana.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entIndIndustriagps.Fields.usu_manzana.ToString(),typeof(entIndIndustriagps).GetProperty(entIndIndustriagps.Fields.usu_manzana.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entIndIndustriagps.Fields.fec_manzana.ToString(),typeof(entIndIndustriagps).GetProperty(entIndIndustriagps.Fields.fec_manzana.ToString()).PropertyType);
				table.Columns.Add(dc);

				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que genera un DataTable con determinadas columnas de una ind_industriagps
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ind_industriagps
		/// </returns>
		public DataTable NuevoDataTable(ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'2'");
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entIndIndustriagps.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla ind_industriagps
		/// </summary>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ind_industriagps
		/// </returns>
		public DataTable CargarDataTable()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return CargarDataTable(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla y n condicion WHERE ind_industriagps
		/// </summary>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ind_industriagps
		/// </returns>
		public DataTable CargarDataTable(String strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return CargarDataTable(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla ind_industriagps
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ind_industriagps
		/// </returns>
		public DataTable CargarDataTable(ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				return CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla ind_industriagps
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ind_industriagps
		/// </returns>
		public DataTable CargarDataTable(ArrayList arrColumnas, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				return CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla ind_industriagps
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ind_industriagps
		/// </returns>
		public DataTable CargarDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla ind_industriagps
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ind_industriagps
		/// </returns>
		public DataTable CargarDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return CargarDataTable(arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla ind_industriagps
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ind_industriagps
		/// </returns>
		public DataTable CargarDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entIndIndustriagps.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla ind_industriagps
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ind_industriagps
		/// </returns>
		public DataTable CargarDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustriagps.Fields.gestion.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.mes.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_gps.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_industria.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.latitud.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.longitud.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.hora_control.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.apiestado.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.apitransaccion.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usucre.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.feccre.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usumod.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.fecmod.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.codigo_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usu_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.fec_manzana.ToString());
				
				return CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="searchField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable CargarDataTable(entIndIndustriagps.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return CargarDataTable(arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="searchField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable CargarDataTable(entIndIndustriagps.Fields searchField, object searchValue, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return CargarDataTable(arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable CargarDataTable(ArrayList arrColumnas, entIndIndustriagps.Fields searchField, object searchValue, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable CargarDataTable(ArrayList arrColumnas, entIndIndustriagps.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla ind_industriagps
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ind_industriagps
		/// </returns>
		public DataTable CargarDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustriagps.Fields.gestion.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.mes.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_gps.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_industria.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.latitud.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.longitud.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.hora_control.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.apiestado.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.apitransaccion.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usucre.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.feccre.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usumod.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.fecmod.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.codigo_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usu_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.fec_manzana.ToString());
				return CargarDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla ind_industriagps
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ind_industriagps
		/// </returns>
		public DataTable CargarDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return CargarDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla ind_industriagps
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ind_industriagps
		/// </returns>
		public DataTable CargarDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DataTable table = local.cargarDataTableOr(cParametros.schema + entIndIndustriagps.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ind_industriagps
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla ind_industriagpsind_industriagps
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ind_industriagps
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla ind_industriagpsind_industriagps
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ind_industriagps
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla ind_industriagpsind_industriagps
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ind_industriagps
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla ind_industriagpsind_industriagps
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustriagps.Fields.gestion.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.mes.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ind_industriagps
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ind_industriagpsind_industriagps
		///     </para>
		/// </param>
		/// <param name="valueField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entIndIndustriagps.Fields valueField, entIndIndustriagps.Fields textField)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ind_industriagps
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ind_industriagpsind_industriagps
		///     </para>
		/// </param>
		/// <param name="valueField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entIndIndustriagps.Fields valueField, entIndIndustriagps.Fields textField, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ind_industriagps
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ind_industriagpsind_industriagps
		///     </para>
		/// </param>
		/// <param name="valueField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entIndIndustriagps.Fields valueField, String textField)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ind_industriagps
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ind_industriagpsind_industriagps
		///     </para>
		/// </param>
		/// <param name="valueField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entIndIndustriagps.Fields valueField, String textField, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ind_industriagps
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ind_industriagpsind_industriagps
		///     </para>
		/// </param>
		/// <param name="valueField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entIndIndustriagps.Fields valueField, entIndIndustriagps.Fields textField, entIndIndustriagps.Fields searchField, object searchValue)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField.ToString(), searchField, searchValue);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ind_industriagps
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ind_industriagpsind_industriagps
		///     </para>
		/// </param>
		/// <param name="valueField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entIndIndustriagps.Fields valueField, String textField, entIndIndustriagps.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ind_industriagps
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ind_industriagpsind_industriagps
		///     </para>
		/// </param>
		/// <param name="valueField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entIndIndustriagps.Fields valueField, entIndIndustriagps.Fields textField, entIndIndustriagps.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ind_industriagps
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ind_industriagpsind_industriagps
		///     </para>
		/// </param>
		/// <param name="valueField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entIndIndustriagps.Fields valueField, String textField, entIndIndustriagps.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ind_industriagps
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ind_industriagpsind_industriagps
		///     </para>
		/// </param>
		/// <param name="valueField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entIndIndustriagps.Fields valueField, entIndIndustriagps.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ind_industriagps
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ind_industriagpsind_industriagps
		///     </para>
		/// </param>
		/// <param name="valueField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entIndIndustriagps.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ind_industriagps
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ind_industriagpsind_industriagps
		///     </para>
		/// </param>
		/// <param name="valueField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entIndIndustriagps.Fields valueField, entIndIndustriagps.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ind_industriagps
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ind_industriagpsind_industriagps
		///     </para>
		/// </param>
		/// <param name="valueField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entIndIndustriagps.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ind_industriagps
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ind_industriagpsind_industriagps
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entIndIndustriagps.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Columns.Count > 0)
				{
					cmb.ValueMember = table.Columns[0].ColumnName;
					cmb.DisplayMember = table.Columns[1].ColumnName;
					cmb.DataSource = table;

				}
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla ind_industriagps
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustriagps.Fields.gestion.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.mes.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_gps.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_industria.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.latitud.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.longitud.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.hora_control.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.apiestado.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.apitransaccion.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usucre.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.feccre.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usumod.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.fecmod.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.codigo_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usu_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.fec_manzana.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla ind_industriagps
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		  Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustriagps.Fields.gestion.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.mes.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_gps.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_industria.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.latitud.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.longitud.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.hora_control.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.apiestado.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.apitransaccion.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usucre.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.feccre.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usumod.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.fecmod.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.codigo_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usu_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.fec_manzana.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla ind_industriagps
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla ind_industriagps
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla ind_industriagps
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustriagps.Fields.gestion.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.mes.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_gps.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_industria.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.latitud.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.longitud.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.hora_control.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.apiestado.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.apitransaccion.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usucre.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.feccre.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usumod.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.fecmod.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.codigo_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usu_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.fec_manzana.ToString());
			
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla ind_industriagps
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustriagps.Fields.gestion.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.mes.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_gps.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_industria.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.latitud.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.longitud.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.hora_control.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.apiestado.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.apitransaccion.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usucre.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.feccre.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usumod.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.fecmod.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.codigo_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usu_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.fec_manzana.ToString());
			
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla ind_industriagps
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla ind_industriagps
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DbDataReader dsReader = local.cargarDataReaderAnd(cParametros.schema + entIndIndustriagps.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				dtg.DataSource = dsReader;

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla ind_industriagps
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, entIndIndustriagps.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustriagps.Fields.gestion.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.mes.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_gps.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_industria.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.latitud.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.longitud.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.hora_control.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.apiestado.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.apitransaccion.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usucre.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.feccre.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usumod.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.fecmod.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.codigo_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usu_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.fec_manzana.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla ind_industriagps
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, entIndIndustriagps.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entIndIndustriagps.Fields.gestion.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.mes.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_gps.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_industria.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.latitud.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.longitud.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.hora_control.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.apiestado.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.apitransaccion.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usucre.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.feccre.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usumod.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.fecmod.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.id_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.codigo_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.usu_manzana.ToString());
				arrColumnas.Add(entIndIndustriagps.Fields.fec_manzana.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla ind_industriagps
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entIndIndustriagps.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla ind_industriagps
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entIndIndustriagps.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla ind_industriagps
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarDataGridOr(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla ind_industriagps
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DbDataReader dsReader = local.cargarDataReaderOr(cParametros.schema + entIndIndustriagps.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				dtg.DataSource = dsReader;

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entIndIndustriagps que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesCount(entIndIndustriagps.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entIndIndustriagps que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesCount(entIndIndustriagps.Fields refField, entIndIndustriagps.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entIndIndustriagps que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesCount(entIndIndustriagps.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("count(" + refField + ")");
				DataTable dtTemp = CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return dtTemp.Rows[0][0];
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entIndIndustriagps que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesMin(entIndIndustriagps.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entIndIndustriagps que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesMin(entIndIndustriagps.Fields refField, entIndIndustriagps.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entIndIndustriagps que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesMin(entIndIndustriagps.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("min(" + refField + ")");
				DataTable dtTemp = CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return dtTemp.Rows[0][0];
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entIndIndustriagps que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesMax(entIndIndustriagps.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entIndIndustriagps que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesMax(entIndIndustriagps.Fields refField, entIndIndustriagps.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entIndIndustriagps que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesMax(entIndIndustriagps.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("max(" + refField + ")");
				DataTable dtTemp = CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return dtTemp.Rows[0][0];
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entIndIndustriagps que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesSum(entIndIndustriagps.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entIndIndustriagps que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesSum(entIndIndustriagps.Fields refField, entIndIndustriagps.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entIndIndustriagps que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesSum(entIndIndustriagps.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("sum(" + refField + ")");
				DataTable dtTemp = CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return dtTemp.Rows[0][0];
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entIndIndustriagps que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesAvg(entIndIndustriagps.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entIndIndustriagps que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesAvg(entIndIndustriagps.Fields refField, entIndIndustriagps.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entIndIndustriagps que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesAvg(entIndIndustriagps.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("avg(" + refField + ")");
				DataTable dtTemp = CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return dtTemp.Rows[0][0];
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion

		/// <summary>
		/// 	 Funcion que devuelve un objeto a partir de un DataRow
		/// </summary>
		/// <param name="row" type="System.Data.DataRow">
		///     <para>
		/// 		 DataRow con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Objeto ind_industriagps
		/// </returns>
		internal entIndIndustriagps crearObjeto(DataRow row)
		{
			entIndIndustriagps obj = new entIndIndustriagps();
			obj.gestion = GetColumnType(row[entIndIndustriagps.Fields.gestion.ToString()], entIndIndustriagps.Fields.gestion);
			obj.mes = GetColumnType(row[entIndIndustriagps.Fields.mes.ToString()], entIndIndustriagps.Fields.mes);
			obj.id_gps = GetColumnType(row[entIndIndustriagps.Fields.id_gps.ToString()], entIndIndustriagps.Fields.id_gps);
			obj.id_industria = GetColumnType(row[entIndIndustriagps.Fields.id_industria.ToString()], entIndIndustriagps.Fields.id_industria);
			obj.latitud = GetColumnType(row[entIndIndustriagps.Fields.latitud.ToString()], entIndIndustriagps.Fields.latitud);
			obj.longitud = GetColumnType(row[entIndIndustriagps.Fields.longitud.ToString()], entIndIndustriagps.Fields.longitud);
			obj.hora_control = GetColumnType(row[entIndIndustriagps.Fields.hora_control.ToString()], entIndIndustriagps.Fields.hora_control);
			obj.apiestado = GetColumnType(row[entIndIndustriagps.Fields.apiestado.ToString()], entIndIndustriagps.Fields.apiestado);
			obj.apitransaccion = GetColumnType(row[entIndIndustriagps.Fields.apitransaccion.ToString()], entIndIndustriagps.Fields.apitransaccion);
			obj.usucre = GetColumnType(row[entIndIndustriagps.Fields.usucre.ToString()], entIndIndustriagps.Fields.usucre);
			obj.feccre = GetColumnType(row[entIndIndustriagps.Fields.feccre.ToString()], entIndIndustriagps.Fields.feccre);
			obj.usumod = GetColumnType(row[entIndIndustriagps.Fields.usumod.ToString()], entIndIndustriagps.Fields.usumod);
			obj.fecmod = GetColumnType(row[entIndIndustriagps.Fields.fecmod.ToString()], entIndIndustriagps.Fields.fecmod);
			obj.id_manzana = GetColumnType(row[entIndIndustriagps.Fields.id_manzana.ToString()], entIndIndustriagps.Fields.id_manzana);
			obj.codigo_manzana = GetColumnType(row[entIndIndustriagps.Fields.codigo_manzana.ToString()], entIndIndustriagps.Fields.codigo_manzana);
			obj.usu_manzana = GetColumnType(row[entIndIndustriagps.Fields.usu_manzana.ToString()], entIndIndustriagps.Fields.usu_manzana);
			obj.fec_manzana = GetColumnType(row[entIndIndustriagps.Fields.fec_manzana.ToString()], entIndIndustriagps.Fields.fec_manzana);
			return obj;
		}

		/// <summary>
		/// 	 Funcion que devuelve un objeto a partir de un DataRow
		/// </summary>
		/// <param name="row" type="System.Data.DataRow">
		///     <para>
		/// 		 DataRow con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Objeto ind_industriagps
		/// </returns>
		internal entIndIndustriagps crearObjetoRevisado(DataRow row)
		{
			entIndIndustriagps obj = new entIndIndustriagps();
			if (row.Table.Columns.Contains(entIndIndustriagps.Fields.gestion.ToString()))
				obj.gestion = GetColumnType(row[entIndIndustriagps.Fields.gestion.ToString()], entIndIndustriagps.Fields.gestion);
			if (row.Table.Columns.Contains(entIndIndustriagps.Fields.mes.ToString()))
				obj.mes = GetColumnType(row[entIndIndustriagps.Fields.mes.ToString()], entIndIndustriagps.Fields.mes);
			if (row.Table.Columns.Contains(entIndIndustriagps.Fields.id_gps.ToString()))
				obj.id_gps = GetColumnType(row[entIndIndustriagps.Fields.id_gps.ToString()], entIndIndustriagps.Fields.id_gps);
			if (row.Table.Columns.Contains(entIndIndustriagps.Fields.id_industria.ToString()))
				obj.id_industria = GetColumnType(row[entIndIndustriagps.Fields.id_industria.ToString()], entIndIndustriagps.Fields.id_industria);
			if (row.Table.Columns.Contains(entIndIndustriagps.Fields.latitud.ToString()))
				obj.latitud = GetColumnType(row[entIndIndustriagps.Fields.latitud.ToString()], entIndIndustriagps.Fields.latitud);
			if (row.Table.Columns.Contains(entIndIndustriagps.Fields.longitud.ToString()))
				obj.longitud = GetColumnType(row[entIndIndustriagps.Fields.longitud.ToString()], entIndIndustriagps.Fields.longitud);
			if (row.Table.Columns.Contains(entIndIndustriagps.Fields.hora_control.ToString()))
				obj.hora_control = GetColumnType(row[entIndIndustriagps.Fields.hora_control.ToString()], entIndIndustriagps.Fields.hora_control);
			if (row.Table.Columns.Contains(entIndIndustriagps.Fields.apiestado.ToString()))
				obj.apiestado = GetColumnType(row[entIndIndustriagps.Fields.apiestado.ToString()], entIndIndustriagps.Fields.apiestado);
			if (row.Table.Columns.Contains(entIndIndustriagps.Fields.apitransaccion.ToString()))
				obj.apitransaccion = GetColumnType(row[entIndIndustriagps.Fields.apitransaccion.ToString()], entIndIndustriagps.Fields.apitransaccion);
			if (row.Table.Columns.Contains(entIndIndustriagps.Fields.usucre.ToString()))
				obj.usucre = GetColumnType(row[entIndIndustriagps.Fields.usucre.ToString()], entIndIndustriagps.Fields.usucre);
			if (row.Table.Columns.Contains(entIndIndustriagps.Fields.feccre.ToString()))
				obj.feccre = GetColumnType(row[entIndIndustriagps.Fields.feccre.ToString()], entIndIndustriagps.Fields.feccre);
			if (row.Table.Columns.Contains(entIndIndustriagps.Fields.usumod.ToString()))
				obj.usumod = GetColumnType(row[entIndIndustriagps.Fields.usumod.ToString()], entIndIndustriagps.Fields.usumod);
			if (row.Table.Columns.Contains(entIndIndustriagps.Fields.fecmod.ToString()))
				obj.fecmod = GetColumnType(row[entIndIndustriagps.Fields.fecmod.ToString()], entIndIndustriagps.Fields.fecmod);
			if (row.Table.Columns.Contains(entIndIndustriagps.Fields.id_manzana.ToString()))
				obj.id_manzana = GetColumnType(row[entIndIndustriagps.Fields.id_manzana.ToString()], entIndIndustriagps.Fields.id_manzana);
			if (row.Table.Columns.Contains(entIndIndustriagps.Fields.codigo_manzana.ToString()))
				obj.codigo_manzana = GetColumnType(row[entIndIndustriagps.Fields.codigo_manzana.ToString()], entIndIndustriagps.Fields.codigo_manzana);
			if (row.Table.Columns.Contains(entIndIndustriagps.Fields.usu_manzana.ToString()))
				obj.usu_manzana = GetColumnType(row[entIndIndustriagps.Fields.usu_manzana.ToString()], entIndIndustriagps.Fields.usu_manzana);
			if (row.Table.Columns.Contains(entIndIndustriagps.Fields.fec_manzana.ToString()))
				obj.fec_manzana = GetColumnType(row[entIndIndustriagps.Fields.fec_manzana.ToString()], entIndIndustriagps.Fields.fec_manzana);
			return obj;
		}

		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtind_industriagps" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Lista de Objetos ind_industriagps
		/// </returns>
		internal List<entIndIndustriagps> crearLista(DataTable dtind_industriagps)
		{
			List<entIndIndustriagps> list = new List<entIndIndustriagps>();
			
			entIndIndustriagps obj = new entIndIndustriagps();
			foreach (DataRow row in dtind_industriagps.Rows)
			{
				obj = crearObjeto(row);
				list.Add(obj);
			}
			return list;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable y con solo algunas columnas
		/// </summary>
		/// <param name="dtind_industriagps" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Lista de Objetos ind_industriagps
		/// </returns>
		internal List<entIndIndustriagps> crearListaRevisada(DataTable dtind_industriagps)
		{
			List<entIndIndustriagps> list = new List<entIndIndustriagps>();
			
			entIndIndustriagps obj = new entIndIndustriagps();
			foreach (DataRow row in dtind_industriagps.Rows)
			{
				obj = crearObjetoRevisado(row);
				list.Add(obj);
			}
			return list;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtind_industriagps" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cola de Objetos ind_industriagps
		/// </returns>
		internal Queue<entIndIndustriagps> crearCola(DataTable dtind_industriagps)
		{
			Queue<entIndIndustriagps> cola = new Queue<entIndIndustriagps>();
			
			entIndIndustriagps obj = new entIndIndustriagps();
			foreach (DataRow row in dtind_industriagps.Rows)
			{
				obj = crearObjeto(row);
				cola.Enqueue(obj);
			}
			return cola;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtind_industriagps" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Pila de Objetos ind_industriagps
		/// </returns>
		internal Stack<entIndIndustriagps> crearPila(DataTable dtind_industriagps)
		{
			Stack<entIndIndustriagps> pila = new Stack<entIndIndustriagps>();
			
			entIndIndustriagps obj = new entIndIndustriagps();
			foreach (DataRow row in dtind_industriagps.Rows)
			{
				obj = crearObjeto(row);
				pila.Push(obj);
			}
			return pila;
		}
		
		/// <summary>
		/// 	 Funcion que crea un Dicionario a partir de un DataTable
		/// </summary>
		/// <param name="dtind_industriagps" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Diccionario de Objetos ind_industriagps
		/// </returns>
		internal Dictionary<String, entIndIndustriagps> crearDiccionario(DataTable dtind_industriagps)
		{
			Dictionary<String, entIndIndustriagps>  miDic = new Dictionary<String, entIndIndustriagps>();
			
			entIndIndustriagps obj = new entIndIndustriagps();
			foreach (DataRow row in dtind_industriagps.Rows)
			{
				obj = crearObjeto(row);
				miDic.Add(obj.GetHashCode().ToString(), obj);
			}
			return miDic;
		}
		
		/// <summary>
		/// 	 Funcion que crea un Dicionario a partir de un DataTable y solo con columnas existentes
		/// </summary>
		/// <param name="dtind_industriagps" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Diccionario de Objetos ind_industriagps
		/// </returns>
		internal Dictionary<String, entIndIndustriagps> crearDiccionarioRevisado(DataTable dtind_industriagps)
		{
			Dictionary<String, entIndIndustriagps>  miDic = new Dictionary<String, entIndIndustriagps>();
			
			entIndIndustriagps obj = new entIndIndustriagps();
			foreach (DataRow row in dtind_industriagps.Rows)
			{
				obj = crearObjetoRevisado(row);
				miDic.Add(obj.GetHashCode().ToString(), obj);
			}
			return miDic;
		}
		
		public void Dispose()
		{
			GC.SuppressFinalize(this);
		}
		
		protected void Finalize()
		{
			Dispose();
		}
	}
}

