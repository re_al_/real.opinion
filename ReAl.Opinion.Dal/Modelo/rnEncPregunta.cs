#region 
/***********************************************************************************************************
	NOMBRE:       rnEncPregunta
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla enc_pregunta

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using ReAl.Opinion.Dal;
using ReAl.Opinion.PgConn;
using ReAl.Opinion.Dal.Entidades;
using ReAl.Opinion.Dal.Interface;
using System.Windows.Forms;
#endregion

namespace ReAl.Opinion.Dal.Modelo
{
	public partial class rnEncPregunta: inEncPregunta
	{
		//Debe implementar la Interface (Alt + Shift + F10)

		#region inEncPregunta Members

		#region Reflection

		/// <summary>
		/// Metodo que devuelve el Script SQL de la Tabla
		/// </summary>
		/// <returns>Script SQL</returns>
		public string getTableScript()
		{
			TableClass tabla = new TableClass(typeof(entEncPregunta));
			return tabla.CreateTableScript();
		}
		
		/// <summary>
		/// Metodo para castear Dinamicamente un Tipo
		/// <summary>
		/// <param name="valor">Tipo a ser casteado</param>
		/// <param name="myField">Enum de la columna</param>
		/// <returns>Devuelve un objeto del Tipo de la columna especificada en el Enum</returns>
		public dynamic GetColumnType(object valor, entEncPregunta.Fields myField)
		{
			if (DBNull.Value.Equals(valor)) 
				return null;
			Type destino = typeof(entEncPregunta).GetProperty(myField.ToString()).PropertyType;
			var miTipo = Nullable.GetUnderlyingType(destino) ?? destino;
			return Convert.ChangeType(valor, miTipo);
		}

		/// <summary>
		/// 	 Funcion que obtiene la llave primaria unica de la tabla enc_pregunta a partir de una cadena
		/// </summary>
		/// <param name="args" type="string[]">
		///     <para>
		/// 		 Cadena desde la que se construye el identificador unico de la tabla enc_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Identificador unico de la tabla enc_pregunta
		/// </returns>
		public string CreatePK(string[] args)
		{
			return args[0];
		}
		
		#endregion 

		#region ObtenerObjeto

		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo entEncPregunta que cumple con los filtros de los parametros
		/// </returns>
		public entEncPregunta ObtenerObjeto(Int64 Int64id_pregunta)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(entEncPregunta.Fields.id_pregunta.ToString());
		
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'" + Int64id_pregunta + "'");
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncPregunta que cumple con los filtros de los parametros
		/// </returns>
		public entEncPregunta ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncPregunta que cumple con los filtros de los parametros
		/// </returns>
		public entEncPregunta ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncPregunta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_proyecto.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_nivel.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_seccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.ayuda.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.minimo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.maximo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.catalogo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.longitud.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especifique.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mostrar_ventana.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable.ToString());
				arrColumnas.Add(entEncPregunta.Fields.formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn_formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.regla.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mensaje.ToString());
				arrColumnas.Add(entEncPregunta.Fields.revision.ToString());
				arrColumnas.Add(entEncPregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usucre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.feccre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usumod.ToString());
				arrColumnas.Add(entEncPregunta.Fields.fecmod.ToString());
				arrColumnas.Add(entEncPregunta.Fields.instruccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable_bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especial.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entEncPregunta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count == 1)
				{
					entEncPregunta obj = new entEncPregunta();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncPregunta que cumple con los filtros de los parametros
		/// </returns>
		public entEncPregunta ObtenerObjeto(Hashtable htbFiltro)
		{
			return ObtenerObjeto(htbFiltro, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncPregunta que cumple con los filtros de los parametros
		/// </returns>
		public entEncPregunta ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncPregunta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_proyecto.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_nivel.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_seccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.ayuda.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.minimo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.maximo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.catalogo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.longitud.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especifique.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mostrar_ventana.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable.ToString());
				arrColumnas.Add(entEncPregunta.Fields.formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn_formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.regla.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mensaje.ToString());
				arrColumnas.Add(entEncPregunta.Fields.revision.ToString());
				arrColumnas.Add(entEncPregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usucre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.feccre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usumod.ToString());
				arrColumnas.Add(entEncPregunta.Fields.fecmod.ToString());
				arrColumnas.Add(entEncPregunta.Fields.instruccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable_bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especial.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entEncPregunta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count == 1)
				{
					entEncPregunta obj = new entEncPregunta();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncPregunta que cumple con los filtros de los parametros
		/// </returns>
		public entEncPregunta ObtenerObjeto(entEncPregunta.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncPregunta que cumple con los filtros de los parametros
		/// </returns>
		public entEncPregunta ObtenerObjeto(entEncPregunta.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un Business Object del Tipo entEncPregunta a partir de su llave promaria
		/// </summary>
		/// <returns>
		/// 	Objeto del Tipo entEncPregunta
		/// </returns>
		public entEncPregunta ObtenerObjeto(Int64 Int64id_pregunta, ref cTrans localTrans )
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(entEncPregunta.Fields.id_pregunta.ToString());
		
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'" + Int64id_pregunta + "'");
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncPregunta que cumple con los filtros de los parametros
		/// </returns>
		public entEncPregunta ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncPregunta que cumple con los filtros de los parametros
		/// </returns>
		public entEncPregunta ObtenerObjeto(Hashtable htbFiltro, ref cTrans localTrans)
		{
			return ObtenerObjeto(htbFiltro, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncPregunta que cumple con los filtros de los parametros
		/// </returns>
		public entEncPregunta ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales,  ref cTrans localTrans)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncPregunta que cumple con los filtros de los parametros
		/// </returns>
		public entEncPregunta ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncPregunta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_proyecto.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_nivel.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_seccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.ayuda.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.minimo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.maximo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.catalogo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.longitud.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especifique.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mostrar_ventana.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable.ToString());
				arrColumnas.Add(entEncPregunta.Fields.formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn_formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.regla.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mensaje.ToString());
				arrColumnas.Add(entEncPregunta.Fields.revision.ToString());
				arrColumnas.Add(entEncPregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usucre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.feccre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usumod.ToString());
				arrColumnas.Add(entEncPregunta.Fields.fecmod.ToString());
				arrColumnas.Add(entEncPregunta.Fields.instruccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable_bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especial.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entEncPregunta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count == 1)
				{
					entEncPregunta obj = new entEncPregunta();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncPregunta que cumple con los filtros de los parametros
		/// </returns>
		public entEncPregunta ObtenerObjeto(entEncPregunta.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncPregunta que cumple con los filtros de los parametros
		/// </returns>
		public entEncPregunta ObtenerObjeto(entEncPregunta.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion

		#region ObtenerLista

		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncPregunta> ObtenerLista()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncPregunta> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncPregunta> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncPregunta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_proyecto.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_nivel.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_seccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.ayuda.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.minimo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.maximo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.catalogo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.longitud.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especifique.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mostrar_ventana.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable.ToString());
				arrColumnas.Add(entEncPregunta.Fields.formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn_formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.regla.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mensaje.ToString());
				arrColumnas.Add(entEncPregunta.Fields.revision.ToString());
				arrColumnas.Add(entEncPregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usucre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.feccre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usumod.ToString());
				arrColumnas.Add(entEncPregunta.Fields.fecmod.ToString());
				arrColumnas.Add(entEncPregunta.Fields.instruccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable_bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especial.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entEncPregunta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<entEncPregunta>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncPregunta> ObtenerLista(entEncPregunta.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncPregunta> ObtenerLista(entEncPregunta.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncPregunta> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncPregunta> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncPregunta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_proyecto.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_nivel.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_seccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.ayuda.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.minimo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.maximo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.catalogo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.longitud.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especifique.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mostrar_ventana.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable.ToString());
				arrColumnas.Add(entEncPregunta.Fields.formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn_formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.regla.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mensaje.ToString());
				arrColumnas.Add(entEncPregunta.Fields.revision.ToString());
				arrColumnas.Add(entEncPregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usucre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.feccre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usumod.ToString());
				arrColumnas.Add(entEncPregunta.Fields.fecmod.ToString());
				arrColumnas.Add(entEncPregunta.Fields.instruccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable_bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especial.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entEncPregunta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<entEncPregunta>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncPregunta> ObtenerLista(entEncPregunta.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncPregunta> ObtenerLista(entEncPregunta.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncPregunta> ObtenerLista(Hashtable htbFiltro)
		{
			return ObtenerLista(htbFiltro, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncPregunta> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncPregunta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_proyecto.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_nivel.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_seccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.ayuda.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.minimo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.maximo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.catalogo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.longitud.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especifique.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mostrar_ventana.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable.ToString());
				arrColumnas.Add(entEncPregunta.Fields.formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn_formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.regla.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mensaje.ToString());
				arrColumnas.Add(entEncPregunta.Fields.revision.ToString());
				arrColumnas.Add(entEncPregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usucre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.feccre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usumod.ToString());
				arrColumnas.Add(entEncPregunta.Fields.fecmod.ToString());
				arrColumnas.Add(entEncPregunta.Fields.instruccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable_bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especial.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entEncPregunta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<entEncPregunta>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncPregunta> ObtenerLista(Hashtable htbFiltro, ref cTrans localTrans)
		{
			return ObtenerLista(htbFiltro, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncPregunta> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncPregunta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_proyecto.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_nivel.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_seccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.ayuda.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.minimo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.maximo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.catalogo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.longitud.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especifique.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mostrar_ventana.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable.ToString());
				arrColumnas.Add(entEncPregunta.Fields.formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn_formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.regla.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mensaje.ToString());
				arrColumnas.Add(entEncPregunta.Fields.revision.ToString());
				arrColumnas.Add(entEncPregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usucre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.feccre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usumod.ToString());
				arrColumnas.Add(entEncPregunta.Fields.fecmod.ToString());
				arrColumnas.Add(entEncPregunta.Fields.instruccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable_bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especial.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entEncPregunta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<entEncPregunta>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncPregunta> ObtenerListaDesdeVista(String strVista)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncPregunta> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncPregunta> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<entEncPregunta>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncPregunta> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro)
		{
			return ObtenerListaDesdeVista(strVista, htbFiltro, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncPregunta> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<entEncPregunta>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncPregunta> ObtenerListaDesdeVista(String strVista, entEncPregunta.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncPregunta> ObtenerListaDesdeVista(String strVista, entEncPregunta.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncPregunta> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncPregunta> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<entEncPregunta>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncPregunta> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref cTrans localTrans)
		{
			return ObtenerListaDesdeVista(strVista, htbFiltro, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncPregunta> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<entEncPregunta>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncPregunta> ObtenerListaDesdeVista(String strVista, entEncPregunta.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entEncPregunta> ObtenerListaDesdeVista(String strVista, entEncPregunta.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion 

		#region ObtenerCola y Obtener Pila

		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entEncPregunta> ObtenerCola()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entEncPregunta> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entEncPregunta> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncPregunta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_proyecto.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_nivel.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_seccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.ayuda.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.minimo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.maximo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.catalogo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.longitud.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especifique.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mostrar_ventana.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable.ToString());
				arrColumnas.Add(entEncPregunta.Fields.formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn_formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.regla.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mensaje.ToString());
				arrColumnas.Add(entEncPregunta.Fields.revision.ToString());
				arrColumnas.Add(entEncPregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usucre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.feccre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usumod.ToString());
				arrColumnas.Add(entEncPregunta.Fields.fecmod.ToString());
				arrColumnas.Add(entEncPregunta.Fields.instruccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable_bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especial.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entEncPregunta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearCola(table);
				}
				else
					return new Queue<entEncPregunta>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entEncPregunta> ObtenerCola(entEncPregunta.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entEncPregunta> ObtenerCola(entEncPregunta.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entEncPregunta> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entEncPregunta> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncPregunta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_proyecto.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_nivel.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_seccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.ayuda.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.minimo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.maximo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.catalogo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.longitud.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especifique.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mostrar_ventana.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable.ToString());
				arrColumnas.Add(entEncPregunta.Fields.formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn_formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.regla.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mensaje.ToString());
				arrColumnas.Add(entEncPregunta.Fields.revision.ToString());
				arrColumnas.Add(entEncPregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usucre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.feccre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usumod.ToString());
				arrColumnas.Add(entEncPregunta.Fields.fecmod.ToString());
				arrColumnas.Add(entEncPregunta.Fields.instruccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable_bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especial.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entEncPregunta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearCola(table);
				}
				else
					return new Queue<entEncPregunta>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entEncPregunta> ObtenerCola(entEncPregunta.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entEncPregunta> ObtenerCola(entEncPregunta.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entEncPregunta> ObtenerPila()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entEncPregunta> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entEncPregunta> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncPregunta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_proyecto.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_nivel.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_seccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.ayuda.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.minimo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.maximo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.catalogo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.longitud.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especifique.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mostrar_ventana.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable.ToString());
				arrColumnas.Add(entEncPregunta.Fields.formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn_formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.regla.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mensaje.ToString());
				arrColumnas.Add(entEncPregunta.Fields.revision.ToString());
				arrColumnas.Add(entEncPregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usucre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.feccre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usumod.ToString());
				arrColumnas.Add(entEncPregunta.Fields.fecmod.ToString());
				arrColumnas.Add(entEncPregunta.Fields.instruccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable_bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especial.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entEncPregunta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearPila(table);
				}
				else
					return new Stack<entEncPregunta>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entEncPregunta> ObtenerPila(entEncPregunta.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entEncPregunta> ObtenerPila(entEncPregunta.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entEncPregunta> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entEncPregunta> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncPregunta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_proyecto.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_nivel.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_seccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.ayuda.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.minimo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.maximo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.catalogo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.longitud.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especifique.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mostrar_ventana.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable.ToString());
				arrColumnas.Add(entEncPregunta.Fields.formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn_formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.regla.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mensaje.ToString());
				arrColumnas.Add(entEncPregunta.Fields.revision.ToString());
				arrColumnas.Add(entEncPregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usucre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.feccre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usumod.ToString());
				arrColumnas.Add(entEncPregunta.Fields.fecmod.ToString());
				arrColumnas.Add(entEncPregunta.Fields.instruccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable_bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especial.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entEncPregunta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearPila(table);
				}
				else
					return new Stack<entEncPregunta>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entEncPregunta> ObtenerPila(entEncPregunta.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entEncPregunta> ObtenerPila(entEncPregunta.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion 

		#region ObtenerDataTable

		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla enc_pregunta
		/// </summary>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de enc_pregunta
		/// </returns>
		public DataTable NuevoDataTable()
		{
			try
			{
				DataTable table = new DataTable ();
				DataColumn dc;
				dc = new DataColumn(entEncPregunta.Fields.id_pregunta.ToString(),typeof(entEncPregunta).GetProperty(entEncPregunta.Fields.id_pregunta.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entEncPregunta.Fields.id_proyecto.ToString(),typeof(entEncPregunta).GetProperty(entEncPregunta.Fields.id_proyecto.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entEncPregunta.Fields.id_nivel.ToString(),typeof(entEncPregunta).GetProperty(entEncPregunta.Fields.id_nivel.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entEncPregunta.Fields.id_seccion.ToString(),typeof(entEncPregunta).GetProperty(entEncPregunta.Fields.id_seccion.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entEncPregunta.Fields.codigo_pregunta.ToString(),typeof(entEncPregunta).GetProperty(entEncPregunta.Fields.codigo_pregunta.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entEncPregunta.Fields.pregunta.ToString(),typeof(entEncPregunta).GetProperty(entEncPregunta.Fields.pregunta.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entEncPregunta.Fields.ayuda.ToString(),typeof(entEncPregunta).GetProperty(entEncPregunta.Fields.ayuda.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entEncPregunta.Fields.id_tipo_pregunta.ToString(),typeof(entEncPregunta).GetProperty(entEncPregunta.Fields.id_tipo_pregunta.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entEncPregunta.Fields.minimo.ToString(),typeof(entEncPregunta).GetProperty(entEncPregunta.Fields.minimo.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entEncPregunta.Fields.maximo.ToString(),typeof(entEncPregunta).GetProperty(entEncPregunta.Fields.maximo.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entEncPregunta.Fields.catalogo.ToString(),typeof(entEncPregunta).GetProperty(entEncPregunta.Fields.catalogo.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entEncPregunta.Fields.longitud.ToString(),typeof(entEncPregunta).GetProperty(entEncPregunta.Fields.longitud.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entEncPregunta.Fields.codigo_especifique.ToString(),typeof(entEncPregunta).GetProperty(entEncPregunta.Fields.codigo_especifique.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entEncPregunta.Fields.mostrar_ventana.ToString(),typeof(entEncPregunta).GetProperty(entEncPregunta.Fields.mostrar_ventana.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entEncPregunta.Fields.variable.ToString(),typeof(entEncPregunta).GetProperty(entEncPregunta.Fields.variable.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entEncPregunta.Fields.formula.ToString(),typeof(entEncPregunta).GetProperty(entEncPregunta.Fields.formula.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entEncPregunta.Fields.rpn_formula.ToString(),typeof(entEncPregunta).GetProperty(entEncPregunta.Fields.rpn_formula.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entEncPregunta.Fields.regla.ToString(),typeof(entEncPregunta).GetProperty(entEncPregunta.Fields.regla.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entEncPregunta.Fields.rpn.ToString(),typeof(entEncPregunta).GetProperty(entEncPregunta.Fields.rpn.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entEncPregunta.Fields.mensaje.ToString(),typeof(entEncPregunta).GetProperty(entEncPregunta.Fields.mensaje.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entEncPregunta.Fields.revision.ToString(),typeof(entEncPregunta).GetProperty(entEncPregunta.Fields.revision.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entEncPregunta.Fields.apiestado.ToString(),typeof(entEncPregunta).GetProperty(entEncPregunta.Fields.apiestado.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entEncPregunta.Fields.usucre.ToString(),typeof(entEncPregunta).GetProperty(entEncPregunta.Fields.usucre.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entEncPregunta.Fields.feccre.ToString(),typeof(entEncPregunta).GetProperty(entEncPregunta.Fields.feccre.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entEncPregunta.Fields.usumod.ToString(),typeof(entEncPregunta).GetProperty(entEncPregunta.Fields.usumod.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entEncPregunta.Fields.fecmod.ToString(),typeof(entEncPregunta).GetProperty(entEncPregunta.Fields.fecmod.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entEncPregunta.Fields.instruccion.ToString(),typeof(entEncPregunta).GetProperty(entEncPregunta.Fields.instruccion.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entEncPregunta.Fields.bucle.ToString(),typeof(entEncPregunta).GetProperty(entEncPregunta.Fields.bucle.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entEncPregunta.Fields.variable_bucle.ToString(),typeof(entEncPregunta).GetProperty(entEncPregunta.Fields.variable_bucle.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entEncPregunta.Fields.codigo_especial.ToString(),typeof(entEncPregunta).GetProperty(entEncPregunta.Fields.codigo_especial.ToString()).PropertyType);
				table.Columns.Add(dc);

				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que genera un DataTable con determinadas columnas de una enc_pregunta
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de enc_pregunta
		/// </returns>
		public DataTable NuevoDataTable(ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'2'");
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entEncPregunta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla enc_pregunta
		/// </summary>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de enc_pregunta
		/// </returns>
		public DataTable ObtenerDataTable()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDataTable(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla y n condicion WHERE enc_pregunta
		/// </summary>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de enc_pregunta
		/// </returns>
		public DataTable ObtenerDataTable(String strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla enc_pregunta
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de enc_pregunta
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla enc_pregunta
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de enc_pregunta
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla enc_pregunta
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de enc_pregunta
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla enc_pregunta
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de enc_pregunta
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro)
		{
			try
			{
				return ObtenerDataTable(arrColumnas, htbFiltro, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla enc_pregunta
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de enc_pregunta
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla enc_pregunta
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de enc_pregunta
		/// </returns>
		public DataTable ObtenerDataTable(Hashtable htbFiltro)
		{
			try
			{
				return ObtenerDataTable(htbFiltro, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla enc_pregunta
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de enc_pregunta
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entEncPregunta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla enc_pregunta
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de enc_pregunta
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entEncPregunta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla enc_pregunta
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de enc_pregunta
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncPregunta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_proyecto.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_nivel.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_seccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.ayuda.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.minimo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.maximo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.catalogo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.longitud.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especifique.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mostrar_ventana.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable.ToString());
				arrColumnas.Add(entEncPregunta.Fields.formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn_formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.regla.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mensaje.ToString());
				arrColumnas.Add(entEncPregunta.Fields.revision.ToString());
				arrColumnas.Add(entEncPregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usucre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.feccre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usumod.ToString());
				arrColumnas.Add(entEncPregunta.Fields.fecmod.ToString());
				arrColumnas.Add(entEncPregunta.Fields.instruccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable_bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especial.ToString());
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla enc_pregunta
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de enc_pregunta
		/// </returns>
		public DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncPregunta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_proyecto.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_nivel.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_seccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.ayuda.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.minimo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.maximo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.catalogo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.longitud.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especifique.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mostrar_ventana.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable.ToString());
				arrColumnas.Add(entEncPregunta.Fields.formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn_formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.regla.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mensaje.ToString());
				arrColumnas.Add(entEncPregunta.Fields.revision.ToString());
				arrColumnas.Add(entEncPregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usucre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.feccre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usumod.ToString());
				arrColumnas.Add(entEncPregunta.Fields.fecmod.ToString());
				arrColumnas.Add(entEncPregunta.Fields.instruccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable_bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especial.ToString());
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla enc_pregunta
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de enc_pregunta
		/// </returns>
		public DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncPregunta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_proyecto.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_nivel.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_seccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.ayuda.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.minimo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.maximo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.catalogo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.longitud.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especifique.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mostrar_ventana.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable.ToString());
				arrColumnas.Add(entEncPregunta.Fields.formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn_formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.regla.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mensaje.ToString());
				arrColumnas.Add(entEncPregunta.Fields.revision.ToString());
				arrColumnas.Add(entEncPregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usucre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.feccre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usumod.ToString());
				arrColumnas.Add(entEncPregunta.Fields.fecmod.ToString());
				arrColumnas.Add(entEncPregunta.Fields.instruccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable_bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especial.ToString());
				return ObtenerDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla enc_pregunta
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de enc_pregunta
		/// </returns>
		public DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return ObtenerDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla enc_pregunta
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de enc_pregunta
		/// </returns>
		public DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DataTable table = local.cargarDataTableOr(cParametros.schema + entEncPregunta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="searchField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(entEncPregunta.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="searchField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(entEncPregunta.Fields searchField, object searchValue, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, entEncPregunta.Fields searchField, object searchValue, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, entEncPregunta.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


        #endregion

        public Dictionary<String, entEncPregunta> ObtenerDiccionarioKey(entEncPregunta.Fields dicKey)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, dicKey);
        }

        public Dictionary<String, entEncPregunta> ObtenerDiccionarioKey(String strParamAdic, entEncPregunta.Fields dicKey)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, strParamAdic, dicKey);
        }

        public Dictionary<String, entEncPregunta> ObtenerDiccionarioKey(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, entEncPregunta.Fields dicKey)
        {
            return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, "", dicKey);
        }

        public Dictionary<String, entEncPregunta> ObtenerDiccionarioKey(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, entEncPregunta.Fields dicKey)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entEncPregunta.Fields.id_pregunta.ToString());
                arrColumnas.Add(entEncPregunta.Fields.id_proyecto.ToString());
                arrColumnas.Add(entEncPregunta.Fields.id_nivel.ToString());
                arrColumnas.Add(entEncPregunta.Fields.id_seccion.ToString());
                arrColumnas.Add(entEncPregunta.Fields.codigo_pregunta.ToString());
                arrColumnas.Add(entEncPregunta.Fields.pregunta.ToString());
                arrColumnas.Add(entEncPregunta.Fields.ayuda.ToString());
                arrColumnas.Add(entEncPregunta.Fields.id_tipo_pregunta.ToString());
                arrColumnas.Add(entEncPregunta.Fields.minimo.ToString());
                arrColumnas.Add(entEncPregunta.Fields.maximo.ToString());
                arrColumnas.Add(entEncPregunta.Fields.catalogo.ToString());
                arrColumnas.Add(entEncPregunta.Fields.longitud.ToString());
                arrColumnas.Add(entEncPregunta.Fields.codigo_especifique.ToString());
                arrColumnas.Add(entEncPregunta.Fields.mostrar_ventana.ToString());
                arrColumnas.Add(entEncPregunta.Fields.variable.ToString());
                arrColumnas.Add(entEncPregunta.Fields.formula.ToString());
                arrColumnas.Add(entEncPregunta.Fields.rpn_formula.ToString());
                arrColumnas.Add(entEncPregunta.Fields.regla.ToString());
                arrColumnas.Add(entEncPregunta.Fields.rpn.ToString());
                arrColumnas.Add(entEncPregunta.Fields.mensaje.ToString());
                arrColumnas.Add(entEncPregunta.Fields.revision.ToString());
                arrColumnas.Add(entEncPregunta.Fields.apiestado.ToString());
                arrColumnas.Add(entEncPregunta.Fields.usucre.ToString());
                arrColumnas.Add(entEncPregunta.Fields.feccre.ToString());
                arrColumnas.Add(entEncPregunta.Fields.usumod.ToString());
                arrColumnas.Add(entEncPregunta.Fields.fecmod.ToString());
                arrColumnas.Add(entEncPregunta.Fields.instruccion.ToString());
                arrColumnas.Add(entEncPregunta.Fields.bucle.ToString());
                arrColumnas.Add(entEncPregunta.Fields.variable_bucle.ToString());
                arrColumnas.Add(entEncPregunta.Fields.codigo_especial.ToString());

                cConn local = new cConn();
                DataTable table = local.cargarDataTableAnd(cParametros.schema + entEncPregunta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearDiccionario(table, dicKey);
                }
                else
                    return new Dictionary<string, entEncPregunta>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        public Dictionary<String, entEncPregunta> ObtenerDiccionarioKey(entEncPregunta.Fields searchField, object searchValue, entEncPregunta.Fields dicKey)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, dicKey);
        }

        public Dictionary<String, entEncPregunta> ObtenerDiccionarioKey(entEncPregunta.Fields searchField, object searchValue, string strParamAdicionales, entEncPregunta.Fields dicKey)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, strParamAdicionales, dicKey);
        }

       public Dictionary<String, entEncPregunta> ObtenerDiccionarioKey(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, entEncPregunta.Fields dicKey, ref cTrans localTrans)
        {
            return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, "", dicKey, ref localTrans);
        }

       public Dictionary<String, entEncPregunta> ObtenerDiccionarioKey(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, entEncPregunta.Fields dicKey, ref cTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entEncPregunta.Fields.id_pregunta.ToString());
                arrColumnas.Add(entEncPregunta.Fields.id_proyecto.ToString());
                arrColumnas.Add(entEncPregunta.Fields.id_nivel.ToString());
                arrColumnas.Add(entEncPregunta.Fields.id_seccion.ToString());
                arrColumnas.Add(entEncPregunta.Fields.codigo_pregunta.ToString());
                arrColumnas.Add(entEncPregunta.Fields.pregunta.ToString());
                arrColumnas.Add(entEncPregunta.Fields.ayuda.ToString());
                arrColumnas.Add(entEncPregunta.Fields.id_tipo_pregunta.ToString());
                arrColumnas.Add(entEncPregunta.Fields.minimo.ToString());
                arrColumnas.Add(entEncPregunta.Fields.maximo.ToString());
                arrColumnas.Add(entEncPregunta.Fields.catalogo.ToString());
                arrColumnas.Add(entEncPregunta.Fields.longitud.ToString());
                arrColumnas.Add(entEncPregunta.Fields.codigo_especifique.ToString());
                arrColumnas.Add(entEncPregunta.Fields.mostrar_ventana.ToString());
                arrColumnas.Add(entEncPregunta.Fields.variable.ToString());
                arrColumnas.Add(entEncPregunta.Fields.formula.ToString());
                arrColumnas.Add(entEncPregunta.Fields.rpn_formula.ToString());
                arrColumnas.Add(entEncPregunta.Fields.regla.ToString());
                arrColumnas.Add(entEncPregunta.Fields.rpn.ToString());
                arrColumnas.Add(entEncPregunta.Fields.mensaje.ToString());
                arrColumnas.Add(entEncPregunta.Fields.revision.ToString());
                arrColumnas.Add(entEncPregunta.Fields.apiestado.ToString());
                arrColumnas.Add(entEncPregunta.Fields.usucre.ToString());
                arrColumnas.Add(entEncPregunta.Fields.feccre.ToString());
                arrColumnas.Add(entEncPregunta.Fields.usumod.ToString());
                arrColumnas.Add(entEncPregunta.Fields.fecmod.ToString());
                arrColumnas.Add(entEncPregunta.Fields.instruccion.ToString());
                arrColumnas.Add(entEncPregunta.Fields.bucle.ToString());
                arrColumnas.Add(entEncPregunta.Fields.variable_bucle.ToString());
                arrColumnas.Add(entEncPregunta.Fields.codigo_especial.ToString());

                cConn local = new cConn();
                DataTable table = local.cargarDataTableAnd(cParametros.schema + entEncPregunta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearDiccionario(table, dicKey);
                }
                else
                    return new Dictionary<string, entEncPregunta>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        public Dictionary<String, entEncPregunta> ObtenerDiccionarioKey(entEncPregunta.Fields searchField, object searchValue, entEncPregunta.Fields dicKey, ref cTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, dicKey, ref localTrans);
        }

        public Dictionary<String, entEncPregunta> ObtenerDiccionarioKey(entEncPregunta.Fields searchField, object searchValue, string strParamAdicionales, entEncPregunta.Fields dicKey, ref cTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, strParamAdicionales, dicKey, ref localTrans);
        }






        #region ObtenerDiccionario

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
        /// </summary>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, entEncPregunta> ObtenerDiccionario()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entEncPregunta> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "");
		}

        
        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, entEncPregunta> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncPregunta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_proyecto.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_nivel.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_seccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.ayuda.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.minimo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.maximo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.catalogo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.longitud.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especifique.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mostrar_ventana.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable.ToString());
				arrColumnas.Add(entEncPregunta.Fields.formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn_formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.regla.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mensaje.ToString());
				arrColumnas.Add(entEncPregunta.Fields.revision.ToString());
				arrColumnas.Add(entEncPregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usucre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.feccre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usumod.ToString());
				arrColumnas.Add(entEncPregunta.Fields.fecmod.ToString());
				arrColumnas.Add(entEncPregunta.Fields.instruccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable_bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especial.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entEncPregunta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearDiccionario(table);
				}
				else
					return new Dictionary<string, entEncPregunta>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entEncPregunta> ObtenerDiccionario(entEncPregunta.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entEncPregunta> ObtenerDiccionario(entEncPregunta.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entEncPregunta> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entEncPregunta> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncPregunta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_proyecto.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_nivel.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_seccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.ayuda.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.minimo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.maximo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.catalogo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.longitud.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especifique.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mostrar_ventana.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable.ToString());
				arrColumnas.Add(entEncPregunta.Fields.formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn_formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.regla.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mensaje.ToString());
				arrColumnas.Add(entEncPregunta.Fields.revision.ToString());
				arrColumnas.Add(entEncPregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usucre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.feccre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usumod.ToString());
				arrColumnas.Add(entEncPregunta.Fields.fecmod.ToString());
				arrColumnas.Add(entEncPregunta.Fields.instruccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable_bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especial.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entEncPregunta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearDiccionario(table);
				}
				else
					return new Dictionary<string, entEncPregunta>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entEncPregunta> ObtenerDiccionario(entEncPregunta.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entEncPregunta a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entEncPregunta> ObtenerDiccionario(entEncPregunta.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion 

		#region FuncionesAgregadas

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncPregunta que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesCount(entEncPregunta.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncPregunta que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesCount(entEncPregunta.Fields refField, entEncPregunta.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncPregunta que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesCount(entEncPregunta.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("count(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncPregunta que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMin(entEncPregunta.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncPregunta que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMin(entEncPregunta.Fields refField, entEncPregunta.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncPregunta que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMin(entEncPregunta.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("min(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncPregunta que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMax(entEncPregunta.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncPregunta que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMax(entEncPregunta.Fields refField, entEncPregunta.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncPregunta que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMax(entEncPregunta.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("max(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncPregunta que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesSum(entEncPregunta.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncPregunta que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesSum(entEncPregunta.Fields refField, entEncPregunta.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncPregunta que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesSum(entEncPregunta.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("sum(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncPregunta que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesAvg(entEncPregunta.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncPregunta que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesAvg(entEncPregunta.Fields refField, entEncPregunta.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entEncPregunta que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesAvg(entEncPregunta.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("avg(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion 

		#region ABMs

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla enc_pregunta a partir de una clase del tipo Eenc_pregunta
		/// </summary>
		/// <param name="obj" type="Entidades.entEncPregunta">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla enc_preguntaenc_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionenc_pregunta
		/// </returns>
		public bool Insert(entEncPregunta obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(entEncPregunta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_proyecto.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_nivel.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_seccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.ayuda.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.minimo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.maximo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.catalogo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.longitud.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especifique.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mostrar_ventana.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable.ToString());
				arrColumnas.Add(entEncPregunta.Fields.formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn_formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.regla.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mensaje.ToString());
				arrColumnas.Add(entEncPregunta.Fields.revision.ToString());
				arrColumnas.Add(entEncPregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usucre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.feccre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.instruccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable_bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especial.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.id_pregunta == null ? null : "'" + obj.id_pregunta + "'");
				arrValores.Add(obj.id_proyecto);
				arrValores.Add(obj.id_nivel);
				arrValores.Add(obj.id_seccion);
				arrValores.Add(obj.codigo_pregunta == null ? null : "'" + obj.codigo_pregunta + "'");
				arrValores.Add(obj.pregunta == null ? null : "'" + obj.pregunta + "'");
				arrValores.Add(obj.ayuda == null ? null : "'" + obj.ayuda + "'");
				arrValores.Add(obj.id_tipo_pregunta);
				arrValores.Add(obj.minimo);
				arrValores.Add(obj.maximo);
				arrValores.Add(obj.catalogo == null ? null : "'" + obj.catalogo + "'");
				arrValores.Add(obj.longitud);
				arrValores.Add(obj.codigo_especifique == null ? null : "'" + obj.codigo_especifique + "'");
				arrValores.Add(obj.mostrar_ventana);
				arrValores.Add(obj.variable == null ? null : "'" + obj.variable + "'");
				arrValores.Add(obj.formula == null ? null : "'" + obj.formula + "'");
				arrValores.Add(obj.rpn_formula == null ? null : "'" + obj.rpn_formula + "'");
				arrValores.Add(obj.regla == null ? null : "'" + obj.regla + "'");
				arrValores.Add(obj.rpn == null ? null : "'" + obj.rpn + "'");
				arrValores.Add(obj.mensaje == null ? null : "'" + obj.mensaje + "'");
				arrValores.Add(obj.revision == null ? null : "'" + obj.revision + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.instruccion == null ? null : "'" + obj.instruccion + "'");
				arrValores.Add(obj.bucle);
				arrValores.Add(obj.variable_bucle == null ? null : "'" + obj.variable_bucle + "'");
				arrValores.Add(obj.codigo_especial);

			
				cConn local = new cConn();
				return local.insertBD(entEncPregunta.strNombreTabla, arrColumnas, arrValores);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla enc_pregunta a partir de una clase del tipo Eenc_pregunta
		/// </summary>
		/// <param name="obj" type="Entidades.entEncPregunta">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla enc_preguntaenc_pregunta
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionenc_pregunta
		/// </returns>
		public bool Insert(entEncPregunta obj, ref cTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(entEncPregunta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_proyecto.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_nivel.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_seccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.ayuda.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.minimo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.maximo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.catalogo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.longitud.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especifique.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mostrar_ventana.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable.ToString());
				arrColumnas.Add(entEncPregunta.Fields.formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn_formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.regla.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mensaje.ToString());
				arrColumnas.Add(entEncPregunta.Fields.revision.ToString());
				arrColumnas.Add(entEncPregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usucre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.feccre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.instruccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable_bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especial.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.id_pregunta == null ? null : "'" + obj.id_pregunta + "'");
				arrValores.Add(obj.id_proyecto);
				arrValores.Add(obj.id_nivel);
				arrValores.Add(obj.id_seccion);
				arrValores.Add(obj.codigo_pregunta == null ? null : "'" + obj.codigo_pregunta + "'");
				arrValores.Add(obj.pregunta == null ? null : "'" + obj.pregunta + "'");
				arrValores.Add(obj.ayuda == null ? null : "'" + obj.ayuda + "'");
				arrValores.Add(obj.id_tipo_pregunta);
				arrValores.Add(obj.minimo);
				arrValores.Add(obj.maximo);
				arrValores.Add(obj.catalogo == null ? null : "'" + obj.catalogo + "'");
				arrValores.Add(obj.longitud);
				arrValores.Add(obj.codigo_especifique == null ? null : "'" + obj.codigo_especifique + "'");
				arrValores.Add(obj.mostrar_ventana);
				arrValores.Add(obj.variable == null ? null : "'" + obj.variable + "'");
				arrValores.Add(obj.formula == null ? null : "'" + obj.formula + "'");
				arrValores.Add(obj.rpn_formula == null ? null : "'" + obj.rpn_formula + "'");
				arrValores.Add(obj.regla == null ? null : "'" + obj.regla + "'");
				arrValores.Add(obj.rpn == null ? null : "'" + obj.rpn + "'");
				arrValores.Add(obj.mensaje == null ? null : "'" + obj.mensaje + "'");
				arrValores.Add(obj.revision == null ? null : "'" + obj.revision + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.instruccion == null ? null : "'" + obj.instruccion + "'");
				arrValores.Add(obj.bucle);
				arrValores.Add(obj.variable_bucle == null ? null : "'" + obj.variable_bucle + "'");
				arrValores.Add(obj.codigo_especial);

			
				cConn local = new cConn();
				return local.insertBD(entEncPregunta.strNombreTabla, arrColumnas, arrValores, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla enc_pregunta a partir de una clase del tipo Eenc_pregunta
		/// </summary>
		/// <param name="obj" type="Entidades.entEncPregunta">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla enc_preguntaenc_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionenc_pregunta
		/// </returns>
		public bool InsertIdentity(entEncPregunta obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(entEncPregunta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_proyecto.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_nivel.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_seccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.ayuda.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.minimo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.maximo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.catalogo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.longitud.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especifique.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mostrar_ventana.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable.ToString());
				arrColumnas.Add(entEncPregunta.Fields.formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn_formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.regla.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mensaje.ToString());
				arrColumnas.Add(entEncPregunta.Fields.revision.ToString());
				arrColumnas.Add(entEncPregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usucre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.feccre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.instruccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable_bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especial.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.id_pregunta == null ? null : "'" + obj.id_pregunta + "'");
				arrValores.Add(obj.id_proyecto);
				arrValores.Add(obj.id_nivel);
				arrValores.Add(obj.id_seccion);
				arrValores.Add(obj.codigo_pregunta == null ? null : "'" + obj.codigo_pregunta + "'");
				arrValores.Add(obj.pregunta == null ? null : "'" + obj.pregunta + "'");
				arrValores.Add(obj.ayuda == null ? null : "'" + obj.ayuda + "'");
				arrValores.Add(obj.id_tipo_pregunta);
				arrValores.Add(obj.minimo);
				arrValores.Add(obj.maximo);
				arrValores.Add(obj.catalogo == null ? null : "'" + obj.catalogo + "'");
				arrValores.Add(obj.longitud);
				arrValores.Add(obj.codigo_especifique == null ? null : "'" + obj.codigo_especifique + "'");
				arrValores.Add(obj.mostrar_ventana);
				arrValores.Add(obj.variable == null ? null : "'" + obj.variable + "'");
				arrValores.Add(obj.formula == null ? null : "'" + obj.formula + "'");
				arrValores.Add(obj.rpn_formula == null ? null : "'" + obj.rpn_formula + "'");
				arrValores.Add(obj.regla == null ? null : "'" + obj.regla + "'");
				arrValores.Add(obj.rpn == null ? null : "'" + obj.rpn + "'");
				arrValores.Add(obj.mensaje == null ? null : "'" + obj.mensaje + "'");
				arrValores.Add(obj.revision == null ? null : "'" + obj.revision + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.instruccion == null ? null : "'" + obj.instruccion + "'");
				arrValores.Add(obj.bucle);
				arrValores.Add(obj.variable_bucle == null ? null : "'" + obj.variable_bucle + "'");
				arrValores.Add(obj.codigo_especial);

			
				cConn local = new cConn();
				int intIdentidad = -1;
				bool res = local.insertBD(entEncPregunta.strNombreTabla, arrColumnas, arrValores, ref intIdentidad);
				obj.id_pregunta = intIdentidad;
				return res;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla enc_pregunta a partir de una clase del tipo Eenc_pregunta
		/// </summary>
		/// <param name="obj" type="Entidades.entEncPregunta">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla enc_preguntaenc_pregunta
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionenc_pregunta
		/// </returns>
		public bool InsertIdentity(entEncPregunta obj, ref cTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(entEncPregunta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_proyecto.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_nivel.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_seccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.ayuda.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.minimo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.maximo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.catalogo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.longitud.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especifique.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mostrar_ventana.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable.ToString());
				arrColumnas.Add(entEncPregunta.Fields.formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn_formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.regla.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mensaje.ToString());
				arrColumnas.Add(entEncPregunta.Fields.revision.ToString());
				arrColumnas.Add(entEncPregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usucre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.feccre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.instruccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable_bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especial.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.id_pregunta == null ? null : "'" + obj.id_pregunta + "'");
				arrValores.Add(obj.id_proyecto);
				arrValores.Add(obj.id_nivel);
				arrValores.Add(obj.id_seccion);
				arrValores.Add(obj.codigo_pregunta == null ? null : "'" + obj.codigo_pregunta + "'");
				arrValores.Add(obj.pregunta == null ? null : "'" + obj.pregunta + "'");
				arrValores.Add(obj.ayuda == null ? null : "'" + obj.ayuda + "'");
				arrValores.Add(obj.id_tipo_pregunta);
				arrValores.Add(obj.minimo);
				arrValores.Add(obj.maximo);
				arrValores.Add(obj.catalogo == null ? null : "'" + obj.catalogo + "'");
				arrValores.Add(obj.longitud);
				arrValores.Add(obj.codigo_especifique == null ? null : "'" + obj.codigo_especifique + "'");
				arrValores.Add(obj.mostrar_ventana);
				arrValores.Add(obj.variable == null ? null : "'" + obj.variable + "'");
				arrValores.Add(obj.formula == null ? null : "'" + obj.formula + "'");
				arrValores.Add(obj.rpn_formula == null ? null : "'" + obj.rpn_formula + "'");
				arrValores.Add(obj.regla == null ? null : "'" + obj.regla + "'");
				arrValores.Add(obj.rpn == null ? null : "'" + obj.rpn + "'");
				arrValores.Add(obj.mensaje == null ? null : "'" + obj.mensaje + "'");
				arrValores.Add(obj.revision == null ? null : "'" + obj.revision + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.instruccion == null ? null : "'" + obj.instruccion + "'");
				arrValores.Add(obj.bucle);
				arrValores.Add(obj.variable_bucle == null ? null : "'" + obj.variable_bucle + "'");
				arrValores.Add(obj.codigo_especial);

			
				cConn local = new cConn();
				int intIdentidad = -1;
				bool res = local.insertBD(entEncPregunta.strNombreTabla, arrColumnas, arrValores, ref intIdentidad, ref localTrans);
				obj.id_pregunta = intIdentidad;
				return res;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla enc_pregunta a partir de una clase del tipo Eenc_pregunta
		/// </summary>
		/// <param name="obj" type="Entidades.entEncPregunta">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla enc_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionenc_pregunta
		/// </returns>
		public int Update(entEncPregunta obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncPregunta.Fields.id_proyecto.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_nivel.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_seccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.ayuda.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.minimo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.maximo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.catalogo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.longitud.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especifique.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mostrar_ventana.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable.ToString());
				arrColumnas.Add(entEncPregunta.Fields.formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn_formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.regla.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mensaje.ToString());
				arrColumnas.Add(entEncPregunta.Fields.revision.ToString());
				arrColumnas.Add(entEncPregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usucre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.feccre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.instruccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable_bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especial.ToString());
			
				ArrayList arrValores = new ArrayList();
				arrValores.Add(obj.id_proyecto);
				arrValores.Add(obj.id_nivel);
				arrValores.Add(obj.id_seccion);
				arrValores.Add(obj.codigo_pregunta == null ? null : "'" + obj.codigo_pregunta + "'");
				arrValores.Add(obj.pregunta == null ? null : "'" + obj.pregunta + "'");
				arrValores.Add(obj.ayuda == null ? null : "'" + obj.ayuda + "'");
				arrValores.Add(obj.id_tipo_pregunta);
				arrValores.Add(obj.minimo);
				arrValores.Add(obj.maximo);
				arrValores.Add(obj.catalogo == null ? null : "'" + obj.catalogo + "'");
				arrValores.Add(obj.longitud);
				arrValores.Add(obj.codigo_especifique == null ? null : "'" + obj.codigo_especifique + "'");
				arrValores.Add(obj.mostrar_ventana);
				arrValores.Add(obj.variable == null ? null : "'" + obj.variable + "'");
				arrValores.Add(obj.formula == null ? null : "'" + obj.formula + "'");
				arrValores.Add(obj.rpn_formula == null ? null : "'" + obj.rpn_formula + "'");
				arrValores.Add(obj.regla == null ? null : "'" + obj.regla + "'");
				arrValores.Add(obj.rpn == null ? null : "'" + obj.rpn + "'");
				arrValores.Add(obj.mensaje == null ? null : "'" + obj.mensaje + "'");
				arrValores.Add(obj.revision == null ? null : "'" + obj.revision + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.instruccion == null ? null : "'" + obj.instruccion + "'");
				arrValores.Add(obj.bucle);
				arrValores.Add(obj.variable_bucle == null ? null : "'" + obj.variable_bucle + "'");
				arrValores.Add(obj.codigo_especial);

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(entEncPregunta.Fields.id_pregunta.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.id_pregunta == null ? null : "'" + obj.id_pregunta + "'");

			
				cConn local = new cConn();
				return local.updateBD(entEncPregunta.strNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla enc_pregunta a partir de una clase del tipo eenc_pregunta
		/// </summary>
		/// <param name="obj" type="Entidades.entEncPregunta">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla enc_pregunta
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacion
		/// </returns>
		public int Update(entEncPregunta obj, ref cTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncPregunta.Fields.id_proyecto.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_nivel.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_seccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.ayuda.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.minimo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.maximo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.catalogo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.longitud.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especifique.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mostrar_ventana.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable.ToString());
				arrColumnas.Add(entEncPregunta.Fields.formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn_formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.regla.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mensaje.ToString());
				arrColumnas.Add(entEncPregunta.Fields.revision.ToString());
				arrColumnas.Add(entEncPregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usucre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.feccre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.instruccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable_bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especial.ToString());
			
				ArrayList arrValores = new ArrayList();
				arrValores.Add(obj.id_proyecto);
				arrValores.Add(obj.id_nivel);
				arrValores.Add(obj.id_seccion);
				arrValores.Add(obj.codigo_pregunta == null ? null : "'" + obj.codigo_pregunta + "'");
				arrValores.Add(obj.pregunta == null ? null : "'" + obj.pregunta + "'");
				arrValores.Add(obj.ayuda == null ? null : "'" + obj.ayuda + "'");
				arrValores.Add(obj.id_tipo_pregunta);
				arrValores.Add(obj.minimo);
				arrValores.Add(obj.maximo);
				arrValores.Add(obj.catalogo == null ? null : "'" + obj.catalogo + "'");
				arrValores.Add(obj.longitud);
				arrValores.Add(obj.codigo_especifique == null ? null : "'" + obj.codigo_especifique + "'");
				arrValores.Add(obj.mostrar_ventana);
				arrValores.Add(obj.variable == null ? null : "'" + obj.variable + "'");
				arrValores.Add(obj.formula == null ? null : "'" + obj.formula + "'");
				arrValores.Add(obj.rpn_formula == null ? null : "'" + obj.rpn_formula + "'");
				arrValores.Add(obj.regla == null ? null : "'" + obj.regla + "'");
				arrValores.Add(obj.rpn == null ? null : "'" + obj.rpn + "'");
				arrValores.Add(obj.mensaje == null ? null : "'" + obj.mensaje + "'");
				arrValores.Add(obj.revision == null ? null : "'" + obj.revision + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.instruccion == null ? null : "'" + obj.instruccion + "'");
				arrValores.Add(obj.bucle);
				arrValores.Add(obj.variable_bucle == null ? null : "'" + obj.variable_bucle + "'");
				arrValores.Add(obj.codigo_especial);

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(entEncPregunta.Fields.id_pregunta.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.id_pregunta == null ? null : "'" + obj.id_pregunta + "'");

			
				cConn local = new cConn();
				return local.updateBD(entEncPregunta.strNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla enc_pregunta a partir de una clase del tipo entEncPregunta y su respectiva PK
		/// </summary>
		/// <param name="obj" type="Entidades.entEncPregunta">
		///     <para>
		/// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla enc_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionenc_pregunta
		/// </returns>
		public int Delete(entEncPregunta obj)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(entEncPregunta.Fields.id_pregunta.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.id_pregunta == null ? null : "'" + obj.id_pregunta + "'");

			
				cConn local = new cConn();
				return local.deleteBD(entEncPregunta.strNombreTabla, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla enc_pregunta a partir de una clase del tipo entEncPregunta y su PK
		/// </summary>
		/// <param name="obj" type="Entidades.eenc_pregunta">
		///     <para>
		/// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla enc_pregunta
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacionenc_pregunta
		/// </returns>
		public int Delete(entEncPregunta obj, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(entEncPregunta.Fields.id_pregunta.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.id_pregunta == null ? null : "'" + obj.id_pregunta + "'");

			
				cConn local = new cConn();
				return local.deleteBD(entEncPregunta.strNombreTabla, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla enc_pregunta a partir de una clase del tipo eenc_pregunta
		/// </summary>
		/// <param name="arrColumnasWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Valores para cada una de las columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionenc_pregunta
		/// </returns>
		public int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				cConn local = new cConn();
				return local.deleteBD("enc_pregunta", arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla enc_pregunta a partir de una clase del tipo eenc_pregunta
		/// </summary>
		/// <param name="arrColumnasWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Valores para cada una de las columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion enc_pregunta
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacionenc_pregunta
		/// </returns>
		public int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			try
			{
				cConn local = new cConn();
				return local.deleteBD(entEncPregunta.strNombreTabla, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		

		#endregion 

		#region Llenado de elementos

		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla enc_pregunta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla enc_preguntaenc_pregunta
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla enc_pregunta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla enc_preguntaenc_pregunta
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla enc_pregunta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla enc_preguntaenc_pregunta
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla enc_pregunta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla enc_preguntaenc_pregunta
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncPregunta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_proyecto.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla enc_pregunta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla enc_preguntaenc_pregunta
		///     </para>
		/// </param>
		/// <param name="valueField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entEncPregunta.Fields valueField, entEncPregunta.Fields textField)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla enc_pregunta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla enc_preguntaenc_pregunta
		///     </para>
		/// </param>
		/// <param name="valueField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entEncPregunta.Fields valueField, entEncPregunta.Fields textField, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla enc_pregunta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla enc_preguntaenc_pregunta
		///     </para>
		/// </param>
		/// <param name="valueField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entEncPregunta.Fields valueField, String textField)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla enc_pregunta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla enc_preguntaenc_pregunta
		///     </para>
		/// </param>
		/// <param name="valueField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entEncPregunta.Fields valueField, String textField, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla enc_pregunta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla enc_preguntaenc_pregunta
		///     </para>
		/// </param>
		/// <param name="valueField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entEncPregunta.Fields valueField, entEncPregunta.Fields textField, entEncPregunta.Fields searchField, object searchValue)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField.ToString(), searchField, searchValue);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla enc_pregunta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla enc_preguntaenc_pregunta
		///     </para>
		/// </param>
		/// <param name="valueField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entEncPregunta.Fields valueField, String textField, entEncPregunta.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla enc_pregunta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla enc_preguntaenc_pregunta
		///     </para>
		/// </param>
		/// <param name="valueField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entEncPregunta.Fields valueField, entEncPregunta.Fields textField, entEncPregunta.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla enc_pregunta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla enc_preguntaenc_pregunta
		///     </para>
		/// </param>
		/// <param name="valueField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entEncPregunta.Fields valueField, String textField, entEncPregunta.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla enc_pregunta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla enc_preguntaenc_pregunta
		///     </para>
		/// </param>
		/// <param name="valueField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entEncPregunta.Fields valueField, entEncPregunta.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla enc_pregunta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla enc_preguntaenc_pregunta
		///     </para>
		/// </param>
		/// <param name="valueField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entEncPregunta.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla enc_pregunta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla enc_preguntaenc_pregunta
		///     </para>
		/// </param>
		/// <param name="valueField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entEncPregunta.Fields valueField, entEncPregunta.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla enc_pregunta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla enc_preguntaenc_pregunta
		///     </para>
		/// </param>
		/// <param name="valueField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entEncPregunta.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla enc_pregunta
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla enc_preguntaenc_pregunta
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entEncPregunta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Columns.Count > 0)
				{
					cmb.ValueMember = table.Columns[0].ColumnName;
					cmb.DisplayMember = table.Columns[1].ColumnName;
					cmb.DataSource = table;

				}
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla enc_pregunta
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncPregunta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_proyecto.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_nivel.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_seccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.ayuda.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.minimo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.maximo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.catalogo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.longitud.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especifique.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mostrar_ventana.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable.ToString());
				arrColumnas.Add(entEncPregunta.Fields.formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn_formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.regla.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mensaje.ToString());
				arrColumnas.Add(entEncPregunta.Fields.revision.ToString());
				arrColumnas.Add(entEncPregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usucre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.feccre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usumod.ToString());
				arrColumnas.Add(entEncPregunta.Fields.fecmod.ToString());
				arrColumnas.Add(entEncPregunta.Fields.instruccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable_bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especial.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla enc_pregunta
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		  Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncPregunta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_proyecto.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_nivel.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_seccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.ayuda.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.minimo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.maximo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.catalogo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.longitud.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especifique.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mostrar_ventana.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable.ToString());
				arrColumnas.Add(entEncPregunta.Fields.formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn_formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.regla.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mensaje.ToString());
				arrColumnas.Add(entEncPregunta.Fields.revision.ToString());
				arrColumnas.Add(entEncPregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usucre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.feccre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usumod.ToString());
				arrColumnas.Add(entEncPregunta.Fields.fecmod.ToString());
				arrColumnas.Add(entEncPregunta.Fields.instruccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable_bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especial.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla enc_pregunta
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla enc_pregunta
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla enc_pregunta
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncPregunta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_proyecto.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_nivel.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_seccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.ayuda.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.minimo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.maximo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.catalogo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.longitud.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especifique.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mostrar_ventana.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable.ToString());
				arrColumnas.Add(entEncPregunta.Fields.formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn_formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.regla.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mensaje.ToString());
				arrColumnas.Add(entEncPregunta.Fields.revision.ToString());
				arrColumnas.Add(entEncPregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usucre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.feccre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usumod.ToString());
				arrColumnas.Add(entEncPregunta.Fields.fecmod.ToString());
				arrColumnas.Add(entEncPregunta.Fields.instruccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable_bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especial.ToString());
			
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla enc_pregunta
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncPregunta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_proyecto.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_nivel.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_seccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.ayuda.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.minimo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.maximo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.catalogo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.longitud.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especifique.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mostrar_ventana.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable.ToString());
				arrColumnas.Add(entEncPregunta.Fields.formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn_formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.regla.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mensaje.ToString());
				arrColumnas.Add(entEncPregunta.Fields.revision.ToString());
				arrColumnas.Add(entEncPregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usucre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.feccre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usumod.ToString());
				arrColumnas.Add(entEncPregunta.Fields.fecmod.ToString());
				arrColumnas.Add(entEncPregunta.Fields.instruccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable_bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especial.ToString());
			
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla enc_pregunta
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla enc_pregunta
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DbDataReader dsReader = local.cargarDataReaderAnd(cParametros.schema + entEncPregunta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				dtg.DataSource = dsReader;

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla enc_pregunta
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, entEncPregunta.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncPregunta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_proyecto.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_nivel.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_seccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.ayuda.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.minimo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.maximo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.catalogo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.longitud.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especifique.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mostrar_ventana.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable.ToString());
				arrColumnas.Add(entEncPregunta.Fields.formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn_formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.regla.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mensaje.ToString());
				arrColumnas.Add(entEncPregunta.Fields.revision.ToString());
				arrColumnas.Add(entEncPregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usucre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.feccre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usumod.ToString());
				arrColumnas.Add(entEncPregunta.Fields.fecmod.ToString());
				arrColumnas.Add(entEncPregunta.Fields.instruccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable_bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especial.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla enc_pregunta
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, entEncPregunta.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entEncPregunta.Fields.id_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_proyecto.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_nivel.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_seccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.ayuda.ToString());
				arrColumnas.Add(entEncPregunta.Fields.id_tipo_pregunta.ToString());
				arrColumnas.Add(entEncPregunta.Fields.minimo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.maximo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.catalogo.ToString());
				arrColumnas.Add(entEncPregunta.Fields.longitud.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especifique.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mostrar_ventana.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable.ToString());
				arrColumnas.Add(entEncPregunta.Fields.formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn_formula.ToString());
				arrColumnas.Add(entEncPregunta.Fields.regla.ToString());
				arrColumnas.Add(entEncPregunta.Fields.rpn.ToString());
				arrColumnas.Add(entEncPregunta.Fields.mensaje.ToString());
				arrColumnas.Add(entEncPregunta.Fields.revision.ToString());
				arrColumnas.Add(entEncPregunta.Fields.apiestado.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usucre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.feccre.ToString());
				arrColumnas.Add(entEncPregunta.Fields.usumod.ToString());
				arrColumnas.Add(entEncPregunta.Fields.fecmod.ToString());
				arrColumnas.Add(entEncPregunta.Fields.instruccion.ToString());
				arrColumnas.Add(entEncPregunta.Fields.bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.variable_bucle.ToString());
				arrColumnas.Add(entEncPregunta.Fields.codigo_especial.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla enc_pregunta
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entEncPregunta.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla enc_pregunta
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entEncPregunta.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla enc_pregunta
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarDataGridOr(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla enc_pregunta
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DbDataReader dsReader = local.cargarDataReaderOr(cParametros.schema + entEncPregunta.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				dtg.DataSource = dsReader;

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion 


		#endregion

		#region Funciones Internas

		/// <summary>
		/// 	 Funcion que devuelve un objeto a partir de un DataRow
		/// </summary>
		/// <param name="row" type="System.Data.DataRow">
		///     <para>
		/// 		 DataRow con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Objeto enc_pregunta
		/// </returns>
		internal entEncPregunta crearObjeto(DataRow row)
		{
			entEncPregunta obj = new entEncPregunta();
			obj.id_pregunta = GetColumnType(row[entEncPregunta.Fields.id_pregunta.ToString()], entEncPregunta.Fields.id_pregunta);
			obj.id_proyecto = GetColumnType(row[entEncPregunta.Fields.id_proyecto.ToString()], entEncPregunta.Fields.id_proyecto);
			obj.id_nivel = GetColumnType(row[entEncPregunta.Fields.id_nivel.ToString()], entEncPregunta.Fields.id_nivel);
			obj.id_seccion = GetColumnType(row[entEncPregunta.Fields.id_seccion.ToString()], entEncPregunta.Fields.id_seccion);
			obj.codigo_pregunta = GetColumnType(row[entEncPregunta.Fields.codigo_pregunta.ToString()], entEncPregunta.Fields.codigo_pregunta);
			obj.pregunta = GetColumnType(row[entEncPregunta.Fields.pregunta.ToString()], entEncPregunta.Fields.pregunta);
			obj.ayuda = GetColumnType(row[entEncPregunta.Fields.ayuda.ToString()], entEncPregunta.Fields.ayuda);
			obj.id_tipo_pregunta = GetColumnType(row[entEncPregunta.Fields.id_tipo_pregunta.ToString()], entEncPregunta.Fields.id_tipo_pregunta);
			obj.minimo = GetColumnType(row[entEncPregunta.Fields.minimo.ToString()], entEncPregunta.Fields.minimo);
			obj.maximo = GetColumnType(row[entEncPregunta.Fields.maximo.ToString()], entEncPregunta.Fields.maximo);
			obj.catalogo = GetColumnType(row[entEncPregunta.Fields.catalogo.ToString()], entEncPregunta.Fields.catalogo);
			obj.longitud = GetColumnType(row[entEncPregunta.Fields.longitud.ToString()], entEncPregunta.Fields.longitud);
			obj.codigo_especifique = GetColumnType(row[entEncPregunta.Fields.codigo_especifique.ToString()], entEncPregunta.Fields.codigo_especifique);
			obj.mostrar_ventana = GetColumnType(row[entEncPregunta.Fields.mostrar_ventana.ToString()], entEncPregunta.Fields.mostrar_ventana);
			obj.variable = GetColumnType(row[entEncPregunta.Fields.variable.ToString()], entEncPregunta.Fields.variable);
			obj.formula = GetColumnType(row[entEncPregunta.Fields.formula.ToString()], entEncPregunta.Fields.formula);
			obj.rpn_formula = GetColumnType(row[entEncPregunta.Fields.rpn_formula.ToString()], entEncPregunta.Fields.rpn_formula);
			obj.regla = GetColumnType(row[entEncPregunta.Fields.regla.ToString()], entEncPregunta.Fields.regla);
			obj.rpn = GetColumnType(row[entEncPregunta.Fields.rpn.ToString()], entEncPregunta.Fields.rpn);
			obj.mensaje = GetColumnType(row[entEncPregunta.Fields.mensaje.ToString()], entEncPregunta.Fields.mensaje);
			obj.revision = GetColumnType(row[entEncPregunta.Fields.revision.ToString()], entEncPregunta.Fields.revision);
			obj.apiestado = GetColumnType(row[entEncPregunta.Fields.apiestado.ToString()], entEncPregunta.Fields.apiestado);
			obj.usucre = GetColumnType(row[entEncPregunta.Fields.usucre.ToString()], entEncPregunta.Fields.usucre);
			obj.feccre = GetColumnType(row[entEncPregunta.Fields.feccre.ToString()], entEncPregunta.Fields.feccre);
			obj.usumod = GetColumnType(row[entEncPregunta.Fields.usumod.ToString()], entEncPregunta.Fields.usumod);
			obj.fecmod = GetColumnType(row[entEncPregunta.Fields.fecmod.ToString()], entEncPregunta.Fields.fecmod);
			obj.instruccion = GetColumnType(row[entEncPregunta.Fields.instruccion.ToString()], entEncPregunta.Fields.instruccion);
			obj.bucle = GetColumnType(row[entEncPregunta.Fields.bucle.ToString()], entEncPregunta.Fields.bucle);
			obj.variable_bucle = GetColumnType(row[entEncPregunta.Fields.variable_bucle.ToString()], entEncPregunta.Fields.variable_bucle);
			obj.codigo_especial = GetColumnType(row[entEncPregunta.Fields.codigo_especial.ToString()], entEncPregunta.Fields.codigo_especial);
			return obj;
		}

		/// <summary>
		/// 	 Funcion que devuelve un objeto a partir de un DataRow
		/// </summary>
		/// <param name="row" type="System.Data.DataRow">
		///     <para>
		/// 		 DataRow con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Objeto enc_pregunta
		/// </returns>
		internal entEncPregunta crearObjetoRevisado(DataRow row)
		{
			entEncPregunta obj = new entEncPregunta();
			if (row.Table.Columns.Contains(entEncPregunta.Fields.id_pregunta.ToString()))
				obj.id_pregunta = GetColumnType(row[entEncPregunta.Fields.id_pregunta.ToString()], entEncPregunta.Fields.id_pregunta);
			if (row.Table.Columns.Contains(entEncPregunta.Fields.id_proyecto.ToString()))
				obj.id_proyecto = GetColumnType(row[entEncPregunta.Fields.id_proyecto.ToString()], entEncPregunta.Fields.id_proyecto);
			if (row.Table.Columns.Contains(entEncPregunta.Fields.id_nivel.ToString()))
				obj.id_nivel = GetColumnType(row[entEncPregunta.Fields.id_nivel.ToString()], entEncPregunta.Fields.id_nivel);
			if (row.Table.Columns.Contains(entEncPregunta.Fields.id_seccion.ToString()))
				obj.id_seccion = GetColumnType(row[entEncPregunta.Fields.id_seccion.ToString()], entEncPregunta.Fields.id_seccion);
			if (row.Table.Columns.Contains(entEncPregunta.Fields.codigo_pregunta.ToString()))
				obj.codigo_pregunta = GetColumnType(row[entEncPregunta.Fields.codigo_pregunta.ToString()], entEncPregunta.Fields.codigo_pregunta);
			if (row.Table.Columns.Contains(entEncPregunta.Fields.pregunta.ToString()))
				obj.pregunta = GetColumnType(row[entEncPregunta.Fields.pregunta.ToString()], entEncPregunta.Fields.pregunta);
			if (row.Table.Columns.Contains(entEncPregunta.Fields.ayuda.ToString()))
				obj.ayuda = GetColumnType(row[entEncPregunta.Fields.ayuda.ToString()], entEncPregunta.Fields.ayuda);
			if (row.Table.Columns.Contains(entEncPregunta.Fields.id_tipo_pregunta.ToString()))
				obj.id_tipo_pregunta = GetColumnType(row[entEncPregunta.Fields.id_tipo_pregunta.ToString()], entEncPregunta.Fields.id_tipo_pregunta);
			if (row.Table.Columns.Contains(entEncPregunta.Fields.minimo.ToString()))
				obj.minimo = GetColumnType(row[entEncPregunta.Fields.minimo.ToString()], entEncPregunta.Fields.minimo);
			if (row.Table.Columns.Contains(entEncPregunta.Fields.maximo.ToString()))
				obj.maximo = GetColumnType(row[entEncPregunta.Fields.maximo.ToString()], entEncPregunta.Fields.maximo);
			if (row.Table.Columns.Contains(entEncPregunta.Fields.catalogo.ToString()))
				obj.catalogo = GetColumnType(row[entEncPregunta.Fields.catalogo.ToString()], entEncPregunta.Fields.catalogo);
			if (row.Table.Columns.Contains(entEncPregunta.Fields.longitud.ToString()))
				obj.longitud = GetColumnType(row[entEncPregunta.Fields.longitud.ToString()], entEncPregunta.Fields.longitud);
			if (row.Table.Columns.Contains(entEncPregunta.Fields.codigo_especifique.ToString()))
				obj.codigo_especifique = GetColumnType(row[entEncPregunta.Fields.codigo_especifique.ToString()], entEncPregunta.Fields.codigo_especifique);
			if (row.Table.Columns.Contains(entEncPregunta.Fields.mostrar_ventana.ToString()))
				obj.mostrar_ventana = GetColumnType(row[entEncPregunta.Fields.mostrar_ventana.ToString()], entEncPregunta.Fields.mostrar_ventana);
			if (row.Table.Columns.Contains(entEncPregunta.Fields.variable.ToString()))
				obj.variable = GetColumnType(row[entEncPregunta.Fields.variable.ToString()], entEncPregunta.Fields.variable);
			if (row.Table.Columns.Contains(entEncPregunta.Fields.formula.ToString()))
				obj.formula = GetColumnType(row[entEncPregunta.Fields.formula.ToString()], entEncPregunta.Fields.formula);
			if (row.Table.Columns.Contains(entEncPregunta.Fields.rpn_formula.ToString()))
				obj.rpn_formula = GetColumnType(row[entEncPregunta.Fields.rpn_formula.ToString()], entEncPregunta.Fields.rpn_formula);
			if (row.Table.Columns.Contains(entEncPregunta.Fields.regla.ToString()))
				obj.regla = GetColumnType(row[entEncPregunta.Fields.regla.ToString()], entEncPregunta.Fields.regla);
			if (row.Table.Columns.Contains(entEncPregunta.Fields.rpn.ToString()))
				obj.rpn = GetColumnType(row[entEncPregunta.Fields.rpn.ToString()], entEncPregunta.Fields.rpn);
			if (row.Table.Columns.Contains(entEncPregunta.Fields.mensaje.ToString()))
				obj.mensaje = GetColumnType(row[entEncPregunta.Fields.mensaje.ToString()], entEncPregunta.Fields.mensaje);
			if (row.Table.Columns.Contains(entEncPregunta.Fields.revision.ToString()))
				obj.revision = GetColumnType(row[entEncPregunta.Fields.revision.ToString()], entEncPregunta.Fields.revision);
			if (row.Table.Columns.Contains(entEncPregunta.Fields.apiestado.ToString()))
				obj.apiestado = GetColumnType(row[entEncPregunta.Fields.apiestado.ToString()], entEncPregunta.Fields.apiestado);
			if (row.Table.Columns.Contains(entEncPregunta.Fields.usucre.ToString()))
				obj.usucre = GetColumnType(row[entEncPregunta.Fields.usucre.ToString()], entEncPregunta.Fields.usucre);
			if (row.Table.Columns.Contains(entEncPregunta.Fields.feccre.ToString()))
				obj.feccre = GetColumnType(row[entEncPregunta.Fields.feccre.ToString()], entEncPregunta.Fields.feccre);
			if (row.Table.Columns.Contains(entEncPregunta.Fields.usumod.ToString()))
				obj.usumod = GetColumnType(row[entEncPregunta.Fields.usumod.ToString()], entEncPregunta.Fields.usumod);
			if (row.Table.Columns.Contains(entEncPregunta.Fields.fecmod.ToString()))
				obj.fecmod = GetColumnType(row[entEncPregunta.Fields.fecmod.ToString()], entEncPregunta.Fields.fecmod);
			if (row.Table.Columns.Contains(entEncPregunta.Fields.instruccion.ToString()))
				obj.instruccion = GetColumnType(row[entEncPregunta.Fields.instruccion.ToString()], entEncPregunta.Fields.instruccion);
			if (row.Table.Columns.Contains(entEncPregunta.Fields.bucle.ToString()))
				obj.bucle = GetColumnType(row[entEncPregunta.Fields.bucle.ToString()], entEncPregunta.Fields.bucle);
			if (row.Table.Columns.Contains(entEncPregunta.Fields.variable_bucle.ToString()))
				obj.variable_bucle = GetColumnType(row[entEncPregunta.Fields.variable_bucle.ToString()], entEncPregunta.Fields.variable_bucle);
			if (row.Table.Columns.Contains(entEncPregunta.Fields.codigo_especial.ToString()))
				obj.codigo_especial = GetColumnType(row[entEncPregunta.Fields.codigo_especial.ToString()], entEncPregunta.Fields.codigo_especial);
			return obj;
		}

		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtenc_pregunta" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Lista de Objetos enc_pregunta
		/// </returns>
		internal List<entEncPregunta> crearLista(DataTable dtenc_pregunta)
		{
			List<entEncPregunta> list = new List<entEncPregunta>();
			
			entEncPregunta obj = new entEncPregunta();
			foreach (DataRow row in dtenc_pregunta.Rows)
			{
				obj = crearObjeto(row);
				list.Add(obj);
			}
			return list;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable y con solo algunas columnas
		/// </summary>
		/// <param name="dtenc_pregunta" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Lista de Objetos enc_pregunta
		/// </returns>
		internal List<entEncPregunta> crearListaRevisada(DataTable dtenc_pregunta)
		{
			List<entEncPregunta> list = new List<entEncPregunta>();
			
			entEncPregunta obj = new entEncPregunta();
			foreach (DataRow row in dtenc_pregunta.Rows)
			{
				obj = crearObjetoRevisado(row);
				list.Add(obj);
			}
			return list;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtenc_pregunta" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cola de Objetos enc_pregunta
		/// </returns>
		internal Queue<entEncPregunta> crearCola(DataTable dtenc_pregunta)
		{
			Queue<entEncPregunta> cola = new Queue<entEncPregunta>();
			
			entEncPregunta obj = new entEncPregunta();
			foreach (DataRow row in dtenc_pregunta.Rows)
			{
				obj = crearObjeto(row);
				cola.Enqueue(obj);
			}
			return cola;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtenc_pregunta" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Pila de Objetos enc_pregunta
		/// </returns>
		internal Stack<entEncPregunta> crearPila(DataTable dtenc_pregunta)
		{
			Stack<entEncPregunta> pila = new Stack<entEncPregunta>();
			
			entEncPregunta obj = new entEncPregunta();
			foreach (DataRow row in dtenc_pregunta.Rows)
			{
				obj = crearObjeto(row);
				pila.Push(obj);
			}
			return pila;
		}
		
		/// <summary>
		/// 	 Funcion que crea un Dicionario a partir de un DataTable
		/// </summary>
		/// <param name="dtenc_pregunta" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Diccionario de Objetos enc_pregunta
		/// </returns>
		internal Dictionary<String, entEncPregunta> crearDiccionario(DataTable dtenc_pregunta)
		{
			Dictionary<String, entEncPregunta>  miDic = new Dictionary<String, entEncPregunta>();
			
			entEncPregunta obj = new entEncPregunta();
			foreach (DataRow row in dtenc_pregunta.Rows)
			{
				obj = crearObjeto(row);
				miDic.Add(obj.id_pregunta.ToString(), obj);
			}
			return miDic;
		}

        internal Dictionary<String, entEncPregunta> crearDiccionario(DataTable dtenc_pregunta, entEncPregunta.Fields dicKey)
        {
            Dictionary<String, entEncPregunta> miDic = new Dictionary<String, entEncPregunta>();

            entEncPregunta obj = new entEncPregunta();
            foreach (DataRow row in dtenc_pregunta.Rows)
            {
                obj = crearObjeto(row);

                var nameOfProperty = dicKey.ToString();
                var propertyInfo = obj.GetType().GetProperty(nameOfProperty);
                var value = propertyInfo.GetValue(obj, null);

                miDic.Add(value.ToString(), obj);
            }
            return miDic;
        }

        /// <summary>
        /// 	 Funcion que crea un Dicionario a partir de un DataTable
        /// </summary>
        /// <param name="dtenc_pregunta" type="System.Data.DateTable">
        ///     <para>
        /// 		 DataTable con el conjunto de Datos recuperados 
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 HashTable de Objetos enc_pregunta
        /// </returns>
        internal Hashtable crearHashTable(DataTable dtenc_pregunta)
		{
			Hashtable miTabla = new Hashtable();
			
			entEncPregunta obj = new entEncPregunta();
			foreach (DataRow row in dtenc_pregunta.Rows)
			{
				obj = crearObjeto(row);
				miTabla.Add(obj.id_pregunta.ToString(), obj);
			}
			return miTabla;
		}
		
		/// <summary>
		/// 	 Funcion que crea un Dicionario a partir de un DataTable y solo con columnas existentes
		/// </summary>
		/// <param name="dtenc_pregunta" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Diccionario de Objetos enc_pregunta
		/// </returns>
		internal Dictionary<String, entEncPregunta> crearDiccionarioRevisado(DataTable dtenc_pregunta)
		{
			Dictionary<String, entEncPregunta>  miDic = new Dictionary<String, entEncPregunta>();
			
			entEncPregunta obj = new entEncPregunta();
			foreach (DataRow row in dtenc_pregunta.Rows)
			{
				obj = crearObjetoRevisado(row);
				miDic.Add(obj.id_pregunta.ToString(), obj);
			}
			return miDic;
		}
		
		public void Dispose()
		{
			GC.SuppressFinalize(this);
		}
		
		protected void Finalize()
		{
			Dispose();
		}
		#endregion

	}
}

