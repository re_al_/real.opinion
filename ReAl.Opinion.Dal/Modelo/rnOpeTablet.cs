#region 
/***********************************************************************************************************
	NOMBRE:       rnOpeTablet
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla ope_tablet

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using ReAl.Opinion.Dal;
using ReAl.Opinion.PgConn;
using ReAl.Opinion.Dal.Entidades;
using ReAl.Opinion.Dal.Interface;
using System.Windows.Forms;
#endregion

namespace ReAl.Opinion.Dal.Modelo
{
	public partial class rnOpeTablet: inOpeTablet
	{
		//Debe implementar la Interface (Alt + Shift + F10)

		#region inOpeTablet Members

		#region Reflection

		/// <summary>
		/// Metodo que devuelve el Script SQL de la Tabla
		/// </summary>
		/// <returns>Script SQL</returns>
		public string getTableScript()
		{
			TableClass tabla = new TableClass(typeof(entOpeTablet));
			return tabla.CreateTableScript();
		}
		
		/// <summary>
		/// Metodo para castear Dinamicamente un Tipo
		/// <summary>
		/// <param name="valor">Tipo a ser casteado</param>
		/// <param name="myField">Enum de la columna</param>
		/// <returns>Devuelve un objeto del Tipo de la columna especificada en el Enum</returns>
		public dynamic GetColumnType(object valor, entOpeTablet.Fields myField)
		{
			if (DBNull.Value.Equals(valor)) 
				return null;
			Type destino = typeof(entOpeTablet).GetProperty(myField.ToString()).PropertyType;
			var miTipo = Nullable.GetUnderlyingType(destino) ?? destino;
			return Convert.ChangeType(valor, miTipo);
		}

		/// <summary>
		/// 	 Funcion que obtiene la llave primaria unica de la tabla ope_tablet a partir de una cadena
		/// </summary>
		/// <param name="args" type="string[]">
		///     <para>
		/// 		 Cadena desde la que se construye el identificador unico de la tabla ope_tablet
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Identificador unico de la tabla ope_tablet
		/// </returns>
		public string CreatePK(string[] args)
		{
			return args[0];
		}
		
		#endregion 

		#region ObtenerObjeto

		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo entOpeTablet que cumple con los filtros de los parametros
		/// </returns>
		public entOpeTablet ObtenerObjeto(int intid_tablet)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(entOpeTablet.Fields.id_tablet.ToString());
		
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(intid_tablet);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeTablet que cumple con los filtros de los parametros
		/// </returns>
		public entOpeTablet ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeTablet que cumple con los filtros de los parametros
		/// </returns>
		public entOpeTablet ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeTablet.Fields.id_tablet.ToString());
				arrColumnas.Add(entOpeTablet.Fields.imei.ToString());
				arrColumnas.Add(entOpeTablet.Fields.marca.ToString());
				arrColumnas.Add(entOpeTablet.Fields.modelo.ToString());
				arrColumnas.Add(entOpeTablet.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usucre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.feccre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usumod.ToString());
				arrColumnas.Add(entOpeTablet.Fields.fecmod.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entOpeTablet.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count == 1)
				{
					entOpeTablet obj = new entOpeTablet();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeTablet que cumple con los filtros de los parametros
		/// </returns>
		public entOpeTablet ObtenerObjeto(Hashtable htbFiltro)
		{
			return ObtenerObjeto(htbFiltro, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeTablet que cumple con los filtros de los parametros
		/// </returns>
		public entOpeTablet ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeTablet.Fields.id_tablet.ToString());
				arrColumnas.Add(entOpeTablet.Fields.imei.ToString());
				arrColumnas.Add(entOpeTablet.Fields.marca.ToString());
				arrColumnas.Add(entOpeTablet.Fields.modelo.ToString());
				arrColumnas.Add(entOpeTablet.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usucre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.feccre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usumod.ToString());
				arrColumnas.Add(entOpeTablet.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entOpeTablet.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count == 1)
				{
					entOpeTablet obj = new entOpeTablet();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeTablet que cumple con los filtros de los parametros
		/// </returns>
		public entOpeTablet ObtenerObjeto(entOpeTablet.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeTablet que cumple con los filtros de los parametros
		/// </returns>
		public entOpeTablet ObtenerObjeto(entOpeTablet.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un Business Object del Tipo entOpeTablet a partir de su llave promaria
		/// </summary>
		/// <returns>
		/// 	Objeto del Tipo entOpeTablet
		/// </returns>
		public entOpeTablet ObtenerObjeto(int intid_tablet, ref cTrans localTrans )
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(entOpeTablet.Fields.id_tablet.ToString());
		
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(intid_tablet);
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_tablet
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeTablet que cumple con los filtros de los parametros
		/// </returns>
		public entOpeTablet ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_tablet
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeTablet que cumple con los filtros de los parametros
		/// </returns>
		public entOpeTablet ObtenerObjeto(Hashtable htbFiltro, ref cTrans localTrans)
		{
			return ObtenerObjeto(htbFiltro, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_tablet
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeTablet que cumple con los filtros de los parametros
		/// </returns>
		public entOpeTablet ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales,  ref cTrans localTrans)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_tablet
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeTablet que cumple con los filtros de los parametros
		/// </returns>
		public entOpeTablet ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeTablet.Fields.id_tablet.ToString());
				arrColumnas.Add(entOpeTablet.Fields.imei.ToString());
				arrColumnas.Add(entOpeTablet.Fields.marca.ToString());
				arrColumnas.Add(entOpeTablet.Fields.modelo.ToString());
				arrColumnas.Add(entOpeTablet.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usucre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.feccre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usumod.ToString());
				arrColumnas.Add(entOpeTablet.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entOpeTablet.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count == 1)
				{
					entOpeTablet obj = new entOpeTablet();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_tablet
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeTablet que cumple con los filtros de los parametros
		/// </returns>
		public entOpeTablet ObtenerObjeto(entOpeTablet.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_tablet
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeTablet que cumple con los filtros de los parametros
		/// </returns>
		public entOpeTablet ObtenerObjeto(entOpeTablet.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion

		#region ObtenerLista

		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeTablet> ObtenerLista()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeTablet> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeTablet> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeTablet.Fields.id_tablet.ToString());
				arrColumnas.Add(entOpeTablet.Fields.imei.ToString());
				arrColumnas.Add(entOpeTablet.Fields.marca.ToString());
				arrColumnas.Add(entOpeTablet.Fields.modelo.ToString());
				arrColumnas.Add(entOpeTablet.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usucre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.feccre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usumod.ToString());
				arrColumnas.Add(entOpeTablet.Fields.fecmod.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entOpeTablet.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<entOpeTablet>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeTablet> ObtenerLista(entOpeTablet.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeTablet> ObtenerLista(entOpeTablet.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_tablet
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeTablet> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_tablet
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeTablet> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeTablet.Fields.id_tablet.ToString());
				arrColumnas.Add(entOpeTablet.Fields.imei.ToString());
				arrColumnas.Add(entOpeTablet.Fields.marca.ToString());
				arrColumnas.Add(entOpeTablet.Fields.modelo.ToString());
				arrColumnas.Add(entOpeTablet.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usucre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.feccre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usumod.ToString());
				arrColumnas.Add(entOpeTablet.Fields.fecmod.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entOpeTablet.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<entOpeTablet>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_tablet
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeTablet> ObtenerLista(entOpeTablet.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_tablet
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeTablet> ObtenerLista(entOpeTablet.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeTablet> ObtenerLista(Hashtable htbFiltro)
		{
			return ObtenerLista(htbFiltro, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeTablet> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeTablet.Fields.id_tablet.ToString());
				arrColumnas.Add(entOpeTablet.Fields.imei.ToString());
				arrColumnas.Add(entOpeTablet.Fields.marca.ToString());
				arrColumnas.Add(entOpeTablet.Fields.modelo.ToString());
				arrColumnas.Add(entOpeTablet.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usucre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.feccre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usumod.ToString());
				arrColumnas.Add(entOpeTablet.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entOpeTablet.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<entOpeTablet>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_tablet
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeTablet> ObtenerLista(Hashtable htbFiltro, ref cTrans localTrans)
		{
			return ObtenerLista(htbFiltro, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_tablet
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeTablet> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeTablet.Fields.id_tablet.ToString());
				arrColumnas.Add(entOpeTablet.Fields.imei.ToString());
				arrColumnas.Add(entOpeTablet.Fields.marca.ToString());
				arrColumnas.Add(entOpeTablet.Fields.modelo.ToString());
				arrColumnas.Add(entOpeTablet.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usucre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.feccre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usumod.ToString());
				arrColumnas.Add(entOpeTablet.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entOpeTablet.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<entOpeTablet>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeTablet> ObtenerListaDesdeVista(String strVista)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeTablet> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeTablet> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<entOpeTablet>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeTablet> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro)
		{
			return ObtenerListaDesdeVista(strVista, htbFiltro, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeTablet> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<entOpeTablet>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeTablet> ObtenerListaDesdeVista(String strVista, entOpeTablet.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeTablet> ObtenerListaDesdeVista(String strVista, entOpeTablet.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_tablet
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeTablet> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_tablet
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeTablet> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<entOpeTablet>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_tablet
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeTablet> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref cTrans localTrans)
		{
			return ObtenerListaDesdeVista(strVista, htbFiltro, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_tablet
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeTablet> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<entOpeTablet>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_tablet
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeTablet> ObtenerListaDesdeVista(String strVista, entOpeTablet.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_tablet
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeTablet> ObtenerListaDesdeVista(String strVista, entOpeTablet.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion 

		#region ObtenerCola y Obtener Pila

		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entOpeTablet> ObtenerCola()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entOpeTablet> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entOpeTablet> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeTablet.Fields.id_tablet.ToString());
				arrColumnas.Add(entOpeTablet.Fields.imei.ToString());
				arrColumnas.Add(entOpeTablet.Fields.marca.ToString());
				arrColumnas.Add(entOpeTablet.Fields.modelo.ToString());
				arrColumnas.Add(entOpeTablet.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usucre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.feccre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usumod.ToString());
				arrColumnas.Add(entOpeTablet.Fields.fecmod.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entOpeTablet.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearCola(table);
				}
				else
					return new Queue<entOpeTablet>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entOpeTablet> ObtenerCola(entOpeTablet.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entOpeTablet> ObtenerCola(entOpeTablet.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_tablet
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entOpeTablet> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_tablet
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entOpeTablet> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeTablet.Fields.id_tablet.ToString());
				arrColumnas.Add(entOpeTablet.Fields.imei.ToString());
				arrColumnas.Add(entOpeTablet.Fields.marca.ToString());
				arrColumnas.Add(entOpeTablet.Fields.modelo.ToString());
				arrColumnas.Add(entOpeTablet.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usucre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.feccre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usumod.ToString());
				arrColumnas.Add(entOpeTablet.Fields.fecmod.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entOpeTablet.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearCola(table);
				}
				else
					return new Queue<entOpeTablet>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_tablet
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entOpeTablet> ObtenerCola(entOpeTablet.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_tablet
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entOpeTablet> ObtenerCola(entOpeTablet.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entOpeTablet> ObtenerPila()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entOpeTablet> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entOpeTablet> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeTablet.Fields.id_tablet.ToString());
				arrColumnas.Add(entOpeTablet.Fields.imei.ToString());
				arrColumnas.Add(entOpeTablet.Fields.marca.ToString());
				arrColumnas.Add(entOpeTablet.Fields.modelo.ToString());
				arrColumnas.Add(entOpeTablet.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usucre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.feccre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usumod.ToString());
				arrColumnas.Add(entOpeTablet.Fields.fecmod.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entOpeTablet.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearPila(table);
				}
				else
					return new Stack<entOpeTablet>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entOpeTablet> ObtenerPila(entOpeTablet.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entOpeTablet> ObtenerPila(entOpeTablet.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_tablet
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entOpeTablet> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_tablet
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entOpeTablet> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeTablet.Fields.id_tablet.ToString());
				arrColumnas.Add(entOpeTablet.Fields.imei.ToString());
				arrColumnas.Add(entOpeTablet.Fields.marca.ToString());
				arrColumnas.Add(entOpeTablet.Fields.modelo.ToString());
				arrColumnas.Add(entOpeTablet.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usucre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.feccre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usumod.ToString());
				arrColumnas.Add(entOpeTablet.Fields.fecmod.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entOpeTablet.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearPila(table);
				}
				else
					return new Stack<entOpeTablet>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_tablet
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entOpeTablet> ObtenerPila(entOpeTablet.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_tablet
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entOpeTablet> ObtenerPila(entOpeTablet.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion 

		#region ObtenerDataTable

		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla ope_tablet
		/// </summary>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ope_tablet
		/// </returns>
		public DataTable NuevoDataTable()
		{
			try
			{
				DataTable table = new DataTable ();
				DataColumn dc;
				dc = new DataColumn(entOpeTablet.Fields.id_tablet.ToString(),typeof(entOpeTablet).GetProperty(entOpeTablet.Fields.id_tablet.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entOpeTablet.Fields.imei.ToString(),typeof(entOpeTablet).GetProperty(entOpeTablet.Fields.imei.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entOpeTablet.Fields.marca.ToString(),typeof(entOpeTablet).GetProperty(entOpeTablet.Fields.marca.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entOpeTablet.Fields.modelo.ToString(),typeof(entOpeTablet).GetProperty(entOpeTablet.Fields.modelo.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entOpeTablet.Fields.apiestado.ToString(),typeof(entOpeTablet).GetProperty(entOpeTablet.Fields.apiestado.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entOpeTablet.Fields.usucre.ToString(),typeof(entOpeTablet).GetProperty(entOpeTablet.Fields.usucre.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entOpeTablet.Fields.feccre.ToString(),typeof(entOpeTablet).GetProperty(entOpeTablet.Fields.feccre.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entOpeTablet.Fields.usumod.ToString(),typeof(entOpeTablet).GetProperty(entOpeTablet.Fields.usumod.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entOpeTablet.Fields.fecmod.ToString(),typeof(entOpeTablet).GetProperty(entOpeTablet.Fields.fecmod.ToString()).PropertyType);
				table.Columns.Add(dc);

				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que genera un DataTable con determinadas columnas de una ope_tablet
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ope_tablet
		/// </returns>
		public DataTable NuevoDataTable(ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'2'");
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entOpeTablet.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla ope_tablet
		/// </summary>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ope_tablet
		/// </returns>
		public DataTable ObtenerDataTable()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDataTable(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla y n condicion WHERE ope_tablet
		/// </summary>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ope_tablet
		/// </returns>
		public DataTable ObtenerDataTable(String strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla ope_tablet
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ope_tablet
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla ope_tablet
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ope_tablet
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla ope_tablet
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ope_tablet
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla ope_tablet
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ope_tablet
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro)
		{
			try
			{
				return ObtenerDataTable(arrColumnas, htbFiltro, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla ope_tablet
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ope_tablet
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla ope_tablet
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ope_tablet
		/// </returns>
		public DataTable ObtenerDataTable(Hashtable htbFiltro)
		{
			try
			{
				return ObtenerDataTable(htbFiltro, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla ope_tablet
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ope_tablet
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entOpeTablet.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla ope_tablet
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ope_tablet
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entOpeTablet.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla ope_tablet
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ope_tablet
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeTablet.Fields.id_tablet.ToString());
				arrColumnas.Add(entOpeTablet.Fields.imei.ToString());
				arrColumnas.Add(entOpeTablet.Fields.marca.ToString());
				arrColumnas.Add(entOpeTablet.Fields.modelo.ToString());
				arrColumnas.Add(entOpeTablet.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usucre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.feccre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usumod.ToString());
				arrColumnas.Add(entOpeTablet.Fields.fecmod.ToString());
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla ope_tablet
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ope_tablet
		/// </returns>
		public DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeTablet.Fields.id_tablet.ToString());
				arrColumnas.Add(entOpeTablet.Fields.imei.ToString());
				arrColumnas.Add(entOpeTablet.Fields.marca.ToString());
				arrColumnas.Add(entOpeTablet.Fields.modelo.ToString());
				arrColumnas.Add(entOpeTablet.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usucre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.feccre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usumod.ToString());
				arrColumnas.Add(entOpeTablet.Fields.fecmod.ToString());
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla ope_tablet
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ope_tablet
		/// </returns>
		public DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeTablet.Fields.id_tablet.ToString());
				arrColumnas.Add(entOpeTablet.Fields.imei.ToString());
				arrColumnas.Add(entOpeTablet.Fields.marca.ToString());
				arrColumnas.Add(entOpeTablet.Fields.modelo.ToString());
				arrColumnas.Add(entOpeTablet.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usucre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.feccre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usumod.ToString());
				arrColumnas.Add(entOpeTablet.Fields.fecmod.ToString());
				return ObtenerDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla ope_tablet
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ope_tablet
		/// </returns>
		public DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return ObtenerDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla ope_tablet
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ope_tablet
		/// </returns>
		public DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DataTable table = local.cargarDataTableOr(cParametros.schema + entOpeTablet.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="searchField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(entOpeTablet.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="searchField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(entOpeTablet.Fields searchField, object searchValue, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, entOpeTablet.Fields searchField, object searchValue, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, entOpeTablet.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		

		#endregion 

		#region ObtenerDiccionario

		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entOpeTablet> ObtenerDiccionario()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entOpeTablet> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entOpeTablet> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeTablet.Fields.id_tablet.ToString());
				arrColumnas.Add(entOpeTablet.Fields.imei.ToString());
				arrColumnas.Add(entOpeTablet.Fields.marca.ToString());
				arrColumnas.Add(entOpeTablet.Fields.modelo.ToString());
				arrColumnas.Add(entOpeTablet.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usucre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.feccre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usumod.ToString());
				arrColumnas.Add(entOpeTablet.Fields.fecmod.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entOpeTablet.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearDiccionario(table);
				}
				else
					return new Dictionary<string, entOpeTablet>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entOpeTablet> ObtenerDiccionario(entOpeTablet.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entOpeTablet> ObtenerDiccionario(entOpeTablet.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_tablet
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entOpeTablet> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_tablet
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entOpeTablet> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeTablet.Fields.id_tablet.ToString());
				arrColumnas.Add(entOpeTablet.Fields.imei.ToString());
				arrColumnas.Add(entOpeTablet.Fields.marca.ToString());
				arrColumnas.Add(entOpeTablet.Fields.modelo.ToString());
				arrColumnas.Add(entOpeTablet.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usucre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.feccre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usumod.ToString());
				arrColumnas.Add(entOpeTablet.Fields.fecmod.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entOpeTablet.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearDiccionario(table);
				}
				else
					return new Dictionary<string, entOpeTablet>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_tablet
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entOpeTablet> ObtenerDiccionario(entOpeTablet.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeTablet a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_tablet
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entOpeTablet> ObtenerDiccionario(entOpeTablet.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion 

		#region FuncionesAgregadas

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeTablet que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesCount(entOpeTablet.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeTablet que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesCount(entOpeTablet.Fields refField, entOpeTablet.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeTablet que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesCount(entOpeTablet.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("count(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeTablet que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMin(entOpeTablet.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeTablet que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMin(entOpeTablet.Fields refField, entOpeTablet.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeTablet que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMin(entOpeTablet.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("min(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeTablet que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMax(entOpeTablet.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeTablet que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMax(entOpeTablet.Fields refField, entOpeTablet.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeTablet que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMax(entOpeTablet.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("max(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeTablet que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesSum(entOpeTablet.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeTablet que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesSum(entOpeTablet.Fields refField, entOpeTablet.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeTablet que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesSum(entOpeTablet.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("sum(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeTablet que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesAvg(entOpeTablet.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeTablet que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesAvg(entOpeTablet.Fields refField, entOpeTablet.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeTablet que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesAvg(entOpeTablet.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("avg(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion 

		#region ABMs

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla ope_tablet a partir de una clase del tipo Eope_tablet
		/// </summary>
		/// <param name="obj" type="Entidades.entOpeTablet">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla ope_tabletope_tablet
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionope_tablet
		/// </returns>
		public bool Insert(entOpeTablet obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(entOpeTablet.Fields.id_tablet.ToString());
				arrColumnas.Add(entOpeTablet.Fields.imei.ToString());
				arrColumnas.Add(entOpeTablet.Fields.marca.ToString());
				arrColumnas.Add(entOpeTablet.Fields.modelo.ToString());
				arrColumnas.Add(entOpeTablet.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usucre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.feccre.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.id_tablet);
				arrValores.Add(obj.imei == null ? null : "'" + obj.imei + "'");
				arrValores.Add(obj.marca == null ? null : "'" + obj.marca + "'");
				arrValores.Add(obj.modelo == null ? null : "'" + obj.modelo + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(cParametros.parFormatoFechaHora) + "'");

			
				cConn local = new cConn();
				return local.insertBD(entOpeTablet.strNombreTabla, arrColumnas, arrValores);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla ope_tablet a partir de una clase del tipo Eope_tablet
		/// </summary>
		/// <param name="obj" type="Entidades.entOpeTablet">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla ope_tabletope_tablet
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_tablet
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionope_tablet
		/// </returns>
		public bool Insert(entOpeTablet obj, ref cTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(entOpeTablet.Fields.id_tablet.ToString());
				arrColumnas.Add(entOpeTablet.Fields.imei.ToString());
				arrColumnas.Add(entOpeTablet.Fields.marca.ToString());
				arrColumnas.Add(entOpeTablet.Fields.modelo.ToString());
				arrColumnas.Add(entOpeTablet.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usucre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.feccre.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.id_tablet);
				arrValores.Add(obj.imei == null ? null : "'" + obj.imei + "'");
				arrValores.Add(obj.marca == null ? null : "'" + obj.marca + "'");
				arrValores.Add(obj.modelo == null ? null : "'" + obj.modelo + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(cParametros.parFormatoFechaHora) + "'");

			
				cConn local = new cConn();
				return local.insertBD(entOpeTablet.strNombreTabla, arrColumnas, arrValores, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla ope_tablet a partir de una clase del tipo Eope_tablet
		/// </summary>
		/// <param name="obj" type="Entidades.entOpeTablet">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla ope_tabletope_tablet
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionope_tablet
		/// </returns>
		public bool InsertIdentity(entOpeTablet obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(entOpeTablet.Fields.id_tablet.ToString());
				arrColumnas.Add(entOpeTablet.Fields.imei.ToString());
				arrColumnas.Add(entOpeTablet.Fields.marca.ToString());
				arrColumnas.Add(entOpeTablet.Fields.modelo.ToString());
				arrColumnas.Add(entOpeTablet.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usucre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.feccre.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.id_tablet);
				arrValores.Add(obj.imei == null ? null : "'" + obj.imei + "'");
				arrValores.Add(obj.marca == null ? null : "'" + obj.marca + "'");
				arrValores.Add(obj.modelo == null ? null : "'" + obj.modelo + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(cParametros.parFormatoFechaHora) + "'");

			
				cConn local = new cConn();
				int intIdentidad = -1;
				bool res = local.insertBD(entOpeTablet.strNombreTabla, arrColumnas, arrValores, ref intIdentidad);
				obj.id_tablet = intIdentidad;
				return res;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla ope_tablet a partir de una clase del tipo Eope_tablet
		/// </summary>
		/// <param name="obj" type="Entidades.entOpeTablet">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla ope_tabletope_tablet
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_tablet
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionope_tablet
		/// </returns>
		public bool InsertIdentity(entOpeTablet obj, ref cTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(entOpeTablet.Fields.id_tablet.ToString());
				arrColumnas.Add(entOpeTablet.Fields.imei.ToString());
				arrColumnas.Add(entOpeTablet.Fields.marca.ToString());
				arrColumnas.Add(entOpeTablet.Fields.modelo.ToString());
				arrColumnas.Add(entOpeTablet.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usucre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.feccre.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.id_tablet);
				arrValores.Add(obj.imei == null ? null : "'" + obj.imei + "'");
				arrValores.Add(obj.marca == null ? null : "'" + obj.marca + "'");
				arrValores.Add(obj.modelo == null ? null : "'" + obj.modelo + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(cParametros.parFormatoFechaHora) + "'");

			
				cConn local = new cConn();
				int intIdentidad = -1;
				bool res = local.insertBD(entOpeTablet.strNombreTabla, arrColumnas, arrValores, ref intIdentidad, ref localTrans);
				obj.id_tablet = intIdentidad;
				return res;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla ope_tablet a partir de una clase del tipo Eope_tablet
		/// </summary>
		/// <param name="obj" type="Entidades.entOpeTablet">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla ope_tablet
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionope_tablet
		/// </returns>
		public int Update(entOpeTablet obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeTablet.Fields.imei.ToString());
				arrColumnas.Add(entOpeTablet.Fields.marca.ToString());
				arrColumnas.Add(entOpeTablet.Fields.modelo.ToString());
				arrColumnas.Add(entOpeTablet.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usucre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.feccre.ToString());
			
				ArrayList arrValores = new ArrayList();
				arrValores.Add(obj.imei == null ? null : "'" + obj.imei + "'");
				arrValores.Add(obj.marca == null ? null : "'" + obj.marca + "'");
				arrValores.Add(obj.modelo == null ? null : "'" + obj.modelo + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(cParametros.parFormatoFechaHora) + "'");

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(entOpeTablet.Fields.id_tablet.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.id_tablet);

			
				cConn local = new cConn();
				return local.updateBD(entOpeTablet.strNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla ope_tablet a partir de una clase del tipo eope_tablet
		/// </summary>
		/// <param name="obj" type="Entidades.entOpeTablet">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla ope_tablet
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_tablet
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacion
		/// </returns>
		public int Update(entOpeTablet obj, ref cTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeTablet.Fields.imei.ToString());
				arrColumnas.Add(entOpeTablet.Fields.marca.ToString());
				arrColumnas.Add(entOpeTablet.Fields.modelo.ToString());
				arrColumnas.Add(entOpeTablet.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usucre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.feccre.ToString());
			
				ArrayList arrValores = new ArrayList();
				arrValores.Add(obj.imei == null ? null : "'" + obj.imei + "'");
				arrValores.Add(obj.marca == null ? null : "'" + obj.marca + "'");
				arrValores.Add(obj.modelo == null ? null : "'" + obj.modelo + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(cParametros.parFormatoFechaHora) + "'");

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(entOpeTablet.Fields.id_tablet.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.id_tablet);

			
				cConn local = new cConn();
				return local.updateBD(entOpeTablet.strNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla ope_tablet a partir de una clase del tipo entOpeTablet y su respectiva PK
		/// </summary>
		/// <param name="obj" type="Entidades.entOpeTablet">
		///     <para>
		/// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla ope_tablet
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionope_tablet
		/// </returns>
		public int Delete(entOpeTablet obj)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(entOpeTablet.Fields.id_tablet.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.id_tablet);

			
				cConn local = new cConn();
				return local.deleteBD(entOpeTablet.strNombreTabla, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla ope_tablet a partir de una clase del tipo entOpeTablet y su PK
		/// </summary>
		/// <param name="obj" type="Entidades.eope_tablet">
		///     <para>
		/// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla ope_tablet
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_tablet
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacionope_tablet
		/// </returns>
		public int Delete(entOpeTablet obj, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(entOpeTablet.Fields.id_tablet.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.id_tablet);

			
				cConn local = new cConn();
				return local.deleteBD(entOpeTablet.strNombreTabla, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla ope_tablet a partir de una clase del tipo eope_tablet
		/// </summary>
		/// <param name="arrColumnasWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Valores para cada una de las columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionope_tablet
		/// </returns>
		public int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				cConn local = new cConn();
				return local.deleteBD("ope_tablet", arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla ope_tablet a partir de una clase del tipo eope_tablet
		/// </summary>
		/// <param name="arrColumnasWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Valores para cada una de las columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_tablet
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacionope_tablet
		/// </returns>
		public int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			try
			{
				cConn local = new cConn();
				return local.deleteBD(entOpeTablet.strNombreTabla, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		

		#endregion 

		#region Llenado de elementos

		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ope_tablet
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla ope_tabletope_tablet
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ope_tablet
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla ope_tabletope_tablet
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ope_tablet
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla ope_tabletope_tablet
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ope_tablet
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla ope_tabletope_tablet
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeTablet.Fields.id_tablet.ToString());
				arrColumnas.Add(entOpeTablet.Fields.imei.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ope_tablet
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ope_tabletope_tablet
		///     </para>
		/// </param>
		/// <param name="valueField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entOpeTablet.Fields valueField, entOpeTablet.Fields textField)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ope_tablet
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ope_tabletope_tablet
		///     </para>
		/// </param>
		/// <param name="valueField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entOpeTablet.Fields valueField, entOpeTablet.Fields textField, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ope_tablet
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ope_tabletope_tablet
		///     </para>
		/// </param>
		/// <param name="valueField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entOpeTablet.Fields valueField, String textField)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ope_tablet
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ope_tabletope_tablet
		///     </para>
		/// </param>
		/// <param name="valueField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entOpeTablet.Fields valueField, String textField, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ope_tablet
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ope_tabletope_tablet
		///     </para>
		/// </param>
		/// <param name="valueField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entOpeTablet.Fields valueField, entOpeTablet.Fields textField, entOpeTablet.Fields searchField, object searchValue)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField.ToString(), searchField, searchValue);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ope_tablet
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ope_tabletope_tablet
		///     </para>
		/// </param>
		/// <param name="valueField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entOpeTablet.Fields valueField, String textField, entOpeTablet.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ope_tablet
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ope_tabletope_tablet
		///     </para>
		/// </param>
		/// <param name="valueField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entOpeTablet.Fields valueField, entOpeTablet.Fields textField, entOpeTablet.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ope_tablet
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ope_tabletope_tablet
		///     </para>
		/// </param>
		/// <param name="valueField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entOpeTablet.Fields valueField, String textField, entOpeTablet.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ope_tablet
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ope_tabletope_tablet
		///     </para>
		/// </param>
		/// <param name="valueField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entOpeTablet.Fields valueField, entOpeTablet.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ope_tablet
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ope_tabletope_tablet
		///     </para>
		/// </param>
		/// <param name="valueField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entOpeTablet.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ope_tablet
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ope_tabletope_tablet
		///     </para>
		/// </param>
		/// <param name="valueField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entOpeTablet.Fields valueField, entOpeTablet.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ope_tablet
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ope_tabletope_tablet
		///     </para>
		/// </param>
		/// <param name="valueField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entOpeTablet.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ope_tablet
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ope_tabletope_tablet
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entOpeTablet.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Columns.Count > 0)
				{
					cmb.ValueMember = table.Columns[0].ColumnName;
					cmb.DisplayMember = table.Columns[1].ColumnName;
					cmb.DataSource = table;

				}
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla ope_tablet
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeTablet.Fields.id_tablet.ToString());
				arrColumnas.Add(entOpeTablet.Fields.imei.ToString());
				arrColumnas.Add(entOpeTablet.Fields.marca.ToString());
				arrColumnas.Add(entOpeTablet.Fields.modelo.ToString());
				arrColumnas.Add(entOpeTablet.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usucre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.feccre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usumod.ToString());
				arrColumnas.Add(entOpeTablet.Fields.fecmod.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla ope_tablet
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		  Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeTablet.Fields.id_tablet.ToString());
				arrColumnas.Add(entOpeTablet.Fields.imei.ToString());
				arrColumnas.Add(entOpeTablet.Fields.marca.ToString());
				arrColumnas.Add(entOpeTablet.Fields.modelo.ToString());
				arrColumnas.Add(entOpeTablet.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usucre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.feccre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usumod.ToString());
				arrColumnas.Add(entOpeTablet.Fields.fecmod.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla ope_tablet
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla ope_tablet
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla ope_tablet
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeTablet.Fields.id_tablet.ToString());
				arrColumnas.Add(entOpeTablet.Fields.imei.ToString());
				arrColumnas.Add(entOpeTablet.Fields.marca.ToString());
				arrColumnas.Add(entOpeTablet.Fields.modelo.ToString());
				arrColumnas.Add(entOpeTablet.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usucre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.feccre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usumod.ToString());
				arrColumnas.Add(entOpeTablet.Fields.fecmod.ToString());
			
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla ope_tablet
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeTablet.Fields.id_tablet.ToString());
				arrColumnas.Add(entOpeTablet.Fields.imei.ToString());
				arrColumnas.Add(entOpeTablet.Fields.marca.ToString());
				arrColumnas.Add(entOpeTablet.Fields.modelo.ToString());
				arrColumnas.Add(entOpeTablet.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usucre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.feccre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usumod.ToString());
				arrColumnas.Add(entOpeTablet.Fields.fecmod.ToString());
			
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla ope_tablet
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla ope_tablet
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DbDataReader dsReader = local.cargarDataReaderAnd(cParametros.schema + entOpeTablet.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				dtg.DataSource = dsReader;

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla ope_tablet
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, entOpeTablet.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeTablet.Fields.id_tablet.ToString());
				arrColumnas.Add(entOpeTablet.Fields.imei.ToString());
				arrColumnas.Add(entOpeTablet.Fields.marca.ToString());
				arrColumnas.Add(entOpeTablet.Fields.modelo.ToString());
				arrColumnas.Add(entOpeTablet.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usucre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.feccre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usumod.ToString());
				arrColumnas.Add(entOpeTablet.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla ope_tablet
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, entOpeTablet.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeTablet.Fields.id_tablet.ToString());
				arrColumnas.Add(entOpeTablet.Fields.imei.ToString());
				arrColumnas.Add(entOpeTablet.Fields.marca.ToString());
				arrColumnas.Add(entOpeTablet.Fields.modelo.ToString());
				arrColumnas.Add(entOpeTablet.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usucre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.feccre.ToString());
				arrColumnas.Add(entOpeTablet.Fields.usumod.ToString());
				arrColumnas.Add(entOpeTablet.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla ope_tablet
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entOpeTablet.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla ope_tablet
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entOpeTablet.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla ope_tablet
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarDataGridOr(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla ope_tablet
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DbDataReader dsReader = local.cargarDataReaderOr(cParametros.schema + entOpeTablet.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				dtg.DataSource = dsReader;

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion 


		#endregion

		#region Funciones Internas

		/// <summary>
		/// 	 Funcion que devuelve un objeto a partir de un DataRow
		/// </summary>
		/// <param name="row" type="System.Data.DataRow">
		///     <para>
		/// 		 DataRow con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Objeto ope_tablet
		/// </returns>
		internal entOpeTablet crearObjeto(DataRow row)
		{
			entOpeTablet obj = new entOpeTablet();
			obj.id_tablet = GetColumnType(row[entOpeTablet.Fields.id_tablet.ToString()], entOpeTablet.Fields.id_tablet);
			obj.imei = GetColumnType(row[entOpeTablet.Fields.imei.ToString()], entOpeTablet.Fields.imei);
			obj.marca = GetColumnType(row[entOpeTablet.Fields.marca.ToString()], entOpeTablet.Fields.marca);
			obj.modelo = GetColumnType(row[entOpeTablet.Fields.modelo.ToString()], entOpeTablet.Fields.modelo);
			obj.apiestado = GetColumnType(row[entOpeTablet.Fields.apiestado.ToString()], entOpeTablet.Fields.apiestado);
			obj.usucre = GetColumnType(row[entOpeTablet.Fields.usucre.ToString()], entOpeTablet.Fields.usucre);
			obj.feccre = GetColumnType(row[entOpeTablet.Fields.feccre.ToString()], entOpeTablet.Fields.feccre);
			obj.usumod = GetColumnType(row[entOpeTablet.Fields.usumod.ToString()], entOpeTablet.Fields.usumod);
			obj.fecmod = GetColumnType(row[entOpeTablet.Fields.fecmod.ToString()], entOpeTablet.Fields.fecmod);
			return obj;
		}

		/// <summary>
		/// 	 Funcion que devuelve un objeto a partir de un DataRow
		/// </summary>
		/// <param name="row" type="System.Data.DataRow">
		///     <para>
		/// 		 DataRow con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Objeto ope_tablet
		/// </returns>
		internal entOpeTablet crearObjetoRevisado(DataRow row)
		{
			entOpeTablet obj = new entOpeTablet();
			if (row.Table.Columns.Contains(entOpeTablet.Fields.id_tablet.ToString()))
				obj.id_tablet = GetColumnType(row[entOpeTablet.Fields.id_tablet.ToString()], entOpeTablet.Fields.id_tablet);
			if (row.Table.Columns.Contains(entOpeTablet.Fields.imei.ToString()))
				obj.imei = GetColumnType(row[entOpeTablet.Fields.imei.ToString()], entOpeTablet.Fields.imei);
			if (row.Table.Columns.Contains(entOpeTablet.Fields.marca.ToString()))
				obj.marca = GetColumnType(row[entOpeTablet.Fields.marca.ToString()], entOpeTablet.Fields.marca);
			if (row.Table.Columns.Contains(entOpeTablet.Fields.modelo.ToString()))
				obj.modelo = GetColumnType(row[entOpeTablet.Fields.modelo.ToString()], entOpeTablet.Fields.modelo);
			if (row.Table.Columns.Contains(entOpeTablet.Fields.apiestado.ToString()))
				obj.apiestado = GetColumnType(row[entOpeTablet.Fields.apiestado.ToString()], entOpeTablet.Fields.apiestado);
			if (row.Table.Columns.Contains(entOpeTablet.Fields.usucre.ToString()))
				obj.usucre = GetColumnType(row[entOpeTablet.Fields.usucre.ToString()], entOpeTablet.Fields.usucre);
			if (row.Table.Columns.Contains(entOpeTablet.Fields.feccre.ToString()))
				obj.feccre = GetColumnType(row[entOpeTablet.Fields.feccre.ToString()], entOpeTablet.Fields.feccre);
			if (row.Table.Columns.Contains(entOpeTablet.Fields.usumod.ToString()))
				obj.usumod = GetColumnType(row[entOpeTablet.Fields.usumod.ToString()], entOpeTablet.Fields.usumod);
			if (row.Table.Columns.Contains(entOpeTablet.Fields.fecmod.ToString()))
				obj.fecmod = GetColumnType(row[entOpeTablet.Fields.fecmod.ToString()], entOpeTablet.Fields.fecmod);
			return obj;
		}

		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtope_tablet" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Lista de Objetos ope_tablet
		/// </returns>
		internal List<entOpeTablet> crearLista(DataTable dtope_tablet)
		{
			List<entOpeTablet> list = new List<entOpeTablet>();
			
			entOpeTablet obj = new entOpeTablet();
			foreach (DataRow row in dtope_tablet.Rows)
			{
				obj = crearObjeto(row);
				list.Add(obj);
			}
			return list;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable y con solo algunas columnas
		/// </summary>
		/// <param name="dtope_tablet" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Lista de Objetos ope_tablet
		/// </returns>
		internal List<entOpeTablet> crearListaRevisada(DataTable dtope_tablet)
		{
			List<entOpeTablet> list = new List<entOpeTablet>();
			
			entOpeTablet obj = new entOpeTablet();
			foreach (DataRow row in dtope_tablet.Rows)
			{
				obj = crearObjetoRevisado(row);
				list.Add(obj);
			}
			return list;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtope_tablet" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cola de Objetos ope_tablet
		/// </returns>
		internal Queue<entOpeTablet> crearCola(DataTable dtope_tablet)
		{
			Queue<entOpeTablet> cola = new Queue<entOpeTablet>();
			
			entOpeTablet obj = new entOpeTablet();
			foreach (DataRow row in dtope_tablet.Rows)
			{
				obj = crearObjeto(row);
				cola.Enqueue(obj);
			}
			return cola;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtope_tablet" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Pila de Objetos ope_tablet
		/// </returns>
		internal Stack<entOpeTablet> crearPila(DataTable dtope_tablet)
		{
			Stack<entOpeTablet> pila = new Stack<entOpeTablet>();
			
			entOpeTablet obj = new entOpeTablet();
			foreach (DataRow row in dtope_tablet.Rows)
			{
				obj = crearObjeto(row);
				pila.Push(obj);
			}
			return pila;
		}
		
		/// <summary>
		/// 	 Funcion que crea un Dicionario a partir de un DataTable
		/// </summary>
		/// <param name="dtope_tablet" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Diccionario de Objetos ope_tablet
		/// </returns>
		internal Dictionary<String, entOpeTablet> crearDiccionario(DataTable dtope_tablet)
		{
			Dictionary<String, entOpeTablet>  miDic = new Dictionary<String, entOpeTablet>();
			
			entOpeTablet obj = new entOpeTablet();
			foreach (DataRow row in dtope_tablet.Rows)
			{
				obj = crearObjeto(row);
				miDic.Add(obj.id_tablet.ToString(), obj);
			}
			return miDic;
		}
		
		/// <summary>
		/// 	 Funcion que crea un Dicionario a partir de un DataTable
		/// </summary>
		/// <param name="dtope_tablet" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 HashTable de Objetos ope_tablet
		/// </returns>
		internal Hashtable crearHashTable(DataTable dtope_tablet)
		{
			Hashtable miTabla = new Hashtable();
			
			entOpeTablet obj = new entOpeTablet();
			foreach (DataRow row in dtope_tablet.Rows)
			{
				obj = crearObjeto(row);
				miTabla.Add(obj.id_tablet.ToString(), obj);
			}
			return miTabla;
		}
		
		/// <summary>
		/// 	 Funcion que crea un Dicionario a partir de un DataTable y solo con columnas existentes
		/// </summary>
		/// <param name="dtope_tablet" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Diccionario de Objetos ope_tablet
		/// </returns>
		internal Dictionary<String, entOpeTablet> crearDiccionarioRevisado(DataTable dtope_tablet)
		{
			Dictionary<String, entOpeTablet>  miDic = new Dictionary<String, entOpeTablet>();
			
			entOpeTablet obj = new entOpeTablet();
			foreach (DataRow row in dtope_tablet.Rows)
			{
				obj = crearObjetoRevisado(row);
				miDic.Add(obj.id_tablet.ToString(), obj);
			}
			return miDic;
		}
		
		public void Dispose()
		{
			GC.SuppressFinalize(this);
		}
		
		protected void Finalize()
		{
			Dispose();
		}
		#endregion

	}
}

