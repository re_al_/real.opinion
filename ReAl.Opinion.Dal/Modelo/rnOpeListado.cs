#region 
/***********************************************************************************************************
	NOMBRE:       rnOpeListado
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla ope_listado

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        24/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using ReAl.Opinion.Dal;
using ReAl.Opinion.PgConn;
using ReAl.Opinion.Dal.Entidades;
using ReAl.Opinion.Dal.Interface;
using System.Windows.Forms;
#endregion

namespace ReAl.Opinion.Dal.Modelo
{
	public partial class rnOpeListado: inOpeListado
	{
		//Debe implementar la Interface (Alt + Shift + F10)

		#region inOpeListado Members

		#region Reflection

		/// <summary>
		/// Metodo que devuelve el Script SQL de la Tabla
		/// </summary>
		/// <returns>Script SQL</returns>
		public string getTableScript()
		{
			TableClass tabla = new TableClass(typeof(entOpeListado));
			return tabla.CreateTableScript();
		}
		
		/// <summary>
		/// Metodo para castear Dinamicamente un Tipo
		/// <summary>
		/// <param name="valor">Tipo a ser casteado</param>
		/// <param name="myField">Enum de la columna</param>
		/// <returns>Devuelve un objeto del Tipo de la columna especificada en el Enum</returns>
		public dynamic GetColumnType(object valor, entOpeListado.Fields myField)
		{
			if (DBNull.Value.Equals(valor)) 
				return null;
			Type destino = typeof(entOpeListado).GetProperty(myField.ToString()).PropertyType;
			var miTipo = Nullable.GetUnderlyingType(destino) ?? destino;
			return Convert.ChangeType(valor, miTipo);
		}

		/// <summary>
		/// 	 Funcion que obtiene la llave primaria unica de la tabla ope_listado a partir de una cadena
		/// </summary>
		/// <param name="args" type="string[]">
		///     <para>
		/// 		 Cadena desde la que se construye el identificador unico de la tabla ope_listado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Identificador unico de la tabla ope_listado
		/// </returns>
		public string CreatePK(string[] args)
		{
			return args[0];
		}
		
		#endregion 

		#region ObtenerObjeto

		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo entOpeListado que cumple con los filtros de los parametros
		/// </returns>
		public entOpeListado ObtenerObjeto(int intid_listado)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(entOpeListado.Fields.id_listado.ToString());
		
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(intid_listado);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeListado que cumple con los filtros de los parametros
		/// </returns>
		public entOpeListado ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeListado que cumple con los filtros de los parametros
		/// </returns>
		public entOpeListado ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeListado.Fields.id_listado.ToString());
				arrColumnas.Add(entOpeListado.Fields.id_upm.ToString());
				arrColumnas.Add(entOpeListado.Fields.codigo_manzana_comunidad.ToString());
				arrColumnas.Add(entOpeListado.Fields.avenida_calle.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_predio.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_puerta.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_orden_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_piso.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_depto.ToString());
				arrColumnas.Add(entOpeListado.Fields.uso_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hogares.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hombres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_mujeres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nombre_jefe.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_voe.ToString());
				arrColumnas.Add(entOpeListado.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeListado.Fields.usucre.ToString());
				arrColumnas.Add(entOpeListado.Fields.feccre.ToString());
				arrColumnas.Add(entOpeListado.Fields.usumod.ToString());
				arrColumnas.Add(entOpeListado.Fields.fecmod.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entOpeListado.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count == 1)
				{
					entOpeListado obj = new entOpeListado();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeListado que cumple con los filtros de los parametros
		/// </returns>
		public entOpeListado ObtenerObjeto(Hashtable htbFiltro)
		{
			return ObtenerObjeto(htbFiltro, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeListado que cumple con los filtros de los parametros
		/// </returns>
		public entOpeListado ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeListado.Fields.id_listado.ToString());
				arrColumnas.Add(entOpeListado.Fields.id_upm.ToString());
				arrColumnas.Add(entOpeListado.Fields.codigo_manzana_comunidad.ToString());
				arrColumnas.Add(entOpeListado.Fields.avenida_calle.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_predio.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_puerta.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_orden_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_piso.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_depto.ToString());
				arrColumnas.Add(entOpeListado.Fields.uso_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hogares.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hombres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_mujeres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nombre_jefe.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_voe.ToString());
				arrColumnas.Add(entOpeListado.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeListado.Fields.usucre.ToString());
				arrColumnas.Add(entOpeListado.Fields.feccre.ToString());
				arrColumnas.Add(entOpeListado.Fields.usumod.ToString());
				arrColumnas.Add(entOpeListado.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entOpeListado.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count == 1)
				{
					entOpeListado obj = new entOpeListado();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeListado que cumple con los filtros de los parametros
		/// </returns>
		public entOpeListado ObtenerObjeto(entOpeListado.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeListado que cumple con los filtros de los parametros
		/// </returns>
		public entOpeListado ObtenerObjeto(entOpeListado.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un Business Object del Tipo entOpeListado a partir de su llave promaria
		/// </summary>
		/// <returns>
		/// 	Objeto del Tipo entOpeListado
		/// </returns>
		public entOpeListado ObtenerObjeto(int intid_listado, ref cTrans localTrans )
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(entOpeListado.Fields.id_listado.ToString());
		
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(intid_listado);
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_listado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeListado que cumple con los filtros de los parametros
		/// </returns>
		public entOpeListado ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_listado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeListado que cumple con los filtros de los parametros
		/// </returns>
		public entOpeListado ObtenerObjeto(Hashtable htbFiltro, ref cTrans localTrans)
		{
			return ObtenerObjeto(htbFiltro, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_listado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeListado que cumple con los filtros de los parametros
		/// </returns>
		public entOpeListado ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales,  ref cTrans localTrans)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_listado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeListado que cumple con los filtros de los parametros
		/// </returns>
		public entOpeListado ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeListado.Fields.id_listado.ToString());
				arrColumnas.Add(entOpeListado.Fields.id_upm.ToString());
				arrColumnas.Add(entOpeListado.Fields.codigo_manzana_comunidad.ToString());
				arrColumnas.Add(entOpeListado.Fields.avenida_calle.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_predio.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_puerta.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_orden_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_piso.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_depto.ToString());
				arrColumnas.Add(entOpeListado.Fields.uso_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hogares.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hombres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_mujeres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nombre_jefe.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_voe.ToString());
				arrColumnas.Add(entOpeListado.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeListado.Fields.usucre.ToString());
				arrColumnas.Add(entOpeListado.Fields.feccre.ToString());
				arrColumnas.Add(entOpeListado.Fields.usumod.ToString());
				arrColumnas.Add(entOpeListado.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entOpeListado.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count == 1)
				{
					entOpeListado obj = new entOpeListado();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_listado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeListado que cumple con los filtros de los parametros
		/// </returns>
		public entOpeListado ObtenerObjeto(entOpeListado.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_listado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeListado que cumple con los filtros de los parametros
		/// </returns>
		public entOpeListado ObtenerObjeto(entOpeListado.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion

		#region ObtenerLista

		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeListado> ObtenerLista()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeListado> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeListado> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeListado.Fields.id_listado.ToString());
				arrColumnas.Add(entOpeListado.Fields.id_upm.ToString());
				arrColumnas.Add(entOpeListado.Fields.codigo_manzana_comunidad.ToString());
				arrColumnas.Add(entOpeListado.Fields.avenida_calle.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_predio.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_puerta.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_orden_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_piso.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_depto.ToString());
				arrColumnas.Add(entOpeListado.Fields.uso_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hogares.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hombres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_mujeres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nombre_jefe.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_voe.ToString());
				arrColumnas.Add(entOpeListado.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeListado.Fields.usucre.ToString());
				arrColumnas.Add(entOpeListado.Fields.feccre.ToString());
				arrColumnas.Add(entOpeListado.Fields.usumod.ToString());
				arrColumnas.Add(entOpeListado.Fields.fecmod.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entOpeListado.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<entOpeListado>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeListado> ObtenerLista(entOpeListado.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeListado> ObtenerLista(entOpeListado.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_listado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeListado> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_listado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeListado> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeListado.Fields.id_listado.ToString());
				arrColumnas.Add(entOpeListado.Fields.id_upm.ToString());
				arrColumnas.Add(entOpeListado.Fields.codigo_manzana_comunidad.ToString());
				arrColumnas.Add(entOpeListado.Fields.avenida_calle.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_predio.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_puerta.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_orden_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_piso.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_depto.ToString());
				arrColumnas.Add(entOpeListado.Fields.uso_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hogares.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hombres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_mujeres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nombre_jefe.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_voe.ToString());
				arrColumnas.Add(entOpeListado.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeListado.Fields.usucre.ToString());
				arrColumnas.Add(entOpeListado.Fields.feccre.ToString());
				arrColumnas.Add(entOpeListado.Fields.usumod.ToString());
				arrColumnas.Add(entOpeListado.Fields.fecmod.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entOpeListado.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<entOpeListado>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_listado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeListado> ObtenerLista(entOpeListado.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_listado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeListado> ObtenerLista(entOpeListado.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeListado> ObtenerLista(Hashtable htbFiltro)
		{
			return ObtenerLista(htbFiltro, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeListado> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeListado.Fields.id_listado.ToString());
				arrColumnas.Add(entOpeListado.Fields.id_upm.ToString());
				arrColumnas.Add(entOpeListado.Fields.codigo_manzana_comunidad.ToString());
				arrColumnas.Add(entOpeListado.Fields.avenida_calle.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_predio.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_puerta.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_orden_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_piso.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_depto.ToString());
				arrColumnas.Add(entOpeListado.Fields.uso_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hogares.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hombres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_mujeres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nombre_jefe.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_voe.ToString());
				arrColumnas.Add(entOpeListado.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeListado.Fields.usucre.ToString());
				arrColumnas.Add(entOpeListado.Fields.feccre.ToString());
				arrColumnas.Add(entOpeListado.Fields.usumod.ToString());
				arrColumnas.Add(entOpeListado.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entOpeListado.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<entOpeListado>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_listado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeListado> ObtenerLista(Hashtable htbFiltro, ref cTrans localTrans)
		{
			return ObtenerLista(htbFiltro, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_listado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeListado> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeListado.Fields.id_listado.ToString());
				arrColumnas.Add(entOpeListado.Fields.id_upm.ToString());
				arrColumnas.Add(entOpeListado.Fields.codigo_manzana_comunidad.ToString());
				arrColumnas.Add(entOpeListado.Fields.avenida_calle.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_predio.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_puerta.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_orden_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_piso.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_depto.ToString());
				arrColumnas.Add(entOpeListado.Fields.uso_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hogares.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hombres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_mujeres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nombre_jefe.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_voe.ToString());
				arrColumnas.Add(entOpeListado.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeListado.Fields.usucre.ToString());
				arrColumnas.Add(entOpeListado.Fields.feccre.ToString());
				arrColumnas.Add(entOpeListado.Fields.usumod.ToString());
				arrColumnas.Add(entOpeListado.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entOpeListado.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<entOpeListado>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeListado> ObtenerListaDesdeVista(String strVista)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeListado> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeListado> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<entOpeListado>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeListado> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro)
		{
			return ObtenerListaDesdeVista(strVista, htbFiltro, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeListado> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<entOpeListado>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeListado> ObtenerListaDesdeVista(String strVista, entOpeListado.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeListado> ObtenerListaDesdeVista(String strVista, entOpeListado.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_listado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeListado> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_listado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeListado> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<entOpeListado>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_listado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeListado> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref cTrans localTrans)
		{
			return ObtenerListaDesdeVista(strVista, htbFiltro, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_listado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeListado> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<entOpeListado>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_listado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeListado> ObtenerListaDesdeVista(String strVista, entOpeListado.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_listado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<entOpeListado> ObtenerListaDesdeVista(String strVista, entOpeListado.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion 

		#region ObtenerCola y Obtener Pila

		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entOpeListado> ObtenerCola()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entOpeListado> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entOpeListado> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeListado.Fields.id_listado.ToString());
				arrColumnas.Add(entOpeListado.Fields.id_upm.ToString());
				arrColumnas.Add(entOpeListado.Fields.codigo_manzana_comunidad.ToString());
				arrColumnas.Add(entOpeListado.Fields.avenida_calle.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_predio.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_puerta.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_orden_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_piso.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_depto.ToString());
				arrColumnas.Add(entOpeListado.Fields.uso_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hogares.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hombres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_mujeres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nombre_jefe.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_voe.ToString());
				arrColumnas.Add(entOpeListado.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeListado.Fields.usucre.ToString());
				arrColumnas.Add(entOpeListado.Fields.feccre.ToString());
				arrColumnas.Add(entOpeListado.Fields.usumod.ToString());
				arrColumnas.Add(entOpeListado.Fields.fecmod.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entOpeListado.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearCola(table);
				}
				else
					return new Queue<entOpeListado>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entOpeListado> ObtenerCola(entOpeListado.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entOpeListado> ObtenerCola(entOpeListado.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_listado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entOpeListado> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_listado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entOpeListado> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeListado.Fields.id_listado.ToString());
				arrColumnas.Add(entOpeListado.Fields.id_upm.ToString());
				arrColumnas.Add(entOpeListado.Fields.codigo_manzana_comunidad.ToString());
				arrColumnas.Add(entOpeListado.Fields.avenida_calle.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_predio.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_puerta.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_orden_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_piso.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_depto.ToString());
				arrColumnas.Add(entOpeListado.Fields.uso_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hogares.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hombres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_mujeres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nombre_jefe.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_voe.ToString());
				arrColumnas.Add(entOpeListado.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeListado.Fields.usucre.ToString());
				arrColumnas.Add(entOpeListado.Fields.feccre.ToString());
				arrColumnas.Add(entOpeListado.Fields.usumod.ToString());
				arrColumnas.Add(entOpeListado.Fields.fecmod.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entOpeListado.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearCola(table);
				}
				else
					return new Queue<entOpeListado>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_listado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entOpeListado> ObtenerCola(entOpeListado.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_listado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entOpeListado> ObtenerCola(entOpeListado.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entOpeListado> ObtenerPila()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entOpeListado> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entOpeListado> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeListado.Fields.id_listado.ToString());
				arrColumnas.Add(entOpeListado.Fields.id_upm.ToString());
				arrColumnas.Add(entOpeListado.Fields.codigo_manzana_comunidad.ToString());
				arrColumnas.Add(entOpeListado.Fields.avenida_calle.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_predio.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_puerta.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_orden_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_piso.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_depto.ToString());
				arrColumnas.Add(entOpeListado.Fields.uso_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hogares.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hombres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_mujeres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nombre_jefe.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_voe.ToString());
				arrColumnas.Add(entOpeListado.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeListado.Fields.usucre.ToString());
				arrColumnas.Add(entOpeListado.Fields.feccre.ToString());
				arrColumnas.Add(entOpeListado.Fields.usumod.ToString());
				arrColumnas.Add(entOpeListado.Fields.fecmod.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entOpeListado.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearPila(table);
				}
				else
					return new Stack<entOpeListado>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entOpeListado> ObtenerPila(entOpeListado.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entOpeListado> ObtenerPila(entOpeListado.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_listado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entOpeListado> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_listado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entOpeListado> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeListado.Fields.id_listado.ToString());
				arrColumnas.Add(entOpeListado.Fields.id_upm.ToString());
				arrColumnas.Add(entOpeListado.Fields.codigo_manzana_comunidad.ToString());
				arrColumnas.Add(entOpeListado.Fields.avenida_calle.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_predio.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_puerta.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_orden_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_piso.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_depto.ToString());
				arrColumnas.Add(entOpeListado.Fields.uso_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hogares.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hombres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_mujeres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nombre_jefe.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_voe.ToString());
				arrColumnas.Add(entOpeListado.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeListado.Fields.usucre.ToString());
				arrColumnas.Add(entOpeListado.Fields.feccre.ToString());
				arrColumnas.Add(entOpeListado.Fields.usumod.ToString());
				arrColumnas.Add(entOpeListado.Fields.fecmod.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entOpeListado.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearPila(table);
				}
				else
					return new Stack<entOpeListado>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_listado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entOpeListado> ObtenerPila(entOpeListado.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_listado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entOpeListado> ObtenerPila(entOpeListado.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion 

		#region ObtenerDataTable

		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla ope_listado
		/// </summary>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ope_listado
		/// </returns>
		public DataTable NuevoDataTable()
		{
			try
			{
				DataTable table = new DataTable ();
				DataColumn dc;
				dc = new DataColumn(entOpeListado.Fields.id_listado.ToString(),typeof(entOpeListado).GetProperty(entOpeListado.Fields.id_listado.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entOpeListado.Fields.id_upm.ToString(),typeof(entOpeListado).GetProperty(entOpeListado.Fields.id_upm.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entOpeListado.Fields.codigo_manzana_comunidad.ToString(),typeof(entOpeListado).GetProperty(entOpeListado.Fields.codigo_manzana_comunidad.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entOpeListado.Fields.avenida_calle.ToString(),typeof(entOpeListado).GetProperty(entOpeListado.Fields.avenida_calle.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entOpeListado.Fields.nro_predio.ToString(),typeof(entOpeListado).GetProperty(entOpeListado.Fields.nro_predio.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entOpeListado.Fields.nro_puerta.ToString(),typeof(entOpeListado).GetProperty(entOpeListado.Fields.nro_puerta.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entOpeListado.Fields.nro_orden_vivienda.ToString(),typeof(entOpeListado).GetProperty(entOpeListado.Fields.nro_orden_vivienda.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entOpeListado.Fields.nro_piso.ToString(),typeof(entOpeListado).GetProperty(entOpeListado.Fields.nro_piso.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entOpeListado.Fields.nro_depto.ToString(),typeof(entOpeListado).GetProperty(entOpeListado.Fields.nro_depto.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entOpeListado.Fields.uso_vivienda.ToString(),typeof(entOpeListado).GetProperty(entOpeListado.Fields.uso_vivienda.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entOpeListado.Fields.nro_hogares.ToString(),typeof(entOpeListado).GetProperty(entOpeListado.Fields.nro_hogares.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entOpeListado.Fields.nro_hombres.ToString(),typeof(entOpeListado).GetProperty(entOpeListado.Fields.nro_hombres.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entOpeListado.Fields.nro_mujeres.ToString(),typeof(entOpeListado).GetProperty(entOpeListado.Fields.nro_mujeres.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entOpeListado.Fields.nombre_jefe.ToString(),typeof(entOpeListado).GetProperty(entOpeListado.Fields.nombre_jefe.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entOpeListado.Fields.nro_voe.ToString(),typeof(entOpeListado).GetProperty(entOpeListado.Fields.nro_voe.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entOpeListado.Fields.apiestado.ToString(),typeof(entOpeListado).GetProperty(entOpeListado.Fields.apiestado.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entOpeListado.Fields.usucre.ToString(),typeof(entOpeListado).GetProperty(entOpeListado.Fields.usucre.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entOpeListado.Fields.feccre.ToString(),typeof(entOpeListado).GetProperty(entOpeListado.Fields.feccre.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entOpeListado.Fields.usumod.ToString(),typeof(entOpeListado).GetProperty(entOpeListado.Fields.usumod.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entOpeListado.Fields.fecmod.ToString(),typeof(entOpeListado).GetProperty(entOpeListado.Fields.fecmod.ToString()).PropertyType);
				table.Columns.Add(dc);

				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que genera un DataTable con determinadas columnas de una ope_listado
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ope_listado
		/// </returns>
		public DataTable NuevoDataTable(ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'2'");
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entOpeListado.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla ope_listado
		/// </summary>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ope_listado
		/// </returns>
		public DataTable ObtenerDataTable()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDataTable(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla y n condicion WHERE ope_listado
		/// </summary>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ope_listado
		/// </returns>
		public DataTable ObtenerDataTable(String strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla ope_listado
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ope_listado
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla ope_listado
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ope_listado
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla ope_listado
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ope_listado
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla ope_listado
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ope_listado
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro)
		{
			try
			{
				return ObtenerDataTable(arrColumnas, htbFiltro, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla ope_listado
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ope_listado
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla ope_listado
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ope_listado
		/// </returns>
		public DataTable ObtenerDataTable(Hashtable htbFiltro)
		{
			try
			{
				return ObtenerDataTable(htbFiltro, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla ope_listado
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ope_listado
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entOpeListado.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla ope_listado
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ope_listado
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entOpeListado.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla ope_listado
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ope_listado
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeListado.Fields.id_listado.ToString());
				arrColumnas.Add(entOpeListado.Fields.id_upm.ToString());
				arrColumnas.Add(entOpeListado.Fields.codigo_manzana_comunidad.ToString());
				arrColumnas.Add(entOpeListado.Fields.avenida_calle.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_predio.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_puerta.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_orden_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_piso.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_depto.ToString());
				arrColumnas.Add(entOpeListado.Fields.uso_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hogares.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hombres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_mujeres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nombre_jefe.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_voe.ToString());
				arrColumnas.Add(entOpeListado.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeListado.Fields.usucre.ToString());
				arrColumnas.Add(entOpeListado.Fields.feccre.ToString());
				arrColumnas.Add(entOpeListado.Fields.usumod.ToString());
				arrColumnas.Add(entOpeListado.Fields.fecmod.ToString());
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla ope_listado
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ope_listado
		/// </returns>
		public DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeListado.Fields.id_listado.ToString());
				arrColumnas.Add(entOpeListado.Fields.id_upm.ToString());
				arrColumnas.Add(entOpeListado.Fields.codigo_manzana_comunidad.ToString());
				arrColumnas.Add(entOpeListado.Fields.avenida_calle.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_predio.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_puerta.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_orden_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_piso.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_depto.ToString());
				arrColumnas.Add(entOpeListado.Fields.uso_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hogares.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hombres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_mujeres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nombre_jefe.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_voe.ToString());
				arrColumnas.Add(entOpeListado.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeListado.Fields.usucre.ToString());
				arrColumnas.Add(entOpeListado.Fields.feccre.ToString());
				arrColumnas.Add(entOpeListado.Fields.usumod.ToString());
				arrColumnas.Add(entOpeListado.Fields.fecmod.ToString());
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla ope_listado
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ope_listado
		/// </returns>
		public DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeListado.Fields.id_listado.ToString());
				arrColumnas.Add(entOpeListado.Fields.id_upm.ToString());
				arrColumnas.Add(entOpeListado.Fields.codigo_manzana_comunidad.ToString());
				arrColumnas.Add(entOpeListado.Fields.avenida_calle.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_predio.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_puerta.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_orden_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_piso.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_depto.ToString());
				arrColumnas.Add(entOpeListado.Fields.uso_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hogares.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hombres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_mujeres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nombre_jefe.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_voe.ToString());
				arrColumnas.Add(entOpeListado.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeListado.Fields.usucre.ToString());
				arrColumnas.Add(entOpeListado.Fields.feccre.ToString());
				arrColumnas.Add(entOpeListado.Fields.usumod.ToString());
				arrColumnas.Add(entOpeListado.Fields.fecmod.ToString());
				return ObtenerDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla ope_listado
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ope_listado
		/// </returns>
		public DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return ObtenerDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla ope_listado
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de ope_listado
		/// </returns>
		public DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DataTable table = local.cargarDataTableOr(cParametros.schema + entOpeListado.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="searchField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(entOpeListado.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="searchField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(entOpeListado.Fields searchField, object searchValue, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, entOpeListado.Fields searchField, object searchValue, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, entOpeListado.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		

		#endregion 

		#region ObtenerDiccionario

		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entOpeListado> ObtenerDiccionario()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entOpeListado> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entOpeListado> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeListado.Fields.id_listado.ToString());
				arrColumnas.Add(entOpeListado.Fields.id_upm.ToString());
				arrColumnas.Add(entOpeListado.Fields.codigo_manzana_comunidad.ToString());
				arrColumnas.Add(entOpeListado.Fields.avenida_calle.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_predio.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_puerta.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_orden_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_piso.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_depto.ToString());
				arrColumnas.Add(entOpeListado.Fields.uso_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hogares.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hombres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_mujeres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nombre_jefe.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_voe.ToString());
				arrColumnas.Add(entOpeListado.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeListado.Fields.usucre.ToString());
				arrColumnas.Add(entOpeListado.Fields.feccre.ToString());
				arrColumnas.Add(entOpeListado.Fields.usumod.ToString());
				arrColumnas.Add(entOpeListado.Fields.fecmod.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entOpeListado.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearDiccionario(table);
				}
				else
					return new Dictionary<string, entOpeListado>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entOpeListado> ObtenerDiccionario(entOpeListado.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entOpeListado> ObtenerDiccionario(entOpeListado.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_listado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entOpeListado> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_listado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entOpeListado> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeListado.Fields.id_listado.ToString());
				arrColumnas.Add(entOpeListado.Fields.id_upm.ToString());
				arrColumnas.Add(entOpeListado.Fields.codigo_manzana_comunidad.ToString());
				arrColumnas.Add(entOpeListado.Fields.avenida_calle.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_predio.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_puerta.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_orden_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_piso.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_depto.ToString());
				arrColumnas.Add(entOpeListado.Fields.uso_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hogares.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hombres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_mujeres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nombre_jefe.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_voe.ToString());
				arrColumnas.Add(entOpeListado.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeListado.Fields.usucre.ToString());
				arrColumnas.Add(entOpeListado.Fields.feccre.ToString());
				arrColumnas.Add(entOpeListado.Fields.usumod.ToString());
				arrColumnas.Add(entOpeListado.Fields.fecmod.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entOpeListado.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearDiccionario(table);
				}
				else
					return new Dictionary<string, entOpeListado>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_listado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entOpeListado> ObtenerDiccionario(entOpeListado.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entOpeListado a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_listado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entOpeListado> ObtenerDiccionario(entOpeListado.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion 

		#region FuncionesAgregadas

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeListado que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesCount(entOpeListado.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeListado que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesCount(entOpeListado.Fields refField, entOpeListado.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeListado que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesCount(entOpeListado.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("count(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeListado que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMin(entOpeListado.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeListado que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMin(entOpeListado.Fields refField, entOpeListado.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeListado que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMin(entOpeListado.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("min(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeListado que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMax(entOpeListado.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeListado que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMax(entOpeListado.Fields refField, entOpeListado.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeListado que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMax(entOpeListado.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("max(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeListado que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesSum(entOpeListado.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeListado que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesSum(entOpeListado.Fields refField, entOpeListado.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeListado que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesSum(entOpeListado.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("sum(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeListado que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesAvg(entOpeListado.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeListado que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesAvg(entOpeListado.Fields refField, entOpeListado.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entOpeListado que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesAvg(entOpeListado.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("avg(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion 

		#region ABMs

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla ope_listado a partir de una clase del tipo Eope_listado
		/// </summary>
		/// <param name="obj" type="Entidades.entOpeListado">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla ope_listadoope_listado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionope_listado
		/// </returns>
		public bool Insert(entOpeListado obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(entOpeListado.Fields.id_listado.ToString());
				arrColumnas.Add(entOpeListado.Fields.id_upm.ToString());
				arrColumnas.Add(entOpeListado.Fields.codigo_manzana_comunidad.ToString());
				arrColumnas.Add(entOpeListado.Fields.avenida_calle.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_predio.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_puerta.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_orden_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_piso.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_depto.ToString());
				arrColumnas.Add(entOpeListado.Fields.uso_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hogares.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hombres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_mujeres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nombre_jefe.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_voe.ToString());
				arrColumnas.Add(entOpeListado.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeListado.Fields.usucre.ToString());
				arrColumnas.Add(entOpeListado.Fields.feccre.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.id_listado);
				arrValores.Add(obj.id_upm);
				arrValores.Add(obj.codigo_manzana_comunidad == null ? null : "'" + obj.codigo_manzana_comunidad + "'");
				arrValores.Add(obj.avenida_calle == null ? null : "'" + obj.avenida_calle + "'");
				arrValores.Add(obj.nro_predio);
				arrValores.Add(obj.nro_puerta == null ? null : "'" + obj.nro_puerta + "'");
				arrValores.Add(obj.nro_orden_vivienda);
				arrValores.Add(obj.nro_piso == null ? null : "'" + obj.nro_piso + "'");
				arrValores.Add(obj.nro_depto == null ? null : "'" + obj.nro_depto + "'");
				arrValores.Add(obj.uso_vivienda == null ? null : "'" + obj.uso_vivienda + "'");
				arrValores.Add(obj.nro_hogares);
				arrValores.Add(obj.nro_hombres);
				arrValores.Add(obj.nro_mujeres);
				arrValores.Add(obj.nombre_jefe == null ? null : "'" + obj.nombre_jefe + "'");
				arrValores.Add(obj.nro_voe);
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(cParametros.parFormatoFechaHora) + "'");

			
				cConn local = new cConn();
				return local.insertBD(entOpeListado.strNombreTabla, arrColumnas, arrValores);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla ope_listado a partir de una clase del tipo Eope_listado
		/// </summary>
		/// <param name="obj" type="Entidades.entOpeListado">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla ope_listadoope_listado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_listado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionope_listado
		/// </returns>
		public bool Insert(entOpeListado obj, ref cTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(entOpeListado.Fields.id_listado.ToString());
				arrColumnas.Add(entOpeListado.Fields.id_upm.ToString());
				arrColumnas.Add(entOpeListado.Fields.codigo_manzana_comunidad.ToString());
				arrColumnas.Add(entOpeListado.Fields.avenida_calle.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_predio.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_puerta.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_orden_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_piso.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_depto.ToString());
				arrColumnas.Add(entOpeListado.Fields.uso_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hogares.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hombres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_mujeres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nombre_jefe.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_voe.ToString());
				arrColumnas.Add(entOpeListado.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeListado.Fields.usucre.ToString());
				arrColumnas.Add(entOpeListado.Fields.feccre.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.id_listado);
				arrValores.Add(obj.id_upm);
				arrValores.Add(obj.codigo_manzana_comunidad == null ? null : "'" + obj.codigo_manzana_comunidad + "'");
				arrValores.Add(obj.avenida_calle == null ? null : "'" + obj.avenida_calle + "'");
				arrValores.Add(obj.nro_predio);
				arrValores.Add(obj.nro_puerta == null ? null : "'" + obj.nro_puerta + "'");
				arrValores.Add(obj.nro_orden_vivienda);
				arrValores.Add(obj.nro_piso == null ? null : "'" + obj.nro_piso + "'");
				arrValores.Add(obj.nro_depto == null ? null : "'" + obj.nro_depto + "'");
				arrValores.Add(obj.uso_vivienda == null ? null : "'" + obj.uso_vivienda + "'");
				arrValores.Add(obj.nro_hogares);
				arrValores.Add(obj.nro_hombres);
				arrValores.Add(obj.nro_mujeres);
				arrValores.Add(obj.nombre_jefe == null ? null : "'" + obj.nombre_jefe + "'");
				arrValores.Add(obj.nro_voe);
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(cParametros.parFormatoFechaHora) + "'");

			
				cConn local = new cConn();
				return local.insertBD(entOpeListado.strNombreTabla, arrColumnas, arrValores, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla ope_listado a partir de una clase del tipo Eope_listado
		/// </summary>
		/// <param name="obj" type="Entidades.entOpeListado">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla ope_listadoope_listado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionope_listado
		/// </returns>
		public bool InsertIdentity(entOpeListado obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(entOpeListado.Fields.id_listado.ToString());
				arrColumnas.Add(entOpeListado.Fields.id_upm.ToString());
				arrColumnas.Add(entOpeListado.Fields.codigo_manzana_comunidad.ToString());
				arrColumnas.Add(entOpeListado.Fields.avenida_calle.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_predio.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_puerta.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_orden_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_piso.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_depto.ToString());
				arrColumnas.Add(entOpeListado.Fields.uso_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hogares.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hombres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_mujeres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nombre_jefe.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_voe.ToString());
				arrColumnas.Add(entOpeListado.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeListado.Fields.usucre.ToString());
				arrColumnas.Add(entOpeListado.Fields.feccre.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.id_listado);
				arrValores.Add(obj.id_upm);
				arrValores.Add(obj.codigo_manzana_comunidad == null ? null : "'" + obj.codigo_manzana_comunidad + "'");
				arrValores.Add(obj.avenida_calle == null ? null : "'" + obj.avenida_calle + "'");
				arrValores.Add(obj.nro_predio);
				arrValores.Add(obj.nro_puerta == null ? null : "'" + obj.nro_puerta + "'");
				arrValores.Add(obj.nro_orden_vivienda);
				arrValores.Add(obj.nro_piso == null ? null : "'" + obj.nro_piso + "'");
				arrValores.Add(obj.nro_depto == null ? null : "'" + obj.nro_depto + "'");
				arrValores.Add(obj.uso_vivienda == null ? null : "'" + obj.uso_vivienda + "'");
				arrValores.Add(obj.nro_hogares);
				arrValores.Add(obj.nro_hombres);
				arrValores.Add(obj.nro_mujeres);
				arrValores.Add(obj.nombre_jefe == null ? null : "'" + obj.nombre_jefe + "'");
				arrValores.Add(obj.nro_voe);
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(cParametros.parFormatoFechaHora) + "'");

			
				cConn local = new cConn();
				int intIdentidad = -1;
				bool res = local.insertBD(entOpeListado.strNombreTabla, arrColumnas, arrValores, ref intIdentidad);
				obj.id_listado = intIdentidad;
				return res;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla ope_listado a partir de una clase del tipo Eope_listado
		/// </summary>
		/// <param name="obj" type="Entidades.entOpeListado">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla ope_listadoope_listado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_listado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionope_listado
		/// </returns>
		public bool InsertIdentity(entOpeListado obj, ref cTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(entOpeListado.Fields.id_listado.ToString());
				arrColumnas.Add(entOpeListado.Fields.id_upm.ToString());
				arrColumnas.Add(entOpeListado.Fields.codigo_manzana_comunidad.ToString());
				arrColumnas.Add(entOpeListado.Fields.avenida_calle.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_predio.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_puerta.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_orden_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_piso.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_depto.ToString());
				arrColumnas.Add(entOpeListado.Fields.uso_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hogares.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hombres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_mujeres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nombre_jefe.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_voe.ToString());
				arrColumnas.Add(entOpeListado.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeListado.Fields.usucre.ToString());
				arrColumnas.Add(entOpeListado.Fields.feccre.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.id_listado);
				arrValores.Add(obj.id_upm);
				arrValores.Add(obj.codigo_manzana_comunidad == null ? null : "'" + obj.codigo_manzana_comunidad + "'");
				arrValores.Add(obj.avenida_calle == null ? null : "'" + obj.avenida_calle + "'");
				arrValores.Add(obj.nro_predio);
				arrValores.Add(obj.nro_puerta == null ? null : "'" + obj.nro_puerta + "'");
				arrValores.Add(obj.nro_orden_vivienda);
				arrValores.Add(obj.nro_piso == null ? null : "'" + obj.nro_piso + "'");
				arrValores.Add(obj.nro_depto == null ? null : "'" + obj.nro_depto + "'");
				arrValores.Add(obj.uso_vivienda == null ? null : "'" + obj.uso_vivienda + "'");
				arrValores.Add(obj.nro_hogares);
				arrValores.Add(obj.nro_hombres);
				arrValores.Add(obj.nro_mujeres);
				arrValores.Add(obj.nombre_jefe == null ? null : "'" + obj.nombre_jefe + "'");
				arrValores.Add(obj.nro_voe);
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(cParametros.parFormatoFechaHora) + "'");

			
				cConn local = new cConn();
				int intIdentidad = -1;
				bool res = local.insertBD(entOpeListado.strNombreTabla, arrColumnas, arrValores, ref intIdentidad, ref localTrans);
				obj.id_listado = intIdentidad;
				return res;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla ope_listado a partir de una clase del tipo Eope_listado
		/// </summary>
		/// <param name="obj" type="Entidades.entOpeListado">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla ope_listado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionope_listado
		/// </returns>
		public int Update(entOpeListado obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeListado.Fields.id_upm.ToString());
				arrColumnas.Add(entOpeListado.Fields.codigo_manzana_comunidad.ToString());
				arrColumnas.Add(entOpeListado.Fields.avenida_calle.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_predio.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_puerta.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_orden_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_piso.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_depto.ToString());
				arrColumnas.Add(entOpeListado.Fields.uso_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hogares.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hombres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_mujeres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nombre_jefe.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_voe.ToString());
				arrColumnas.Add(entOpeListado.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeListado.Fields.usumod.ToString());
				arrColumnas.Add(entOpeListado.Fields.fecmod.ToString());
			
				ArrayList arrValores = new ArrayList();
				arrValores.Add(obj.id_upm);
				arrValores.Add(obj.codigo_manzana_comunidad == null ? null : "'" + obj.codigo_manzana_comunidad + "'");
				arrValores.Add(obj.avenida_calle == null ? null : "'" + obj.avenida_calle + "'");
				arrValores.Add(obj.nro_predio);
				arrValores.Add(obj.nro_puerta == null ? null : "'" + obj.nro_puerta + "'");
				arrValores.Add(obj.nro_orden_vivienda);
				arrValores.Add(obj.nro_piso == null ? null : "'" + obj.nro_piso + "'");
				arrValores.Add(obj.nro_depto == null ? null : "'" + obj.nro_depto + "'");
				arrValores.Add(obj.uso_vivienda == null ? null : "'" + obj.uso_vivienda + "'");
				arrValores.Add(obj.nro_hogares);
				arrValores.Add(obj.nro_hombres);
				arrValores.Add(obj.nro_mujeres);
				arrValores.Add(obj.nombre_jefe == null ? null : "'" + obj.nombre_jefe + "'");
				arrValores.Add(obj.nro_voe);
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");
				arrValores.Add(obj.fecmod == null ? null : "'" + Convert.ToDateTime(obj.fecmod).ToString(cParametros.parFormatoFechaHora) + "'");

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(entOpeListado.Fields.id_listado.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.id_listado);

			
				cConn local = new cConn();
				return local.updateBD(entOpeListado.strNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla ope_listado a partir de una clase del tipo eope_listado
		/// </summary>
		/// <param name="obj" type="Entidades.entOpeListado">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla ope_listado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_listado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacion
		/// </returns>
		public int Update(entOpeListado obj, ref cTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeListado.Fields.id_upm.ToString());
				arrColumnas.Add(entOpeListado.Fields.codigo_manzana_comunidad.ToString());
				arrColumnas.Add(entOpeListado.Fields.avenida_calle.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_predio.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_puerta.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_orden_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_piso.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_depto.ToString());
				arrColumnas.Add(entOpeListado.Fields.uso_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hogares.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hombres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_mujeres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nombre_jefe.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_voe.ToString());
				arrColumnas.Add(entOpeListado.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeListado.Fields.usumod.ToString());
				arrColumnas.Add(entOpeListado.Fields.fecmod.ToString());
			
				ArrayList arrValores = new ArrayList();
				arrValores.Add(obj.id_upm);
				arrValores.Add(obj.codigo_manzana_comunidad == null ? null : "'" + obj.codigo_manzana_comunidad + "'");
				arrValores.Add(obj.avenida_calle == null ? null : "'" + obj.avenida_calle + "'");
				arrValores.Add(obj.nro_predio);
				arrValores.Add(obj.nro_puerta == null ? null : "'" + obj.nro_puerta + "'");
				arrValores.Add(obj.nro_orden_vivienda);
				arrValores.Add(obj.nro_piso == null ? null : "'" + obj.nro_piso + "'");
				arrValores.Add(obj.nro_depto == null ? null : "'" + obj.nro_depto + "'");
				arrValores.Add(obj.uso_vivienda == null ? null : "'" + obj.uso_vivienda + "'");
				arrValores.Add(obj.nro_hogares);
				arrValores.Add(obj.nro_hombres);
				arrValores.Add(obj.nro_mujeres);
				arrValores.Add(obj.nombre_jefe == null ? null : "'" + obj.nombre_jefe + "'");
				arrValores.Add(obj.nro_voe);
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");
				arrValores.Add(obj.fecmod == null ? null : "'" + Convert.ToDateTime(obj.fecmod).ToString(cParametros.parFormatoFechaHora) + "'");

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(entOpeListado.Fields.id_listado.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.id_listado);

			
				cConn local = new cConn();
				return local.updateBD(entOpeListado.strNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla ope_listado a partir de una clase del tipo entOpeListado y su respectiva PK
		/// </summary>
		/// <param name="obj" type="Entidades.entOpeListado">
		///     <para>
		/// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla ope_listado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionope_listado
		/// </returns>
		public int Delete(entOpeListado obj)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(entOpeListado.Fields.id_listado.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.id_listado);

			
				cConn local = new cConn();
				return local.deleteBD(entOpeListado.strNombreTabla, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla ope_listado a partir de una clase del tipo entOpeListado y su PK
		/// </summary>
		/// <param name="obj" type="Entidades.eope_listado">
		///     <para>
		/// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla ope_listado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_listado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacionope_listado
		/// </returns>
		public int Delete(entOpeListado obj, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(entOpeListado.Fields.id_listado.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.id_listado);

			
				cConn local = new cConn();
				return local.deleteBD(entOpeListado.strNombreTabla, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla ope_listado a partir de una clase del tipo eope_listado
		/// </summary>
		/// <param name="arrColumnasWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Valores para cada una de las columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionope_listado
		/// </returns>
		public int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				cConn local = new cConn();
				return local.deleteBD("ope_listado", arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla ope_listado a partir de una clase del tipo eope_listado
		/// </summary>
		/// <param name="arrColumnasWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Valores para cada una de las columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion ope_listado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacionope_listado
		/// </returns>
		public int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			try
			{
				cConn local = new cConn();
				return local.deleteBD(entOpeListado.strNombreTabla, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		

		#endregion 

		#region Llenado de elementos

		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ope_listado
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla ope_listadoope_listado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ope_listado
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla ope_listadoope_listado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ope_listado
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla ope_listadoope_listado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ope_listado
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla ope_listadoope_listado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeListado.Fields.id_listado.ToString());
				arrColumnas.Add(entOpeListado.Fields.id_upm.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ope_listado
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ope_listadoope_listado
		///     </para>
		/// </param>
		/// <param name="valueField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entOpeListado.Fields valueField, entOpeListado.Fields textField)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ope_listado
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ope_listadoope_listado
		///     </para>
		/// </param>
		/// <param name="valueField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entOpeListado.Fields valueField, entOpeListado.Fields textField, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ope_listado
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ope_listadoope_listado
		///     </para>
		/// </param>
		/// <param name="valueField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entOpeListado.Fields valueField, String textField)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ope_listado
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ope_listadoope_listado
		///     </para>
		/// </param>
		/// <param name="valueField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entOpeListado.Fields valueField, String textField, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ope_listado
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ope_listadoope_listado
		///     </para>
		/// </param>
		/// <param name="valueField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entOpeListado.Fields valueField, entOpeListado.Fields textField, entOpeListado.Fields searchField, object searchValue)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField.ToString(), searchField, searchValue);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ope_listado
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ope_listadoope_listado
		///     </para>
		/// </param>
		/// <param name="valueField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entOpeListado.Fields valueField, String textField, entOpeListado.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ope_listado
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ope_listadoope_listado
		///     </para>
		/// </param>
		/// <param name="valueField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entOpeListado.Fields valueField, entOpeListado.Fields textField, entOpeListado.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ope_listado
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ope_listadoope_listado
		///     </para>
		/// </param>
		/// <param name="valueField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entOpeListado.Fields valueField, String textField, entOpeListado.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ope_listado
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ope_listadoope_listado
		///     </para>
		/// </param>
		/// <param name="valueField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entOpeListado.Fields valueField, entOpeListado.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ope_listado
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ope_listadoope_listado
		///     </para>
		/// </param>
		/// <param name="valueField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entOpeListado.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ope_listado
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ope_listadoope_listado
		///     </para>
		/// </param>
		/// <param name="valueField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entOpeListado.Fields valueField, entOpeListado.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ope_listado
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ope_listadoope_listado
		///     </para>
		/// </param>
		/// <param name="valueField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entOpeListado.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla ope_listado
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ope_listadoope_listado
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entOpeListado.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Columns.Count > 0)
				{
					cmb.ValueMember = table.Columns[0].ColumnName;
					cmb.DisplayMember = table.Columns[1].ColumnName;
					cmb.DataSource = table;

				}
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla ope_listado
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeListado.Fields.id_listado.ToString());
				arrColumnas.Add(entOpeListado.Fields.id_upm.ToString());
				arrColumnas.Add(entOpeListado.Fields.codigo_manzana_comunidad.ToString());
				arrColumnas.Add(entOpeListado.Fields.avenida_calle.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_predio.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_puerta.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_orden_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_piso.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_depto.ToString());
				arrColumnas.Add(entOpeListado.Fields.uso_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hogares.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hombres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_mujeres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nombre_jefe.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_voe.ToString());
				arrColumnas.Add(entOpeListado.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeListado.Fields.usucre.ToString());
				arrColumnas.Add(entOpeListado.Fields.feccre.ToString());
				arrColumnas.Add(entOpeListado.Fields.usumod.ToString());
				arrColumnas.Add(entOpeListado.Fields.fecmod.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla ope_listado
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		  Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeListado.Fields.id_listado.ToString());
				arrColumnas.Add(entOpeListado.Fields.id_upm.ToString());
				arrColumnas.Add(entOpeListado.Fields.codigo_manzana_comunidad.ToString());
				arrColumnas.Add(entOpeListado.Fields.avenida_calle.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_predio.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_puerta.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_orden_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_piso.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_depto.ToString());
				arrColumnas.Add(entOpeListado.Fields.uso_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hogares.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hombres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_mujeres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nombre_jefe.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_voe.ToString());
				arrColumnas.Add(entOpeListado.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeListado.Fields.usucre.ToString());
				arrColumnas.Add(entOpeListado.Fields.feccre.ToString());
				arrColumnas.Add(entOpeListado.Fields.usumod.ToString());
				arrColumnas.Add(entOpeListado.Fields.fecmod.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla ope_listado
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla ope_listado
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla ope_listado
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeListado.Fields.id_listado.ToString());
				arrColumnas.Add(entOpeListado.Fields.id_upm.ToString());
				arrColumnas.Add(entOpeListado.Fields.codigo_manzana_comunidad.ToString());
				arrColumnas.Add(entOpeListado.Fields.avenida_calle.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_predio.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_puerta.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_orden_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_piso.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_depto.ToString());
				arrColumnas.Add(entOpeListado.Fields.uso_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hogares.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hombres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_mujeres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nombre_jefe.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_voe.ToString());
				arrColumnas.Add(entOpeListado.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeListado.Fields.usucre.ToString());
				arrColumnas.Add(entOpeListado.Fields.feccre.ToString());
				arrColumnas.Add(entOpeListado.Fields.usumod.ToString());
				arrColumnas.Add(entOpeListado.Fields.fecmod.ToString());
			
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla ope_listado
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeListado.Fields.id_listado.ToString());
				arrColumnas.Add(entOpeListado.Fields.id_upm.ToString());
				arrColumnas.Add(entOpeListado.Fields.codigo_manzana_comunidad.ToString());
				arrColumnas.Add(entOpeListado.Fields.avenida_calle.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_predio.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_puerta.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_orden_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_piso.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_depto.ToString());
				arrColumnas.Add(entOpeListado.Fields.uso_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hogares.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hombres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_mujeres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nombre_jefe.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_voe.ToString());
				arrColumnas.Add(entOpeListado.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeListado.Fields.usucre.ToString());
				arrColumnas.Add(entOpeListado.Fields.feccre.ToString());
				arrColumnas.Add(entOpeListado.Fields.usumod.ToString());
				arrColumnas.Add(entOpeListado.Fields.fecmod.ToString());
			
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla ope_listado
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla ope_listado
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DbDataReader dsReader = local.cargarDataReaderAnd(cParametros.schema + entOpeListado.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				dtg.DataSource = dsReader;

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla ope_listado
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, entOpeListado.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeListado.Fields.id_listado.ToString());
				arrColumnas.Add(entOpeListado.Fields.id_upm.ToString());
				arrColumnas.Add(entOpeListado.Fields.codigo_manzana_comunidad.ToString());
				arrColumnas.Add(entOpeListado.Fields.avenida_calle.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_predio.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_puerta.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_orden_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_piso.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_depto.ToString());
				arrColumnas.Add(entOpeListado.Fields.uso_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hogares.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hombres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_mujeres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nombre_jefe.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_voe.ToString());
				arrColumnas.Add(entOpeListado.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeListado.Fields.usucre.ToString());
				arrColumnas.Add(entOpeListado.Fields.feccre.ToString());
				arrColumnas.Add(entOpeListado.Fields.usumod.ToString());
				arrColumnas.Add(entOpeListado.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla ope_listado
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, entOpeListado.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entOpeListado.Fields.id_listado.ToString());
				arrColumnas.Add(entOpeListado.Fields.id_upm.ToString());
				arrColumnas.Add(entOpeListado.Fields.codigo_manzana_comunidad.ToString());
				arrColumnas.Add(entOpeListado.Fields.avenida_calle.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_predio.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_puerta.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_orden_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_piso.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_depto.ToString());
				arrColumnas.Add(entOpeListado.Fields.uso_vivienda.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hogares.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_hombres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_mujeres.ToString());
				arrColumnas.Add(entOpeListado.Fields.nombre_jefe.ToString());
				arrColumnas.Add(entOpeListado.Fields.nro_voe.ToString());
				arrColumnas.Add(entOpeListado.Fields.apiestado.ToString());
				arrColumnas.Add(entOpeListado.Fields.usucre.ToString());
				arrColumnas.Add(entOpeListado.Fields.feccre.ToString());
				arrColumnas.Add(entOpeListado.Fields.usumod.ToString());
				arrColumnas.Add(entOpeListado.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla ope_listado
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entOpeListado.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla ope_listado
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entOpeListado.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla ope_listado
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarDataGridOr(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla ope_listado
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DbDataReader dsReader = local.cargarDataReaderOr(cParametros.schema + entOpeListado.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				dtg.DataSource = dsReader;

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion 


		#endregion

		#region Funciones Internas

		/// <summary>
		/// 	 Funcion que devuelve un objeto a partir de un DataRow
		/// </summary>
		/// <param name="row" type="System.Data.DataRow">
		///     <para>
		/// 		 DataRow con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Objeto ope_listado
		/// </returns>
		internal entOpeListado crearObjeto(DataRow row)
		{
			entOpeListado obj = new entOpeListado();
			obj.id_listado = GetColumnType(row[entOpeListado.Fields.id_listado.ToString()], entOpeListado.Fields.id_listado);
			obj.id_upm = GetColumnType(row[entOpeListado.Fields.id_upm.ToString()], entOpeListado.Fields.id_upm);
			obj.codigo_manzana_comunidad = GetColumnType(row[entOpeListado.Fields.codigo_manzana_comunidad.ToString()], entOpeListado.Fields.codigo_manzana_comunidad);
			obj.avenida_calle = GetColumnType(row[entOpeListado.Fields.avenida_calle.ToString()], entOpeListado.Fields.avenida_calle);
			obj.nro_predio = GetColumnType(row[entOpeListado.Fields.nro_predio.ToString()], entOpeListado.Fields.nro_predio);
			obj.nro_puerta = GetColumnType(row[entOpeListado.Fields.nro_puerta.ToString()], entOpeListado.Fields.nro_puerta);
			obj.nro_orden_vivienda = GetColumnType(row[entOpeListado.Fields.nro_orden_vivienda.ToString()], entOpeListado.Fields.nro_orden_vivienda);
			obj.nro_piso = GetColumnType(row[entOpeListado.Fields.nro_piso.ToString()], entOpeListado.Fields.nro_piso);
			obj.nro_depto = GetColumnType(row[entOpeListado.Fields.nro_depto.ToString()], entOpeListado.Fields.nro_depto);
			obj.uso_vivienda = GetColumnType(row[entOpeListado.Fields.uso_vivienda.ToString()], entOpeListado.Fields.uso_vivienda);
			obj.nro_hogares = GetColumnType(row[entOpeListado.Fields.nro_hogares.ToString()], entOpeListado.Fields.nro_hogares);
			obj.nro_hombres = GetColumnType(row[entOpeListado.Fields.nro_hombres.ToString()], entOpeListado.Fields.nro_hombres);
			obj.nro_mujeres = GetColumnType(row[entOpeListado.Fields.nro_mujeres.ToString()], entOpeListado.Fields.nro_mujeres);
			obj.nombre_jefe = GetColumnType(row[entOpeListado.Fields.nombre_jefe.ToString()], entOpeListado.Fields.nombre_jefe);
			obj.nro_voe = GetColumnType(row[entOpeListado.Fields.nro_voe.ToString()], entOpeListado.Fields.nro_voe);
			obj.apiestado = GetColumnType(row[entOpeListado.Fields.apiestado.ToString()], entOpeListado.Fields.apiestado);
			obj.usucre = GetColumnType(row[entOpeListado.Fields.usucre.ToString()], entOpeListado.Fields.usucre);
			obj.feccre = GetColumnType(row[entOpeListado.Fields.feccre.ToString()], entOpeListado.Fields.feccre);
			obj.usumod = GetColumnType(row[entOpeListado.Fields.usumod.ToString()], entOpeListado.Fields.usumod);
			obj.fecmod = GetColumnType(row[entOpeListado.Fields.fecmod.ToString()], entOpeListado.Fields.fecmod);
			return obj;
		}

		/// <summary>
		/// 	 Funcion que devuelve un objeto a partir de un DataRow
		/// </summary>
		/// <param name="row" type="System.Data.DataRow">
		///     <para>
		/// 		 DataRow con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Objeto ope_listado
		/// </returns>
		internal entOpeListado crearObjetoRevisado(DataRow row)
		{
			entOpeListado obj = new entOpeListado();
			if (row.Table.Columns.Contains(entOpeListado.Fields.id_listado.ToString()))
				obj.id_listado = GetColumnType(row[entOpeListado.Fields.id_listado.ToString()], entOpeListado.Fields.id_listado);
			if (row.Table.Columns.Contains(entOpeListado.Fields.id_upm.ToString()))
				obj.id_upm = GetColumnType(row[entOpeListado.Fields.id_upm.ToString()], entOpeListado.Fields.id_upm);
			if (row.Table.Columns.Contains(entOpeListado.Fields.codigo_manzana_comunidad.ToString()))
				obj.codigo_manzana_comunidad = GetColumnType(row[entOpeListado.Fields.codigo_manzana_comunidad.ToString()], entOpeListado.Fields.codigo_manzana_comunidad);
			if (row.Table.Columns.Contains(entOpeListado.Fields.avenida_calle.ToString()))
				obj.avenida_calle = GetColumnType(row[entOpeListado.Fields.avenida_calle.ToString()], entOpeListado.Fields.avenida_calle);
			if (row.Table.Columns.Contains(entOpeListado.Fields.nro_predio.ToString()))
				obj.nro_predio = GetColumnType(row[entOpeListado.Fields.nro_predio.ToString()], entOpeListado.Fields.nro_predio);
			if (row.Table.Columns.Contains(entOpeListado.Fields.nro_puerta.ToString()))
				obj.nro_puerta = GetColumnType(row[entOpeListado.Fields.nro_puerta.ToString()], entOpeListado.Fields.nro_puerta);
			if (row.Table.Columns.Contains(entOpeListado.Fields.nro_orden_vivienda.ToString()))
				obj.nro_orden_vivienda = GetColumnType(row[entOpeListado.Fields.nro_orden_vivienda.ToString()], entOpeListado.Fields.nro_orden_vivienda);
			if (row.Table.Columns.Contains(entOpeListado.Fields.nro_piso.ToString()))
				obj.nro_piso = GetColumnType(row[entOpeListado.Fields.nro_piso.ToString()], entOpeListado.Fields.nro_piso);
			if (row.Table.Columns.Contains(entOpeListado.Fields.nro_depto.ToString()))
				obj.nro_depto = GetColumnType(row[entOpeListado.Fields.nro_depto.ToString()], entOpeListado.Fields.nro_depto);
			if (row.Table.Columns.Contains(entOpeListado.Fields.uso_vivienda.ToString()))
				obj.uso_vivienda = GetColumnType(row[entOpeListado.Fields.uso_vivienda.ToString()], entOpeListado.Fields.uso_vivienda);
			if (row.Table.Columns.Contains(entOpeListado.Fields.nro_hogares.ToString()))
				obj.nro_hogares = GetColumnType(row[entOpeListado.Fields.nro_hogares.ToString()], entOpeListado.Fields.nro_hogares);
			if (row.Table.Columns.Contains(entOpeListado.Fields.nro_hombres.ToString()))
				obj.nro_hombres = GetColumnType(row[entOpeListado.Fields.nro_hombres.ToString()], entOpeListado.Fields.nro_hombres);
			if (row.Table.Columns.Contains(entOpeListado.Fields.nro_mujeres.ToString()))
				obj.nro_mujeres = GetColumnType(row[entOpeListado.Fields.nro_mujeres.ToString()], entOpeListado.Fields.nro_mujeres);
			if (row.Table.Columns.Contains(entOpeListado.Fields.nombre_jefe.ToString()))
				obj.nombre_jefe = GetColumnType(row[entOpeListado.Fields.nombre_jefe.ToString()], entOpeListado.Fields.nombre_jefe);
			if (row.Table.Columns.Contains(entOpeListado.Fields.nro_voe.ToString()))
				obj.nro_voe = GetColumnType(row[entOpeListado.Fields.nro_voe.ToString()], entOpeListado.Fields.nro_voe);
			if (row.Table.Columns.Contains(entOpeListado.Fields.apiestado.ToString()))
				obj.apiestado = GetColumnType(row[entOpeListado.Fields.apiestado.ToString()], entOpeListado.Fields.apiestado);
			if (row.Table.Columns.Contains(entOpeListado.Fields.usucre.ToString()))
				obj.usucre = GetColumnType(row[entOpeListado.Fields.usucre.ToString()], entOpeListado.Fields.usucre);
			if (row.Table.Columns.Contains(entOpeListado.Fields.feccre.ToString()))
				obj.feccre = GetColumnType(row[entOpeListado.Fields.feccre.ToString()], entOpeListado.Fields.feccre);
			if (row.Table.Columns.Contains(entOpeListado.Fields.usumod.ToString()))
				obj.usumod = GetColumnType(row[entOpeListado.Fields.usumod.ToString()], entOpeListado.Fields.usumod);
			if (row.Table.Columns.Contains(entOpeListado.Fields.fecmod.ToString()))
				obj.fecmod = GetColumnType(row[entOpeListado.Fields.fecmod.ToString()], entOpeListado.Fields.fecmod);
			return obj;
		}

		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtope_listado" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Lista de Objetos ope_listado
		/// </returns>
		internal List<entOpeListado> crearLista(DataTable dtope_listado)
		{
			List<entOpeListado> list = new List<entOpeListado>();
			
			entOpeListado obj = new entOpeListado();
			foreach (DataRow row in dtope_listado.Rows)
			{
				obj = crearObjeto(row);
				list.Add(obj);
			}
			return list;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable y con solo algunas columnas
		/// </summary>
		/// <param name="dtope_listado" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Lista de Objetos ope_listado
		/// </returns>
		internal List<entOpeListado> crearListaRevisada(DataTable dtope_listado)
		{
			List<entOpeListado> list = new List<entOpeListado>();
			
			entOpeListado obj = new entOpeListado();
			foreach (DataRow row in dtope_listado.Rows)
			{
				obj = crearObjetoRevisado(row);
				list.Add(obj);
			}
			return list;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtope_listado" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cola de Objetos ope_listado
		/// </returns>
		internal Queue<entOpeListado> crearCola(DataTable dtope_listado)
		{
			Queue<entOpeListado> cola = new Queue<entOpeListado>();
			
			entOpeListado obj = new entOpeListado();
			foreach (DataRow row in dtope_listado.Rows)
			{
				obj = crearObjeto(row);
				cola.Enqueue(obj);
			}
			return cola;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtope_listado" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Pila de Objetos ope_listado
		/// </returns>
		internal Stack<entOpeListado> crearPila(DataTable dtope_listado)
		{
			Stack<entOpeListado> pila = new Stack<entOpeListado>();
			
			entOpeListado obj = new entOpeListado();
			foreach (DataRow row in dtope_listado.Rows)
			{
				obj = crearObjeto(row);
				pila.Push(obj);
			}
			return pila;
		}
		
		/// <summary>
		/// 	 Funcion que crea un Dicionario a partir de un DataTable
		/// </summary>
		/// <param name="dtope_listado" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Diccionario de Objetos ope_listado
		/// </returns>
		internal Dictionary<String, entOpeListado> crearDiccionario(DataTable dtope_listado)
		{
			Dictionary<String, entOpeListado>  miDic = new Dictionary<String, entOpeListado>();
			
			entOpeListado obj = new entOpeListado();
			foreach (DataRow row in dtope_listado.Rows)
			{
				obj = crearObjeto(row);
				miDic.Add(obj.id_listado.ToString(), obj);
			}
			return miDic;
		}
		
		/// <summary>
		/// 	 Funcion que crea un Dicionario a partir de un DataTable
		/// </summary>
		/// <param name="dtope_listado" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 HashTable de Objetos ope_listado
		/// </returns>
		internal Hashtable crearHashTable(DataTable dtope_listado)
		{
			Hashtable miTabla = new Hashtable();
			
			entOpeListado obj = new entOpeListado();
			foreach (DataRow row in dtope_listado.Rows)
			{
				obj = crearObjeto(row);
				miTabla.Add(obj.id_listado.ToString(), obj);
			}
			return miTabla;
		}
		
		/// <summary>
		/// 	 Funcion que crea un Dicionario a partir de un DataTable y solo con columnas existentes
		/// </summary>
		/// <param name="dtope_listado" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Diccionario de Objetos ope_listado
		/// </returns>
		internal Dictionary<String, entOpeListado> crearDiccionarioRevisado(DataTable dtope_listado)
		{
			Dictionary<String, entOpeListado>  miDic = new Dictionary<String, entOpeListado>();
			
			entOpeListado obj = new entOpeListado();
			foreach (DataRow row in dtope_listado.Rows)
			{
				obj = crearObjetoRevisado(row);
				miDic.Add(obj.id_listado.ToString(), obj);
			}
			return miDic;
		}
		
		public void Dispose()
		{
			GC.SuppressFinalize(this);
		}
		
		protected void Finalize()
		{
			Dispose();
		}
		#endregion

	}
}

