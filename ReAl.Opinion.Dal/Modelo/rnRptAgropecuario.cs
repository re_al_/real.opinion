#region 
/***********************************************************************************************************
	NOMBRE:       rnRptAgropecuario
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla rpt_agropecuario

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        23/04/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using ReAl.Opinion.Dal;
using ReAl.Opinion.Dal.Entidades;
using ReAl.Opinion.Dal.Interface;
using System.Windows.Forms;
using ReAl.Opinion.PgConn;
using ReAl.Opinion.PgConn;

#endregion

namespace ReAl.Opinion.Dal.Modelo
{
	public partial class rnRptAgropecuario: inRptAgropecuario
	{
		//Debe implementar la Interface (Alt + Shift + F10)

		#region inRptAgropecuario Members

		/// <summary>
		/// Metodo que devuelve el Script SQL de la Tabla
		/// </summary>
		/// <returns>Script SQL</returns>
		public string getTableScript()
		{
			TableClass tabla = new TableClass(typeof(entRptAgropecuario));
			return tabla.CreateTableScript();
		}
		
		/// <summary>
		/// Metodo para castear Dinamicamente un Tipo
		/// <summary>
		/// <param name="valor">Tipo a ser casteado</param>
		/// <param name="myField">Enum de la columna</param>
		/// <returns>Devuelve un objeto del Tipo de la columna especificada en el Enum</returns>
		public dynamic GetColumnType(object valor, entRptAgropecuario.Fields myField)
		{
			if (DBNull.Value.Equals(valor)) 
				return null;
			Type destino = typeof(entRptAgropecuario).GetProperty(myField.ToString()).PropertyType;
			var miTipo = Nullable.GetUnderlyingType(destino) ?? destino;
			return Convert.ChangeType(valor, miTipo);
		}

		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entRptAgropecuario a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo entRptAgropecuario que cumple con los filtros de los parametros
		/// </returns>
		public entRptAgropecuario ObtenerObjeto(int intgestion, int intid_persona)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(entRptAgropecuario.Fields.gestion.ToString());
			arrColumnasWhere.Add(entRptAgropecuario.Fields.id_persona.ToString());
		
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(intgestion);
			arrValoresWhere.Add(intid_persona);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entRptAgropecuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entRptAgropecuario que cumple con los filtros de los parametros
		/// </returns>
		public entRptAgropecuario ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entRptAgropecuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entRptAgropecuario que cumple con los filtros de los parametros
		/// </returns>
		public entRptAgropecuario ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entRptAgropecuario.Fields.gestion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.mes.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.foto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_upmipp.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_provincia.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_municipio.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_listado.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_boleta.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_persona.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_movimiento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.departamento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.provincia.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.municipio.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.comunidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.login.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.es_reemplazo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.posible_reemplazo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.producto_boleta.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.producto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_producto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_ccp.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.informante.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.celular.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.direccion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.carac_adic_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.variedad_tipo_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.talla_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cantidad_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unidad_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.precio_unit_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.observaciones.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.minfec.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.maxfec.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.duracion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.carac_adic.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.variedad_tipo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.talla.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cantidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.precio_unit.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cant_equiv_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unid_equiv_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cant_equiv.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unid_equiv.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_variedad.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entRptAgropecuario.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count == 1)
				{
					entRptAgropecuario obj = new entRptAgropecuario();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entRptAgropecuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entRptAgropecuario que cumple con los filtros de los parametros
		/// </returns>
		public entRptAgropecuario ObtenerObjeto(entRptAgropecuario.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entRptAgropecuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entRptAgropecuario que cumple con los filtros de los parametros
		/// </returns>
		public entRptAgropecuario ObtenerObjeto(entRptAgropecuario.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un Business Object del Tipo entRptAgropecuario a partir de su llave promaria
		/// </summary>
		/// <returns>
		/// 	Objeto del Tipo entRptAgropecuario
		/// </returns>
		public entRptAgropecuario ObtenerObjeto(int intgestion, int intid_persona, ref cTrans localTrans )
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(entRptAgropecuario.Fields.gestion.ToString());
			arrColumnasWhere.Add(entRptAgropecuario.Fields.id_persona.ToString());
		
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(intgestion);
			arrValoresWhere.Add(intid_persona);
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entRptAgropecuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion rpt_agropecuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entRptAgropecuario que cumple con los filtros de los parametros
		/// </returns>
		public entRptAgropecuario ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entRptAgropecuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion rpt_agropecuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entRptAgropecuario que cumple con los filtros de los parametros
		/// </returns>
		public entRptAgropecuario ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entRptAgropecuario.Fields.gestion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.mes.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.foto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_upmipp.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_provincia.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_municipio.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_listado.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_boleta.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_persona.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_movimiento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.departamento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.provincia.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.municipio.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.comunidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.login.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.es_reemplazo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.posible_reemplazo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.producto_boleta.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.producto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_producto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_ccp.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.informante.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.celular.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.direccion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.carac_adic_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.variedad_tipo_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.talla_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cantidad_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unidad_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.precio_unit_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.observaciones.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.minfec.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.maxfec.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.duracion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.carac_adic.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.variedad_tipo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.talla.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cantidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.precio_unit.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cant_equiv_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unid_equiv_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cant_equiv.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unid_equiv.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_variedad.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entRptAgropecuario.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count == 1)
				{
					entRptAgropecuario obj = new entRptAgropecuario();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entRptAgropecuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion rpt_agropecuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entRptAgropecuario que cumple con los filtros de los parametros
		/// </returns>
		public entRptAgropecuario ObtenerObjeto(entRptAgropecuario.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase entRptAgropecuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion rpt_agropecuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entRptAgropecuario que cumple con los filtros de los parametros
		/// </returns>
		public entRptAgropecuario ObtenerObjeto(entRptAgropecuario.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptAgropecuario a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo List<entRptAgropecuario> que cumple con los filtros de los parametros
		/// </returns>
		public List<entRptAgropecuario> ObtenerLista()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptAgropecuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo List<entRptAgropecuario> que cumple con los filtros de los parametros
		/// </returns>
		public List<entRptAgropecuario> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptAgropecuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo List<entRptAgropecuario> que cumple con los filtros de los parametros
		/// </returns>
		public List<entRptAgropecuario> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entRptAgropecuario.Fields.gestion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.mes.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.foto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_upmipp.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_provincia.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_municipio.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_listado.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_boleta.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_persona.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_movimiento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.departamento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.provincia.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.municipio.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.comunidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.login.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.es_reemplazo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.posible_reemplazo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.producto_boleta.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.producto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_producto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_ccp.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.informante.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.celular.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.direccion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.carac_adic_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.variedad_tipo_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.talla_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cantidad_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unidad_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.precio_unit_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.observaciones.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.minfec.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.maxfec.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.duracion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.carac_adic.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.variedad_tipo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.talla.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cantidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.precio_unit.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cant_equiv_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unid_equiv_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cant_equiv.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unid_equiv.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_variedad.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entRptAgropecuario.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<entRptAgropecuario>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptAgropecuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo List<entRptAgropecuario> que cumple con los filtros de los parametros
		/// </returns>
		public List<entRptAgropecuario> ObtenerLista(entRptAgropecuario.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptAgropecuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo List<entRptAgropecuario> que cumple con los filtros de los parametros
		/// </returns>
		public List<entRptAgropecuario> ObtenerLista(entRptAgropecuario.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptAgropecuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion rpt_agropecuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo List<entRptAgropecuario> que cumple con los filtros de los parametros
		/// </returns>
		public List<entRptAgropecuario> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptAgropecuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion rpt_agropecuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo List<entRptAgropecuario> que cumple con los filtros de los parametros
		/// </returns>
		public List<entRptAgropecuario> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entRptAgropecuario.Fields.gestion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.mes.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.foto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_upmipp.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_provincia.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_municipio.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_listado.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_boleta.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_persona.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_movimiento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.departamento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.provincia.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.municipio.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.comunidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.login.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.es_reemplazo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.posible_reemplazo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.producto_boleta.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.producto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_producto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_ccp.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.informante.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.celular.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.direccion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.carac_adic_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.variedad_tipo_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.talla_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cantidad_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unidad_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.precio_unit_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.observaciones.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.minfec.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.maxfec.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.duracion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.carac_adic.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.variedad_tipo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.talla.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cantidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.precio_unit.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cant_equiv_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unid_equiv_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cant_equiv.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unid_equiv.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_variedad.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entRptAgropecuario.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<entRptAgropecuario>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptAgropecuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion rpt_agropecuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo List<entRptAgropecuario> que cumple con los filtros de los parametros
		/// </returns>
		public List<entRptAgropecuario> ObtenerLista(entRptAgropecuario.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptAgropecuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion rpt_agropecuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo List<entRptAgropecuario> que cumple con los filtros de los parametros
		/// </returns>
		public List<entRptAgropecuario> ObtenerLista(entRptAgropecuario.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptAgropecuario a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo Queue<entRptAgropecuario> que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entRptAgropecuario> ObtenerCola()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptAgropecuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Queue<entRptAgropecuario> que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entRptAgropecuario> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptAgropecuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Queue<entRptAgropecuario> que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entRptAgropecuario> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entRptAgropecuario.Fields.gestion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.mes.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.foto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_upmipp.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_provincia.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_municipio.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_listado.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_boleta.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_persona.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_movimiento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.departamento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.provincia.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.municipio.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.comunidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.login.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.es_reemplazo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.posible_reemplazo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.producto_boleta.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.producto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_producto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_ccp.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.informante.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.celular.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.direccion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.carac_adic_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.variedad_tipo_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.talla_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cantidad_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unidad_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.precio_unit_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.observaciones.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.minfec.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.maxfec.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.duracion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.carac_adic.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.variedad_tipo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.talla.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cantidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.precio_unit.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cant_equiv_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unid_equiv_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cant_equiv.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unid_equiv.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_variedad.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entRptAgropecuario.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearCola(table);
				}
				else
					return new Queue<entRptAgropecuario>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptAgropecuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Queue<entRptAgropecuario> que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entRptAgropecuario> ObtenerCola(entRptAgropecuario.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptAgropecuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Queue<entRptAgropecuario> que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entRptAgropecuario> ObtenerCola(entRptAgropecuario.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptAgropecuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion rpt_agropecuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Queue<entRptAgropecuario> que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entRptAgropecuario> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptAgropecuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion rpt_agropecuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Queue<entRptAgropecuario> que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entRptAgropecuario> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entRptAgropecuario.Fields.gestion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.mes.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.foto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_upmipp.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_provincia.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_municipio.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_listado.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_boleta.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_persona.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_movimiento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.departamento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.provincia.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.municipio.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.comunidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.login.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.es_reemplazo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.posible_reemplazo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.producto_boleta.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.producto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_producto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_ccp.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.informante.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.celular.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.direccion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.carac_adic_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.variedad_tipo_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.talla_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cantidad_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unidad_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.precio_unit_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.observaciones.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.minfec.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.maxfec.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.duracion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.carac_adic.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.variedad_tipo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.talla.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cantidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.precio_unit.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cant_equiv_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unid_equiv_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cant_equiv.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unid_equiv.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_variedad.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entRptAgropecuario.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearCola(table);
				}
				else
					return new Queue<entRptAgropecuario>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptAgropecuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion rpt_agropecuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Queue<entRptAgropecuario> que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entRptAgropecuario> ObtenerCola(entRptAgropecuario.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptAgropecuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion rpt_agropecuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Queue<entRptAgropecuario> que cumple con los filtros de los parametros
		/// </returns>
		public Queue<entRptAgropecuario> ObtenerCola(entRptAgropecuario.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptAgropecuario a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo Stack<entRptAgropecuario> que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entRptAgropecuario> ObtenerPila()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptAgropecuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack<entRptAgropecuario> que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entRptAgropecuario> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptAgropecuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack<entRptAgropecuario> que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entRptAgropecuario> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entRptAgropecuario.Fields.gestion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.mes.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.foto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_upmipp.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_provincia.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_municipio.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_listado.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_boleta.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_persona.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_movimiento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.departamento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.provincia.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.municipio.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.comunidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.login.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.es_reemplazo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.posible_reemplazo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.producto_boleta.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.producto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_producto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_ccp.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.informante.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.celular.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.direccion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.carac_adic_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.variedad_tipo_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.talla_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cantidad_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unidad_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.precio_unit_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.observaciones.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.minfec.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.maxfec.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.duracion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.carac_adic.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.variedad_tipo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.talla.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cantidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.precio_unit.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cant_equiv_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unid_equiv_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cant_equiv.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unid_equiv.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_variedad.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entRptAgropecuario.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearPila(table);
				}
				else
					return new Stack<entRptAgropecuario>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptAgropecuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack<entRptAgropecuario> que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entRptAgropecuario> ObtenerPila(entRptAgropecuario.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptAgropecuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack<entRptAgropecuario> que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entRptAgropecuario> ObtenerPila(entRptAgropecuario.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptAgropecuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion rpt_agropecuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack<entRptAgropecuario> que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entRptAgropecuario> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptAgropecuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion rpt_agropecuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack<entRptAgropecuario> que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entRptAgropecuario> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entRptAgropecuario.Fields.gestion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.mes.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.foto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_upmipp.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_provincia.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_municipio.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_listado.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_boleta.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_persona.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_movimiento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.departamento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.provincia.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.municipio.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.comunidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.login.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.es_reemplazo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.posible_reemplazo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.producto_boleta.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.producto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_producto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_ccp.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.informante.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.celular.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.direccion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.carac_adic_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.variedad_tipo_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.talla_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cantidad_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unidad_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.precio_unit_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.observaciones.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.minfec.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.maxfec.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.duracion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.carac_adic.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.variedad_tipo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.talla.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cantidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.precio_unit.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cant_equiv_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unid_equiv_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cant_equiv.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unid_equiv.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_variedad.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entRptAgropecuario.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearPila(table);
				}
				else
					return new Stack<entRptAgropecuario>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptAgropecuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion rpt_agropecuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack<entRptAgropecuario> que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entRptAgropecuario> ObtenerPila(entRptAgropecuario.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptAgropecuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion rpt_agropecuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack<entRptAgropecuario> que cumple con los filtros de los parametros
		/// </returns>
		public Stack<entRptAgropecuario> ObtenerPila(entRptAgropecuario.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptAgropecuario a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo Dictionary<String, entRptAgropecuario> que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entRptAgropecuario> ObtenerDiccionario()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptAgropecuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Dictionary<String, entRptAgropecuario> que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entRptAgropecuario> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptAgropecuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Dictionary<String, entRptAgropecuario> que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entRptAgropecuario> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entRptAgropecuario.Fields.gestion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.mes.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.foto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_upmipp.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_provincia.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_municipio.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_listado.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_boleta.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_persona.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_movimiento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.departamento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.provincia.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.municipio.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.comunidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.login.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.es_reemplazo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.posible_reemplazo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.producto_boleta.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.producto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_producto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_ccp.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.informante.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.celular.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.direccion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.carac_adic_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.variedad_tipo_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.talla_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cantidad_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unidad_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.precio_unit_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.observaciones.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.minfec.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.maxfec.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.duracion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.carac_adic.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.variedad_tipo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.talla.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cantidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.precio_unit.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cant_equiv_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unid_equiv_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cant_equiv.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unid_equiv.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_variedad.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entRptAgropecuario.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearDiccionario(table);
				}
				else
					return new Dictionary<string, entRptAgropecuario>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptAgropecuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Dictionary<String, entRptAgropecuario> que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entRptAgropecuario> ObtenerDiccionario(entRptAgropecuario.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptAgropecuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Dictionary<String, entRptAgropecuario> que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entRptAgropecuario> ObtenerDiccionario(entRptAgropecuario.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptAgropecuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion rpt_agropecuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Dictionary<String, entRptAgropecuario> que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entRptAgropecuario> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptAgropecuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion rpt_agropecuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Dictionary<String, entRptAgropecuario> que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entRptAgropecuario> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entRptAgropecuario.Fields.gestion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.mes.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.foto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_upmipp.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_provincia.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_municipio.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_listado.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_boleta.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_persona.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_movimiento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.departamento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.provincia.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.municipio.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.comunidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.login.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.es_reemplazo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.posible_reemplazo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.producto_boleta.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.producto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_producto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_ccp.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.informante.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.celular.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.direccion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.carac_adic_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.variedad_tipo_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.talla_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cantidad_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unidad_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.precio_unit_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.observaciones.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.minfec.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.maxfec.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.duracion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.carac_adic.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.variedad_tipo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.talla.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cantidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.precio_unit.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cant_equiv_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unid_equiv_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cant_equiv.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unid_equiv.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_variedad.ToString());
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entRptAgropecuario.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearDiccionario(table);
				}
				else
					return new Dictionary<string, entRptAgropecuario>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptAgropecuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion rpt_agropecuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Dictionary<String, entRptAgropecuario> que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entRptAgropecuario> ObtenerDiccionario(entRptAgropecuario.Fields searchField, object searchValue, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase entRptAgropecuario a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion rpt_agropecuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Dictionary<String, entRptAgropecuario> que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, entRptAgropecuario> ObtenerDiccionario(entRptAgropecuario.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	 Funcion que obtiene la llave primaria unica de la tabla rpt_agropecuario a partir de una cadena
		/// </summary>
		/// <param name="cod" type="string">
		///     <para>
		/// 		 Cadena desde la que se construye el identificador unico de la tabla rpt_agropecuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Identificador unico de la tabla rpt_agropecuario
		/// </returns>
		public string CreatePK(string[] args)
		{
			return args[0];
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla RptAgropecuario a partir de una clase del tipo ERptAgropecuario
		/// </summary>
		/// <param name="obj" type="Entidades.entRptAgropecuario">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla RptAgropecuarioRptAgropecuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionRptAgropecuario
		/// </returns>
		public bool Insert(entRptAgropecuario obj)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entRptAgropecuario.Fields.gestion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.mes.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.foto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_upmipp.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_provincia.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_municipio.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_listado.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_boleta.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_persona.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_movimiento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.departamento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.provincia.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.municipio.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.comunidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.login.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.es_reemplazo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.posible_reemplazo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.producto_boleta.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.producto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_producto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_ccp.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.informante.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.celular.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.direccion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.carac_adic_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.variedad_tipo_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.talla_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cantidad_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unidad_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.precio_unit_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.observaciones.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.minfec.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.maxfec.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.duracion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.carac_adic.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.variedad_tipo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.talla.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cantidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.precio_unit.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cant_equiv_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unid_equiv_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cant_equiv.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unid_equiv.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_variedad.ToString());
				
				ArrayList arrValores = new ArrayList();
				arrValores.Add(obj.gestion);
				arrValores.Add(obj.mes);
				arrValores.Add("'" + obj.foto + "'");
				arrValores.Add(obj.id_departamento);
				arrValores.Add(obj.id_upmipp);
				arrValores.Add(obj.id_provincia);
				arrValores.Add(obj.id_municipio);
				arrValores.Add(obj.id_listado);
				arrValores.Add(obj.id_boleta);
				arrValores.Add(obj.id_persona);
				arrValores.Add(obj.id_movimiento);
				arrValores.Add("'" + obj.departamento + "'");
				arrValores.Add("'" + obj.provincia + "'");
				arrValores.Add("'" + obj.municipio + "'");
				arrValores.Add("'" + obj.comunidad + "'");
				arrValores.Add("'" + obj.login + "'");
				arrValores.Add("'" + obj.es_reemplazo + "'");
				arrValores.Add("'" + obj.posible_reemplazo + "'");
				arrValores.Add("'" + obj.producto_boleta + "'");
				arrValores.Add("'" + obj.producto + "'");
				arrValores.Add("'" + obj.codigo_producto + "'");
				arrValores.Add("'" + obj.codigo_ccp + "'");
				arrValores.Add("'" + obj.informante + "'");
				arrValores.Add("'" + obj.celular + "'");
				arrValores.Add("'" + obj.direccion + "'");
				arrValores.Add("'" + obj.carac_adic_original + "'");
				arrValores.Add("'" + obj.variedad_tipo_original + "'");
				arrValores.Add("'" + obj.talla_original + "'");
				arrValores.Add("'" + obj.cantidad_original + "'");
				arrValores.Add("'" + obj.unidad_original + "'");
				arrValores.Add("'" + obj.precio_unit_original + "'");
				arrValores.Add("'" + obj.observaciones + "'");
				if (obj.minfec == null)
					arrValores.Add(null);
				else
					arrValores.Add("'" + Convert.ToDateTime(obj.minfec).ToString(cParametros.parFormatoFechaHora) + "'");
				if (obj.maxfec == null)
					arrValores.Add(null);
				else
					arrValores.Add("'" + Convert.ToDateTime(obj.maxfec).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.duracion);
				arrValores.Add("'" + obj.carac_adic + "'");
				arrValores.Add("'" + obj.variedad_tipo + "'");
				arrValores.Add("'" + obj.talla + "'");
				arrValores.Add("'" + obj.cantidad + "'");
				arrValores.Add("'" + obj.unidad + "'");
				arrValores.Add("'" + obj.precio_unit + "'");
				arrValores.Add("'" + obj.cant_equiv_original + "'");
				arrValores.Add("'" + obj.unid_equiv_original + "'");
				arrValores.Add("'" + obj.cant_equiv + "'");
				arrValores.Add("'" + obj.unid_equiv + "'");
				arrValores.Add("'" + obj.codigo_variedad + "'");

			
				cConn local = new cConn();
				return local.insertBD("RptAgropecuario", arrColumnas, arrValores);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla RptAgropecuario a partir de una clase del tipo ERptAgropecuario
		/// </summary>
		/// <param name="obj" type="Entidades.entRptAgropecuario">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla RptAgropecuarioRptAgropecuario
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion RptAgropecuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionRptAgropecuario
		/// </returns>
		public bool Insert(entRptAgropecuario obj, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entRptAgropecuario.Fields.gestion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.mes.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.foto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_upmipp.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_provincia.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_municipio.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_listado.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_boleta.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_persona.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_movimiento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.departamento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.provincia.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.municipio.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.comunidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.login.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.es_reemplazo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.posible_reemplazo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.producto_boleta.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.producto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_producto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_ccp.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.informante.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.celular.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.direccion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.carac_adic_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.variedad_tipo_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.talla_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cantidad_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unidad_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.precio_unit_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.observaciones.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.minfec.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.maxfec.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.duracion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.carac_adic.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.variedad_tipo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.talla.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cantidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.precio_unit.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cant_equiv_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unid_equiv_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cant_equiv.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unid_equiv.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_variedad.ToString());
			
				ArrayList arrValores = new ArrayList();
				arrValores.Add(obj.gestion);
				arrValores.Add(obj.mes);
				arrValores.Add("'" + obj.foto + "'");
				arrValores.Add(obj.id_departamento);
				arrValores.Add(obj.id_upmipp);
				arrValores.Add(obj.id_provincia);
				arrValores.Add(obj.id_municipio);
				arrValores.Add(obj.id_listado);
				arrValores.Add(obj.id_boleta);
				arrValores.Add(obj.id_persona);
				arrValores.Add(obj.id_movimiento);
				arrValores.Add("'" + obj.departamento + "'");
				arrValores.Add("'" + obj.provincia + "'");
				arrValores.Add("'" + obj.municipio + "'");
				arrValores.Add("'" + obj.comunidad + "'");
				arrValores.Add("'" + obj.login + "'");
				arrValores.Add("'" + obj.es_reemplazo + "'");
				arrValores.Add("'" + obj.posible_reemplazo + "'");
				arrValores.Add("'" + obj.producto_boleta + "'");
				arrValores.Add("'" + obj.producto + "'");
				arrValores.Add("'" + obj.codigo_producto + "'");
				arrValores.Add("'" + obj.codigo_ccp + "'");
				arrValores.Add("'" + obj.informante + "'");
				arrValores.Add("'" + obj.celular + "'");
				arrValores.Add("'" + obj.direccion + "'");
				arrValores.Add("'" + obj.carac_adic_original + "'");
				arrValores.Add("'" + obj.variedad_tipo_original + "'");
				arrValores.Add("'" + obj.talla_original + "'");
				arrValores.Add("'" + obj.cantidad_original + "'");
				arrValores.Add("'" + obj.unidad_original + "'");
				arrValores.Add("'" + obj.precio_unit_original + "'");
				arrValores.Add("'" + obj.observaciones + "'");
				if (obj.minfec == null)
					arrValores.Add(null);
				else
					arrValores.Add("'" + Convert.ToDateTime(obj.minfec).ToString(cParametros.parFormatoFechaHora) + "'");
				if (obj.maxfec == null)
					arrValores.Add(null);
				else
					arrValores.Add("'" + Convert.ToDateTime(obj.maxfec).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.duracion);
				arrValores.Add("'" + obj.carac_adic + "'");
				arrValores.Add("'" + obj.variedad_tipo + "'");
				arrValores.Add("'" + obj.talla + "'");
				arrValores.Add("'" + obj.cantidad + "'");
				arrValores.Add("'" + obj.unidad + "'");
				arrValores.Add("'" + obj.precio_unit + "'");
				arrValores.Add("'" + obj.cant_equiv_original + "'");
				arrValores.Add("'" + obj.unid_equiv_original + "'");
				arrValores.Add("'" + obj.cant_equiv + "'");
				arrValores.Add("'" + obj.unid_equiv + "'");
				arrValores.Add("'" + obj.codigo_variedad + "'");

			
				cConn local = new cConn();
				return local.insertBD("RptAgropecuario", arrColumnas, arrValores, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla RptAgropecuario a partir de una clase del tipo ERptAgropecuario
		/// </summary>
		/// <param name="obj" type="Entidades.entRptAgropecuario">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla RptAgropecuarioRptAgropecuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionRptAgropecuario
		/// </returns>
		public bool InsertIdentity(entRptAgropecuario obj)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entRptAgropecuario.Fields.gestion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.mes.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.foto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_upmipp.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_provincia.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_municipio.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_listado.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_boleta.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_persona.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_movimiento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.departamento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.provincia.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.municipio.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.comunidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.login.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.es_reemplazo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.posible_reemplazo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.producto_boleta.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.producto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_producto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_ccp.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.informante.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.celular.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.direccion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.carac_adic_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.variedad_tipo_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.talla_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cantidad_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unidad_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.precio_unit_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.observaciones.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.minfec.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.maxfec.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.duracion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.carac_adic.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.variedad_tipo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.talla.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cantidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.precio_unit.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cant_equiv_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unid_equiv_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cant_equiv.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unid_equiv.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_variedad.ToString());
				
				ArrayList arrValores = new ArrayList();
				arrValores.Add(obj.gestion);
				arrValores.Add(obj.mes);
				arrValores.Add("'" + obj.foto + "'");
				arrValores.Add(obj.id_departamento);
				arrValores.Add(obj.id_upmipp);
				arrValores.Add(obj.id_provincia);
				arrValores.Add(obj.id_municipio);
				arrValores.Add(obj.id_listado);
				arrValores.Add(obj.id_boleta);
				arrValores.Add(obj.id_persona);
				arrValores.Add(obj.id_movimiento);
				arrValores.Add("'" + obj.departamento + "'");
				arrValores.Add("'" + obj.provincia + "'");
				arrValores.Add("'" + obj.municipio + "'");
				arrValores.Add("'" + obj.comunidad + "'");
				arrValores.Add("'" + obj.login + "'");
				arrValores.Add("'" + obj.es_reemplazo + "'");
				arrValores.Add("'" + obj.posible_reemplazo + "'");
				arrValores.Add("'" + obj.producto_boleta + "'");
				arrValores.Add("'" + obj.producto + "'");
				arrValores.Add("'" + obj.codigo_producto + "'");
				arrValores.Add("'" + obj.codigo_ccp + "'");
				arrValores.Add("'" + obj.informante + "'");
				arrValores.Add("'" + obj.celular + "'");
				arrValores.Add("'" + obj.direccion + "'");
				arrValores.Add("'" + obj.carac_adic_original + "'");
				arrValores.Add("'" + obj.variedad_tipo_original + "'");
				arrValores.Add("'" + obj.talla_original + "'");
				arrValores.Add("'" + obj.cantidad_original + "'");
				arrValores.Add("'" + obj.unidad_original + "'");
				arrValores.Add("'" + obj.precio_unit_original + "'");
				arrValores.Add("'" + obj.observaciones + "'");
				if (obj.minfec == null)
					arrValores.Add(null);
				else
					arrValores.Add("'" + Convert.ToDateTime(obj.minfec).ToString(cParametros.parFormatoFechaHora) + "'");
				if (obj.maxfec == null)
					arrValores.Add(null);
				else
					arrValores.Add("'" + Convert.ToDateTime(obj.maxfec).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.duracion);
				arrValores.Add("'" + obj.carac_adic + "'");
				arrValores.Add("'" + obj.variedad_tipo + "'");
				arrValores.Add("'" + obj.talla + "'");
				arrValores.Add("'" + obj.cantidad + "'");
				arrValores.Add("'" + obj.unidad + "'");
				arrValores.Add("'" + obj.precio_unit + "'");
				arrValores.Add("'" + obj.cant_equiv_original + "'");
				arrValores.Add("'" + obj.unid_equiv_original + "'");
				arrValores.Add("'" + obj.cant_equiv + "'");
				arrValores.Add("'" + obj.unid_equiv + "'");
				arrValores.Add("'" + obj.codigo_variedad + "'");

			
				cConn local = new cConn();
				int intIdentidad = -1;
				bool res = local.insertBD("RptAgropecuario", arrColumnas, arrValores, ref intIdentidad);
				obj.gestion = intIdentidad;
				return res;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla RptAgropecuario a partir de una clase del tipo ERptAgropecuario
		/// </summary>
		/// <param name="obj" type="Entidades.entRptAgropecuario">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla RptAgropecuarioRptAgropecuario
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion RptAgropecuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionRptAgropecuario
		/// </returns>
		public bool InsertIdentity(entRptAgropecuario obj, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entRptAgropecuario.Fields.gestion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.mes.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.foto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_upmipp.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_provincia.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_municipio.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_listado.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_boleta.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_persona.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_movimiento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.departamento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.provincia.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.municipio.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.comunidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.login.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.es_reemplazo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.posible_reemplazo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.producto_boleta.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.producto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_producto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_ccp.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.informante.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.celular.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.direccion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.carac_adic_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.variedad_tipo_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.talla_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cantidad_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unidad_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.precio_unit_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.observaciones.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.minfec.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.maxfec.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.duracion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.carac_adic.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.variedad_tipo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.talla.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cantidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.precio_unit.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cant_equiv_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unid_equiv_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cant_equiv.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unid_equiv.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_variedad.ToString());
			
				ArrayList arrValores = new ArrayList();
				arrValores.Add(obj.gestion);
				arrValores.Add(obj.mes);
				arrValores.Add("'" + obj.foto + "'");
				arrValores.Add(obj.id_departamento);
				arrValores.Add(obj.id_upmipp);
				arrValores.Add(obj.id_provincia);
				arrValores.Add(obj.id_municipio);
				arrValores.Add(obj.id_listado);
				arrValores.Add(obj.id_boleta);
				arrValores.Add(obj.id_persona);
				arrValores.Add(obj.id_movimiento);
				arrValores.Add("'" + obj.departamento + "'");
				arrValores.Add("'" + obj.provincia + "'");
				arrValores.Add("'" + obj.municipio + "'");
				arrValores.Add("'" + obj.comunidad + "'");
				arrValores.Add("'" + obj.login + "'");
				arrValores.Add("'" + obj.es_reemplazo + "'");
				arrValores.Add("'" + obj.posible_reemplazo + "'");
				arrValores.Add("'" + obj.producto_boleta + "'");
				arrValores.Add("'" + obj.producto + "'");
				arrValores.Add("'" + obj.codigo_producto + "'");
				arrValores.Add("'" + obj.codigo_ccp + "'");
				arrValores.Add("'" + obj.informante + "'");
				arrValores.Add("'" + obj.celular + "'");
				arrValores.Add("'" + obj.direccion + "'");
				arrValores.Add("'" + obj.carac_adic_original + "'");
				arrValores.Add("'" + obj.variedad_tipo_original + "'");
				arrValores.Add("'" + obj.talla_original + "'");
				arrValores.Add("'" + obj.cantidad_original + "'");
				arrValores.Add("'" + obj.unidad_original + "'");
				arrValores.Add("'" + obj.precio_unit_original + "'");
				arrValores.Add("'" + obj.observaciones + "'");
				if (obj.minfec == null)
					arrValores.Add(null);
				else
					arrValores.Add("'" + Convert.ToDateTime(obj.minfec).ToString(cParametros.parFormatoFechaHora) + "'");
				if (obj.maxfec == null)
					arrValores.Add(null);
				else
					arrValores.Add("'" + Convert.ToDateTime(obj.maxfec).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.duracion);
				arrValores.Add("'" + obj.carac_adic + "'");
				arrValores.Add("'" + obj.variedad_tipo + "'");
				arrValores.Add("'" + obj.talla + "'");
				arrValores.Add("'" + obj.cantidad + "'");
				arrValores.Add("'" + obj.unidad + "'");
				arrValores.Add("'" + obj.precio_unit + "'");
				arrValores.Add("'" + obj.cant_equiv_original + "'");
				arrValores.Add("'" + obj.unid_equiv_original + "'");
				arrValores.Add("'" + obj.cant_equiv + "'");
				arrValores.Add("'" + obj.unid_equiv + "'");
				arrValores.Add("'" + obj.codigo_variedad + "'");

			
				cConn local = new cConn();
				int intIdentidad = -1;
				bool res = local.insertBD("RptAgropecuario", arrColumnas, arrValores, ref intIdentidad, ref localTrans);
				obj.gestion = intIdentidad;
				return res;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla RptAgropecuario a partir de una clase del tipo ERptAgropecuario
		/// </summary>
		/// <param name="obj" type="Entidades.entRptAgropecuario">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla RptAgropecuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionRptAgropecuario
		/// </returns>
		public int Update(entRptAgropecuario obj)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_producto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_ccp.ToString());
                arrColumnas.Add(entRptAgropecuario.Fields.codigo_variedad.ToString());
                arrColumnas.Add(entRptAgropecuario.Fields.talla.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cantidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unidad.ToString());				
				arrColumnas.Add(entRptAgropecuario.Fields.cant_equiv.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unid_equiv.ToString());
                arrColumnas.Add(entRptAgropecuario.Fields.precio_unit.ToString());  
				
			
				ArrayList arrValores = new ArrayList();
				arrValores.Add("'" + obj.codigo_producto + "'");
				arrValores.Add("'" + obj.codigo_ccp + "'");
                arrValores.Add("'" + obj.codigo_variedad + "'");
				arrValores.Add("'" + obj.talla + "'");
				arrValores.Add("'" + obj.cantidad + "'");
				arrValores.Add("'" + obj.unidad + "'");
                arrValores.Add("'" + obj.cant_equiv + "'");
                arrValores.Add("'" + obj.unid_equiv + "'");
                arrValores.Add("'" + obj.precio_unit + "'");
				
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(entRptAgropecuario.Fields.gestion.ToString());
				arrColumnasWhere.Add(entRptAgropecuario.Fields.id_persona.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.gestion);
				arrValoresWhere.Add(obj.id_persona);
			
				cConn local = new cConn();
				return local.updateBD(entRptAgropecuario.strAliasTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla RptAgropecuario a partir de una clase del tipo eRptAgropecuario
		/// </summary>
		/// <param name="obj" type="Entidades.entRptAgropecuario">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla RptAgropecuario
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion RptAgropecuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacion
		/// </returns>
		public int Update(entRptAgropecuario obj, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entRptAgropecuario.Fields.mes.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.foto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_upmipp.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_provincia.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_municipio.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_listado.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_boleta.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_movimiento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.departamento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.provincia.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.municipio.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.comunidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.login.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.es_reemplazo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.posible_reemplazo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.producto_boleta.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.producto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_producto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_ccp.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.informante.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.celular.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.direccion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.carac_adic_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.variedad_tipo_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.talla_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cantidad_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unidad_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.precio_unit_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.observaciones.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.minfec.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.maxfec.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.duracion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.carac_adic.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.variedad_tipo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.talla.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cantidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.precio_unit.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cant_equiv_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unid_equiv_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cant_equiv.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unid_equiv.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_variedad.ToString());
			
				ArrayList arrValores = new ArrayList();
				arrValores.Add(obj.mes);
				arrValores.Add("'" + obj.foto + "'");
				arrValores.Add(obj.id_departamento);
				arrValores.Add(obj.id_upmipp);
				arrValores.Add(obj.id_provincia);
				arrValores.Add(obj.id_municipio);
				arrValores.Add(obj.id_listado);
				arrValores.Add(obj.id_boleta);
				arrValores.Add(obj.id_movimiento);
				arrValores.Add("'" + obj.departamento + "'");
				arrValores.Add("'" + obj.provincia + "'");
				arrValores.Add("'" + obj.municipio + "'");
				arrValores.Add("'" + obj.comunidad + "'");
				arrValores.Add("'" + obj.login + "'");
				arrValores.Add("'" + obj.es_reemplazo + "'");
				arrValores.Add("'" + obj.posible_reemplazo + "'");
				arrValores.Add("'" + obj.producto_boleta + "'");
				arrValores.Add("'" + obj.producto + "'");
				arrValores.Add("'" + obj.codigo_producto + "'");
				arrValores.Add("'" + obj.codigo_ccp + "'");
				arrValores.Add("'" + obj.informante + "'");
				arrValores.Add("'" + obj.celular + "'");
				arrValores.Add("'" + obj.direccion + "'");
				arrValores.Add("'" + obj.carac_adic_original + "'");
				arrValores.Add("'" + obj.variedad_tipo_original + "'");
				arrValores.Add("'" + obj.talla_original + "'");
				arrValores.Add("'" + obj.cantidad_original + "'");
				arrValores.Add("'" + obj.unidad_original + "'");
				arrValores.Add("'" + obj.precio_unit_original + "'");
				arrValores.Add("'" + obj.observaciones + "'");
				if (obj.minfec == null)
					arrValores.Add(null);
				else
					arrValores.Add("'" + Convert.ToDateTime(obj.minfec).ToString(cParametros.parFormatoFechaHora) + "'");
				if (obj.maxfec == null)
					arrValores.Add(null);
				else
					arrValores.Add("'" + Convert.ToDateTime(obj.maxfec).ToString(cParametros.parFormatoFechaHora) + "'");
				arrValores.Add(obj.duracion);
				arrValores.Add("'" + obj.carac_adic + "'");
				arrValores.Add("'" + obj.variedad_tipo + "'");
				arrValores.Add("'" + obj.talla + "'");
				arrValores.Add("'" + obj.cantidad + "'");
				arrValores.Add("'" + obj.unidad + "'");
				arrValores.Add("'" + obj.precio_unit + "'");
				arrValores.Add("'" + obj.cant_equiv_original + "'");
				arrValores.Add("'" + obj.unid_equiv_original + "'");
				arrValores.Add("'" + obj.cant_equiv + "'");
				arrValores.Add("'" + obj.unid_equiv + "'");
				arrValores.Add("'" + obj.codigo_variedad + "'");

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(entRptAgropecuario.Fields.gestion.ToString());
				arrColumnasWhere.Add(entRptAgropecuario.Fields.id_persona.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.gestion);
				arrValoresWhere.Add(obj.id_persona);

			
				cConn local = new cConn();
				return local.updateBD("RptAgropecuario", arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla RptAgropecuario a partir de una clase del tipo entRptAgropecuario y su respectiva PK
		/// </summary>
		/// <param name="obj" type="Entidades.entRptAgropecuario">
		///     <para>
		/// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla RptAgropecuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionRptAgropecuario
		/// </returns>
		public int Delete(entRptAgropecuario obj)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(entRptAgropecuario.Fields.gestion.ToString());
				arrColumnasWhere.Add(entRptAgropecuario.Fields.id_persona.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.gestion);
				arrValoresWhere.Add(obj.id_persona);

			
				cConn local = new cConn();
				return local.deleteBD("RptAgropecuario", arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla RptAgropecuario a partir de una clase del tipo entRptAgropecuario y su PK
		/// </summary>
		/// <param name="obj" type="Entidades.eRptAgropecuario">
		///     <para>
		/// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla RptAgropecuario
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion RptAgropecuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacionRptAgropecuario
		/// </returns>
		public int Delete(entRptAgropecuario obj, ref cTrans localTrans)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(entRptAgropecuario.Fields.gestion.ToString());
				arrColumnasWhere.Add(entRptAgropecuario.Fields.id_persona.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.gestion);
				arrValoresWhere.Add(obj.id_persona);

			
				cConn local = new cConn();
				return local.deleteBD("RptAgropecuario", arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla RptAgropecuario a partir de una clase del tipo eRptAgropecuario
		/// </summary>
		/// <param name="ArrColumnasWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="ArrValoresWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Valores para cada una de las columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionRptAgropecuario
		/// </returns>
		public int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				cConn local = new cConn();
				return local.deleteBD("RptAgropecuario", arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla RptAgropecuario a partir de una clase del tipo eRptAgropecuario
		/// </summary>
		/// <param name="ArrColumnasWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="ArrValoresWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Valores para cada una de las columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="localTrans" type="Clases.Conexion.cTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion RptAgropecuario
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacionRptAgropecuario
		/// </returns>
		public int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
		{
			try
			{
				cConn local = new cConn();
				return local.deleteBD("RptAgropecuario", arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		

		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla rpt_agropecuario
		/// </summary>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de rpt_agropecuario
		/// </returns>
		public DataTable NuevoDataTable()
		{
			try
			{
				DataTable table = new DataTable ();
				DataColumn dc;
				dc = new DataColumn(entRptAgropecuario.Fields.gestion.ToString(),typeof(entRptAgropecuario).GetProperty(entRptAgropecuario.Fields.gestion.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptAgropecuario.Fields.mes.ToString(),typeof(entRptAgropecuario).GetProperty(entRptAgropecuario.Fields.mes.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptAgropecuario.Fields.foto.ToString(),typeof(entRptAgropecuario).GetProperty(entRptAgropecuario.Fields.foto.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptAgropecuario.Fields.id_departamento.ToString(),typeof(entRptAgropecuario).GetProperty(entRptAgropecuario.Fields.id_departamento.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptAgropecuario.Fields.id_upmipp.ToString(),typeof(entRptAgropecuario).GetProperty(entRptAgropecuario.Fields.id_upmipp.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptAgropecuario.Fields.id_provincia.ToString(),typeof(entRptAgropecuario).GetProperty(entRptAgropecuario.Fields.id_provincia.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptAgropecuario.Fields.id_municipio.ToString(),typeof(entRptAgropecuario).GetProperty(entRptAgropecuario.Fields.id_municipio.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptAgropecuario.Fields.id_listado.ToString(),typeof(entRptAgropecuario).GetProperty(entRptAgropecuario.Fields.id_listado.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptAgropecuario.Fields.id_boleta.ToString(),typeof(entRptAgropecuario).GetProperty(entRptAgropecuario.Fields.id_boleta.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptAgropecuario.Fields.id_persona.ToString(),typeof(entRptAgropecuario).GetProperty(entRptAgropecuario.Fields.id_persona.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptAgropecuario.Fields.id_movimiento.ToString(),typeof(entRptAgropecuario).GetProperty(entRptAgropecuario.Fields.id_movimiento.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptAgropecuario.Fields.departamento.ToString(),typeof(entRptAgropecuario).GetProperty(entRptAgropecuario.Fields.departamento.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptAgropecuario.Fields.provincia.ToString(),typeof(entRptAgropecuario).GetProperty(entRptAgropecuario.Fields.provincia.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptAgropecuario.Fields.municipio.ToString(),typeof(entRptAgropecuario).GetProperty(entRptAgropecuario.Fields.municipio.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptAgropecuario.Fields.comunidad.ToString(),typeof(entRptAgropecuario).GetProperty(entRptAgropecuario.Fields.comunidad.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptAgropecuario.Fields.login.ToString(),typeof(entRptAgropecuario).GetProperty(entRptAgropecuario.Fields.login.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptAgropecuario.Fields.es_reemplazo.ToString(),typeof(entRptAgropecuario).GetProperty(entRptAgropecuario.Fields.es_reemplazo.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptAgropecuario.Fields.posible_reemplazo.ToString(),typeof(entRptAgropecuario).GetProperty(entRptAgropecuario.Fields.posible_reemplazo.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptAgropecuario.Fields.producto_boleta.ToString(),typeof(entRptAgropecuario).GetProperty(entRptAgropecuario.Fields.producto_boleta.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptAgropecuario.Fields.producto.ToString(),typeof(entRptAgropecuario).GetProperty(entRptAgropecuario.Fields.producto.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptAgropecuario.Fields.codigo_producto.ToString(),typeof(entRptAgropecuario).GetProperty(entRptAgropecuario.Fields.codigo_producto.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptAgropecuario.Fields.codigo_ccp.ToString(),typeof(entRptAgropecuario).GetProperty(entRptAgropecuario.Fields.codigo_ccp.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptAgropecuario.Fields.informante.ToString(),typeof(entRptAgropecuario).GetProperty(entRptAgropecuario.Fields.informante.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptAgropecuario.Fields.celular.ToString(),typeof(entRptAgropecuario).GetProperty(entRptAgropecuario.Fields.celular.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptAgropecuario.Fields.direccion.ToString(),typeof(entRptAgropecuario).GetProperty(entRptAgropecuario.Fields.direccion.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptAgropecuario.Fields.carac_adic_original.ToString(),typeof(entRptAgropecuario).GetProperty(entRptAgropecuario.Fields.carac_adic_original.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptAgropecuario.Fields.variedad_tipo_original.ToString(),typeof(entRptAgropecuario).GetProperty(entRptAgropecuario.Fields.variedad_tipo_original.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptAgropecuario.Fields.talla_original.ToString(),typeof(entRptAgropecuario).GetProperty(entRptAgropecuario.Fields.talla_original.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptAgropecuario.Fields.cantidad_original.ToString(),typeof(entRptAgropecuario).GetProperty(entRptAgropecuario.Fields.cantidad_original.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptAgropecuario.Fields.unidad_original.ToString(),typeof(entRptAgropecuario).GetProperty(entRptAgropecuario.Fields.unidad_original.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptAgropecuario.Fields.precio_unit_original.ToString(),typeof(entRptAgropecuario).GetProperty(entRptAgropecuario.Fields.precio_unit_original.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptAgropecuario.Fields.observaciones.ToString(),typeof(entRptAgropecuario).GetProperty(entRptAgropecuario.Fields.observaciones.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptAgropecuario.Fields.minfec.ToString(),typeof(entRptAgropecuario).GetProperty(entRptAgropecuario.Fields.minfec.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptAgropecuario.Fields.maxfec.ToString(),typeof(entRptAgropecuario).GetProperty(entRptAgropecuario.Fields.maxfec.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptAgropecuario.Fields.duracion.ToString(),typeof(entRptAgropecuario).GetProperty(entRptAgropecuario.Fields.duracion.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptAgropecuario.Fields.carac_adic.ToString(),typeof(entRptAgropecuario).GetProperty(entRptAgropecuario.Fields.carac_adic.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptAgropecuario.Fields.variedad_tipo.ToString(),typeof(entRptAgropecuario).GetProperty(entRptAgropecuario.Fields.variedad_tipo.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptAgropecuario.Fields.talla.ToString(),typeof(entRptAgropecuario).GetProperty(entRptAgropecuario.Fields.talla.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptAgropecuario.Fields.cantidad.ToString(),typeof(entRptAgropecuario).GetProperty(entRptAgropecuario.Fields.cantidad.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptAgropecuario.Fields.unidad.ToString(),typeof(entRptAgropecuario).GetProperty(entRptAgropecuario.Fields.unidad.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptAgropecuario.Fields.precio_unit.ToString(),typeof(entRptAgropecuario).GetProperty(entRptAgropecuario.Fields.precio_unit.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptAgropecuario.Fields.cant_equiv_original.ToString(),typeof(entRptAgropecuario).GetProperty(entRptAgropecuario.Fields.cant_equiv_original.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptAgropecuario.Fields.unid_equiv_original.ToString(),typeof(entRptAgropecuario).GetProperty(entRptAgropecuario.Fields.unid_equiv_original.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptAgropecuario.Fields.cant_equiv.ToString(),typeof(entRptAgropecuario).GetProperty(entRptAgropecuario.Fields.cant_equiv.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptAgropecuario.Fields.unid_equiv.ToString(),typeof(entRptAgropecuario).GetProperty(entRptAgropecuario.Fields.unid_equiv.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(entRptAgropecuario.Fields.codigo_variedad.ToString(),typeof(entRptAgropecuario).GetProperty(entRptAgropecuario.Fields.codigo_variedad.ToString()).PropertyType);
				table.Columns.Add(dc);

				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que genera un DataTable con determinadas columnas de una rpt_agropecuario
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de rpt_agropecuario
		/// </returns>
		public DataTable NuevoDataTable(ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'2'");
				
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entRptAgropecuario.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla rpt_agropecuario
		/// </summary>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de rpt_agropecuario
		/// </returns>
		public DataTable CargarDataTable()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return CargarDataTable(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla y n condicion WHERE rpt_agropecuario
		/// </summary>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de rpt_agropecuario
		/// </returns>
		public DataTable CargarDataTable(String strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return CargarDataTable(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla rpt_agropecuario
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de rpt_agropecuario
		/// </returns>
		public DataTable CargarDataTable(ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				return CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla rpt_agropecuario
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de rpt_agropecuario
		/// </returns>
		public DataTable CargarDataTable(ArrayList arrColumnas, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				return CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla rpt_agropecuario
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de rpt_agropecuario
		/// </returns>
		public DataTable CargarDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla rpt_agropecuario
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de rpt_agropecuario
		/// </returns>
		public DataTable CargarDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return CargarDataTable(arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla rpt_agropecuario
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de rpt_agropecuario
		/// </returns>
		public DataTable CargarDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entRptAgropecuario.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla rpt_agropecuario
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de rpt_agropecuario
		/// </returns>
		public DataTable CargarDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entRptAgropecuario.Fields.gestion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.mes.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.foto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_upmipp.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_provincia.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_municipio.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_listado.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_boleta.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_persona.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_movimiento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.departamento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.provincia.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.municipio.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.comunidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.login.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.es_reemplazo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.posible_reemplazo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.producto_boleta.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.producto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_producto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_ccp.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.informante.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.celular.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.direccion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.carac_adic_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.variedad_tipo_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.talla_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cantidad_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unidad_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.precio_unit_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.observaciones.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.minfec.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.maxfec.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.duracion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.carac_adic.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.variedad_tipo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.talla.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cantidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.precio_unit.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cant_equiv_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unid_equiv_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cant_equiv.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unid_equiv.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_variedad.ToString());
				
				return CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="searchField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable CargarDataTable(entRptAgropecuario.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return CargarDataTable(arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="searchField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable CargarDataTable(entRptAgropecuario.Fields searchField, object searchValue, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return CargarDataTable(arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable CargarDataTable(ArrayList arrColumnas, entRptAgropecuario.Fields searchField, object searchValue, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable CargarDataTable(ArrayList arrColumnas, entRptAgropecuario.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla rpt_agropecuario
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de rpt_agropecuario
		/// </returns>
		public DataTable CargarDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entRptAgropecuario.Fields.gestion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.mes.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.foto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_upmipp.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_provincia.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_municipio.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_listado.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_boleta.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_persona.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_movimiento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.departamento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.provincia.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.municipio.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.comunidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.login.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.es_reemplazo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.posible_reemplazo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.producto_boleta.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.producto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_producto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_ccp.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.informante.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.celular.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.direccion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.carac_adic_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.variedad_tipo_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.talla_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cantidad_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unidad_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.precio_unit_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.observaciones.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.minfec.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.maxfec.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.duracion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.carac_adic.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.variedad_tipo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.talla.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cantidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.precio_unit.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cant_equiv_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unid_equiv_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cant_equiv.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unid_equiv.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_variedad.ToString());
				return CargarDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla rpt_agropecuario
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de rpt_agropecuario
		/// </returns>
		public DataTable CargarDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return CargarDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla rpt_agropecuario
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de rpt_agropecuario
		/// </returns>
		public DataTable CargarDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DataTable table = local.cargarDataTableOr(cParametros.schema + entRptAgropecuario.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla rpt_agropecuario
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla rpt_agropecuariorpt_agropecuario
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla rpt_agropecuario
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla rpt_agropecuariorpt_agropecuario
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla rpt_agropecuario
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla rpt_agropecuariorpt_agropecuario
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla rpt_agropecuario
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla rpt_agropecuariorpt_agropecuario
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entRptAgropecuario.Fields.gestion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.mes.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla rpt_agropecuario
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla rpt_agropecuariorpt_agropecuario
		///     </para>
		/// </param>
		/// <param name="valueField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entRptAgropecuario.Fields valueField, entRptAgropecuario.Fields textField)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla rpt_agropecuario
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla rpt_agropecuariorpt_agropecuario
		///     </para>
		/// </param>
		/// <param name="valueField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entRptAgropecuario.Fields valueField, entRptAgropecuario.Fields textField, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla rpt_agropecuario
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla rpt_agropecuariorpt_agropecuario
		///     </para>
		/// </param>
		/// <param name="valueField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entRptAgropecuario.Fields valueField, String textField)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla rpt_agropecuario
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla rpt_agropecuariorpt_agropecuario
		///     </para>
		/// </param>
		/// <param name="valueField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entRptAgropecuario.Fields valueField, String textField, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla rpt_agropecuario
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla rpt_agropecuariorpt_agropecuario
		///     </para>
		/// </param>
		/// <param name="valueField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entRptAgropecuario.Fields valueField, entRptAgropecuario.Fields textField, entRptAgropecuario.Fields searchField, object searchValue)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField.ToString(), searchField, searchValue);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla rpt_agropecuario
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla rpt_agropecuariorpt_agropecuario
		///     </para>
		/// </param>
		/// <param name="valueField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entRptAgropecuario.Fields valueField, String textField, entRptAgropecuario.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla rpt_agropecuario
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla rpt_agropecuariorpt_agropecuario
		///     </para>
		/// </param>
		/// <param name="valueField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entRptAgropecuario.Fields valueField, entRptAgropecuario.Fields textField, entRptAgropecuario.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla rpt_agropecuario
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla rpt_agropecuariorpt_agropecuario
		///     </para>
		/// </param>
		/// <param name="valueField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entRptAgropecuario.Fields valueField, String textField, entRptAgropecuario.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla rpt_agropecuario
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla rpt_agropecuariorpt_agropecuario
		///     </para>
		/// </param>
		/// <param name="valueField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entRptAgropecuario.Fields valueField, entRptAgropecuario.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla rpt_agropecuario
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla rpt_agropecuariorpt_agropecuario
		///     </para>
		/// </param>
		/// <param name="valueField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entRptAgropecuario.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla rpt_agropecuario
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla rpt_agropecuariorpt_agropecuario
		///     </para>
		/// </param>
		/// <param name="valueField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entRptAgropecuario.Fields valueField, entRptAgropecuario.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla rpt_agropecuario
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla rpt_agropecuariorpt_agropecuario
		///     </para>
		/// </param>
		/// <param name="valueField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, entRptAgropecuario.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla rpt_agropecuario
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla rpt_agropecuariorpt_agropecuario
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DataTable table = local.cargarDataTableAnd(cParametros.schema + entRptAgropecuario.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Columns.Count > 0)
				{
					cmb.ValueMember = table.Columns[0].ColumnName;
					cmb.DisplayMember = table.Columns[1].ColumnName;
					cmb.DataSource = table;

				}
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla rpt_agropecuario
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entRptAgropecuario.Fields.gestion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.mes.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.foto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_upmipp.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_provincia.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_municipio.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_listado.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_boleta.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_persona.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_movimiento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.departamento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.provincia.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.municipio.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.comunidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.login.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.es_reemplazo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.posible_reemplazo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.producto_boleta.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.producto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_producto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_ccp.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.informante.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.celular.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.direccion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.carac_adic_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.variedad_tipo_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.talla_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cantidad_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unidad_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.precio_unit_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.observaciones.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.minfec.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.maxfec.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.duracion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.carac_adic.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.variedad_tipo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.talla.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cantidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.precio_unit.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cant_equiv_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unid_equiv_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cant_equiv.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unid_equiv.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_variedad.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla rpt_agropecuario
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		  Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entRptAgropecuario.Fields.gestion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.mes.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.foto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_upmipp.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_provincia.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_municipio.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_listado.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_boleta.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_persona.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_movimiento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.departamento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.provincia.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.municipio.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.comunidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.login.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.es_reemplazo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.posible_reemplazo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.producto_boleta.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.producto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_producto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_ccp.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.informante.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.celular.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.direccion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.carac_adic_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.variedad_tipo_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.talla_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cantidad_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unidad_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.precio_unit_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.observaciones.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.minfec.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.maxfec.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.duracion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.carac_adic.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.variedad_tipo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.talla.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cantidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.precio_unit.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cant_equiv_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unid_equiv_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cant_equiv.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unid_equiv.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_variedad.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla rpt_agropecuario
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla rpt_agropecuario
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla rpt_agropecuario
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entRptAgropecuario.Fields.gestion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.mes.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.foto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_upmipp.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_provincia.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_municipio.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_listado.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_boleta.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_persona.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_movimiento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.departamento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.provincia.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.municipio.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.comunidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.login.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.es_reemplazo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.posible_reemplazo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.producto_boleta.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.producto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_producto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_ccp.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.informante.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.celular.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.direccion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.carac_adic_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.variedad_tipo_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.talla_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cantidad_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unidad_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.precio_unit_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.observaciones.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.minfec.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.maxfec.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.duracion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.carac_adic.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.variedad_tipo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.talla.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cantidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.precio_unit.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cant_equiv_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unid_equiv_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cant_equiv.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unid_equiv.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_variedad.ToString());
			
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla rpt_agropecuario
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entRptAgropecuario.Fields.gestion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.mes.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.foto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_upmipp.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_provincia.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_municipio.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_listado.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_boleta.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_persona.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_movimiento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.departamento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.provincia.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.municipio.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.comunidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.login.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.es_reemplazo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.posible_reemplazo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.producto_boleta.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.producto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_producto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_ccp.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.informante.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.celular.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.direccion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.carac_adic_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.variedad_tipo_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.talla_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cantidad_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unidad_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.precio_unit_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.observaciones.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.minfec.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.maxfec.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.duracion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.carac_adic.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.variedad_tipo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.talla.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cantidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.precio_unit.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cant_equiv_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unid_equiv_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cant_equiv.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unid_equiv.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_variedad.ToString());
			
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla rpt_agropecuario
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla rpt_agropecuario
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DbDataReader dsReader = local.cargarDataReaderAnd(cParametros.schema + entRptAgropecuario.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				dtg.DataSource = dsReader;

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla rpt_agropecuario
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, entRptAgropecuario.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entRptAgropecuario.Fields.gestion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.mes.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.foto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_upmipp.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_provincia.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_municipio.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_listado.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_boleta.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_persona.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_movimiento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.departamento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.provincia.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.municipio.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.comunidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.login.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.es_reemplazo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.posible_reemplazo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.producto_boleta.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.producto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_producto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_ccp.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.informante.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.celular.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.direccion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.carac_adic_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.variedad_tipo_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.talla_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cantidad_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unidad_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.precio_unit_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.observaciones.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.minfec.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.maxfec.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.duracion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.carac_adic.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.variedad_tipo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.talla.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cantidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.precio_unit.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cant_equiv_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unid_equiv_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cant_equiv.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unid_equiv.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_variedad.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla rpt_agropecuario
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, entRptAgropecuario.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(entRptAgropecuario.Fields.gestion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.mes.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.foto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_departamento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_upmipp.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_provincia.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_municipio.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_listado.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_boleta.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_persona.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.id_movimiento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.departamento.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.provincia.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.municipio.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.comunidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.login.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.es_reemplazo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.posible_reemplazo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.producto_boleta.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.producto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_producto.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_ccp.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.informante.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.celular.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.direccion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.carac_adic_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.variedad_tipo_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.talla_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cantidad_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unidad_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.precio_unit_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.observaciones.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.minfec.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.maxfec.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.duracion.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.carac_adic.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.variedad_tipo.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.talla.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cantidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unidad.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.precio_unit.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cant_equiv_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unid_equiv_original.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.cant_equiv.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.unid_equiv.ToString());
				arrColumnas.Add(entRptAgropecuario.Fields.codigo_variedad.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla rpt_agropecuario
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entRptAgropecuario.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla rpt_agropecuario
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entRptAgropecuario.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla rpt_agropecuario
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarDataGridOr(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla rpt_agropecuario
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				cConn local = new cConn();
				DbDataReader dsReader = local.cargarDataReaderOr(cParametros.schema + entRptAgropecuario.strNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				dtg.DataSource = dsReader;

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entRptAgropecuario que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesCount(entRptAgropecuario.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entRptAgropecuario que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesCount(entRptAgropecuario.Fields refField, entRptAgropecuario.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entRptAgropecuario que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesCount(entRptAgropecuario.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("count(" + refField + ")");
				DataTable dtTemp = CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return dtTemp.Rows[0][0];
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entRptAgropecuario que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesMin(entRptAgropecuario.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entRptAgropecuario que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesMin(entRptAgropecuario.Fields refField, entRptAgropecuario.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entRptAgropecuario que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesMin(entRptAgropecuario.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("min(" + refField + ")");
				DataTable dtTemp = CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return dtTemp.Rows[0][0];
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entRptAgropecuario que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesMax(entRptAgropecuario.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entRptAgropecuario que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesMax(entRptAgropecuario.Fields refField, entRptAgropecuario.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entRptAgropecuario que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesMax(entRptAgropecuario.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("max(" + refField + ")");
				DataTable dtTemp = CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return dtTemp.Rows[0][0];
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entRptAgropecuario que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesSum(entRptAgropecuario.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entRptAgropecuario que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesSum(entRptAgropecuario.Fields refField, entRptAgropecuario.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entRptAgropecuario que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesSum(entRptAgropecuario.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("sum(" + refField + ")");
				DataTable dtTemp = CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return dtTemp.Rows[0][0];
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entRptAgropecuario que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesAvg(entRptAgropecuario.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entRptAgropecuario que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesAvg(entRptAgropecuario.Fields refField, entRptAgropecuario.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo entRptAgropecuario que cumple con los filtros de los parametros
		/// </returns>
		public object FuncionesAvg(entRptAgropecuario.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("avg(" + refField + ")");
				DataTable dtTemp = CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				return dtTemp.Rows[0][0];
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion

		/// <summary>
		/// 	 Funcion que devuelve un objeto a partir de un DataRow
		/// </summary>
		/// <param name="row" type="System.Data.DataRow">
		///     <para>
		/// 		 DataRow con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Objeto rpt_agropecuario
		/// </returns>
		internal entRptAgropecuario crearObjeto(DataRow row)
		{
			entRptAgropecuario obj = new entRptAgropecuario();
			obj.gestion = GetColumnType(row[entRptAgropecuario.Fields.gestion.ToString()], entRptAgropecuario.Fields.gestion);
			obj.mes = GetColumnType(row[entRptAgropecuario.Fields.mes.ToString()], entRptAgropecuario.Fields.mes);
			obj.foto = GetColumnType(row[entRptAgropecuario.Fields.foto.ToString()], entRptAgropecuario.Fields.foto);
			obj.id_departamento = GetColumnType(row[entRptAgropecuario.Fields.id_departamento.ToString()], entRptAgropecuario.Fields.id_departamento);
			obj.id_upmipp = GetColumnType(row[entRptAgropecuario.Fields.id_upmipp.ToString()], entRptAgropecuario.Fields.id_upmipp);
			obj.id_provincia = GetColumnType(row[entRptAgropecuario.Fields.id_provincia.ToString()], entRptAgropecuario.Fields.id_provincia);
			obj.id_municipio = GetColumnType(row[entRptAgropecuario.Fields.id_municipio.ToString()], entRptAgropecuario.Fields.id_municipio);
			obj.id_listado = GetColumnType(row[entRptAgropecuario.Fields.id_listado.ToString()], entRptAgropecuario.Fields.id_listado);
			obj.id_boleta = GetColumnType(row[entRptAgropecuario.Fields.id_boleta.ToString()], entRptAgropecuario.Fields.id_boleta);
			obj.id_persona = GetColumnType(row[entRptAgropecuario.Fields.id_persona.ToString()], entRptAgropecuario.Fields.id_persona);
			obj.id_movimiento = GetColumnType(row[entRptAgropecuario.Fields.id_movimiento.ToString()], entRptAgropecuario.Fields.id_movimiento);
			obj.departamento = GetColumnType(row[entRptAgropecuario.Fields.departamento.ToString()], entRptAgropecuario.Fields.departamento);
			obj.provincia = GetColumnType(row[entRptAgropecuario.Fields.provincia.ToString()], entRptAgropecuario.Fields.provincia);
			obj.municipio = GetColumnType(row[entRptAgropecuario.Fields.municipio.ToString()], entRptAgropecuario.Fields.municipio);
			obj.comunidad = GetColumnType(row[entRptAgropecuario.Fields.comunidad.ToString()], entRptAgropecuario.Fields.comunidad);
			obj.login = GetColumnType(row[entRptAgropecuario.Fields.login.ToString()], entRptAgropecuario.Fields.login);
			obj.es_reemplazo = GetColumnType(row[entRptAgropecuario.Fields.es_reemplazo.ToString()], entRptAgropecuario.Fields.es_reemplazo);
			obj.posible_reemplazo = GetColumnType(row[entRptAgropecuario.Fields.posible_reemplazo.ToString()], entRptAgropecuario.Fields.posible_reemplazo);
			obj.producto_boleta = GetColumnType(row[entRptAgropecuario.Fields.producto_boleta.ToString()], entRptAgropecuario.Fields.producto_boleta);
			obj.producto = GetColumnType(row[entRptAgropecuario.Fields.producto.ToString()], entRptAgropecuario.Fields.producto);
			obj.codigo_producto = GetColumnType(row[entRptAgropecuario.Fields.codigo_producto.ToString()], entRptAgropecuario.Fields.codigo_producto);
			obj.codigo_ccp = GetColumnType(row[entRptAgropecuario.Fields.codigo_ccp.ToString()], entRptAgropecuario.Fields.codigo_ccp);
			obj.informante = GetColumnType(row[entRptAgropecuario.Fields.informante.ToString()], entRptAgropecuario.Fields.informante);
			obj.celular = GetColumnType(row[entRptAgropecuario.Fields.celular.ToString()], entRptAgropecuario.Fields.celular);
			obj.direccion = GetColumnType(row[entRptAgropecuario.Fields.direccion.ToString()], entRptAgropecuario.Fields.direccion);
			obj.carac_adic_original = GetColumnType(row[entRptAgropecuario.Fields.carac_adic_original.ToString()], entRptAgropecuario.Fields.carac_adic_original);
			obj.variedad_tipo_original = GetColumnType(row[entRptAgropecuario.Fields.variedad_tipo_original.ToString()], entRptAgropecuario.Fields.variedad_tipo_original);
			obj.talla_original = GetColumnType(row[entRptAgropecuario.Fields.talla_original.ToString()], entRptAgropecuario.Fields.talla_original);
			obj.cantidad_original = GetColumnType(row[entRptAgropecuario.Fields.cantidad_original.ToString()], entRptAgropecuario.Fields.cantidad_original);
			obj.unidad_original = GetColumnType(row[entRptAgropecuario.Fields.unidad_original.ToString()], entRptAgropecuario.Fields.unidad_original);
			obj.precio_unit_original = GetColumnType(row[entRptAgropecuario.Fields.precio_unit_original.ToString()], entRptAgropecuario.Fields.precio_unit_original);
			obj.observaciones = GetColumnType(row[entRptAgropecuario.Fields.observaciones.ToString()], entRptAgropecuario.Fields.observaciones);
			obj.minfec = GetColumnType(row[entRptAgropecuario.Fields.minfec.ToString()], entRptAgropecuario.Fields.minfec);
			obj.maxfec = GetColumnType(row[entRptAgropecuario.Fields.maxfec.ToString()], entRptAgropecuario.Fields.maxfec);
			obj.duracion = GetColumnType(row[entRptAgropecuario.Fields.duracion.ToString()], entRptAgropecuario.Fields.duracion);
			obj.carac_adic = GetColumnType(row[entRptAgropecuario.Fields.carac_adic.ToString()], entRptAgropecuario.Fields.carac_adic);
			obj.variedad_tipo = GetColumnType(row[entRptAgropecuario.Fields.variedad_tipo.ToString()], entRptAgropecuario.Fields.variedad_tipo);
			obj.talla = GetColumnType(row[entRptAgropecuario.Fields.talla.ToString()], entRptAgropecuario.Fields.talla);
			obj.cantidad = GetColumnType(row[entRptAgropecuario.Fields.cantidad.ToString()], entRptAgropecuario.Fields.cantidad);
			obj.unidad = GetColumnType(row[entRptAgropecuario.Fields.unidad.ToString()], entRptAgropecuario.Fields.unidad);
			obj.precio_unit = GetColumnType(row[entRptAgropecuario.Fields.precio_unit.ToString()], entRptAgropecuario.Fields.precio_unit);
			obj.cant_equiv_original = GetColumnType(row[entRptAgropecuario.Fields.cant_equiv_original.ToString()], entRptAgropecuario.Fields.cant_equiv_original);
			obj.unid_equiv_original = GetColumnType(row[entRptAgropecuario.Fields.unid_equiv_original.ToString()], entRptAgropecuario.Fields.unid_equiv_original);
			obj.cant_equiv = GetColumnType(row[entRptAgropecuario.Fields.cant_equiv.ToString()], entRptAgropecuario.Fields.cant_equiv);
			obj.unid_equiv = GetColumnType(row[entRptAgropecuario.Fields.unid_equiv.ToString()], entRptAgropecuario.Fields.unid_equiv);
			obj.codigo_variedad = GetColumnType(row[entRptAgropecuario.Fields.codigo_variedad.ToString()], entRptAgropecuario.Fields.codigo_variedad);
			return obj;
		}

		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtrpt_agropecuario" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Lista de Objetos rpt_agropecuario
		/// </returns>
		internal List<entRptAgropecuario> crearLista(DataTable dtrpt_agropecuario)
		{
			List<entRptAgropecuario> list = new List<entRptAgropecuario>();
			
			entRptAgropecuario obj = new entRptAgropecuario();
			foreach (DataRow row in dtrpt_agropecuario.Rows)
			{
				obj = crearObjeto(row);
				list.Add(obj);
			}
			return list;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtrpt_agropecuario" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cola de Objetos rpt_agropecuario
		/// </returns>
		internal Queue<entRptAgropecuario> crearCola(DataTable dtrpt_agropecuario)
		{
			Queue<entRptAgropecuario> cola = new Queue<entRptAgropecuario>();
			
			entRptAgropecuario obj = new entRptAgropecuario();
			foreach (DataRow row in dtrpt_agropecuario.Rows)
			{
				obj = crearObjeto(row);
				cola.Enqueue(obj);
			}
			return cola;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtrpt_agropecuario" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Pila de Objetos rpt_agropecuario
		/// </returns>
		internal Stack<entRptAgropecuario> crearPila(DataTable dtrpt_agropecuario)
		{
			Stack<entRptAgropecuario> pila = new Stack<entRptAgropecuario>();
			
			entRptAgropecuario obj = new entRptAgropecuario();
			foreach (DataRow row in dtrpt_agropecuario.Rows)
			{
				obj = crearObjeto(row);
				pila.Push(obj);
			}
			return pila;
		}
		
		/// <summary>
		/// 	 Funcion que crea un Dicionario a partir de un DataTable
		/// </summary>
		/// <param name="dtrpt_agropecuario" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Diccionario de Objetos rpt_agropecuario
		/// </returns>
		internal Dictionary<String, entRptAgropecuario> crearDiccionario(DataTable dtrpt_agropecuario)
		{
			Dictionary<String, entRptAgropecuario>  miDic = new Dictionary<String, entRptAgropecuario>();
			
			entRptAgropecuario obj = new entRptAgropecuario();
			foreach (DataRow row in dtrpt_agropecuario.Rows)
			{
				obj = crearObjeto(row);
				miDic.Add(obj.GetHashCode().ToString(), obj);
			}
			return miDic;
		}
		
		public void Dispose()
		{
			GC.SuppressFinalize(this);
		}
		
		protected void Finalize()
		{
			Dispose();
		}
	}
}

