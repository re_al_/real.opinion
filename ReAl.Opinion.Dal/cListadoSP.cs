namespace ReAl.Opinion.Dal
{
	public class cListadoSP
	{
		public enum RptAgropecuario
		{
			/// <summary>
			///     Proc para Insertar datos en la tabla rpt_agropecuario
			/// </summary>
			Sprpt_agropecuarioIns,
			
			/// <summary>
			///     Proc para Actualizar datos en la tabla rpt_agropecuario
			/// </summary>
			Sprpt_agropecuarioUpd,
			
			/// <summary>
			///     Proc para Eliminar datos en la tabla rpt_agropecuario
			/// </summary>
			Sprpt_agropecuarioDel,
			
			/// <summary>
			///     Proc para Insertar y/o Actualizar datos en la tabla rpt_agropecuario
			/// </summary>
			Sprpt_agropecuarioInsUpd
		}

		public enum RptInsumos
		{
			/// <summary>
			///     Proc para Insertar datos en la tabla rpt_insumos
			/// </summary>
			Sprpt_insumosIns,
			
			/// <summary>
			///     Proc para Actualizar datos en la tabla rpt_insumos
			/// </summary>
			Sprpt_insumosUpd,
			
			/// <summary>
			///     Proc para Eliminar datos en la tabla rpt_insumos
			/// </summary>
			Sprpt_insumosDel,
			
			/// <summary>
			///     Proc para Insertar y/o Actualizar datos en la tabla rpt_insumos
			/// </summary>
			Sprpt_insumosInsUpd
		}

		public enum SegRol
		{
			/// <summary>
			///     Proc para Insertar datos en la tabla seg_rol
			/// </summary>
			Spseg_rolIns,
			
			/// <summary>
			///     Proc para Actualizar datos en la tabla seg_rol
			/// </summary>
			Spseg_rolUpd,
			
			/// <summary>
			///     Proc para Eliminar datos en la tabla seg_rol
			/// </summary>
			Spseg_rolDel,
			
			/// <summary>
			///     Proc para Insertar y/o Actualizar datos en la tabla seg_rol
			/// </summary>
			Spseg_rolInsUpd
		}

		public enum SegUsuario
		{
			/// <summary>
			///     Proc para Insertar datos en la tabla seg_usuario
			/// </summary>
			Spseg_usuarioIns,
			
			/// <summary>
			///     Proc para Actualizar datos en la tabla seg_usuario
			/// </summary>
			Spseg_usuarioUpd,
			
			/// <summary>
			///     Proc para Eliminar datos en la tabla seg_usuario
			/// </summary>
			Spseg_usuarioDel,
			
			/// <summary>
			///     Proc para Insertar y/o Actualizar datos en la tabla seg_usuario
			/// </summary>
			Spseg_usuarioInsUpd
		}

		public enum SegUsuariorestriccion
		{
			/// <summary>
			///     Proc para Insertar datos en la tabla seg_usuariorestriccion
			/// </summary>
			Spseg_usuariorestriccionIns,
			
			/// <summary>
			///     Proc para Actualizar datos en la tabla seg_usuariorestriccion
			/// </summary>
			Spseg_usuariorestriccionUpd,
			
			/// <summary>
			///     Proc para Eliminar datos en la tabla seg_usuariorestriccion
			/// </summary>
			Spseg_usuariorestriccionDel,
			
			/// <summary>
			///     Proc para Insertar y/o Actualizar datos en la tabla seg_usuariorestriccion
			/// </summary>
			Spseg_usuariorestriccionInsUpd
		}

	}
}

