#region 
/***********************************************************************************************************
	NOMBRE:       entEncRespuesta
	DESCRIPCION:
		Clase que define un objeto para la Tabla enc_respuesta

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.Opinion.Dal.Entidades
{
	public class entEncRespuesta : cBaseClass
	{
		public const String strNombreTabla = "enc_respuesta";
		public const String strAliasTabla = "enc_respuesta";
		public enum Fields
		{
			id_respuesta
			,id_pregunta
			,codigo
			,respuesta
			,apiestado
			,usucre
			,feccre
			,usumod
			,fecmod

		}
		
		#region Constructoress
		
		public entEncRespuesta()
		{
			//Inicializacion de Variables
			this.id_pregunta = null;
			this.codigo = null;
			this.respuesta = null;
			this.apiestado = null;
			this.usucre = null;
			this.usumod = null;
			this.fecmod = null;
		}
		
		public entEncRespuesta(entEncRespuesta obj)
		{
			this.id_respuesta = obj.id_respuesta;
			this.id_pregunta = obj.id_pregunta;
			this.codigo = obj.codigo;
			this.respuesta = obj.respuesta;
			this.apiestado = obj.apiestado;
			this.usucre = obj.usucre;
			this.feccre = obj.feccre;
			this.usumod = obj.usumod;
			this.fecmod = obj.fecmod;
		}
		
		#endregion
		
		/// <summary>
		/// 	 Variable local de tipo Int64 que representa a la columna id_respuesta de la Tabla enc_respuesta
		/// </summary>
		private Int64 _id_respuesta;
		/// <summary>
		/// 	 Representa al identificador unica del registro en la tabla
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: Yes
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_respuesta", Description = "Representa al identificador unica del registro en la tabla")]
		[Required(ErrorMessage = "id_respuesta es un campo requerido.")]
		[Key]
		public Int64 id_respuesta
		{
			get {return _id_respuesta;}
			set
			{
				if (_id_respuesta != value)
				{
					RaisePropertyChanging(entEncRespuesta.Fields.id_respuesta.ToString());
					_id_respuesta = value;
					RaisePropertyChanged(entEncRespuesta.Fields.id_respuesta.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo Int64 que representa a la columna id_pregunta de la Tabla enc_respuesta
		/// </summary>
		private Int64? _id_pregunta;
		/// <summary>
		/// 	 Representa al identificador de la tabla enc_pregunta al que esta asociado este registro
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: Yes
		/// </summary>
		[Display(Name = "id_pregunta", Description = "Representa al identificador de la tabla enc_pregunta al que esta asociado este registro")]
		public Int64? id_pregunta
		{
			get {return _id_pregunta;}
			set
			{
				if (_id_pregunta != value)
				{
					RaisePropertyChanging(entEncRespuesta.Fields.id_pregunta.ToString());
					_id_pregunta = value;
					RaisePropertyChanged(entEncRespuesta.Fields.id_pregunta.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna codigo de la Tabla enc_respuesta
		/// </summary>
		private String _codigo;
		/// <summary>
		/// 	 Codigo de la respuesta
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "codigo", Description = "Codigo de la respuesta")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "codigo es un campo requerido.")]
		public String codigo
		{
			get {return _codigo;}
			set
			{
				if (_codigo != value)
				{
					RaisePropertyChanging(entEncRespuesta.Fields.codigo.ToString());
					_codigo = value;
					RaisePropertyChanged(entEncRespuesta.Fields.codigo.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna respuesta de la Tabla enc_respuesta
		/// </summary>
		private String _respuesta;
		/// <summary>
		/// 	 Texto de la respuesta
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "respuesta", Description = "Texto de la respuesta")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "respuesta es un campo requerido.")]
		public String respuesta
		{
			get {return _respuesta;}
			set
			{
				if (_respuesta != value)
				{
					RaisePropertyChanging(entEncRespuesta.Fields.respuesta.ToString());
					_respuesta = value;
					RaisePropertyChanged(entEncRespuesta.Fields.respuesta.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna apiestado de la Tabla enc_respuesta
		/// </summary>
		private String _apiestado;
		/// <summary>
		/// 	 Estado en el que se encuentra el registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "apiestado", Description = "Estado en el que se encuentra el registro")]
		[EnumDataType(typeof(cApi.Estado))]
		public String apiestado
		{
			get {return _apiestado;}
			set
			{
				if (_apiestado != value)
				{
					RaisePropertyChanging(entEncRespuesta.Fields.apiestado.ToString());
					_apiestado = value;
					RaisePropertyChanged(entEncRespuesta.Fields.apiestado.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usucre de la Tabla enc_respuesta
		/// </summary>
		private String _usucre;
		/// <summary>
		/// 	 Login o nombre de usuario que ha creado el registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usucre", Description = "Login o nombre de usuario que ha creado el registro")]
		public String usucre
		{
			get {return _usucre;}
			set
			{
				if (_usucre != value)
				{
					RaisePropertyChanging(entEncRespuesta.Fields.usucre.ToString());
					_usucre = value;
					RaisePropertyChanged(entEncRespuesta.Fields.usucre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna feccre de la Tabla enc_respuesta
		/// </summary>
		private DateTime _feccre;
		/// <summary>
		/// 	 Fecha de creacion del registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "feccre", Description = "Fecha de creacion del registro")]
		public DateTime feccre
		{
			get {return _feccre;}
			set
			{
				if (_feccre != value)
				{
					RaisePropertyChanging(entEncRespuesta.Fields.feccre.ToString());
					_feccre = value;
					RaisePropertyChanged(entEncRespuesta.Fields.feccre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usumod de la Tabla enc_respuesta
		/// </summary>
		private String _usumod;
		/// <summary>
		/// 	 Login o nombre de usuario que ha realizado la ULTIMA modificacion registro
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usumod", Description = "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro")]
		public String usumod
		{
			get {return _usumod;}
			set
			{
				if (_usumod != value)
				{
					RaisePropertyChanging(entEncRespuesta.Fields.usumod.ToString());
					_usumod = value;
					RaisePropertyChanged(entEncRespuesta.Fields.usumod.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna fecmod de la Tabla enc_respuesta
		/// </summary>
		private DateTime? _fecmod;
		/// <summary>
		/// 	 Fecha en la que se ha realizado la ULTIMA modificacion registro
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "fecmod", Description = "Fecha en la que se ha realizado la ULTIMA modificacion registro")]
		public DateTime? fecmod
		{
			get {return _fecmod;}
			set
			{
				if (_fecmod != value)
				{
					RaisePropertyChanging(entEncRespuesta.Fields.fecmod.ToString());
					_fecmod = value;
					RaisePropertyChanged(entEncRespuesta.Fields.fecmod.ToString());
				}
			}
		}


	}
}

