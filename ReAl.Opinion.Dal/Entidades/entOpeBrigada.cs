#region 
/***********************************************************************************************************
	NOMBRE:       entOpeBrigada
	DESCRIPCION:
		Clase que define un objeto para la Tabla ope_brigada

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.Opinion.Dal.Entidades
{
	public class entOpeBrigada : cBaseClass
	{
		public const String strNombreTabla = "ope_brigada";
		public const String strAliasTabla = "ope_brigada";
		public enum Fields
		{
			id_brigada
			,id_departamento
			,codigo_brigada
			,apiestado
			,usucre
			,feccre
			,usumod
			,fecmod

		}
		
		#region Constructoress
		
		public entOpeBrigada()
		{
			//Inicializacion de Variables
			this.codigo_brigada = null;
			this.apiestado = null;
			this.usucre = null;
			this.usumod = null;
			this.fecmod = null;
		}
		
		public entOpeBrigada(entOpeBrigada obj)
		{
			this.id_brigada = obj.id_brigada;
			this.id_departamento = obj.id_departamento;
			this.codigo_brigada = obj.codigo_brigada;
			this.apiestado = obj.apiestado;
			this.usucre = obj.usucre;
			this.feccre = obj.feccre;
			this.usumod = obj.usumod;
			this.fecmod = obj.fecmod;
		}
		
		#endregion
		
		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_brigada de la Tabla ope_brigada
		/// </summary>
		private int _id_brigada;
		/// <summary>
		/// 	 Es el identificador unico que representa al registro en la tabla
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: Yes
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_brigada", Description = "Es el identificador unico que representa al registro en la tabla")]
		[Required(ErrorMessage = "id_brigada es un campo requerido.")]
		[Key]
		public int id_brigada
		{
			get {return _id_brigada;}
			set
			{
				if (_id_brigada != value)
				{
					RaisePropertyChanging(entOpeBrigada.Fields.id_brigada.ToString());
					_id_brigada = value;
					RaisePropertyChanged(entOpeBrigada.Fields.id_brigada.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_departamento de la Tabla ope_brigada
		/// </summary>
		private int _id_departamento;
		/// <summary>
		/// 	 Es el identificador del Departamento al que pertenece la brigada
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: Yes
		/// </summary>
		[Display(Name = "id_departamento", Description = "Es el identificador del Departamento al que pertenece la brigada")]
		[Required(ErrorMessage = "id_departamento es un campo requerido.")]
		public int id_departamento
		{
			get {return _id_departamento;}
			set
			{
				if (_id_departamento != value)
				{
					RaisePropertyChanging(entOpeBrigada.Fields.id_departamento.ToString());
					_id_departamento = value;
					RaisePropertyChanged(entOpeBrigada.Fields.id_departamento.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna codigo_brigada de la Tabla ope_brigada
		/// </summary>
		private String _codigo_brigada;
		/// <summary>
		/// 	 Codigo que representa a la brigada
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(20, MinimumLength=0)]
		[Display(Name = "codigo_brigada", Description = "Codigo que representa a la brigada")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "codigo_brigada es un campo requerido.")]
		public String codigo_brigada
		{
			get {return _codigo_brigada;}
			set
			{
				if (_codigo_brigada != value)
				{
					RaisePropertyChanging(entOpeBrigada.Fields.codigo_brigada.ToString());
					_codigo_brigada = value;
					RaisePropertyChanged(entOpeBrigada.Fields.codigo_brigada.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna apiestado de la Tabla ope_brigada
		/// </summary>
		private String _apiestado;
		/// <summary>
		/// 	 Estado en el que se encuentra el registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "apiestado", Description = "Estado en el que se encuentra el registro")]
		[EnumDataType(typeof(cApi.Estado))]
		public String apiestado
		{
			get {return _apiestado;}
			set
			{
				if (_apiestado != value)
				{
					RaisePropertyChanging(entOpeBrigada.Fields.apiestado.ToString());
					_apiestado = value;
					RaisePropertyChanged(entOpeBrigada.Fields.apiestado.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usucre de la Tabla ope_brigada
		/// </summary>
		private String _usucre;
		/// <summary>
		/// 	 Login o nombre de usuario que ha creado el registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usucre", Description = "Login o nombre de usuario que ha creado el registro")]
		public String usucre
		{
			get {return _usucre;}
			set
			{
				if (_usucre != value)
				{
					RaisePropertyChanging(entOpeBrigada.Fields.usucre.ToString());
					_usucre = value;
					RaisePropertyChanged(entOpeBrigada.Fields.usucre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna feccre de la Tabla ope_brigada
		/// </summary>
		private DateTime _feccre;
		/// <summary>
		/// 	 Fecha de creacion del registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "feccre", Description = "Fecha de creacion del registro")]
		public DateTime feccre
		{
			get {return _feccre;}
			set
			{
				if (_feccre != value)
				{
					RaisePropertyChanging(entOpeBrigada.Fields.feccre.ToString());
					_feccre = value;
					RaisePropertyChanged(entOpeBrigada.Fields.feccre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usumod de la Tabla ope_brigada
		/// </summary>
		private String _usumod;
		/// <summary>
		/// 	 Login o nombre de usuario que ha realizado la ULTIMA modificacion registro
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usumod", Description = "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro")]
		public String usumod
		{
			get {return _usumod;}
			set
			{
				if (_usumod != value)
				{
					RaisePropertyChanging(entOpeBrigada.Fields.usumod.ToString());
					_usumod = value;
					RaisePropertyChanged(entOpeBrigada.Fields.usumod.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna fecmod de la Tabla ope_brigada
		/// </summary>
		private DateTime? _fecmod;
		/// <summary>
		/// 	 Fecha en la que se ha realizado la ULTIMA modificacion registro
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "fecmod", Description = "Fecha en la que se ha realizado la ULTIMA modificacion registro")]
		public DateTime? fecmod
		{
			get {return _fecmod;}
			set
			{
				if (_fecmod != value)
				{
					RaisePropertyChanging(entOpeBrigada.Fields.fecmod.ToString());
					_fecmod = value;
					RaisePropertyChanged(entOpeBrigada.Fields.fecmod.ToString());
				}
			}
		}


	}
}

