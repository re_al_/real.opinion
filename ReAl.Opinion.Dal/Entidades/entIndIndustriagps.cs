#region 
/***********************************************************************************************************
	NOMBRE:       entIndIndustriagps
	DESCRIPCION:
		Clase que define un objeto para la Tabla ind_industriagps

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/05/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.Opinion.Dal.Entidades
{
	public class entIndIndustriagps : cBaseClass
	{
		public const String strNombreTabla = "ind_industriagps";
		public const String strAliasTabla = "ind_industriagps";
		public enum Fields
		{
			gestion
			,mes
			,id_gps
			,id_industria
			,latitud
			,longitud
			,hora_control
			,apiestado
			,apitransaccion
			,usucre
			,feccre
			,usumod
			,fecmod
			,id_manzana
			,codigo_manzana
			,usu_manzana
			,fec_manzana

		}
		
		#region Constructoress
		
		public entIndIndustriagps()
		{
			//Inicializacion de Variables
			this.apiestado = null;
			this.apitransaccion = null;
			this.usucre = null;
			this.usumod = null;
			this.fecmod = null;
			this.id_manzana = null;
			this.codigo_manzana = null;
			this.usu_manzana = null;
			this.fec_manzana = null;
		}
		
		public entIndIndustriagps(entIndIndustriagps obj)
		{
			this.gestion = obj.gestion;
			this.mes = obj.mes;
			this.id_gps = obj.id_gps;
			this.id_industria = obj.id_industria;
			this.latitud = obj.latitud;
			this.longitud = obj.longitud;
			this.hora_control = obj.hora_control;
			this.apiestado = obj.apiestado;
			this.apitransaccion = obj.apitransaccion;
			this.usucre = obj.usucre;
			this.feccre = obj.feccre;
			this.usumod = obj.usumod;
			this.fecmod = obj.fecmod;
			this.id_manzana = obj.id_manzana;
			this.codigo_manzana = obj.codigo_manzana;
			this.usu_manzana = obj.usu_manzana;
			this.fec_manzana = obj.fec_manzana;
		}
		
		#endregion
		
		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna gestion de la Tabla ind_industriagps
		/// </summary>
		private int _gestion;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna gestion de la Tabla ind_industriagps
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: Yes
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "gestion", Description = " Propiedad publica de tipo int que representa a la columna gestion de la Tabla ind_industriagps")]
		[Required(ErrorMessage = "gestion es un campo requerido.")]
		[Key]
		public int gestion
		{
			get {return _gestion;}
			set
			{
				if (_gestion != value)
				{
					RaisePropertyChanging(entIndIndustriagps.Fields.gestion.ToString());
					_gestion = value;
					RaisePropertyChanged(entIndIndustriagps.Fields.gestion.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna mes de la Tabla ind_industriagps
		/// </summary>
		private int _mes;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna mes de la Tabla ind_industriagps
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: Yes
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "mes", Description = " Propiedad publica de tipo int que representa a la columna mes de la Tabla ind_industriagps")]
		[Required(ErrorMessage = "mes es un campo requerido.")]
		[Key]
		public int mes
		{
			get {return _mes;}
			set
			{
				if (_mes != value)
				{
					RaisePropertyChanging(entIndIndustriagps.Fields.mes.ToString());
					_mes = value;
					RaisePropertyChanged(entIndIndustriagps.Fields.mes.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_gps de la Tabla ind_industriagps
		/// </summary>
		private int _id_gps;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna id_gps de la Tabla ind_industriagps
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: Yes
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_gps", Description = " Propiedad publica de tipo int que representa a la columna id_gps de la Tabla ind_industriagps")]
		[Required(ErrorMessage = "id_gps es un campo requerido.")]
		[Key]
		public int id_gps
		{
			get {return _id_gps;}
			set
			{
				if (_id_gps != value)
				{
					RaisePropertyChanging(entIndIndustriagps.Fields.id_gps.ToString());
					_id_gps = value;
					RaisePropertyChanged(entIndIndustriagps.Fields.id_gps.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_industria de la Tabla ind_industriagps
		/// </summary>
		private int _id_industria;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna id_industria de la Tabla ind_industriagps
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_industria", Description = " Propiedad publica de tipo int que representa a la columna id_industria de la Tabla ind_industriagps")]
		[Required(ErrorMessage = "id_industria es un campo requerido.")]
		public int id_industria
		{
			get {return _id_industria;}
			set
			{
				if (_id_industria != value)
				{
					RaisePropertyChanging(entIndIndustriagps.Fields.id_industria.ToString());
					_id_industria = value;
					RaisePropertyChanged(entIndIndustriagps.Fields.id_industria.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo Decimal que representa a la columna latitud de la Tabla ind_industriagps
		/// </summary>
		private Decimal _latitud;
		/// <summary>
		/// 	 Propiedad publica de tipo Decimal que representa a la columna latitud de la Tabla ind_industriagps
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "latitud", Description = " Propiedad publica de tipo Decimal que representa a la columna latitud de la Tabla ind_industriagps")]
		[Required(ErrorMessage = "latitud es un campo requerido.")]
		public Decimal latitud
		{
			get {return _latitud;}
			set
			{
				if (_latitud != value)
				{
					RaisePropertyChanging(entIndIndustriagps.Fields.latitud.ToString());
					_latitud = value;
					RaisePropertyChanged(entIndIndustriagps.Fields.latitud.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo Decimal que representa a la columna longitud de la Tabla ind_industriagps
		/// </summary>
		private Decimal _longitud;
		/// <summary>
		/// 	 Propiedad publica de tipo Decimal que representa a la columna longitud de la Tabla ind_industriagps
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "longitud", Description = " Propiedad publica de tipo Decimal que representa a la columna longitud de la Tabla ind_industriagps")]
		[Required(ErrorMessage = "longitud es un campo requerido.")]
		public Decimal longitud
		{
			get {return _longitud;}
			set
			{
				if (_longitud != value)
				{
					RaisePropertyChanging(entIndIndustriagps.Fields.longitud.ToString());
					_longitud = value;
					RaisePropertyChanged(entIndIndustriagps.Fields.longitud.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna hora_control de la Tabla ind_industriagps
		/// </summary>
		private DateTime _hora_control;
		/// <summary>
		/// 	 Propiedad publica de tipo DateTime que representa a la columna hora_control de la Tabla ind_industriagps
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "hora_control", Description = " Propiedad publica de tipo DateTime que representa a la columna hora_control de la Tabla ind_industriagps")]
		[Required(ErrorMessage = "hora_control es un campo requerido.")]
		public DateTime hora_control
		{
			get {return _hora_control;}
			set
			{
				if (_hora_control != value)
				{
					RaisePropertyChanging(entIndIndustriagps.Fields.hora_control.ToString());
					_hora_control = value;
					RaisePropertyChanged(entIndIndustriagps.Fields.hora_control.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna apiestado de la Tabla ind_industriagps
		/// </summary>
		private String _apiestado;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna apiestado de la Tabla ind_industriagps
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "apiestado", Description = " Propiedad publica de tipo String que representa a la columna apiestado de la Tabla ind_industriagps")]
		[EnumDataType(typeof(cApi.Estado))]
		public String apiestado
		{
			get {return _apiestado;}
			set
			{
				if (_apiestado != value)
				{
					RaisePropertyChanging(entIndIndustriagps.Fields.apiestado.ToString());
					_apiestado = value;
					RaisePropertyChanged(entIndIndustriagps.Fields.apiestado.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna apitransaccion de la Tabla ind_industriagps
		/// </summary>
		private String _apitransaccion;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna apitransaccion de la Tabla ind_industriagps
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "apitransaccion", Description = " Propiedad publica de tipo String que representa a la columna apitransaccion de la Tabla ind_industriagps")]
		[EnumDataType(typeof(cApi.Transaccion))]
		public String apitransaccion
		{
			get {return _apitransaccion;}
			set
			{
				if (_apitransaccion != value)
				{
					RaisePropertyChanging(entIndIndustriagps.Fields.apitransaccion.ToString());
					_apitransaccion = value;
					RaisePropertyChanged(entIndIndustriagps.Fields.apitransaccion.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usucre de la Tabla ind_industriagps
		/// </summary>
		private String _usucre;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna usucre de la Tabla ind_industriagps
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: Yes
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "usucre", Description = " Propiedad publica de tipo String que representa a la columna usucre de la Tabla ind_industriagps")]
		[Key]
		public String usucre
		{
			get {return _usucre;}
			set
			{
				if (_usucre != value)
				{
					RaisePropertyChanging(entIndIndustriagps.Fields.usucre.ToString());
					_usucre = value;
					RaisePropertyChanged(entIndIndustriagps.Fields.usucre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna feccre de la Tabla ind_industriagps
		/// </summary>
		private DateTime _feccre;
		/// <summary>
		/// 	 Propiedad publica de tipo DateTime que representa a la columna feccre de la Tabla ind_industriagps
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "feccre", Description = " Propiedad publica de tipo DateTime que representa a la columna feccre de la Tabla ind_industriagps")]
		public DateTime feccre
		{
			get {return _feccre;}
			set
			{
				if (_feccre != value)
				{
					RaisePropertyChanging(entIndIndustriagps.Fields.feccre.ToString());
					_feccre = value;
					RaisePropertyChanged(entIndIndustriagps.Fields.feccre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usumod de la Tabla ind_industriagps
		/// </summary>
		private String _usumod;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna usumod de la Tabla ind_industriagps
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "usumod", Description = " Propiedad publica de tipo String que representa a la columna usumod de la Tabla ind_industriagps")]
		public String usumod
		{
			get {return _usumod;}
			set
			{
				if (_usumod != value)
				{
					RaisePropertyChanging(entIndIndustriagps.Fields.usumod.ToString());
					_usumod = value;
					RaisePropertyChanged(entIndIndustriagps.Fields.usumod.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna fecmod de la Tabla ind_industriagps
		/// </summary>
		private DateTime? _fecmod;
		/// <summary>
		/// 	 Propiedad publica de tipo DateTime que representa a la columna fecmod de la Tabla ind_industriagps
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "fecmod", Description = " Propiedad publica de tipo DateTime que representa a la columna fecmod de la Tabla ind_industriagps")]
		public DateTime? fecmod
		{
			get {return _fecmod;}
			set
			{
				if (_fecmod != value)
				{
					RaisePropertyChanging(entIndIndustriagps.Fields.fecmod.ToString());
					_fecmod = value;
					RaisePropertyChanged(entIndIndustriagps.Fields.fecmod.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna id_manzana de la Tabla ind_industriagps
		/// </summary>
		private String _id_manzana;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna id_manzana de la Tabla ind_industriagps
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_manzana", Description = " Propiedad publica de tipo String que representa a la columna id_manzana de la Tabla ind_industriagps")]
		public String id_manzana
		{
			get {return _id_manzana;}
			set
			{
				if (_id_manzana != value)
				{
					RaisePropertyChanging(entIndIndustriagps.Fields.id_manzana.ToString());
					_id_manzana = value;
					RaisePropertyChanged(entIndIndustriagps.Fields.id_manzana.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna codigo_manzana de la Tabla ind_industriagps
		/// </summary>
		private String _codigo_manzana;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna codigo_manzana de la Tabla ind_industriagps
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "codigo_manzana", Description = " Propiedad publica de tipo String que representa a la columna codigo_manzana de la Tabla ind_industriagps")]
		public String codigo_manzana
		{
			get {return _codigo_manzana;}
			set
			{
				if (_codigo_manzana != value)
				{
					RaisePropertyChanging(entIndIndustriagps.Fields.codigo_manzana.ToString());
					_codigo_manzana = value;
					RaisePropertyChanged(entIndIndustriagps.Fields.codigo_manzana.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usu_manzana de la Tabla ind_industriagps
		/// </summary>
		private String _usu_manzana;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna usu_manzana de la Tabla ind_industriagps
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=1)]
		[Display(Name = "usu_manzana", Description = " Propiedad publica de tipo String que representa a la columna usu_manzana de la Tabla ind_industriagps")]
		public String usu_manzana
		{
			get {return _usu_manzana;}
			set
			{
				if (_usu_manzana != value)
				{
					RaisePropertyChanging(entIndIndustriagps.Fields.usu_manzana.ToString());
					_usu_manzana = value;
					RaisePropertyChanged(entIndIndustriagps.Fields.usu_manzana.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna fec_manzana de la Tabla ind_industriagps
		/// </summary>
		private DateTime? _fec_manzana;
		/// <summary>
		/// 	 Propiedad publica de tipo DateTime que representa a la columna fec_manzana de la Tabla ind_industriagps
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "fec_manzana", Description = " Propiedad publica de tipo DateTime que representa a la columna fec_manzana de la Tabla ind_industriagps")]
		public DateTime? fec_manzana
		{
			get {return _fec_manzana;}
			set
			{
				if (_fec_manzana != value)
				{
					RaisePropertyChanging(entIndIndustriagps.Fields.fec_manzana.ToString());
					_fec_manzana = value;
					RaisePropertyChanged(entIndIndustriagps.Fields.fec_manzana.ToString());
				}
			}
		}


	}
}

