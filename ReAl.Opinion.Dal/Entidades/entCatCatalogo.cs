#region 
/***********************************************************************************************************
	NOMBRE:       entCatCatalogo
	DESCRIPCION:
		Clase que define un objeto para la Tabla cat_catalogo

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        05/10/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.Opinion.Dal.Entidades
{
	public class entCatCatalogo : cBaseClass
	{
		public const String strNombreTabla = "cat_catalogo";
		public const String strAliasTabla = "cat_catalogo";
		public enum Fields
		{
			id_catalogo
			,catalogo
			,codigo
			,descripcion
			,apiestado
			,usucre
			,feccre
			,usumod
			,fecmod

		}
		
		#region Constructoress
		
		public entCatCatalogo()
		{
			//Inicializacion de Variables
			this.catalogo = null;
			this.codigo = null;
			this.descripcion = null;
			this.apiestado = null;
			this.usucre = null;
			this.usumod = null;
			this.fecmod = null;
		}
		
		public entCatCatalogo(entCatCatalogo obj)
		{
			this.id_catalogo = obj.id_catalogo;
			this.catalogo = obj.catalogo;
			this.codigo = obj.codigo;
			this.descripcion = obj.descripcion;
			this.apiestado = obj.apiestado;
			this.usucre = obj.usucre;
			this.feccre = obj.feccre;
			this.usumod = obj.usumod;
			this.fecmod = obj.fecmod;
		}
		
		#endregion
		
		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_catalogo de la Tabla cat_catalogo
		/// </summary>
		private int _id_catalogo;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna id_catalogo de la Tabla cat_catalogo
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: Yes
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_catalogo", Description = " Propiedad publica de tipo int que representa a la columna id_catalogo de la Tabla cat_catalogo")]
		[Required(ErrorMessage = "id_catalogo es un campo requerido.")]
		[Key]
		public int id_catalogo
		{
			get {return _id_catalogo;}
			set
			{
				if (_id_catalogo != value)
				{
					RaisePropertyChanging(entCatCatalogo.Fields.id_catalogo.ToString());
					_id_catalogo = value;
					RaisePropertyChanged(entCatCatalogo.Fields.id_catalogo.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna catalogo de la Tabla cat_catalogo
		/// </summary>
		private String _catalogo;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna catalogo de la Tabla cat_catalogo
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "catalogo", Description = " Propiedad publica de tipo String que representa a la columna catalogo de la Tabla cat_catalogo")]
		public String catalogo
		{
			get {return _catalogo;}
			set
			{
				if (_catalogo != value)
				{
					RaisePropertyChanging(entCatCatalogo.Fields.catalogo.ToString());
					_catalogo = value;
					RaisePropertyChanged(entCatCatalogo.Fields.catalogo.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna codigo de la Tabla cat_catalogo
		/// </summary>
		private String _codigo;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna codigo de la Tabla cat_catalogo
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "codigo", Description = " Propiedad publica de tipo String que representa a la columna codigo de la Tabla cat_catalogo")]
		public String codigo
		{
			get {return _codigo;}
			set
			{
				if (_codigo != value)
				{
					RaisePropertyChanging(entCatCatalogo.Fields.codigo.ToString());
					_codigo = value;
					RaisePropertyChanged(entCatCatalogo.Fields.codigo.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna descripcion de la Tabla cat_catalogo
		/// </summary>
		private String _descripcion;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna descripcion de la Tabla cat_catalogo
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "descripcion", Description = " Propiedad publica de tipo String que representa a la columna descripcion de la Tabla cat_catalogo")]
		public String descripcion
		{
			get {return _descripcion;}
			set
			{
				if (_descripcion != value)
				{
					RaisePropertyChanging(entCatCatalogo.Fields.descripcion.ToString());
					_descripcion = value;
					RaisePropertyChanged(entCatCatalogo.Fields.descripcion.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna apiestado de la Tabla cat_catalogo
		/// </summary>
		private String _apiestado;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna apiestado de la Tabla cat_catalogo
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "apiestado", Description = " Propiedad publica de tipo String que representa a la columna apiestado de la Tabla cat_catalogo")]
		[EnumDataType(typeof(cApi.Estado))]
		public String apiestado
		{
			get {return _apiestado;}
			set
			{
				if (_apiestado != value)
				{
					RaisePropertyChanging(entCatCatalogo.Fields.apiestado.ToString());
					_apiestado = value;
					RaisePropertyChanged(entCatCatalogo.Fields.apiestado.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usucre de la Tabla cat_catalogo
		/// </summary>
		private String _usucre;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna usucre de la Tabla cat_catalogo
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usucre", Description = " Propiedad publica de tipo String que representa a la columna usucre de la Tabla cat_catalogo")]
		public String usucre
		{
			get {return _usucre;}
			set
			{
				if (_usucre != value)
				{
					RaisePropertyChanging(entCatCatalogo.Fields.usucre.ToString());
					_usucre = value;
					RaisePropertyChanged(entCatCatalogo.Fields.usucre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna feccre de la Tabla cat_catalogo
		/// </summary>
		private DateTime _feccre;
		/// <summary>
		/// 	 Propiedad publica de tipo DateTime que representa a la columna feccre de la Tabla cat_catalogo
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "feccre", Description = " Propiedad publica de tipo DateTime que representa a la columna feccre de la Tabla cat_catalogo")]
		public DateTime feccre
		{
			get {return _feccre;}
			set
			{
				if (_feccre != value)
				{
					RaisePropertyChanging(entCatCatalogo.Fields.feccre.ToString());
					_feccre = value;
					RaisePropertyChanged(entCatCatalogo.Fields.feccre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usumod de la Tabla cat_catalogo
		/// </summary>
		private String _usumod;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna usumod de la Tabla cat_catalogo
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usumod", Description = " Propiedad publica de tipo String que representa a la columna usumod de la Tabla cat_catalogo")]
		public String usumod
		{
			get {return _usumod;}
			set
			{
				if (_usumod != value)
				{
					RaisePropertyChanging(entCatCatalogo.Fields.usumod.ToString());
					_usumod = value;
					RaisePropertyChanged(entCatCatalogo.Fields.usumod.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna fecmod de la Tabla cat_catalogo
		/// </summary>
		private DateTime? _fecmod;
		/// <summary>
		/// 	 Propiedad publica de tipo DateTime que representa a la columna fecmod de la Tabla cat_catalogo
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "fecmod", Description = " Propiedad publica de tipo DateTime que representa a la columna fecmod de la Tabla cat_catalogo")]
		public DateTime? fecmod
		{
			get {return _fecmod;}
			set
			{
				if (_fecmod != value)
				{
					RaisePropertyChanging(entCatCatalogo.Fields.fecmod.ToString());
					_fecmod = value;
					RaisePropertyChanged(entCatCatalogo.Fields.fecmod.ToString());
				}
			}
		}


	}
}

