#region 
/***********************************************************************************************************
	NOMBRE:       entFnCmAgropecuarioMatadero
	DESCRIPCION:
		Clase que define un objeto para la Tabla fn_cm_agropecuario_matadero()

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        19/08/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.Opinion.Dal.Entidades
{
	public class entFnCmAgropecuarioMatadero : cBaseClass
	{
		public const String strNombreTabla = "fn_cm_agropecuario_matadero()";
		public const String strAliasTabla = "fn_cm_agropecuario_matadero()";
		public enum Fields
		{
			tipo
			,id_persona
			,gestion
			,mes
			,departamento
			,matadero
			,codigo_producto
			,producto
			,variedad
			,del_depto
			,otro_depto
			,peso_pie
			,peso_carne
			,precio_pie
			,precio_carne

		}
		
		#region Constructoress
		
		public entFnCmAgropecuarioMatadero()
		{
			//Inicializacion de Variables
			this.tipo = null;
			this.departamento = null;
			this.matadero = null;
			this.codigo_producto = null;
			this.producto = null;
			this.variedad = null;
			this.del_depto = null;
			this.otro_depto = null;
			this.peso_pie = null;
			this.peso_carne = null;
			this.precio_pie = null;
			this.precio_carne = null;
		}
		
		public entFnCmAgropecuarioMatadero(entFnCmAgropecuarioMatadero obj)
		{
			this.tipo = obj.tipo;
			this.id_persona = obj.id_persona;
			this.gestion = obj.gestion;
			this.mes = obj.mes;
			this.departamento = obj.departamento;
			this.matadero = obj.matadero;
			this.codigo_producto = obj.codigo_producto;
			this.producto = obj.producto;
			this.variedad = obj.variedad;
			this.del_depto = obj.del_depto;
			this.otro_depto = obj.otro_depto;
			this.peso_pie = obj.peso_pie;
			this.peso_carne = obj.peso_carne;
			this.precio_pie = obj.precio_pie;
			this.precio_carne = obj.precio_carne;
		}
		
		#endregion
		
		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna tipo de la Tabla fn_cm_agropecuario_matadero()
		/// </summary>
		private String _tipo;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna tipo de la Tabla fn_cm_agropecuario_matadero()
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "tipo", Description = " Propiedad publica de tipo String que representa a la columna tipo de la Tabla fn_cm_agropecuario_matadero()")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "tipo es un campo requerido.")]
		public String tipo
		{
			get {return _tipo;}
			set
			{
				if (_tipo != value)
				{
					RaisePropertyChanging(entFnCmAgropecuarioMatadero.Fields.tipo.ToString());
					_tipo = value;
					RaisePropertyChanged(entFnCmAgropecuarioMatadero.Fields.tipo.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_persona de la Tabla fn_cm_agropecuario_matadero()
		/// </summary>
		private int _id_persona;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna id_persona de la Tabla fn_cm_agropecuario_matadero()
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_persona", Description = " Propiedad publica de tipo int que representa a la columna id_persona de la Tabla fn_cm_agropecuario_matadero()")]
		[Required(ErrorMessage = "id_persona es un campo requerido.")]
		public int id_persona
		{
			get {return _id_persona;}
			set
			{
				if (_id_persona != value)
				{
					RaisePropertyChanging(entFnCmAgropecuarioMatadero.Fields.id_persona.ToString());
					_id_persona = value;
					RaisePropertyChanged(entFnCmAgropecuarioMatadero.Fields.id_persona.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna gestion de la Tabla fn_cm_agropecuario_matadero()
		/// </summary>
		private int _gestion;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna gestion de la Tabla fn_cm_agropecuario_matadero()
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "gestion", Description = " Propiedad publica de tipo int que representa a la columna gestion de la Tabla fn_cm_agropecuario_matadero()")]
		[Required(ErrorMessage = "gestion es un campo requerido.")]
		public int gestion
		{
			get {return _gestion;}
			set
			{
				if (_gestion != value)
				{
					RaisePropertyChanging(entFnCmAgropecuarioMatadero.Fields.gestion.ToString());
					_gestion = value;
					RaisePropertyChanged(entFnCmAgropecuarioMatadero.Fields.gestion.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna mes de la Tabla fn_cm_agropecuario_matadero()
		/// </summary>
		private int _mes;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna mes de la Tabla fn_cm_agropecuario_matadero()
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "mes", Description = " Propiedad publica de tipo int que representa a la columna mes de la Tabla fn_cm_agropecuario_matadero()")]
		[Required(ErrorMessage = "mes es un campo requerido.")]
		public int mes
		{
			get {return _mes;}
			set
			{
				if (_mes != value)
				{
					RaisePropertyChanging(entFnCmAgropecuarioMatadero.Fields.mes.ToString());
					_mes = value;
					RaisePropertyChanged(entFnCmAgropecuarioMatadero.Fields.mes.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna departamento de la Tabla fn_cm_agropecuario_matadero()
		/// </summary>
		private String _departamento;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna departamento de la Tabla fn_cm_agropecuario_matadero()
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "departamento", Description = " Propiedad publica de tipo String que representa a la columna departamento de la Tabla fn_cm_agropecuario_matadero()")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "departamento es un campo requerido.")]
		public String departamento
		{
			get {return _departamento;}
			set
			{
				if (_departamento != value)
				{
					RaisePropertyChanging(entFnCmAgropecuarioMatadero.Fields.departamento.ToString());
					_departamento = value;
					RaisePropertyChanged(entFnCmAgropecuarioMatadero.Fields.departamento.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna matadero de la Tabla fn_cm_agropecuario_matadero()
		/// </summary>
		private String _matadero;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna matadero de la Tabla fn_cm_agropecuario_matadero()
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "matadero", Description = " Propiedad publica de tipo String que representa a la columna matadero de la Tabla fn_cm_agropecuario_matadero()")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "matadero es un campo requerido.")]
		public String matadero
		{
			get {return _matadero;}
			set
			{
				if (_matadero != value)
				{
					RaisePropertyChanging(entFnCmAgropecuarioMatadero.Fields.matadero.ToString());
					_matadero = value;
					RaisePropertyChanged(entFnCmAgropecuarioMatadero.Fields.matadero.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna codigo_producto de la Tabla fn_cm_agropecuario_matadero()
		/// </summary>
		private String _codigo_producto;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna codigo_producto de la Tabla fn_cm_agropecuario_matadero()
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "codigo_producto", Description = " Propiedad publica de tipo String que representa a la columna codigo_producto de la Tabla fn_cm_agropecuario_matadero()")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "codigo_producto es un campo requerido.")]
		public String codigo_producto
		{
			get {return _codigo_producto;}
			set
			{
				if (_codigo_producto != value)
				{
					RaisePropertyChanging(entFnCmAgropecuarioMatadero.Fields.codigo_producto.ToString());
					_codigo_producto = value;
					RaisePropertyChanged(entFnCmAgropecuarioMatadero.Fields.codigo_producto.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna producto de la Tabla fn_cm_agropecuario_matadero()
		/// </summary>
		private String _producto;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna producto de la Tabla fn_cm_agropecuario_matadero()
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "producto", Description = " Propiedad publica de tipo String que representa a la columna producto de la Tabla fn_cm_agropecuario_matadero()")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "producto es un campo requerido.")]
		public String producto
		{
			get {return _producto;}
			set
			{
				if (_producto != value)
				{
					RaisePropertyChanging(entFnCmAgropecuarioMatadero.Fields.producto.ToString());
					_producto = value;
					RaisePropertyChanged(entFnCmAgropecuarioMatadero.Fields.producto.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna variedad de la Tabla fn_cm_agropecuario_matadero()
		/// </summary>
		private String _variedad;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna variedad de la Tabla fn_cm_agropecuario_matadero()
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "variedad", Description = " Propiedad publica de tipo String que representa a la columna variedad de la Tabla fn_cm_agropecuario_matadero()")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "variedad es un campo requerido.")]
		public String variedad
		{
			get {return _variedad;}
			set
			{
				if (_variedad != value)
				{
					RaisePropertyChanging(entFnCmAgropecuarioMatadero.Fields.variedad.ToString());
					_variedad = value;
					RaisePropertyChanged(entFnCmAgropecuarioMatadero.Fields.variedad.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna del_depto de la Tabla fn_cm_agropecuario_matadero()
		/// </summary>
		private String _del_depto;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna del_depto de la Tabla fn_cm_agropecuario_matadero()
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "del_depto", Description = " Propiedad publica de tipo String que representa a la columna del_depto de la Tabla fn_cm_agropecuario_matadero()")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "del_depto es un campo requerido.")]
		public String del_depto
		{
			get {return _del_depto;}
			set
			{
				if (_del_depto != value)
				{
					RaisePropertyChanging(entFnCmAgropecuarioMatadero.Fields.del_depto.ToString());
					_del_depto = value;
					RaisePropertyChanged(entFnCmAgropecuarioMatadero.Fields.del_depto.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna otro_depto de la Tabla fn_cm_agropecuario_matadero()
		/// </summary>
		private String _otro_depto;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna otro_depto de la Tabla fn_cm_agropecuario_matadero()
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "otro_depto", Description = " Propiedad publica de tipo String que representa a la columna otro_depto de la Tabla fn_cm_agropecuario_matadero()")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "otro_depto es un campo requerido.")]
		public String otro_depto
		{
			get {return _otro_depto;}
			set
			{
				if (_otro_depto != value)
				{
					RaisePropertyChanging(entFnCmAgropecuarioMatadero.Fields.otro_depto.ToString());
					_otro_depto = value;
					RaisePropertyChanged(entFnCmAgropecuarioMatadero.Fields.otro_depto.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna peso_pie de la Tabla fn_cm_agropecuario_matadero()
		/// </summary>
		private String _peso_pie;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna peso_pie de la Tabla fn_cm_agropecuario_matadero()
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "peso_pie", Description = " Propiedad publica de tipo String que representa a la columna peso_pie de la Tabla fn_cm_agropecuario_matadero()")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "peso_pie es un campo requerido.")]
		public String peso_pie
		{
			get {return _peso_pie;}
			set
			{
				if (_peso_pie != value)
				{
					RaisePropertyChanging(entFnCmAgropecuarioMatadero.Fields.peso_pie.ToString());
					_peso_pie = value;
					RaisePropertyChanged(entFnCmAgropecuarioMatadero.Fields.peso_pie.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna peso_carne de la Tabla fn_cm_agropecuario_matadero()
		/// </summary>
		private String _peso_carne;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna peso_carne de la Tabla fn_cm_agropecuario_matadero()
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "peso_carne", Description = " Propiedad publica de tipo String que representa a la columna peso_carne de la Tabla fn_cm_agropecuario_matadero()")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "peso_carne es un campo requerido.")]
		public String peso_carne
		{
			get {return _peso_carne;}
			set
			{
				if (_peso_carne != value)
				{
					RaisePropertyChanging(entFnCmAgropecuarioMatadero.Fields.peso_carne.ToString());
					_peso_carne = value;
					RaisePropertyChanged(entFnCmAgropecuarioMatadero.Fields.peso_carne.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna precio_pie de la Tabla fn_cm_agropecuario_matadero()
		/// </summary>
		private String _precio_pie;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna precio_pie de la Tabla fn_cm_agropecuario_matadero()
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "precio_pie", Description = " Propiedad publica de tipo String que representa a la columna precio_pie de la Tabla fn_cm_agropecuario_matadero()")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "precio_pie es un campo requerido.")]
		public String precio_pie
		{
			get {return _precio_pie;}
			set
			{
				if (_precio_pie != value)
				{
					RaisePropertyChanging(entFnCmAgropecuarioMatadero.Fields.precio_pie.ToString());
					_precio_pie = value;
					RaisePropertyChanged(entFnCmAgropecuarioMatadero.Fields.precio_pie.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna precio_carne de la Tabla fn_cm_agropecuario_matadero()
		/// </summary>
		private String _precio_carne;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna precio_carne de la Tabla fn_cm_agropecuario_matadero()
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "precio_carne", Description = " Propiedad publica de tipo String que representa a la columna precio_carne de la Tabla fn_cm_agropecuario_matadero()")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "precio_carne es un campo requerido.")]
		public String precio_carne
		{
			get {return _precio_carne;}
			set
			{
				if (_precio_carne != value)
				{
					RaisePropertyChanging(entFnCmAgropecuarioMatadero.Fields.precio_carne.ToString());
					_precio_carne = value;
					RaisePropertyChanged(entFnCmAgropecuarioMatadero.Fields.precio_carne.ToString());
				}
			}
		}


	}
}

