#region 
/***********************************************************************************************************
	NOMBRE:       entSegRolpagina
	DESCRIPCION:
		Clase que define un objeto para la Tabla seg_rolpagina

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.Opinion.Dal.Entidades
{
	public class entSegRolpagina : cBaseClass
	{
		public const String strNombreTabla = "seg_rolpagina";
		public const String strAliasTabla = "seg_rolpagina";
		public enum Fields
		{
			id_rolpagina
			,id_rol
			,id_pagina
			,apiestado
			,usucre
			,feccre
			,usumod
			,fecmod
			,alta
			,baja
			,modificacion

		}
		
		#region Constructoress
		
		public entSegRolpagina()
		{
			//Inicializacion de Variables
			this.apiestado = null;
			this.usucre = null;
			this.usumod = null;
			this.fecmod = null;
		}
		
		public entSegRolpagina(entSegRolpagina obj)
		{
			this.id_rolpagina = obj.id_rolpagina;
			this.id_rol = obj.id_rol;
			this.id_pagina = obj.id_pagina;
			this.apiestado = obj.apiestado;
			this.usucre = obj.usucre;
			this.feccre = obj.feccre;
			this.usumod = obj.usumod;
			this.fecmod = obj.fecmod;
			this.alta = obj.alta;
			this.baja = obj.baja;
			this.modificacion = obj.modificacion;
		}
		
		#endregion
		
		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_rolpagina de la Tabla seg_rolpagina
		/// </summary>
		private int _id_rolpagina;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna id_rolpagina de la Tabla seg_rolpagina
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: Yes
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_rolpagina", Description = " Propiedad publica de tipo int que representa a la columna id_rolpagina de la Tabla seg_rolpagina")]
		[Required(ErrorMessage = "id_rolpagina es un campo requerido.")]
		[Key]
		public int id_rolpagina
		{
			get {return _id_rolpagina;}
			set
			{
				if (_id_rolpagina != value)
				{
					RaisePropertyChanging(entSegRolpagina.Fields.id_rolpagina.ToString());
					_id_rolpagina = value;
					RaisePropertyChanged(entSegRolpagina.Fields.id_rolpagina.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_rol de la Tabla seg_rolpagina
		/// </summary>
		private int _id_rol;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna id_rol de la Tabla seg_rolpagina
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: Yes
		/// </summary>
		[Display(Name = "id_rol", Description = " Propiedad publica de tipo int que representa a la columna id_rol de la Tabla seg_rolpagina")]
		[Required(ErrorMessage = "id_rol es un campo requerido.")]
		public int id_rol
		{
			get {return _id_rol;}
			set
			{
				if (_id_rol != value)
				{
					RaisePropertyChanging(entSegRolpagina.Fields.id_rol.ToString());
					_id_rol = value;
					RaisePropertyChanged(entSegRolpagina.Fields.id_rol.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_pagina de la Tabla seg_rolpagina
		/// </summary>
		private int _id_pagina;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna id_pagina de la Tabla seg_rolpagina
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: Yes
		/// </summary>
		[Display(Name = "id_pagina", Description = " Propiedad publica de tipo int que representa a la columna id_pagina de la Tabla seg_rolpagina")]
		[Required(ErrorMessage = "id_pagina es un campo requerido.")]
		public int id_pagina
		{
			get {return _id_pagina;}
			set
			{
				if (_id_pagina != value)
				{
					RaisePropertyChanging(entSegRolpagina.Fields.id_pagina.ToString());
					_id_pagina = value;
					RaisePropertyChanged(entSegRolpagina.Fields.id_pagina.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna apiestado de la Tabla seg_rolpagina
		/// </summary>
		private String _apiestado;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna apiestado de la Tabla seg_rolpagina
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "apiestado", Description = " Propiedad publica de tipo String que representa a la columna apiestado de la Tabla seg_rolpagina")]
		[EnumDataType(typeof(cApi.Estado))]
		public String apiestado
		{
			get {return _apiestado;}
			set
			{
				if (_apiestado != value)
				{
					RaisePropertyChanging(entSegRolpagina.Fields.apiestado.ToString());
					_apiestado = value;
					RaisePropertyChanged(entSegRolpagina.Fields.apiestado.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usucre de la Tabla seg_rolpagina
		/// </summary>
		private String _usucre;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna usucre de la Tabla seg_rolpagina
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "usucre", Description = " Propiedad publica de tipo String que representa a la columna usucre de la Tabla seg_rolpagina")]
		public String usucre
		{
			get {return _usucre;}
			set
			{
				if (_usucre != value)
				{
					RaisePropertyChanging(entSegRolpagina.Fields.usucre.ToString());
					_usucre = value;
					RaisePropertyChanged(entSegRolpagina.Fields.usucre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna feccre de la Tabla seg_rolpagina
		/// </summary>
		private DateTime _feccre;
		/// <summary>
		/// 	 Propiedad publica de tipo DateTime que representa a la columna feccre de la Tabla seg_rolpagina
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "feccre", Description = " Propiedad publica de tipo DateTime que representa a la columna feccre de la Tabla seg_rolpagina")]
		public DateTime feccre
		{
			get {return _feccre;}
			set
			{
				if (_feccre != value)
				{
					RaisePropertyChanging(entSegRolpagina.Fields.feccre.ToString());
					_feccre = value;
					RaisePropertyChanged(entSegRolpagina.Fields.feccre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usumod de la Tabla seg_rolpagina
		/// </summary>
		private String _usumod;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna usumod de la Tabla seg_rolpagina
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "usumod", Description = " Propiedad publica de tipo String que representa a la columna usumod de la Tabla seg_rolpagina")]
		public String usumod
		{
			get {return _usumod;}
			set
			{
				if (_usumod != value)
				{
					RaisePropertyChanging(entSegRolpagina.Fields.usumod.ToString());
					_usumod = value;
					RaisePropertyChanged(entSegRolpagina.Fields.usumod.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna fecmod de la Tabla seg_rolpagina
		/// </summary>
		private DateTime? _fecmod;
		/// <summary>
		/// 	 Propiedad publica de tipo DateTime que representa a la columna fecmod de la Tabla seg_rolpagina
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "fecmod", Description = " Propiedad publica de tipo DateTime que representa a la columna fecmod de la Tabla seg_rolpagina")]
		public DateTime? fecmod
		{
			get {return _fecmod;}
			set
			{
				if (_fecmod != value)
				{
					RaisePropertyChanging(entSegRolpagina.Fields.fecmod.ToString());
					_fecmod = value;
					RaisePropertyChanged(entSegRolpagina.Fields.fecmod.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna alta de la Tabla seg_rolpagina
		/// </summary>
		private int _alta;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna alta de la Tabla seg_rolpagina
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "alta", Description = " Propiedad publica de tipo int que representa a la columna alta de la Tabla seg_rolpagina")]
		[Required(ErrorMessage = "alta es un campo requerido.")]
		public int alta
		{
			get {return _alta;}
			set
			{
				if (_alta != value)
				{
					RaisePropertyChanging(entSegRolpagina.Fields.alta.ToString());
					_alta = value;
					RaisePropertyChanged(entSegRolpagina.Fields.alta.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna baja de la Tabla seg_rolpagina
		/// </summary>
		private int _baja;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna baja de la Tabla seg_rolpagina
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "baja", Description = " Propiedad publica de tipo int que representa a la columna baja de la Tabla seg_rolpagina")]
		[Required(ErrorMessage = "baja es un campo requerido.")]
		public int baja
		{
			get {return _baja;}
			set
			{
				if (_baja != value)
				{
					RaisePropertyChanging(entSegRolpagina.Fields.baja.ToString());
					_baja = value;
					RaisePropertyChanged(entSegRolpagina.Fields.baja.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna modificacion de la Tabla seg_rolpagina
		/// </summary>
		private int _modificacion;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna modificacion de la Tabla seg_rolpagina
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "modificacion", Description = " Propiedad publica de tipo int que representa a la columna modificacion de la Tabla seg_rolpagina")]
		[Required(ErrorMessage = "modificacion es un campo requerido.")]
		public int modificacion
		{
			get {return _modificacion;}
			set
			{
				if (_modificacion != value)
				{
					RaisePropertyChanging(entSegRolpagina.Fields.modificacion.ToString());
					_modificacion = value;
					RaisePropertyChanged(entSegRolpagina.Fields.modificacion.ToString());
				}
			}
		}


	}
}

