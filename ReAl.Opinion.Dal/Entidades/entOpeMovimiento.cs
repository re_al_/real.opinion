#region 
/***********************************************************************************************************
	NOMBRE:       entOpeMovimiento
	DESCRIPCION:
		Clase que define un objeto para la Tabla ope_movimiento

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        22/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.Opinion.Dal.Entidades
{
	public class entOpeMovimiento : cBaseClass
	{
		public const String strNombreTabla = "ope_movimiento";
		public const String strAliasTabla = "ope_movimiento";
		public enum Fields
		{
			id_movimiento
			,id_asignacion
			,id_upm
			,apiestado
			,usucre
			,feccre
			,usumod
			,fecmod
			,gestion
			,mes

		}
		
		#region Constructoress
		
		public entOpeMovimiento()
		{
			//Inicializacion de Variables
			this.id_upm = null;
			this.apiestado = null;
			this.usucre = null;
			this.usumod = null;
			this.fecmod = null;
			this.gestion = null;
			this.mes = null;
		}
		
		public entOpeMovimiento(entOpeMovimiento obj)
		{
			this.id_movimiento = obj.id_movimiento;
			this.id_asignacion = obj.id_asignacion;
			this.id_upm = obj.id_upm;
			this.apiestado = obj.apiestado;
			this.usucre = obj.usucre;
			this.feccre = obj.feccre;
			this.usumod = obj.usumod;
			this.fecmod = obj.fecmod;
			this.gestion = obj.gestion;
			this.mes = obj.mes;
		}
		
		#endregion
		
		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_movimiento de la Tabla ope_movimiento
		/// </summary>
		private int _id_movimiento;
		/// <summary>
		/// 	 Es el identificador unico que representa al registro en la tabla
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: Yes
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_movimiento", Description = "Es el identificador unico que representa al registro en la tabla")]
		[Required(ErrorMessage = "id_movimiento es un campo requerido.")]
		[Key]
		public int id_movimiento
		{
			get {return _id_movimiento;}
			set
			{
				if (_id_movimiento != value)
				{
					RaisePropertyChanging(entOpeMovimiento.Fields.id_movimiento.ToString());
					_id_movimiento = value;
					RaisePropertyChanged(entOpeMovimiento.Fields.id_movimiento.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_asignacion de la Tabla ope_movimiento
		/// </summary>
		private int _id_asignacion;
		/// <summary>
		/// 	 Es el identificador unico de la tabla ope_asignacion
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: Yes
		/// </summary>
		[Display(Name = "id_asignacion", Description = "Es el identificador unico de la tabla ope_asignacion")]
		[Required(ErrorMessage = "id_asignacion es un campo requerido.")]
		public int id_asignacion
		{
			get {return _id_asignacion;}
			set
			{
				if (_id_asignacion != value)
				{
					RaisePropertyChanging(entOpeMovimiento.Fields.id_asignacion.ToString());
					_id_asignacion = value;
					RaisePropertyChanged(entOpeMovimiento.Fields.id_asignacion.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_upm de la Tabla ope_movimiento
		/// </summary>
		private int? _id_upm;
		/// <summary>
		/// 	 Es el identificador unico de la tabla cat_upm
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: Yes
		/// </summary>
		[Display(Name = "id_upm", Description = "Es el identificador unico de la tabla cat_upm")]
		public int? id_upm
		{
			get {return _id_upm;}
			set
			{
				if (_id_upm != value)
				{
					RaisePropertyChanging(entOpeMovimiento.Fields.id_upm.ToString());
					_id_upm = value;
					RaisePropertyChanged(entOpeMovimiento.Fields.id_upm.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna apiestado de la Tabla ope_movimiento
		/// </summary>
		private String _apiestado;
		/// <summary>
		/// 	 Estado en el que se encuentra el registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "apiestado", Description = "Estado en el que se encuentra el registro")]
		[EnumDataType(typeof(cApi.Estado))]
		public String apiestado
		{
			get {return _apiestado;}
			set
			{
				if (_apiestado != value)
				{
					RaisePropertyChanging(entOpeMovimiento.Fields.apiestado.ToString());
					_apiestado = value;
					RaisePropertyChanged(entOpeMovimiento.Fields.apiestado.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usucre de la Tabla ope_movimiento
		/// </summary>
		private String _usucre;
		/// <summary>
		/// 	 Login o nombre de usuario que ha creado el registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usucre", Description = "Login o nombre de usuario que ha creado el registro")]
		public String usucre
		{
			get {return _usucre;}
			set
			{
				if (_usucre != value)
				{
					RaisePropertyChanging(entOpeMovimiento.Fields.usucre.ToString());
					_usucre = value;
					RaisePropertyChanged(entOpeMovimiento.Fields.usucre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna feccre de la Tabla ope_movimiento
		/// </summary>
		private DateTime _feccre;
		/// <summary>
		/// 	 Fecha de creacion del registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "feccre", Description = "Fecha de creacion del registro")]
		public DateTime feccre
		{
			get {return _feccre;}
			set
			{
				if (_feccre != value)
				{
					RaisePropertyChanging(entOpeMovimiento.Fields.feccre.ToString());
					_feccre = value;
					RaisePropertyChanged(entOpeMovimiento.Fields.feccre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usumod de la Tabla ope_movimiento
		/// </summary>
		private String _usumod;
		/// <summary>
		/// 	 Login o nombre de usuario que ha realizado la ULTIMA modificacion registro
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usumod", Description = "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro")]
		public String usumod
		{
			get {return _usumod;}
			set
			{
				if (_usumod != value)
				{
					RaisePropertyChanging(entOpeMovimiento.Fields.usumod.ToString());
					_usumod = value;
					RaisePropertyChanged(entOpeMovimiento.Fields.usumod.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna fecmod de la Tabla ope_movimiento
		/// </summary>
		private DateTime? _fecmod;
		/// <summary>
		/// 	 Fecha en la que se ha realizado la ULTIMA modificacion registro
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "fecmod", Description = "Fecha en la que se ha realizado la ULTIMA modificacion registro")]
		public DateTime? fecmod
		{
			get {return _fecmod;}
			set
			{
				if (_fecmod != value)
				{
					RaisePropertyChanging(entOpeMovimiento.Fields.fecmod.ToString());
					_fecmod = value;
					RaisePropertyChanged(entOpeMovimiento.Fields.fecmod.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna gestion de la Tabla ope_movimiento
		/// </summary>
		private int? _gestion;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna gestion de la Tabla ope_movimiento
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "gestion", Description = " Propiedad publica de tipo int que representa a la columna gestion de la Tabla ope_movimiento")]
		public int? gestion
		{
			get {return _gestion;}
			set
			{
				if (_gestion != value)
				{
					RaisePropertyChanging(entOpeMovimiento.Fields.gestion.ToString());
					_gestion = value;
					RaisePropertyChanged(entOpeMovimiento.Fields.gestion.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna mes de la Tabla ope_movimiento
		/// </summary>
		private int? _mes;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna mes de la Tabla ope_movimiento
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "mes", Description = " Propiedad publica de tipo int que representa a la columna mes de la Tabla ope_movimiento")]
		public int? mes
		{
			get {return _mes;}
			set
			{
				if (_mes != value)
				{
					RaisePropertyChanging(entOpeMovimiento.Fields.mes.ToString());
					_mes = value;
					RaisePropertyChanged(entOpeMovimiento.Fields.mes.ToString());
				}
			}
		}


	}
}

