#region 
/***********************************************************************************************************
	NOMBRE:       entEncEstado
	DESCRIPCION:
		Clase que define un objeto para la Tabla enc_estado

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.Opinion.Dal.Entidades
{
	public class entEncEstado : cBaseClass
	{
		public const String strNombreTabla = "enc_estado";
		public const String strAliasTabla = "enc_estado";
		public enum Fields
		{
			id_informante
			,id_seccion
			,apiestado
			,usucre
			,feccre
			,usumod
			,fecmod

		}
		
		#region Constructoress
		
		public entEncEstado()
		{
			//Inicializacion de Variables
			this.apiestado = null;
			this.usucre = null;
			this.usumod = null;
			this.fecmod = null;
		}
		
		public entEncEstado(entEncEstado obj)
		{
			this.id_informante = obj.id_informante;
			this.id_seccion = obj.id_seccion;
			this.apiestado = obj.apiestado;
			this.usucre = obj.usucre;
			this.feccre = obj.feccre;
			this.usumod = obj.usumod;
			this.fecmod = obj.fecmod;
		}
		
		#endregion
		
		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_informante de la Tabla enc_estado
		/// </summary>
		private int _id_informante;
		/// <summary>
		/// 	 Es el identificador que representa a la informante segun la tabla enc_informante
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: Yes
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_informante", Description = "Es el identificador que representa a la informante segun la tabla enc_informante")]
		[Required(ErrorMessage = "id_informante es un campo requerido.")]
		[Key]
		public int id_informante
		{
			get {return _id_informante;}
			set
			{
				if (_id_informante != value)
				{
					RaisePropertyChanging(entEncEstado.Fields.id_informante.ToString());
					_id_informante = value;
					RaisePropertyChanged(entEncEstado.Fields.id_informante.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_seccion de la Tabla enc_estado
		/// </summary>
		private int _id_seccion;
		/// <summary>
		/// 	 Es el identificador que representa a la seccion segun la tabla enc_seccion
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: Yes
		/// 	 Es ForeignKey: Yes
		/// </summary>
		[Display(Name = "id_seccion", Description = "Es el identificador que representa a la seccion segun la tabla enc_seccion")]
		[Required(ErrorMessage = "id_seccion es un campo requerido.")]
		[Key]
		public int id_seccion
		{
			get {return _id_seccion;}
			set
			{
				if (_id_seccion != value)
				{
					RaisePropertyChanging(entEncEstado.Fields.id_seccion.ToString());
					_id_seccion = value;
					RaisePropertyChanged(entEncEstado.Fields.id_seccion.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna apiestado de la Tabla enc_estado
		/// </summary>
		private String _apiestado;
		/// <summary>
		/// 	 Estado en el que se encuentra el registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "apiestado", Description = "Estado en el que se encuentra el registro")]
		[EnumDataType(typeof(cApi.Estado))]
		public String apiestado
		{
			get {return _apiestado;}
			set
			{
				if (_apiestado != value)
				{
					RaisePropertyChanging(entEncEstado.Fields.apiestado.ToString());
					_apiestado = value;
					RaisePropertyChanged(entEncEstado.Fields.apiestado.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usucre de la Tabla enc_estado
		/// </summary>
		private String _usucre;
		/// <summary>
		/// 	 Login o nombre de usuario que ha creado el registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usucre", Description = "Login o nombre de usuario que ha creado el registro")]
		public String usucre
		{
			get {return _usucre;}
			set
			{
				if (_usucre != value)
				{
					RaisePropertyChanging(entEncEstado.Fields.usucre.ToString());
					_usucre = value;
					RaisePropertyChanged(entEncEstado.Fields.usucre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna feccre de la Tabla enc_estado
		/// </summary>
		private DateTime _feccre;
		/// <summary>
		/// 	 Fecha de creacion del registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "feccre", Description = "Fecha de creacion del registro")]
		public DateTime feccre
		{
			get {return _feccre;}
			set
			{
				if (_feccre != value)
				{
					RaisePropertyChanging(entEncEstado.Fields.feccre.ToString());
					_feccre = value;
					RaisePropertyChanged(entEncEstado.Fields.feccre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usumod de la Tabla enc_estado
		/// </summary>
		private String _usumod;
		/// <summary>
		/// 	 Login o nombre de usuario que ha realizado la ULTIMA modificacion registro
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usumod", Description = "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro")]
		public String usumod
		{
			get {return _usumod;}
			set
			{
				if (_usumod != value)
				{
					RaisePropertyChanging(entEncEstado.Fields.usumod.ToString());
					_usumod = value;
					RaisePropertyChanged(entEncEstado.Fields.usumod.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna fecmod de la Tabla enc_estado
		/// </summary>
		private DateTime? _fecmod;
		/// <summary>
		/// 	 Fecha en la que se ha realizado la ULTIMA modificacion registro
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "fecmod", Description = "Fecha en la que se ha realizado la ULTIMA modificacion registro")]
		public DateTime? fecmod
		{
			get {return _fecmod;}
			set
			{
				if (_fecmod != value)
				{
					RaisePropertyChanging(entEncEstado.Fields.fecmod.ToString());
					_fecmod = value;
					RaisePropertyChanged(entEncEstado.Fields.fecmod.ToString());
				}
			}
		}


	}
}

