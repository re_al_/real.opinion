#region 
/***********************************************************************************************************
	NOMBRE:       entIndIndustria
	DESCRIPCION:
		Clase que define un objeto para la Tabla ind_industria

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/05/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.Opinion.Dal.Entidades
{
	public class entIndIndustria : cBaseClass
	{
		public const String strNombreTabla = "ind_industria";
		public const String strAliasTabla = "ind_industria";
		public enum Fields
		{
			id_industria
			,id_departamento
			,razon_social
			,nombre_comercial
			,nit
			,regine
			,situacion
			,direccion
			,numero
			,edificio
			,piso
			,oficina
			,zona
			,entre_calles
			,referencia
			,telefono
			,fax
			,casilla
			,nombre_informante
			,e_mail
			,pagina_web
			,apiestado
			,apitransaccion
			,usucre
			,feccre
			,usumod
			,fecmod
			,geoposicion
			,usugeo
			,geoposicion_fabrica
			,usuge_fabrica
			,gps_informante
			,gps_fabrica

		}
		
		#region Constructoress
		
		public entIndIndustria()
		{
			//Inicializacion de Variables
			this.razon_social = null;
			this.nombre_comercial = null;
			this.nit = null;
			this.regine = null;
			this.situacion = null;
			this.direccion = null;
			this.numero = null;
			this.edificio = null;
			this.piso = null;
			this.oficina = null;
			this.zona = null;
			this.entre_calles = null;
			this.referencia = null;
			this.telefono = null;
			this.fax = null;
			this.casilla = null;
			this.nombre_informante = null;
			this.e_mail = null;
			this.pagina_web = null;
			this.apiestado = null;
			this.apitransaccion = null;
			this.usucre = null;
			this.usumod = null;
			this.fecmod = null;
			this.geoposicion = null;
			this.usugeo = null;
			this.geoposicion_fabrica = null;
			this.usuge_fabrica = null;
			this.gps_informante = null;
			this.gps_fabrica = null;
		}
		
		public entIndIndustria(entIndIndustria obj)
		{
			this.id_industria = obj.id_industria;
			this.id_departamento = obj.id_departamento;
			this.razon_social = obj.razon_social;
			this.nombre_comercial = obj.nombre_comercial;
			this.nit = obj.nit;
			this.regine = obj.regine;
			this.situacion = obj.situacion;
			this.direccion = obj.direccion;
			this.numero = obj.numero;
			this.edificio = obj.edificio;
			this.piso = obj.piso;
			this.oficina = obj.oficina;
			this.zona = obj.zona;
			this.entre_calles = obj.entre_calles;
			this.referencia = obj.referencia;
			this.telefono = obj.telefono;
			this.fax = obj.fax;
			this.casilla = obj.casilla;
			this.nombre_informante = obj.nombre_informante;
			this.e_mail = obj.e_mail;
			this.pagina_web = obj.pagina_web;
			this.apiestado = obj.apiestado;
			this.apitransaccion = obj.apitransaccion;
			this.usucre = obj.usucre;
			this.feccre = obj.feccre;
			this.usumod = obj.usumod;
			this.fecmod = obj.fecmod;
			this.geoposicion = obj.geoposicion;
			this.usugeo = obj.usugeo;
			this.geoposicion_fabrica = obj.geoposicion_fabrica;
			this.usuge_fabrica = obj.usuge_fabrica;
			this.gps_informante = obj.gps_informante;
			this.gps_fabrica = obj.gps_fabrica;
		}
		
		#endregion
		
		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_industria de la Tabla ind_industria
		/// </summary>
		private int _id_industria;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna id_industria de la Tabla ind_industria
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: Yes
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_industria", Description = " Propiedad publica de tipo int que representa a la columna id_industria de la Tabla ind_industria")]
		[Required(ErrorMessage = "id_industria es un campo requerido.")]
		[Key]
		public int id_industria
		{
			get {return _id_industria;}
			set
			{
				if (_id_industria != value)
				{
					RaisePropertyChanging(entIndIndustria.Fields.id_industria.ToString());
					_id_industria = value;
					RaisePropertyChanged(entIndIndustria.Fields.id_industria.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_departamento de la Tabla ind_industria
		/// </summary>
		private int _id_departamento;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna id_departamento de la Tabla ind_industria
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_departamento", Description = " Propiedad publica de tipo int que representa a la columna id_departamento de la Tabla ind_industria")]
		[Required(ErrorMessage = "id_departamento es un campo requerido.")]
		public int id_departamento
		{
			get {return _id_departamento;}
			set
			{
				if (_id_departamento != value)
				{
					RaisePropertyChanging(entIndIndustria.Fields.id_departamento.ToString());
					_id_departamento = value;
					RaisePropertyChanged(entIndIndustria.Fields.id_departamento.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna razon_social de la Tabla ind_industria
		/// </summary>
		private String _razon_social;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna razon_social de la Tabla ind_industria
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "razon_social", Description = " Propiedad publica de tipo String que representa a la columna razon_social de la Tabla ind_industria")]
		public String razon_social
		{
			get {return _razon_social;}
			set
			{
				if (_razon_social != value)
				{
					RaisePropertyChanging(entIndIndustria.Fields.razon_social.ToString());
					_razon_social = value;
					RaisePropertyChanged(entIndIndustria.Fields.razon_social.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna nombre_comercial de la Tabla ind_industria
		/// </summary>
		private String _nombre_comercial;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna nombre_comercial de la Tabla ind_industria
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "nombre_comercial", Description = " Propiedad publica de tipo String que representa a la columna nombre_comercial de la Tabla ind_industria")]
		public String nombre_comercial
		{
			get {return _nombre_comercial;}
			set
			{
				if (_nombre_comercial != value)
				{
					RaisePropertyChanging(entIndIndustria.Fields.nombre_comercial.ToString());
					_nombre_comercial = value;
					RaisePropertyChanged(entIndIndustria.Fields.nombre_comercial.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna nit de la Tabla ind_industria
		/// </summary>
		private String _nit;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna nit de la Tabla ind_industria
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "nit", Description = " Propiedad publica de tipo String que representa a la columna nit de la Tabla ind_industria")]
		public String nit
		{
			get {return _nit;}
			set
			{
				if (_nit != value)
				{
					RaisePropertyChanging(entIndIndustria.Fields.nit.ToString());
					_nit = value;
					RaisePropertyChanged(entIndIndustria.Fields.nit.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna regine de la Tabla ind_industria
		/// </summary>
		private String _regine;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna regine de la Tabla ind_industria
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "regine", Description = " Propiedad publica de tipo String que representa a la columna regine de la Tabla ind_industria")]
		public String regine
		{
			get {return _regine;}
			set
			{
				if (_regine != value)
				{
					RaisePropertyChanging(entIndIndustria.Fields.regine.ToString());
					_regine = value;
					RaisePropertyChanged(entIndIndustria.Fields.regine.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna situacion de la Tabla ind_industria
		/// </summary>
		private String _situacion;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna situacion de la Tabla ind_industria
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "situacion", Description = " Propiedad publica de tipo String que representa a la columna situacion de la Tabla ind_industria")]
		public String situacion
		{
			get {return _situacion;}
			set
			{
				if (_situacion != value)
				{
					RaisePropertyChanging(entIndIndustria.Fields.situacion.ToString());
					_situacion = value;
					RaisePropertyChanged(entIndIndustria.Fields.situacion.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna direccion de la Tabla ind_industria
		/// </summary>
		private String _direccion;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna direccion de la Tabla ind_industria
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "direccion", Description = " Propiedad publica de tipo String que representa a la columna direccion de la Tabla ind_industria")]
		public String direccion
		{
			get {return _direccion;}
			set
			{
				if (_direccion != value)
				{
					RaisePropertyChanging(entIndIndustria.Fields.direccion.ToString());
					_direccion = value;
					RaisePropertyChanged(entIndIndustria.Fields.direccion.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna numero de la Tabla ind_industria
		/// </summary>
		private String _numero;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna numero de la Tabla ind_industria
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(30, MinimumLength=1)]
		[Display(Name = "numero", Description = " Propiedad publica de tipo String que representa a la columna numero de la Tabla ind_industria")]
		public String numero
		{
			get {return _numero;}
			set
			{
				if (_numero != value)
				{
					RaisePropertyChanging(entIndIndustria.Fields.numero.ToString());
					_numero = value;
					RaisePropertyChanged(entIndIndustria.Fields.numero.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna edificio de la Tabla ind_industria
		/// </summary>
		private String _edificio;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna edificio de la Tabla ind_industria
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "edificio", Description = " Propiedad publica de tipo String que representa a la columna edificio de la Tabla ind_industria")]
		public String edificio
		{
			get {return _edificio;}
			set
			{
				if (_edificio != value)
				{
					RaisePropertyChanging(entIndIndustria.Fields.edificio.ToString());
					_edificio = value;
					RaisePropertyChanged(entIndIndustria.Fields.edificio.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna piso de la Tabla ind_industria
		/// </summary>
		private String _piso;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna piso de la Tabla ind_industria
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(30, MinimumLength=1)]
		[Display(Name = "piso", Description = " Propiedad publica de tipo String que representa a la columna piso de la Tabla ind_industria")]
		public String piso
		{
			get {return _piso;}
			set
			{
				if (_piso != value)
				{
					RaisePropertyChanging(entIndIndustria.Fields.piso.ToString());
					_piso = value;
					RaisePropertyChanged(entIndIndustria.Fields.piso.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna oficina de la Tabla ind_industria
		/// </summary>
		private String _oficina;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna oficina de la Tabla ind_industria
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(30, MinimumLength=1)]
		[Display(Name = "oficina", Description = " Propiedad publica de tipo String que representa a la columna oficina de la Tabla ind_industria")]
		public String oficina
		{
			get {return _oficina;}
			set
			{
				if (_oficina != value)
				{
					RaisePropertyChanging(entIndIndustria.Fields.oficina.ToString());
					_oficina = value;
					RaisePropertyChanged(entIndIndustria.Fields.oficina.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna zona de la Tabla ind_industria
		/// </summary>
		private String _zona;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna zona de la Tabla ind_industria
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(250, MinimumLength=1)]
		[Display(Name = "zona", Description = " Propiedad publica de tipo String que representa a la columna zona de la Tabla ind_industria")]
		public String zona
		{
			get {return _zona;}
			set
			{
				if (_zona != value)
				{
					RaisePropertyChanging(entIndIndustria.Fields.zona.ToString());
					_zona = value;
					RaisePropertyChanged(entIndIndustria.Fields.zona.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna entre_calles de la Tabla ind_industria
		/// </summary>
		private String _entre_calles;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna entre_calles de la Tabla ind_industria
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(150, MinimumLength=1)]
		[Display(Name = "entre_calles", Description = " Propiedad publica de tipo String que representa a la columna entre_calles de la Tabla ind_industria")]
		public String entre_calles
		{
			get {return _entre_calles;}
			set
			{
				if (_entre_calles != value)
				{
					RaisePropertyChanging(entIndIndustria.Fields.entre_calles.ToString());
					_entre_calles = value;
					RaisePropertyChanged(entIndIndustria.Fields.entre_calles.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna referencia de la Tabla ind_industria
		/// </summary>
		private String _referencia;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna referencia de la Tabla ind_industria
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=1)]
		[Display(Name = "referencia", Description = " Propiedad publica de tipo String que representa a la columna referencia de la Tabla ind_industria")]
		public String referencia
		{
			get {return _referencia;}
			set
			{
				if (_referencia != value)
				{
					RaisePropertyChanging(entIndIndustria.Fields.referencia.ToString());
					_referencia = value;
					RaisePropertyChanged(entIndIndustria.Fields.referencia.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna telefono de la Tabla ind_industria
		/// </summary>
		private String _telefono;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna telefono de la Tabla ind_industria
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "telefono", Description = " Propiedad publica de tipo String que representa a la columna telefono de la Tabla ind_industria")]
		public String telefono
		{
			get {return _telefono;}
			set
			{
				if (_telefono != value)
				{
					RaisePropertyChanging(entIndIndustria.Fields.telefono.ToString());
					_telefono = value;
					RaisePropertyChanged(entIndIndustria.Fields.telefono.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna fax de la Tabla ind_industria
		/// </summary>
		private String _fax;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna fax de la Tabla ind_industria
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(30, MinimumLength=1)]
		[Display(Name = "fax", Description = " Propiedad publica de tipo String que representa a la columna fax de la Tabla ind_industria")]
		public String fax
		{
			get {return _fax;}
			set
			{
				if (_fax != value)
				{
					RaisePropertyChanging(entIndIndustria.Fields.fax.ToString());
					_fax = value;
					RaisePropertyChanged(entIndIndustria.Fields.fax.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna casilla de la Tabla ind_industria
		/// </summary>
		private String _casilla;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna casilla de la Tabla ind_industria
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(30, MinimumLength=1)]
		[Display(Name = "casilla", Description = " Propiedad publica de tipo String que representa a la columna casilla de la Tabla ind_industria")]
		public String casilla
		{
			get {return _casilla;}
			set
			{
				if (_casilla != value)
				{
					RaisePropertyChanging(entIndIndustria.Fields.casilla.ToString());
					_casilla = value;
					RaisePropertyChanged(entIndIndustria.Fields.casilla.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna nombre_informante de la Tabla ind_industria
		/// </summary>
		private String _nombre_informante;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna nombre_informante de la Tabla ind_industria
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "nombre_informante", Description = " Propiedad publica de tipo String que representa a la columna nombre_informante de la Tabla ind_industria")]
		public String nombre_informante
		{
			get {return _nombre_informante;}
			set
			{
				if (_nombre_informante != value)
				{
					RaisePropertyChanging(entIndIndustria.Fields.nombre_informante.ToString());
					_nombre_informante = value;
					RaisePropertyChanged(entIndIndustria.Fields.nombre_informante.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna e_mail de la Tabla ind_industria
		/// </summary>
		private String _e_mail;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna e_mail de la Tabla ind_industria
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "e_mail", Description = " Propiedad publica de tipo String que representa a la columna e_mail de la Tabla ind_industria")]
		public String e_mail
		{
			get {return _e_mail;}
			set
			{
				if (_e_mail != value)
				{
					RaisePropertyChanging(entIndIndustria.Fields.e_mail.ToString());
					_e_mail = value;
					RaisePropertyChanged(entIndIndustria.Fields.e_mail.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna pagina_web de la Tabla ind_industria
		/// </summary>
		private String _pagina_web;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna pagina_web de la Tabla ind_industria
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "pagina_web", Description = " Propiedad publica de tipo String que representa a la columna pagina_web de la Tabla ind_industria")]
		public String pagina_web
		{
			get {return _pagina_web;}
			set
			{
				if (_pagina_web != value)
				{
					RaisePropertyChanging(entIndIndustria.Fields.pagina_web.ToString());
					_pagina_web = value;
					RaisePropertyChanged(entIndIndustria.Fields.pagina_web.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna apiestado de la Tabla ind_industria
		/// </summary>
		private String _apiestado;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna apiestado de la Tabla ind_industria
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=1)]
		[Display(Name = "apiestado", Description = " Propiedad publica de tipo String que representa a la columna apiestado de la Tabla ind_industria")]
		[EnumDataType(typeof(cApi.Estado))]
		public String apiestado
		{
			get {return _apiestado;}
			set
			{
				if (_apiestado != value)
				{
					RaisePropertyChanging(entIndIndustria.Fields.apiestado.ToString());
					_apiestado = value;
					RaisePropertyChanged(entIndIndustria.Fields.apiestado.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna apitransaccion de la Tabla ind_industria
		/// </summary>
		private String _apitransaccion;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna apitransaccion de la Tabla ind_industria
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=1)]
		[Display(Name = "apitransaccion", Description = " Propiedad publica de tipo String que representa a la columna apitransaccion de la Tabla ind_industria")]
		[EnumDataType(typeof(cApi.Transaccion))]
		public String apitransaccion
		{
			get {return _apitransaccion;}
			set
			{
				if (_apitransaccion != value)
				{
					RaisePropertyChanging(entIndIndustria.Fields.apitransaccion.ToString());
					_apitransaccion = value;
					RaisePropertyChanged(entIndIndustria.Fields.apitransaccion.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usucre de la Tabla ind_industria
		/// </summary>
		private String _usucre;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna usucre de la Tabla ind_industria
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=1)]
		[Display(Name = "usucre", Description = " Propiedad publica de tipo String que representa a la columna usucre de la Tabla ind_industria")]
		public String usucre
		{
			get {return _usucre;}
			set
			{
				if (_usucre != value)
				{
					RaisePropertyChanging(entIndIndustria.Fields.usucre.ToString());
					_usucre = value;
					RaisePropertyChanged(entIndIndustria.Fields.usucre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna feccre de la Tabla ind_industria
		/// </summary>
		private DateTime _feccre;
		/// <summary>
		/// 	 Propiedad publica de tipo DateTime que representa a la columna feccre de la Tabla ind_industria
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "feccre", Description = " Propiedad publica de tipo DateTime que representa a la columna feccre de la Tabla ind_industria")]
		public DateTime feccre
		{
			get {return _feccre;}
			set
			{
				if (_feccre != value)
				{
					RaisePropertyChanging(entIndIndustria.Fields.feccre.ToString());
					_feccre = value;
					RaisePropertyChanged(entIndIndustria.Fields.feccre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usumod de la Tabla ind_industria
		/// </summary>
		private String _usumod;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna usumod de la Tabla ind_industria
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=1)]
		[Display(Name = "usumod", Description = " Propiedad publica de tipo String que representa a la columna usumod de la Tabla ind_industria")]
		public String usumod
		{
			get {return _usumod;}
			set
			{
				if (_usumod != value)
				{
					RaisePropertyChanging(entIndIndustria.Fields.usumod.ToString());
					_usumod = value;
					RaisePropertyChanged(entIndIndustria.Fields.usumod.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna fecmod de la Tabla ind_industria
		/// </summary>
		private DateTime? _fecmod;
		/// <summary>
		/// 	 Propiedad publica de tipo DateTime que representa a la columna fecmod de la Tabla ind_industria
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "fecmod", Description = " Propiedad publica de tipo DateTime que representa a la columna fecmod de la Tabla ind_industria")]
		public DateTime? fecmod
		{
			get {return _fecmod;}
			set
			{
				if (_fecmod != value)
				{
					RaisePropertyChanging(entIndIndustria.Fields.fecmod.ToString());
					_fecmod = value;
					RaisePropertyChanged(entIndIndustria.Fields.fecmod.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna geoposicion de la Tabla ind_industria
		/// </summary>
		private String _geoposicion;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna geoposicion de la Tabla ind_industria
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "geoposicion", Description = " Propiedad publica de tipo String que representa a la columna geoposicion de la Tabla ind_industria")]
		public String geoposicion
		{
			get {return _geoposicion;}
			set
			{
				if (_geoposicion != value)
				{
					RaisePropertyChanging(entIndIndustria.Fields.geoposicion.ToString());
					_geoposicion = value;
					RaisePropertyChanged(entIndIndustria.Fields.geoposicion.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usugeo de la Tabla ind_industria
		/// </summary>
		private String _usugeo;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna usugeo de la Tabla ind_industria
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=1)]
		[Display(Name = "usugeo", Description = " Propiedad publica de tipo String que representa a la columna usugeo de la Tabla ind_industria")]
		public String usugeo
		{
			get {return _usugeo;}
			set
			{
				if (_usugeo != value)
				{
					RaisePropertyChanging(entIndIndustria.Fields.usugeo.ToString());
					_usugeo = value;
					RaisePropertyChanged(entIndIndustria.Fields.usugeo.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna geoposicion_fabrica de la Tabla ind_industria
		/// </summary>
		private String _geoposicion_fabrica;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna geoposicion_fabrica de la Tabla ind_industria
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "geoposicion_fabrica", Description = " Propiedad publica de tipo String que representa a la columna geoposicion_fabrica de la Tabla ind_industria")]
		public String geoposicion_fabrica
		{
			get {return _geoposicion_fabrica;}
			set
			{
				if (_geoposicion_fabrica != value)
				{
					RaisePropertyChanging(entIndIndustria.Fields.geoposicion_fabrica.ToString());
					_geoposicion_fabrica = value;
					RaisePropertyChanged(entIndIndustria.Fields.geoposicion_fabrica.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usuge_fabrica de la Tabla ind_industria
		/// </summary>
		private String _usuge_fabrica;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna usuge_fabrica de la Tabla ind_industria
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=1)]
		[Display(Name = "usuge_fabrica", Description = " Propiedad publica de tipo String que representa a la columna usuge_fabrica de la Tabla ind_industria")]
		public String usuge_fabrica
		{
			get {return _usuge_fabrica;}
			set
			{
				if (_usuge_fabrica != value)
				{
					RaisePropertyChanging(entIndIndustria.Fields.usuge_fabrica.ToString());
					_usuge_fabrica = value;
					RaisePropertyChanged(entIndIndustria.Fields.usuge_fabrica.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna gps_informante de la Tabla ind_industria
		/// </summary>
		private String _gps_informante;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna gps_informante de la Tabla ind_industria
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "gps_informante", Description = " Propiedad publica de tipo String que representa a la columna gps_informante de la Tabla ind_industria")]
		public String gps_informante
		{
			get {return _gps_informante;}
			set
			{
				if (_gps_informante != value)
				{
					RaisePropertyChanging(entIndIndustria.Fields.gps_informante.ToString());
					_gps_informante = value;
					RaisePropertyChanged(entIndIndustria.Fields.gps_informante.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna gps_fabrica de la Tabla ind_industria
		/// </summary>
		private String _gps_fabrica;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna gps_fabrica de la Tabla ind_industria
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "gps_fabrica", Description = " Propiedad publica de tipo String que representa a la columna gps_fabrica de la Tabla ind_industria")]
		public String gps_fabrica
		{
			get {return _gps_fabrica;}
			set
			{
				if (_gps_fabrica != value)
				{
					RaisePropertyChanging(entIndIndustria.Fields.gps_fabrica.ToString());
					_gps_fabrica = value;
					RaisePropertyChanged(entIndIndustria.Fields.gps_fabrica.ToString());
				}
			}
		}


	}
}

