#region 
/***********************************************************************************************************
	NOMBRE:       entEncNivel
	DESCRIPCION:
		Clase que define un objeto para la Tabla enc_nivel

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.Opinion.Dal.Entidades
{
	public class entEncNivel : cBaseClass
	{
		public const String strNombreTabla = "enc_nivel";
		public const String strAliasTabla = "enc_nivel";
		public enum Fields
		{
			id_nivel
			,id_proyecto
			,nivel
			,descripcion
			,apiestado
			,usucre
			,feccre
			,usumod
			,fecmod

		}
		
		#region Constructoress
		
		public entEncNivel()
		{
			//Inicializacion de Variables
			this.nivel = null;
			this.descripcion = null;
			this.apiestado = null;
			this.usucre = null;
			this.usumod = null;
			this.fecmod = null;
		}
		
		public entEncNivel(entEncNivel obj)
		{
			this.id_nivel = obj.id_nivel;
			this.id_proyecto = obj.id_proyecto;
			this.nivel = obj.nivel;
			this.descripcion = obj.descripcion;
			this.apiestado = obj.apiestado;
			this.usucre = obj.usucre;
			this.feccre = obj.feccre;
			this.usumod = obj.usumod;
			this.fecmod = obj.fecmod;
		}
		
		#endregion
		
		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_nivel de la Tabla enc_nivel
		/// </summary>
		private int _id_nivel;
		/// <summary>
		/// 	 Es el identificador unico que representa al registro en la tabla
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: Yes
		/// 	 Es ForeignKey: Yes
		/// </summary>
		[Display(Name = "id_nivel", Description = "Es el identificador unico que representa al registro en la tabla")]
		[Required(ErrorMessage = "id_nivel es un campo requerido.")]
		[Key]
		public int id_nivel
		{
			get {return _id_nivel;}
			set
			{
				if (_id_nivel != value)
				{
					RaisePropertyChanging(entEncNivel.Fields.id_nivel.ToString());
					_id_nivel = value;
					RaisePropertyChanged(entEncNivel.Fields.id_nivel.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_proyecto de la Tabla enc_nivel
		/// </summary>
		private int _id_proyecto;
		/// <summary>
		/// 	 Es el identificador unico de la tabla seg_proyecto
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_proyecto", Description = "Es el identificador unico de la tabla seg_proyecto")]
		[Required(ErrorMessage = "id_proyecto es un campo requerido.")]
		public int id_proyecto
		{
			get {return _id_proyecto;}
			set
			{
				if (_id_proyecto != value)
				{
					RaisePropertyChanging(entEncNivel.Fields.id_proyecto.ToString());
					_id_proyecto = value;
					RaisePropertyChanged(entEncNivel.Fields.id_proyecto.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna nivel de la Tabla enc_nivel
		/// </summary>
		private int? _nivel;
		/// <summary>
		/// 	 Es el numero de Nivel
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "nivel", Description = "Es el numero de Nivel")]
		public int? nivel
		{
			get {return _nivel;}
			set
			{
				if (_nivel != value)
				{
					RaisePropertyChanging(entEncNivel.Fields.nivel.ToString());
					_nivel = value;
					RaisePropertyChanged(entEncNivel.Fields.nivel.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna descripcion de la Tabla enc_nivel
		/// </summary>
		private String _descripcion;
		/// <summary>
		/// 	 Es la descripcion del Nivel
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "descripcion", Description = "Es la descripcion del Nivel")]
		public String descripcion
		{
			get {return _descripcion;}
			set
			{
				if (_descripcion != value)
				{
					RaisePropertyChanging(entEncNivel.Fields.descripcion.ToString());
					_descripcion = value;
					RaisePropertyChanged(entEncNivel.Fields.descripcion.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna apiestado de la Tabla enc_nivel
		/// </summary>
		private String _apiestado;
		/// <summary>
		/// 	 Estado en el que se encuentra el registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "apiestado", Description = "Estado en el que se encuentra el registro")]
		[EnumDataType(typeof(cApi.Estado))]
		public String apiestado
		{
			get {return _apiestado;}
			set
			{
				if (_apiestado != value)
				{
					RaisePropertyChanging(entEncNivel.Fields.apiestado.ToString());
					_apiestado = value;
					RaisePropertyChanged(entEncNivel.Fields.apiestado.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usucre de la Tabla enc_nivel
		/// </summary>
		private String _usucre;
		/// <summary>
		/// 	 Login o nombre de usuario que ha creado el registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usucre", Description = "Login o nombre de usuario que ha creado el registro")]
		public String usucre
		{
			get {return _usucre;}
			set
			{
				if (_usucre != value)
				{
					RaisePropertyChanging(entEncNivel.Fields.usucre.ToString());
					_usucre = value;
					RaisePropertyChanged(entEncNivel.Fields.usucre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna feccre de la Tabla enc_nivel
		/// </summary>
		private DateTime _feccre;
		/// <summary>
		/// 	 Fecha de creacion del registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "feccre", Description = "Fecha de creacion del registro")]
		public DateTime feccre
		{
			get {return _feccre;}
			set
			{
				if (_feccre != value)
				{
					RaisePropertyChanging(entEncNivel.Fields.feccre.ToString());
					_feccre = value;
					RaisePropertyChanged(entEncNivel.Fields.feccre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usumod de la Tabla enc_nivel
		/// </summary>
		private String _usumod;
		/// <summary>
		/// 	 Login o nombre de usuario que ha realizado la ULTIMA modificacion registro
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usumod", Description = "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro")]
		public String usumod
		{
			get {return _usumod;}
			set
			{
				if (_usumod != value)
				{
					RaisePropertyChanging(entEncNivel.Fields.usumod.ToString());
					_usumod = value;
					RaisePropertyChanged(entEncNivel.Fields.usumod.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna fecmod de la Tabla enc_nivel
		/// </summary>
		private DateTime? _fecmod;
		/// <summary>
		/// 	 Fecha en la que se ha realizado la ULTIMA modificacion registro
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "fecmod", Description = "Fecha en la que se ha realizado la ULTIMA modificacion registro")]
		public DateTime? fecmod
		{
			get {return _fecmod;}
			set
			{
				if (_fecmod != value)
				{
					RaisePropertyChanging(entEncNivel.Fields.fecmod.ToString());
					_fecmod = value;
					RaisePropertyChanged(entEncNivel.Fields.fecmod.ToString());
				}
			}
		}


	}
}

