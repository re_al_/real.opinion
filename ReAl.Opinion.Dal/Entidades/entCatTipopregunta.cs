#region 
/***********************************************************************************************************
	NOMBRE:       entCatTipopregunta
	DESCRIPCION:
		Clase que define un objeto para la Tabla cat_tipo_pregunta

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.Opinion.Dal.Entidades
{
	public class entCatTipopregunta : cBaseClass
	{
		public const String strNombreTabla = "cat_tipo_pregunta";
		public const String strAliasTabla = "cat_tipo_pregunta";
		public enum Fields
		{
			id_tipo_pregunta
			,tipo_pregunta
			,descripcion
			,respuesta_valor
			,apiestado
			,usucre
			,feccre
			,usumod
			,fecmod
			,codigo_activo
			,exportar_codigo

		}
		
		#region Constructoress
		
		public entCatTipopregunta()
		{
			//Inicializacion de Variables
			this.tipo_pregunta = null;
			this.descripcion = null;
			this.respuesta_valor = null;
			this.apiestado = null;
			this.usucre = null;
			this.usumod = null;
			this.fecmod = null;
			this.codigo_activo = null;
		}
		
		public entCatTipopregunta(entCatTipopregunta obj)
		{
			this.id_tipo_pregunta = obj.id_tipo_pregunta;
			this.tipo_pregunta = obj.tipo_pregunta;
			this.descripcion = obj.descripcion;
			this.respuesta_valor = obj.respuesta_valor;
			this.apiestado = obj.apiestado;
			this.usucre = obj.usucre;
			this.feccre = obj.feccre;
			this.usumod = obj.usumod;
			this.fecmod = obj.fecmod;
			this.codigo_activo = obj.codigo_activo;
			this.exportar_codigo = obj.exportar_codigo;
		}
		
		#endregion
		
		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_tipo_pregunta de la Tabla cat_tipo_pregunta
		/// </summary>
		private int _id_tipo_pregunta;
		/// <summary>
		/// 	 Es el identificador unico que representa al registro en la tabla
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: Yes
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_tipo_pregunta", Description = "Es el identificador unico que representa al registro en la tabla")]
		[Required(ErrorMessage = "id_tipo_pregunta es un campo requerido.")]
		[Key]
		public int id_tipo_pregunta
		{
			get {return _id_tipo_pregunta;}
			set
			{
				if (_id_tipo_pregunta != value)
				{
					RaisePropertyChanging(entCatTipopregunta.Fields.id_tipo_pregunta.ToString());
					_id_tipo_pregunta = value;
					RaisePropertyChanged(entCatTipopregunta.Fields.id_tipo_pregunta.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna tipo_pregunta de la Tabla cat_tipo_pregunta
		/// </summary>
		private String _tipo_pregunta;
		/// <summary>
		/// 	 Es el tipo de pregunta UNICO
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "tipo_pregunta", Description = "Es el tipo de pregunta UNICO")]
		public String tipo_pregunta
		{
			get {return _tipo_pregunta;}
			set
			{
				if (_tipo_pregunta != value)
				{
					RaisePropertyChanging(entCatTipopregunta.Fields.tipo_pregunta.ToString());
					_tipo_pregunta = value;
					RaisePropertyChanged(entCatTipopregunta.Fields.tipo_pregunta.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna descripcion de la Tabla cat_tipo_pregunta
		/// </summary>
		private String _descripcion;
		/// <summary>
		/// 	 Es la descripcion del tipo de pregunta
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "descripcion", Description = "Es la descripcion del tipo de pregunta")]
		public String descripcion
		{
			get {return _descripcion;}
			set
			{
				if (_descripcion != value)
				{
					RaisePropertyChanging(entCatTipopregunta.Fields.descripcion.ToString());
					_descripcion = value;
					RaisePropertyChanged(entCatTipopregunta.Fields.descripcion.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna respuesta_valor de la Tabla cat_tipo_pregunta
		/// </summary>
		private String _respuesta_valor;
		/// <summary>
		/// 	 Es el tipo predominante para la evaluacion del salto en la pregunta. Puede ser RESPUESTA o CODIGO
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "respuesta_valor", Description = "Es el tipo predominante para la evaluacion del salto en la pregunta. Puede ser RESPUESTA o CODIGO")]
		public String respuesta_valor
		{
			get {return _respuesta_valor;}
			set
			{
				if (_respuesta_valor != value)
				{
					RaisePropertyChanging(entCatTipopregunta.Fields.respuesta_valor.ToString());
					_respuesta_valor = value;
					RaisePropertyChanged(entCatTipopregunta.Fields.respuesta_valor.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna apiestado de la Tabla cat_tipo_pregunta
		/// </summary>
		private String _apiestado;
		/// <summary>
		/// 	 Estado en el que se encuentra el registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "apiestado", Description = "Estado en el que se encuentra el registro")]
		[EnumDataType(typeof(cApi.Estado))]
		public String apiestado
		{
			get {return _apiestado;}
			set
			{
				if (_apiestado != value)
				{
					RaisePropertyChanging(entCatTipopregunta.Fields.apiestado.ToString());
					_apiestado = value;
					RaisePropertyChanged(entCatTipopregunta.Fields.apiestado.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usucre de la Tabla cat_tipo_pregunta
		/// </summary>
		private String _usucre;
		/// <summary>
		/// 	 Login o nombre de usuario que ha creado el registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usucre", Description = "Login o nombre de usuario que ha creado el registro")]
		public String usucre
		{
			get {return _usucre;}
			set
			{
				if (_usucre != value)
				{
					RaisePropertyChanging(entCatTipopregunta.Fields.usucre.ToString());
					_usucre = value;
					RaisePropertyChanged(entCatTipopregunta.Fields.usucre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna feccre de la Tabla cat_tipo_pregunta
		/// </summary>
		private DateTime _feccre;
		/// <summary>
		/// 	 Fecha de creacion del registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "feccre", Description = "Fecha de creacion del registro")]
		public DateTime feccre
		{
			get {return _feccre;}
			set
			{
				if (_feccre != value)
				{
					RaisePropertyChanging(entCatTipopregunta.Fields.feccre.ToString());
					_feccre = value;
					RaisePropertyChanged(entCatTipopregunta.Fields.feccre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usumod de la Tabla cat_tipo_pregunta
		/// </summary>
		private String _usumod;
		/// <summary>
		/// 	 Login o nombre de usuario que ha realizado la ULTIMA modificacion registro
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usumod", Description = "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro")]
		public String usumod
		{
			get {return _usumod;}
			set
			{
				if (_usumod != value)
				{
					RaisePropertyChanging(entCatTipopregunta.Fields.usumod.ToString());
					_usumod = value;
					RaisePropertyChanged(entCatTipopregunta.Fields.usumod.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna fecmod de la Tabla cat_tipo_pregunta
		/// </summary>
		private DateTime? _fecmod;
		/// <summary>
		/// 	 Fecha en la que se ha realizado la ULTIMA modificacion registro
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "fecmod", Description = "Fecha en la que se ha realizado la ULTIMA modificacion registro")]
		public DateTime? fecmod
		{
			get {return _fecmod;}
			set
			{
				if (_fecmod != value)
				{
					RaisePropertyChanging(entCatTipopregunta.Fields.fecmod.ToString());
					_fecmod = value;
					RaisePropertyChanged(entCatTipopregunta.Fields.fecmod.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna codigo_activo de la Tabla cat_tipo_pregunta
		/// </summary>
		private String _codigo_activo;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna codigo_activo de la Tabla cat_tipo_pregunta
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "codigo_activo", Description = " Propiedad publica de tipo String que representa a la columna codigo_activo de la Tabla cat_tipo_pregunta")]
		public String codigo_activo
		{
			get {return _codigo_activo;}
			set
			{
				if (_codigo_activo != value)
				{
					RaisePropertyChanging(entCatTipopregunta.Fields.codigo_activo.ToString());
					_codigo_activo = value;
					RaisePropertyChanged(entCatTipopregunta.Fields.codigo_activo.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna exportar_codigo de la Tabla cat_tipo_pregunta
		/// </summary>
		private int _exportar_codigo;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna exportar_codigo de la Tabla cat_tipo_pregunta
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "exportar_codigo", Description = " Propiedad publica de tipo int que representa a la columna exportar_codigo de la Tabla cat_tipo_pregunta")]
		[Required(ErrorMessage = "exportar_codigo es un campo requerido.")]
		public int exportar_codigo
		{
			get {return _exportar_codigo;}
			set
			{
				if (_exportar_codigo != value)
				{
					RaisePropertyChanging(entCatTipopregunta.Fields.exportar_codigo.ToString());
					_exportar_codigo = value;
					RaisePropertyChanged(entCatTipopregunta.Fields.exportar_codigo.ToString());
				}
			}
		}


	}
}

