#region 
/***********************************************************************************************************
	NOMBRE:       entOpeAsignacion
	DESCRIPCION:
		Clase que define un objeto para la Tabla ope_asignacion

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.Opinion.Dal.Entidades
{
	public class entOpeAsignacion : cBaseClass
	{
		public const String strNombreTabla = "ope_asignacion";
		public const String strAliasTabla = "ope_asignacion";
		public enum Fields
		{
			id_asignacion
			,id_usuario
			,id_proyecto
			,id_brigada
			,id_tablet
			,apiestado
			,usucre
			,feccre
			,usumod
			,fecmod

		}
		
		#region Constructoress
		
		public entOpeAsignacion()
		{
			//Inicializacion de Variables
			this.id_usuario = null;
			this.id_proyecto = null;
			this.id_brigada = null;
			this.id_tablet = null;
			this.apiestado = null;
			this.usucre = null;
			this.usumod = null;
			this.fecmod = null;
		}
		
		public entOpeAsignacion(entOpeAsignacion obj)
		{
			this.id_asignacion = obj.id_asignacion;
			this.id_usuario = obj.id_usuario;
			this.id_proyecto = obj.id_proyecto;
			this.id_brigada = obj.id_brigada;
			this.id_tablet = obj.id_tablet;
			this.apiestado = obj.apiestado;
			this.usucre = obj.usucre;
			this.feccre = obj.feccre;
			this.usumod = obj.usumod;
			this.fecmod = obj.fecmod;
		}
		
		#endregion
		
		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_asignacion de la Tabla ope_asignacion
		/// </summary>
		private int _id_asignacion;
		/// <summary>
		/// 	 Es el identificador unico que representa al registro en la tabla
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: Yes
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_asignacion", Description = "Es el identificador unico que representa al registro en la tabla")]
		[Required(ErrorMessage = "id_asignacion es un campo requerido.")]
		[Key]
		public int id_asignacion
		{
			get {return _id_asignacion;}
			set
			{
				if (_id_asignacion != value)
				{
					RaisePropertyChanging(entOpeAsignacion.Fields.id_asignacion.ToString());
					_id_asignacion = value;
					RaisePropertyChanged(entOpeAsignacion.Fields.id_asignacion.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_usuario de la Tabla ope_asignacion
		/// </summary>
		private int? _id_usuario;
		/// <summary>
		/// 	 Es el identificador unico de la tabla seg_usuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: Yes
		/// </summary>
		[Display(Name = "id_usuario", Description = "Es el identificador unico de la tabla seg_usuario")]
		public int? id_usuario
		{
			get {return _id_usuario;}
			set
			{
				if (_id_usuario != value)
				{
					RaisePropertyChanging(entOpeAsignacion.Fields.id_usuario.ToString());
					_id_usuario = value;
					RaisePropertyChanged(entOpeAsignacion.Fields.id_usuario.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_proyecto de la Tabla ope_asignacion
		/// </summary>
		private int? _id_proyecto;
		/// <summary>
		/// 	 Es el identificador unico de la tabla seg_proyecto
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: Yes
		/// </summary>
		[Display(Name = "id_proyecto", Description = "Es el identificador unico de la tabla seg_proyecto")]
		public int? id_proyecto
		{
			get {return _id_proyecto;}
			set
			{
				if (_id_proyecto != value)
				{
					RaisePropertyChanging(entOpeAsignacion.Fields.id_proyecto.ToString());
					_id_proyecto = value;
					RaisePropertyChanged(entOpeAsignacion.Fields.id_proyecto.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_brigada de la Tabla ope_asignacion
		/// </summary>
		private int? _id_brigada;
		/// <summary>
		/// 	 Es el identificador unico de la tabla ope_brigada
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: Yes
		/// </summary>
		[Display(Name = "id_brigada", Description = "Es el identificador unico de la tabla ope_brigada")]
		public int? id_brigada
		{
			get {return _id_brigada;}
			set
			{
				if (_id_brigada != value)
				{
					RaisePropertyChanging(entOpeAsignacion.Fields.id_brigada.ToString());
					_id_brigada = value;
					RaisePropertyChanged(entOpeAsignacion.Fields.id_brigada.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_tablet de la Tabla ope_asignacion
		/// </summary>
		private int? _id_tablet;
		/// <summary>
		/// 	 Es el identificador unico de la tabla ope_tablet
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: Yes
		/// </summary>
		[Display(Name = "id_tablet", Description = "Es el identificador unico de la tabla ope_tablet")]
		public int? id_tablet
		{
			get {return _id_tablet;}
			set
			{
				if (_id_tablet != value)
				{
					RaisePropertyChanging(entOpeAsignacion.Fields.id_tablet.ToString());
					_id_tablet = value;
					RaisePropertyChanged(entOpeAsignacion.Fields.id_tablet.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna apiestado de la Tabla ope_asignacion
		/// </summary>
		private String _apiestado;
		/// <summary>
		/// 	 Estado en el que se encuentra el registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "apiestado", Description = "Estado en el que se encuentra el registro")]
		[EnumDataType(typeof(cApi.Estado))]
		public String apiestado
		{
			get {return _apiestado;}
			set
			{
				if (_apiestado != value)
				{
					RaisePropertyChanging(entOpeAsignacion.Fields.apiestado.ToString());
					_apiestado = value;
					RaisePropertyChanged(entOpeAsignacion.Fields.apiestado.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usucre de la Tabla ope_asignacion
		/// </summary>
		private String _usucre;
		/// <summary>
		/// 	 Login o nombre de usuario que ha creado el registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usucre", Description = "Login o nombre de usuario que ha creado el registro")]
		public String usucre
		{
			get {return _usucre;}
			set
			{
				if (_usucre != value)
				{
					RaisePropertyChanging(entOpeAsignacion.Fields.usucre.ToString());
					_usucre = value;
					RaisePropertyChanged(entOpeAsignacion.Fields.usucre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna feccre de la Tabla ope_asignacion
		/// </summary>
		private DateTime _feccre;
		/// <summary>
		/// 	 Fecha de creacion del registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "feccre", Description = "Fecha de creacion del registro")]
		public DateTime feccre
		{
			get {return _feccre;}
			set
			{
				if (_feccre != value)
				{
					RaisePropertyChanging(entOpeAsignacion.Fields.feccre.ToString());
					_feccre = value;
					RaisePropertyChanged(entOpeAsignacion.Fields.feccre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usumod de la Tabla ope_asignacion
		/// </summary>
		private String _usumod;
		/// <summary>
		/// 	 Login o nombre de usuario que ha realizado la ULTIMA modificacion registro
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usumod", Description = "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro")]
		public String usumod
		{
			get {return _usumod;}
			set
			{
				if (_usumod != value)
				{
					RaisePropertyChanging(entOpeAsignacion.Fields.usumod.ToString());
					_usumod = value;
					RaisePropertyChanged(entOpeAsignacion.Fields.usumod.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna fecmod de la Tabla ope_asignacion
		/// </summary>
		private DateTime? _fecmod;
		/// <summary>
		/// 	 Fecha en la que se ha realizado la ULTIMA modificacion registro
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "fecmod", Description = "Fecha en la que se ha realizado la ULTIMA modificacion registro")]
		public DateTime? fecmod
		{
			get {return _fecmod;}
			set
			{
				if (_fecmod != value)
				{
					RaisePropertyChanging(entOpeAsignacion.Fields.fecmod.ToString());
					_fecmod = value;
					RaisePropertyChanged(entOpeAsignacion.Fields.fecmod.ToString());
				}
			}
		}


	}
}

