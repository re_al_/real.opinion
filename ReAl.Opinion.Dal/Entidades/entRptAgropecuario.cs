#region 
/***********************************************************************************************************
	NOMBRE:       entRptAgropecuario
	DESCRIPCION:
		Clase que define un objeto para la Tabla rpt_agropecuario

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        22/04/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.Opinion.Dal.Entidades
{
	public class entRptAgropecuario : cBaseClass
	{
		public const String strNombreTabla = "rpt_agropecuario";
		public const String strAliasTabla = "rpt_agropecuario";
		public enum Fields
		{
			gestion
			,mes
			,foto
			,id_departamento
			,id_upmipp
			,id_provincia
			,id_municipio
			,id_listado
			,id_boleta
			,id_persona
			,id_movimiento
			,departamento
			,provincia
			,municipio
			,comunidad
			,login
			,es_reemplazo
			,posible_reemplazo
			,producto_boleta
			,producto
			,codigo_producto
			,codigo_ccp
			,informante
			,celular
			,direccion
			,carac_adic_original
			,variedad_tipo_original
			,talla_original
			,cantidad_original
			,unidad_original
			,precio_unit_original
			,observaciones
			,minfec
			,maxfec
			,duracion
			,carac_adic
			,variedad_tipo
			,talla
			,cantidad
			,unidad
			,precio_unit
			,cant_equiv_original
			,unid_equiv_original
			,cant_equiv
			,unid_equiv
			,codigo_variedad

		}
		
		#region Constructoress
		
		public entRptAgropecuario()
		{
			//Inicializacion de Variables
		}
		
		public entRptAgropecuario(entRptAgropecuario obj)
		{
			this.gestion = obj.gestion;
			this.mes = obj.mes;
			this.foto = obj.foto;
			this.id_departamento = obj.id_departamento;
			this.id_upmipp = obj.id_upmipp;
			this.id_provincia = obj.id_provincia;
			this.id_municipio = obj.id_municipio;
			this.id_listado = obj.id_listado;
			this.id_boleta = obj.id_boleta;
			this.id_persona = obj.id_persona;
			this.id_movimiento = obj.id_movimiento;
			this.departamento = obj.departamento;
			this.provincia = obj.provincia;
			this.municipio = obj.municipio;
			this.comunidad = obj.comunidad;
			this.login = obj.login;
			this.es_reemplazo = obj.es_reemplazo;
			this.posible_reemplazo = obj.posible_reemplazo;
			this.producto_boleta = obj.producto_boleta;
			this.producto = obj.producto;
			this.codigo_producto = obj.codigo_producto;
			this.codigo_ccp = obj.codigo_ccp;
			this.informante = obj.informante;
			this.celular = obj.celular;
			this.direccion = obj.direccion;
			this.carac_adic_original = obj.carac_adic_original;
			this.variedad_tipo_original = obj.variedad_tipo_original;
			this.talla_original = obj.talla_original;
			this.cantidad_original = obj.cantidad_original;
			this.unidad_original = obj.unidad_original;
			this.precio_unit_original = obj.precio_unit_original;
			this.observaciones = obj.observaciones;
			this.minfec = obj.minfec;
			this.maxfec = obj.maxfec;
			this.duracion = obj.duracion;
			this.carac_adic = obj.carac_adic;
			this.variedad_tipo = obj.variedad_tipo;
			this.talla = obj.talla;
			this.cantidad = obj.cantidad;
			this.unidad = obj.unidad;
			this.precio_unit = obj.precio_unit;
			this.cant_equiv_original = obj.cant_equiv_original;
			this.unid_equiv_original = obj.unid_equiv_original;
			this.cant_equiv = obj.cant_equiv;
			this.unid_equiv = obj.unid_equiv;
			this.codigo_variedad = obj.codigo_variedad;
		}
		
		#endregion
		
		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna gestion de la Tabla rpt_agropecuario
		/// </summary>
		private int _gestion;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna gestion de la Tabla rpt_agropecuario
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: Yes
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "gestion", Description = " Propiedad publica de tipo int que representa a la columna gestion de la Tabla rpt_agropecuario")]
		[Required(ErrorMessage = "gestion es un campo requerido.")]
		[Key]
		public int gestion
		{
			get {return _gestion;}
			set
			{
				if (_gestion != value)
				{
					RaisePropertyChanging(entRptAgropecuario.Fields.gestion.ToString());
					_gestion = value;
					RaisePropertyChanged(entRptAgropecuario.Fields.gestion.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna mes de la Tabla rpt_agropecuario
		/// </summary>
		private int? _mes;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna mes de la Tabla rpt_agropecuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "mes", Description = " Propiedad publica de tipo int que representa a la columna mes de la Tabla rpt_agropecuario")]
		public int? mes
		{
			get {return _mes;}
			set
			{
				if (_mes != value)
				{
					RaisePropertyChanging(entRptAgropecuario.Fields.mes.ToString());
					_mes = value;
					RaisePropertyChanged(entRptAgropecuario.Fields.mes.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna foto de la Tabla rpt_agropecuario
		/// </summary>
		private String _foto;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna foto de la Tabla rpt_agropecuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "foto", Description = " Propiedad publica de tipo String que representa a la columna foto de la Tabla rpt_agropecuario")]
		public String foto
		{
			get {return _foto;}
			set
			{
				if (_foto != value)
				{
					RaisePropertyChanging(entRptAgropecuario.Fields.foto.ToString());
					_foto = value;
					RaisePropertyChanged(entRptAgropecuario.Fields.foto.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_departamento de la Tabla rpt_agropecuario
		/// </summary>
		private int? _id_departamento;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna id_departamento de la Tabla rpt_agropecuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_departamento", Description = " Propiedad publica de tipo int que representa a la columna id_departamento de la Tabla rpt_agropecuario")]
		public int? id_departamento
		{
			get {return _id_departamento;}
			set
			{
				if (_id_departamento != value)
				{
					RaisePropertyChanging(entRptAgropecuario.Fields.id_departamento.ToString());
					_id_departamento = value;
					RaisePropertyChanged(entRptAgropecuario.Fields.id_departamento.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_upmipp de la Tabla rpt_agropecuario
		/// </summary>
		private int? _id_upmipp;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna id_upmipp de la Tabla rpt_agropecuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_upmipp", Description = " Propiedad publica de tipo int que representa a la columna id_upmipp de la Tabla rpt_agropecuario")]
		public int? id_upmipp
		{
			get {return _id_upmipp;}
			set
			{
				if (_id_upmipp != value)
				{
					RaisePropertyChanging(entRptAgropecuario.Fields.id_upmipp.ToString());
					_id_upmipp = value;
					RaisePropertyChanged(entRptAgropecuario.Fields.id_upmipp.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_provincia de la Tabla rpt_agropecuario
		/// </summary>
		private int? _id_provincia;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna id_provincia de la Tabla rpt_agropecuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_provincia", Description = " Propiedad publica de tipo int que representa a la columna id_provincia de la Tabla rpt_agropecuario")]
		public int? id_provincia
		{
			get {return _id_provincia;}
			set
			{
				if (_id_provincia != value)
				{
					RaisePropertyChanging(entRptAgropecuario.Fields.id_provincia.ToString());
					_id_provincia = value;
					RaisePropertyChanged(entRptAgropecuario.Fields.id_provincia.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_municipio de la Tabla rpt_agropecuario
		/// </summary>
		private int? _id_municipio;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna id_municipio de la Tabla rpt_agropecuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_municipio", Description = " Propiedad publica de tipo int que representa a la columna id_municipio de la Tabla rpt_agropecuario")]
		public int? id_municipio
		{
			get {return _id_municipio;}
			set
			{
				if (_id_municipio != value)
				{
					RaisePropertyChanging(entRptAgropecuario.Fields.id_municipio.ToString());
					_id_municipio = value;
					RaisePropertyChanged(entRptAgropecuario.Fields.id_municipio.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_listado de la Tabla rpt_agropecuario
		/// </summary>
		private int? _id_listado;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna id_listado de la Tabla rpt_agropecuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_listado", Description = " Propiedad publica de tipo int que representa a la columna id_listado de la Tabla rpt_agropecuario")]
		public int? id_listado
		{
			get {return _id_listado;}
			set
			{
				if (_id_listado != value)
				{
					RaisePropertyChanging(entRptAgropecuario.Fields.id_listado.ToString());
					_id_listado = value;
					RaisePropertyChanged(entRptAgropecuario.Fields.id_listado.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_boleta de la Tabla rpt_agropecuario
		/// </summary>
		private int? _id_boleta;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna id_boleta de la Tabla rpt_agropecuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_boleta", Description = " Propiedad publica de tipo int que representa a la columna id_boleta de la Tabla rpt_agropecuario")]
		public int? id_boleta
		{
			get {return _id_boleta;}
			set
			{
				if (_id_boleta != value)
				{
					RaisePropertyChanging(entRptAgropecuario.Fields.id_boleta.ToString());
					_id_boleta = value;
					RaisePropertyChanged(entRptAgropecuario.Fields.id_boleta.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_persona de la Tabla rpt_agropecuario
		/// </summary>
		private int _id_persona;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna id_persona de la Tabla rpt_agropecuario
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: Yes
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_persona", Description = " Propiedad publica de tipo int que representa a la columna id_persona de la Tabla rpt_agropecuario")]
		[Required(ErrorMessage = "id_persona es un campo requerido.")]
		[Key]
		public int id_persona
		{
			get {return _id_persona;}
			set
			{
				if (_id_persona != value)
				{
					RaisePropertyChanging(entRptAgropecuario.Fields.id_persona.ToString());
					_id_persona = value;
					RaisePropertyChanged(entRptAgropecuario.Fields.id_persona.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_movimiento de la Tabla rpt_agropecuario
		/// </summary>
		private int? _id_movimiento;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna id_movimiento de la Tabla rpt_agropecuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_movimiento", Description = " Propiedad publica de tipo int que representa a la columna id_movimiento de la Tabla rpt_agropecuario")]
		public int? id_movimiento
		{
			get {return _id_movimiento;}
			set
			{
				if (_id_movimiento != value)
				{
					RaisePropertyChanging(entRptAgropecuario.Fields.id_movimiento.ToString());
					_id_movimiento = value;
					RaisePropertyChanged(entRptAgropecuario.Fields.id_movimiento.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna departamento de la Tabla rpt_agropecuario
		/// </summary>
		private String _departamento;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna departamento de la Tabla rpt_agropecuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "departamento", Description = " Propiedad publica de tipo String que representa a la columna departamento de la Tabla rpt_agropecuario")]
		public String departamento
		{
			get {return _departamento;}
			set
			{
				if (_departamento != value)
				{
					RaisePropertyChanging(entRptAgropecuario.Fields.departamento.ToString());
					_departamento = value;
					RaisePropertyChanged(entRptAgropecuario.Fields.departamento.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna provincia de la Tabla rpt_agropecuario
		/// </summary>
		private String _provincia;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna provincia de la Tabla rpt_agropecuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "provincia", Description = " Propiedad publica de tipo String que representa a la columna provincia de la Tabla rpt_agropecuario")]
		public String provincia
		{
			get {return _provincia;}
			set
			{
				if (_provincia != value)
				{
					RaisePropertyChanging(entRptAgropecuario.Fields.provincia.ToString());
					_provincia = value;
					RaisePropertyChanged(entRptAgropecuario.Fields.provincia.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna municipio de la Tabla rpt_agropecuario
		/// </summary>
		private String _municipio;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna municipio de la Tabla rpt_agropecuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "municipio", Description = " Propiedad publica de tipo String que representa a la columna municipio de la Tabla rpt_agropecuario")]
		public String municipio
		{
			get {return _municipio;}
			set
			{
				if (_municipio != value)
				{
					RaisePropertyChanging(entRptAgropecuario.Fields.municipio.ToString());
					_municipio = value;
					RaisePropertyChanged(entRptAgropecuario.Fields.municipio.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna comunidad de la Tabla rpt_agropecuario
		/// </summary>
		private String _comunidad;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna comunidad de la Tabla rpt_agropecuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "comunidad", Description = " Propiedad publica de tipo String que representa a la columna comunidad de la Tabla rpt_agropecuario")]
		public String comunidad
		{
			get {return _comunidad;}
			set
			{
				if (_comunidad != value)
				{
					RaisePropertyChanging(entRptAgropecuario.Fields.comunidad.ToString());
					_comunidad = value;
					RaisePropertyChanged(entRptAgropecuario.Fields.comunidad.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna login de la Tabla rpt_agropecuario
		/// </summary>
		private String _login;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna login de la Tabla rpt_agropecuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "login", Description = " Propiedad publica de tipo String que representa a la columna login de la Tabla rpt_agropecuario")]
		public String login
		{
			get {return _login;}
			set
			{
				if (_login != value)
				{
					RaisePropertyChanging(entRptAgropecuario.Fields.login.ToString());
					_login = value;
					RaisePropertyChanged(entRptAgropecuario.Fields.login.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna es_reemplazo de la Tabla rpt_agropecuario
		/// </summary>
		private String _es_reemplazo;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna es_reemplazo de la Tabla rpt_agropecuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "es_reemplazo", Description = " Propiedad publica de tipo String que representa a la columna es_reemplazo de la Tabla rpt_agropecuario")]
		public String es_reemplazo
		{
			get {return _es_reemplazo;}
			set
			{
				if (_es_reemplazo != value)
				{
					RaisePropertyChanging(entRptAgropecuario.Fields.es_reemplazo.ToString());
					_es_reemplazo = value;
					RaisePropertyChanged(entRptAgropecuario.Fields.es_reemplazo.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna posible_reemplazo de la Tabla rpt_agropecuario
		/// </summary>
		private String _posible_reemplazo;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna posible_reemplazo de la Tabla rpt_agropecuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "posible_reemplazo", Description = " Propiedad publica de tipo String que representa a la columna posible_reemplazo de la Tabla rpt_agropecuario")]
		public String posible_reemplazo
		{
			get {return _posible_reemplazo;}
			set
			{
				if (_posible_reemplazo != value)
				{
					RaisePropertyChanging(entRptAgropecuario.Fields.posible_reemplazo.ToString());
					_posible_reemplazo = value;
					RaisePropertyChanged(entRptAgropecuario.Fields.posible_reemplazo.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna producto_boleta de la Tabla rpt_agropecuario
		/// </summary>
		private String _producto_boleta;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna producto_boleta de la Tabla rpt_agropecuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "producto_boleta", Description = " Propiedad publica de tipo String que representa a la columna producto_boleta de la Tabla rpt_agropecuario")]
		public String producto_boleta
		{
			get {return _producto_boleta;}
			set
			{
				if (_producto_boleta != value)
				{
					RaisePropertyChanging(entRptAgropecuario.Fields.producto_boleta.ToString());
					_producto_boleta = value;
					RaisePropertyChanged(entRptAgropecuario.Fields.producto_boleta.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna producto de la Tabla rpt_agropecuario
		/// </summary>
		private String _producto;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna producto de la Tabla rpt_agropecuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "producto", Description = " Propiedad publica de tipo String que representa a la columna producto de la Tabla rpt_agropecuario")]
		public String producto
		{
			get {return _producto;}
			set
			{
				if (_producto != value)
				{
					RaisePropertyChanging(entRptAgropecuario.Fields.producto.ToString());
					_producto = value;
					RaisePropertyChanged(entRptAgropecuario.Fields.producto.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna codigo_producto de la Tabla rpt_agropecuario
		/// </summary>
		private String _codigo_producto;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna codigo_producto de la Tabla rpt_agropecuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "codigo_producto", Description = " Propiedad publica de tipo String que representa a la columna codigo_producto de la Tabla rpt_agropecuario")]
		public String codigo_producto
		{
			get {return _codigo_producto;}
			set
			{
				if (_codigo_producto != value)
				{
					RaisePropertyChanging(entRptAgropecuario.Fields.codigo_producto.ToString());
					_codigo_producto = value;
					RaisePropertyChanged(entRptAgropecuario.Fields.codigo_producto.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna codigo_ccp de la Tabla rpt_agropecuario
		/// </summary>
		private String _codigo_ccp;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna codigo_ccp de la Tabla rpt_agropecuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "codigo_ccp", Description = " Propiedad publica de tipo String que representa a la columna codigo_ccp de la Tabla rpt_agropecuario")]
		public String codigo_ccp
		{
			get {return _codigo_ccp;}
			set
			{
				if (_codigo_ccp != value)
				{
					RaisePropertyChanging(entRptAgropecuario.Fields.codigo_ccp.ToString());
					_codigo_ccp = value;
					RaisePropertyChanged(entRptAgropecuario.Fields.codigo_ccp.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna informante de la Tabla rpt_agropecuario
		/// </summary>
		private String _informante;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna informante de la Tabla rpt_agropecuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "informante", Description = " Propiedad publica de tipo String que representa a la columna informante de la Tabla rpt_agropecuario")]
		public String informante
		{
			get {return _informante;}
			set
			{
				if (_informante != value)
				{
					RaisePropertyChanging(entRptAgropecuario.Fields.informante.ToString());
					_informante = value;
					RaisePropertyChanged(entRptAgropecuario.Fields.informante.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna celular de la Tabla rpt_agropecuario
		/// </summary>
		private String _celular;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna celular de la Tabla rpt_agropecuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "celular", Description = " Propiedad publica de tipo String que representa a la columna celular de la Tabla rpt_agropecuario")]
		public String celular
		{
			get {return _celular;}
			set
			{
				if (_celular != value)
				{
					RaisePropertyChanging(entRptAgropecuario.Fields.celular.ToString());
					_celular = value;
					RaisePropertyChanged(entRptAgropecuario.Fields.celular.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna direccion de la Tabla rpt_agropecuario
		/// </summary>
		private String _direccion;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna direccion de la Tabla rpt_agropecuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "direccion", Description = " Propiedad publica de tipo String que representa a la columna direccion de la Tabla rpt_agropecuario")]
		public String direccion
		{
			get {return _direccion;}
			set
			{
				if (_direccion != value)
				{
					RaisePropertyChanging(entRptAgropecuario.Fields.direccion.ToString());
					_direccion = value;
					RaisePropertyChanged(entRptAgropecuario.Fields.direccion.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna carac_adic_original de la Tabla rpt_agropecuario
		/// </summary>
		private String _carac_adic_original;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna carac_adic_original de la Tabla rpt_agropecuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "carac_adic_original", Description = " Propiedad publica de tipo String que representa a la columna carac_adic_original de la Tabla rpt_agropecuario")]
		public String carac_adic_original
		{
			get {return _carac_adic_original;}
			set
			{
				if (_carac_adic_original != value)
				{
					RaisePropertyChanging(entRptAgropecuario.Fields.carac_adic_original.ToString());
					_carac_adic_original = value;
					RaisePropertyChanged(entRptAgropecuario.Fields.carac_adic_original.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna variedad_tipo_original de la Tabla rpt_agropecuario
		/// </summary>
		private String _variedad_tipo_original;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna variedad_tipo_original de la Tabla rpt_agropecuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "variedad_tipo_original", Description = " Propiedad publica de tipo String que representa a la columna variedad_tipo_original de la Tabla rpt_agropecuario")]
		public String variedad_tipo_original
		{
			get {return _variedad_tipo_original;}
			set
			{
				if (_variedad_tipo_original != value)
				{
					RaisePropertyChanging(entRptAgropecuario.Fields.variedad_tipo_original.ToString());
					_variedad_tipo_original = value;
					RaisePropertyChanged(entRptAgropecuario.Fields.variedad_tipo_original.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna talla_original de la Tabla rpt_agropecuario
		/// </summary>
		private String _talla_original;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna talla_original de la Tabla rpt_agropecuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "talla_original", Description = " Propiedad publica de tipo String que representa a la columna talla_original de la Tabla rpt_agropecuario")]
		public String talla_original
		{
			get {return _talla_original;}
			set
			{
				if (_talla_original != value)
				{
					RaisePropertyChanging(entRptAgropecuario.Fields.talla_original.ToString());
					_talla_original = value;
					RaisePropertyChanged(entRptAgropecuario.Fields.talla_original.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna cantidad_original de la Tabla rpt_agropecuario
		/// </summary>
		private String _cantidad_original;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna cantidad_original de la Tabla rpt_agropecuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "cantidad_original", Description = " Propiedad publica de tipo String que representa a la columna cantidad_original de la Tabla rpt_agropecuario")]
		public String cantidad_original
		{
			get {return _cantidad_original;}
			set
			{
				if (_cantidad_original != value)
				{
					RaisePropertyChanging(entRptAgropecuario.Fields.cantidad_original.ToString());
					_cantidad_original = value;
					RaisePropertyChanged(entRptAgropecuario.Fields.cantidad_original.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna unidad_original de la Tabla rpt_agropecuario
		/// </summary>
		private String _unidad_original;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna unidad_original de la Tabla rpt_agropecuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "unidad_original", Description = " Propiedad publica de tipo String que representa a la columna unidad_original de la Tabla rpt_agropecuario")]
		public String unidad_original
		{
			get {return _unidad_original;}
			set
			{
				if (_unidad_original != value)
				{
					RaisePropertyChanging(entRptAgropecuario.Fields.unidad_original.ToString());
					_unidad_original = value;
					RaisePropertyChanged(entRptAgropecuario.Fields.unidad_original.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna precio_unit_original de la Tabla rpt_agropecuario
		/// </summary>
		private String _precio_unit_original;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna precio_unit_original de la Tabla rpt_agropecuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "precio_unit_original", Description = " Propiedad publica de tipo String que representa a la columna precio_unit_original de la Tabla rpt_agropecuario")]
		public String precio_unit_original
		{
			get {return _precio_unit_original;}
			set
			{
				if (_precio_unit_original != value)
				{
					RaisePropertyChanging(entRptAgropecuario.Fields.precio_unit_original.ToString());
					_precio_unit_original = value;
					RaisePropertyChanged(entRptAgropecuario.Fields.precio_unit_original.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna observaciones de la Tabla rpt_agropecuario
		/// </summary>
		private String _observaciones;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna observaciones de la Tabla rpt_agropecuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "observaciones", Description = " Propiedad publica de tipo String que representa a la columna observaciones de la Tabla rpt_agropecuario")]
		public String observaciones
		{
			get {return _observaciones;}
			set
			{
				if (_observaciones != value)
				{
					RaisePropertyChanging(entRptAgropecuario.Fields.observaciones.ToString());
					_observaciones = value;
					RaisePropertyChanged(entRptAgropecuario.Fields.observaciones.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna minfec de la Tabla rpt_agropecuario
		/// </summary>
		private DateTime? _minfec;
		/// <summary>
		/// 	 Propiedad publica de tipo DateTime que representa a la columna minfec de la Tabla rpt_agropecuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "minfec", Description = " Propiedad publica de tipo DateTime que representa a la columna minfec de la Tabla rpt_agropecuario")]
		public DateTime? minfec
		{
			get {return _minfec;}
			set
			{
				if (_minfec != value)
				{
					RaisePropertyChanging(entRptAgropecuario.Fields.minfec.ToString());
					_minfec = value;
					RaisePropertyChanged(entRptAgropecuario.Fields.minfec.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna maxfec de la Tabla rpt_agropecuario
		/// </summary>
		private DateTime? _maxfec;
		/// <summary>
		/// 	 Propiedad publica de tipo DateTime que representa a la columna maxfec de la Tabla rpt_agropecuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "maxfec", Description = " Propiedad publica de tipo DateTime que representa a la columna maxfec de la Tabla rpt_agropecuario")]
		public DateTime? maxfec
		{
			get {return _maxfec;}
			set
			{
				if (_maxfec != value)
				{
					RaisePropertyChanging(entRptAgropecuario.Fields.maxfec.ToString());
					_maxfec = value;
					RaisePropertyChanged(entRptAgropecuario.Fields.maxfec.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo Decimal que representa a la columna duracion de la Tabla rpt_agropecuario
		/// </summary>
		private Decimal? _duracion;
		/// <summary>
		/// 	 Propiedad publica de tipo Decimal que representa a la columna duracion de la Tabla rpt_agropecuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "duracion", Description = " Propiedad publica de tipo Decimal que representa a la columna duracion de la Tabla rpt_agropecuario")]
		public Decimal? duracion
		{
			get {return _duracion;}
			set
			{
				if (_duracion != value)
				{
					RaisePropertyChanging(entRptAgropecuario.Fields.duracion.ToString());
					_duracion = value;
					RaisePropertyChanged(entRptAgropecuario.Fields.duracion.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna carac_adic de la Tabla rpt_agropecuario
		/// </summary>
		private String _carac_adic;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna carac_adic de la Tabla rpt_agropecuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "carac_adic", Description = " Propiedad publica de tipo String que representa a la columna carac_adic de la Tabla rpt_agropecuario")]
		public String carac_adic
		{
			get {return _carac_adic;}
			set
			{
				if (_carac_adic != value)
				{
					RaisePropertyChanging(entRptAgropecuario.Fields.carac_adic.ToString());
					_carac_adic = value;
					RaisePropertyChanged(entRptAgropecuario.Fields.carac_adic.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna variedad_tipo de la Tabla rpt_agropecuario
		/// </summary>
		private String _variedad_tipo;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna variedad_tipo de la Tabla rpt_agropecuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "variedad_tipo", Description = " Propiedad publica de tipo String que representa a la columna variedad_tipo de la Tabla rpt_agropecuario")]
		public String variedad_tipo
		{
			get {return _variedad_tipo;}
			set
			{
				if (_variedad_tipo != value)
				{
					RaisePropertyChanging(entRptAgropecuario.Fields.variedad_tipo.ToString());
					_variedad_tipo = value;
					RaisePropertyChanged(entRptAgropecuario.Fields.variedad_tipo.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna talla de la Tabla rpt_agropecuario
		/// </summary>
		private String _talla;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna talla de la Tabla rpt_agropecuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "talla", Description = " Propiedad publica de tipo String que representa a la columna talla de la Tabla rpt_agropecuario")]
		public String talla
		{
			get {return _talla;}
			set
			{
				if (_talla != value)
				{
					RaisePropertyChanging(entRptAgropecuario.Fields.talla.ToString());
					_talla = value;
					RaisePropertyChanged(entRptAgropecuario.Fields.talla.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna cantidad de la Tabla rpt_agropecuario
		/// </summary>
		private String _cantidad;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna cantidad de la Tabla rpt_agropecuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "cantidad", Description = " Propiedad publica de tipo String que representa a la columna cantidad de la Tabla rpt_agropecuario")]
		public String cantidad
		{
			get {return _cantidad;}
			set
			{
				if (_cantidad != value)
				{
					RaisePropertyChanging(entRptAgropecuario.Fields.cantidad.ToString());
					_cantidad = value;
					RaisePropertyChanged(entRptAgropecuario.Fields.cantidad.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna unidad de la Tabla rpt_agropecuario
		/// </summary>
		private String _unidad;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna unidad de la Tabla rpt_agropecuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "unidad", Description = " Propiedad publica de tipo String que representa a la columna unidad de la Tabla rpt_agropecuario")]
		public String unidad
		{
			get {return _unidad;}
			set
			{
				if (_unidad != value)
				{
					RaisePropertyChanging(entRptAgropecuario.Fields.unidad.ToString());
					_unidad = value;
					RaisePropertyChanged(entRptAgropecuario.Fields.unidad.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna precio_unit de la Tabla rpt_agropecuario
		/// </summary>
		private String _precio_unit;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna precio_unit de la Tabla rpt_agropecuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "precio_unit", Description = " Propiedad publica de tipo String que representa a la columna precio_unit de la Tabla rpt_agropecuario")]
		public String precio_unit
		{
			get {return _precio_unit;}
			set
			{
				if (_precio_unit != value)
				{
					RaisePropertyChanging(entRptAgropecuario.Fields.precio_unit.ToString());
					_precio_unit = value;
					RaisePropertyChanged(entRptAgropecuario.Fields.precio_unit.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna cant_equiv_original de la Tabla rpt_agropecuario
		/// </summary>
		private String _cant_equiv_original;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna cant_equiv_original de la Tabla rpt_agropecuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "cant_equiv_original", Description = " Propiedad publica de tipo String que representa a la columna cant_equiv_original de la Tabla rpt_agropecuario")]
		public String cant_equiv_original
		{
			get {return _cant_equiv_original;}
			set
			{
				if (_cant_equiv_original != value)
				{
					RaisePropertyChanging(entRptAgropecuario.Fields.cant_equiv_original.ToString());
					_cant_equiv_original = value;
					RaisePropertyChanged(entRptAgropecuario.Fields.cant_equiv_original.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna unid_equiv_original de la Tabla rpt_agropecuario
		/// </summary>
		private String _unid_equiv_original;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna unid_equiv_original de la Tabla rpt_agropecuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "unid_equiv_original", Description = " Propiedad publica de tipo String que representa a la columna unid_equiv_original de la Tabla rpt_agropecuario")]
		public String unid_equiv_original
		{
			get {return _unid_equiv_original;}
			set
			{
				if (_unid_equiv_original != value)
				{
					RaisePropertyChanging(entRptAgropecuario.Fields.unid_equiv_original.ToString());
					_unid_equiv_original = value;
					RaisePropertyChanged(entRptAgropecuario.Fields.unid_equiv_original.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna cant_equiv de la Tabla rpt_agropecuario
		/// </summary>
		private String _cant_equiv;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna cant_equiv de la Tabla rpt_agropecuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "cant_equiv", Description = " Propiedad publica de tipo String que representa a la columna cant_equiv de la Tabla rpt_agropecuario")]
		public String cant_equiv
		{
			get {return _cant_equiv;}
			set
			{
				if (_cant_equiv != value)
				{
					RaisePropertyChanging(entRptAgropecuario.Fields.cant_equiv.ToString());
					_cant_equiv = value;
					RaisePropertyChanged(entRptAgropecuario.Fields.cant_equiv.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna unid_equiv de la Tabla rpt_agropecuario
		/// </summary>
		private String _unid_equiv;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna unid_equiv de la Tabla rpt_agropecuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "unid_equiv", Description = " Propiedad publica de tipo String que representa a la columna unid_equiv de la Tabla rpt_agropecuario")]
		public String unid_equiv
		{
			get {return _unid_equiv;}
			set
			{
				if (_unid_equiv != value)
				{
					RaisePropertyChanging(entRptAgropecuario.Fields.unid_equiv.ToString());
					_unid_equiv = value;
					RaisePropertyChanged(entRptAgropecuario.Fields.unid_equiv.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna codigo_variedad de la Tabla rpt_agropecuario
		/// </summary>
		private String _codigo_variedad;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna codigo_variedad de la Tabla rpt_agropecuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "codigo_variedad", Description = " Propiedad publica de tipo String que representa a la columna codigo_variedad de la Tabla rpt_agropecuario")]
		public String codigo_variedad
		{
			get {return _codigo_variedad;}
			set
			{
				if (_codigo_variedad != value)
				{
					RaisePropertyChanging(entRptAgropecuario.Fields.codigo_variedad.ToString());
					_codigo_variedad = value;
					RaisePropertyChanged(entRptAgropecuario.Fields.codigo_variedad.ToString());
				}
			}
		}


	}
}

