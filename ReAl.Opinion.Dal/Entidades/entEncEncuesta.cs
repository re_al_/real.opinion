#region 
/***********************************************************************************************************
	NOMBRE:       entEncEncuesta
	DESCRIPCION:
		Clase que define un objeto para la Tabla enc_encuesta

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.Opinion.Dal.Entidades
{
	public class entEncEncuesta : cBaseClass
	{
		public const String strNombreTabla = "enc_encuesta";
		public const String strAliasTabla = "enc_encuesta";
		public enum Fields
		{
			id_encuesta
			,id_tab_encuesta
			,id_informante
			,id_movimiento
			,id_pregunta
			,id_respuesta
			,codigo_respuesta
			,respuesta
			,observacion
			,latitud
			,longitud
			,id_last
			,fila
			,apiestado
			,usucre
			,feccre
			,usumod
			,fecmod

		}
		
		#region Constructoress
		
		public entEncEncuesta()
		{
			//Inicializacion de Variables
			this.codigo_respuesta = null;
			this.respuesta = null;
			this.observacion = null;
			this.apiestado = null;
			this.usucre = null;
			this.usumod = null;
			this.fecmod = null;
		}
		
		public entEncEncuesta(entEncEncuesta obj)
		{
			this.id_encuesta = obj.id_encuesta;
			this.id_tab_encuesta = obj.id_tab_encuesta;
			this.id_informante = obj.id_informante;
			this.id_movimiento = obj.id_movimiento;
			this.id_pregunta = obj.id_pregunta;
			this.id_respuesta = obj.id_respuesta;
			this.codigo_respuesta = obj.codigo_respuesta;
			this.respuesta = obj.respuesta;
			this.observacion = obj.observacion;
			this.latitud = obj.latitud;
			this.longitud = obj.longitud;
			this.id_last = obj.id_last;
			this.fila = obj.fila;
			this.apiestado = obj.apiestado;
			this.usucre = obj.usucre;
			this.feccre = obj.feccre;
			this.usumod = obj.usumod;
			this.fecmod = obj.fecmod;
		}
		
		#endregion
		
		/// <summary>
		/// 	 Variable local de tipo Int64 que representa a la columna id_encuesta de la Tabla enc_encuesta
		/// </summary>
		private Int64 _id_encuesta;
		/// <summary>
		/// 	 Identificador unico que representa al registro en la tabla
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: Yes
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_encuesta", Description = "Identificador unico que representa al registro en la tabla")]
		[Required(ErrorMessage = "id_encuesta es un campo requerido.")]
		[Key]
		public Int64 id_encuesta
		{
			get {return _id_encuesta;}
			set
			{
				if (_id_encuesta != value)
				{
					RaisePropertyChanging(entEncEncuesta.Fields.id_encuesta.ToString());
					_id_encuesta = value;
					RaisePropertyChanged(entEncEncuesta.Fields.id_encuesta.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_tab_encuesta de la Tabla enc_encuesta
		/// </summary>
		private int _id_tab_encuesta;
		/// <summary>
		/// 	 Identificador unico que representa al registro en la tabla en el Dispositivo
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_tab_encuesta", Description = "Identificador unico que representa al registro en la tabla en el Dispositivo")]
		[Required(ErrorMessage = "id_tab_encuesta es un campo requerido.")]
		public int id_tab_encuesta
		{
			get {return _id_tab_encuesta;}
			set
			{
				if (_id_tab_encuesta != value)
				{
					RaisePropertyChanging(entEncEncuesta.Fields.id_tab_encuesta.ToString());
					_id_tab_encuesta = value;
					RaisePropertyChanged(entEncEncuesta.Fields.id_tab_encuesta.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo Int64 que representa a la columna id_informante de la Tabla enc_encuesta
		/// </summary>
		private Int64 _id_informante;
		/// <summary>
		/// 	 Es el identificador unico de la tabla enc_informante
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_informante", Description = "Es el identificador unico de la tabla enc_informante")]
		[Required(ErrorMessage = "id_informante es un campo requerido.")]
		public Int64 id_informante
		{
			get {return _id_informante;}
			set
			{
				if (_id_informante != value)
				{
					RaisePropertyChanging(entEncEncuesta.Fields.id_informante.ToString());
					_id_informante = value;
					RaisePropertyChanged(entEncEncuesta.Fields.id_informante.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_movimiento de la Tabla enc_encuesta
		/// </summary>
		private int _id_movimiento;
		/// <summary>
		/// 	 Es el identificador unico de la tabla enc_movimiento
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_movimiento", Description = "Es el identificador unico de la tabla enc_movimiento")]
		[Required(ErrorMessage = "id_movimiento es un campo requerido.")]
		public int id_movimiento
		{
			get {return _id_movimiento;}
			set
			{
				if (_id_movimiento != value)
				{
					RaisePropertyChanging(entEncEncuesta.Fields.id_movimiento.ToString());
					_id_movimiento = value;
					RaisePropertyChanged(entEncEncuesta.Fields.id_movimiento.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_pregunta de la Tabla enc_encuesta
		/// </summary>
		private int _id_pregunta;
		/// <summary>
		/// 	 Es el identificador unico de la tabla enc_pregunta
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: Yes
		/// </summary>
		[Display(Name = "id_pregunta", Description = "Es el identificador unico de la tabla enc_pregunta")]
		[Required(ErrorMessage = "id_pregunta es un campo requerido.")]
		public int id_pregunta
		{
			get {return _id_pregunta;}
			set
			{
				if (_id_pregunta != value)
				{
					RaisePropertyChanging(entEncEncuesta.Fields.id_pregunta.ToString());
					_id_pregunta = value;
					RaisePropertyChanged(entEncEncuesta.Fields.id_pregunta.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_respuesta de la Tabla enc_encuesta
		/// </summary>
		private int _id_respuesta;
		/// <summary>
		/// 	 Es el identificador unico de la tabla enc_respuesta
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: Yes
		/// </summary>
		[Display(Name = "id_respuesta", Description = "Es el identificador unico de la tabla enc_respuesta")]
		[Required(ErrorMessage = "id_respuesta es un campo requerido.")]
		public int id_respuesta
		{
			get {return _id_respuesta;}
			set
			{
				if (_id_respuesta != value)
				{
					RaisePropertyChanging(entEncEncuesta.Fields.id_respuesta.ToString());
					_id_respuesta = value;
					RaisePropertyChanged(entEncEncuesta.Fields.id_respuesta.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna codigo_respuesta de la Tabla enc_encuesta
		/// </summary>
		private String _codigo_respuesta;
		/// <summary>
		/// 	 Es el Codigo utilizado por la respuesta, segun la tabla enc_respuesta
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "codigo_respuesta", Description = "Es el Codigo utilizado por la respuesta, segun la tabla enc_respuesta")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "codigo_respuesta es un campo requerido.")]
		public String codigo_respuesta
		{
			get {return _codigo_respuesta;}
			set
			{
				if (_codigo_respuesta != value)
				{
					RaisePropertyChanging(entEncEncuesta.Fields.codigo_respuesta.ToString());
					_codigo_respuesta = value;
					RaisePropertyChanged(entEncEncuesta.Fields.codigo_respuesta.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna respuesta de la Tabla enc_encuesta
		/// </summary>
		private String _respuesta;
		/// <summary>
		/// 	 Es la respuesta TEXTUAL a la pregunta
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "respuesta", Description = "Es la respuesta TEXTUAL a la pregunta")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "respuesta es un campo requerido.")]
		public String respuesta
		{
			get {return _respuesta;}
			set
			{
				if (_respuesta != value)
				{
					RaisePropertyChanging(entEncEncuesta.Fields.respuesta.ToString());
					_respuesta = value;
					RaisePropertyChanged(entEncEncuesta.Fields.respuesta.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna observacion de la Tabla enc_encuesta
		/// </summary>
		private String _observacion;
		/// <summary>
		/// 	 Representa alguna observacion sobre la respuesta o pregunta
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "observacion", Description = "Representa alguna observacion sobre la respuesta o pregunta")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "observacion es un campo requerido.")]
		public String observacion
		{
			get {return _observacion;}
			set
			{
				if (_observacion != value)
				{
					RaisePropertyChanging(entEncEncuesta.Fields.observacion.ToString());
					_observacion = value;
					RaisePropertyChanged(entEncEncuesta.Fields.observacion.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo Decimal que representa a la columna latitud de la Tabla enc_encuesta
		/// </summary>
		private Decimal _latitud;
		/// <summary>
		/// 	 Latitud del punto CERO de la Capital de Departamento
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "latitud", Description = "Latitud del punto CERO de la Capital de Departamento")]
		[Required(ErrorMessage = "latitud es un campo requerido.")]
		public Decimal latitud
		{
			get {return _latitud;}
			set
			{
				if (_latitud != value)
				{
					RaisePropertyChanging(entEncEncuesta.Fields.latitud.ToString());
					_latitud = value;
					RaisePropertyChanged(entEncEncuesta.Fields.latitud.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo Decimal que representa a la columna longitud de la Tabla enc_encuesta
		/// </summary>
		private Decimal _longitud;
		/// <summary>
		/// 	 Longitud del punto CERO de la Capital de Departamento
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "longitud", Description = "Longitud del punto CERO de la Capital de Departamento")]
		[Required(ErrorMessage = "longitud es un campo requerido.")]
		public Decimal longitud
		{
			get {return _longitud;}
			set
			{
				if (_longitud != value)
				{
					RaisePropertyChanging(entEncEncuesta.Fields.longitud.ToString());
					_longitud = value;
					RaisePropertyChanged(entEncEncuesta.Fields.longitud.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_last de la Tabla enc_encuesta
		/// </summary>
		private int _id_last;
		/// <summary>
		/// 	 Es el identificador para control de ediciones
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_last", Description = "Es el identificador para control de ediciones")]
		[Required(ErrorMessage = "id_last es un campo requerido.")]
		public int id_last
		{
			get {return _id_last;}
			set
			{
				if (_id_last != value)
				{
					RaisePropertyChanging(entEncEncuesta.Fields.id_last.ToString());
					_id_last = value;
					RaisePropertyChanged(entEncEncuesta.Fields.id_last.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna fila de la Tabla enc_encuesta
		/// </summary>
		private int _fila;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna fila de la Tabla enc_encuesta
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "fila", Description = " Propiedad publica de tipo int que representa a la columna fila de la Tabla enc_encuesta")]
		[Required(ErrorMessage = "fila es un campo requerido.")]
		public int fila
		{
			get {return _fila;}
			set
			{
				if (_fila != value)
				{
					RaisePropertyChanging(entEncEncuesta.Fields.fila.ToString());
					_fila = value;
					RaisePropertyChanged(entEncEncuesta.Fields.fila.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna apiestado de la Tabla enc_encuesta
		/// </summary>
		private String _apiestado;
		/// <summary>
		/// 	 Estado en el que se encuentra el registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "apiestado", Description = "Estado en el que se encuentra el registro")]
		[EnumDataType(typeof(cApi.Estado))]
		public String apiestado
		{
			get {return _apiestado;}
			set
			{
				if (_apiestado != value)
				{
					RaisePropertyChanging(entEncEncuesta.Fields.apiestado.ToString());
					_apiestado = value;
					RaisePropertyChanged(entEncEncuesta.Fields.apiestado.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usucre de la Tabla enc_encuesta
		/// </summary>
		private String _usucre;
		/// <summary>
		/// 	 Login o nombre de usuario que ha creado el registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usucre", Description = "Login o nombre de usuario que ha creado el registro")]
		public String usucre
		{
			get {return _usucre;}
			set
			{
				if (_usucre != value)
				{
					RaisePropertyChanging(entEncEncuesta.Fields.usucre.ToString());
					_usucre = value;
					RaisePropertyChanged(entEncEncuesta.Fields.usucre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna feccre de la Tabla enc_encuesta
		/// </summary>
		private DateTime _feccre;
		/// <summary>
		/// 	 Fecha de creacion del registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "feccre", Description = "Fecha de creacion del registro")]
		public DateTime feccre
		{
			get {return _feccre;}
			set
			{
				if (_feccre != value)
				{
					RaisePropertyChanging(entEncEncuesta.Fields.feccre.ToString());
					_feccre = value;
					RaisePropertyChanged(entEncEncuesta.Fields.feccre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usumod de la Tabla enc_encuesta
		/// </summary>
		private String _usumod;
		/// <summary>
		/// 	 Login o nombre de usuario que ha realizado la ULTIMA modificacion registro
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usumod", Description = "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro")]
		public String usumod
		{
			get {return _usumod;}
			set
			{
				if (_usumod != value)
				{
					RaisePropertyChanging(entEncEncuesta.Fields.usumod.ToString());
					_usumod = value;
					RaisePropertyChanged(entEncEncuesta.Fields.usumod.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna fecmod de la Tabla enc_encuesta
		/// </summary>
		private DateTime? _fecmod;
		/// <summary>
		/// 	 Fecha en la que se ha realizado la ULTIMA modificacion registro
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "fecmod", Description = "Fecha en la que se ha realizado la ULTIMA modificacion registro")]
		public DateTime? fecmod
		{
			get {return _fecmod;}
			set
			{
				if (_fecmod != value)
				{
					RaisePropertyChanging(entEncEncuesta.Fields.fecmod.ToString());
					_fecmod = value;
					RaisePropertyChanged(entEncEncuesta.Fields.fecmod.ToString());
				}
			}
		}


	}
}

