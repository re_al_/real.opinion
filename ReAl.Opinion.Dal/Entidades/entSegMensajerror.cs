#region 
/***********************************************************************************************************
	NOMBRE:       entSegMensajerror
	DESCRIPCION:
		Clase que define un objeto para la Tabla seg_mensajerror

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        22/10/2014  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.Opinion.Dal.Entidades
{
	public class entSegMensajerror
	{
		public const String strNombreTabla = "seg_mensajerror";
		public const String strAliasTabla = "seg_mensajerror";
		public enum Fields
		{
			id_error
			,id_aplicacion
			,aplicacionerror
			,descripcion
			,causa
			,accion
			,comentario
			,origen
			,apiestado
			,apitransaccion
			,usucre
			,feccre
			,usumod
			,fecmod

		}
		
		#region Constructoress
		
		public entSegMensajerror()
		{
			//Inicializacion de Variables
		}
		
		public entSegMensajerror(entSegMensajerror obj)
		{
			this.id_error = obj.id_error;
			this.id_aplicacion = obj.id_aplicacion;
			this.aplicacionerror = obj.aplicacionerror;
			this.descripcion = obj.descripcion;
			this.causa = obj.causa;
			this.accion = obj.accion;
			this.comentario = obj.comentario;
			this.origen = obj.origen;
			this.apiestado = obj.apiestado;
			this.apitransaccion = obj.apitransaccion;
			this.usucre = obj.usucre;
			this.feccre = obj.feccre;
			this.usumod = obj.usumod;
			this.fecmod = obj.fecmod;
		}
		
		#endregion
		
		#region Metodos Privados
		
		/// <summary>
		/// Obtiene el Hash a partir de un array de Bytes
		/// </summary>
		/// <param name="objectAsBytes"></param>
		/// <returns>string</returns>
		private string ComputeHash(byte[] objectAsBytes)
		{
			MD5 md5 = new MD5CryptoServiceProvider();
			try
			{
				byte[] result = md5.ComputeHash(objectAsBytes);
				
				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < result.Length; i++)
				{
					sb.Append(result[i].ToString("X2"));
				}
				
				return sb.ToString();
			}
			catch (ArgumentNullException ane)
			{
				return null;
			}
		}
		
		/// <summary>
		///     Obtienen el Hash basado en algun algoritmo de Encriptación
		/// </summary>
		/// <typeparam name="T">
		///     Algoritmo de encriptación
		/// </typeparam>
		/// <param name="cryptoServiceProvider">
		///     Provvedor de Servicios de Criptografía
		/// </param>
		/// <returns>
		///     String que representa el Hash calculado
		        /// </returns>
		private string computeHash<T>( T cryptoServiceProvider) where T : HashAlgorithm, new()
		{
			DataContractSerializer serializer = new DataContractSerializer(this.GetType());
			using (MemoryStream memoryStream = new MemoryStream())
			{
				serializer.WriteObject(memoryStream, this);
				cryptoServiceProvider.ComputeHash(memoryStream.ToArray());
				return Convert.ToBase64String(cryptoServiceProvider.Hash);
			}
		}
		
		#endregion
		
		#region Overrides
		
		/// <summary>
		/// Devuelve un String que representa al Objeto
		/// </summary>
		/// <returns>string</returns>
		public override string ToString()
		{
			string hashString;
			
			//Evitar parametros NULL
			if (this == null)
				throw new ArgumentNullException("Parametro NULL no es valido");
			
			//Se verifica si el objeto es serializable.
			try
			{
				MemoryStream memStream = new MemoryStream();
				XmlSerializer serializer = new XmlSerializer(typeof(entSegMensajerror));
				serializer.Serialize(memStream, this);
				
				//Ahora se obtiene el Hash del Objeto.
				hashString = ComputeHash(memStream.ToArray());
				
				return hashString;
			}
			catch (AmbiguousMatchException ame)
			{
				throw new ApplicationException("El Objeto no es Serializable. Message:" + ame.Message);
			}
		}
		
		/// <summary>
		/// Verifica que dos objetos sean identicos
		/// </summary>
		public static bool operator ==(entSegMensajerror first, entSegMensajerror second)
		{
			// Verifica si el puntero en memoria es el mismo
			if (Object.ReferenceEquals(first, second))
				return true;
			
			// Verifica valores nulos
			if ((object) first == null || (object) second == null)
				return false;
			
			return first.GetHashCode() == second.GetHashCode();
		}
		
		/// <summary>
		/// Verifica que dos objetos sean distintos
		/// </summary>
		public static bool operator !=(entSegMensajerror first, entSegMensajerror second)
		{
			return !(first == second);
		}
		
		/// <summary>
		/// Compara este objeto con otro
		/// </summary>
		/// <param name="obj">El objeto a comparar</param>
		/// <returns>Devuelve Verdadero si ambos objetos son iguales</returns>
		public override bool Equals(object obj)
		{
			if (obj == null)
				return false;
			
			if (obj.GetType() == this.GetType())
				return obj.GetHashCode() == this.GetHashCode();
			
			return false;
		}
		
		#endregion
		
		#region DataAnnotations
		
		/// <summary>
		/// Obtiene los errores basado en los DataAnnotations 
		/// </summary>
		/// <returns>Devuelve un IList del tipo ValidationResult con los errores obtenidos</returns>
		public IList<ValidationResult> ValidationErrors()
		{
			ValidationContext context = new ValidationContext(this, null, null);
			IList<ValidationResult> errors = new List<ValidationResult>();
			
			if (!Validator.TryValidateObject(this, context, errors, true))
				return errors;
			
			return new List<ValidationResult>(0);
		}
		
		/// <summary>
		/// Obtiene los errores basado en los DataAnnotations 
		/// </summary>
		/// <returns>Devuelve un String con los errores obtenidos</returns>
		public string ValidationErrorsString()
		{
			string strErrors = "";
			ValidationContext context = new ValidationContext(this, null, null);
			IList<ValidationResult> errors = new List<ValidationResult>();
			
			if (!Validator.TryValidateObject(this, context, errors, true))
			{
				foreach (ValidationResult result in errors)
					strErrors = strErrors + result.ErrorMessage + Environment.NewLine;
			}
			return strErrors;
		}
		
		/// <summary>
		/// Funcion que determina si un objeto es valido o no
		/// </summary>
		/// <returns>Resultado de la validacion</returns>
		public bool IsValid()
		{
			ValidationContext context = new ValidationContext(this, null, null);
			IList<ValidationResult> errors = new List<ValidationResult>();
			
			return Validator.TryValidateObject(this, context, errors, true);
		}
		
		#endregion
		
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna id_error de la Tabla seg_mensajerror
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: Yes
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_error", Description = " Propiedad publica de tipo int que representa a la columna id_error de la Tabla seg_mensajerror")]
		[Required(ErrorMessage = "id_error es un campo requerido.")]
		[Key]
		public int id_error { get; set; } 

		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna id_aplicacion de la Tabla seg_mensajerror
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: Yes
		/// 	 Es ForeignKey: Yes
		/// </summary>
		[StringLength(3, MinimumLength=0)]
		[Display(Name = "id_aplicacion", Description = " Propiedad publica de tipo String que representa a la columna id_aplicacion de la Tabla seg_mensajerror")]
		[Required(AllowEmptyStrings = false, ErrorMessage = "id_aplicacion es un campo requerido.")]
		[Key]
		public String id_aplicacion { get; set; } 

		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna aplicacionerror de la Tabla seg_mensajerror
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(9, MinimumLength=0)]
		[Display(Name = "aplicacionerror", Description = " Propiedad publica de tipo String que representa a la columna aplicacionerror de la Tabla seg_mensajerror")]
		[Required(AllowEmptyStrings = false, ErrorMessage = "aplicacionerror es un campo requerido.")]
		public String aplicacionerror { get; set; } 

		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna descripcion de la Tabla seg_mensajerror
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(180, MinimumLength=0)]
		[Display(Name = "descripcion", Description = " Propiedad publica de tipo String que representa a la columna descripcion de la Tabla seg_mensajerror")]
		[Required(AllowEmptyStrings = false, ErrorMessage = "descripcion es un campo requerido.")]
		public String descripcion { get; set; } 

		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna causa de la Tabla seg_mensajerror
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(700, MinimumLength=0)]
		[Display(Name = "causa", Description = " Propiedad publica de tipo String que representa a la columna causa de la Tabla seg_mensajerror")]
		[Required(AllowEmptyStrings = false, ErrorMessage = "causa es un campo requerido.")]
		public String causa { get; set; } 

		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna accion de la Tabla seg_mensajerror
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(700, MinimumLength=0)]
		[Display(Name = "accion", Description = " Propiedad publica de tipo String que representa a la columna accion de la Tabla seg_mensajerror")]
		[Required(AllowEmptyStrings = false, ErrorMessage = "accion es un campo requerido.")]
		public String accion { get; set; } 

		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna comentario de la Tabla seg_mensajerror
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(180, MinimumLength=0)]
		[Display(Name = "comentario", Description = " Propiedad publica de tipo String que representa a la columna comentario de la Tabla seg_mensajerror")]
		public String comentario { get; set; } 

		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna origen de la Tabla seg_mensajerror
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "origen", Description = " Propiedad publica de tipo String que representa a la columna origen de la Tabla seg_mensajerror")]
		public String origen { get; set; } 

		/// <summary>
		/// 	 Describe el estado en el que se encuentra un determinado registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "apiestado", Description = "Describe el estado en el que se encuentra un determinado registro")]
		[EnumDataType(typeof(cApi.Estado))]
		public String apiestado { get; set; } 

		/// <summary>
		/// 	 Describe la trasacciÃ³n en la que se encuentra un registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "apitransaccion", Description = "Describe la trasacciÃ³n en la que se encuentra un registro")]
		[EnumDataType(typeof(cApi.Transaccion))]
		public String apitransaccion { get; set; } 

		/// <summary>
		/// 	 Describe el usuario que creo un determinado un registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "usucre", Description = "Describe el usuario que creo un determinado un registro")]
		public String usucre { get; set; } 

		/// <summary>
		/// 	 Describe la fecha de creaciÃ³n de un determinado registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "feccre", Description = "Describe la fecha de creaciÃ³n de un determinado registro")]
		public DateTime feccre { get; set; } 

		/// <summary>
		/// 	 Describe el usuario que modifico un determinado registro
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "usumod", Description = "Describe el usuario que modifico un determinado registro")]
		public String usumod { get; set; } 

		/// <summary>
		/// 	 Describe la fecha de modificacciÃ³n de determinado un registro
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "fecmod", Description = "Describe la fecha de modificacciÃ³n de determinado un registro")]
		public DateTime? fecmod { get; set; } 

	}
}

