#region 
/***********************************************************************************************************
	NOMBRE:       entSegRol
	DESCRIPCION:
		Clase que define un objeto para la Tabla seg_rol

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.Opinion.Dal.Entidades
{
	public class entSegRol : cBaseClass
	{
		public const String strNombreTabla = "seg_rol";
		public const String strAliasTabla = "seg_rol";
		public enum Fields
		{
			id_rol
			,sigla
			,descripcion
			,apiestado
			,usucre
			,feccre
			,usumod
			,fecmod

		}
		
		#region Constructoress
		
		public entSegRol()
		{
			//Inicializacion de Variables
			this.sigla = null;
			this.descripcion = null;
			this.apiestado = null;
			this.usucre = null;
			this.usumod = null;
			this.fecmod = null;
		}
		
		public entSegRol(entSegRol obj)
		{
			this.id_rol = obj.id_rol;
			this.sigla = obj.sigla;
			this.descripcion = obj.descripcion;
			this.apiestado = obj.apiestado;
			this.usucre = obj.usucre;
			this.feccre = obj.feccre;
			this.usumod = obj.usumod;
			this.fecmod = obj.fecmod;
		}
		
		#endregion
		
		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_rol de la Tabla seg_rol
		/// </summary>
		private int _id_rol;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna id_rol de la Tabla seg_rol
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: Yes
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_rol", Description = " Propiedad publica de tipo int que representa a la columna id_rol de la Tabla seg_rol")]
		[Required(ErrorMessage = "id_rol es un campo requerido.")]
		[Key]
		public int id_rol
		{
			get {return _id_rol;}
			set
			{
				if (_id_rol != value)
				{
					RaisePropertyChanging(entSegRol.Fields.id_rol.ToString());
					_id_rol = value;
					RaisePropertyChanged(entSegRol.Fields.id_rol.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna sigla de la Tabla seg_rol
		/// </summary>
		private String _sigla;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna sigla de la Tabla seg_rol
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(30, MinimumLength=0)]
		[Display(Name = "sigla", Description = " Propiedad publica de tipo String que representa a la columna sigla de la Tabla seg_rol")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "sigla es un campo requerido.")]
		public String sigla
		{
			get {return _sigla;}
			set
			{
				if (_sigla != value)
				{
					RaisePropertyChanging(entSegRol.Fields.sigla.ToString());
					_sigla = value;
					RaisePropertyChanged(entSegRol.Fields.sigla.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna descripcion de la Tabla seg_rol
		/// </summary>
		private String _descripcion;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna descripcion de la Tabla seg_rol
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "descripcion", Description = " Propiedad publica de tipo String que representa a la columna descripcion de la Tabla seg_rol")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "descripcion es un campo requerido.")]
		public String descripcion
		{
			get {return _descripcion;}
			set
			{
				if (_descripcion != value)
				{
					RaisePropertyChanging(entSegRol.Fields.descripcion.ToString());
					_descripcion = value;
					RaisePropertyChanged(entSegRol.Fields.descripcion.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna apiestado de la Tabla seg_rol
		/// </summary>
		private String _apiestado;
		/// <summary>
		/// 	 Describe el estado en el que se encuentra un determinado registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "apiestado", Description = "Describe el estado en el que se encuentra un determinado registro")]
		[EnumDataType(typeof(cApi.Estado))]
		public String apiestado
		{
			get {return _apiestado;}
			set
			{
				if (_apiestado != value)
				{
					RaisePropertyChanging(entSegRol.Fields.apiestado.ToString());
					_apiestado = value;
					RaisePropertyChanged(entSegRol.Fields.apiestado.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usucre de la Tabla seg_rol
		/// </summary>
		private String _usucre;
		/// <summary>
		/// 	 Describe el usuario que creo un determinado un registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "usucre", Description = "Describe el usuario que creo un determinado un registro")]
		public String usucre
		{
			get {return _usucre;}
			set
			{
				if (_usucre != value)
				{
					RaisePropertyChanging(entSegRol.Fields.usucre.ToString());
					_usucre = value;
					RaisePropertyChanged(entSegRol.Fields.usucre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna feccre de la Tabla seg_rol
		/// </summary>
		private DateTime _feccre;
		/// <summary>
		/// 	 Describe la fecha de creaciÃ³n de un determinado registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "feccre", Description = "Describe la fecha de creaciÃ³n de un determinado registro")]
		public DateTime feccre
		{
			get {return _feccre;}
			set
			{
				if (_feccre != value)
				{
					RaisePropertyChanging(entSegRol.Fields.feccre.ToString());
					_feccre = value;
					RaisePropertyChanged(entSegRol.Fields.feccre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usumod de la Tabla seg_rol
		/// </summary>
		private String _usumod;
		/// <summary>
		/// 	 Describe el usuario que modifico un determinado registro
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "usumod", Description = "Describe el usuario que modifico un determinado registro")]
		public String usumod
		{
			get {return _usumod;}
			set
			{
				if (_usumod != value)
				{
					RaisePropertyChanging(entSegRol.Fields.usumod.ToString());
					_usumod = value;
					RaisePropertyChanged(entSegRol.Fields.usumod.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna fecmod de la Tabla seg_rol
		/// </summary>
		private DateTime? _fecmod;
		/// <summary>
		/// 	 Describe la fecha de modificacciÃ³n de determinado un registro
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "fecmod", Description = "Describe la fecha de modificacciÃ³n de determinado un registro")]
		public DateTime? fecmod
		{
			get {return _fecmod;}
			set
			{
				if (_fecmod != value)
				{
					RaisePropertyChanging(entSegRol.Fields.fecmod.ToString());
					_fecmod = value;
					RaisePropertyChanged(entSegRol.Fields.fecmod.ToString());
				}
			}
		}


	}
}

