#region 
/***********************************************************************************************************
	NOMBRE:       entSegProyecto
	DESCRIPCION:
		Clase que define un objeto para la Tabla seg_proyecto

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        12/10/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.Opinion.Dal.Entidades
{
	public class entSegProyecto : cBaseClass
	{
		public const String strNombreTabla = "seg_proyecto";
		public const String strAliasTabla = "seg_proyecto";
		public enum Fields
		{
			id_proyecto
			,nombre
			,codigo
			,descripcion
			,fecinicio
			,fecfin
			,apiestado
			,usucre
			,feccre
			,usumod
			,fecmod
			,color_web
			,color_movil
			,color_font

		}
		
		#region Constructoress
		
		public entSegProyecto()
		{
			//Inicializacion de Variables
			this.nombre = null;
			this.codigo = null;
			this.descripcion = null;
			this.apiestado = null;
			this.usucre = null;
			this.usumod = null;
			this.fecmod = null;
			this.color_web = null;
			this.color_movil = null;
			this.color_font = null;
		}
		
		public entSegProyecto(entSegProyecto obj)
		{
			this.id_proyecto = obj.id_proyecto;
			this.nombre = obj.nombre;
			this.codigo = obj.codigo;
			this.descripcion = obj.descripcion;
			this.fecinicio = obj.fecinicio;
			this.fecfin = obj.fecfin;
			this.apiestado = obj.apiestado;
			this.usucre = obj.usucre;
			this.feccre = obj.feccre;
			this.usumod = obj.usumod;
			this.fecmod = obj.fecmod;
			this.color_web = obj.color_web;
			this.color_movil = obj.color_movil;
			this.color_font = obj.color_font;
		}
		
		#endregion
		
		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_proyecto de la Tabla seg_proyecto
		/// </summary>
		private int _id_proyecto;
		/// <summary>
		/// 	 Llave primaria del identificador de la tabla
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: Yes
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_proyecto", Description = "Llave primaria del identificador de la tabla")]
		[Required(ErrorMessage = "id_proyecto es un campo requerido.")]
		[Key]
		public int id_proyecto
		{
			get {return _id_proyecto;}
			set
			{
				if (_id_proyecto != value)
				{
					RaisePropertyChanging(entSegProyecto.Fields.id_proyecto.ToString());
					_id_proyecto = value;
					RaisePropertyChanged(entSegProyecto.Fields.id_proyecto.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna nombre de la Tabla seg_proyecto
		/// </summary>
		private String _nombre;
		/// <summary>
		/// 	 Nombre del Proyecto
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "nombre", Description = "Nombre del Proyecto")]
		public String nombre
		{
			get {return _nombre;}
			set
			{
				if (_nombre != value)
				{
					RaisePropertyChanging(entSegProyecto.Fields.nombre.ToString());
					_nombre = value;
					RaisePropertyChanged(entSegProyecto.Fields.nombre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna codigo de la Tabla seg_proyecto
		/// </summary>
		private String _codigo;
		/// <summary>
		/// 	 Codigo Unico de 4 CARACTERES con el que se identifica al Proyecto
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(4, MinimumLength=0)]
		[Display(Name = "codigo", Description = "Codigo Unico de 4 CARACTERES con el que se identifica al Proyecto")]
		public String codigo
		{
			get {return _codigo;}
			set
			{
				if (_codigo != value)
				{
					RaisePropertyChanging(entSegProyecto.Fields.codigo.ToString());
					_codigo = value;
					RaisePropertyChanged(entSegProyecto.Fields.codigo.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna descripcion de la Tabla seg_proyecto
		/// </summary>
		private String _descripcion;
		/// <summary>
		/// 	 Descripcion del proyecto
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "descripcion", Description = "Descripcion del proyecto")]
		public String descripcion
		{
			get {return _descripcion;}
			set
			{
				if (_descripcion != value)
				{
					RaisePropertyChanging(entSegProyecto.Fields.descripcion.ToString());
					_descripcion = value;
					RaisePropertyChanged(entSegProyecto.Fields.descripcion.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna fecinicio de la Tabla seg_proyecto
		/// </summary>
		private DateTime _fecinicio;
		/// <summary>
		/// 	 Fecha en la que INICIA el proyecto 
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "fecinicio", Description = "Fecha en la que INICIA el proyecto ")]
		[Required(ErrorMessage = "fecinicio es un campo requerido.")]
		public DateTime fecinicio
		{
			get {return _fecinicio;}
			set
			{
				if (_fecinicio != value)
				{
					RaisePropertyChanging(entSegProyecto.Fields.fecinicio.ToString());
					_fecinicio = value;
					RaisePropertyChanged(entSegProyecto.Fields.fecinicio.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna fecfin de la Tabla seg_proyecto
		/// </summary>
		private DateTime _fecfin;
		/// <summary>
		/// 	 Fecha en la que FINALIZA el proyecto 
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "fecfin", Description = "Fecha en la que FINALIZA el proyecto ")]
		[Required(ErrorMessage = "fecfin es un campo requerido.")]
		public DateTime fecfin
		{
			get {return _fecfin;}
			set
			{
				if (_fecfin != value)
				{
					RaisePropertyChanging(entSegProyecto.Fields.fecfin.ToString());
					_fecfin = value;
					RaisePropertyChanged(entSegProyecto.Fields.fecfin.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna apiestado de la Tabla seg_proyecto
		/// </summary>
		private String _apiestado;
		/// <summary>
		/// 	 Estado en el que se encuentra el registro: debe ser ACTIVO o INACTIVO
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "apiestado", Description = "Estado en el que se encuentra el registro: debe ser ACTIVO o INACTIVO")]
		[EnumDataType(typeof(cApi.Estado))]
		public String apiestado
		{
			get {return _apiestado;}
			set
			{
				if (_apiestado != value)
				{
					RaisePropertyChanging(entSegProyecto.Fields.apiestado.ToString());
					_apiestado = value;
					RaisePropertyChanged(entSegProyecto.Fields.apiestado.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usucre de la Tabla seg_proyecto
		/// </summary>
		private String _usucre;
		/// <summary>
		/// 	 Login o nombre de usuario que ha creado el registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usucre", Description = "Login o nombre de usuario que ha creado el registro")]
		public String usucre
		{
			get {return _usucre;}
			set
			{
				if (_usucre != value)
				{
					RaisePropertyChanging(entSegProyecto.Fields.usucre.ToString());
					_usucre = value;
					RaisePropertyChanged(entSegProyecto.Fields.usucre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna feccre de la Tabla seg_proyecto
		/// </summary>
		private DateTime _feccre;
		/// <summary>
		/// 	 Fecha de creacion del registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "feccre", Description = "Fecha de creacion del registro")]
		public DateTime feccre
		{
			get {return _feccre;}
			set
			{
				if (_feccre != value)
				{
					RaisePropertyChanging(entSegProyecto.Fields.feccre.ToString());
					_feccre = value;
					RaisePropertyChanged(entSegProyecto.Fields.feccre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usumod de la Tabla seg_proyecto
		/// </summary>
		private String _usumod;
		/// <summary>
		/// 	 Login o nombre de usuario que ha realizado la ULTIMA modificacion registro
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usumod", Description = "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro")]
		public String usumod
		{
			get {return _usumod;}
			set
			{
				if (_usumod != value)
				{
					RaisePropertyChanging(entSegProyecto.Fields.usumod.ToString());
					_usumod = value;
					RaisePropertyChanged(entSegProyecto.Fields.usumod.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna fecmod de la Tabla seg_proyecto
		/// </summary>
		private DateTime? _fecmod;
		/// <summary>
		/// 	 Fecha en la que se ha realizado la ULTIMA modificacion registro
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "fecmod", Description = "Fecha en la que se ha realizado la ULTIMA modificacion registro")]
		public DateTime? fecmod
		{
			get {return _fecmod;}
			set
			{
				if (_fecmod != value)
				{
					RaisePropertyChanging(entSegProyecto.Fields.fecmod.ToString());
					_fecmod = value;
					RaisePropertyChanged(entSegProyecto.Fields.fecmod.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna color_web de la Tabla seg_proyecto
		/// </summary>
		private String _color_web;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna color_web de la Tabla seg_proyecto
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "color_web", Description = " Propiedad publica de tipo String que representa a la columna color_web de la Tabla seg_proyecto")]
		public String color_web
		{
			get {return _color_web;}
			set
			{
				if (_color_web != value)
				{
					RaisePropertyChanging(entSegProyecto.Fields.color_web.ToString());
					_color_web = value;
					RaisePropertyChanged(entSegProyecto.Fields.color_web.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna color_movil de la Tabla seg_proyecto
		/// </summary>
		private String _color_movil;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna color_movil de la Tabla seg_proyecto
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "color_movil", Description = " Propiedad publica de tipo String que representa a la columna color_movil de la Tabla seg_proyecto")]
		public String color_movil
		{
			get {return _color_movil;}
			set
			{
				if (_color_movil != value)
				{
					RaisePropertyChanging(entSegProyecto.Fields.color_movil.ToString());
					_color_movil = value;
					RaisePropertyChanged(entSegProyecto.Fields.color_movil.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna color_font de la Tabla seg_proyecto
		/// </summary>
		private String _color_font;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna color_font de la Tabla seg_proyecto
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "color_font", Description = " Propiedad publica de tipo String que representa a la columna color_font de la Tabla seg_proyecto")]
		public String color_font
		{
			get {return _color_font;}
			set
			{
				if (_color_font != value)
				{
					RaisePropertyChanging(entSegProyecto.Fields.color_font.ToString());
					_color_font = value;
					RaisePropertyChanged(entSegProyecto.Fields.color_font.ToString());
				}
			}
		}


	}
}

