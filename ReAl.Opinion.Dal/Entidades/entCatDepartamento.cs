#region 
/***********************************************************************************************************
	NOMBRE:       entCatDepartamento
	DESCRIPCION:
		Clase que define un objeto para la Tabla cat_departamento

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.Opinion.Dal.Entidades
{
	public class entCatDepartamento : cBaseClass
	{
		public const String strNombreTabla = "cat_departamento";
		public const String strAliasTabla = "cat_departamento";
		public enum Fields
		{
			id_departamento
			,codigo
			,nombre
			,latitud
			,longitud
			,abreviatura
			,apiestado
			,usucre
			,feccre
			,usumod
			,fecmod

		}
		
		#region Constructoress
		
		public entCatDepartamento()
		{
			//Inicializacion de Variables
			this.codigo = null;
			this.nombre = null;
			this.latitud = null;
			this.longitud = null;
			this.abreviatura = null;
			this.apiestado = null;
			this.usucre = null;
			this.usumod = null;
			this.fecmod = null;
		}
		
		public entCatDepartamento(entCatDepartamento obj)
		{
			this.id_departamento = obj.id_departamento;
			this.codigo = obj.codigo;
			this.nombre = obj.nombre;
			this.latitud = obj.latitud;
			this.longitud = obj.longitud;
			this.abreviatura = obj.abreviatura;
			this.apiestado = obj.apiestado;
			this.usucre = obj.usucre;
			this.feccre = obj.feccre;
			this.usumod = obj.usumod;
			this.fecmod = obj.fecmod;
		}
		
		#endregion
		
		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_departamento de la Tabla cat_departamento
		/// </summary>
		private int _id_departamento;
		/// <summary>
		/// 	 Llave primaria del identificador del departamento
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: Yes
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_departamento", Description = "Llave primaria del identificador del departamento")]
		[Required(ErrorMessage = "id_departamento es un campo requerido.")]
		[Key]
		public int id_departamento
		{
			get {return _id_departamento;}
			set
			{
				if (_id_departamento != value)
				{
					RaisePropertyChanging(entCatDepartamento.Fields.id_departamento.ToString());
					_id_departamento = value;
					RaisePropertyChanged(entCatDepartamento.Fields.id_departamento.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna codigo de la Tabla cat_departamento
		/// </summary>
		private String _codigo;
		/// <summary>
		/// 	 Codigo de departamento
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(2, MinimumLength=0)]
		[Display(Name = "codigo", Description = "Codigo de departamento")]
		public String codigo
		{
			get {return _codigo;}
			set
			{
				if (_codigo != value)
				{
					RaisePropertyChanging(entCatDepartamento.Fields.codigo.ToString());
					_codigo = value;
					RaisePropertyChanged(entCatDepartamento.Fields.codigo.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna nombre de la Tabla cat_departamento
		/// </summary>
		private String _nombre;
		/// <summary>
		/// 	 Nombre del Departamento
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "nombre", Description = "Nombre del Departamento")]
		public String nombre
		{
			get {return _nombre;}
			set
			{
				if (_nombre != value)
				{
					RaisePropertyChanging(entCatDepartamento.Fields.nombre.ToString());
					_nombre = value;
					RaisePropertyChanged(entCatDepartamento.Fields.nombre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo Decimal que representa a la columna latitud de la Tabla cat_departamento
		/// </summary>
		private Decimal? _latitud;
		/// <summary>
		/// 	 Latitud del punto CERO de la Capital de Departamento
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "latitud", Description = "Latitud del punto CERO de la Capital de Departamento")]
		public Decimal? latitud
		{
			get {return _latitud;}
			set
			{
				if (_latitud != value)
				{
					RaisePropertyChanging(entCatDepartamento.Fields.latitud.ToString());
					_latitud = value;
					RaisePropertyChanged(entCatDepartamento.Fields.latitud.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo Decimal que representa a la columna longitud de la Tabla cat_departamento
		/// </summary>
		private Decimal? _longitud;
		/// <summary>
		/// 	 Longitud del punto CERO de la Capital de Departamento
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "longitud", Description = "Longitud del punto CERO de la Capital de Departamento")]
		public Decimal? longitud
		{
			get {return _longitud;}
			set
			{
				if (_longitud != value)
				{
					RaisePropertyChanging(entCatDepartamento.Fields.longitud.ToString());
					_longitud = value;
					RaisePropertyChanged(entCatDepartamento.Fields.longitud.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna abreviatura de la Tabla cat_departamento
		/// </summary>
		private String _abreviatura;
		/// <summary>
		/// 	 Abreviatura del Departamento
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(3, MinimumLength=0)]
		[Display(Name = "abreviatura", Description = "Abreviatura del Departamento")]
		public String abreviatura
		{
			get {return _abreviatura;}
			set
			{
				if (_abreviatura != value)
				{
					RaisePropertyChanging(entCatDepartamento.Fields.abreviatura.ToString());
					_abreviatura = value;
					RaisePropertyChanged(entCatDepartamento.Fields.abreviatura.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna apiestado de la Tabla cat_departamento
		/// </summary>
		private String _apiestado;
		/// <summary>
		/// 	 Estado en el que se encuentra el registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "apiestado", Description = "Estado en el que se encuentra el registro")]
		[EnumDataType(typeof(cApi.Estado))]
		public String apiestado
		{
			get {return _apiestado;}
			set
			{
				if (_apiestado != value)
				{
					RaisePropertyChanging(entCatDepartamento.Fields.apiestado.ToString());
					_apiestado = value;
					RaisePropertyChanged(entCatDepartamento.Fields.apiestado.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usucre de la Tabla cat_departamento
		/// </summary>
		private String _usucre;
		/// <summary>
		/// 	 Login o nombre de usuario que ha creado el registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usucre", Description = "Login o nombre de usuario que ha creado el registro")]
		public String usucre
		{
			get {return _usucre;}
			set
			{
				if (_usucre != value)
				{
					RaisePropertyChanging(entCatDepartamento.Fields.usucre.ToString());
					_usucre = value;
					RaisePropertyChanged(entCatDepartamento.Fields.usucre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna feccre de la Tabla cat_departamento
		/// </summary>
		private DateTime _feccre;
		/// <summary>
		/// 	 Fecha de creacion del registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "feccre", Description = "Fecha de creacion del registro")]
		public DateTime feccre
		{
			get {return _feccre;}
			set
			{
				if (_feccre != value)
				{
					RaisePropertyChanging(entCatDepartamento.Fields.feccre.ToString());
					_feccre = value;
					RaisePropertyChanged(entCatDepartamento.Fields.feccre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usumod de la Tabla cat_departamento
		/// </summary>
		private String _usumod;
		/// <summary>
		/// 	 Login o nombre de usuario que ha realizado la ULTIMA modificacion registro
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usumod", Description = "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro")]
		public String usumod
		{
			get {return _usumod;}
			set
			{
				if (_usumod != value)
				{
					RaisePropertyChanging(entCatDepartamento.Fields.usumod.ToString());
					_usumod = value;
					RaisePropertyChanged(entCatDepartamento.Fields.usumod.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna fecmod de la Tabla cat_departamento
		/// </summary>
		private DateTime? _fecmod;
		/// <summary>
		/// 	 Fecha en la que se ha realizado la ULTIMA modificacion registro
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "fecmod", Description = "Fecha en la que se ha realizado la ULTIMA modificacion registro")]
		public DateTime? fecmod
		{
			get {return _fecmod;}
			set
			{
				if (_fecmod != value)
				{
					RaisePropertyChanging(entCatDepartamento.Fields.fecmod.ToString());
					_fecmod = value;
					RaisePropertyChanged(entCatDepartamento.Fields.fecmod.ToString());
				}
			}
		}


	}
}

