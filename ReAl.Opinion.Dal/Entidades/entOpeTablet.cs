#region 
/***********************************************************************************************************
	NOMBRE:       entOpeTablet
	DESCRIPCION:
		Clase que define un objeto para la Tabla ope_tablet

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.Opinion.Dal.Entidades
{
	public class entOpeTablet : cBaseClass
	{
		public const String strNombreTabla = "ope_tablet";
		public const String strAliasTabla = "ope_tablet";
		public enum Fields
		{
			id_tablet
			,imei
			,marca
			,modelo
			,apiestado
			,usucre
			,feccre
			,usumod
			,fecmod

		}
		
		#region Constructoress
		
		public entOpeTablet()
		{
			//Inicializacion de Variables
			this.imei = null;
			this.marca = null;
			this.modelo = null;
			this.apiestado = null;
			this.usucre = null;
			this.usumod = null;
			this.fecmod = null;
		}
		
		public entOpeTablet(entOpeTablet obj)
		{
			this.id_tablet = obj.id_tablet;
			this.imei = obj.imei;
			this.marca = obj.marca;
			this.modelo = obj.modelo;
			this.apiestado = obj.apiestado;
			this.usucre = obj.usucre;
			this.feccre = obj.feccre;
			this.usumod = obj.usumod;
			this.fecmod = obj.fecmod;
		}
		
		#endregion
		
		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_tablet de la Tabla ope_tablet
		/// </summary>
		private int _id_tablet;
		/// <summary>
		/// 	 Es el identificador unico que representa al registro en la tabla
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: Yes
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_tablet", Description = "Es el identificador unico que representa al registro en la tabla")]
		[Required(ErrorMessage = "id_tablet es un campo requerido.")]
		[Key]
		public int id_tablet
		{
			get {return _id_tablet;}
			set
			{
				if (_id_tablet != value)
				{
					RaisePropertyChanging(entOpeTablet.Fields.id_tablet.ToString());
					_id_tablet = value;
					RaisePropertyChanged(entOpeTablet.Fields.id_tablet.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna imei de la Tabla ope_tablet
		/// </summary>
		private String _imei;
		/// <summary>
		/// 	 Es el Nro IMEI del dispositivo
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "imei", Description = "Es el Nro IMEI del dispositivo")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "imei es un campo requerido.")]
		public String imei
		{
			get {return _imei;}
			set
			{
				if (_imei != value)
				{
					RaisePropertyChanging(entOpeTablet.Fields.imei.ToString());
					_imei = value;
					RaisePropertyChanged(entOpeTablet.Fields.imei.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna marca de la Tabla ope_tablet
		/// </summary>
		private String _marca;
		/// <summary>
		/// 	 Marca del dispositivo
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "marca", Description = "Marca del dispositivo")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "marca es un campo requerido.")]
		public String marca
		{
			get {return _marca;}
			set
			{
				if (_marca != value)
				{
					RaisePropertyChanging(entOpeTablet.Fields.marca.ToString());
					_marca = value;
					RaisePropertyChanged(entOpeTablet.Fields.marca.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna modelo de la Tabla ope_tablet
		/// </summary>
		private String _modelo;
		/// <summary>
		/// 	 Modelo del Dispositivo
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "modelo", Description = "Modelo del Dispositivo")]
		public String modelo
		{
			get {return _modelo;}
			set
			{
				if (_modelo != value)
				{
					RaisePropertyChanging(entOpeTablet.Fields.modelo.ToString());
					_modelo = value;
					RaisePropertyChanged(entOpeTablet.Fields.modelo.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna apiestado de la Tabla ope_tablet
		/// </summary>
		private String _apiestado;
		/// <summary>
		/// 	 Estado en el que se encuentra el registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "apiestado", Description = "Estado en el que se encuentra el registro")]
		[EnumDataType(typeof(cApi.Estado))]
		public String apiestado
		{
			get {return _apiestado;}
			set
			{
				if (_apiestado != value)
				{
					RaisePropertyChanging(entOpeTablet.Fields.apiestado.ToString());
					_apiestado = value;
					RaisePropertyChanged(entOpeTablet.Fields.apiestado.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usucre de la Tabla ope_tablet
		/// </summary>
		private String _usucre;
		/// <summary>
		/// 	 Login o nombre de usuario que ha creado el registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usucre", Description = "Login o nombre de usuario que ha creado el registro")]
		public String usucre
		{
			get {return _usucre;}
			set
			{
				if (_usucre != value)
				{
					RaisePropertyChanging(entOpeTablet.Fields.usucre.ToString());
					_usucre = value;
					RaisePropertyChanged(entOpeTablet.Fields.usucre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna feccre de la Tabla ope_tablet
		/// </summary>
		private DateTime _feccre;
		/// <summary>
		/// 	 Fecha de creacion del registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "feccre", Description = "Fecha de creacion del registro")]
		public DateTime feccre
		{
			get {return _feccre;}
			set
			{
				if (_feccre != value)
				{
					RaisePropertyChanging(entOpeTablet.Fields.feccre.ToString());
					_feccre = value;
					RaisePropertyChanged(entOpeTablet.Fields.feccre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usumod de la Tabla ope_tablet
		/// </summary>
		private String _usumod;
		/// <summary>
		/// 	 Login o nombre de usuario que ha realizado la ULTIMA modificacion registro
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usumod", Description = "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro")]
		public String usumod
		{
			get {return _usumod;}
			set
			{
				if (_usumod != value)
				{
					RaisePropertyChanging(entOpeTablet.Fields.usumod.ToString());
					_usumod = value;
					RaisePropertyChanged(entOpeTablet.Fields.usumod.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna fecmod de la Tabla ope_tablet
		/// </summary>
		private DateTime? _fecmod;
		/// <summary>
		/// 	 Fecha en la que se ha realizado la ULTIMA modificacion registro
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "fecmod", Description = "Fecha en la que se ha realizado la ULTIMA modificacion registro")]
		public DateTime? fecmod
		{
			get {return _fecmod;}
			set
			{
				if (_fecmod != value)
				{
					RaisePropertyChanging(entOpeTablet.Fields.fecmod.ToString());
					_fecmod = value;
					RaisePropertyChanged(entOpeTablet.Fields.fecmod.ToString());
				}
			}
		}


	}
}

