#region 
/***********************************************************************************************************
	NOMBRE:       entSegPagina
	DESCRIPCION:
		Clase que define un objeto para la Tabla seg_pagina

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.Opinion.Dal.Entidades
{
	public class entSegPagina : cBaseClass
	{
		public const String strNombreTabla = "seg_pagina";
		public const String strAliasTabla = "seg_pagina";
		public enum Fields
		{
			id_pagina
			,id_pagina_padre
			,enlace
			,icono
			,nombre
			,apiestado
			,usucre
			,feccre
			,usumod
			,fecmod
			,orden

		}
		
		#region Constructoress
		
		public entSegPagina()
		{
			//Inicializacion de Variables
			this.id_pagina_padre = null;
			this.enlace = null;
			this.icono = null;
			this.nombre = null;
			this.apiestado = null;
			this.usucre = null;
			this.usumod = null;
			this.fecmod = null;
			this.orden = null;
		}
		
		public entSegPagina(entSegPagina obj)
		{
			this.id_pagina = obj.id_pagina;
			this.id_pagina_padre = obj.id_pagina_padre;
			this.enlace = obj.enlace;
			this.icono = obj.icono;
			this.nombre = obj.nombre;
			this.apiestado = obj.apiestado;
			this.usucre = obj.usucre;
			this.feccre = obj.feccre;
			this.usumod = obj.usumod;
			this.fecmod = obj.fecmod;
			this.orden = obj.orden;
		}
		
		#endregion
		
		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_pagina de la Tabla seg_pagina
		/// </summary>
		private int _id_pagina;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna id_pagina de la Tabla seg_pagina
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: Yes
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_pagina", Description = " Propiedad publica de tipo int que representa a la columna id_pagina de la Tabla seg_pagina")]
		[Required(ErrorMessage = "id_pagina es un campo requerido.")]
		[Key]
		public int id_pagina
		{
			get {return _id_pagina;}
			set
			{
				if (_id_pagina != value)
				{
					RaisePropertyChanging(entSegPagina.Fields.id_pagina.ToString());
					_id_pagina = value;
					RaisePropertyChanged(entSegPagina.Fields.id_pagina.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_pagina_padre de la Tabla seg_pagina
		/// </summary>
		private int? _id_pagina_padre;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna id_pagina_padre de la Tabla seg_pagina
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_pagina_padre", Description = " Propiedad publica de tipo int que representa a la columna id_pagina_padre de la Tabla seg_pagina")]
		public int? id_pagina_padre
		{
			get {return _id_pagina_padre;}
			set
			{
				if (_id_pagina_padre != value)
				{
					RaisePropertyChanging(entSegPagina.Fields.id_pagina_padre.ToString());
					_id_pagina_padre = value;
					RaisePropertyChanged(entSegPagina.Fields.id_pagina_padre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna enlace de la Tabla seg_pagina
		/// </summary>
		private String _enlace;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna enlace de la Tabla seg_pagina
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "enlace", Description = " Propiedad publica de tipo String que representa a la columna enlace de la Tabla seg_pagina")]
		public String enlace
		{
			get {return _enlace;}
			set
			{
				if (_enlace != value)
				{
					RaisePropertyChanging(entSegPagina.Fields.enlace.ToString());
					_enlace = value;
					RaisePropertyChanged(entSegPagina.Fields.enlace.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna icono de la Tabla seg_pagina
		/// </summary>
		private String _icono;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna icono de la Tabla seg_pagina
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "icono", Description = " Propiedad publica de tipo String que representa a la columna icono de la Tabla seg_pagina")]
		public String icono
		{
			get {return _icono;}
			set
			{
				if (_icono != value)
				{
					RaisePropertyChanging(entSegPagina.Fields.icono.ToString());
					_icono = value;
					RaisePropertyChanged(entSegPagina.Fields.icono.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna nombre de la Tabla seg_pagina
		/// </summary>
		private String _nombre;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna nombre de la Tabla seg_pagina
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "nombre", Description = " Propiedad publica de tipo String que representa a la columna nombre de la Tabla seg_pagina")]
		public String nombre
		{
			get {return _nombre;}
			set
			{
				if (_nombre != value)
				{
					RaisePropertyChanging(entSegPagina.Fields.nombre.ToString());
					_nombre = value;
					RaisePropertyChanged(entSegPagina.Fields.nombre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna apiestado de la Tabla seg_pagina
		/// </summary>
		private String _apiestado;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna apiestado de la Tabla seg_pagina
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "apiestado", Description = " Propiedad publica de tipo String que representa a la columna apiestado de la Tabla seg_pagina")]
		[EnumDataType(typeof(cApi.Estado))]
		public String apiestado
		{
			get {return _apiestado;}
			set
			{
				if (_apiestado != value)
				{
					RaisePropertyChanging(entSegPagina.Fields.apiestado.ToString());
					_apiestado = value;
					RaisePropertyChanged(entSegPagina.Fields.apiestado.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usucre de la Tabla seg_pagina
		/// </summary>
		private String _usucre;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna usucre de la Tabla seg_pagina
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usucre", Description = " Propiedad publica de tipo String que representa a la columna usucre de la Tabla seg_pagina")]
		public String usucre
		{
			get {return _usucre;}
			set
			{
				if (_usucre != value)
				{
					RaisePropertyChanging(entSegPagina.Fields.usucre.ToString());
					_usucre = value;
					RaisePropertyChanged(entSegPagina.Fields.usucre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna feccre de la Tabla seg_pagina
		/// </summary>
		private DateTime _feccre;
		/// <summary>
		/// 	 Propiedad publica de tipo DateTime que representa a la columna feccre de la Tabla seg_pagina
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "feccre", Description = " Propiedad publica de tipo DateTime que representa a la columna feccre de la Tabla seg_pagina")]
		public DateTime feccre
		{
			get {return _feccre;}
			set
			{
				if (_feccre != value)
				{
					RaisePropertyChanging(entSegPagina.Fields.feccre.ToString());
					_feccre = value;
					RaisePropertyChanged(entSegPagina.Fields.feccre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usumod de la Tabla seg_pagina
		/// </summary>
		private String _usumod;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna usumod de la Tabla seg_pagina
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usumod", Description = " Propiedad publica de tipo String que representa a la columna usumod de la Tabla seg_pagina")]
		public String usumod
		{
			get {return _usumod;}
			set
			{
				if (_usumod != value)
				{
					RaisePropertyChanging(entSegPagina.Fields.usumod.ToString());
					_usumod = value;
					RaisePropertyChanged(entSegPagina.Fields.usumod.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna fecmod de la Tabla seg_pagina
		/// </summary>
		private DateTime? _fecmod;
		/// <summary>
		/// 	 Propiedad publica de tipo DateTime que representa a la columna fecmod de la Tabla seg_pagina
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "fecmod", Description = " Propiedad publica de tipo DateTime que representa a la columna fecmod de la Tabla seg_pagina")]
		public DateTime? fecmod
		{
			get {return _fecmod;}
			set
			{
				if (_fecmod != value)
				{
					RaisePropertyChanging(entSegPagina.Fields.fecmod.ToString());
					_fecmod = value;
					RaisePropertyChanged(entSegPagina.Fields.fecmod.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna orden de la Tabla seg_pagina
		/// </summary>
		private int? _orden;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna orden de la Tabla seg_pagina
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "orden", Description = " Propiedad publica de tipo int que representa a la columna orden de la Tabla seg_pagina")]
		public int? orden
		{
			get {return _orden;}
			set
			{
				if (_orden != value)
				{
					RaisePropertyChanging(entSegPagina.Fields.orden.ToString());
					_orden = value;
					RaisePropertyChanged(entSegPagina.Fields.orden.ToString());
				}
			}
		}


	}
}

