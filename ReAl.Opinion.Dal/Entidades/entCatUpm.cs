#region 
/***********************************************************************************************************
	NOMBRE:       entCatUpm
	DESCRIPCION:
		Clase que define un objeto para la Tabla cat_upm

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.Opinion.Dal.Entidades
{
	public class entCatUpm : cBaseClass
	{
		public const String strNombreTabla = "cat_upm";
		public const String strAliasTabla = "cat_upm";
		public enum Fields
		{
			id_upm
			,id_proyecto
			,id_departamento
			,codigo
			,nombre
			,fecinicio
			,latitud
			,longitud
			,apiestado
			,usucre
			,feccre
			,usumod
			,fecmod

		}
		
		#region Constructoress
		
		public entCatUpm()
		{
			//Inicializacion de Variables
			this.id_departamento = null;
			this.codigo = null;
			this.nombre = null;
			this.apiestado = null;
			this.usucre = null;
			this.usumod = null;
			this.fecmod = null;
		}
		
		public entCatUpm(entCatUpm obj)
		{
			this.id_upm = obj.id_upm;
			this.id_proyecto = obj.id_proyecto;
			this.id_departamento = obj.id_departamento;
			this.codigo = obj.codigo;
			this.nombre = obj.nombre;
			this.fecinicio = obj.fecinicio;
			this.latitud = obj.latitud;
			this.longitud = obj.longitud;
			this.apiestado = obj.apiestado;
			this.usucre = obj.usucre;
			this.feccre = obj.feccre;
			this.usumod = obj.usumod;
			this.fecmod = obj.fecmod;
		}
		
		#endregion
		
		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_upm de la Tabla cat_upm
		/// </summary>
		private int _id_upm;
		/// <summary>
		/// 	 Es el identificador unico que representa al registro en la tabla
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: Yes
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_upm", Description = "Es el identificador unico que representa al registro en la tabla")]
		[Required(ErrorMessage = "id_upm es un campo requerido.")]
		[Key]
		public int id_upm
		{
			get {return _id_upm;}
			set
			{
				if (_id_upm != value)
				{
					RaisePropertyChanging(entCatUpm.Fields.id_upm.ToString());
					_id_upm = value;
					RaisePropertyChanged(entCatUpm.Fields.id_upm.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_proyecto de la Tabla cat_upm
		/// </summary>
		private int _id_proyecto;
		/// <summary>
		/// 	 Es el identificador unico de la tabla seg_proyecto
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_proyecto", Description = "Es el identificador unico de la tabla seg_proyecto")]
		[Required(ErrorMessage = "id_proyecto es un campo requerido.")]
		public int id_proyecto
		{
			get {return _id_proyecto;}
			set
			{
				if (_id_proyecto != value)
				{
					RaisePropertyChanging(entCatUpm.Fields.id_proyecto.ToString());
					_id_proyecto = value;
					RaisePropertyChanged(entCatUpm.Fields.id_proyecto.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_departamento de la Tabla cat_upm
		/// </summary>
		private int? _id_departamento;
		/// <summary>
		/// 	 Es el identificador unico de la tabla cat_departamento
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: Yes
		/// </summary>
		[Display(Name = "id_departamento", Description = "Es el identificador unico de la tabla cat_departamento")]
		public int? id_departamento
		{
			get {return _id_departamento;}
			set
			{
				if (_id_departamento != value)
				{
					RaisePropertyChanging(entCatUpm.Fields.id_departamento.ToString());
					_id_departamento = value;
					RaisePropertyChanged(entCatUpm.Fields.id_departamento.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna codigo de la Tabla cat_upm
		/// </summary>
		private String _codigo;
		/// <summary>
		/// 	 Codigo con el que se representa a la UPM
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "codigo", Description = "Codigo con el que se representa a la UPM")]
		public String codigo
		{
			get {return _codigo;}
			set
			{
				if (_codigo != value)
				{
					RaisePropertyChanging(entCatUpm.Fields.codigo.ToString());
					_codigo = value;
					RaisePropertyChanged(entCatUpm.Fields.codigo.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna nombre de la Tabla cat_upm
		/// </summary>
		private String _nombre;
		/// <summary>
		/// 	 Nombre con el que se representa a la UPM
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "nombre", Description = "Nombre con el que se representa a la UPM")]
		public String nombre
		{
			get {return _nombre;}
			set
			{
				if (_nombre != value)
				{
					RaisePropertyChanging(entCatUpm.Fields.nombre.ToString());
					_nombre = value;
					RaisePropertyChanged(entCatUpm.Fields.nombre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna fecinicio de la Tabla cat_upm
		/// </summary>
		private DateTime _fecinicio;
		/// <summary>
		/// 	 Fecha desde la que es valida la UPM
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "fecinicio", Description = "Fecha desde la que es valida la UPM")]
		[Required(ErrorMessage = "fecinicio es un campo requerido.")]
		public DateTime fecinicio
		{
			get {return _fecinicio;}
			set
			{
				if (_fecinicio != value)
				{
					RaisePropertyChanging(entCatUpm.Fields.fecinicio.ToString());
					_fecinicio = value;
					RaisePropertyChanged(entCatUpm.Fields.fecinicio.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo Decimal que representa a la columna latitud de la Tabla cat_upm
		/// </summary>
		private Decimal _latitud;
		/// <summary>
		/// 	 Latitud del punto CERO de la Capital de Departamento
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "latitud", Description = "Latitud del punto CERO de la Capital de Departamento")]
		[Required(ErrorMessage = "latitud es un campo requerido.")]
		public Decimal latitud
		{
			get {return _latitud;}
			set
			{
				if (_latitud != value)
				{
					RaisePropertyChanging(entCatUpm.Fields.latitud.ToString());
					_latitud = value;
					RaisePropertyChanged(entCatUpm.Fields.latitud.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo Decimal que representa a la columna longitud de la Tabla cat_upm
		/// </summary>
		private Decimal _longitud;
		/// <summary>
		/// 	 Longitud del punto CERO de la Capital de Departamento
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "longitud", Description = "Longitud del punto CERO de la Capital de Departamento")]
		[Required(ErrorMessage = "longitud es un campo requerido.")]
		public Decimal longitud
		{
			get {return _longitud;}
			set
			{
				if (_longitud != value)
				{
					RaisePropertyChanging(entCatUpm.Fields.longitud.ToString());
					_longitud = value;
					RaisePropertyChanged(entCatUpm.Fields.longitud.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna apiestado de la Tabla cat_upm
		/// </summary>
		private String _apiestado;
		/// <summary>
		/// 	 Estado en el que se encuentra el registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "apiestado", Description = "Estado en el que se encuentra el registro")]
		[EnumDataType(typeof(cApi.Estado))]
		public String apiestado
		{
			get {return _apiestado;}
			set
			{
				if (_apiestado != value)
				{
					RaisePropertyChanging(entCatUpm.Fields.apiestado.ToString());
					_apiestado = value;
					RaisePropertyChanged(entCatUpm.Fields.apiestado.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usucre de la Tabla cat_upm
		/// </summary>
		private String _usucre;
		/// <summary>
		/// 	 Login o nombre de usuario que ha creado el registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usucre", Description = "Login o nombre de usuario que ha creado el registro")]
		public String usucre
		{
			get {return _usucre;}
			set
			{
				if (_usucre != value)
				{
					RaisePropertyChanging(entCatUpm.Fields.usucre.ToString());
					_usucre = value;
					RaisePropertyChanged(entCatUpm.Fields.usucre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna feccre de la Tabla cat_upm
		/// </summary>
		private DateTime _feccre;
		/// <summary>
		/// 	 Fecha de creacion del registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "feccre", Description = "Fecha de creacion del registro")]
		public DateTime feccre
		{
			get {return _feccre;}
			set
			{
				if (_feccre != value)
				{
					RaisePropertyChanging(entCatUpm.Fields.feccre.ToString());
					_feccre = value;
					RaisePropertyChanged(entCatUpm.Fields.feccre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usumod de la Tabla cat_upm
		/// </summary>
		private String _usumod;
		/// <summary>
		/// 	 Login o nombre de usuario que ha realizado la ULTIMA modificacion registro
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usumod", Description = "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro")]
		public String usumod
		{
			get {return _usumod;}
			set
			{
				if (_usumod != value)
				{
					RaisePropertyChanging(entCatUpm.Fields.usumod.ToString());
					_usumod = value;
					RaisePropertyChanged(entCatUpm.Fields.usumod.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna fecmod de la Tabla cat_upm
		/// </summary>
		private DateTime? _fecmod;
		/// <summary>
		/// 	 Fecha en la que se ha realizado la ULTIMA modificacion registro
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "fecmod", Description = "Fecha en la que se ha realizado la ULTIMA modificacion registro")]
		public DateTime? fecmod
		{
			get {return _fecmod;}
			set
			{
				if (_fecmod != value)
				{
					RaisePropertyChanging(entCatUpm.Fields.fecmod.ToString());
					_fecmod = value;
					RaisePropertyChanged(entCatUpm.Fields.fecmod.ToString());
				}
			}
		}


	}
}

