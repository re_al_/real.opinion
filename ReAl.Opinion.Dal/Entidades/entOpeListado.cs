#region 
/***********************************************************************************************************
	NOMBRE:       entOpeListado
	DESCRIPCION:
		Clase que define un objeto para la Tabla ope_listado

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        24/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.Opinion.Dal.Entidades
{
	public class entOpeListado : cBaseClass
	{
		public const String strNombreTabla = "ope_listado";
		public const String strAliasTabla = "ope_listado";
		public enum Fields
		{
			id_listado
			,id_upm
			,codigo_manzana_comunidad
			,avenida_calle
			,nro_predio
			,nro_puerta
			,nro_orden_vivienda
			,nro_piso
			,nro_depto
			,uso_vivienda
			,nro_hogares
			,nro_hombres
			,nro_mujeres
			,nombre_jefe
			,nro_voe
			,apiestado
			,usucre
			,feccre
			,usumod
			,fecmod

		}
		
		#region Constructoress
		
		public entOpeListado()
		{
			//Inicializacion de Variables
			this.codigo_manzana_comunidad = null;
			this.avenida_calle = null;
			this.nro_predio = null;
			this.nro_puerta = null;
			this.nro_orden_vivienda = null;
			this.nro_piso = null;
			this.nro_depto = null;
			this.uso_vivienda = null;
			this.nro_hogares = null;
			this.nro_hombres = null;
			this.nro_mujeres = null;
			this.nombre_jefe = null;
			this.nro_voe = null;
			this.apiestado = null;
			this.usucre = null;
			this.usumod = null;
			this.fecmod = null;
		}
		
		public entOpeListado(entOpeListado obj)
		{
			this.id_listado = obj.id_listado;
			this.id_upm = obj.id_upm;
			this.codigo_manzana_comunidad = obj.codigo_manzana_comunidad;
			this.avenida_calle = obj.avenida_calle;
			this.nro_predio = obj.nro_predio;
			this.nro_puerta = obj.nro_puerta;
			this.nro_orden_vivienda = obj.nro_orden_vivienda;
			this.nro_piso = obj.nro_piso;
			this.nro_depto = obj.nro_depto;
			this.uso_vivienda = obj.uso_vivienda;
			this.nro_hogares = obj.nro_hogares;
			this.nro_hombres = obj.nro_hombres;
			this.nro_mujeres = obj.nro_mujeres;
			this.nombre_jefe = obj.nombre_jefe;
			this.nro_voe = obj.nro_voe;
			this.apiestado = obj.apiestado;
			this.usucre = obj.usucre;
			this.feccre = obj.feccre;
			this.usumod = obj.usumod;
			this.fecmod = obj.fecmod;
		}
		
		#endregion
		
		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_listado de la Tabla ope_listado
		/// </summary>
		private int _id_listado;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna id_listado de la Tabla ope_listado
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: Yes
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_listado", Description = " Propiedad publica de tipo int que representa a la columna id_listado de la Tabla ope_listado")]
		[Required(ErrorMessage = "id_listado es un campo requerido.")]
		[Key]
		public int id_listado
		{
			get {return _id_listado;}
			set
			{
				if (_id_listado != value)
				{
					RaisePropertyChanging(entOpeListado.Fields.id_listado.ToString());
					_id_listado = value;
					RaisePropertyChanged(entOpeListado.Fields.id_listado.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_upm de la Tabla ope_listado
		/// </summary>
		private int _id_upm;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna id_upm de la Tabla ope_listado
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: Yes
		/// </summary>
		[Display(Name = "id_upm", Description = " Propiedad publica de tipo int que representa a la columna id_upm de la Tabla ope_listado")]
		[Required(ErrorMessage = "id_upm es un campo requerido.")]
		public int id_upm
		{
			get {return _id_upm;}
			set
			{
				if (_id_upm != value)
				{
					RaisePropertyChanging(entOpeListado.Fields.id_upm.ToString());
					_id_upm = value;
					RaisePropertyChanged(entOpeListado.Fields.id_upm.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna codigo_manzana_comunidad de la Tabla ope_listado
		/// </summary>
		private String _codigo_manzana_comunidad;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna codigo_manzana_comunidad de la Tabla ope_listado
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "codigo_manzana_comunidad", Description = " Propiedad publica de tipo String que representa a la columna codigo_manzana_comunidad de la Tabla ope_listado")]
		public String codigo_manzana_comunidad
		{
			get {return _codigo_manzana_comunidad;}
			set
			{
				if (_codigo_manzana_comunidad != value)
				{
					RaisePropertyChanging(entOpeListado.Fields.codigo_manzana_comunidad.ToString());
					_codigo_manzana_comunidad = value;
					RaisePropertyChanged(entOpeListado.Fields.codigo_manzana_comunidad.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna avenida_calle de la Tabla ope_listado
		/// </summary>
		private String _avenida_calle;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna avenida_calle de la Tabla ope_listado
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "avenida_calle", Description = " Propiedad publica de tipo String que representa a la columna avenida_calle de la Tabla ope_listado")]
		public String avenida_calle
		{
			get {return _avenida_calle;}
			set
			{
				if (_avenida_calle != value)
				{
					RaisePropertyChanging(entOpeListado.Fields.avenida_calle.ToString());
					_avenida_calle = value;
					RaisePropertyChanged(entOpeListado.Fields.avenida_calle.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna nro_predio de la Tabla ope_listado
		/// </summary>
		private int? _nro_predio;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna nro_predio de la Tabla ope_listado
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "nro_predio", Description = " Propiedad publica de tipo int que representa a la columna nro_predio de la Tabla ope_listado")]
		public int? nro_predio
		{
			get {return _nro_predio;}
			set
			{
				if (_nro_predio != value)
				{
					RaisePropertyChanging(entOpeListado.Fields.nro_predio.ToString());
					_nro_predio = value;
					RaisePropertyChanged(entOpeListado.Fields.nro_predio.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna nro_puerta de la Tabla ope_listado
		/// </summary>
		private String _nro_puerta;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna nro_puerta de la Tabla ope_listado
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "nro_puerta", Description = " Propiedad publica de tipo String que representa a la columna nro_puerta de la Tabla ope_listado")]
		public String nro_puerta
		{
			get {return _nro_puerta;}
			set
			{
				if (_nro_puerta != value)
				{
					RaisePropertyChanging(entOpeListado.Fields.nro_puerta.ToString());
					_nro_puerta = value;
					RaisePropertyChanged(entOpeListado.Fields.nro_puerta.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna nro_orden_vivienda de la Tabla ope_listado
		/// </summary>
		private int? _nro_orden_vivienda;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna nro_orden_vivienda de la Tabla ope_listado
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "nro_orden_vivienda", Description = " Propiedad publica de tipo int que representa a la columna nro_orden_vivienda de la Tabla ope_listado")]
		public int? nro_orden_vivienda
		{
			get {return _nro_orden_vivienda;}
			set
			{
				if (_nro_orden_vivienda != value)
				{
					RaisePropertyChanging(entOpeListado.Fields.nro_orden_vivienda.ToString());
					_nro_orden_vivienda = value;
					RaisePropertyChanged(entOpeListado.Fields.nro_orden_vivienda.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna nro_piso de la Tabla ope_listado
		/// </summary>
		private String _nro_piso;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna nro_piso de la Tabla ope_listado
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "nro_piso", Description = " Propiedad publica de tipo String que representa a la columna nro_piso de la Tabla ope_listado")]
		public String nro_piso
		{
			get {return _nro_piso;}
			set
			{
				if (_nro_piso != value)
				{
					RaisePropertyChanging(entOpeListado.Fields.nro_piso.ToString());
					_nro_piso = value;
					RaisePropertyChanged(entOpeListado.Fields.nro_piso.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna nro_depto de la Tabla ope_listado
		/// </summary>
		private String _nro_depto;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna nro_depto de la Tabla ope_listado
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "nro_depto", Description = " Propiedad publica de tipo String que representa a la columna nro_depto de la Tabla ope_listado")]
		public String nro_depto
		{
			get {return _nro_depto;}
			set
			{
				if (_nro_depto != value)
				{
					RaisePropertyChanging(entOpeListado.Fields.nro_depto.ToString());
					_nro_depto = value;
					RaisePropertyChanged(entOpeListado.Fields.nro_depto.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna uso_vivienda de la Tabla ope_listado
		/// </summary>
		private String _uso_vivienda;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna uso_vivienda de la Tabla ope_listado
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "uso_vivienda", Description = " Propiedad publica de tipo String que representa a la columna uso_vivienda de la Tabla ope_listado")]
		public String uso_vivienda
		{
			get {return _uso_vivienda;}
			set
			{
				if (_uso_vivienda != value)
				{
					RaisePropertyChanging(entOpeListado.Fields.uso_vivienda.ToString());
					_uso_vivienda = value;
					RaisePropertyChanged(entOpeListado.Fields.uso_vivienda.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna nro_hogares de la Tabla ope_listado
		/// </summary>
		private int? _nro_hogares;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna nro_hogares de la Tabla ope_listado
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "nro_hogares", Description = " Propiedad publica de tipo int que representa a la columna nro_hogares de la Tabla ope_listado")]
		public int? nro_hogares
		{
			get {return _nro_hogares;}
			set
			{
				if (_nro_hogares != value)
				{
					RaisePropertyChanging(entOpeListado.Fields.nro_hogares.ToString());
					_nro_hogares = value;
					RaisePropertyChanged(entOpeListado.Fields.nro_hogares.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna nro_hombres de la Tabla ope_listado
		/// </summary>
		private int? _nro_hombres;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna nro_hombres de la Tabla ope_listado
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "nro_hombres", Description = " Propiedad publica de tipo int que representa a la columna nro_hombres de la Tabla ope_listado")]
		public int? nro_hombres
		{
			get {return _nro_hombres;}
			set
			{
				if (_nro_hombres != value)
				{
					RaisePropertyChanging(entOpeListado.Fields.nro_hombres.ToString());
					_nro_hombres = value;
					RaisePropertyChanged(entOpeListado.Fields.nro_hombres.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna nro_mujeres de la Tabla ope_listado
		/// </summary>
		private int? _nro_mujeres;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna nro_mujeres de la Tabla ope_listado
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "nro_mujeres", Description = " Propiedad publica de tipo int que representa a la columna nro_mujeres de la Tabla ope_listado")]
		public int? nro_mujeres
		{
			get {return _nro_mujeres;}
			set
			{
				if (_nro_mujeres != value)
				{
					RaisePropertyChanging(entOpeListado.Fields.nro_mujeres.ToString());
					_nro_mujeres = value;
					RaisePropertyChanged(entOpeListado.Fields.nro_mujeres.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna nombre_jefe de la Tabla ope_listado
		/// </summary>
		private String _nombre_jefe;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna nombre_jefe de la Tabla ope_listado
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "nombre_jefe", Description = " Propiedad publica de tipo String que representa a la columna nombre_jefe de la Tabla ope_listado")]
		public String nombre_jefe
		{
			get {return _nombre_jefe;}
			set
			{
				if (_nombre_jefe != value)
				{
					RaisePropertyChanging(entOpeListado.Fields.nombre_jefe.ToString());
					_nombre_jefe = value;
					RaisePropertyChanged(entOpeListado.Fields.nombre_jefe.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna nro_voe de la Tabla ope_listado
		/// </summary>
		private int? _nro_voe;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna nro_voe de la Tabla ope_listado
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "nro_voe", Description = " Propiedad publica de tipo int que representa a la columna nro_voe de la Tabla ope_listado")]
		public int? nro_voe
		{
			get {return _nro_voe;}
			set
			{
				if (_nro_voe != value)
				{
					RaisePropertyChanging(entOpeListado.Fields.nro_voe.ToString());
					_nro_voe = value;
					RaisePropertyChanged(entOpeListado.Fields.nro_voe.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna apiestado de la Tabla ope_listado
		/// </summary>
		private String _apiestado;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna apiestado de la Tabla ope_listado
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "apiestado", Description = " Propiedad publica de tipo String que representa a la columna apiestado de la Tabla ope_listado")]
		[EnumDataType(typeof(cApi.Estado))]
		public String apiestado
		{
			get {return _apiestado;}
			set
			{
				if (_apiestado != value)
				{
					RaisePropertyChanging(entOpeListado.Fields.apiestado.ToString());
					_apiestado = value;
					RaisePropertyChanged(entOpeListado.Fields.apiestado.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usucre de la Tabla ope_listado
		/// </summary>
		private String _usucre;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna usucre de la Tabla ope_listado
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usucre", Description = " Propiedad publica de tipo String que representa a la columna usucre de la Tabla ope_listado")]
		public String usucre
		{
			get {return _usucre;}
			set
			{
				if (_usucre != value)
				{
					RaisePropertyChanging(entOpeListado.Fields.usucre.ToString());
					_usucre = value;
					RaisePropertyChanged(entOpeListado.Fields.usucre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna feccre de la Tabla ope_listado
		/// </summary>
		private DateTime _feccre;
		/// <summary>
		/// 	 Propiedad publica de tipo DateTime que representa a la columna feccre de la Tabla ope_listado
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "feccre", Description = " Propiedad publica de tipo DateTime que representa a la columna feccre de la Tabla ope_listado")]
		public DateTime feccre
		{
			get {return _feccre;}
			set
			{
				if (_feccre != value)
				{
					RaisePropertyChanging(entOpeListado.Fields.feccre.ToString());
					_feccre = value;
					RaisePropertyChanged(entOpeListado.Fields.feccre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usumod de la Tabla ope_listado
		/// </summary>
		private String _usumod;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna usumod de la Tabla ope_listado
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usumod", Description = " Propiedad publica de tipo String que representa a la columna usumod de la Tabla ope_listado")]
		public String usumod
		{
			get {return _usumod;}
			set
			{
				if (_usumod != value)
				{
					RaisePropertyChanging(entOpeListado.Fields.usumod.ToString());
					_usumod = value;
					RaisePropertyChanged(entOpeListado.Fields.usumod.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna fecmod de la Tabla ope_listado
		/// </summary>
		private DateTime? _fecmod;
		/// <summary>
		/// 	 Propiedad publica de tipo DateTime que representa a la columna fecmod de la Tabla ope_listado
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "fecmod", Description = " Propiedad publica de tipo DateTime que representa a la columna fecmod de la Tabla ope_listado")]
		public DateTime? fecmod
		{
			get {return _fecmod;}
			set
			{
				if (_fecmod != value)
				{
					RaisePropertyChanging(entOpeListado.Fields.fecmod.ToString());
					_fecmod = value;
					RaisePropertyChanged(entOpeListado.Fields.fecmod.ToString());
				}
			}
		}


	}
}

