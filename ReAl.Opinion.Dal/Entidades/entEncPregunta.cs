#region 
/***********************************************************************************************************
	NOMBRE:       entEncPregunta
	DESCRIPCION:
		Clase que define un objeto para la Tabla enc_pregunta

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.Opinion.Dal.Entidades
{
	public class entEncPregunta : cBaseClass
	{
		public const String strNombreTabla = "enc_pregunta";
		public const String strAliasTabla = "enc_pregunta";
		public enum Fields
		{
			id_pregunta
			,id_proyecto
			,id_nivel
			,id_seccion
			,codigo_pregunta
			,pregunta
			,ayuda
			,id_tipo_pregunta
			,minimo
			,maximo
			,catalogo
			,longitud
			,codigo_especifique
			,mostrar_ventana
			,variable
			,formula
			,rpn_formula
			,regla
			,rpn
			,mensaje
			,revision
			,apiestado
			,usucre
			,feccre
			,usumod
			,fecmod
			,instruccion
			,bucle
			,variable_bucle
			,codigo_especial

		}
		
		#region Constructoress
		
		public entEncPregunta()
		{
			//Inicializacion de Variables
			this.id_seccion = null;
			this.codigo_pregunta = null;
			this.pregunta = null;
			this.ayuda = null;
			this.catalogo = null;
			this.longitud = null;
			this.codigo_especifique = null;
			this.variable = null;
			this.formula = null;
			this.rpn_formula = null;
			this.regla = null;
			this.rpn = null;
			this.mensaje = null;
			this.revision = null;
			this.apiestado = null;
			this.usucre = null;
			this.usumod = null;
			this.fecmod = null;
			this.instruccion = null;
			this.variable_bucle = null;
		}
		
		public entEncPregunta(entEncPregunta obj)
		{
			this.id_pregunta = obj.id_pregunta;
			this.id_proyecto = obj.id_proyecto;
			this.id_nivel = obj.id_nivel;
			this.id_seccion = obj.id_seccion;
			this.codigo_pregunta = obj.codigo_pregunta;
			this.pregunta = obj.pregunta;
			this.ayuda = obj.ayuda;
			this.id_tipo_pregunta = obj.id_tipo_pregunta;
			this.minimo = obj.minimo;
			this.maximo = obj.maximo;
			this.catalogo = obj.catalogo;
			this.longitud = obj.longitud;
			this.codigo_especifique = obj.codigo_especifique;
			this.mostrar_ventana = obj.mostrar_ventana;
			this.variable = obj.variable;
			this.formula = obj.formula;
			this.rpn_formula = obj.rpn_formula;
			this.regla = obj.regla;
			this.rpn = obj.rpn;
			this.mensaje = obj.mensaje;
			this.revision = obj.revision;
			this.apiestado = obj.apiestado;
			this.usucre = obj.usucre;
			this.feccre = obj.feccre;
			this.usumod = obj.usumod;
			this.fecmod = obj.fecmod;
			this.instruccion = obj.instruccion;
			this.bucle = obj.bucle;
			this.variable_bucle = obj.variable_bucle;
			this.codigo_especial = obj.codigo_especial;
		}
		
		#endregion
		
		/// <summary>
		/// 	 Variable local de tipo Int64 que representa a la columna id_pregunta de la Tabla enc_pregunta
		/// </summary>
		private Int64 _id_pregunta;
		/// <summary>
		/// 	 Propiedad publica de tipo Int64 que representa a la columna id_pregunta de la Tabla enc_pregunta
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: Yes
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_pregunta", Description = " Propiedad publica de tipo Int64 que representa a la columna id_pregunta de la Tabla enc_pregunta")]
		[Required(ErrorMessage = "id_pregunta es un campo requerido.")]
		[Key]
		public Int64 id_pregunta
		{
			get {return _id_pregunta;}
			set
			{
				if (_id_pregunta != value)
				{
					RaisePropertyChanging(entEncPregunta.Fields.id_pregunta.ToString());
					_id_pregunta = value;
					RaisePropertyChanged(entEncPregunta.Fields.id_pregunta.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_proyecto de la Tabla enc_pregunta
		/// </summary>
		private int _id_proyecto;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna id_proyecto de la Tabla enc_pregunta
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_proyecto", Description = " Propiedad publica de tipo int que representa a la columna id_proyecto de la Tabla enc_pregunta")]
		[Required(ErrorMessage = "id_proyecto es un campo requerido.")]
		public int id_proyecto
		{
			get {return _id_proyecto;}
			set
			{
				if (_id_proyecto != value)
				{
					RaisePropertyChanging(entEncPregunta.Fields.id_proyecto.ToString());
					_id_proyecto = value;
					RaisePropertyChanged(entEncPregunta.Fields.id_proyecto.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_nivel de la Tabla enc_pregunta
		/// </summary>
		private int _id_nivel;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna id_nivel de la Tabla enc_pregunta
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: Yes
		/// </summary>
		[Display(Name = "id_nivel", Description = " Propiedad publica de tipo int que representa a la columna id_nivel de la Tabla enc_pregunta")]
		[Required(ErrorMessage = "id_nivel es un campo requerido.")]
		public int id_nivel
		{
			get {return _id_nivel;}
			set
			{
				if (_id_nivel != value)
				{
					RaisePropertyChanging(entEncPregunta.Fields.id_nivel.ToString());
					_id_nivel = value;
					RaisePropertyChanged(entEncPregunta.Fields.id_nivel.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_seccion de la Tabla enc_pregunta
		/// </summary>
		private int? _id_seccion;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna id_seccion de la Tabla enc_pregunta
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: Yes
		/// </summary>
		[Display(Name = "id_seccion", Description = " Propiedad publica de tipo int que representa a la columna id_seccion de la Tabla enc_pregunta")]
		public int? id_seccion
		{
			get {return _id_seccion;}
			set
			{
				if (_id_seccion != value)
				{
					RaisePropertyChanging(entEncPregunta.Fields.id_seccion.ToString());
					_id_seccion = value;
					RaisePropertyChanged(entEncPregunta.Fields.id_seccion.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna codigo_pregunta de la Tabla enc_pregunta
		/// </summary>
		private String _codigo_pregunta;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna codigo_pregunta de la Tabla enc_pregunta
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "codigo_pregunta", Description = " Propiedad publica de tipo String que representa a la columna codigo_pregunta de la Tabla enc_pregunta")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "codigo_pregunta es un campo requerido.")]
		public String codigo_pregunta
		{
			get {return _codigo_pregunta;}
			set
			{
				if (_codigo_pregunta != value)
				{
					RaisePropertyChanging(entEncPregunta.Fields.codigo_pregunta.ToString());
					_codigo_pregunta = value;
					RaisePropertyChanged(entEncPregunta.Fields.codigo_pregunta.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna pregunta de la Tabla enc_pregunta
		/// </summary>
		private String _pregunta;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna pregunta de la Tabla enc_pregunta
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "pregunta", Description = " Propiedad publica de tipo String que representa a la columna pregunta de la Tabla enc_pregunta")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "pregunta es un campo requerido.")]
		public String pregunta
		{
			get {return _pregunta;}
			set
			{
				if (_pregunta != value)
				{
					RaisePropertyChanging(entEncPregunta.Fields.pregunta.ToString());
					_pregunta = value;
					RaisePropertyChanged(entEncPregunta.Fields.pregunta.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna ayuda de la Tabla enc_pregunta
		/// </summary>
		private String _ayuda;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna ayuda de la Tabla enc_pregunta
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "ayuda", Description = " Propiedad publica de tipo String que representa a la columna ayuda de la Tabla enc_pregunta")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "ayuda es un campo requerido.")]
		public String ayuda
		{
			get {return _ayuda;}
			set
			{
				if (_ayuda != value)
				{
					RaisePropertyChanging(entEncPregunta.Fields.ayuda.ToString());
					_ayuda = value;
					RaisePropertyChanged(entEncPregunta.Fields.ayuda.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_tipo_pregunta de la Tabla enc_pregunta
		/// </summary>
		private int _id_tipo_pregunta;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna id_tipo_pregunta de la Tabla enc_pregunta
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: Yes
		/// </summary>
		[Display(Name = "id_tipo_pregunta", Description = " Propiedad publica de tipo int que representa a la columna id_tipo_pregunta de la Tabla enc_pregunta")]
		[Required(ErrorMessage = "id_tipo_pregunta es un campo requerido.")]
		public int id_tipo_pregunta
		{
			get {return _id_tipo_pregunta;}
			set
			{
				if (_id_tipo_pregunta != value)
				{
					RaisePropertyChanging(entEncPregunta.Fields.id_tipo_pregunta.ToString());
					_id_tipo_pregunta = value;
					RaisePropertyChanged(entEncPregunta.Fields.id_tipo_pregunta.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna minimo de la Tabla enc_pregunta
		/// </summary>
		private int _minimo;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna minimo de la Tabla enc_pregunta
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "minimo", Description = " Propiedad publica de tipo int que representa a la columna minimo de la Tabla enc_pregunta")]
		[Required(ErrorMessage = "minimo es un campo requerido.")]
		public int minimo
		{
			get {return _minimo;}
			set
			{
				if (_minimo != value)
				{
					RaisePropertyChanging(entEncPregunta.Fields.minimo.ToString());
					_minimo = value;
					RaisePropertyChanged(entEncPregunta.Fields.minimo.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna maximo de la Tabla enc_pregunta
		/// </summary>
		private int _maximo;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna maximo de la Tabla enc_pregunta
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "maximo", Description = " Propiedad publica de tipo int que representa a la columna maximo de la Tabla enc_pregunta")]
		[Required(ErrorMessage = "maximo es un campo requerido.")]
		public int maximo
		{
			get {return _maximo;}
			set
			{
				if (_maximo != value)
				{
					RaisePropertyChanging(entEncPregunta.Fields.maximo.ToString());
					_maximo = value;
					RaisePropertyChanged(entEncPregunta.Fields.maximo.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna catalogo de la Tabla enc_pregunta
		/// </summary>
		private String _catalogo;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna catalogo de la Tabla enc_pregunta
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "catalogo", Description = " Propiedad publica de tipo String que representa a la columna catalogo de la Tabla enc_pregunta")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "catalogo es un campo requerido.")]
		public String catalogo
		{
			get {return _catalogo;}
			set
			{
				if (_catalogo != value)
				{
					RaisePropertyChanging(entEncPregunta.Fields.catalogo.ToString());
					_catalogo = value;
					RaisePropertyChanged(entEncPregunta.Fields.catalogo.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna longitud de la Tabla enc_pregunta
		/// </summary>
		private int? _longitud;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna longitud de la Tabla enc_pregunta
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "longitud", Description = " Propiedad publica de tipo int que representa a la columna longitud de la Tabla enc_pregunta")]
		public int? longitud
		{
			get {return _longitud;}
			set
			{
				if (_longitud != value)
				{
					RaisePropertyChanging(entEncPregunta.Fields.longitud.ToString());
					_longitud = value;
					RaisePropertyChanged(entEncPregunta.Fields.longitud.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna codigo_especifique de la Tabla enc_pregunta
		/// </summary>
		private String _codigo_especifique;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna codigo_especifique de la Tabla enc_pregunta
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(2, MinimumLength=0)]
		[Display(Name = "codigo_especifique", Description = " Propiedad publica de tipo String que representa a la columna codigo_especifique de la Tabla enc_pregunta")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "codigo_especifique es un campo requerido.")]
		public String codigo_especifique
		{
			get {return _codigo_especifique;}
			set
			{
				if (_codigo_especifique != value)
				{
					RaisePropertyChanging(entEncPregunta.Fields.codigo_especifique.ToString());
					_codigo_especifique = value;
					RaisePropertyChanged(entEncPregunta.Fields.codigo_especifique.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna mostrar_ventana de la Tabla enc_pregunta
		/// </summary>
		private int _mostrar_ventana;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna mostrar_ventana de la Tabla enc_pregunta
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "mostrar_ventana", Description = " Propiedad publica de tipo int que representa a la columna mostrar_ventana de la Tabla enc_pregunta")]
		[Required(ErrorMessage = "mostrar_ventana es un campo requerido.")]
		public int mostrar_ventana
		{
			get {return _mostrar_ventana;}
			set
			{
				if (_mostrar_ventana != value)
				{
					RaisePropertyChanging(entEncPregunta.Fields.mostrar_ventana.ToString());
					_mostrar_ventana = value;
					RaisePropertyChanged(entEncPregunta.Fields.mostrar_ventana.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna variable de la Tabla enc_pregunta
		/// </summary>
		private String _variable;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna variable de la Tabla enc_pregunta
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "variable", Description = " Propiedad publica de tipo String que representa a la columna variable de la Tabla enc_pregunta")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "variable es un campo requerido.")]
		public String variable
		{
			get {return _variable;}
			set
			{
				if (_variable != value)
				{
					RaisePropertyChanging(entEncPregunta.Fields.variable.ToString());
					_variable = value;
					RaisePropertyChanged(entEncPregunta.Fields.variable.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna formula de la Tabla enc_pregunta
		/// </summary>
		private String _formula;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna formula de la Tabla enc_pregunta
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "formula", Description = " Propiedad publica de tipo String que representa a la columna formula de la Tabla enc_pregunta")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "formula es un campo requerido.")]
		public String formula
		{
			get {return _formula;}
			set
			{
				if (_formula != value)
				{
					RaisePropertyChanging(entEncPregunta.Fields.formula.ToString());
					_formula = value;
					RaisePropertyChanged(entEncPregunta.Fields.formula.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna rpn_formula de la Tabla enc_pregunta
		/// </summary>
		private String _rpn_formula;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna rpn_formula de la Tabla enc_pregunta
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "rpn_formula", Description = " Propiedad publica de tipo String que representa a la columna rpn_formula de la Tabla enc_pregunta")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "rpn_formula es un campo requerido.")]
		public String rpn_formula
		{
			get {return _rpn_formula;}
			set
			{
				if (_rpn_formula != value)
				{
					RaisePropertyChanging(entEncPregunta.Fields.rpn_formula.ToString());
					_rpn_formula = value;
					RaisePropertyChanged(entEncPregunta.Fields.rpn_formula.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna regla de la Tabla enc_pregunta
		/// </summary>
		private String _regla;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna regla de la Tabla enc_pregunta
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "regla", Description = " Propiedad publica de tipo String que representa a la columna regla de la Tabla enc_pregunta")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "regla es un campo requerido.")]
		public String regla
		{
			get {return _regla;}
			set
			{
				if (_regla != value)
				{
					RaisePropertyChanging(entEncPregunta.Fields.regla.ToString());
					_regla = value;
					RaisePropertyChanged(entEncPregunta.Fields.regla.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna rpn de la Tabla enc_pregunta
		/// </summary>
		private String _rpn;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna rpn de la Tabla enc_pregunta
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "rpn", Description = " Propiedad publica de tipo String que representa a la columna rpn de la Tabla enc_pregunta")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "rpn es un campo requerido.")]
		public String rpn
		{
			get {return _rpn;}
			set
			{
				if (_rpn != value)
				{
					RaisePropertyChanging(entEncPregunta.Fields.rpn.ToString());
					_rpn = value;
					RaisePropertyChanged(entEncPregunta.Fields.rpn.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna mensaje de la Tabla enc_pregunta
		/// </summary>
		private String _mensaje;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna mensaje de la Tabla enc_pregunta
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "mensaje", Description = " Propiedad publica de tipo String que representa a la columna mensaje de la Tabla enc_pregunta")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "mensaje es un campo requerido.")]
		public String mensaje
		{
			get {return _mensaje;}
			set
			{
				if (_mensaje != value)
				{
					RaisePropertyChanging(entEncPregunta.Fields.mensaje.ToString());
					_mensaje = value;
					RaisePropertyChanged(entEncPregunta.Fields.mensaje.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna revision de la Tabla enc_pregunta
		/// </summary>
		private String _revision;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna revision de la Tabla enc_pregunta
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "revision", Description = " Propiedad publica de tipo String que representa a la columna revision de la Tabla enc_pregunta")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "revision es un campo requerido.")]
		public String revision
		{
			get {return _revision;}
			set
			{
				if (_revision != value)
				{
					RaisePropertyChanging(entEncPregunta.Fields.revision.ToString());
					_revision = value;
					RaisePropertyChanged(entEncPregunta.Fields.revision.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna apiestado de la Tabla enc_pregunta
		/// </summary>
		private String _apiestado;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna apiestado de la Tabla enc_pregunta
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "apiestado", Description = " Propiedad publica de tipo String que representa a la columna apiestado de la Tabla enc_pregunta")]
		[EnumDataType(typeof(cApi.Estado))]
		public String apiestado
		{
			get {return _apiestado;}
			set
			{
				if (_apiestado != value)
				{
					RaisePropertyChanging(entEncPregunta.Fields.apiestado.ToString());
					_apiestado = value;
					RaisePropertyChanged(entEncPregunta.Fields.apiestado.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usucre de la Tabla enc_pregunta
		/// </summary>
		private String _usucre;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna usucre de la Tabla enc_pregunta
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usucre", Description = " Propiedad publica de tipo String que representa a la columna usucre de la Tabla enc_pregunta")]
		public String usucre
		{
			get {return _usucre;}
			set
			{
				if (_usucre != value)
				{
					RaisePropertyChanging(entEncPregunta.Fields.usucre.ToString());
					_usucre = value;
					RaisePropertyChanged(entEncPregunta.Fields.usucre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna feccre de la Tabla enc_pregunta
		/// </summary>
		private DateTime _feccre;
		/// <summary>
		/// 	 Propiedad publica de tipo DateTime que representa a la columna feccre de la Tabla enc_pregunta
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "feccre", Description = " Propiedad publica de tipo DateTime que representa a la columna feccre de la Tabla enc_pregunta")]
		public DateTime feccre
		{
			get {return _feccre;}
			set
			{
				if (_feccre != value)
				{
					RaisePropertyChanging(entEncPregunta.Fields.feccre.ToString());
					_feccre = value;
					RaisePropertyChanged(entEncPregunta.Fields.feccre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usumod de la Tabla enc_pregunta
		/// </summary>
		private String _usumod;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna usumod de la Tabla enc_pregunta
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usumod", Description = " Propiedad publica de tipo String que representa a la columna usumod de la Tabla enc_pregunta")]
		public String usumod
		{
			get {return _usumod;}
			set
			{
				if (_usumod != value)
				{
					RaisePropertyChanging(entEncPregunta.Fields.usumod.ToString());
					_usumod = value;
					RaisePropertyChanged(entEncPregunta.Fields.usumod.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna fecmod de la Tabla enc_pregunta
		/// </summary>
		private DateTime? _fecmod;
		/// <summary>
		/// 	 Propiedad publica de tipo DateTime que representa a la columna fecmod de la Tabla enc_pregunta
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "fecmod", Description = " Propiedad publica de tipo DateTime que representa a la columna fecmod de la Tabla enc_pregunta")]
		public DateTime? fecmod
		{
			get {return _fecmod;}
			set
			{
				if (_fecmod != value)
				{
					RaisePropertyChanging(entEncPregunta.Fields.fecmod.ToString());
					_fecmod = value;
					RaisePropertyChanged(entEncPregunta.Fields.fecmod.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna instruccion de la Tabla enc_pregunta
		/// </summary>
		private String _instruccion;
		/// <summary>
		/// 	 En caso de ser necesario una instruccion en PopUp
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "instruccion", Description = "En caso de ser necesario una instruccion en PopUp")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "instruccion es un campo requerido.")]
		public String instruccion
		{
			get {return _instruccion;}
			set
			{
				if (_instruccion != value)
				{
					RaisePropertyChanging(entEncPregunta.Fields.instruccion.ToString());
					_instruccion = value;
					RaisePropertyChanged(entEncPregunta.Fields.instruccion.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna bucle de la Tabla enc_pregunta
		/// </summary>
		private int _bucle;
		/// <summary>
		/// 	 Indica si esta pregunta es un FIN DE BUCLE (1) o No (0)
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "bucle", Description = "Indica si esta pregunta es un FIN DE BUCLE (1) o No (0)")]
		[Required(ErrorMessage = "bucle es un campo requerido.")]
		public int bucle
		{
			get {return _bucle;}
			set
			{
				if (_bucle != value)
				{
					RaisePropertyChanging(entEncPregunta.Fields.bucle.ToString());
					_bucle = value;
					RaisePropertyChanged(entEncPregunta.Fields.bucle.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna variable_bucle de la Tabla enc_pregunta
		/// </summary>
		private String _variable_bucle;
		/// <summary>
		/// 	 Necesario para las preguntas del Tipo BUCLE, en cuyo caso, se usara este campo para indicar las posibles respuestas
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(150, MinimumLength=0)]
		[Display(Name = "variable_bucle", Description = "Necesario para las preguntas del Tipo BUCLE, en cuyo caso, se usara este campo para indicar las posibles respuestas")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "variable_bucle es un campo requerido.")]
		public String variable_bucle
		{
			get {return _variable_bucle;}
			set
			{
				if (_variable_bucle != value)
				{
					RaisePropertyChanging(entEncPregunta.Fields.variable_bucle.ToString());
					_variable_bucle = value;
					RaisePropertyChanged(entEncPregunta.Fields.variable_bucle.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna codigo_especial de la Tabla enc_pregunta
		/// </summary>
		private int _codigo_especial;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna codigo_especial de la Tabla enc_pregunta
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "codigo_especial", Description = " Propiedad publica de tipo int que representa a la columna codigo_especial de la Tabla enc_pregunta")]
		[Required(ErrorMessage = "codigo_especial es un campo requerido.")]
		public int codigo_especial
		{
			get {return _codigo_especial;}
			set
			{
				if (_codigo_especial != value)
				{
					RaisePropertyChanging(entEncPregunta.Fields.codigo_especial.ToString());
					_codigo_especial = value;
					RaisePropertyChanged(entEncPregunta.Fields.codigo_especial.ToString());
				}
			}
		}


	}
}

