#region 
/***********************************************************************************************************
	NOMBRE:       entEncFlujo
	DESCRIPCION:
		Clase que define un objeto para la Tabla enc_flujo

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.Opinion.Dal.Entidades
{
	public class entEncFlujo : cBaseClass
	{
		public const String strNombreTabla = "enc_flujo";
		public const String strAliasTabla = "enc_flujo";
		public enum Fields
		{
			id_flujo
			,id_proyecto
			,id_pregunta
			,id_pregunta_destino
			,orden
			,regla
			,rpn
			,apiestado
			,usucre
			,feccre
			,usumod
			,fecmod

		}
		
		#region Constructoress
		
		public entEncFlujo()
		{
			//Inicializacion de Variables
			this.id_proyecto = null;
			this.regla = null;
			this.rpn = null;
			this.apiestado = null;
			this.usucre = null;
			this.usumod = null;
			this.fecmod = null;
		}
		
		public entEncFlujo(entEncFlujo obj)
		{
			this.id_flujo = obj.id_flujo;
			this.id_proyecto = obj.id_proyecto;
			this.id_pregunta = obj.id_pregunta;
			this.id_pregunta_destino = obj.id_pregunta_destino;
			this.orden = obj.orden;
			this.regla = obj.regla;
			this.rpn = obj.rpn;
			this.apiestado = obj.apiestado;
			this.usucre = obj.usucre;
			this.feccre = obj.feccre;
			this.usumod = obj.usumod;
			this.fecmod = obj.fecmod;
		}
		
		#endregion
		
		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_flujo de la Tabla enc_flujo
		/// </summary>
		private int _id_flujo;
		/// <summary>
		/// 	 Identificador unico que representa al registro en la tabla
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: Yes
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_flujo", Description = "Identificador unico que representa al registro en la tabla")]
		[Required(ErrorMessage = "id_flujo es un campo requerido.")]
		[Key]
		public int id_flujo
		{
			get {return _id_flujo;}
			set
			{
				if (_id_flujo != value)
				{
					RaisePropertyChanging(entEncFlujo.Fields.id_flujo.ToString());
					_id_flujo = value;
					RaisePropertyChanged(entEncFlujo.Fields.id_flujo.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_proyecto de la Tabla enc_flujo
		/// </summary>
		private int? _id_proyecto;
		/// <summary>
		/// 	 Identificador del proyecto al que pertenece el registro
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: Yes
		/// </summary>
		[Display(Name = "id_proyecto", Description = "Identificador del proyecto al que pertenece el registro")]
		public int? id_proyecto
		{
			get {return _id_proyecto;}
			set
			{
				if (_id_proyecto != value)
				{
					RaisePropertyChanging(entEncFlujo.Fields.id_proyecto.ToString());
					_id_proyecto = value;
					RaisePropertyChanged(entEncFlujo.Fields.id_proyecto.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_pregunta de la Tabla enc_flujo
		/// </summary>
		private int _id_pregunta;
		/// <summary>
		/// 	 Almacena el Id de pregunta origen
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: Yes
		/// </summary>
		[Display(Name = "id_pregunta", Description = "Almacena el Id de pregunta origen")]
		[Required(ErrorMessage = "id_pregunta es un campo requerido.")]
		public int id_pregunta
		{
			get {return _id_pregunta;}
			set
			{
				if (_id_pregunta != value)
				{
					RaisePropertyChanging(entEncFlujo.Fields.id_pregunta.ToString());
					_id_pregunta = value;
					RaisePropertyChanged(entEncFlujo.Fields.id_pregunta.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_pregunta_destino de la Tabla enc_flujo
		/// </summary>
		private int _id_pregunta_destino;
		/// <summary>
		/// 	 Almacena el Id de pregunta destino. -1 representa FIN DE NIVEL. -2 representa FIN DE ENTREVISTA
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_pregunta_destino", Description = "Almacena el Id de pregunta destino. -1 representa FIN DE NIVEL. -2 representa FIN DE ENTREVISTA")]
		[Required(ErrorMessage = "id_pregunta_destino es un campo requerido.")]
		public int id_pregunta_destino
		{
			get {return _id_pregunta_destino;}
			set
			{
				if (_id_pregunta_destino != value)
				{
					RaisePropertyChanging(entEncFlujo.Fields.id_pregunta_destino.ToString());
					_id_pregunta_destino = value;
					RaisePropertyChanged(entEncFlujo.Fields.id_pregunta_destino.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna orden de la Tabla enc_flujo
		/// </summary>
		private int _orden;
		/// <summary>
		/// 	 Es el orden de PRIORIDAD en el que va ha evuarse el flujo
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "orden", Description = "Es el orden de PRIORIDAD en el que va ha evuarse el flujo")]
		[Required(ErrorMessage = "orden es un campo requerido.")]
		public int orden
		{
			get {return _orden;}
			set
			{
				if (_orden != value)
				{
					RaisePropertyChanging(entEncFlujo.Fields.orden.ToString());
					_orden = value;
					RaisePropertyChanged(entEncFlujo.Fields.orden.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna regla de la Tabla enc_flujo
		/// </summary>
		private String _regla;
		/// <summary>
		/// 	 Es la regla de evaluaciÃ³n para consistencia aplicado a la pregunta
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "regla", Description = "Es la regla de evaluaciÃ³n para consistencia aplicado a la pregunta")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "regla es un campo requerido.")]
		public String regla
		{
			get {return _regla;}
			set
			{
				if (_regla != value)
				{
					RaisePropertyChanging(entEncFlujo.Fields.regla.ToString());
					_regla = value;
					RaisePropertyChanged(entEncFlujo.Fields.regla.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna rpn de la Tabla enc_flujo
		/// </summary>
		private String _rpn;
		/// <summary>
		/// 	 Por sus siglas en ingles (Reverse Polish Notation), es la notacion polaca inversa que se genera automaticamente a partir del campo regla
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "rpn", Description = "Por sus siglas en ingles (Reverse Polish Notation), es la notacion polaca inversa que se genera automaticamente a partir del campo regla")]
		public String rpn
		{
			get {return _rpn;}
			set
			{
				if (_rpn != value)
				{
					RaisePropertyChanging(entEncFlujo.Fields.rpn.ToString());
					_rpn = value;
					RaisePropertyChanged(entEncFlujo.Fields.rpn.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna apiestado de la Tabla enc_flujo
		/// </summary>
		private String _apiestado;
		/// <summary>
		/// 	 Estado en el que se encuentra el registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "apiestado", Description = "Estado en el que se encuentra el registro")]
		[EnumDataType(typeof(cApi.Estado))]
		public String apiestado
		{
			get {return _apiestado;}
			set
			{
				if (_apiestado != value)
				{
					RaisePropertyChanging(entEncFlujo.Fields.apiestado.ToString());
					_apiestado = value;
					RaisePropertyChanged(entEncFlujo.Fields.apiestado.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usucre de la Tabla enc_flujo
		/// </summary>
		private String _usucre;
		/// <summary>
		/// 	 Login o nombre de usuario que ha creado el registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usucre", Description = "Login o nombre de usuario que ha creado el registro")]
		public String usucre
		{
			get {return _usucre;}
			set
			{
				if (_usucre != value)
				{
					RaisePropertyChanging(entEncFlujo.Fields.usucre.ToString());
					_usucre = value;
					RaisePropertyChanged(entEncFlujo.Fields.usucre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna feccre de la Tabla enc_flujo
		/// </summary>
		private DateTime _feccre;
		/// <summary>
		/// 	 Fecha de creacion del registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "feccre", Description = "Fecha de creacion del registro")]
		public DateTime feccre
		{
			get {return _feccre;}
			set
			{
				if (_feccre != value)
				{
					RaisePropertyChanging(entEncFlujo.Fields.feccre.ToString());
					_feccre = value;
					RaisePropertyChanged(entEncFlujo.Fields.feccre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usumod de la Tabla enc_flujo
		/// </summary>
		private String _usumod;
		/// <summary>
		/// 	 Login o nombre de usuario que ha realizado la ULTIMA modificacion registro
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usumod", Description = "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro")]
		public String usumod
		{
			get {return _usumod;}
			set
			{
				if (_usumod != value)
				{
					RaisePropertyChanging(entEncFlujo.Fields.usumod.ToString());
					_usumod = value;
					RaisePropertyChanged(entEncFlujo.Fields.usumod.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna fecmod de la Tabla enc_flujo
		/// </summary>
		private DateTime? _fecmod;
		/// <summary>
		/// 	 Fecha en la que se ha realizado la ULTIMA modificacion registro
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "fecmod", Description = "Fecha en la que se ha realizado la ULTIMA modificacion registro")]
		public DateTime? fecmod
		{
			get {return _fecmod;}
			set
			{
				if (_fecmod != value)
				{
					RaisePropertyChanging(entEncFlujo.Fields.fecmod.ToString());
					_fecmod = value;
					RaisePropertyChanged(entEncFlujo.Fields.fecmod.ToString());
				}
			}
		}


	}
}

