#region 
/***********************************************************************************************************
	NOMBRE:       entSegUsuariorestriccion
	DESCRIPCION:
		Clase que define un objeto para la Tabla seg_usuariorestriccion

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.Opinion.Dal.Entidades
{
	public class entSegUsuariorestriccion : cBaseClass
	{
		public const String strNombreTabla = "seg_usuariorestriccion";
		public const String strAliasTabla = "seg_usuariorestriccion";
		public enum Fields
		{
			id_usurestriccion
			,id_proyecto
			,id_usuario
			,id_rol
			,departamentos
			,fechavigente
			,apiestado
			,usucre
			,feccre
			,usumod
			,fecmod

		}
		
		#region Constructoress
		
		public entSegUsuariorestriccion()
		{
			//Inicializacion de Variables
			this.departamentos = null;
			this.fechavigente = null;
			this.apiestado = null;
			this.usucre = null;
			this.usumod = null;
			this.fecmod = null;
		}
		
		public entSegUsuariorestriccion(entSegUsuariorestriccion obj)
		{
			this.id_usurestriccion = obj.id_usurestriccion;
			this.id_proyecto = obj.id_proyecto;
			this.id_usuario = obj.id_usuario;
			this.id_rol = obj.id_rol;
			this.departamentos = obj.departamentos;
			this.fechavigente = obj.fechavigente;
			this.apiestado = obj.apiestado;
			this.usucre = obj.usucre;
			this.feccre = obj.feccre;
			this.usumod = obj.usumod;
			this.fecmod = obj.fecmod;
		}
		
		#endregion
		
		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_usurestriccion de la Tabla seg_usuariorestriccion
		/// </summary>
		private int _id_usurestriccion;
		/// <summary>
		/// 	 Identificador unico de la tabla
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: Yes
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_usurestriccion", Description = "Identificador unico de la tabla")]
		[Required(ErrorMessage = "id_usurestriccion es un campo requerido.")]
		[Key]
		public int id_usurestriccion
		{
			get {return _id_usurestriccion;}
			set
			{
				if (_id_usurestriccion != value)
				{
					RaisePropertyChanging(entSegUsuariorestriccion.Fields.id_usurestriccion.ToString());
					_id_usurestriccion = value;
					RaisePropertyChanged(entSegUsuariorestriccion.Fields.id_usurestriccion.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_proyecto de la Tabla seg_usuariorestriccion
		/// </summary>
		private int _id_proyecto;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna id_proyecto de la Tabla seg_usuariorestriccion
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: Yes
		/// </summary>
		[Display(Name = "id_proyecto", Description = " Propiedad publica de tipo int que representa a la columna id_proyecto de la Tabla seg_usuariorestriccion")]
		[Required(ErrorMessage = "id_proyecto es un campo requerido.")]
		public int id_proyecto
		{
			get {return _id_proyecto;}
			set
			{
				if (_id_proyecto != value)
				{
					RaisePropertyChanging(entSegUsuariorestriccion.Fields.id_proyecto.ToString());
					_id_proyecto = value;
					RaisePropertyChanged(entSegUsuariorestriccion.Fields.id_proyecto.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_usuario de la Tabla seg_usuariorestriccion
		/// </summary>
		private int _id_usuario;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna id_usuario de la Tabla seg_usuariorestriccion
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: Yes
		/// </summary>
		[Display(Name = "id_usuario", Description = " Propiedad publica de tipo int que representa a la columna id_usuario de la Tabla seg_usuariorestriccion")]
		[Required(ErrorMessage = "id_usuario es un campo requerido.")]
		public int id_usuario
		{
			get {return _id_usuario;}
			set
			{
				if (_id_usuario != value)
				{
					RaisePropertyChanging(entSegUsuariorestriccion.Fields.id_usuario.ToString());
					_id_usuario = value;
					RaisePropertyChanged(entSegUsuariorestriccion.Fields.id_usuario.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_rol de la Tabla seg_usuariorestriccion
		/// </summary>
		private int _id_rol;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna id_rol de la Tabla seg_usuariorestriccion
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: Yes
		/// </summary>
		[Display(Name = "id_rol", Description = " Propiedad publica de tipo int que representa a la columna id_rol de la Tabla seg_usuariorestriccion")]
		[Required(ErrorMessage = "id_rol es un campo requerido.")]
		public int id_rol
		{
			get {return _id_rol;}
			set
			{
				if (_id_rol != value)
				{
					RaisePropertyChanging(entSegUsuariorestriccion.Fields.id_rol.ToString());
					_id_rol = value;
					RaisePropertyChanged(entSegUsuariorestriccion.Fields.id_rol.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna departamentos de la Tabla seg_usuariorestriccion
		/// </summary>
		private String _departamentos;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna departamentos de la Tabla seg_usuariorestriccion
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "departamentos", Description = " Propiedad publica de tipo String que representa a la columna departamentos de la Tabla seg_usuariorestriccion")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "departamentos es un campo requerido.")]
		public String departamentos
		{
			get {return _departamentos;}
			set
			{
				if (_departamentos != value)
				{
					RaisePropertyChanging(entSegUsuariorestriccion.Fields.departamentos.ToString());
					_departamentos = value;
					RaisePropertyChanged(entSegUsuariorestriccion.Fields.departamentos.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna fechavigente de la Tabla seg_usuariorestriccion
		/// </summary>
		private DateTime? _fechavigente;
		/// <summary>
		/// 	 Propiedad publica de tipo DateTime que representa a la columna fechavigente de la Tabla seg_usuariorestriccion
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "fechavigente", Description = " Propiedad publica de tipo DateTime que representa a la columna fechavigente de la Tabla seg_usuariorestriccion")]
		public DateTime? fechavigente
		{
			get {return _fechavigente;}
			set
			{
				if (_fechavigente != value)
				{
					RaisePropertyChanging(entSegUsuariorestriccion.Fields.fechavigente.ToString());
					_fechavigente = value;
					RaisePropertyChanged(entSegUsuariorestriccion.Fields.fechavigente.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna apiestado de la Tabla seg_usuariorestriccion
		/// </summary>
		private String _apiestado;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna apiestado de la Tabla seg_usuariorestriccion
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "apiestado", Description = " Propiedad publica de tipo String que representa a la columna apiestado de la Tabla seg_usuariorestriccion")]
		[EnumDataType(typeof(cApi.Estado))]
		public String apiestado
		{
			get {return _apiestado;}
			set
			{
				if (_apiestado != value)
				{
					RaisePropertyChanging(entSegUsuariorestriccion.Fields.apiestado.ToString());
					_apiestado = value;
					RaisePropertyChanged(entSegUsuariorestriccion.Fields.apiestado.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usucre de la Tabla seg_usuariorestriccion
		/// </summary>
		private String _usucre;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna usucre de la Tabla seg_usuariorestriccion
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "usucre", Description = " Propiedad publica de tipo String que representa a la columna usucre de la Tabla seg_usuariorestriccion")]
		public String usucre
		{
			get {return _usucre;}
			set
			{
				if (_usucre != value)
				{
					RaisePropertyChanging(entSegUsuariorestriccion.Fields.usucre.ToString());
					_usucre = value;
					RaisePropertyChanged(entSegUsuariorestriccion.Fields.usucre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna feccre de la Tabla seg_usuariorestriccion
		/// </summary>
		private DateTime _feccre;
		/// <summary>
		/// 	 Propiedad publica de tipo DateTime que representa a la columna feccre de la Tabla seg_usuariorestriccion
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "feccre", Description = " Propiedad publica de tipo DateTime que representa a la columna feccre de la Tabla seg_usuariorestriccion")]
		public DateTime feccre
		{
			get {return _feccre;}
			set
			{
				if (_feccre != value)
				{
					RaisePropertyChanging(entSegUsuariorestriccion.Fields.feccre.ToString());
					_feccre = value;
					RaisePropertyChanged(entSegUsuariorestriccion.Fields.feccre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usumod de la Tabla seg_usuariorestriccion
		/// </summary>
		private String _usumod;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna usumod de la Tabla seg_usuariorestriccion
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "usumod", Description = " Propiedad publica de tipo String que representa a la columna usumod de la Tabla seg_usuariorestriccion")]
		public String usumod
		{
			get {return _usumod;}
			set
			{
				if (_usumod != value)
				{
					RaisePropertyChanging(entSegUsuariorestriccion.Fields.usumod.ToString());
					_usumod = value;
					RaisePropertyChanged(entSegUsuariorestriccion.Fields.usumod.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna fecmod de la Tabla seg_usuariorestriccion
		/// </summary>
		private DateTime? _fecmod;
		/// <summary>
		/// 	 Propiedad publica de tipo DateTime que representa a la columna fecmod de la Tabla seg_usuariorestriccion
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "fecmod", Description = " Propiedad publica de tipo DateTime que representa a la columna fecmod de la Tabla seg_usuariorestriccion")]
		public DateTime? fecmod
		{
			get {return _fecmod;}
			set
			{
				if (_fecmod != value)
				{
					RaisePropertyChanging(entSegUsuariorestriccion.Fields.fecmod.ToString());
					_fecmod = value;
					RaisePropertyChanged(entSegUsuariorestriccion.Fields.fecmod.ToString());
				}
			}
		}


	}
}

