#region 
/***********************************************************************************************************
	NOMBRE:       entRptInsumos
	DESCRIPCION:
		Clase que define un objeto para la Tabla rpt_insumos

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        22/10/2014  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.Opinion.Dal.Entidades
{
	public class entRptInsumos
	{
		public const String strNombreTabla = "rpt_insumos";
		public const String strAliasTabla = "rpt_insumos";
		public enum Fields
		{
			gestion
			,mes
			,foto
			,id_departamento
			,id_upmipp
			,id_provincia
			,id_municipio
			,id_listado
			,id_boleta
			,id_persona
			,id_movimiento
			,control
			,minimo
			,maximo
			,departamento
			,provincia
			,municipio
			,comunidad
			,login
			,producto_boleta
			,codigo_producto
			,codigo_ccp
			,informante
			,telefono
			,direccion
			,producto
			,caracteristicas
			,variedad
			,origen
			,cantidad
			,unidad
			,precio
			,superficie
            ,unidad_superficie
			,observaciones
			,minfec
			,maxfec
			,duracion

		}
		
		#region Constructoress
		
		public entRptInsumos()
		{
			//Inicializacion de Variables
		}
		
		public entRptInsumos(entRptInsumos obj)
		{
			this.gestion = obj.gestion;
			this.mes = obj.mes;
			this.foto = obj.foto;
			this.id_departamento = obj.id_departamento;
			this.id_upmipp = obj.id_upmipp;
			this.id_provincia = obj.id_provincia;
			this.id_municipio = obj.id_municipio;
			this.id_listado = obj.id_listado;
			this.id_boleta = obj.id_boleta;
			this.id_persona = obj.id_persona;
			this.id_movimiento = obj.id_movimiento;
			this.control = obj.control;
			this.minimo = obj.minimo;
			this.maximo = obj.maximo;
			this.departamento = obj.departamento;
			this.provincia = obj.provincia;
			this.municipio = obj.municipio;
			this.comunidad = obj.comunidad;
			this.login = obj.login;
			this.producto_boleta = obj.producto_boleta;
			this.codigo_producto = obj.codigo_producto;
			this.codigo_ccp = obj.codigo_ccp;
			this.informante = obj.informante;
			this.telefono = obj.telefono;
			this.direccion = obj.direccion;
			this.producto = obj.producto;
			this.caracteristicas = obj.caracteristicas;
			this.variedad = obj.variedad;
			this.origen = obj.origen;
			this.cantidad = obj.cantidad;
			this.unidad = obj.unidad;
			this.precio = obj.precio;
			this.superficie = obj.superficie;
			this.observaciones = obj.observaciones;
			this.minfec = obj.minfec;
			this.maxfec = obj.maxfec;
			this.duracion = obj.duracion;
		}
		
		#endregion
		
		#region Metodos Privados
		
		/// <summary>
		/// Obtiene el Hash a partir de un array de Bytes
		/// </summary>
		/// <param name="objectAsBytes"></param>
		/// <returns>string</returns>
		private string ComputeHash(byte[] objectAsBytes)
		{
			MD5 md5 = new MD5CryptoServiceProvider();
			try
			{
				byte[] result = md5.ComputeHash(objectAsBytes);
				
				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < result.Length; i++)
				{
					sb.Append(result[i].ToString("X2"));
				}
				
				return sb.ToString();
			}
			catch (ArgumentNullException ane)
			{
				return null;
			}
		}
		
		/// <summary>
		///     Obtienen el Hash basado en algun algoritmo de Encriptación
		/// </summary>
		/// <typeparam name="T">
		///     Algoritmo de encriptación
		/// </typeparam>
		/// <param name="cryptoServiceProvider">
		///     Provvedor de Servicios de Criptografía
		/// </param>
		/// <returns>
		///     String que representa el Hash calculado
		        /// </returns>
		private string computeHash<T>( T cryptoServiceProvider) where T : HashAlgorithm, new()
		{
			DataContractSerializer serializer = new DataContractSerializer(this.GetType());
			using (MemoryStream memoryStream = new MemoryStream())
			{
				serializer.WriteObject(memoryStream, this);
				cryptoServiceProvider.ComputeHash(memoryStream.ToArray());
				return Convert.ToBase64String(cryptoServiceProvider.Hash);
			}
		}
		
		#endregion
		
		#region Overrides
		
		/// <summary>
		/// Devuelve un String que representa al Objeto
		/// </summary>
		/// <returns>string</returns>
		public override string ToString()
		{
			string hashString;
			
			//Evitar parametros NULL
			if (this == null)
				throw new ArgumentNullException("Parametro NULL no es valido");
			
			//Se verifica si el objeto es serializable.
			try
			{
				MemoryStream memStream = new MemoryStream();
				XmlSerializer serializer = new XmlSerializer(typeof(entRptInsumos));
				serializer.Serialize(memStream, this);
				
				//Ahora se obtiene el Hash del Objeto.
				hashString = ComputeHash(memStream.ToArray());
				
				return hashString;
			}
			catch (AmbiguousMatchException ame)
			{
				throw new ApplicationException("El Objeto no es Serializable. Message:" + ame.Message);
			}
		}
		
		/// <summary>
		/// Verifica que dos objetos sean identicos
		/// </summary>
		public static bool operator ==(entRptInsumos first, entRptInsumos second)
		{
			// Verifica si el puntero en memoria es el mismo
			if (Object.ReferenceEquals(first, second))
				return true;
			
			// Verifica valores nulos
			if ((object) first == null || (object) second == null)
				return false;
			
			return first.GetHashCode() == second.GetHashCode();
		}
		
		/// <summary>
		/// Verifica que dos objetos sean distintos
		/// </summary>
		public static bool operator !=(entRptInsumos first, entRptInsumos second)
		{
			return !(first == second);
		}
		
		/// <summary>
		/// Compara este objeto con otro
		/// </summary>
		/// <param name="obj">El objeto a comparar</param>
		/// <returns>Devuelve Verdadero si ambos objetos son iguales</returns>
		public override bool Equals(object obj)
		{
			if (obj == null)
				return false;
			
			if (obj.GetType() == this.GetType())
				return obj.GetHashCode() == this.GetHashCode();
			
			return false;
		}
		
		#endregion
		
		#region DataAnnotations
		
		/// <summary>
		/// Obtiene los errores basado en los DataAnnotations 
		/// </summary>
		/// <returns>Devuelve un IList del tipo ValidationResult con los errores obtenidos</returns>
		public IList<ValidationResult> ValidationErrors()
		{
			ValidationContext context = new ValidationContext(this, null, null);
			IList<ValidationResult> errors = new List<ValidationResult>();
			
			if (!Validator.TryValidateObject(this, context, errors, true))
				return errors;
			
			return new List<ValidationResult>(0);
		}
		
		/// <summary>
		/// Obtiene los errores basado en los DataAnnotations 
		/// </summary>
		/// <returns>Devuelve un String con los errores obtenidos</returns>
		public string ValidationErrorsString()
		{
			string strErrors = "";
			ValidationContext context = new ValidationContext(this, null, null);
			IList<ValidationResult> errors = new List<ValidationResult>();
			
			if (!Validator.TryValidateObject(this, context, errors, true))
			{
				foreach (ValidationResult result in errors)
					strErrors = strErrors + result.ErrorMessage + Environment.NewLine;
			}
			return strErrors;
		}
		
		/// <summary>
		/// Funcion que determina si un objeto es valido o no
		/// </summary>
		/// <returns>Resultado de la validacion</returns>
		public bool IsValid()
		{
			ValidationContext context = new ValidationContext(this, null, null);
			IList<ValidationResult> errors = new List<ValidationResult>();
			
			return Validator.TryValidateObject(this, context, errors, true);
		}
		
		#endregion
		
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna gestion de la Tabla rpt_insumos
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "gestion", Description = " Propiedad publica de tipo int que representa a la columna gestion de la Tabla rpt_insumos")]
		public int? gestion { get; set; } 

		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna mes de la Tabla rpt_insumos
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "mes", Description = " Propiedad publica de tipo int que representa a la columna mes de la Tabla rpt_insumos")]
		public int? mes { get; set; } 

		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna foto de la Tabla rpt_insumos
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "foto", Description = " Propiedad publica de tipo String que representa a la columna foto de la Tabla rpt_insumos")]
		public String foto { get; set; } 

		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna id_departamento de la Tabla rpt_insumos
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_departamento", Description = " Propiedad publica de tipo int que representa a la columna id_departamento de la Tabla rpt_insumos")]
		public int? id_departamento { get; set; } 

		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna id_upmipp de la Tabla rpt_insumos
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_upmipp", Description = " Propiedad publica de tipo int que representa a la columna id_upmipp de la Tabla rpt_insumos")]
		public int? id_upmipp { get; set; } 

		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna id_provincia de la Tabla rpt_insumos
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_provincia", Description = " Propiedad publica de tipo int que representa a la columna id_provincia de la Tabla rpt_insumos")]
		public int? id_provincia { get; set; } 

		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna id_municipio de la Tabla rpt_insumos
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_municipio", Description = " Propiedad publica de tipo int que representa a la columna id_municipio de la Tabla rpt_insumos")]
		public int? id_municipio { get; set; } 

		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna id_listado de la Tabla rpt_insumos
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_listado", Description = " Propiedad publica de tipo int que representa a la columna id_listado de la Tabla rpt_insumos")]
		public int? id_listado { get; set; } 

		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna id_boleta de la Tabla rpt_insumos
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_boleta", Description = " Propiedad publica de tipo int que representa a la columna id_boleta de la Tabla rpt_insumos")]
		public int? id_boleta { get; set; } 

		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna id_persona de la Tabla rpt_insumos
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_persona", Description = " Propiedad publica de tipo int que representa a la columna id_persona de la Tabla rpt_insumos")]
		public int? id_persona { get; set; } 

		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna id_movimiento de la Tabla rpt_insumos
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_movimiento", Description = " Propiedad publica de tipo int que representa a la columna id_movimiento de la Tabla rpt_insumos")]
		public int? id_movimiento { get; set; } 

		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna control de la Tabla rpt_insumos
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "control", Description = " Propiedad publica de tipo int que representa a la columna control de la Tabla rpt_insumos")]
		public int? control { get; set; } 

		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna minimo de la Tabla rpt_insumos
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "minimo", Description = " Propiedad publica de tipo int que representa a la columna minimo de la Tabla rpt_insumos")]
		public int? minimo { get; set; } 

		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna maximo de la Tabla rpt_insumos
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "maximo", Description = " Propiedad publica de tipo int que representa a la columna maximo de la Tabla rpt_insumos")]
		public int? maximo { get; set; } 

		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna departamento de la Tabla rpt_insumos
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "departamento", Description = " Propiedad publica de tipo String que representa a la columna departamento de la Tabla rpt_insumos")]
		public String departamento { get; set; } 

		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna provincia de la Tabla rpt_insumos
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "provincia", Description = " Propiedad publica de tipo String que representa a la columna provincia de la Tabla rpt_insumos")]
		public String provincia { get; set; } 

		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna municipio de la Tabla rpt_insumos
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "municipio", Description = " Propiedad publica de tipo String que representa a la columna municipio de la Tabla rpt_insumos")]
		public String municipio { get; set; } 

		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna comunidad de la Tabla rpt_insumos
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "comunidad", Description = " Propiedad publica de tipo String que representa a la columna comunidad de la Tabla rpt_insumos")]
		public String comunidad { get; set; } 

		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna login de la Tabla rpt_insumos
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "login", Description = " Propiedad publica de tipo String que representa a la columna login de la Tabla rpt_insumos")]
		public String login { get; set; } 

		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna producto_boleta de la Tabla rpt_insumos
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "producto_boleta", Description = " Propiedad publica de tipo String que representa a la columna producto_boleta de la Tabla rpt_insumos")]
		public String producto_boleta { get; set; } 

		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna codigo_producto de la Tabla rpt_insumos
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "codigo_producto", Description = " Propiedad publica de tipo String que representa a la columna codigo_producto de la Tabla rpt_insumos")]
		public String codigo_producto { get; set; } 

		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna codigo_ccp de la Tabla rpt_insumos
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "codigo_ccp", Description = " Propiedad publica de tipo String que representa a la columna codigo_ccp de la Tabla rpt_insumos")]
		public String codigo_ccp { get; set; } 

		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna informante de la Tabla rpt_insumos
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "informante", Description = " Propiedad publica de tipo String que representa a la columna informante de la Tabla rpt_insumos")]
		public String informante { get; set; } 

		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna telefono de la Tabla rpt_insumos
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "telefono", Description = " Propiedad publica de tipo String que representa a la columna telefono de la Tabla rpt_insumos")]
		public String telefono { get; set; } 

		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna direccion de la Tabla rpt_insumos
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "direccion", Description = " Propiedad publica de tipo String que representa a la columna direccion de la Tabla rpt_insumos")]
		public String direccion { get; set; } 

		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna producto de la Tabla rpt_insumos
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "producto", Description = " Propiedad publica de tipo String que representa a la columna producto de la Tabla rpt_insumos")]
		public String producto { get; set; } 

		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna caracteristicas de la Tabla rpt_insumos
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "caracteristicas", Description = " Propiedad publica de tipo String que representa a la columna caracteristicas de la Tabla rpt_insumos")]
		public String caracteristicas { get; set; } 

		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna variedad de la Tabla rpt_insumos
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "variedad", Description = " Propiedad publica de tipo String que representa a la columna variedad de la Tabla rpt_insumos")]
		public String variedad { get; set; } 

		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna origen de la Tabla rpt_insumos
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "origen", Description = " Propiedad publica de tipo String que representa a la columna origen de la Tabla rpt_insumos")]
		public String origen { get; set; } 

		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna cantidad de la Tabla rpt_insumos
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "cantidad", Description = " Propiedad publica de tipo String que representa a la columna cantidad de la Tabla rpt_insumos")]
		public String cantidad { get; set; } 

		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna unidad de la Tabla rpt_insumos
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "unidad", Description = " Propiedad publica de tipo String que representa a la columna unidad de la Tabla rpt_insumos")]
		public String unidad { get; set; } 

		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna precio de la Tabla rpt_insumos
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "precio", Description = " Propiedad publica de tipo String que representa a la columna precio de la Tabla rpt_insumos")]
		public String precio { get; set; } 

		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna superficie de la Tabla rpt_insumos
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "superficie", Description = " Propiedad publica de tipo String que representa a la columna superficie de la Tabla rpt_insumos")]
		public String superficie { get; set; }

        /// <summary>
        /// 	 Propiedad publica de tipo String que representa a la columna unidad_superficie de la Tabla rpt_insumos
        /// 	 Permite Null: Yes
        /// 	 Es Calculada: No
        /// 	 Es RowGui: No
        /// 	 Es PrimaryKey: No
        /// 	 Es ForeignKey: No
        /// </summary>
        [Display(Name = "unidad_superficie", Description = " Propiedad publica de tipo String que representa a la columna unidad_superficie de la Tabla rpt_insumos")]
        public String unidad_superficie { get; set; } 

		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna observaciones de la Tabla rpt_insumos
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "observaciones", Description = " Propiedad publica de tipo String que representa a la columna observaciones de la Tabla rpt_insumos")]
		public String observaciones { get; set; } 

		/// <summary>
		/// 	 Propiedad publica de tipo DateTime que representa a la columna minfec de la Tabla rpt_insumos
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "minfec", Description = " Propiedad publica de tipo DateTime que representa a la columna minfec de la Tabla rpt_insumos")]
		public DateTime? minfec { get; set; } 

		/// <summary>
		/// 	 Propiedad publica de tipo DateTime que representa a la columna maxfec de la Tabla rpt_insumos
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "maxfec", Description = " Propiedad publica de tipo DateTime que representa a la columna maxfec de la Tabla rpt_insumos")]
		public DateTime? maxfec { get; set; } 

		/// <summary>
		/// 	 Propiedad publica de tipo Decimal que representa a la columna duracion de la Tabla rpt_insumos
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "duracion", Description = " Propiedad publica de tipo Decimal que representa a la columna duracion de la Tabla rpt_insumos")]
		public Decimal? duracion { get; set; } 

	}
}

