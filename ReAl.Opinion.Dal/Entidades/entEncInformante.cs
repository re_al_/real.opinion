#region 
/***********************************************************************************************************
	NOMBRE:       entEncInformante
	DESCRIPCION:
		Clase que define un objeto para la Tabla enc_informante

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.Opinion.Dal.Entidades
{
	public class entEncInformante : cBaseClass
	{
		public const String strNombreTabla = "enc_informante";
		public const String strAliasTabla = "enc_informante";
		public enum Fields
		{
			id_informante
			,id_tab_informante
			,id_informante_padre
			,id_tab_informante_padre
			,id_upm
			,id_nivel
			,id_movimiento
			,latitud
			,longitud
			,codigo
			,descripcion
			,apiestado
			,usucre
			,feccre
			,usumod
			,fecmod

		}
		
		#region Constructoress
		
		public entEncInformante()
		{
			//Inicializacion de Variables
			this.id_informante_padre = null;
			this.id_tab_informante_padre = null;
			this.codigo = null;
			this.descripcion = null;
			this.apiestado = null;
			this.usucre = null;
			this.usumod = null;
			this.fecmod = null;
		}
		
		public entEncInformante(entEncInformante obj)
		{
			this.id_informante = obj.id_informante;
			this.id_tab_informante = obj.id_tab_informante;
			this.id_informante_padre = obj.id_informante_padre;
			this.id_tab_informante_padre = obj.id_tab_informante_padre;
			this.id_upm = obj.id_upm;
			this.id_nivel = obj.id_nivel;
			this.id_movimiento = obj.id_movimiento;
			this.latitud = obj.latitud;
			this.longitud = obj.longitud;
			this.codigo = obj.codigo;
			this.descripcion = obj.descripcion;
			this.apiestado = obj.apiestado;
			this.usucre = obj.usucre;
			this.feccre = obj.feccre;
			this.usumod = obj.usumod;
			this.fecmod = obj.fecmod;
		}
		
		#endregion
		
		/// <summary>
		/// 	 Variable local de tipo Int64 que representa a la columna id_informante de la Tabla enc_informante
		/// </summary>
		private Int64 _id_informante;
		/// <summary>
		/// 	 Identificador unico que representa al registro en la tabla
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: Yes
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_informante", Description = "Identificador unico que representa al registro en la tabla")]
		[Required(ErrorMessage = "id_informante es un campo requerido.")]
		[Key]
		public Int64 id_informante
		{
			get {return _id_informante;}
			set
			{
				if (_id_informante != value)
				{
					RaisePropertyChanging(entEncInformante.Fields.id_informante.ToString());
					_id_informante = value;
					RaisePropertyChanged(entEncInformante.Fields.id_informante.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_tab_informante de la Tabla enc_informante
		/// </summary>
		private int _id_tab_informante;
		/// <summary>
		/// 	 Identificador unico que representa al registro en la tabla en el Dispositivo
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_tab_informante", Description = "Identificador unico que representa al registro en la tabla en el Dispositivo")]
		[Required(ErrorMessage = "id_tab_informante es un campo requerido.")]
		public int id_tab_informante
		{
			get {return _id_tab_informante;}
			set
			{
				if (_id_tab_informante != value)
				{
					RaisePropertyChanging(entEncInformante.Fields.id_tab_informante.ToString());
					_id_tab_informante = value;
					RaisePropertyChanged(entEncInformante.Fields.id_tab_informante.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_informante_padre de la Tabla enc_informante
		/// </summary>
		private int? _id_informante_padre;
		/// <summary>
		/// 	 Identificador que representa al registro padre en la misma tabla
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_informante_padre", Description = "Identificador que representa al registro padre en la misma tabla")]
		public int? id_informante_padre
		{
			get {return _id_informante_padre;}
			set
			{
				if (_id_informante_padre != value)
				{
					RaisePropertyChanging(entEncInformante.Fields.id_informante_padre.ToString());
					_id_informante_padre = value;
					RaisePropertyChanged(entEncInformante.Fields.id_informante_padre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_tab_informante_padre de la Tabla enc_informante
		/// </summary>
		private int? _id_tab_informante_padre;
		/// <summary>
		/// 	 Identificador que representa al registro padre en la misma tabla en el Dispositivo
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_tab_informante_padre", Description = "Identificador que representa al registro padre en la misma tabla en el Dispositivo")]
		public int? id_tab_informante_padre
		{
			get {return _id_tab_informante_padre;}
			set
			{
				if (_id_tab_informante_padre != value)
				{
					RaisePropertyChanging(entEncInformante.Fields.id_tab_informante_padre.ToString());
					_id_tab_informante_padre = value;
					RaisePropertyChanged(entEncInformante.Fields.id_tab_informante_padre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_upm de la Tabla enc_informante
		/// </summary>
		private int _id_upm;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna id_upm de la Tabla enc_informante
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_upm", Description = " Propiedad publica de tipo int que representa a la columna id_upm de la Tabla enc_informante")]
		[Required(ErrorMessage = "id_upm es un campo requerido.")]
		public int id_upm
		{
			get {return _id_upm;}
			set
			{
				if (_id_upm != value)
				{
					RaisePropertyChanging(entEncInformante.Fields.id_upm.ToString());
					_id_upm = value;
					RaisePropertyChanged(entEncInformante.Fields.id_upm.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_nivel de la Tabla enc_informante
		/// </summary>
		private int _id_nivel;
		/// <summary>
		/// 	 Es el identificador unico de la tabla enc_nivel
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: Yes
		/// </summary>
		[Display(Name = "id_nivel", Description = "Es el identificador unico de la tabla enc_nivel")]
		[Required(ErrorMessage = "id_nivel es un campo requerido.")]
		public int id_nivel
		{
			get {return _id_nivel;}
			set
			{
				if (_id_nivel != value)
				{
					RaisePropertyChanging(entEncInformante.Fields.id_nivel.ToString());
					_id_nivel = value;
					RaisePropertyChanged(entEncInformante.Fields.id_nivel.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_movimiento de la Tabla enc_informante
		/// </summary>
		private int _id_movimiento;
		/// <summary>
		/// 	 Es el identificador unico de la tabla enc_movimiento
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_movimiento", Description = "Es el identificador unico de la tabla enc_movimiento")]
		[Required(ErrorMessage = "id_movimiento es un campo requerido.")]
		public int id_movimiento
		{
			get {return _id_movimiento;}
			set
			{
				if (_id_movimiento != value)
				{
					RaisePropertyChanging(entEncInformante.Fields.id_movimiento.ToString());
					_id_movimiento = value;
					RaisePropertyChanged(entEncInformante.Fields.id_movimiento.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo Decimal que representa a la columna latitud de la Tabla enc_informante
		/// </summary>
		private Decimal _latitud;
		/// <summary>
		/// 	 Latitud del punto CERO de la Capital de Departamento
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "latitud", Description = "Latitud del punto CERO de la Capital de Departamento")]
		[Required(ErrorMessage = "latitud es un campo requerido.")]
		public Decimal latitud
		{
			get {return _latitud;}
			set
			{
				if (_latitud != value)
				{
					RaisePropertyChanging(entEncInformante.Fields.latitud.ToString());
					_latitud = value;
					RaisePropertyChanged(entEncInformante.Fields.latitud.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo Decimal que representa a la columna longitud de la Tabla enc_informante
		/// </summary>
		private Decimal _longitud;
		/// <summary>
		/// 	 Longitud del punto CERO de la Capital de Departamento
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "longitud", Description = "Longitud del punto CERO de la Capital de Departamento")]
		[Required(ErrorMessage = "longitud es un campo requerido.")]
		public Decimal longitud
		{
			get {return _longitud;}
			set
			{
				if (_longitud != value)
				{
					RaisePropertyChanging(entEncInformante.Fields.longitud.ToString());
					_longitud = value;
					RaisePropertyChanged(entEncInformante.Fields.longitud.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna codigo de la Tabla enc_informante
		/// </summary>
		private String _codigo;
		/// <summary>
		/// 	 Codigo del informante
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(50, MinimumLength=0)]
		[Display(Name = "codigo", Description = "Codigo del informante")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "codigo es un campo requerido.")]
		public String codigo
		{
			get {return _codigo;}
			set
			{
				if (_codigo != value)
				{
					RaisePropertyChanging(entEncInformante.Fields.codigo.ToString());
					_codigo = value;
					RaisePropertyChanged(entEncInformante.Fields.codigo.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna descripcion de la Tabla enc_informante
		/// </summary>
		private String _descripcion;
		/// <summary>
		/// 	 Descripcion del informante
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "descripcion", Description = "Descripcion del informante")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "descripcion es un campo requerido.")]
		public String descripcion
		{
			get {return _descripcion;}
			set
			{
				if (_descripcion != value)
				{
					RaisePropertyChanging(entEncInformante.Fields.descripcion.ToString());
					_descripcion = value;
					RaisePropertyChanged(entEncInformante.Fields.descripcion.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna apiestado de la Tabla enc_informante
		/// </summary>
		private String _apiestado;
		/// <summary>
		/// 	 Estado en el que se encuentra el registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "apiestado", Description = "Estado en el que se encuentra el registro")]
		[EnumDataType(typeof(cApi.Estado))]
		public String apiestado
		{
			get {return _apiestado;}
			set
			{
				if (_apiestado != value)
				{
					RaisePropertyChanging(entEncInformante.Fields.apiestado.ToString());
					_apiestado = value;
					RaisePropertyChanged(entEncInformante.Fields.apiestado.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usucre de la Tabla enc_informante
		/// </summary>
		private String _usucre;
		/// <summary>
		/// 	 Login o nombre de usuario que ha creado el registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usucre", Description = "Login o nombre de usuario que ha creado el registro")]
		public String usucre
		{
			get {return _usucre;}
			set
			{
				if (_usucre != value)
				{
					RaisePropertyChanging(entEncInformante.Fields.usucre.ToString());
					_usucre = value;
					RaisePropertyChanged(entEncInformante.Fields.usucre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna feccre de la Tabla enc_informante
		/// </summary>
		private DateTime _feccre;
		/// <summary>
		/// 	 Fecha de creacion del registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "feccre", Description = "Fecha de creacion del registro")]
		public DateTime feccre
		{
			get {return _feccre;}
			set
			{
				if (_feccre != value)
				{
					RaisePropertyChanging(entEncInformante.Fields.feccre.ToString());
					_feccre = value;
					RaisePropertyChanged(entEncInformante.Fields.feccre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usumod de la Tabla enc_informante
		/// </summary>
		private String _usumod;
		/// <summary>
		/// 	 Login o nombre de usuario que ha realizado la ULTIMA modificacion registro
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usumod", Description = "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro")]
		public String usumod
		{
			get {return _usumod;}
			set
			{
				if (_usumod != value)
				{
					RaisePropertyChanging(entEncInformante.Fields.usumod.ToString());
					_usumod = value;
					RaisePropertyChanged(entEncInformante.Fields.usumod.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna fecmod de la Tabla enc_informante
		/// </summary>
		private DateTime? _fecmod;
		/// <summary>
		/// 	 Fecha en la que se ha realizado la ULTIMA modificacion registro
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "fecmod", Description = "Fecha en la que se ha realizado la ULTIMA modificacion registro")]
		public DateTime? fecmod
		{
			get {return _fecmod;}
			set
			{
				if (_fecmod != value)
				{
					RaisePropertyChanging(entEncInformante.Fields.fecmod.ToString());
					_fecmod = value;
					RaisePropertyChanged(entEncInformante.Fields.fecmod.ToString());
				}
			}
		}


	}
}

