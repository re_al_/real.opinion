#region 
/***********************************************************************************************************
	NOMBRE:       entEncSeccion
	DESCRIPCION:
		Clase que define un objeto para la Tabla enc_seccion

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.Opinion.Dal.Entidades
{
	public class entEncSeccion : cBaseClass
	{
		public const String strNombreTabla = "enc_seccion";
		public const String strAliasTabla = "enc_seccion";
		public enum Fields
		{
			id_seccion
			,id_proyecto
			,id_nivel
			,codigo
			,seccion
			,apiestado
			,usucre
			,feccre
			,usumod
			,fecmod
			,abierta

		}
		
		#region Constructoress
		
		public entEncSeccion()
		{
			//Inicializacion de Variables
			this.codigo = null;
			this.seccion = null;
			this.apiestado = null;
			this.usucre = null;
			this.usumod = null;
			this.fecmod = null;
		}
		
		public entEncSeccion(entEncSeccion obj)
		{
			this.id_seccion = obj.id_seccion;
			this.id_proyecto = obj.id_proyecto;
			this.id_nivel = obj.id_nivel;
			this.codigo = obj.codigo;
			this.seccion = obj.seccion;
			this.apiestado = obj.apiestado;
			this.usucre = obj.usucre;
			this.feccre = obj.feccre;
			this.usumod = obj.usumod;
			this.fecmod = obj.fecmod;
			this.abierta = obj.abierta;
		}
		
		#endregion
		
		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_seccion de la Tabla enc_seccion
		/// </summary>
		private int _id_seccion;
		/// <summary>
		/// 	 Identificador de la seccion al que pertenece el registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: Yes
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_seccion", Description = "Identificador de la seccion al que pertenece el registro")]
		[Required(ErrorMessage = "id_seccion es un campo requerido.")]
		[Key]
		public int id_seccion
		{
			get {return _id_seccion;}
			set
			{
				if (_id_seccion != value)
				{
					RaisePropertyChanging(entEncSeccion.Fields.id_seccion.ToString());
					_id_seccion = value;
					RaisePropertyChanged(entEncSeccion.Fields.id_seccion.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_proyecto de la Tabla enc_seccion
		/// </summary>
		private int _id_proyecto;
		/// <summary>
		/// 	 Identificador del proyecto al que pertenece el registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_proyecto", Description = "Identificador del proyecto al que pertenece el registro")]
		[Required(ErrorMessage = "id_proyecto es un campo requerido.")]
		public int id_proyecto
		{
			get {return _id_proyecto;}
			set
			{
				if (_id_proyecto != value)
				{
					RaisePropertyChanging(entEncSeccion.Fields.id_proyecto.ToString());
					_id_proyecto = value;
					RaisePropertyChanged(entEncSeccion.Fields.id_proyecto.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_nivel de la Tabla enc_seccion
		/// </summary>
		private int _id_nivel;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna id_nivel de la Tabla enc_seccion
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_nivel", Description = " Propiedad publica de tipo int que representa a la columna id_nivel de la Tabla enc_seccion")]
		[Required(ErrorMessage = "id_nivel es un campo requerido.")]
		public int id_nivel
		{
			get {return _id_nivel;}
			set
			{
				if (_id_nivel != value)
				{
					RaisePropertyChanging(entEncSeccion.Fields.id_nivel.ToString());
					_id_nivel = value;
					RaisePropertyChanged(entEncSeccion.Fields.id_nivel.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna codigo de la Tabla enc_seccion
		/// </summary>
		private String _codigo;
		/// <summary>
		/// 	 Codigo de seccion
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "codigo", Description = "Codigo de seccion")]
		public String codigo
		{
			get {return _codigo;}
			set
			{
				if (_codigo != value)
				{
					RaisePropertyChanging(entEncSeccion.Fields.codigo.ToString());
					_codigo = value;
					RaisePropertyChanged(entEncSeccion.Fields.codigo.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna seccion de la Tabla enc_seccion
		/// </summary>
		private String _seccion;
		/// <summary>
		/// 	 Texto o nombre de seccion
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "seccion", Description = "Texto o nombre de seccion")]
		public String seccion
		{
			get {return _seccion;}
			set
			{
				if (_seccion != value)
				{
					RaisePropertyChanging(entEncSeccion.Fields.seccion.ToString());
					_seccion = value;
					RaisePropertyChanged(entEncSeccion.Fields.seccion.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna apiestado de la Tabla enc_seccion
		/// </summary>
		private String _apiestado;
		/// <summary>
		/// 	 Describe el estado en el que se encuentra un determinado registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "apiestado", Description = "Describe el estado en el que se encuentra un determinado registro")]
		[EnumDataType(typeof(cApi.Estado))]
		public String apiestado
		{
			get {return _apiestado;}
			set
			{
				if (_apiestado != value)
				{
					RaisePropertyChanging(entEncSeccion.Fields.apiestado.ToString());
					_apiestado = value;
					RaisePropertyChanged(entEncSeccion.Fields.apiestado.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usucre de la Tabla enc_seccion
		/// </summary>
		private String _usucre;
		/// <summary>
		/// 	 Describe el usuario que creo un determinado un registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usucre", Description = "Describe el usuario que creo un determinado un registro")]
		public String usucre
		{
			get {return _usucre;}
			set
			{
				if (_usucre != value)
				{
					RaisePropertyChanging(entEncSeccion.Fields.usucre.ToString());
					_usucre = value;
					RaisePropertyChanged(entEncSeccion.Fields.usucre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna feccre de la Tabla enc_seccion
		/// </summary>
		private DateTime _feccre;
		/// <summary>
		/// 	 Describe la fecha de creaciÃ³n de un determinado registro
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "feccre", Description = "Describe la fecha de creaciÃ³n de un determinado registro")]
		public DateTime feccre
		{
			get {return _feccre;}
			set
			{
				if (_feccre != value)
				{
					RaisePropertyChanging(entEncSeccion.Fields.feccre.ToString());
					_feccre = value;
					RaisePropertyChanged(entEncSeccion.Fields.feccre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usumod de la Tabla enc_seccion
		/// </summary>
		private String _usumod;
		/// <summary>
		/// 	 Describe el usuario que modifico un determinado registro
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usumod", Description = "Describe el usuario que modifico un determinado registro")]
		public String usumod
		{
			get {return _usumod;}
			set
			{
				if (_usumod != value)
				{
					RaisePropertyChanging(entEncSeccion.Fields.usumod.ToString());
					_usumod = value;
					RaisePropertyChanged(entEncSeccion.Fields.usumod.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna fecmod de la Tabla enc_seccion
		/// </summary>
		private DateTime? _fecmod;
		/// <summary>
		/// 	 Describe la fecha de modificacciÃ³n de determinado un registro
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "fecmod", Description = "Describe la fecha de modificacciÃ³n de determinado un registro")]
		public DateTime? fecmod
		{
			get {return _fecmod;}
			set
			{
				if (_fecmod != value)
				{
					RaisePropertyChanging(entEncSeccion.Fields.fecmod.ToString());
					_fecmod = value;
					RaisePropertyChanged(entEncSeccion.Fields.fecmod.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna abierta de la Tabla enc_seccion
		/// </summary>
		private int _abierta;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna abierta de la Tabla enc_seccion
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "abierta", Description = " Propiedad publica de tipo int que representa a la columna abierta de la Tabla enc_seccion")]
		[Required(ErrorMessage = "abierta es un campo requerido.")]
		public int abierta
		{
			get {return _abierta;}
			set
			{
				if (_abierta != value)
				{
					RaisePropertyChanging(entEncSeccion.Fields.abierta.ToString());
					_abierta = value;
					RaisePropertyChanged(entEncSeccion.Fields.abierta.ToString());
				}
			}
		}


	}
}

