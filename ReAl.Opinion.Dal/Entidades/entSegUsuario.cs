#region 
/***********************************************************************************************************
	NOMBRE:       entSegUsuario
	DESCRIPCION:
		Clase que define un objeto para la Tabla seg_usuario

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.Opinion.Dal.Entidades
{
	public class entSegUsuario : cBaseClass
	{
		public const String strNombreTabla = "seg_usuario";
		public const String strAliasTabla = "seg_usuario";
		public enum Fields
		{
			id_usuario
			,id_departamento
			,login
			,password
			,carnet
			,nombre
			,paterno
			,materno
			,direccion
			,telefono
			,fecha_vigente
			,foto
			,fec_nacimiento
			,correo
			,remember_token
			,apiestado
			,usucre
			,feccre
			,usumod
			,fecmod

		}
		
		#region Constructoress
		
		public entSegUsuario()
		{
			//Inicializacion de Variables
			this.id_departamento = null;
			this.login = null;
			this.password = null;
			this.carnet = null;
			this.nombre = null;
			this.paterno = null;
			this.materno = null;
			this.direccion = null;
			this.telefono = null;
			this.foto = null;
			this.fec_nacimiento = null;
			this.correo = null;
			this.remember_token = null;
			this.apiestado = null;
			this.usucre = null;
			this.usumod = null;
			this.fecmod = null;
		}
		
		public entSegUsuario(entSegUsuario obj)
		{
			this.id_usuario = obj.id_usuario;
			this.id_departamento = obj.id_departamento;
			this.login = obj.login;
			this.password = obj.password;
			this.carnet = obj.carnet;
			this.nombre = obj.nombre;
			this.paterno = obj.paterno;
			this.materno = obj.materno;
			this.direccion = obj.direccion;
			this.telefono = obj.telefono;
			this.fecha_vigente = obj.fecha_vigente;
			this.foto = obj.foto;
			this.fec_nacimiento = obj.fec_nacimiento;
			this.correo = obj.correo;
			this.remember_token = obj.remember_token;
			this.apiestado = obj.apiestado;
			this.usucre = obj.usucre;
			this.feccre = obj.feccre;
			this.usumod = obj.usumod;
			this.fecmod = obj.fecmod;
		}
		
		#endregion
		
		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_usuario de la Tabla seg_usuario
		/// </summary>
		private int _id_usuario;
		/// <summary>
		/// 	 llave primaria de los usuarios
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: Yes
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_usuario", Description = "llave primaria de los usuarios")]
		[Required(ErrorMessage = "id_usuario es un campo requerido.")]
		[Key]
		public int id_usuario
		{
			get {return _id_usuario;}
			set
			{
				if (_id_usuario != value)
				{
					RaisePropertyChanging(entSegUsuario.Fields.id_usuario.ToString());
					_id_usuario = value;
					RaisePropertyChanged(entSegUsuario.Fields.id_usuario.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo int que representa a la columna id_departamento de la Tabla seg_usuario
		/// </summary>
		private int? _id_departamento;
		/// <summary>
		/// 	 Propiedad publica de tipo int que representa a la columna id_departamento de la Tabla seg_usuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: Yes
		/// </summary>
		[Display(Name = "id_departamento", Description = " Propiedad publica de tipo int que representa a la columna id_departamento de la Tabla seg_usuario")]
		public int? id_departamento
		{
			get {return _id_departamento;}
			set
			{
				if (_id_departamento != value)
				{
					RaisePropertyChanging(entSegUsuario.Fields.id_departamento.ToString());
					_id_departamento = value;
					RaisePropertyChanged(entSegUsuario.Fields.id_departamento.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna login de la Tabla seg_usuario
		/// </summary>
		private String _login;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna login de la Tabla seg_usuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(20, MinimumLength=0)]
		[Display(Name = "login", Description = " Propiedad publica de tipo String que representa a la columna login de la Tabla seg_usuario")]
		public String login
		{
			get {return _login;}
			set
			{
				if (_login != value)
				{
					RaisePropertyChanging(entSegUsuario.Fields.login.ToString());
					_login = value;
					RaisePropertyChanged(entSegUsuario.Fields.login.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna password de la Tabla seg_usuario
		/// </summary>
		private String _password;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna password de la Tabla seg_usuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "password", Description = " Propiedad publica de tipo String que representa a la columna password de la Tabla seg_usuario")]
		public String password
		{
			get {return _password;}
			set
			{
				if (_password != value)
				{
					RaisePropertyChanging(entSegUsuario.Fields.password.ToString());
					_password = value;
					RaisePropertyChanged(entSegUsuario.Fields.password.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo Decimal que representa a la columna carnet de la Tabla seg_usuario
		/// </summary>
		private Decimal? _carnet;
		/// <summary>
		/// 	 Propiedad publica de tipo Decimal que representa a la columna carnet de la Tabla seg_usuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "carnet", Description = " Propiedad publica de tipo Decimal que representa a la columna carnet de la Tabla seg_usuario")]
		public Decimal? carnet
		{
			get {return _carnet;}
			set
			{
				if (_carnet != value)
				{
					RaisePropertyChanging(entSegUsuario.Fields.carnet.ToString());
					_carnet = value;
					RaisePropertyChanged(entSegUsuario.Fields.carnet.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna nombre de la Tabla seg_usuario
		/// </summary>
		private String _nombre;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna nombre de la Tabla seg_usuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "nombre", Description = " Propiedad publica de tipo String que representa a la columna nombre de la Tabla seg_usuario")]
		public String nombre
		{
			get {return _nombre;}
			set
			{
				if (_nombre != value)
				{
					RaisePropertyChanging(entSegUsuario.Fields.nombre.ToString());
					_nombre = value;
					RaisePropertyChanged(entSegUsuario.Fields.nombre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna paterno de la Tabla seg_usuario
		/// </summary>
		private String _paterno;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna paterno de la Tabla seg_usuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "paterno", Description = " Propiedad publica de tipo String que representa a la columna paterno de la Tabla seg_usuario")]
		public String paterno
		{
			get {return _paterno;}
			set
			{
				if (_paterno != value)
				{
					RaisePropertyChanging(entSegUsuario.Fields.paterno.ToString());
					_paterno = value;
					RaisePropertyChanged(entSegUsuario.Fields.paterno.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna materno de la Tabla seg_usuario
		/// </summary>
		private String _materno;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna materno de la Tabla seg_usuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "materno", Description = " Propiedad publica de tipo String que representa a la columna materno de la Tabla seg_usuario")]
		public String materno
		{
			get {return _materno;}
			set
			{
				if (_materno != value)
				{
					RaisePropertyChanging(entSegUsuario.Fields.materno.ToString());
					_materno = value;
					RaisePropertyChanged(entSegUsuario.Fields.materno.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna direccion de la Tabla seg_usuario
		/// </summary>
		private String _direccion;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna direccion de la Tabla seg_usuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "direccion", Description = " Propiedad publica de tipo String que representa a la columna direccion de la Tabla seg_usuario")]
		public String direccion
		{
			get {return _direccion;}
			set
			{
				if (_direccion != value)
				{
					RaisePropertyChanging(entSegUsuario.Fields.direccion.ToString());
					_direccion = value;
					RaisePropertyChanged(entSegUsuario.Fields.direccion.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo Decimal que representa a la columna telefono de la Tabla seg_usuario
		/// </summary>
		private Decimal? _telefono;
		/// <summary>
		/// 	 Propiedad publica de tipo Decimal que representa a la columna telefono de la Tabla seg_usuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "telefono", Description = " Propiedad publica de tipo Decimal que representa a la columna telefono de la Tabla seg_usuario")]
		public Decimal? telefono
		{
			get {return _telefono;}
			set
			{
				if (_telefono != value)
				{
					RaisePropertyChanging(entSegUsuario.Fields.telefono.ToString());
					_telefono = value;
					RaisePropertyChanged(entSegUsuario.Fields.telefono.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna fecha_vigente de la Tabla seg_usuario
		/// </summary>
		private DateTime _fecha_vigente;
		/// <summary>
		/// 	 Propiedad publica de tipo DateTime que representa a la columna fecha_vigente de la Tabla seg_usuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "fecha_vigente", Description = " Propiedad publica de tipo DateTime que representa a la columna fecha_vigente de la Tabla seg_usuario")]
		public DateTime fecha_vigente
		{
			get {return _fecha_vigente;}
			set
			{
				if (_fecha_vigente != value)
				{
					RaisePropertyChanging(entSegUsuario.Fields.fecha_vigente.ToString());
					_fecha_vigente = value;
					RaisePropertyChanged(entSegUsuario.Fields.fecha_vigente.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna foto de la Tabla seg_usuario
		/// </summary>
		private String _foto;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna foto de la Tabla seg_usuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "foto", Description = " Propiedad publica de tipo String que representa a la columna foto de la Tabla seg_usuario")]
		public String foto
		{
			get {return _foto;}
			set
			{
				if (_foto != value)
				{
					RaisePropertyChanging(entSegUsuario.Fields.foto.ToString());
					_foto = value;
					RaisePropertyChanged(entSegUsuario.Fields.foto.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna fec_nacimiento de la Tabla seg_usuario
		/// </summary>
		private DateTime? _fec_nacimiento;
		/// <summary>
		/// 	 Propiedad publica de tipo DateTime que representa a la columna fec_nacimiento de la Tabla seg_usuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "fec_nacimiento", Description = " Propiedad publica de tipo DateTime que representa a la columna fec_nacimiento de la Tabla seg_usuario")]
		public DateTime? fec_nacimiento
		{
			get {return _fec_nacimiento;}
			set
			{
				if (_fec_nacimiento != value)
				{
					RaisePropertyChanging(entSegUsuario.Fields.fec_nacimiento.ToString());
					_fec_nacimiento = value;
					RaisePropertyChanged(entSegUsuario.Fields.fec_nacimiento.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna correo de la Tabla seg_usuario
		/// </summary>
		private String _correo;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna correo de la Tabla seg_usuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "correo", Description = " Propiedad publica de tipo String que representa a la columna correo de la Tabla seg_usuario")]
		public String correo
		{
			get {return _correo;}
			set
			{
				if (_correo != value)
				{
					RaisePropertyChanging(entSegUsuario.Fields.correo.ToString());
					_correo = value;
					RaisePropertyChanged(entSegUsuario.Fields.correo.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna remember_token de la Tabla seg_usuario
		/// </summary>
		private String _remember_token;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna remember_token de la Tabla seg_usuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(50, MinimumLength=0)]
		[Display(Name = "remember_token", Description = " Propiedad publica de tipo String que representa a la columna remember_token de la Tabla seg_usuario")]
		public String remember_token
		{
			get {return _remember_token;}
			set
			{
				if (_remember_token != value)
				{
					RaisePropertyChanging(entSegUsuario.Fields.remember_token.ToString());
					_remember_token = value;
					RaisePropertyChanged(entSegUsuario.Fields.remember_token.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna apiestado de la Tabla seg_usuario
		/// </summary>
		private String _apiestado;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna apiestado de la Tabla seg_usuario
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "apiestado", Description = " Propiedad publica de tipo String que representa a la columna apiestado de la Tabla seg_usuario")]
		[EnumDataType(typeof(cApi.Estado))]
		public String apiestado
		{
			get {return _apiestado;}
			set
			{
				if (_apiestado != value)
				{
					RaisePropertyChanging(entSegUsuario.Fields.apiestado.ToString());
					_apiestado = value;
					RaisePropertyChanged(entSegUsuario.Fields.apiestado.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usucre de la Tabla seg_usuario
		/// </summary>
		private String _usucre;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna usucre de la Tabla seg_usuario
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usucre", Description = " Propiedad publica de tipo String que representa a la columna usucre de la Tabla seg_usuario")]
		public String usucre
		{
			get {return _usucre;}
			set
			{
				if (_usucre != value)
				{
					RaisePropertyChanging(entSegUsuario.Fields.usucre.ToString());
					_usucre = value;
					RaisePropertyChanged(entSegUsuario.Fields.usucre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna feccre de la Tabla seg_usuario
		/// </summary>
		private DateTime _feccre;
		/// <summary>
		/// 	 Propiedad publica de tipo DateTime que representa a la columna feccre de la Tabla seg_usuario
		/// 	 Permite Null: No
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "feccre", Description = " Propiedad publica de tipo DateTime que representa a la columna feccre de la Tabla seg_usuario")]
		public DateTime feccre
		{
			get {return _feccre;}
			set
			{
				if (_feccre != value)
				{
					RaisePropertyChanging(entSegUsuario.Fields.feccre.ToString());
					_feccre = value;
					RaisePropertyChanged(entSegUsuario.Fields.feccre.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo String que representa a la columna usumod de la Tabla seg_usuario
		/// </summary>
		private String _usumod;
		/// <summary>
		/// 	 Propiedad publica de tipo String que representa a la columna usumod de la Tabla seg_usuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usumod", Description = " Propiedad publica de tipo String que representa a la columna usumod de la Tabla seg_usuario")]
		public String usumod
		{
			get {return _usumod;}
			set
			{
				if (_usumod != value)
				{
					RaisePropertyChanging(entSegUsuario.Fields.usumod.ToString());
					_usumod = value;
					RaisePropertyChanged(entSegUsuario.Fields.usumod.ToString());
				}
			}
		}


		/// <summary>
		/// 	 Variable local de tipo DateTime que representa a la columna fecmod de la Tabla seg_usuario
		/// </summary>
		private DateTime? _fecmod;
		/// <summary>
		/// 	 Propiedad publica de tipo DateTime que representa a la columna fecmod de la Tabla seg_usuario
		/// 	 Permite Null: Yes
		/// 	 Es Calculada: No
		/// 	 Es RowGui: No
		/// 	 Es PrimaryKey: No
		/// 	 Es ForeignKey: No
		/// </summary>
		[Display(Name = "fecmod", Description = " Propiedad publica de tipo DateTime que representa a la columna fecmod de la Tabla seg_usuario")]
		public DateTime? fecmod
		{
			get {return _fecmod;}
			set
			{
				if (_fecmod != value)
				{
					RaisePropertyChanging(entSegUsuario.Fields.fecmod.ToString());
					_fecmod = value;
					RaisePropertyChanged(entSegUsuario.Fields.fecmod.ToString());
				}
			}
		}


	}
}

