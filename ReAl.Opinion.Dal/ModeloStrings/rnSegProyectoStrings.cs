#region 
/***********************************************************************************************************
	NOMBRE:       rnSegProyecto
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla seg_proyecto

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        12/10/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Ine.Sice.Dal.Sociales; 
using Ine.Sice.PgConn.Sociales; 
using Ine.Sice.Dal.Sociales.Entidades;
using Ine.Sice.Dal.Sociales.Interface;
using System.Web.UI.WebControls;
#endregion

namespace Ine.Sice.Dal.Sociales.Modelo
{
	public partial class rnSegProyecto: inSegProyecto
	{
		public const string strNombreLisForm = "Listado de seg_proyecto";
		public const string strNombreDocForm = "Detalle de seg_proyecto";
		public const string strNombreForm = "Registro de seg_proyecto";
		/// <summary>
		/// 	Funcion que obtiene la Descripcion de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetDesc(entSegProyecto.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entSegProyecto.Fields.id_proyecto == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Llave primaria del identificador de la tabla";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Llave primaria del identificador de la tabla";
					return "Llave primaria del identificador de la tabla";
				}
				if (entSegProyecto.Fields.nombre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Nombre del Proyecto";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Nombre del Proyecto";
					return "Nombre del Proyecto";
				}
				if (entSegProyecto.Fields.codigo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Codigo Unico de 4 CARACTERES con el que se identifica al Proyecto";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Codigo Unico de 4 CARACTERES con el que se identifica al Proyecto";
					return "Codigo Unico de 4 CARACTERES con el que se identifica al Proyecto";
				}
				if (entSegProyecto.Fields.descripcion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Descripcion del proyecto";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Descripcion del proyecto";
					return "Descripcion del proyecto";
				}
				if (entSegProyecto.Fields.fecinicio == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Fecha en la que INICIA el proyecto ";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Fecha en la que INICIA el proyecto ";
					return "Fecha en la que INICIA el proyecto ";
				}
				if (entSegProyecto.Fields.fecfin == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Fecha en la que FINALIZA el proyecto ";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Fecha en la que FINALIZA el proyecto ";
					return "Fecha en la que FINALIZA el proyecto ";
				}
				if (entSegProyecto.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Estado en el que se encuentra el registro: debe ser ACTIVO o INACTIVO";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Estado en el que se encuentra el registro: debe ser ACTIVO o INACTIVO";
					return "Estado en el que se encuentra el registro: debe ser ACTIVO o INACTIVO";
				}
				if (entSegProyecto.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Login o nombre de usuario que ha creado el registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Login o nombre de usuario que ha creado el registro";
					return "Login o nombre de usuario que ha creado el registro";
				}
				if (entSegProyecto.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Fecha de creacion del registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Fecha de creacion del registro";
					return "Fecha de creacion del registro";
				}
				if (entSegProyecto.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro";
					return "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro";
				}
				if (entSegProyecto.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Fecha en la que se ha realizado la ULTIMA modificacion registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Fecha en la que se ha realizado la ULTIMA modificacion registro";
					return "Fecha en la que se ha realizado la ULTIMA modificacion registro";
				}
				if (entSegProyecto.Fields.color_web == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para color_web";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para color_web";
					return "Valor de tipo String para color_web";
				}
				if (entSegProyecto.Fields.color_movil == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para color_movil";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para color_movil";
					return "Valor de tipo String para color_movil";
				}
				if (entSegProyecto.Fields.color_font == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para color_font";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para color_font";
					return "Valor de tipo String para color_font";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Error de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetError(entSegProyecto.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entSegProyecto.Fields.id_proyecto == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_proyecto";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_proyecto";
					return "int para id_proyecto";
				}
				if (entSegProyecto.Fields.nombre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para nombre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para nombre";
					return "String para nombre";
				}
				if (entSegProyecto.Fields.codigo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para codigo";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para codigo";
					return "String para codigo";
				}
				if (entSegProyecto.Fields.descripcion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para descripcion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para descripcion";
					return "String para descripcion";
				}
				if (entSegProyecto.Fields.fecinicio == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para fecinicio";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para fecinicio";
					return "DateTime para fecinicio";
				}
				if (entSegProyecto.Fields.fecfin == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para fecfin";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para fecfin";
					return "DateTime para fecfin";
				}
				if (entSegProyecto.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para apiestado";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para apiestado";
					return "String para apiestado";
				}
				if (entSegProyecto.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usucre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usucre";
					return "String para usucre";
				}
				if (entSegProyecto.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para feccre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para feccre";
					return "DateTime para feccre";
				}
				if (entSegProyecto.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usumod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usumod";
					return "String para usumod";
				}
				if (entSegProyecto.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para fecmod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para fecmod";
					return "DateTime para fecmod";
				}
				if (entSegProyecto.Fields.color_web == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para color_web";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para color_web";
					return "String para color_web";
				}
				if (entSegProyecto.Fields.color_movil == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para color_movil";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para color_movil";
					return "String para color_movil";
				}
				if (entSegProyecto.Fields.color_font == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para color_font";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para color_font";
					return "String para color_font";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Nombre de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entSegProyecto.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetColumn(entSegProyecto.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entSegProyecto.Fields.id_proyecto == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_proyecto:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_proyecto:";
					return "id_proyecto:";
				}
				if (entSegProyecto.Fields.nombre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "nombre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "nombre:";
					return "nombre:";
				}
				if (entSegProyecto.Fields.codigo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "codigo:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "codigo:";
					return "codigo:";
				}
				if (entSegProyecto.Fields.descripcion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "descripcion:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "descripcion:";
					return "descripcion:";
				}
				if (entSegProyecto.Fields.fecinicio == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "fecinicio:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "fecinicio:";
					return "fecinicio:";
				}
				if (entSegProyecto.Fields.fecfin == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "fecfin:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "fecfin:";
					return "fecfin:";
				}
				if (entSegProyecto.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "apiestado:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "apiestado:";
					return "apiestado:";
				}
				if (entSegProyecto.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usucre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usucre:";
					return "usucre:";
				}
				if (entSegProyecto.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "feccre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "feccre:";
					return "feccre:";
				}
				if (entSegProyecto.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usumod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usumod:";
					return "usumod:";
				}
				if (entSegProyecto.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "fecmod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "fecmod:";
					return "fecmod:";
				}
				if (entSegProyecto.Fields.color_web == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "color_web:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "color_web:";
					return "color_web:";
				}
				if (entSegProyecto.Fields.color_movil == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "color_movil:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "color_movil:";
					return "color_movil:";
				}
				if (entSegProyecto.Fields.color_font == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "color_font:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "color_font:";
					return "color_font:";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
	}
}

