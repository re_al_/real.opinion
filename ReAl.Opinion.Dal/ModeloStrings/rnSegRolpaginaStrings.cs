#region 
/***********************************************************************************************************
	NOMBRE:       rnSegRolpagina
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla seg_rolpagina

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Ine.Sice.Dal.Sociales; 
using Ine.Sice.PgConn.Sociales; 
using Ine.Sice.Dal.Sociales.Entidades;
using Ine.Sice.Dal.Sociales.Interface;
using System.Windows.Forms;
#endregion

namespace Ine.Sice.Dal.Sociales.Modelo
{
	public partial class rnSegRolpagina: inSegRolpagina
	{
		public const string strNombreLisForm = "Listado de seg_rolpagina";
		public const string strNombreDocForm = "Detalle de seg_rolpagina";
		public const string strNombreForm = "Registro de seg_rolpagina";
		/// <summary>
		/// 	Funcion que obtiene la Descripcion de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entSegRolpagina.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetDesc(entSegRolpagina.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entSegRolpagina.Fields.id_rolpagina == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para id_rolpagina";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para id_rolpagina";
					return "Valor de tipo int para id_rolpagina";
				}
				if (entSegRolpagina.Fields.id_rol == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para id_rol";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para id_rol";
					return "Valor de tipo int para id_rol";
				}
				if (entSegRolpagina.Fields.id_pagina == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para id_pagina";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para id_pagina";
					return "Valor de tipo int para id_pagina";
				}
				if (entSegRolpagina.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para apiestado";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para apiestado";
					return "Valor de tipo String para apiestado";
				}
				if (entSegRolpagina.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para usucre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para usucre";
					return "Valor de tipo String para usucre";
				}
				if (entSegRolpagina.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo DateTime para feccre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo DateTime para feccre";
					return "Valor de tipo DateTime para feccre";
				}
				if (entSegRolpagina.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para usumod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para usumod";
					return "Valor de tipo String para usumod";
				}
				if (entSegRolpagina.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo DateTime para fecmod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo DateTime para fecmod";
					return "Valor de tipo DateTime para fecmod";
				}
				if (entSegRolpagina.Fields.alta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para alta";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para alta";
					return "Valor de tipo int para alta";
				}
				if (entSegRolpagina.Fields.baja == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para baja";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para baja";
					return "Valor de tipo int para baja";
				}
				if (entSegRolpagina.Fields.modificacion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para modificacion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para modificacion";
					return "Valor de tipo int para modificacion";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Error de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entSegRolpagina.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetError(entSegRolpagina.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entSegRolpagina.Fields.id_rolpagina == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_rolpagina";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_rolpagina";
					return "int para id_rolpagina";
				}
				if (entSegRolpagina.Fields.id_rol == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_rol";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_rol";
					return "int para id_rol";
				}
				if (entSegRolpagina.Fields.id_pagina == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_pagina";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_pagina";
					return "int para id_pagina";
				}
				if (entSegRolpagina.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para apiestado";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para apiestado";
					return "String para apiestado";
				}
				if (entSegRolpagina.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usucre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usucre";
					return "String para usucre";
				}
				if (entSegRolpagina.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para feccre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para feccre";
					return "DateTime para feccre";
				}
				if (entSegRolpagina.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usumod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usumod";
					return "String para usumod";
				}
				if (entSegRolpagina.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para fecmod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para fecmod";
					return "DateTime para fecmod";
				}
				if (entSegRolpagina.Fields.alta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para alta";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para alta";
					return "int para alta";
				}
				if (entSegRolpagina.Fields.baja == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para baja";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para baja";
					return "int para baja";
				}
				if (entSegRolpagina.Fields.modificacion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para modificacion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para modificacion";
					return "int para modificacion";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Nombre de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entSegRolpagina.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetColumn(entSegRolpagina.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entSegRolpagina.Fields.id_rolpagina == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_rolpagina:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_rolpagina:";
					return "id_rolpagina:";
				}
				if (entSegRolpagina.Fields.id_rol == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_rol:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_rol:";
					return "id_rol:";
				}
				if (entSegRolpagina.Fields.id_pagina == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_pagina:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_pagina:";
					return "id_pagina:";
				}
				if (entSegRolpagina.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "apiestado:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "apiestado:";
					return "apiestado:";
				}
				if (entSegRolpagina.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usucre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usucre:";
					return "usucre:";
				}
				if (entSegRolpagina.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "feccre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "feccre:";
					return "feccre:";
				}
				if (entSegRolpagina.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usumod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usumod:";
					return "usumod:";
				}
				if (entSegRolpagina.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "fecmod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "fecmod:";
					return "fecmod:";
				}
				if (entSegRolpagina.Fields.alta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "alta:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "alta:";
					return "alta:";
				}
				if (entSegRolpagina.Fields.baja == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "baja:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "baja:";
					return "baja:";
				}
				if (entSegRolpagina.Fields.modificacion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "modificacion:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "modificacion:";
					return "modificacion:";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
	}
}

