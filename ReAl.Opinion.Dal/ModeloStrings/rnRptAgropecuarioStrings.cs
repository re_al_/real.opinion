#region 
/***********************************************************************************************************
	NOMBRE:       rnRptAgropecuario
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla rpt_agropecuario

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        22/10/2014  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Ine.Sice.PgConn.Sociales; 
using Ine.Sice.Dal.Sociales.Entidades;
using Ine.Sice.Dal.Sociales.Interface;
using System.Windows.Forms;
#endregion

namespace Ine.Sice.Dal.Sociales.Modelo
{
	public partial class rnRptAgropecuario: inRptAgropecuario
	{
		public const string strNombreLisForm = "Listado de rpt_agropecuario";
		public const string strNombreDocForm = "Detalle de rpt_agropecuario";
		public const string strNombreForm = "Registro de rpt_agropecuario";
		/// <summary>
		/// 	Funcion que obtiene la Descripcion de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetDesc(entRptAgropecuario.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entRptAgropecuario.Fields.gestion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para gestion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para gestion";
					return "Valor de tipo int para gestion";
				}
				if (entRptAgropecuario.Fields.mes == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para mes";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para mes";
					return "Valor de tipo int para mes";
				}
				if (entRptAgropecuario.Fields.foto == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para foto";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para foto";
					return "Valor de tipo String para foto";
				}
				if (entRptAgropecuario.Fields.id_departamento == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para id_departamento";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para id_departamento";
					return "Valor de tipo int para id_departamento";
				}
				if (entRptAgropecuario.Fields.id_upmipp == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para id_upmipp";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para id_upmipp";
					return "Valor de tipo int para id_upmipp";
				}
				if (entRptAgropecuario.Fields.id_provincia == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para id_provincia";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para id_provincia";
					return "Valor de tipo int para id_provincia";
				}
				if (entRptAgropecuario.Fields.id_municipio == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para id_municipio";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para id_municipio";
					return "Valor de tipo int para id_municipio";
				}
				if (entRptAgropecuario.Fields.id_listado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para id_listado";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para id_listado";
					return "Valor de tipo int para id_listado";
				}
				if (entRptAgropecuario.Fields.id_boleta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para id_boleta";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para id_boleta";
					return "Valor de tipo int para id_boleta";
				}
				if (entRptAgropecuario.Fields.id_persona == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para id_persona";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para id_persona";
					return "Valor de tipo int para id_persona";
				}
				if (entRptAgropecuario.Fields.id_movimiento == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para id_movimiento";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para id_movimiento";
					return "Valor de tipo int para id_movimiento";
				}
				if (entRptAgropecuario.Fields.departamento == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para departamento";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para departamento";
					return "Valor de tipo String para departamento";
				}
				if (entRptAgropecuario.Fields.provincia == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para provincia";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para provincia";
					return "Valor de tipo String para provincia";
				}
				if (entRptAgropecuario.Fields.municipio == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para municipio";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para municipio";
					return "Valor de tipo String para municipio";
				}
				if (entRptAgropecuario.Fields.comunidad == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para comunidad";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para comunidad";
					return "Valor de tipo String para comunidad";
				}
				if (entRptAgropecuario.Fields.login == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para login";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para login";
					return "Valor de tipo String para login";
				}
				if (entRptAgropecuario.Fields.es_reemplazo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para es_reemplazo";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para es_reemplazo";
					return "Valor de tipo String para es_reemplazo";
				}
				if (entRptAgropecuario.Fields.posible_reemplazo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para posible_reemplazo";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para posible_reemplazo";
					return "Valor de tipo String para posible_reemplazo";
				}
				if (entRptAgropecuario.Fields.producto_boleta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para producto_boleta";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para producto_boleta";
					return "Valor de tipo String para producto_boleta";
				}
				if (entRptAgropecuario.Fields.producto == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para producto";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para producto";
					return "Valor de tipo String para producto";
				}
				if (entRptAgropecuario.Fields.codigo_producto == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para codigo_producto";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para codigo_producto";
					return "Valor de tipo String para codigo_producto";
				}
				if (entRptAgropecuario.Fields.codigo_ccp == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para codigo_ccp";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para codigo_ccp";
					return "Valor de tipo String para codigo_ccp";
				}
				if (entRptAgropecuario.Fields.informante == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para informante";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para informante";
					return "Valor de tipo String para informante";
				}
				if (entRptAgropecuario.Fields.celular == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para celular";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para celular";
					return "Valor de tipo String para celular";
				}
				if (entRptAgropecuario.Fields.direccion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para direccion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para direccion";
					return "Valor de tipo String para direccion";
				}
				if (entRptAgropecuario.Fields.carac_adic == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para carac_adic";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para carac_adic";
					return "Valor de tipo String para carac_adic";
				}
				if (entRptAgropecuario.Fields.variedad_tipo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para variedad_tipo";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para variedad_tipo";
					return "Valor de tipo String para variedad_tipo";
				}
				if (entRptAgropecuario.Fields.talla == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para talla";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para talla";
					return "Valor de tipo String para talla";
				}
				if (entRptAgropecuario.Fields.cantidad == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para cantidad";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para cantidad";
					return "Valor de tipo String para cantidad";
				}
				if (entRptAgropecuario.Fields.unidad == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para unidad";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para unidad";
					return "Valor de tipo String para unidad";
				}
				if (entRptAgropecuario.Fields.precio_unit == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para precio_unit";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para precio_unit";
					return "Valor de tipo String para precio_unit";
				}
				if (entRptAgropecuario.Fields.observaciones == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para observaciones";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para observaciones";
					return "Valor de tipo String para observaciones";
				}
				if (entRptAgropecuario.Fields.minfec == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo DateTime para minfec";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo DateTime para minfec";
					return "Valor de tipo DateTime para minfec";
				}
				if (entRptAgropecuario.Fields.maxfec == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo DateTime para maxfec";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo DateTime para maxfec";
					return "Valor de tipo DateTime para maxfec";
				}
				if (entRptAgropecuario.Fields.duracion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo Decimal para duracion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo Decimal para duracion";
					return "Valor de tipo Decimal para duracion";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Error de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetError(entRptAgropecuario.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entRptAgropecuario.Fields.gestion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para gestion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para gestion";
					return "int para gestion";
				}
				if (entRptAgropecuario.Fields.mes == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para mes";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para mes";
					return "int para mes";
				}
				if (entRptAgropecuario.Fields.foto == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para foto";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para foto";
					return "String para foto";
				}
				if (entRptAgropecuario.Fields.id_departamento == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_departamento";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_departamento";
					return "int para id_departamento";
				}
				if (entRptAgropecuario.Fields.id_upmipp == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_upmipp";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_upmipp";
					return "int para id_upmipp";
				}
				if (entRptAgropecuario.Fields.id_provincia == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_provincia";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_provincia";
					return "int para id_provincia";
				}
				if (entRptAgropecuario.Fields.id_municipio == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_municipio";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_municipio";
					return "int para id_municipio";
				}
				if (entRptAgropecuario.Fields.id_listado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_listado";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_listado";
					return "int para id_listado";
				}
				if (entRptAgropecuario.Fields.id_boleta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_boleta";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_boleta";
					return "int para id_boleta";
				}
				if (entRptAgropecuario.Fields.id_persona == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_persona";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_persona";
					return "int para id_persona";
				}
				if (entRptAgropecuario.Fields.id_movimiento == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_movimiento";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_movimiento";
					return "int para id_movimiento";
				}
				if (entRptAgropecuario.Fields.departamento == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para departamento";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para departamento";
					return "String para departamento";
				}
				if (entRptAgropecuario.Fields.provincia == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para provincia";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para provincia";
					return "String para provincia";
				}
				if (entRptAgropecuario.Fields.municipio == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para municipio";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para municipio";
					return "String para municipio";
				}
				if (entRptAgropecuario.Fields.comunidad == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para comunidad";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para comunidad";
					return "String para comunidad";
				}
				if (entRptAgropecuario.Fields.login == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para login";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para login";
					return "String para login";
				}
				if (entRptAgropecuario.Fields.es_reemplazo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para es_reemplazo";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para es_reemplazo";
					return "String para es_reemplazo";
				}
				if (entRptAgropecuario.Fields.posible_reemplazo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para posible_reemplazo";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para posible_reemplazo";
					return "String para posible_reemplazo";
				}
				if (entRptAgropecuario.Fields.producto_boleta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para producto_boleta";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para producto_boleta";
					return "String para producto_boleta";
				}
				if (entRptAgropecuario.Fields.producto == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para producto";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para producto";
					return "String para producto";
				}
				if (entRptAgropecuario.Fields.codigo_producto == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para codigo_producto";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para codigo_producto";
					return "String para codigo_producto";
				}
				if (entRptAgropecuario.Fields.codigo_ccp == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para codigo_ccp";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para codigo_ccp";
					return "String para codigo_ccp";
				}
				if (entRptAgropecuario.Fields.informante == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para informante";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para informante";
					return "String para informante";
				}
				if (entRptAgropecuario.Fields.celular == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para celular";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para celular";
					return "String para celular";
				}
				if (entRptAgropecuario.Fields.direccion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para direccion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para direccion";
					return "String para direccion";
				}
				if (entRptAgropecuario.Fields.carac_adic == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para carac_adic";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para carac_adic";
					return "String para carac_adic";
				}
				if (entRptAgropecuario.Fields.variedad_tipo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para variedad_tipo";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para variedad_tipo";
					return "String para variedad_tipo";
				}
				if (entRptAgropecuario.Fields.talla == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para talla";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para talla";
					return "String para talla";
				}
				if (entRptAgropecuario.Fields.cantidad == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para cantidad";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para cantidad";
					return "String para cantidad";
				}
				if (entRptAgropecuario.Fields.unidad == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para unidad";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para unidad";
					return "String para unidad";
				}
				if (entRptAgropecuario.Fields.precio_unit == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para precio_unit";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para precio_unit";
					return "String para precio_unit";
				}
				if (entRptAgropecuario.Fields.observaciones == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para observaciones";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para observaciones";
					return "String para observaciones";
				}
				if (entRptAgropecuario.Fields.minfec == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para minfec";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para minfec";
					return "DateTime para minfec";
				}
				if (entRptAgropecuario.Fields.maxfec == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para maxfec";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para maxfec";
					return "DateTime para maxfec";
				}
				if (entRptAgropecuario.Fields.duracion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Decimal para duracion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Decimal para duracion";
					return "Decimal para duracion";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Nombre de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entRptAgropecuario.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetColumn(entRptAgropecuario.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entRptAgropecuario.Fields.gestion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "gestion:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "gestion:";
					return "gestion:";
				}
				if (entRptAgropecuario.Fields.mes == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "mes:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "mes:";
					return "mes:";
				}
				if (entRptAgropecuario.Fields.foto == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "foto:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "foto:";
					return "foto:";
				}
				if (entRptAgropecuario.Fields.id_departamento == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_departamento:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_departamento:";
					return "id_departamento:";
				}
				if (entRptAgropecuario.Fields.id_upmipp == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_upmipp:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_upmipp:";
					return "id_upmipp:";
				}
				if (entRptAgropecuario.Fields.id_provincia == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_provincia:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_provincia:";
					return "id_provincia:";
				}
				if (entRptAgropecuario.Fields.id_municipio == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_municipio:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_municipio:";
					return "id_municipio:";
				}
				if (entRptAgropecuario.Fields.id_listado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_listado:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_listado:";
					return "id_listado:";
				}
				if (entRptAgropecuario.Fields.id_boleta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_boleta:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_boleta:";
					return "id_boleta:";
				}
				if (entRptAgropecuario.Fields.id_persona == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_persona:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_persona:";
					return "id_persona:";
				}
				if (entRptAgropecuario.Fields.id_movimiento == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_movimiento:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_movimiento:";
					return "id_movimiento:";
				}
				if (entRptAgropecuario.Fields.departamento == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "departamento:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "departamento:";
					return "departamento:";
				}
				if (entRptAgropecuario.Fields.provincia == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "provincia:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "provincia:";
					return "provincia:";
				}
				if (entRptAgropecuario.Fields.municipio == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "municipio:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "municipio:";
					return "municipio:";
				}
				if (entRptAgropecuario.Fields.comunidad == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "comunidad:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "comunidad:";
					return "comunidad:";
				}
				if (entRptAgropecuario.Fields.login == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "login:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "login:";
					return "login:";
				}
				if (entRptAgropecuario.Fields.es_reemplazo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "es_reemplazo:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "es_reemplazo:";
					return "es_reemplazo:";
				}
				if (entRptAgropecuario.Fields.posible_reemplazo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "posible_reemplazo:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "posible_reemplazo:";
					return "posible_reemplazo:";
				}
				if (entRptAgropecuario.Fields.producto_boleta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "producto_boleta:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "producto_boleta:";
					return "producto_boleta:";
				}
				if (entRptAgropecuario.Fields.producto == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "producto:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "producto:";
					return "producto:";
				}
				if (entRptAgropecuario.Fields.codigo_producto == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "codigo_producto:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "codigo_producto:";
					return "codigo_producto:";
				}
				if (entRptAgropecuario.Fields.codigo_ccp == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "codigo_ccp:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "codigo_ccp:";
					return "codigo_ccp:";
				}
				if (entRptAgropecuario.Fields.informante == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "informante:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "informante:";
					return "informante:";
				}
				if (entRptAgropecuario.Fields.celular == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "celular:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "celular:";
					return "celular:";
				}
				if (entRptAgropecuario.Fields.direccion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "direccion:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "direccion:";
					return "direccion:";
				}
				if (entRptAgropecuario.Fields.carac_adic == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "carac_adic:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "carac_adic:";
					return "carac_adic:";
				}
				if (entRptAgropecuario.Fields.variedad_tipo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "variedad_tipo:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "variedad_tipo:";
					return "variedad_tipo:";
				}
				if (entRptAgropecuario.Fields.talla == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "talla:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "talla:";
					return "talla:";
				}
				if (entRptAgropecuario.Fields.cantidad == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "cantidad:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "cantidad:";
					return "cantidad:";
				}
				if (entRptAgropecuario.Fields.unidad == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "unidad:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "unidad:";
					return "unidad:";
				}
				if (entRptAgropecuario.Fields.precio_unit == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "precio_unit:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "precio_unit:";
					return "precio_unit:";
				}
				if (entRptAgropecuario.Fields.observaciones == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "observaciones:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "observaciones:";
					return "observaciones:";
				}
				if (entRptAgropecuario.Fields.minfec == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "minfec:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "minfec:";
					return "minfec:";
				}
				if (entRptAgropecuario.Fields.maxfec == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "maxfec:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "maxfec:";
					return "maxfec:";
				}
				if (entRptAgropecuario.Fields.duracion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "duracion:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "duracion:";
					return "duracion:";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
	}
}

