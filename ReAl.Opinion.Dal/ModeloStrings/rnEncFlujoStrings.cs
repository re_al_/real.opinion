#region 
/***********************************************************************************************************
	NOMBRE:       rnEncFlujo
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla enc_flujo

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Ine.Sice.Dal.Sociales; 
using Ine.Sice.PgConn.Sociales; 
using Ine.Sice.Dal.Sociales.Entidades;
using Ine.Sice.Dal.Sociales.Interface;
using System.Windows.Forms;
#endregion

namespace Ine.Sice.Dal.Sociales.Modelo
{
	public partial class rnEncFlujo: inEncFlujo
	{
		public const string strNombreLisForm = "Listado de enc_flujo";
		public const string strNombreDocForm = "Detalle de enc_flujo";
		public const string strNombreForm = "Registro de enc_flujo";
		/// <summary>
		/// 	Funcion que obtiene la Descripcion de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entEncFlujo.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetDesc(entEncFlujo.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entEncFlujo.Fields.id_flujo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Identificador unico que representa al registro en la tabla";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Identificador unico que representa al registro en la tabla";
					return "Identificador unico que representa al registro en la tabla";
				}
				if (entEncFlujo.Fields.id_proyecto == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Identificador del proyecto al que pertenece el registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Identificador del proyecto al que pertenece el registro";
					return "Identificador del proyecto al que pertenece el registro";
				}
				if (entEncFlujo.Fields.id_pregunta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Almacena el Id de pregunta origen";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Almacena el Id de pregunta origen";
					return "Almacena el Id de pregunta origen";
				}
				if (entEncFlujo.Fields.id_pregunta_destino == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Almacena el Id de pregunta destino. -1 representa FIN DE NIVEL. -2 representa FIN DE ENTREVISTA";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Almacena el Id de pregunta destino. -1 representa FIN DE NIVEL. -2 representa FIN DE ENTREVISTA";
					return "Almacena el Id de pregunta destino. -1 representa FIN DE NIVEL. -2 representa FIN DE ENTREVISTA";
				}
				if (entEncFlujo.Fields.orden == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Es el orden de PRIORIDAD en el que va ha evuarse el flujo";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Es el orden de PRIORIDAD en el que va ha evuarse el flujo";
					return "Es el orden de PRIORIDAD en el que va ha evuarse el flujo";
				}
				if (entEncFlujo.Fields.regla == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Es la regla de evaluaciÃ³n para consistencia aplicado a la pregunta";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Es la regla de evaluaciÃ³n para consistencia aplicado a la pregunta";
					return "Es la regla de evaluaciÃ³n para consistencia aplicado a la pregunta";
				}
				if (entEncFlujo.Fields.rpn == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Por sus siglas en ingles (Reverse Polish Notation), es la notacion polaca inversa que se genera automaticamente a partir del campo regla";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Por sus siglas en ingles (Reverse Polish Notation), es la notacion polaca inversa que se genera automaticamente a partir del campo regla";
					return "Por sus siglas en ingles (Reverse Polish Notation), es la notacion polaca inversa que se genera automaticamente a partir del campo regla";
				}
				if (entEncFlujo.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Estado en el que se encuentra el registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Estado en el que se encuentra el registro";
					return "Estado en el que se encuentra el registro";
				}
				if (entEncFlujo.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Login o nombre de usuario que ha creado el registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Login o nombre de usuario que ha creado el registro";
					return "Login o nombre de usuario que ha creado el registro";
				}
				if (entEncFlujo.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Fecha de creacion del registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Fecha de creacion del registro";
					return "Fecha de creacion del registro";
				}
				if (entEncFlujo.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro";
					return "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro";
				}
				if (entEncFlujo.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Fecha en la que se ha realizado la ULTIMA modificacion registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Fecha en la que se ha realizado la ULTIMA modificacion registro";
					return "Fecha en la que se ha realizado la ULTIMA modificacion registro";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Error de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entEncFlujo.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetError(entEncFlujo.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entEncFlujo.Fields.id_flujo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_flujo";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_flujo";
					return "int para id_flujo";
				}
				if (entEncFlujo.Fields.id_proyecto == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_proyecto";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_proyecto";
					return "int para id_proyecto";
				}
				if (entEncFlujo.Fields.id_pregunta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_pregunta";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_pregunta";
					return "int para id_pregunta";
				}
				if (entEncFlujo.Fields.id_pregunta_destino == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_pregunta_destino";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_pregunta_destino";
					return "int para id_pregunta_destino";
				}
				if (entEncFlujo.Fields.orden == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para orden";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para orden";
					return "int para orden";
				}
				if (entEncFlujo.Fields.regla == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para regla";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para regla";
					return "String para regla";
				}
				if (entEncFlujo.Fields.rpn == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para rpn";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para rpn";
					return "String para rpn";
				}
				if (entEncFlujo.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para apiestado";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para apiestado";
					return "String para apiestado";
				}
				if (entEncFlujo.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usucre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usucre";
					return "String para usucre";
				}
				if (entEncFlujo.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para feccre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para feccre";
					return "DateTime para feccre";
				}
				if (entEncFlujo.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usumod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usumod";
					return "String para usumod";
				}
				if (entEncFlujo.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para fecmod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para fecmod";
					return "DateTime para fecmod";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Nombre de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entEncFlujo.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetColumn(entEncFlujo.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entEncFlujo.Fields.id_flujo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_flujo:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_flujo:";
					return "id_flujo:";
				}
				if (entEncFlujo.Fields.id_proyecto == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_proyecto:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_proyecto:";
					return "id_proyecto:";
				}
				if (entEncFlujo.Fields.id_pregunta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_pregunta:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_pregunta:";
					return "id_pregunta:";
				}
				if (entEncFlujo.Fields.id_pregunta_destino == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_pregunta_destino:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_pregunta_destino:";
					return "id_pregunta_destino:";
				}
				if (entEncFlujo.Fields.orden == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "orden:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "orden:";
					return "orden:";
				}
				if (entEncFlujo.Fields.regla == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "regla:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "regla:";
					return "regla:";
				}
				if (entEncFlujo.Fields.rpn == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "rpn:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "rpn:";
					return "rpn:";
				}
				if (entEncFlujo.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "apiestado:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "apiestado:";
					return "apiestado:";
				}
				if (entEncFlujo.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usucre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usucre:";
					return "usucre:";
				}
				if (entEncFlujo.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "feccre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "feccre:";
					return "feccre:";
				}
				if (entEncFlujo.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usumod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usumod:";
					return "usumod:";
				}
				if (entEncFlujo.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "fecmod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "fecmod:";
					return "fecmod:";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
	}
}

