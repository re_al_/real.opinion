#region 
/***********************************************************************************************************
	NOMBRE:       rnCatCatalogo
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla cat_catalogo

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        05/10/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Ine.Sice.Dal.Sociales; 
using Ine.Sice.PgConn.Sociales; 
using Ine.Sice.Dal.Sociales.Entidades;
using Ine.Sice.Dal.Sociales.Interface;
using System.Windows.Forms;
#endregion

namespace Ine.Sice.Dal.Sociales.Modelo
{
	public partial class rnCatCatalogo: inCatCatalogo
	{
		public const string strNombreLisForm = "Listado de cat_catalogo";
		public const string strNombreDocForm = "Detalle de cat_catalogo";
		public const string strNombreForm = "Registro de cat_catalogo";
		/// <summary>
		/// 	Funcion que obtiene la Descripcion de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entCatCatalogo.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetDesc(entCatCatalogo.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entCatCatalogo.Fields.id_catalogo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para id_catalogo";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para id_catalogo";
					return "Valor de tipo int para id_catalogo";
				}
				if (entCatCatalogo.Fields.catalogo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para catalogo";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para catalogo";
					return "Valor de tipo String para catalogo";
				}
				if (entCatCatalogo.Fields.codigo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para codigo";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para codigo";
					return "Valor de tipo String para codigo";
				}
				if (entCatCatalogo.Fields.descripcion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para descripcion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para descripcion";
					return "Valor de tipo String para descripcion";
				}
				if (entCatCatalogo.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para apiestado";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para apiestado";
					return "Valor de tipo String para apiestado";
				}
				if (entCatCatalogo.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para usucre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para usucre";
					return "Valor de tipo String para usucre";
				}
				if (entCatCatalogo.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo DateTime para feccre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo DateTime para feccre";
					return "Valor de tipo DateTime para feccre";
				}
				if (entCatCatalogo.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para usumod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para usumod";
					return "Valor de tipo String para usumod";
				}
				if (entCatCatalogo.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo DateTime para fecmod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo DateTime para fecmod";
					return "Valor de tipo DateTime para fecmod";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Error de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entCatCatalogo.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetError(entCatCatalogo.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entCatCatalogo.Fields.id_catalogo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_catalogo";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_catalogo";
					return "int para id_catalogo";
				}
				if (entCatCatalogo.Fields.catalogo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para catalogo";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para catalogo";
					return "String para catalogo";
				}
				if (entCatCatalogo.Fields.codigo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para codigo";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para codigo";
					return "String para codigo";
				}
				if (entCatCatalogo.Fields.descripcion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para descripcion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para descripcion";
					return "String para descripcion";
				}
				if (entCatCatalogo.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para apiestado";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para apiestado";
					return "String para apiestado";
				}
				if (entCatCatalogo.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usucre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usucre";
					return "String para usucre";
				}
				if (entCatCatalogo.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para feccre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para feccre";
					return "DateTime para feccre";
				}
				if (entCatCatalogo.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usumod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usumod";
					return "String para usumod";
				}
				if (entCatCatalogo.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para fecmod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para fecmod";
					return "DateTime para fecmod";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Nombre de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entCatCatalogo.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetColumn(entCatCatalogo.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entCatCatalogo.Fields.id_catalogo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_catalogo:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_catalogo:";
					return "id_catalogo:";
				}
				if (entCatCatalogo.Fields.catalogo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "catalogo:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "catalogo:";
					return "catalogo:";
				}
				if (entCatCatalogo.Fields.codigo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "codigo:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "codigo:";
					return "codigo:";
				}
				if (entCatCatalogo.Fields.descripcion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "descripcion:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "descripcion:";
					return "descripcion:";
				}
				if (entCatCatalogo.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "apiestado:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "apiestado:";
					return "apiestado:";
				}
				if (entCatCatalogo.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usucre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usucre:";
					return "usucre:";
				}
				if (entCatCatalogo.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "feccre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "feccre:";
					return "feccre:";
				}
				if (entCatCatalogo.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usumod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usumod:";
					return "usumod:";
				}
				if (entCatCatalogo.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "fecmod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "fecmod:";
					return "fecmod:";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
	}
}

