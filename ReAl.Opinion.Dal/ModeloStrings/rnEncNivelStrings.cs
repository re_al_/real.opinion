#region 
/***********************************************************************************************************
	NOMBRE:       rnEncNivel
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla enc_nivel

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Ine.Sice.Dal.Sociales; 
using Ine.Sice.PgConn.Sociales; 
using Ine.Sice.Dal.Sociales.Entidades;
using Ine.Sice.Dal.Sociales.Interface;
using System.Windows.Forms;
#endregion

namespace Ine.Sice.Dal.Sociales.Modelo
{
	public partial class rnEncNivel: inEncNivel
	{
		public const string strNombreLisForm = "Listado de enc_nivel";
		public const string strNombreDocForm = "Detalle de enc_nivel";
		public const string strNombreForm = "Registro de enc_nivel";
		/// <summary>
		/// 	Funcion que obtiene la Descripcion de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entEncNivel.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetDesc(entEncNivel.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entEncNivel.Fields.id_nivel == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Es el identificador unico que representa al registro en la tabla";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Es el identificador unico que representa al registro en la tabla";
					return "Es el identificador unico que representa al registro en la tabla";
				}
				if (entEncNivel.Fields.id_proyecto == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Es el identificador unico de la tabla seg_proyecto";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Es el identificador unico de la tabla seg_proyecto";
					return "Es el identificador unico de la tabla seg_proyecto";
				}
				if (entEncNivel.Fields.nivel == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Es el numero de Nivel";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Es el numero de Nivel";
					return "Es el numero de Nivel";
				}
				if (entEncNivel.Fields.descripcion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Es la descripcion del Nivel";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Es la descripcion del Nivel";
					return "Es la descripcion del Nivel";
				}
				if (entEncNivel.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Estado en el que se encuentra el registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Estado en el que se encuentra el registro";
					return "Estado en el que se encuentra el registro";
				}
				if (entEncNivel.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Login o nombre de usuario que ha creado el registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Login o nombre de usuario que ha creado el registro";
					return "Login o nombre de usuario que ha creado el registro";
				}
				if (entEncNivel.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Fecha de creacion del registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Fecha de creacion del registro";
					return "Fecha de creacion del registro";
				}
				if (entEncNivel.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro";
					return "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro";
				}
				if (entEncNivel.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Fecha en la que se ha realizado la ULTIMA modificacion registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Fecha en la que se ha realizado la ULTIMA modificacion registro";
					return "Fecha en la que se ha realizado la ULTIMA modificacion registro";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Error de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entEncNivel.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetError(entEncNivel.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entEncNivel.Fields.id_nivel == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_nivel";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_nivel";
					return "int para id_nivel";
				}
				if (entEncNivel.Fields.id_proyecto == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_proyecto";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_proyecto";
					return "int para id_proyecto";
				}
				if (entEncNivel.Fields.nivel == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para nivel";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para nivel";
					return "int para nivel";
				}
				if (entEncNivel.Fields.descripcion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para descripcion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para descripcion";
					return "String para descripcion";
				}
				if (entEncNivel.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para apiestado";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para apiestado";
					return "String para apiestado";
				}
				if (entEncNivel.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usucre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usucre";
					return "String para usucre";
				}
				if (entEncNivel.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para feccre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para feccre";
					return "DateTime para feccre";
				}
				if (entEncNivel.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usumod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usumod";
					return "String para usumod";
				}
				if (entEncNivel.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para fecmod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para fecmod";
					return "DateTime para fecmod";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Nombre de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entEncNivel.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetColumn(entEncNivel.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entEncNivel.Fields.id_nivel == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_nivel:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_nivel:";
					return "id_nivel:";
				}
				if (entEncNivel.Fields.id_proyecto == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_proyecto:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_proyecto:";
					return "id_proyecto:";
				}
				if (entEncNivel.Fields.nivel == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "nivel:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "nivel:";
					return "nivel:";
				}
				if (entEncNivel.Fields.descripcion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "descripcion:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "descripcion:";
					return "descripcion:";
				}
				if (entEncNivel.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "apiestado:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "apiestado:";
					return "apiestado:";
				}
				if (entEncNivel.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usucre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usucre:";
					return "usucre:";
				}
				if (entEncNivel.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "feccre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "feccre:";
					return "feccre:";
				}
				if (entEncNivel.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usumod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usumod:";
					return "usumod:";
				}
				if (entEncNivel.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "fecmod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "fecmod:";
					return "fecmod:";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
	}
}

