#region 
/***********************************************************************************************************
	NOMBRE:       rnOpeMovimiento
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla ope_movimiento

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        22/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Ine.Sice.Dal.Sociales; 
using Ine.Sice.PgConn.Sociales; 
using Ine.Sice.Dal.Sociales.Entidades;
using Ine.Sice.Dal.Sociales.Interface;
using System.Windows.Forms;
#endregion

namespace Ine.Sice.Dal.Sociales.Modelo
{
	public partial class rnOpeMovimiento: inOpeMovimiento
	{
		public const string strNombreLisForm = "Listado de ope_movimiento";
		public const string strNombreDocForm = "Detalle de ope_movimiento";
		public const string strNombreForm = "Registro de ope_movimiento";
		/// <summary>
		/// 	Funcion que obtiene la Descripcion de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entOpeMovimiento.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetDesc(entOpeMovimiento.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entOpeMovimiento.Fields.id_movimiento == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Es el identificador unico que representa al registro en la tabla";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Es el identificador unico que representa al registro en la tabla";
					return "Es el identificador unico que representa al registro en la tabla";
				}
				if (entOpeMovimiento.Fields.id_asignacion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Es el identificador unico de la tabla ope_asignacion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Es el identificador unico de la tabla ope_asignacion";
					return "Es el identificador unico de la tabla ope_asignacion";
				}
				if (entOpeMovimiento.Fields.id_upm == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Es el identificador unico de la tabla cat_upm";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Es el identificador unico de la tabla cat_upm";
					return "Es el identificador unico de la tabla cat_upm";
				}
				if (entOpeMovimiento.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Estado en el que se encuentra el registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Estado en el que se encuentra el registro";
					return "Estado en el que se encuentra el registro";
				}
				if (entOpeMovimiento.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Login o nombre de usuario que ha creado el registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Login o nombre de usuario que ha creado el registro";
					return "Login o nombre de usuario que ha creado el registro";
				}
				if (entOpeMovimiento.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Fecha de creacion del registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Fecha de creacion del registro";
					return "Fecha de creacion del registro";
				}
				if (entOpeMovimiento.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro";
					return "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro";
				}
				if (entOpeMovimiento.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Fecha en la que se ha realizado la ULTIMA modificacion registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Fecha en la que se ha realizado la ULTIMA modificacion registro";
					return "Fecha en la que se ha realizado la ULTIMA modificacion registro";
				}
				if (entOpeMovimiento.Fields.gestion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para gestion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para gestion";
					return "Valor de tipo int para gestion";
				}
				if (entOpeMovimiento.Fields.mes == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para mes";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para mes";
					return "Valor de tipo int para mes";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Error de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entOpeMovimiento.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetError(entOpeMovimiento.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entOpeMovimiento.Fields.id_movimiento == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_movimiento";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_movimiento";
					return "int para id_movimiento";
				}
				if (entOpeMovimiento.Fields.id_asignacion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_asignacion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_asignacion";
					return "int para id_asignacion";
				}
				if (entOpeMovimiento.Fields.id_upm == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_upm";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_upm";
					return "int para id_upm";
				}
				if (entOpeMovimiento.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para apiestado";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para apiestado";
					return "String para apiestado";
				}
				if (entOpeMovimiento.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usucre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usucre";
					return "String para usucre";
				}
				if (entOpeMovimiento.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para feccre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para feccre";
					return "DateTime para feccre";
				}
				if (entOpeMovimiento.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usumod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usumod";
					return "String para usumod";
				}
				if (entOpeMovimiento.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para fecmod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para fecmod";
					return "DateTime para fecmod";
				}
				if (entOpeMovimiento.Fields.gestion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para gestion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para gestion";
					return "int para gestion";
				}
				if (entOpeMovimiento.Fields.mes == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para mes";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para mes";
					return "int para mes";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Nombre de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entOpeMovimiento.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetColumn(entOpeMovimiento.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entOpeMovimiento.Fields.id_movimiento == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_movimiento:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_movimiento:";
					return "id_movimiento:";
				}
				if (entOpeMovimiento.Fields.id_asignacion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_asignacion:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_asignacion:";
					return "id_asignacion:";
				}
				if (entOpeMovimiento.Fields.id_upm == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_upm:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_upm:";
					return "id_upm:";
				}
				if (entOpeMovimiento.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "apiestado:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "apiestado:";
					return "apiestado:";
				}
				if (entOpeMovimiento.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usucre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usucre:";
					return "usucre:";
				}
				if (entOpeMovimiento.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "feccre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "feccre:";
					return "feccre:";
				}
				if (entOpeMovimiento.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usumod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usumod:";
					return "usumod:";
				}
				if (entOpeMovimiento.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "fecmod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "fecmod:";
					return "fecmod:";
				}
				if (entOpeMovimiento.Fields.gestion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "gestion:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "gestion:";
					return "gestion:";
				}
				if (entOpeMovimiento.Fields.mes == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "mes:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "mes:";
					return "mes:";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
	}
}

