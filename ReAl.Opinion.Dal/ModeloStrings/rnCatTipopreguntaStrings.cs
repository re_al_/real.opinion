#region 
/***********************************************************************************************************
	NOMBRE:       rnCatTipopregunta
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla cat_tipo_pregunta

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Ine.Sice.Dal.Sociales; 
using Ine.Sice.PgConn.Sociales; 
using Ine.Sice.Dal.Sociales.Entidades;
using Ine.Sice.Dal.Sociales.Interface;
using System.Windows.Forms;
#endregion

namespace Ine.Sice.Dal.Sociales.Modelo
{
	public partial class rnCatTipopregunta: inCatTipopregunta
	{
		public const string strNombreLisForm = "Listado de cat_tipo_pregunta";
		public const string strNombreDocForm = "Detalle de cat_tipo_pregunta";
		public const string strNombreForm = "Registro de cat_tipo_pregunta";
		/// <summary>
		/// 	Funcion que obtiene la Descripcion de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetDesc(entCatTipopregunta.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entCatTipopregunta.Fields.id_tipo_pregunta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Es el identificador unico que representa al registro en la tabla";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Es el identificador unico que representa al registro en la tabla";
					return "Es el identificador unico que representa al registro en la tabla";
				}
				if (entCatTipopregunta.Fields.tipo_pregunta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Es el tipo de pregunta UNICO";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Es el tipo de pregunta UNICO";
					return "Es el tipo de pregunta UNICO";
				}
				if (entCatTipopregunta.Fields.descripcion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Es la descripcion del tipo de pregunta";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Es la descripcion del tipo de pregunta";
					return "Es la descripcion del tipo de pregunta";
				}
				if (entCatTipopregunta.Fields.respuesta_valor == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Es el tipo predominante para la evaluacion del salto en la pregunta. Puede ser RESPUESTA o CODIGO";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Es el tipo predominante para la evaluacion del salto en la pregunta. Puede ser RESPUESTA o CODIGO";
					return "Es el tipo predominante para la evaluacion del salto en la pregunta. Puede ser RESPUESTA o CODIGO";
				}
				if (entCatTipopregunta.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Estado en el que se encuentra el registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Estado en el que se encuentra el registro";
					return "Estado en el que se encuentra el registro";
				}
				if (entCatTipopregunta.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Login o nombre de usuario que ha creado el registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Login o nombre de usuario que ha creado el registro";
					return "Login o nombre de usuario que ha creado el registro";
				}
				if (entCatTipopregunta.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Fecha de creacion del registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Fecha de creacion del registro";
					return "Fecha de creacion del registro";
				}
				if (entCatTipopregunta.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro";
					return "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro";
				}
				if (entCatTipopregunta.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Fecha en la que se ha realizado la ULTIMA modificacion registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Fecha en la que se ha realizado la ULTIMA modificacion registro";
					return "Fecha en la que se ha realizado la ULTIMA modificacion registro";
				}
				if (entCatTipopregunta.Fields.codigo_activo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para codigo_activo";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para codigo_activo";
					return "Valor de tipo String para codigo_activo";
				}
				if (entCatTipopregunta.Fields.exportar_codigo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para exportar_codigo";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para exportar_codigo";
					return "Valor de tipo int para exportar_codigo";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Error de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetError(entCatTipopregunta.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entCatTipopregunta.Fields.id_tipo_pregunta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_tipo_pregunta";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_tipo_pregunta";
					return "int para id_tipo_pregunta";
				}
				if (entCatTipopregunta.Fields.tipo_pregunta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para tipo_pregunta";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para tipo_pregunta";
					return "String para tipo_pregunta";
				}
				if (entCatTipopregunta.Fields.descripcion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para descripcion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para descripcion";
					return "String para descripcion";
				}
				if (entCatTipopregunta.Fields.respuesta_valor == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para respuesta_valor";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para respuesta_valor";
					return "String para respuesta_valor";
				}
				if (entCatTipopregunta.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para apiestado";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para apiestado";
					return "String para apiestado";
				}
				if (entCatTipopregunta.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usucre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usucre";
					return "String para usucre";
				}
				if (entCatTipopregunta.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para feccre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para feccre";
					return "DateTime para feccre";
				}
				if (entCatTipopregunta.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usumod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usumod";
					return "String para usumod";
				}
				if (entCatTipopregunta.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para fecmod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para fecmod";
					return "DateTime para fecmod";
				}
				if (entCatTipopregunta.Fields.codigo_activo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para codigo_activo";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para codigo_activo";
					return "String para codigo_activo";
				}
				if (entCatTipopregunta.Fields.exportar_codigo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para exportar_codigo";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para exportar_codigo";
					return "int para exportar_codigo";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Nombre de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entCatTipopregunta.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetColumn(entCatTipopregunta.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entCatTipopregunta.Fields.id_tipo_pregunta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_tipo_pregunta:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_tipo_pregunta:";
					return "id_tipo_pregunta:";
				}
				if (entCatTipopregunta.Fields.tipo_pregunta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "tipo_pregunta:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "tipo_pregunta:";
					return "tipo_pregunta:";
				}
				if (entCatTipopregunta.Fields.descripcion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "descripcion:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "descripcion:";
					return "descripcion:";
				}
				if (entCatTipopregunta.Fields.respuesta_valor == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "respuesta_valor:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "respuesta_valor:";
					return "respuesta_valor:";
				}
				if (entCatTipopregunta.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "apiestado:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "apiestado:";
					return "apiestado:";
				}
				if (entCatTipopregunta.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usucre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usucre:";
					return "usucre:";
				}
				if (entCatTipopregunta.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "feccre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "feccre:";
					return "feccre:";
				}
				if (entCatTipopregunta.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usumod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usumod:";
					return "usumod:";
				}
				if (entCatTipopregunta.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "fecmod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "fecmod:";
					return "fecmod:";
				}
				if (entCatTipopregunta.Fields.codigo_activo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "codigo_activo:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "codigo_activo:";
					return "codigo_activo:";
				}
				if (entCatTipopregunta.Fields.exportar_codigo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "exportar_codigo:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "exportar_codigo:";
					return "exportar_codigo:";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
	}
}

