#region 
/***********************************************************************************************************
	NOMBRE:       rnEncRespuesta
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla enc_respuesta

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Ine.Sice.Dal.Sociales; 
using Ine.Sice.PgConn.Sociales; 
using Ine.Sice.Dal.Sociales.Entidades;
using Ine.Sice.Dal.Sociales.Interface;
using System.Windows.Forms;
#endregion

namespace Ine.Sice.Dal.Sociales.Modelo
{
	public partial class rnEncRespuesta: inEncRespuesta
	{
		public const string strNombreLisForm = "Listado de enc_respuesta";
		public const string strNombreDocForm = "Detalle de enc_respuesta";
		public const string strNombreForm = "Registro de enc_respuesta";
		/// <summary>
		/// 	Funcion que obtiene la Descripcion de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entEncRespuesta.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetDesc(entEncRespuesta.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entEncRespuesta.Fields.id_respuesta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Representa al identificador unica del registro en la tabla";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Representa al identificador unica del registro en la tabla";
					return "Representa al identificador unica del registro en la tabla";
				}
				if (entEncRespuesta.Fields.id_pregunta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Representa al identificador de la tabla enc_pregunta al que esta asociado este registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Representa al identificador de la tabla enc_pregunta al que esta asociado este registro";
					return "Representa al identificador de la tabla enc_pregunta al que esta asociado este registro";
				}
				if (entEncRespuesta.Fields.codigo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Codigo de la respuesta";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Codigo de la respuesta";
					return "Codigo de la respuesta";
				}
				if (entEncRespuesta.Fields.respuesta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Texto de la respuesta";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Texto de la respuesta";
					return "Texto de la respuesta";
				}
				if (entEncRespuesta.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Estado en el que se encuentra el registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Estado en el que se encuentra el registro";
					return "Estado en el que se encuentra el registro";
				}
				if (entEncRespuesta.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Login o nombre de usuario que ha creado el registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Login o nombre de usuario que ha creado el registro";
					return "Login o nombre de usuario que ha creado el registro";
				}
				if (entEncRespuesta.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Fecha de creacion del registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Fecha de creacion del registro";
					return "Fecha de creacion del registro";
				}
				if (entEncRespuesta.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro";
					return "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro";
				}
				if (entEncRespuesta.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Fecha en la que se ha realizado la ULTIMA modificacion registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Fecha en la que se ha realizado la ULTIMA modificacion registro";
					return "Fecha en la que se ha realizado la ULTIMA modificacion registro";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Error de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entEncRespuesta.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetError(entEncRespuesta.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entEncRespuesta.Fields.id_respuesta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Int64 para id_respuesta";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Int64 para id_respuesta";
					return "Int64 para id_respuesta";
				}
				if (entEncRespuesta.Fields.id_pregunta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Int64 para id_pregunta";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Int64 para id_pregunta";
					return "Int64 para id_pregunta";
				}
				if (entEncRespuesta.Fields.codigo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para codigo";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para codigo";
					return "String para codigo";
				}
				if (entEncRespuesta.Fields.respuesta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para respuesta";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para respuesta";
					return "String para respuesta";
				}
				if (entEncRespuesta.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para apiestado";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para apiestado";
					return "String para apiestado";
				}
				if (entEncRespuesta.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usucre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usucre";
					return "String para usucre";
				}
				if (entEncRespuesta.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para feccre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para feccre";
					return "DateTime para feccre";
				}
				if (entEncRespuesta.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usumod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usumod";
					return "String para usumod";
				}
				if (entEncRespuesta.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para fecmod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para fecmod";
					return "DateTime para fecmod";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Nombre de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entEncRespuesta.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetColumn(entEncRespuesta.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entEncRespuesta.Fields.id_respuesta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_respuesta:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_respuesta:";
					return "id_respuesta:";
				}
				if (entEncRespuesta.Fields.id_pregunta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_pregunta:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_pregunta:";
					return "id_pregunta:";
				}
				if (entEncRespuesta.Fields.codigo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "codigo:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "codigo:";
					return "codigo:";
				}
				if (entEncRespuesta.Fields.respuesta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "respuesta:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "respuesta:";
					return "respuesta:";
				}
				if (entEncRespuesta.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "apiestado:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "apiestado:";
					return "apiestado:";
				}
				if (entEncRespuesta.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usucre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usucre:";
					return "usucre:";
				}
				if (entEncRespuesta.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "feccre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "feccre:";
					return "feccre:";
				}
				if (entEncRespuesta.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usumod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usumod:";
					return "usumod:";
				}
				if (entEncRespuesta.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "fecmod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "fecmod:";
					return "fecmod:";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
	}
}

