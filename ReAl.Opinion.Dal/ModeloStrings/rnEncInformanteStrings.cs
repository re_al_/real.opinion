#region 
/***********************************************************************************************************
	NOMBRE:       rnEncInformante
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla enc_informante

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Ine.Sice.Dal.Sociales; 
using Ine.Sice.PgConn.Sociales; 
using Ine.Sice.Dal.Sociales.Entidades;
using Ine.Sice.Dal.Sociales.Interface;
using System.Windows.Forms;
#endregion

namespace Ine.Sice.Dal.Sociales.Modelo
{
	public partial class rnEncInformante: inEncInformante
	{
		public const string strNombreLisForm = "Listado de enc_informante";
		public const string strNombreDocForm = "Detalle de enc_informante";
		public const string strNombreForm = "Registro de enc_informante";
		/// <summary>
		/// 	Funcion que obtiene la Descripcion de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entEncInformante.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetDesc(entEncInformante.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entEncInformante.Fields.id_informante == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Identificador unico que representa al registro en la tabla";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Identificador unico que representa al registro en la tabla";
					return "Identificador unico que representa al registro en la tabla";
				}
				if (entEncInformante.Fields.id_tab_informante == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Identificador unico que representa al registro en la tabla en el Dispositivo";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Identificador unico que representa al registro en la tabla en el Dispositivo";
					return "Identificador unico que representa al registro en la tabla en el Dispositivo";
				}
				if (entEncInformante.Fields.id_informante_padre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Identificador que representa al registro padre en la misma tabla";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Identificador que representa al registro padre en la misma tabla";
					return "Identificador que representa al registro padre en la misma tabla";
				}
				if (entEncInformante.Fields.id_tab_informante_padre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Identificador que representa al registro padre en la misma tabla en el Dispositivo";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Identificador que representa al registro padre en la misma tabla en el Dispositivo";
					return "Identificador que representa al registro padre en la misma tabla en el Dispositivo";
				}
				if (entEncInformante.Fields.id_upm == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para id_upm";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para id_upm";
					return "Valor de tipo int para id_upm";
				}
				if (entEncInformante.Fields.id_nivel == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Es el identificador unico de la tabla enc_nivel";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Es el identificador unico de la tabla enc_nivel";
					return "Es el identificador unico de la tabla enc_nivel";
				}
				if (entEncInformante.Fields.id_movimiento == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Es el identificador unico de la tabla enc_movimiento";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Es el identificador unico de la tabla enc_movimiento";
					return "Es el identificador unico de la tabla enc_movimiento";
				}
				if (entEncInformante.Fields.latitud == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Latitud del punto CERO de la Capital de Departamento";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Latitud del punto CERO de la Capital de Departamento";
					return "Latitud del punto CERO de la Capital de Departamento";
				}
				if (entEncInformante.Fields.longitud == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Longitud del punto CERO de la Capital de Departamento";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Longitud del punto CERO de la Capital de Departamento";
					return "Longitud del punto CERO de la Capital de Departamento";
				}
				if (entEncInformante.Fields.codigo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Codigo del informante";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Codigo del informante";
					return "Codigo del informante";
				}
				if (entEncInformante.Fields.descripcion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Descripcion del informante";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Descripcion del informante";
					return "Descripcion del informante";
				}
				if (entEncInformante.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Estado en el que se encuentra el registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Estado en el que se encuentra el registro";
					return "Estado en el que se encuentra el registro";
				}
				if (entEncInformante.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Login o nombre de usuario que ha creado el registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Login o nombre de usuario que ha creado el registro";
					return "Login o nombre de usuario que ha creado el registro";
				}
				if (entEncInformante.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Fecha de creacion del registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Fecha de creacion del registro";
					return "Fecha de creacion del registro";
				}
				if (entEncInformante.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro";
					return "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro";
				}
				if (entEncInformante.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Fecha en la que se ha realizado la ULTIMA modificacion registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Fecha en la que se ha realizado la ULTIMA modificacion registro";
					return "Fecha en la que se ha realizado la ULTIMA modificacion registro";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Error de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entEncInformante.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetError(entEncInformante.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entEncInformante.Fields.id_informante == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Int64 para id_informante";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Int64 para id_informante";
					return "Int64 para id_informante";
				}
				if (entEncInformante.Fields.id_tab_informante == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_tab_informante";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_tab_informante";
					return "int para id_tab_informante";
				}
				if (entEncInformante.Fields.id_informante_padre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_informante_padre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_informante_padre";
					return "int para id_informante_padre";
				}
				if (entEncInformante.Fields.id_tab_informante_padre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_tab_informante_padre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_tab_informante_padre";
					return "int para id_tab_informante_padre";
				}
				if (entEncInformante.Fields.id_upm == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_upm";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_upm";
					return "int para id_upm";
				}
				if (entEncInformante.Fields.id_nivel == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_nivel";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_nivel";
					return "int para id_nivel";
				}
				if (entEncInformante.Fields.id_movimiento == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_movimiento";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_movimiento";
					return "int para id_movimiento";
				}
				if (entEncInformante.Fields.latitud == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Decimal para latitud";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Decimal para latitud";
					return "Decimal para latitud";
				}
				if (entEncInformante.Fields.longitud == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Decimal para longitud";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Decimal para longitud";
					return "Decimal para longitud";
				}
				if (entEncInformante.Fields.codigo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para codigo";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para codigo";
					return "String para codigo";
				}
				if (entEncInformante.Fields.descripcion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para descripcion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para descripcion";
					return "String para descripcion";
				}
				if (entEncInformante.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para apiestado";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para apiestado";
					return "String para apiestado";
				}
				if (entEncInformante.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usucre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usucre";
					return "String para usucre";
				}
				if (entEncInformante.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para feccre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para feccre";
					return "DateTime para feccre";
				}
				if (entEncInformante.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usumod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usumod";
					return "String para usumod";
				}
				if (entEncInformante.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para fecmod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para fecmod";
					return "DateTime para fecmod";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Nombre de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entEncInformante.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetColumn(entEncInformante.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entEncInformante.Fields.id_informante == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_informante:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_informante:";
					return "id_informante:";
				}
				if (entEncInformante.Fields.id_tab_informante == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_tab_informante:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_tab_informante:";
					return "id_tab_informante:";
				}
				if (entEncInformante.Fields.id_informante_padre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_informante_padre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_informante_padre:";
					return "id_informante_padre:";
				}
				if (entEncInformante.Fields.id_tab_informante_padre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_tab_informante_padre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_tab_informante_padre:";
					return "id_tab_informante_padre:";
				}
				if (entEncInformante.Fields.id_upm == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_upm:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_upm:";
					return "id_upm:";
				}
				if (entEncInformante.Fields.id_nivel == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_nivel:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_nivel:";
					return "id_nivel:";
				}
				if (entEncInformante.Fields.id_movimiento == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_movimiento:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_movimiento:";
					return "id_movimiento:";
				}
				if (entEncInformante.Fields.latitud == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "latitud:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "latitud:";
					return "latitud:";
				}
				if (entEncInformante.Fields.longitud == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "longitud:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "longitud:";
					return "longitud:";
				}
				if (entEncInformante.Fields.codigo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "codigo:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "codigo:";
					return "codigo:";
				}
				if (entEncInformante.Fields.descripcion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "descripcion:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "descripcion:";
					return "descripcion:";
				}
				if (entEncInformante.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "apiestado:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "apiestado:";
					return "apiestado:";
				}
				if (entEncInformante.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usucre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usucre:";
					return "usucre:";
				}
				if (entEncInformante.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "feccre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "feccre:";
					return "feccre:";
				}
				if (entEncInformante.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usumod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usumod:";
					return "usumod:";
				}
				if (entEncInformante.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "fecmod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "fecmod:";
					return "fecmod:";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
	}
}

