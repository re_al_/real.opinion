#region 
/***********************************************************************************************************
	NOMBRE:       rnEncSeccion
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla enc_seccion

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Ine.Sice.Dal.Sociales; 
using Ine.Sice.PgConn.Sociales; 
using Ine.Sice.Dal.Sociales.Entidades;
using Ine.Sice.Dal.Sociales.Interface;
using System.Windows.Forms;
#endregion

namespace Ine.Sice.Dal.Sociales.Modelo
{
	public partial class rnEncSeccion: inEncSeccion
	{
		public const string strNombreLisForm = "Listado de enc_seccion";
		public const string strNombreDocForm = "Detalle de enc_seccion";
		public const string strNombreForm = "Registro de enc_seccion";
		/// <summary>
		/// 	Funcion que obtiene la Descripcion de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entEncSeccion.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetDesc(entEncSeccion.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entEncSeccion.Fields.id_seccion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Identificador de la seccion al que pertenece el registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Identificador de la seccion al que pertenece el registro";
					return "Identificador de la seccion al que pertenece el registro";
				}
				if (entEncSeccion.Fields.id_proyecto == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Identificador del proyecto al que pertenece el registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Identificador del proyecto al que pertenece el registro";
					return "Identificador del proyecto al que pertenece el registro";
				}
				if (entEncSeccion.Fields.id_nivel == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para id_nivel";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para id_nivel";
					return "Valor de tipo int para id_nivel";
				}
				if (entEncSeccion.Fields.codigo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Codigo de seccion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Codigo de seccion";
					return "Codigo de seccion";
				}
				if (entEncSeccion.Fields.seccion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Texto o nombre de seccion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Texto o nombre de seccion";
					return "Texto o nombre de seccion";
				}
				if (entEncSeccion.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Describe el estado en el que se encuentra un determinado registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Describe el estado en el que se encuentra un determinado registro";
					return "Describe el estado en el que se encuentra un determinado registro";
				}
				if (entEncSeccion.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Describe el usuario que creo un determinado un registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Describe el usuario que creo un determinado un registro";
					return "Describe el usuario que creo un determinado un registro";
				}
				if (entEncSeccion.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Describe la fecha de creaciÃ³n de un determinado registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Describe la fecha de creaciÃ³n de un determinado registro";
					return "Describe la fecha de creaciÃ³n de un determinado registro";
				}
				if (entEncSeccion.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Describe el usuario que modifico un determinado registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Describe el usuario que modifico un determinado registro";
					return "Describe el usuario que modifico un determinado registro";
				}
				if (entEncSeccion.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Describe la fecha de modificacciÃ³n de determinado un registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Describe la fecha de modificacciÃ³n de determinado un registro";
					return "Describe la fecha de modificacciÃ³n de determinado un registro";
				}
				if (entEncSeccion.Fields.abierta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para abierta";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para abierta";
					return "Valor de tipo int para abierta";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Error de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entEncSeccion.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetError(entEncSeccion.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entEncSeccion.Fields.id_seccion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_seccion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_seccion";
					return "int para id_seccion";
				}
				if (entEncSeccion.Fields.id_proyecto == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_proyecto";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_proyecto";
					return "int para id_proyecto";
				}
				if (entEncSeccion.Fields.id_nivel == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_nivel";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_nivel";
					return "int para id_nivel";
				}
				if (entEncSeccion.Fields.codigo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para codigo";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para codigo";
					return "String para codigo";
				}
				if (entEncSeccion.Fields.seccion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para seccion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para seccion";
					return "String para seccion";
				}
				if (entEncSeccion.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para apiestado";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para apiestado";
					return "String para apiestado";
				}
				if (entEncSeccion.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usucre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usucre";
					return "String para usucre";
				}
				if (entEncSeccion.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para feccre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para feccre";
					return "DateTime para feccre";
				}
				if (entEncSeccion.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usumod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usumod";
					return "String para usumod";
				}
				if (entEncSeccion.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para fecmod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para fecmod";
					return "DateTime para fecmod";
				}
				if (entEncSeccion.Fields.abierta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para abierta";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para abierta";
					return "int para abierta";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Nombre de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entEncSeccion.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetColumn(entEncSeccion.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entEncSeccion.Fields.id_seccion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_seccion:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_seccion:";
					return "id_seccion:";
				}
				if (entEncSeccion.Fields.id_proyecto == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_proyecto:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_proyecto:";
					return "id_proyecto:";
				}
				if (entEncSeccion.Fields.id_nivel == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_nivel:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_nivel:";
					return "id_nivel:";
				}
				if (entEncSeccion.Fields.codigo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "codigo:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "codigo:";
					return "codigo:";
				}
				if (entEncSeccion.Fields.seccion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "seccion:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "seccion:";
					return "seccion:";
				}
				if (entEncSeccion.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "apiestado:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "apiestado:";
					return "apiestado:";
				}
				if (entEncSeccion.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usucre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usucre:";
					return "usucre:";
				}
				if (entEncSeccion.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "feccre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "feccre:";
					return "feccre:";
				}
				if (entEncSeccion.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usumod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usumod:";
					return "usumod:";
				}
				if (entEncSeccion.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "fecmod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "fecmod:";
					return "fecmod:";
				}
				if (entEncSeccion.Fields.abierta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "abierta:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "abierta:";
					return "abierta:";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
	}
}

