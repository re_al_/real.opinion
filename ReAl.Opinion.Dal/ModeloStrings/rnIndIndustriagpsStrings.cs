#region 
/***********************************************************************************************************
	NOMBRE:       rnIndIndustriagps
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla ind_industriagps

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/05/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

using Ine.Sice.Dal.Sociales; 
using Ine.Sice.Dal.Sociales.Entidades;
using Ine.Sice.Dal.Sociales.Interface;
using System.Windows.Forms;
#endregion

namespace Ine.Sice.Dal.Sociales.Modelo
{
	public partial class rnIndIndustriagps: inIndIndustriagps
	{
		public const string strNombreLisForm = "Listado de ind_industriagps";
		public const string strNombreDocForm = "Detalle de ind_industriagps";
		public const string strNombreForm = "Registro de ind_industriagps";
		/// <summary>
		/// 	Funcion que obtiene la Descripcion de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetDesc(entIndIndustriagps.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entIndIndustriagps.Fields.gestion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para gestion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para gestion";
					return "Valor de tipo int para gestion";
				}
				if (entIndIndustriagps.Fields.mes == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para mes";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para mes";
					return "Valor de tipo int para mes";
				}
				if (entIndIndustriagps.Fields.id_gps == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para id_gps";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para id_gps";
					return "Valor de tipo int para id_gps";
				}
				if (entIndIndustriagps.Fields.id_industria == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para id_industria";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para id_industria";
					return "Valor de tipo int para id_industria";
				}
				if (entIndIndustriagps.Fields.latitud == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo Decimal para latitud";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo Decimal para latitud";
					return "Valor de tipo Decimal para latitud";
				}
				if (entIndIndustriagps.Fields.longitud == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo Decimal para longitud";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo Decimal para longitud";
					return "Valor de tipo Decimal para longitud";
				}
				if (entIndIndustriagps.Fields.hora_control == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo DateTime para hora_control";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo DateTime para hora_control";
					return "Valor de tipo DateTime para hora_control";
				}
				if (entIndIndustriagps.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para apiestado";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para apiestado";
					return "Valor de tipo String para apiestado";
				}
				if (entIndIndustriagps.Fields.apitransaccion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para apitransaccion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para apitransaccion";
					return "Valor de tipo String para apitransaccion";
				}
				if (entIndIndustriagps.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para usucre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para usucre";
					return "Valor de tipo String para usucre";
				}
				if (entIndIndustriagps.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo DateTime para feccre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo DateTime para feccre";
					return "Valor de tipo DateTime para feccre";
				}
				if (entIndIndustriagps.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para usumod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para usumod";
					return "Valor de tipo String para usumod";
				}
				if (entIndIndustriagps.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo DateTime para fecmod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo DateTime para fecmod";
					return "Valor de tipo DateTime para fecmod";
				}
				if (entIndIndustriagps.Fields.id_manzana == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para id_manzana";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para id_manzana";
					return "Valor de tipo String para id_manzana";
				}
				if (entIndIndustriagps.Fields.codigo_manzana == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para codigo_manzana";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para codigo_manzana";
					return "Valor de tipo String para codigo_manzana";
				}
				if (entIndIndustriagps.Fields.usu_manzana == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para usu_manzana";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para usu_manzana";
					return "Valor de tipo String para usu_manzana";
				}
				if (entIndIndustriagps.Fields.fec_manzana == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo DateTime para fec_manzana";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo DateTime para fec_manzana";
					return "Valor de tipo DateTime para fec_manzana";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Error de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetError(entIndIndustriagps.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entIndIndustriagps.Fields.gestion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para gestion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para gestion";
					return "int para gestion";
				}
				if (entIndIndustriagps.Fields.mes == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para mes";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para mes";
					return "int para mes";
				}
				if (entIndIndustriagps.Fields.id_gps == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_gps";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_gps";
					return "int para id_gps";
				}
				if (entIndIndustriagps.Fields.id_industria == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_industria";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_industria";
					return "int para id_industria";
				}
				if (entIndIndustriagps.Fields.latitud == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Decimal para latitud";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Decimal para latitud";
					return "Decimal para latitud";
				}
				if (entIndIndustriagps.Fields.longitud == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Decimal para longitud";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Decimal para longitud";
					return "Decimal para longitud";
				}
				if (entIndIndustriagps.Fields.hora_control == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para hora_control";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para hora_control";
					return "DateTime para hora_control";
				}
				if (entIndIndustriagps.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para apiestado";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para apiestado";
					return "String para apiestado";
				}
				if (entIndIndustriagps.Fields.apitransaccion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para apitransaccion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para apitransaccion";
					return "String para apitransaccion";
				}
				if (entIndIndustriagps.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usucre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usucre";
					return "String para usucre";
				}
				if (entIndIndustriagps.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para feccre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para feccre";
					return "DateTime para feccre";
				}
				if (entIndIndustriagps.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usumod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usumod";
					return "String para usumod";
				}
				if (entIndIndustriagps.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para fecmod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para fecmod";
					return "DateTime para fecmod";
				}
				if (entIndIndustriagps.Fields.id_manzana == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para id_manzana";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para id_manzana";
					return "String para id_manzana";
				}
				if (entIndIndustriagps.Fields.codigo_manzana == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para codigo_manzana";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para codigo_manzana";
					return "String para codigo_manzana";
				}
				if (entIndIndustriagps.Fields.usu_manzana == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usu_manzana";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usu_manzana";
					return "String para usu_manzana";
				}
				if (entIndIndustriagps.Fields.fec_manzana == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para fec_manzana";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para fec_manzana";
					return "DateTime para fec_manzana";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Nombre de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entIndIndustriagps.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetColumn(entIndIndustriagps.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entIndIndustriagps.Fields.gestion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "gestion:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "gestion:";
					return "gestion:";
				}
				if (entIndIndustriagps.Fields.mes == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "mes:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "mes:";
					return "mes:";
				}
				if (entIndIndustriagps.Fields.id_gps == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_gps:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_gps:";
					return "id_gps:";
				}
				if (entIndIndustriagps.Fields.id_industria == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_industria:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_industria:";
					return "id_industria:";
				}
				if (entIndIndustriagps.Fields.latitud == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "latitud:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "latitud:";
					return "latitud:";
				}
				if (entIndIndustriagps.Fields.longitud == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "longitud:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "longitud:";
					return "longitud:";
				}
				if (entIndIndustriagps.Fields.hora_control == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "hora_control:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "hora_control:";
					return "hora_control:";
				}
				if (entIndIndustriagps.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "apiestado:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "apiestado:";
					return "apiestado:";
				}
				if (entIndIndustriagps.Fields.apitransaccion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "apitransaccion:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "apitransaccion:";
					return "apitransaccion:";
				}
				if (entIndIndustriagps.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usucre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usucre:";
					return "usucre:";
				}
				if (entIndIndustriagps.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "feccre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "feccre:";
					return "feccre:";
				}
				if (entIndIndustriagps.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usumod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usumod:";
					return "usumod:";
				}
				if (entIndIndustriagps.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "fecmod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "fecmod:";
					return "fecmod:";
				}
				if (entIndIndustriagps.Fields.id_manzana == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_manzana:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_manzana:";
					return "id_manzana:";
				}
				if (entIndIndustriagps.Fields.codigo_manzana == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "codigo_manzana:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "codigo_manzana:";
					return "codigo_manzana:";
				}
				if (entIndIndustriagps.Fields.usu_manzana == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usu_manzana:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usu_manzana:";
					return "usu_manzana:";
				}
				if (entIndIndustriagps.Fields.fec_manzana == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "fec_manzana:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "fec_manzana:";
					return "fec_manzana:";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
	}
}

