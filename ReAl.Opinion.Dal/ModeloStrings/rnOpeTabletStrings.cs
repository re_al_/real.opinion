#region 
/***********************************************************************************************************
	NOMBRE:       rnOpeTablet
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla ope_tablet

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Ine.Sice.Dal.Sociales; 
using Ine.Sice.PgConn.Sociales; 
using Ine.Sice.Dal.Sociales.Entidades;
using Ine.Sice.Dal.Sociales.Interface;
using System.Windows.Forms;
#endregion

namespace Ine.Sice.Dal.Sociales.Modelo
{
	public partial class rnOpeTablet: inOpeTablet
	{
		public const string strNombreLisForm = "Listado de ope_tablet";
		public const string strNombreDocForm = "Detalle de ope_tablet";
		public const string strNombreForm = "Registro de ope_tablet";
		/// <summary>
		/// 	Funcion que obtiene la Descripcion de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetDesc(entOpeTablet.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entOpeTablet.Fields.id_tablet == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Es el identificador unico que representa al registro en la tabla";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Es el identificador unico que representa al registro en la tabla";
					return "Es el identificador unico que representa al registro en la tabla";
				}
				if (entOpeTablet.Fields.imei == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Es el Nro IMEI del dispositivo";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Es el Nro IMEI del dispositivo";
					return "Es el Nro IMEI del dispositivo";
				}
				if (entOpeTablet.Fields.marca == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Marca del dispositivo";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Marca del dispositivo";
					return "Marca del dispositivo";
				}
				if (entOpeTablet.Fields.modelo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Modelo del Dispositivo";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Modelo del Dispositivo";
					return "Modelo del Dispositivo";
				}
				if (entOpeTablet.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Estado en el que se encuentra el registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Estado en el que se encuentra el registro";
					return "Estado en el que se encuentra el registro";
				}
				if (entOpeTablet.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Login o nombre de usuario que ha creado el registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Login o nombre de usuario que ha creado el registro";
					return "Login o nombre de usuario que ha creado el registro";
				}
				if (entOpeTablet.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Fecha de creacion del registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Fecha de creacion del registro";
					return "Fecha de creacion del registro";
				}
				if (entOpeTablet.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro";
					return "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro";
				}
				if (entOpeTablet.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Fecha en la que se ha realizado la ULTIMA modificacion registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Fecha en la que se ha realizado la ULTIMA modificacion registro";
					return "Fecha en la que se ha realizado la ULTIMA modificacion registro";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Error de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetError(entOpeTablet.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entOpeTablet.Fields.id_tablet == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_tablet";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_tablet";
					return "int para id_tablet";
				}
				if (entOpeTablet.Fields.imei == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para imei";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para imei";
					return "String para imei";
				}
				if (entOpeTablet.Fields.marca == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para marca";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para marca";
					return "String para marca";
				}
				if (entOpeTablet.Fields.modelo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para modelo";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para modelo";
					return "String para modelo";
				}
				if (entOpeTablet.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para apiestado";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para apiestado";
					return "String para apiestado";
				}
				if (entOpeTablet.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usucre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usucre";
					return "String para usucre";
				}
				if (entOpeTablet.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para feccre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para feccre";
					return "DateTime para feccre";
				}
				if (entOpeTablet.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usumod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usumod";
					return "String para usumod";
				}
				if (entOpeTablet.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para fecmod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para fecmod";
					return "DateTime para fecmod";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Nombre de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entOpeTablet.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetColumn(entOpeTablet.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entOpeTablet.Fields.id_tablet == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_tablet:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_tablet:";
					return "id_tablet:";
				}
				if (entOpeTablet.Fields.imei == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "imei:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "imei:";
					return "imei:";
				}
				if (entOpeTablet.Fields.marca == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "marca:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "marca:";
					return "marca:";
				}
				if (entOpeTablet.Fields.modelo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "modelo:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "modelo:";
					return "modelo:";
				}
				if (entOpeTablet.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "apiestado:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "apiestado:";
					return "apiestado:";
				}
				if (entOpeTablet.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usucre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usucre:";
					return "usucre:";
				}
				if (entOpeTablet.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "feccre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "feccre:";
					return "feccre:";
				}
				if (entOpeTablet.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usumod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usumod:";
					return "usumod:";
				}
				if (entOpeTablet.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "fecmod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "fecmod:";
					return "fecmod:";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
	}
}

