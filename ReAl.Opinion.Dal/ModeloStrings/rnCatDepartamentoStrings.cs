#region 
/***********************************************************************************************************
	NOMBRE:       rnCatDepartamento
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla cat_departamento

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Ine.Sice.Dal.Sociales; 
using Ine.Sice.PgConn.Sociales; 
using Ine.Sice.Dal.Sociales.Entidades;
using Ine.Sice.Dal.Sociales.Interface;
using System.Windows.Forms;
#endregion

namespace Ine.Sice.Dal.Sociales.Modelo
{
	public partial class rnCatDepartamento: inCatDepartamento
	{
		public const string strNombreLisForm = "Listado de cat_departamento";
		public const string strNombreDocForm = "Detalle de cat_departamento";
		public const string strNombreForm = "Registro de cat_departamento";
		/// <summary>
		/// 	Funcion que obtiene la Descripcion de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entCatDepartamento.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetDesc(entCatDepartamento.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entCatDepartamento.Fields.id_departamento == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Llave primaria del identificador del departamento";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Llave primaria del identificador del departamento";
					return "Llave primaria del identificador del departamento";
				}
				if (entCatDepartamento.Fields.codigo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Codigo de departamento";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Codigo de departamento";
					return "Codigo de departamento";
				}
				if (entCatDepartamento.Fields.nombre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Nombre del Departamento";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Nombre del Departamento";
					return "Nombre del Departamento";
				}
				if (entCatDepartamento.Fields.latitud == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Latitud del punto CERO de la Capital de Departamento";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Latitud del punto CERO de la Capital de Departamento";
					return "Latitud del punto CERO de la Capital de Departamento";
				}
				if (entCatDepartamento.Fields.longitud == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Longitud del punto CERO de la Capital de Departamento";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Longitud del punto CERO de la Capital de Departamento";
					return "Longitud del punto CERO de la Capital de Departamento";
				}
				if (entCatDepartamento.Fields.abreviatura == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Abreviatura del Departamento";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Abreviatura del Departamento";
					return "Abreviatura del Departamento";
				}
				if (entCatDepartamento.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Estado en el que se encuentra el registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Estado en el que se encuentra el registro";
					return "Estado en el que se encuentra el registro";
				}
				if (entCatDepartamento.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Login o nombre de usuario que ha creado el registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Login o nombre de usuario que ha creado el registro";
					return "Login o nombre de usuario que ha creado el registro";
				}
				if (entCatDepartamento.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Fecha de creacion del registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Fecha de creacion del registro";
					return "Fecha de creacion del registro";
				}
				if (entCatDepartamento.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro";
					return "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro";
				}
				if (entCatDepartamento.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Fecha en la que se ha realizado la ULTIMA modificacion registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Fecha en la que se ha realizado la ULTIMA modificacion registro";
					return "Fecha en la que se ha realizado la ULTIMA modificacion registro";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Error de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entCatDepartamento.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetError(entCatDepartamento.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entCatDepartamento.Fields.id_departamento == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_departamento";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_departamento";
					return "int para id_departamento";
				}
				if (entCatDepartamento.Fields.codigo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para codigo";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para codigo";
					return "String para codigo";
				}
				if (entCatDepartamento.Fields.nombre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para nombre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para nombre";
					return "String para nombre";
				}
				if (entCatDepartamento.Fields.latitud == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Decimal para latitud";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Decimal para latitud";
					return "Decimal para latitud";
				}
				if (entCatDepartamento.Fields.longitud == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Decimal para longitud";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Decimal para longitud";
					return "Decimal para longitud";
				}
				if (entCatDepartamento.Fields.abreviatura == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para abreviatura";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para abreviatura";
					return "String para abreviatura";
				}
				if (entCatDepartamento.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para apiestado";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para apiestado";
					return "String para apiestado";
				}
				if (entCatDepartamento.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usucre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usucre";
					return "String para usucre";
				}
				if (entCatDepartamento.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para feccre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para feccre";
					return "DateTime para feccre";
				}
				if (entCatDepartamento.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usumod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usumod";
					return "String para usumod";
				}
				if (entCatDepartamento.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para fecmod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para fecmod";
					return "DateTime para fecmod";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Nombre de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entCatDepartamento.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetColumn(entCatDepartamento.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entCatDepartamento.Fields.id_departamento == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_departamento:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_departamento:";
					return "id_departamento:";
				}
				if (entCatDepartamento.Fields.codigo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "codigo:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "codigo:";
					return "codigo:";
				}
				if (entCatDepartamento.Fields.nombre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "nombre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "nombre:";
					return "nombre:";
				}
				if (entCatDepartamento.Fields.latitud == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "latitud:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "latitud:";
					return "latitud:";
				}
				if (entCatDepartamento.Fields.longitud == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "longitud:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "longitud:";
					return "longitud:";
				}
				if (entCatDepartamento.Fields.abreviatura == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "abreviatura:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "abreviatura:";
					return "abreviatura:";
				}
				if (entCatDepartamento.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "apiestado:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "apiestado:";
					return "apiestado:";
				}
				if (entCatDepartamento.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usucre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usucre:";
					return "usucre:";
				}
				if (entCatDepartamento.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "feccre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "feccre:";
					return "feccre:";
				}
				if (entCatDepartamento.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usumod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usumod:";
					return "usumod:";
				}
				if (entCatDepartamento.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "fecmod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "fecmod:";
					return "fecmod:";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
	}
}

