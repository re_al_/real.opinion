#region 
/***********************************************************************************************************
	NOMBRE:       rnIndIndustria
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla ind_industria

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/05/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Ine.Sice.Dal.Sociales; 
using Ine.Sice.Dal.Sociales.Entidades;
using Ine.Sice.Dal.Sociales.Interface;
using System.Windows.Forms;
#endregion

namespace Ine.Sice.Dal.Sociales.Modelo
{
	public partial class rnIndIndustria: inIndIndustria
	{
		public const string strNombreLisForm = "Listado de ind_industria";
		public const string strNombreDocForm = "Detalle de ind_industria";
		public const string strNombreForm = "Registro de ind_industria";
		/// <summary>
		/// 	Funcion que obtiene la Descripcion de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetDesc(entIndIndustria.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entIndIndustria.Fields.id_industria == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para id_industria";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para id_industria";
					return "Valor de tipo int para id_industria";
				}
				if (entIndIndustria.Fields.id_departamento == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para id_departamento";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para id_departamento";
					return "Valor de tipo int para id_departamento";
				}
				if (entIndIndustria.Fields.razon_social == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para razon_social";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para razon_social";
					return "Valor de tipo String para razon_social";
				}
				if (entIndIndustria.Fields.nombre_comercial == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para nombre_comercial";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para nombre_comercial";
					return "Valor de tipo String para nombre_comercial";
				}
				if (entIndIndustria.Fields.nit == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para nit";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para nit";
					return "Valor de tipo String para nit";
				}
				if (entIndIndustria.Fields.regine == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para regine";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para regine";
					return "Valor de tipo String para regine";
				}
				if (entIndIndustria.Fields.situacion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para situacion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para situacion";
					return "Valor de tipo String para situacion";
				}
				if (entIndIndustria.Fields.direccion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para direccion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para direccion";
					return "Valor de tipo String para direccion";
				}
				if (entIndIndustria.Fields.numero == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para numero";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para numero";
					return "Valor de tipo String para numero";
				}
				if (entIndIndustria.Fields.edificio == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para edificio";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para edificio";
					return "Valor de tipo String para edificio";
				}
				if (entIndIndustria.Fields.piso == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para piso";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para piso";
					return "Valor de tipo String para piso";
				}
				if (entIndIndustria.Fields.oficina == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para oficina";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para oficina";
					return "Valor de tipo String para oficina";
				}
				if (entIndIndustria.Fields.zona == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para zona";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para zona";
					return "Valor de tipo String para zona";
				}
				if (entIndIndustria.Fields.entre_calles == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para entre_calles";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para entre_calles";
					return "Valor de tipo String para entre_calles";
				}
				if (entIndIndustria.Fields.referencia == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para referencia";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para referencia";
					return "Valor de tipo String para referencia";
				}
				if (entIndIndustria.Fields.telefono == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para telefono";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para telefono";
					return "Valor de tipo String para telefono";
				}
				if (entIndIndustria.Fields.fax == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para fax";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para fax";
					return "Valor de tipo String para fax";
				}
				if (entIndIndustria.Fields.casilla == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para casilla";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para casilla";
					return "Valor de tipo String para casilla";
				}
				if (entIndIndustria.Fields.nombre_informante == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para nombre_informante";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para nombre_informante";
					return "Valor de tipo String para nombre_informante";
				}
				if (entIndIndustria.Fields.e_mail == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para e_mail";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para e_mail";
					return "Valor de tipo String para e_mail";
				}
				if (entIndIndustria.Fields.pagina_web == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para pagina_web";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para pagina_web";
					return "Valor de tipo String para pagina_web";
				}
				if (entIndIndustria.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para apiestado";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para apiestado";
					return "Valor de tipo String para apiestado";
				}
				if (entIndIndustria.Fields.apitransaccion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para apitransaccion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para apitransaccion";
					return "Valor de tipo String para apitransaccion";
				}
				if (entIndIndustria.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para usucre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para usucre";
					return "Valor de tipo String para usucre";
				}
				if (entIndIndustria.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo DateTime para feccre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo DateTime para feccre";
					return "Valor de tipo DateTime para feccre";
				}
				if (entIndIndustria.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para usumod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para usumod";
					return "Valor de tipo String para usumod";
				}
				if (entIndIndustria.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo DateTime para fecmod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo DateTime para fecmod";
					return "Valor de tipo DateTime para fecmod";
				}
				if (entIndIndustria.Fields.geoposicion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para geoposicion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para geoposicion";
					return "Valor de tipo String para geoposicion";
				}
				if (entIndIndustria.Fields.usugeo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para usugeo";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para usugeo";
					return "Valor de tipo String para usugeo";
				}
				if (entIndIndustria.Fields.geoposicion_fabrica == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para geoposicion_fabrica";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para geoposicion_fabrica";
					return "Valor de tipo String para geoposicion_fabrica";
				}
				if (entIndIndustria.Fields.usuge_fabrica == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para usuge_fabrica";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para usuge_fabrica";
					return "Valor de tipo String para usuge_fabrica";
				}
				if (entIndIndustria.Fields.gps_informante == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para gps_informante";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para gps_informante";
					return "Valor de tipo String para gps_informante";
				}
				if (entIndIndustria.Fields.gps_fabrica == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para gps_fabrica";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para gps_fabrica";
					return "Valor de tipo String para gps_fabrica";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Error de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetError(entIndIndustria.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entIndIndustria.Fields.id_industria == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_industria";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_industria";
					return "int para id_industria";
				}
				if (entIndIndustria.Fields.id_departamento == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_departamento";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_departamento";
					return "int para id_departamento";
				}
				if (entIndIndustria.Fields.razon_social == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para razon_social";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para razon_social";
					return "String para razon_social";
				}
				if (entIndIndustria.Fields.nombre_comercial == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para nombre_comercial";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para nombre_comercial";
					return "String para nombre_comercial";
				}
				if (entIndIndustria.Fields.nit == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para nit";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para nit";
					return "String para nit";
				}
				if (entIndIndustria.Fields.regine == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para regine";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para regine";
					return "String para regine";
				}
				if (entIndIndustria.Fields.situacion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para situacion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para situacion";
					return "String para situacion";
				}
				if (entIndIndustria.Fields.direccion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para direccion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para direccion";
					return "String para direccion";
				}
				if (entIndIndustria.Fields.numero == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para numero";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para numero";
					return "String para numero";
				}
				if (entIndIndustria.Fields.edificio == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para edificio";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para edificio";
					return "String para edificio";
				}
				if (entIndIndustria.Fields.piso == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para piso";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para piso";
					return "String para piso";
				}
				if (entIndIndustria.Fields.oficina == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para oficina";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para oficina";
					return "String para oficina";
				}
				if (entIndIndustria.Fields.zona == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para zona";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para zona";
					return "String para zona";
				}
				if (entIndIndustria.Fields.entre_calles == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para entre_calles";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para entre_calles";
					return "String para entre_calles";
				}
				if (entIndIndustria.Fields.referencia == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para referencia";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para referencia";
					return "String para referencia";
				}
				if (entIndIndustria.Fields.telefono == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para telefono";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para telefono";
					return "String para telefono";
				}
				if (entIndIndustria.Fields.fax == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para fax";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para fax";
					return "String para fax";
				}
				if (entIndIndustria.Fields.casilla == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para casilla";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para casilla";
					return "String para casilla";
				}
				if (entIndIndustria.Fields.nombre_informante == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para nombre_informante";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para nombre_informante";
					return "String para nombre_informante";
				}
				if (entIndIndustria.Fields.e_mail == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para e_mail";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para e_mail";
					return "String para e_mail";
				}
				if (entIndIndustria.Fields.pagina_web == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para pagina_web";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para pagina_web";
					return "String para pagina_web";
				}
				if (entIndIndustria.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para apiestado";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para apiestado";
					return "String para apiestado";
				}
				if (entIndIndustria.Fields.apitransaccion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para apitransaccion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para apitransaccion";
					return "String para apitransaccion";
				}
				if (entIndIndustria.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usucre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usucre";
					return "String para usucre";
				}
				if (entIndIndustria.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para feccre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para feccre";
					return "DateTime para feccre";
				}
				if (entIndIndustria.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usumod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usumod";
					return "String para usumod";
				}
				if (entIndIndustria.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para fecmod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para fecmod";
					return "DateTime para fecmod";
				}
				if (entIndIndustria.Fields.geoposicion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para geoposicion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para geoposicion";
					return "String para geoposicion";
				}
				if (entIndIndustria.Fields.usugeo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usugeo";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usugeo";
					return "String para usugeo";
				}
				if (entIndIndustria.Fields.geoposicion_fabrica == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para geoposicion_fabrica";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para geoposicion_fabrica";
					return "String para geoposicion_fabrica";
				}
				if (entIndIndustria.Fields.usuge_fabrica == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usuge_fabrica";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usuge_fabrica";
					return "String para usuge_fabrica";
				}
				if (entIndIndustria.Fields.gps_informante == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para gps_informante";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para gps_informante";
					return "String para gps_informante";
				}
				if (entIndIndustria.Fields.gps_fabrica == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para gps_fabrica";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para gps_fabrica";
					return "String para gps_fabrica";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Nombre de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entIndIndustria.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetColumn(entIndIndustria.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entIndIndustria.Fields.id_industria == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_industria:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_industria:";
					return "id_industria:";
				}
				if (entIndIndustria.Fields.id_departamento == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_departamento:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_departamento:";
					return "id_departamento:";
				}
				if (entIndIndustria.Fields.razon_social == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "razon_social:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "razon_social:";
					return "razon_social:";
				}
				if (entIndIndustria.Fields.nombre_comercial == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "nombre_comercial:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "nombre_comercial:";
					return "nombre_comercial:";
				}
				if (entIndIndustria.Fields.nit == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "nit:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "nit:";
					return "nit:";
				}
				if (entIndIndustria.Fields.regine == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "regine:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "regine:";
					return "regine:";
				}
				if (entIndIndustria.Fields.situacion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "situacion:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "situacion:";
					return "situacion:";
				}
				if (entIndIndustria.Fields.direccion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "direccion:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "direccion:";
					return "direccion:";
				}
				if (entIndIndustria.Fields.numero == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "numero:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "numero:";
					return "numero:";
				}
				if (entIndIndustria.Fields.edificio == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "edificio:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "edificio:";
					return "edificio:";
				}
				if (entIndIndustria.Fields.piso == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "piso:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "piso:";
					return "piso:";
				}
				if (entIndIndustria.Fields.oficina == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "oficina:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "oficina:";
					return "oficina:";
				}
				if (entIndIndustria.Fields.zona == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "zona:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "zona:";
					return "zona:";
				}
				if (entIndIndustria.Fields.entre_calles == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "entre_calles:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "entre_calles:";
					return "entre_calles:";
				}
				if (entIndIndustria.Fields.referencia == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "referencia:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "referencia:";
					return "referencia:";
				}
				if (entIndIndustria.Fields.telefono == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "telefono:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "telefono:";
					return "telefono:";
				}
				if (entIndIndustria.Fields.fax == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "fax:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "fax:";
					return "fax:";
				}
				if (entIndIndustria.Fields.casilla == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "casilla:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "casilla:";
					return "casilla:";
				}
				if (entIndIndustria.Fields.nombre_informante == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "nombre_informante:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "nombre_informante:";
					return "nombre_informante:";
				}
				if (entIndIndustria.Fields.e_mail == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "e_mail:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "e_mail:";
					return "e_mail:";
				}
				if (entIndIndustria.Fields.pagina_web == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "pagina_web:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "pagina_web:";
					return "pagina_web:";
				}
				if (entIndIndustria.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "apiestado:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "apiestado:";
					return "apiestado:";
				}
				if (entIndIndustria.Fields.apitransaccion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "apitransaccion:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "apitransaccion:";
					return "apitransaccion:";
				}
				if (entIndIndustria.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usucre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usucre:";
					return "usucre:";
				}
				if (entIndIndustria.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "feccre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "feccre:";
					return "feccre:";
				}
				if (entIndIndustria.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usumod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usumod:";
					return "usumod:";
				}
				if (entIndIndustria.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "fecmod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "fecmod:";
					return "fecmod:";
				}
				if (entIndIndustria.Fields.geoposicion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "geoposicion:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "geoposicion:";
					return "geoposicion:";
				}
				if (entIndIndustria.Fields.usugeo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usugeo:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usugeo:";
					return "usugeo:";
				}
				if (entIndIndustria.Fields.geoposicion_fabrica == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "geoposicion_fabrica:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "geoposicion_fabrica:";
					return "geoposicion_fabrica:";
				}
				if (entIndIndustria.Fields.usuge_fabrica == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usuge_fabrica:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usuge_fabrica:";
					return "usuge_fabrica:";
				}
				if (entIndIndustria.Fields.gps_informante == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "gps_informante:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "gps_informante:";
					return "gps_informante:";
				}
				if (entIndIndustria.Fields.gps_fabrica == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "gps_fabrica:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "gps_fabrica:";
					return "gps_fabrica:";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
	}
}

