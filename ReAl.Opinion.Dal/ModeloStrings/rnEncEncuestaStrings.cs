#region 
/***********************************************************************************************************
	NOMBRE:       rnEncEncuesta
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla enc_encuesta

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Ine.Sice.Dal.Sociales; 
using Ine.Sice.PgConn.Sociales; 
using Ine.Sice.Dal.Sociales.Entidades;
using Ine.Sice.Dal.Sociales.Interface;
using System.Windows.Forms;
#endregion

namespace Ine.Sice.Dal.Sociales.Modelo
{
	public partial class rnEncEncuesta: inEncEncuesta
	{
		public const string strNombreLisForm = "Listado de enc_encuesta";
		public const string strNombreDocForm = "Detalle de enc_encuesta";
		public const string strNombreForm = "Registro de enc_encuesta";
		/// <summary>
		/// 	Funcion que obtiene la Descripcion de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetDesc(entEncEncuesta.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entEncEncuesta.Fields.id_encuesta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Identificador unico que representa al registro en la tabla";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Identificador unico que representa al registro en la tabla";
					return "Identificador unico que representa al registro en la tabla";
				}
				if (entEncEncuesta.Fields.id_tab_encuesta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Identificador unico que representa al registro en la tabla en el Dispositivo";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Identificador unico que representa al registro en la tabla en el Dispositivo";
					return "Identificador unico que representa al registro en la tabla en el Dispositivo";
				}
				if (entEncEncuesta.Fields.id_informante == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Es el identificador unico de la tabla enc_informante";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Es el identificador unico de la tabla enc_informante";
					return "Es el identificador unico de la tabla enc_informante";
				}
				if (entEncEncuesta.Fields.id_movimiento == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Es el identificador unico de la tabla enc_movimiento";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Es el identificador unico de la tabla enc_movimiento";
					return "Es el identificador unico de la tabla enc_movimiento";
				}
				if (entEncEncuesta.Fields.id_pregunta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Es el identificador unico de la tabla enc_pregunta";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Es el identificador unico de la tabla enc_pregunta";
					return "Es el identificador unico de la tabla enc_pregunta";
				}
				if (entEncEncuesta.Fields.id_respuesta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Es el identificador unico de la tabla enc_respuesta";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Es el identificador unico de la tabla enc_respuesta";
					return "Es el identificador unico de la tabla enc_respuesta";
				}
				if (entEncEncuesta.Fields.codigo_respuesta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Es el Codigo utilizado por la respuesta, segun la tabla enc_respuesta";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Es el Codigo utilizado por la respuesta, segun la tabla enc_respuesta";
					return "Es el Codigo utilizado por la respuesta, segun la tabla enc_respuesta";
				}
				if (entEncEncuesta.Fields.respuesta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Es la respuesta TEXTUAL a la pregunta";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Es la respuesta TEXTUAL a la pregunta";
					return "Es la respuesta TEXTUAL a la pregunta";
				}
				if (entEncEncuesta.Fields.observacion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Representa alguna observacion sobre la respuesta o pregunta";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Representa alguna observacion sobre la respuesta o pregunta";
					return "Representa alguna observacion sobre la respuesta o pregunta";
				}
				if (entEncEncuesta.Fields.latitud == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Latitud del punto CERO de la Capital de Departamento";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Latitud del punto CERO de la Capital de Departamento";
					return "Latitud del punto CERO de la Capital de Departamento";
				}
				if (entEncEncuesta.Fields.longitud == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Longitud del punto CERO de la Capital de Departamento";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Longitud del punto CERO de la Capital de Departamento";
					return "Longitud del punto CERO de la Capital de Departamento";
				}
				if (entEncEncuesta.Fields.id_last == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Es el identificador para control de ediciones";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Es el identificador para control de ediciones";
					return "Es el identificador para control de ediciones";
				}
				if (entEncEncuesta.Fields.fila == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para fila";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para fila";
					return "Valor de tipo int para fila";
				}
				if (entEncEncuesta.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Estado en el que se encuentra el registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Estado en el que se encuentra el registro";
					return "Estado en el que se encuentra el registro";
				}
				if (entEncEncuesta.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Login o nombre de usuario que ha creado el registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Login o nombre de usuario que ha creado el registro";
					return "Login o nombre de usuario que ha creado el registro";
				}
				if (entEncEncuesta.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Fecha de creacion del registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Fecha de creacion del registro";
					return "Fecha de creacion del registro";
				}
				if (entEncEncuesta.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro";
					return "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro";
				}
				if (entEncEncuesta.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Fecha en la que se ha realizado la ULTIMA modificacion registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Fecha en la que se ha realizado la ULTIMA modificacion registro";
					return "Fecha en la que se ha realizado la ULTIMA modificacion registro";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Error de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetError(entEncEncuesta.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entEncEncuesta.Fields.id_encuesta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Int64 para id_encuesta";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Int64 para id_encuesta";
					return "Int64 para id_encuesta";
				}
				if (entEncEncuesta.Fields.id_tab_encuesta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_tab_encuesta";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_tab_encuesta";
					return "int para id_tab_encuesta";
				}
				if (entEncEncuesta.Fields.id_informante == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Int64 para id_informante";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Int64 para id_informante";
					return "Int64 para id_informante";
				}
				if (entEncEncuesta.Fields.id_movimiento == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_movimiento";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_movimiento";
					return "int para id_movimiento";
				}
				if (entEncEncuesta.Fields.id_pregunta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_pregunta";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_pregunta";
					return "int para id_pregunta";
				}
				if (entEncEncuesta.Fields.id_respuesta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_respuesta";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_respuesta";
					return "int para id_respuesta";
				}
				if (entEncEncuesta.Fields.codigo_respuesta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para codigo_respuesta";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para codigo_respuesta";
					return "String para codigo_respuesta";
				}
				if (entEncEncuesta.Fields.respuesta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para respuesta";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para respuesta";
					return "String para respuesta";
				}
				if (entEncEncuesta.Fields.observacion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para observacion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para observacion";
					return "String para observacion";
				}
				if (entEncEncuesta.Fields.latitud == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Decimal para latitud";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Decimal para latitud";
					return "Decimal para latitud";
				}
				if (entEncEncuesta.Fields.longitud == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Decimal para longitud";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Decimal para longitud";
					return "Decimal para longitud";
				}
				if (entEncEncuesta.Fields.id_last == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_last";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_last";
					return "int para id_last";
				}
				if (entEncEncuesta.Fields.fila == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para fila";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para fila";
					return "int para fila";
				}
				if (entEncEncuesta.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para apiestado";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para apiestado";
					return "String para apiestado";
				}
				if (entEncEncuesta.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usucre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usucre";
					return "String para usucre";
				}
				if (entEncEncuesta.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para feccre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para feccre";
					return "DateTime para feccre";
				}
				if (entEncEncuesta.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usumod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usumod";
					return "String para usumod";
				}
				if (entEncEncuesta.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para fecmod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para fecmod";
					return "DateTime para fecmod";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Nombre de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entEncEncuesta.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetColumn(entEncEncuesta.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entEncEncuesta.Fields.id_encuesta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_encuesta:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_encuesta:";
					return "id_encuesta:";
				}
				if (entEncEncuesta.Fields.id_tab_encuesta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_tab_encuesta:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_tab_encuesta:";
					return "id_tab_encuesta:";
				}
				if (entEncEncuesta.Fields.id_informante == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_informante:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_informante:";
					return "id_informante:";
				}
				if (entEncEncuesta.Fields.id_movimiento == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_movimiento:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_movimiento:";
					return "id_movimiento:";
				}
				if (entEncEncuesta.Fields.id_pregunta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_pregunta:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_pregunta:";
					return "id_pregunta:";
				}
				if (entEncEncuesta.Fields.id_respuesta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_respuesta:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_respuesta:";
					return "id_respuesta:";
				}
				if (entEncEncuesta.Fields.codigo_respuesta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "codigo_respuesta:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "codigo_respuesta:";
					return "codigo_respuesta:";
				}
				if (entEncEncuesta.Fields.respuesta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "respuesta:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "respuesta:";
					return "respuesta:";
				}
				if (entEncEncuesta.Fields.observacion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "observacion:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "observacion:";
					return "observacion:";
				}
				if (entEncEncuesta.Fields.latitud == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "latitud:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "latitud:";
					return "latitud:";
				}
				if (entEncEncuesta.Fields.longitud == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "longitud:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "longitud:";
					return "longitud:";
				}
				if (entEncEncuesta.Fields.id_last == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_last:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_last:";
					return "id_last:";
				}
				if (entEncEncuesta.Fields.fila == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "fila:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "fila:";
					return "fila:";
				}
				if (entEncEncuesta.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "apiestado:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "apiestado:";
					return "apiestado:";
				}
				if (entEncEncuesta.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usucre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usucre:";
					return "usucre:";
				}
				if (entEncEncuesta.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "feccre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "feccre:";
					return "feccre:";
				}
				if (entEncEncuesta.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usumod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usumod:";
					return "usumod:";
				}
				if (entEncEncuesta.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "fecmod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "fecmod:";
					return "fecmod:";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
	}
}

