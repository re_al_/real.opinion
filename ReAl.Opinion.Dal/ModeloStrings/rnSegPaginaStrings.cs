#region 
/***********************************************************************************************************
	NOMBRE:       rnSegPagina
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla seg_pagina

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Ine.Sice.Dal.Sociales; 
using Ine.Sice.PgConn.Sociales; 
using Ine.Sice.Dal.Sociales.Entidades;
using Ine.Sice.Dal.Sociales.Interface;
using System.Windows.Forms;
#endregion

namespace Ine.Sice.Dal.Sociales.Modelo
{
	public partial class rnSegPagina: inSegPagina
	{
		public const string strNombreLisForm = "Listado de seg_pagina";
		public const string strNombreDocForm = "Detalle de seg_pagina";
		public const string strNombreForm = "Registro de seg_pagina";
		/// <summary>
		/// 	Funcion que obtiene la Descripcion de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entSegPagina.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetDesc(entSegPagina.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entSegPagina.Fields.id_pagina == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para id_pagina";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para id_pagina";
					return "Valor de tipo int para id_pagina";
				}
				if (entSegPagina.Fields.id_pagina_padre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para id_pagina_padre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para id_pagina_padre";
					return "Valor de tipo int para id_pagina_padre";
				}
				if (entSegPagina.Fields.enlace == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para enlace";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para enlace";
					return "Valor de tipo String para enlace";
				}
				if (entSegPagina.Fields.icono == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para icono";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para icono";
					return "Valor de tipo String para icono";
				}
				if (entSegPagina.Fields.nombre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para nombre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para nombre";
					return "Valor de tipo String para nombre";
				}
				if (entSegPagina.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para apiestado";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para apiestado";
					return "Valor de tipo String para apiestado";
				}
				if (entSegPagina.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para usucre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para usucre";
					return "Valor de tipo String para usucre";
				}
				if (entSegPagina.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo DateTime para feccre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo DateTime para feccre";
					return "Valor de tipo DateTime para feccre";
				}
				if (entSegPagina.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para usumod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para usumod";
					return "Valor de tipo String para usumod";
				}
				if (entSegPagina.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo DateTime para fecmod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo DateTime para fecmod";
					return "Valor de tipo DateTime para fecmod";
				}
				if (entSegPagina.Fields.orden == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para orden";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para orden";
					return "Valor de tipo int para orden";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Error de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entSegPagina.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetError(entSegPagina.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entSegPagina.Fields.id_pagina == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_pagina";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_pagina";
					return "int para id_pagina";
				}
				if (entSegPagina.Fields.id_pagina_padre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_pagina_padre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_pagina_padre";
					return "int para id_pagina_padre";
				}
				if (entSegPagina.Fields.enlace == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para enlace";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para enlace";
					return "String para enlace";
				}
				if (entSegPagina.Fields.icono == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para icono";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para icono";
					return "String para icono";
				}
				if (entSegPagina.Fields.nombre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para nombre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para nombre";
					return "String para nombre";
				}
				if (entSegPagina.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para apiestado";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para apiestado";
					return "String para apiestado";
				}
				if (entSegPagina.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usucre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usucre";
					return "String para usucre";
				}
				if (entSegPagina.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para feccre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para feccre";
					return "DateTime para feccre";
				}
				if (entSegPagina.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usumod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usumod";
					return "String para usumod";
				}
				if (entSegPagina.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para fecmod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para fecmod";
					return "DateTime para fecmod";
				}
				if (entSegPagina.Fields.orden == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para orden";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para orden";
					return "int para orden";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Nombre de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entSegPagina.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetColumn(entSegPagina.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entSegPagina.Fields.id_pagina == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_pagina:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_pagina:";
					return "id_pagina:";
				}
				if (entSegPagina.Fields.id_pagina_padre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_pagina_padre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_pagina_padre:";
					return "id_pagina_padre:";
				}
				if (entSegPagina.Fields.enlace == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "enlace:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "enlace:";
					return "enlace:";
				}
				if (entSegPagina.Fields.icono == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "icono:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "icono:";
					return "icono:";
				}
				if (entSegPagina.Fields.nombre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "nombre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "nombre:";
					return "nombre:";
				}
				if (entSegPagina.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "apiestado:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "apiestado:";
					return "apiestado:";
				}
				if (entSegPagina.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usucre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usucre:";
					return "usucre:";
				}
				if (entSegPagina.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "feccre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "feccre:";
					return "feccre:";
				}
				if (entSegPagina.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usumod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usumod:";
					return "usumod:";
				}
				if (entSegPagina.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "fecmod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "fecmod:";
					return "fecmod:";
				}
				if (entSegPagina.Fields.orden == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "orden:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "orden:";
					return "orden:";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
	}
}

