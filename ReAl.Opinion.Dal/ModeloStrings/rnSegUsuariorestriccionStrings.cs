#region 
/***********************************************************************************************************
	NOMBRE:       rnSegUsuariorestriccion
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla seg_usuariorestriccion

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Ine.Sice.Dal.Sociales; 
using Ine.Sice.PgConn.Sociales; 
using Ine.Sice.Dal.Sociales.Entidades;
using Ine.Sice.Dal.Sociales.Interface;
using System.Windows.Forms;
#endregion

namespace Ine.Sice.Dal.Sociales.Modelo
{
	public partial class rnSegUsuariorestriccion: inSegUsuariorestriccion
	{
		public const string strNombreLisForm = "Listado de seg_usuariorestriccion";
		public const string strNombreDocForm = "Detalle de seg_usuariorestriccion";
		public const string strNombreForm = "Registro de seg_usuariorestriccion";
		/// <summary>
		/// 	Funcion que obtiene la Descripcion de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entSegUsuariorestriccion.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetDesc(entSegUsuariorestriccion.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entSegUsuariorestriccion.Fields.id_usurestriccion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Identificador unico de la tabla";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Identificador unico de la tabla";
					return "Identificador unico de la tabla";
				}
				if (entSegUsuariorestriccion.Fields.id_proyecto == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para id_proyecto";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para id_proyecto";
					return "Valor de tipo int para id_proyecto";
				}
				if (entSegUsuariorestriccion.Fields.id_usuario == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para id_usuario";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para id_usuario";
					return "Valor de tipo int para id_usuario";
				}
				if (entSegUsuariorestriccion.Fields.id_rol == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para id_rol";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para id_rol";
					return "Valor de tipo int para id_rol";
				}
				if (entSegUsuariorestriccion.Fields.departamentos == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para departamentos";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para departamentos";
					return "Valor de tipo String para departamentos";
				}
				if (entSegUsuariorestriccion.Fields.fechavigente == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo DateTime para fechavigente";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo DateTime para fechavigente";
					return "Valor de tipo DateTime para fechavigente";
				}
				if (entSegUsuariorestriccion.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para apiestado";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para apiestado";
					return "Valor de tipo String para apiestado";
				}
				if (entSegUsuariorestriccion.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para usucre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para usucre";
					return "Valor de tipo String para usucre";
				}
				if (entSegUsuariorestriccion.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo DateTime para feccre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo DateTime para feccre";
					return "Valor de tipo DateTime para feccre";
				}
				if (entSegUsuariorestriccion.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para usumod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para usumod";
					return "Valor de tipo String para usumod";
				}
				if (entSegUsuariorestriccion.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo DateTime para fecmod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo DateTime para fecmod";
					return "Valor de tipo DateTime para fecmod";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Error de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entSegUsuariorestriccion.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetError(entSegUsuariorestriccion.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entSegUsuariorestriccion.Fields.id_usurestriccion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_usurestriccion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_usurestriccion";
					return "int para id_usurestriccion";
				}
				if (entSegUsuariorestriccion.Fields.id_proyecto == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_proyecto";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_proyecto";
					return "int para id_proyecto";
				}
				if (entSegUsuariorestriccion.Fields.id_usuario == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_usuario";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_usuario";
					return "int para id_usuario";
				}
				if (entSegUsuariorestriccion.Fields.id_rol == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_rol";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_rol";
					return "int para id_rol";
				}
				if (entSegUsuariorestriccion.Fields.departamentos == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para departamentos";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para departamentos";
					return "String para departamentos";
				}
				if (entSegUsuariorestriccion.Fields.fechavigente == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para fechavigente";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para fechavigente";
					return "DateTime para fechavigente";
				}
				if (entSegUsuariorestriccion.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para apiestado";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para apiestado";
					return "String para apiestado";
				}
				if (entSegUsuariorestriccion.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usucre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usucre";
					return "String para usucre";
				}
				if (entSegUsuariorestriccion.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para feccre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para feccre";
					return "DateTime para feccre";
				}
				if (entSegUsuariorestriccion.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usumod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usumod";
					return "String para usumod";
				}
				if (entSegUsuariorestriccion.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para fecmod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para fecmod";
					return "DateTime para fecmod";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Nombre de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entSegUsuariorestriccion.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetColumn(entSegUsuariorestriccion.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entSegUsuariorestriccion.Fields.id_usurestriccion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_usurestriccion:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_usurestriccion:";
					return "id_usurestriccion:";
				}
				if (entSegUsuariorestriccion.Fields.id_proyecto == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_proyecto:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_proyecto:";
					return "id_proyecto:";
				}
				if (entSegUsuariorestriccion.Fields.id_usuario == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_usuario:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_usuario:";
					return "id_usuario:";
				}
				if (entSegUsuariorestriccion.Fields.id_rol == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_rol:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_rol:";
					return "id_rol:";
				}
				if (entSegUsuariorestriccion.Fields.departamentos == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "departamentos:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "departamentos:";
					return "departamentos:";
				}
				if (entSegUsuariorestriccion.Fields.fechavigente == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "fechavigente:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "fechavigente:";
					return "fechavigente:";
				}
				if (entSegUsuariorestriccion.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "apiestado:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "apiestado:";
					return "apiestado:";
				}
				if (entSegUsuariorestriccion.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usucre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usucre:";
					return "usucre:";
				}
				if (entSegUsuariorestriccion.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "feccre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "feccre:";
					return "feccre:";
				}
				if (entSegUsuariorestriccion.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usumod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usumod:";
					return "usumod:";
				}
				if (entSegUsuariorestriccion.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "fecmod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "fecmod:";
					return "fecmod:";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
	}
}

