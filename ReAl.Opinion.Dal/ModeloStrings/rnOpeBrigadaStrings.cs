#region 
/***********************************************************************************************************
	NOMBRE:       rnOpeBrigada
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla ope_brigada

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Ine.Sice.Dal.Sociales; 
using Ine.Sice.PgConn.Sociales; 
using Ine.Sice.Dal.Sociales.Entidades;
using Ine.Sice.Dal.Sociales.Interface;
using System.Windows.Forms;
#endregion

namespace Ine.Sice.Dal.Sociales.Modelo
{
	public partial class rnOpeBrigada: inOpeBrigada
	{
		public const string strNombreLisForm = "Listado de ope_brigada";
		public const string strNombreDocForm = "Detalle de ope_brigada";
		public const string strNombreForm = "Registro de ope_brigada";
		/// <summary>
		/// 	Funcion que obtiene la Descripcion de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entOpeBrigada.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetDesc(entOpeBrigada.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entOpeBrigada.Fields.id_brigada == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Es el identificador unico que representa al registro en la tabla";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Es el identificador unico que representa al registro en la tabla";
					return "Es el identificador unico que representa al registro en la tabla";
				}
				if (entOpeBrigada.Fields.id_departamento == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Es el identificador del Departamento al que pertenece la brigada";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Es el identificador del Departamento al que pertenece la brigada";
					return "Es el identificador del Departamento al que pertenece la brigada";
				}
				if (entOpeBrigada.Fields.codigo_brigada == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Codigo que representa a la brigada";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Codigo que representa a la brigada";
					return "Codigo que representa a la brigada";
				}
				if (entOpeBrigada.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Estado en el que se encuentra el registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Estado en el que se encuentra el registro";
					return "Estado en el que se encuentra el registro";
				}
				if (entOpeBrigada.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Login o nombre de usuario que ha creado el registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Login o nombre de usuario que ha creado el registro";
					return "Login o nombre de usuario que ha creado el registro";
				}
				if (entOpeBrigada.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Fecha de creacion del registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Fecha de creacion del registro";
					return "Fecha de creacion del registro";
				}
				if (entOpeBrigada.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro";
					return "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro";
				}
				if (entOpeBrigada.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Fecha en la que se ha realizado la ULTIMA modificacion registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Fecha en la que se ha realizado la ULTIMA modificacion registro";
					return "Fecha en la que se ha realizado la ULTIMA modificacion registro";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Error de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entOpeBrigada.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetError(entOpeBrigada.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entOpeBrigada.Fields.id_brigada == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_brigada";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_brigada";
					return "int para id_brigada";
				}
				if (entOpeBrigada.Fields.id_departamento == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_departamento";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_departamento";
					return "int para id_departamento";
				}
				if (entOpeBrigada.Fields.codigo_brigada == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para codigo_brigada";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para codigo_brigada";
					return "String para codigo_brigada";
				}
				if (entOpeBrigada.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para apiestado";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para apiestado";
					return "String para apiestado";
				}
				if (entOpeBrigada.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usucre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usucre";
					return "String para usucre";
				}
				if (entOpeBrigada.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para feccre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para feccre";
					return "DateTime para feccre";
				}
				if (entOpeBrigada.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usumod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usumod";
					return "String para usumod";
				}
				if (entOpeBrigada.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para fecmod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para fecmod";
					return "DateTime para fecmod";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Nombre de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entOpeBrigada.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetColumn(entOpeBrigada.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entOpeBrigada.Fields.id_brigada == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_brigada:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_brigada:";
					return "id_brigada:";
				}
				if (entOpeBrigada.Fields.id_departamento == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_departamento:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_departamento:";
					return "id_departamento:";
				}
				if (entOpeBrigada.Fields.codigo_brigada == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "codigo_brigada:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "codigo_brigada:";
					return "codigo_brigada:";
				}
				if (entOpeBrigada.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "apiestado:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "apiestado:";
					return "apiestado:";
				}
				if (entOpeBrigada.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usucre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usucre:";
					return "usucre:";
				}
				if (entOpeBrigada.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "feccre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "feccre:";
					return "feccre:";
				}
				if (entOpeBrigada.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usumod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usumod:";
					return "usumod:";
				}
				if (entOpeBrigada.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "fecmod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "fecmod:";
					return "fecmod:";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
	}
}

