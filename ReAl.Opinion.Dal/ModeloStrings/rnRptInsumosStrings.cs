#region 
/***********************************************************************************************************
	NOMBRE:       rnRptInsumos
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla rpt_insumos

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        22/10/2014  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Ine.Sice.PgConn.Sociales; 
using Ine.Sice.Dal.Sociales.Entidades;
using Ine.Sice.Dal.Sociales.Interface;
using System.Windows.Forms;
#endregion

namespace Ine.Sice.Dal.Sociales.Modelo
{
	public partial class rnRptInsumos: inRptInsumos
	{
		public const string strNombreLisForm = "Listado de rpt_insumos";
		public const string strNombreDocForm = "Detalle de rpt_insumos";
		public const string strNombreForm = "Registro de rpt_insumos";
		/// <summary>
		/// 	Funcion que obtiene la Descripcion de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetDesc(entRptInsumos.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entRptInsumos.Fields.gestion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para gestion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para gestion";
					return "Valor de tipo int para gestion";
				}
				if (entRptInsumos.Fields.mes == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para mes";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para mes";
					return "Valor de tipo int para mes";
				}
				if (entRptInsumos.Fields.foto == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para foto";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para foto";
					return "Valor de tipo String para foto";
				}
				if (entRptInsumos.Fields.id_departamento == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para id_departamento";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para id_departamento";
					return "Valor de tipo int para id_departamento";
				}
				if (entRptInsumos.Fields.id_upmipp == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para id_upmipp";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para id_upmipp";
					return "Valor de tipo int para id_upmipp";
				}
				if (entRptInsumos.Fields.id_provincia == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para id_provincia";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para id_provincia";
					return "Valor de tipo int para id_provincia";
				}
				if (entRptInsumos.Fields.id_municipio == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para id_municipio";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para id_municipio";
					return "Valor de tipo int para id_municipio";
				}
				if (entRptInsumos.Fields.id_listado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para id_listado";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para id_listado";
					return "Valor de tipo int para id_listado";
				}
				if (entRptInsumos.Fields.id_boleta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para id_boleta";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para id_boleta";
					return "Valor de tipo int para id_boleta";
				}
				if (entRptInsumos.Fields.id_persona == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para id_persona";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para id_persona";
					return "Valor de tipo int para id_persona";
				}
				if (entRptInsumos.Fields.id_movimiento == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para id_movimiento";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para id_movimiento";
					return "Valor de tipo int para id_movimiento";
				}
				if (entRptInsumos.Fields.control == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para control";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para control";
					return "Valor de tipo int para control";
				}
				if (entRptInsumos.Fields.minimo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para minimo";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para minimo";
					return "Valor de tipo int para minimo";
				}
				if (entRptInsumos.Fields.maximo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para maximo";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para maximo";
					return "Valor de tipo int para maximo";
				}
				if (entRptInsumos.Fields.departamento == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para departamento";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para departamento";
					return "Valor de tipo String para departamento";
				}
				if (entRptInsumos.Fields.provincia == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para provincia";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para provincia";
					return "Valor de tipo String para provincia";
				}
				if (entRptInsumos.Fields.municipio == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para municipio";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para municipio";
					return "Valor de tipo String para municipio";
				}
				if (entRptInsumos.Fields.comunidad == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para comunidad";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para comunidad";
					return "Valor de tipo String para comunidad";
				}
				if (entRptInsumos.Fields.login == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para login";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para login";
					return "Valor de tipo String para login";
				}
				if (entRptInsumos.Fields.producto_boleta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para producto_boleta";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para producto_boleta";
					return "Valor de tipo String para producto_boleta";
				}
				if (entRptInsumos.Fields.codigo_producto == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para codigo_producto";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para codigo_producto";
					return "Valor de tipo String para codigo_producto";
				}
				if (entRptInsumos.Fields.codigo_ccp == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para codigo_ccp";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para codigo_ccp";
					return "Valor de tipo String para codigo_ccp";
				}
				if (entRptInsumos.Fields.informante == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para informante";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para informante";
					return "Valor de tipo String para informante";
				}
				if (entRptInsumos.Fields.telefono == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para telefono";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para telefono";
					return "Valor de tipo String para telefono";
				}
				if (entRptInsumos.Fields.direccion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para direccion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para direccion";
					return "Valor de tipo String para direccion";
				}
				if (entRptInsumos.Fields.producto == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para producto";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para producto";
					return "Valor de tipo String para producto";
				}
				if (entRptInsumos.Fields.caracteristicas == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para caracteristicas";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para caracteristicas";
					return "Valor de tipo String para caracteristicas";
				}
				if (entRptInsumos.Fields.variedad == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para variedad";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para variedad";
					return "Valor de tipo String para variedad";
				}
				if (entRptInsumos.Fields.origen == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para origen";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para origen";
					return "Valor de tipo String para origen";
				}
				if (entRptInsumos.Fields.cantidad == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para cantidad";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para cantidad";
					return "Valor de tipo String para cantidad";
				}
				if (entRptInsumos.Fields.unidad == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para unidad";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para unidad";
					return "Valor de tipo String para unidad";
				}
				if (entRptInsumos.Fields.precio == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para precio";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para precio";
					return "Valor de tipo String para precio";
				}
				if (entRptInsumos.Fields.superficie == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para superficie";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para superficie";
					return "Valor de tipo String para superficie";
				}
				if (entRptInsumos.Fields.observaciones == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para observaciones";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para observaciones";
					return "Valor de tipo String para observaciones";
				}
				if (entRptInsumos.Fields.minfec == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo DateTime para minfec";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo DateTime para minfec";
					return "Valor de tipo DateTime para minfec";
				}
				if (entRptInsumos.Fields.maxfec == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo DateTime para maxfec";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo DateTime para maxfec";
					return "Valor de tipo DateTime para maxfec";
				}
				if (entRptInsumos.Fields.duracion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo Decimal para duracion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo Decimal para duracion";
					return "Valor de tipo Decimal para duracion";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Error de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetError(entRptInsumos.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entRptInsumos.Fields.gestion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para gestion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para gestion";
					return "int para gestion";
				}
				if (entRptInsumos.Fields.mes == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para mes";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para mes";
					return "int para mes";
				}
				if (entRptInsumos.Fields.foto == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para foto";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para foto";
					return "String para foto";
				}
				if (entRptInsumos.Fields.id_departamento == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_departamento";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_departamento";
					return "int para id_departamento";
				}
				if (entRptInsumos.Fields.id_upmipp == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_upmipp";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_upmipp";
					return "int para id_upmipp";
				}
				if (entRptInsumos.Fields.id_provincia == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_provincia";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_provincia";
					return "int para id_provincia";
				}
				if (entRptInsumos.Fields.id_municipio == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_municipio";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_municipio";
					return "int para id_municipio";
				}
				if (entRptInsumos.Fields.id_listado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_listado";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_listado";
					return "int para id_listado";
				}
				if (entRptInsumos.Fields.id_boleta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_boleta";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_boleta";
					return "int para id_boleta";
				}
				if (entRptInsumos.Fields.id_persona == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_persona";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_persona";
					return "int para id_persona";
				}
				if (entRptInsumos.Fields.id_movimiento == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_movimiento";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_movimiento";
					return "int para id_movimiento";
				}
				if (entRptInsumos.Fields.control == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para control";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para control";
					return "int para control";
				}
				if (entRptInsumos.Fields.minimo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para minimo";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para minimo";
					return "int para minimo";
				}
				if (entRptInsumos.Fields.maximo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para maximo";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para maximo";
					return "int para maximo";
				}
				if (entRptInsumos.Fields.departamento == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para departamento";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para departamento";
					return "String para departamento";
				}
				if (entRptInsumos.Fields.provincia == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para provincia";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para provincia";
					return "String para provincia";
				}
				if (entRptInsumos.Fields.municipio == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para municipio";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para municipio";
					return "String para municipio";
				}
				if (entRptInsumos.Fields.comunidad == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para comunidad";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para comunidad";
					return "String para comunidad";
				}
				if (entRptInsumos.Fields.login == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para login";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para login";
					return "String para login";
				}
				if (entRptInsumos.Fields.producto_boleta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para producto_boleta";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para producto_boleta";
					return "String para producto_boleta";
				}
				if (entRptInsumos.Fields.codigo_producto == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para codigo_producto";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para codigo_producto";
					return "String para codigo_producto";
				}
				if (entRptInsumos.Fields.codigo_ccp == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para codigo_ccp";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para codigo_ccp";
					return "String para codigo_ccp";
				}
				if (entRptInsumos.Fields.informante == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para informante";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para informante";
					return "String para informante";
				}
				if (entRptInsumos.Fields.telefono == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para telefono";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para telefono";
					return "String para telefono";
				}
				if (entRptInsumos.Fields.direccion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para direccion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para direccion";
					return "String para direccion";
				}
				if (entRptInsumos.Fields.producto == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para producto";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para producto";
					return "String para producto";
				}
				if (entRptInsumos.Fields.caracteristicas == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para caracteristicas";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para caracteristicas";
					return "String para caracteristicas";
				}
				if (entRptInsumos.Fields.variedad == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para variedad";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para variedad";
					return "String para variedad";
				}
				if (entRptInsumos.Fields.origen == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para origen";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para origen";
					return "String para origen";
				}
				if (entRptInsumos.Fields.cantidad == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para cantidad";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para cantidad";
					return "String para cantidad";
				}
				if (entRptInsumos.Fields.unidad == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para unidad";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para unidad";
					return "String para unidad";
				}
				if (entRptInsumos.Fields.precio == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para precio";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para precio";
					return "String para precio";
				}
				if (entRptInsumos.Fields.superficie == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para superficie";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para superficie";
					return "String para superficie";
				}
				if (entRptInsumos.Fields.observaciones == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para observaciones";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para observaciones";
					return "String para observaciones";
				}
				if (entRptInsumos.Fields.minfec == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para minfec";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para minfec";
					return "DateTime para minfec";
				}
				if (entRptInsumos.Fields.maxfec == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para maxfec";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para maxfec";
					return "DateTime para maxfec";
				}
				if (entRptInsumos.Fields.duracion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Decimal para duracion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Decimal para duracion";
					return "Decimal para duracion";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Nombre de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entRptInsumos.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetColumn(entRptInsumos.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entRptInsumos.Fields.gestion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "gestion:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "gestion:";
					return "gestion:";
				}
				if (entRptInsumos.Fields.mes == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "mes:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "mes:";
					return "mes:";
				}
				if (entRptInsumos.Fields.foto == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "foto:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "foto:";
					return "foto:";
				}
				if (entRptInsumos.Fields.id_departamento == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_departamento:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_departamento:";
					return "id_departamento:";
				}
				if (entRptInsumos.Fields.id_upmipp == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_upmipp:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_upmipp:";
					return "id_upmipp:";
				}
				if (entRptInsumos.Fields.id_provincia == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_provincia:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_provincia:";
					return "id_provincia:";
				}
				if (entRptInsumos.Fields.id_municipio == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_municipio:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_municipio:";
					return "id_municipio:";
				}
				if (entRptInsumos.Fields.id_listado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_listado:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_listado:";
					return "id_listado:";
				}
				if (entRptInsumos.Fields.id_boleta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_boleta:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_boleta:";
					return "id_boleta:";
				}
				if (entRptInsumos.Fields.id_persona == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_persona:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_persona:";
					return "id_persona:";
				}
				if (entRptInsumos.Fields.id_movimiento == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_movimiento:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_movimiento:";
					return "id_movimiento:";
				}
				if (entRptInsumos.Fields.control == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "control:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "control:";
					return "control:";
				}
				if (entRptInsumos.Fields.minimo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "minimo:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "minimo:";
					return "minimo:";
				}
				if (entRptInsumos.Fields.maximo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "maximo:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "maximo:";
					return "maximo:";
				}
				if (entRptInsumos.Fields.departamento == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "departamento:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "departamento:";
					return "departamento:";
				}
				if (entRptInsumos.Fields.provincia == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "provincia:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "provincia:";
					return "provincia:";
				}
				if (entRptInsumos.Fields.municipio == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "municipio:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "municipio:";
					return "municipio:";
				}
				if (entRptInsumos.Fields.comunidad == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "comunidad:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "comunidad:";
					return "comunidad:";
				}
				if (entRptInsumos.Fields.login == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "login:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "login:";
					return "login:";
				}
				if (entRptInsumos.Fields.producto_boleta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "producto_boleta:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "producto_boleta:";
					return "producto_boleta:";
				}
				if (entRptInsumos.Fields.codigo_producto == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "codigo_producto:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "codigo_producto:";
					return "codigo_producto:";
				}
				if (entRptInsumos.Fields.codigo_ccp == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "codigo_ccp:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "codigo_ccp:";
					return "codigo_ccp:";
				}
				if (entRptInsumos.Fields.informante == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "informante:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "informante:";
					return "informante:";
				}
				if (entRptInsumos.Fields.telefono == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "telefono:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "telefono:";
					return "telefono:";
				}
				if (entRptInsumos.Fields.direccion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "direccion:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "direccion:";
					return "direccion:";
				}
				if (entRptInsumos.Fields.producto == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "producto:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "producto:";
					return "producto:";
				}
				if (entRptInsumos.Fields.caracteristicas == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "caracteristicas:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "caracteristicas:";
					return "caracteristicas:";
				}
				if (entRptInsumos.Fields.variedad == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "variedad:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "variedad:";
					return "variedad:";
				}
				if (entRptInsumos.Fields.origen == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "origen:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "origen:";
					return "origen:";
				}
				if (entRptInsumos.Fields.cantidad == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "cantidad:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "cantidad:";
					return "cantidad:";
				}
				if (entRptInsumos.Fields.unidad == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "unidad:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "unidad:";
					return "unidad:";
				}
				if (entRptInsumos.Fields.precio == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "precio:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "precio:";
					return "precio:";
				}
				if (entRptInsumos.Fields.superficie == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "superficie:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "superficie:";
					return "superficie:";
				}
				if (entRptInsumos.Fields.observaciones == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "observaciones:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "observaciones:";
					return "observaciones:";
				}
				if (entRptInsumos.Fields.minfec == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "minfec:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "minfec:";
					return "minfec:";
				}
				if (entRptInsumos.Fields.maxfec == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "maxfec:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "maxfec:";
					return "maxfec:";
				}
				if (entRptInsumos.Fields.duracion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "duracion:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "duracion:";
					return "duracion:";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
	}
}

