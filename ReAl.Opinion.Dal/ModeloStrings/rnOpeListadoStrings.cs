#region 
/***********************************************************************************************************
	NOMBRE:       rnOpeListado
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla ope_listado

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        24/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Ine.Sice.Dal.Sociales; 
using Ine.Sice.PgConn.Sociales; 
using Ine.Sice.Dal.Sociales.Entidades;
using Ine.Sice.Dal.Sociales.Interface;
using System.Windows.Forms;
#endregion

namespace Ine.Sice.Dal.Sociales.Modelo
{
	public partial class rnOpeListado: inOpeListado
	{
		public const string strNombreLisForm = "Listado de ope_listado";
		public const string strNombreDocForm = "Detalle de ope_listado";
		public const string strNombreForm = "Registro de ope_listado";
		/// <summary>
		/// 	Funcion que obtiene la Descripcion de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetDesc(entOpeListado.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entOpeListado.Fields.id_listado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para id_listado";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para id_listado";
					return "Valor de tipo int para id_listado";
				}
				if (entOpeListado.Fields.id_upm == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para id_upm";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para id_upm";
					return "Valor de tipo int para id_upm";
				}
				if (entOpeListado.Fields.codigo_manzana_comunidad == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para codigo_manzana_comunidad";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para codigo_manzana_comunidad";
					return "Valor de tipo String para codigo_manzana_comunidad";
				}
				if (entOpeListado.Fields.avenida_calle == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para avenida_calle";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para avenida_calle";
					return "Valor de tipo String para avenida_calle";
				}
				if (entOpeListado.Fields.nro_predio == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para nro_predio";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para nro_predio";
					return "Valor de tipo int para nro_predio";
				}
				if (entOpeListado.Fields.nro_puerta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para nro_puerta";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para nro_puerta";
					return "Valor de tipo String para nro_puerta";
				}
				if (entOpeListado.Fields.nro_orden_vivienda == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para nro_orden_vivienda";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para nro_orden_vivienda";
					return "Valor de tipo int para nro_orden_vivienda";
				}
				if (entOpeListado.Fields.nro_piso == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para nro_piso";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para nro_piso";
					return "Valor de tipo String para nro_piso";
				}
				if (entOpeListado.Fields.nro_depto == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para nro_depto";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para nro_depto";
					return "Valor de tipo String para nro_depto";
				}
				if (entOpeListado.Fields.uso_vivienda == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para uso_vivienda";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para uso_vivienda";
					return "Valor de tipo String para uso_vivienda";
				}
				if (entOpeListado.Fields.nro_hogares == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para nro_hogares";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para nro_hogares";
					return "Valor de tipo int para nro_hogares";
				}
				if (entOpeListado.Fields.nro_hombres == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para nro_hombres";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para nro_hombres";
					return "Valor de tipo int para nro_hombres";
				}
				if (entOpeListado.Fields.nro_mujeres == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para nro_mujeres";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para nro_mujeres";
					return "Valor de tipo int para nro_mujeres";
				}
				if (entOpeListado.Fields.nombre_jefe == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para nombre_jefe";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para nombre_jefe";
					return "Valor de tipo String para nombre_jefe";
				}
				if (entOpeListado.Fields.nro_voe == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para nro_voe";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para nro_voe";
					return "Valor de tipo int para nro_voe";
				}
				if (entOpeListado.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para apiestado";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para apiestado";
					return "Valor de tipo String para apiestado";
				}
				if (entOpeListado.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para usucre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para usucre";
					return "Valor de tipo String para usucre";
				}
				if (entOpeListado.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo DateTime para feccre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo DateTime para feccre";
					return "Valor de tipo DateTime para feccre";
				}
				if (entOpeListado.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para usumod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para usumod";
					return "Valor de tipo String para usumod";
				}
				if (entOpeListado.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo DateTime para fecmod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo DateTime para fecmod";
					return "Valor de tipo DateTime para fecmod";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Error de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetError(entOpeListado.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entOpeListado.Fields.id_listado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_listado";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_listado";
					return "int para id_listado";
				}
				if (entOpeListado.Fields.id_upm == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_upm";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_upm";
					return "int para id_upm";
				}
				if (entOpeListado.Fields.codigo_manzana_comunidad == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para codigo_manzana_comunidad";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para codigo_manzana_comunidad";
					return "String para codigo_manzana_comunidad";
				}
				if (entOpeListado.Fields.avenida_calle == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para avenida_calle";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para avenida_calle";
					return "String para avenida_calle";
				}
				if (entOpeListado.Fields.nro_predio == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para nro_predio";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para nro_predio";
					return "int para nro_predio";
				}
				if (entOpeListado.Fields.nro_puerta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para nro_puerta";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para nro_puerta";
					return "String para nro_puerta";
				}
				if (entOpeListado.Fields.nro_orden_vivienda == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para nro_orden_vivienda";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para nro_orden_vivienda";
					return "int para nro_orden_vivienda";
				}
				if (entOpeListado.Fields.nro_piso == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para nro_piso";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para nro_piso";
					return "String para nro_piso";
				}
				if (entOpeListado.Fields.nro_depto == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para nro_depto";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para nro_depto";
					return "String para nro_depto";
				}
				if (entOpeListado.Fields.uso_vivienda == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para uso_vivienda";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para uso_vivienda";
					return "String para uso_vivienda";
				}
				if (entOpeListado.Fields.nro_hogares == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para nro_hogares";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para nro_hogares";
					return "int para nro_hogares";
				}
				if (entOpeListado.Fields.nro_hombres == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para nro_hombres";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para nro_hombres";
					return "int para nro_hombres";
				}
				if (entOpeListado.Fields.nro_mujeres == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para nro_mujeres";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para nro_mujeres";
					return "int para nro_mujeres";
				}
				if (entOpeListado.Fields.nombre_jefe == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para nombre_jefe";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para nombre_jefe";
					return "String para nombre_jefe";
				}
				if (entOpeListado.Fields.nro_voe == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para nro_voe";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para nro_voe";
					return "int para nro_voe";
				}
				if (entOpeListado.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para apiestado";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para apiestado";
					return "String para apiestado";
				}
				if (entOpeListado.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usucre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usucre";
					return "String para usucre";
				}
				if (entOpeListado.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para feccre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para feccre";
					return "DateTime para feccre";
				}
				if (entOpeListado.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usumod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usumod";
					return "String para usumod";
				}
				if (entOpeListado.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para fecmod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para fecmod";
					return "DateTime para fecmod";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Nombre de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entOpeListado.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetColumn(entOpeListado.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entOpeListado.Fields.id_listado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_listado:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_listado:";
					return "id_listado:";
				}
				if (entOpeListado.Fields.id_upm == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_upm:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_upm:";
					return "id_upm:";
				}
				if (entOpeListado.Fields.codigo_manzana_comunidad == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "codigo_manzana_comunidad:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "codigo_manzana_comunidad:";
					return "codigo_manzana_comunidad:";
				}
				if (entOpeListado.Fields.avenida_calle == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "avenida_calle:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "avenida_calle:";
					return "avenida_calle:";
				}
				if (entOpeListado.Fields.nro_predio == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "nro_predio:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "nro_predio:";
					return "nro_predio:";
				}
				if (entOpeListado.Fields.nro_puerta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "nro_puerta:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "nro_puerta:";
					return "nro_puerta:";
				}
				if (entOpeListado.Fields.nro_orden_vivienda == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "nro_orden_vivienda:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "nro_orden_vivienda:";
					return "nro_orden_vivienda:";
				}
				if (entOpeListado.Fields.nro_piso == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "nro_piso:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "nro_piso:";
					return "nro_piso:";
				}
				if (entOpeListado.Fields.nro_depto == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "nro_depto:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "nro_depto:";
					return "nro_depto:";
				}
				if (entOpeListado.Fields.uso_vivienda == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "uso_vivienda:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "uso_vivienda:";
					return "uso_vivienda:";
				}
				if (entOpeListado.Fields.nro_hogares == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "nro_hogares:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "nro_hogares:";
					return "nro_hogares:";
				}
				if (entOpeListado.Fields.nro_hombres == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "nro_hombres:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "nro_hombres:";
					return "nro_hombres:";
				}
				if (entOpeListado.Fields.nro_mujeres == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "nro_mujeres:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "nro_mujeres:";
					return "nro_mujeres:";
				}
				if (entOpeListado.Fields.nombre_jefe == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "nombre_jefe:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "nombre_jefe:";
					return "nombre_jefe:";
				}
				if (entOpeListado.Fields.nro_voe == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "nro_voe:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "nro_voe:";
					return "nro_voe:";
				}
				if (entOpeListado.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "apiestado:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "apiestado:";
					return "apiestado:";
				}
				if (entOpeListado.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usucre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usucre:";
					return "usucre:";
				}
				if (entOpeListado.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "feccre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "feccre:";
					return "feccre:";
				}
				if (entOpeListado.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usumod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usumod:";
					return "usumod:";
				}
				if (entOpeListado.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "fecmod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "fecmod:";
					return "fecmod:";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
	}
}

