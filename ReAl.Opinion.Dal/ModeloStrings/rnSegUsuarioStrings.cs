#region 
/***********************************************************************************************************
	NOMBRE:       rnSegUsuario
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla seg_usuario

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Ine.Sice.Dal.Sociales; 
using Ine.Sice.PgConn.Sociales; 
using Ine.Sice.Dal.Sociales.Entidades;
using Ine.Sice.Dal.Sociales.Interface;
using System.Windows.Forms;
#endregion

namespace Ine.Sice.Dal.Sociales.Modelo
{
	public partial class rnSegUsuario: inSegUsuario
	{
		public const string strNombreLisForm = "Listado de seg_usuario";
		public const string strNombreDocForm = "Detalle de seg_usuario";
		public const string strNombreForm = "Registro de seg_usuario";
		/// <summary>
		/// 	Funcion que obtiene la Descripcion de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetDesc(entSegUsuario.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entSegUsuario.Fields.id_usuario == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "llave primaria de los usuarios";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "llave primaria de los usuarios";
					return "llave primaria de los usuarios";
				}
				if (entSegUsuario.Fields.id_departamento == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para id_departamento";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para id_departamento";
					return "Valor de tipo int para id_departamento";
				}
				if (entSegUsuario.Fields.login == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para login";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para login";
					return "Valor de tipo String para login";
				}
				if (entSegUsuario.Fields.password == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para password";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para password";
					return "Valor de tipo String para password";
				}
				if (entSegUsuario.Fields.carnet == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo Decimal para carnet";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo Decimal para carnet";
					return "Valor de tipo Decimal para carnet";
				}
				if (entSegUsuario.Fields.nombre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para nombre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para nombre";
					return "Valor de tipo String para nombre";
				}
				if (entSegUsuario.Fields.paterno == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para paterno";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para paterno";
					return "Valor de tipo String para paterno";
				}
				if (entSegUsuario.Fields.materno == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para materno";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para materno";
					return "Valor de tipo String para materno";
				}
				if (entSegUsuario.Fields.direccion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para direccion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para direccion";
					return "Valor de tipo String para direccion";
				}
				if (entSegUsuario.Fields.telefono == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo Decimal para telefono";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo Decimal para telefono";
					return "Valor de tipo Decimal para telefono";
				}
				if (entSegUsuario.Fields.fecha_vigente == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo DateTime para fecha_vigente";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo DateTime para fecha_vigente";
					return "Valor de tipo DateTime para fecha_vigente";
				}
				if (entSegUsuario.Fields.foto == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para foto";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para foto";
					return "Valor de tipo String para foto";
				}
				if (entSegUsuario.Fields.fec_nacimiento == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo DateTime para fec_nacimiento";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo DateTime para fec_nacimiento";
					return "Valor de tipo DateTime para fec_nacimiento";
				}
				if (entSegUsuario.Fields.correo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para correo";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para correo";
					return "Valor de tipo String para correo";
				}
				if (entSegUsuario.Fields.remember_token == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para remember_token";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para remember_token";
					return "Valor de tipo String para remember_token";
				}
				if (entSegUsuario.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para apiestado";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para apiestado";
					return "Valor de tipo String para apiestado";
				}
				if (entSegUsuario.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para usucre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para usucre";
					return "Valor de tipo String para usucre";
				}
				if (entSegUsuario.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo DateTime para feccre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo DateTime para feccre";
					return "Valor de tipo DateTime para feccre";
				}
				if (entSegUsuario.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para usumod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para usumod";
					return "Valor de tipo String para usumod";
				}
				if (entSegUsuario.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo DateTime para fecmod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo DateTime para fecmod";
					return "Valor de tipo DateTime para fecmod";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Error de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetError(entSegUsuario.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entSegUsuario.Fields.id_usuario == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_usuario";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_usuario";
					return "int para id_usuario";
				}
				if (entSegUsuario.Fields.id_departamento == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_departamento";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_departamento";
					return "int para id_departamento";
				}
				if (entSegUsuario.Fields.login == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para login";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para login";
					return "String para login";
				}
				if (entSegUsuario.Fields.password == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para password";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para password";
					return "String para password";
				}
				if (entSegUsuario.Fields.carnet == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Decimal para carnet";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Decimal para carnet";
					return "Decimal para carnet";
				}
				if (entSegUsuario.Fields.nombre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para nombre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para nombre";
					return "String para nombre";
				}
				if (entSegUsuario.Fields.paterno == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para paterno";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para paterno";
					return "String para paterno";
				}
				if (entSegUsuario.Fields.materno == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para materno";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para materno";
					return "String para materno";
				}
				if (entSegUsuario.Fields.direccion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para direccion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para direccion";
					return "String para direccion";
				}
				if (entSegUsuario.Fields.telefono == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Decimal para telefono";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Decimal para telefono";
					return "Decimal para telefono";
				}
				if (entSegUsuario.Fields.fecha_vigente == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para fecha_vigente";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para fecha_vigente";
					return "DateTime para fecha_vigente";
				}
				if (entSegUsuario.Fields.foto == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para foto";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para foto";
					return "String para foto";
				}
				if (entSegUsuario.Fields.fec_nacimiento == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para fec_nacimiento";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para fec_nacimiento";
					return "DateTime para fec_nacimiento";
				}
				if (entSegUsuario.Fields.correo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para correo";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para correo";
					return "String para correo";
				}
				if (entSegUsuario.Fields.remember_token == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para remember_token";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para remember_token";
					return "String para remember_token";
				}
				if (entSegUsuario.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para apiestado";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para apiestado";
					return "String para apiestado";
				}
				if (entSegUsuario.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usucre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usucre";
					return "String para usucre";
				}
				if (entSegUsuario.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para feccre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para feccre";
					return "DateTime para feccre";
				}
				if (entSegUsuario.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usumod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usumod";
					return "String para usumod";
				}
				if (entSegUsuario.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para fecmod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para fecmod";
					return "DateTime para fecmod";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Nombre de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entSegUsuario.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetColumn(entSegUsuario.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entSegUsuario.Fields.id_usuario == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_usuario:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_usuario:";
					return "id_usuario:";
				}
				if (entSegUsuario.Fields.id_departamento == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_departamento:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_departamento:";
					return "id_departamento:";
				}
				if (entSegUsuario.Fields.login == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "login:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "login:";
					return "login:";
				}
				if (entSegUsuario.Fields.password == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "password:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "password:";
					return "password:";
				}
				if (entSegUsuario.Fields.carnet == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "carnet:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "carnet:";
					return "carnet:";
				}
				if (entSegUsuario.Fields.nombre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "nombre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "nombre:";
					return "nombre:";
				}
				if (entSegUsuario.Fields.paterno == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "paterno:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "paterno:";
					return "paterno:";
				}
				if (entSegUsuario.Fields.materno == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "materno:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "materno:";
					return "materno:";
				}
				if (entSegUsuario.Fields.direccion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "direccion:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "direccion:";
					return "direccion:";
				}
				if (entSegUsuario.Fields.telefono == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "telefono:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "telefono:";
					return "telefono:";
				}
				if (entSegUsuario.Fields.fecha_vigente == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "fecha_vigente:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "fecha_vigente:";
					return "fecha_vigente:";
				}
				if (entSegUsuario.Fields.foto == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "foto:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "foto:";
					return "foto:";
				}
				if (entSegUsuario.Fields.fec_nacimiento == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "fec_nacimiento:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "fec_nacimiento:";
					return "fec_nacimiento:";
				}
				if (entSegUsuario.Fields.correo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "correo:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "correo:";
					return "correo:";
				}
				if (entSegUsuario.Fields.remember_token == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "remember_token:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "remember_token:";
					return "remember_token:";
				}
				if (entSegUsuario.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "apiestado:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "apiestado:";
					return "apiestado:";
				}
				if (entSegUsuario.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usucre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usucre:";
					return "usucre:";
				}
				if (entSegUsuario.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "feccre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "feccre:";
					return "feccre:";
				}
				if (entSegUsuario.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usumod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usumod:";
					return "usumod:";
				}
				if (entSegUsuario.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "fecmod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "fecmod:";
					return "fecmod:";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
	}
}

