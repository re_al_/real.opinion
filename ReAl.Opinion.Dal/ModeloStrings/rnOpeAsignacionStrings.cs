#region 
/***********************************************************************************************************
	NOMBRE:       rnOpeAsignacion
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla ope_asignacion

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Ine.Sice.Dal.Sociales; 
using Ine.Sice.PgConn.Sociales; 
using Ine.Sice.Dal.Sociales.Entidades;
using Ine.Sice.Dal.Sociales.Interface;
using System.Windows.Forms;
#endregion

namespace Ine.Sice.Dal.Sociales.Modelo
{
	public partial class rnOpeAsignacion: inOpeAsignacion
	{
		public const string strNombreLisForm = "Listado de ope_asignacion";
		public const string strNombreDocForm = "Detalle de ope_asignacion";
		public const string strNombreForm = "Registro de ope_asignacion";
		/// <summary>
		/// 	Funcion que obtiene la Descripcion de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entOpeAsignacion.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetDesc(entOpeAsignacion.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entOpeAsignacion.Fields.id_asignacion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Es el identificador unico que representa al registro en la tabla";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Es el identificador unico que representa al registro en la tabla";
					return "Es el identificador unico que representa al registro en la tabla";
				}
				if (entOpeAsignacion.Fields.id_usuario == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Es el identificador unico de la tabla seg_usuario";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Es el identificador unico de la tabla seg_usuario";
					return "Es el identificador unico de la tabla seg_usuario";
				}
				if (entOpeAsignacion.Fields.id_proyecto == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Es el identificador unico de la tabla seg_proyecto";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Es el identificador unico de la tabla seg_proyecto";
					return "Es el identificador unico de la tabla seg_proyecto";
				}
				if (entOpeAsignacion.Fields.id_brigada == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Es el identificador unico de la tabla ope_brigada";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Es el identificador unico de la tabla ope_brigada";
					return "Es el identificador unico de la tabla ope_brigada";
				}
				if (entOpeAsignacion.Fields.id_tablet == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Es el identificador unico de la tabla ope_tablet";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Es el identificador unico de la tabla ope_tablet";
					return "Es el identificador unico de la tabla ope_tablet";
				}
				if (entOpeAsignacion.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Estado en el que se encuentra el registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Estado en el que se encuentra el registro";
					return "Estado en el que se encuentra el registro";
				}
				if (entOpeAsignacion.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Login o nombre de usuario que ha creado el registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Login o nombre de usuario que ha creado el registro";
					return "Login o nombre de usuario que ha creado el registro";
				}
				if (entOpeAsignacion.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Fecha de creacion del registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Fecha de creacion del registro";
					return "Fecha de creacion del registro";
				}
				if (entOpeAsignacion.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro";
					return "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro";
				}
				if (entOpeAsignacion.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Fecha en la que se ha realizado la ULTIMA modificacion registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Fecha en la que se ha realizado la ULTIMA modificacion registro";
					return "Fecha en la que se ha realizado la ULTIMA modificacion registro";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Error de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entOpeAsignacion.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetError(entOpeAsignacion.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entOpeAsignacion.Fields.id_asignacion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_asignacion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_asignacion";
					return "int para id_asignacion";
				}
				if (entOpeAsignacion.Fields.id_usuario == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_usuario";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_usuario";
					return "int para id_usuario";
				}
				if (entOpeAsignacion.Fields.id_proyecto == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_proyecto";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_proyecto";
					return "int para id_proyecto";
				}
				if (entOpeAsignacion.Fields.id_brigada == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_brigada";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_brigada";
					return "int para id_brigada";
				}
				if (entOpeAsignacion.Fields.id_tablet == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_tablet";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_tablet";
					return "int para id_tablet";
				}
				if (entOpeAsignacion.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para apiestado";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para apiestado";
					return "String para apiestado";
				}
				if (entOpeAsignacion.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usucre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usucre";
					return "String para usucre";
				}
				if (entOpeAsignacion.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para feccre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para feccre";
					return "DateTime para feccre";
				}
				if (entOpeAsignacion.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usumod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usumod";
					return "String para usumod";
				}
				if (entOpeAsignacion.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para fecmod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para fecmod";
					return "DateTime para fecmod";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Nombre de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entOpeAsignacion.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetColumn(entOpeAsignacion.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entOpeAsignacion.Fields.id_asignacion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_asignacion:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_asignacion:";
					return "id_asignacion:";
				}
				if (entOpeAsignacion.Fields.id_usuario == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_usuario:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_usuario:";
					return "id_usuario:";
				}
				if (entOpeAsignacion.Fields.id_proyecto == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_proyecto:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_proyecto:";
					return "id_proyecto:";
				}
				if (entOpeAsignacion.Fields.id_brigada == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_brigada:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_brigada:";
					return "id_brigada:";
				}
				if (entOpeAsignacion.Fields.id_tablet == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_tablet:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_tablet:";
					return "id_tablet:";
				}
				if (entOpeAsignacion.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "apiestado:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "apiestado:";
					return "apiestado:";
				}
				if (entOpeAsignacion.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usucre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usucre:";
					return "usucre:";
				}
				if (entOpeAsignacion.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "feccre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "feccre:";
					return "feccre:";
				}
				if (entOpeAsignacion.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usumod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usumod:";
					return "usumod:";
				}
				if (entOpeAsignacion.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "fecmod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "fecmod:";
					return "fecmod:";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
	}
}

