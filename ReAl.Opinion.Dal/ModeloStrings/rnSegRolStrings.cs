#region 
/***********************************************************************************************************
	NOMBRE:       rnSegRol
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla seg_rol

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Ine.Sice.Dal.Sociales; 
using Ine.Sice.PgConn.Sociales; 
using Ine.Sice.Dal.Sociales.Entidades;
using Ine.Sice.Dal.Sociales.Interface;
using System.Windows.Forms;
#endregion

namespace Ine.Sice.Dal.Sociales.Modelo
{
	public partial class rnSegRol: inSegRol
	{
		public const string strNombreLisForm = "Listado de seg_rol";
		public const string strNombreDocForm = "Detalle de seg_rol";
		public const string strNombreForm = "Registro de seg_rol";
		/// <summary>
		/// 	Funcion que obtiene la Descripcion de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entSegRol.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetDesc(entSegRol.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entSegRol.Fields.id_rol == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para id_rol";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para id_rol";
					return "Valor de tipo int para id_rol";
				}
				if (entSegRol.Fields.sigla == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para sigla";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para sigla";
					return "Valor de tipo String para sigla";
				}
				if (entSegRol.Fields.descripcion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para descripcion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para descripcion";
					return "Valor de tipo String para descripcion";
				}
				if (entSegRol.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Describe el estado en el que se encuentra un determinado registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Describe el estado en el que se encuentra un determinado registro";
					return "Describe el estado en el que se encuentra un determinado registro";
				}
				if (entSegRol.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Describe el usuario que creo un determinado un registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Describe el usuario que creo un determinado un registro";
					return "Describe el usuario que creo un determinado un registro";
				}
				if (entSegRol.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Describe la fecha de creaciÃ³n de un determinado registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Describe la fecha de creaciÃ³n de un determinado registro";
					return "Describe la fecha de creaciÃ³n de un determinado registro";
				}
				if (entSegRol.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Describe el usuario que modifico un determinado registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Describe el usuario que modifico un determinado registro";
					return "Describe el usuario que modifico un determinado registro";
				}
				if (entSegRol.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Describe la fecha de modificacciÃ³n de determinado un registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Describe la fecha de modificacciÃ³n de determinado un registro";
					return "Describe la fecha de modificacciÃ³n de determinado un registro";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Error de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entSegRol.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetError(entSegRol.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entSegRol.Fields.id_rol == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_rol";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_rol";
					return "int para id_rol";
				}
				if (entSegRol.Fields.sigla == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para sigla";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para sigla";
					return "String para sigla";
				}
				if (entSegRol.Fields.descripcion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para descripcion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para descripcion";
					return "String para descripcion";
				}
				if (entSegRol.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para apiestado";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para apiestado";
					return "String para apiestado";
				}
				if (entSegRol.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usucre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usucre";
					return "String para usucre";
				}
				if (entSegRol.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para feccre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para feccre";
					return "DateTime para feccre";
				}
				if (entSegRol.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usumod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usumod";
					return "String para usumod";
				}
				if (entSegRol.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para fecmod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para fecmod";
					return "DateTime para fecmod";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Nombre de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entSegRol.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetColumn(entSegRol.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entSegRol.Fields.id_rol == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_rol:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_rol:";
					return "id_rol:";
				}
				if (entSegRol.Fields.sigla == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "sigla:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "sigla:";
					return "sigla:";
				}
				if (entSegRol.Fields.descripcion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "descripcion:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "descripcion:";
					return "descripcion:";
				}
				if (entSegRol.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "apiestado:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "apiestado:";
					return "apiestado:";
				}
				if (entSegRol.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usucre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usucre:";
					return "usucre:";
				}
				if (entSegRol.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "feccre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "feccre:";
					return "feccre:";
				}
				if (entSegRol.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usumod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usumod:";
					return "usumod:";
				}
				if (entSegRol.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "fecmod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "fecmod:";
					return "fecmod:";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
	}
}

