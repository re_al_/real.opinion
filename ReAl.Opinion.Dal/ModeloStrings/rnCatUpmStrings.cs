#region 
/***********************************************************************************************************
	NOMBRE:       rnCatUpm
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla cat_upm

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Ine.Sice.Dal.Sociales; 
using Ine.Sice.PgConn.Sociales; 
using Ine.Sice.Dal.Sociales.Entidades;
using Ine.Sice.Dal.Sociales.Interface;
using System.Windows.Forms;
#endregion

namespace Ine.Sice.Dal.Sociales.Modelo
{
	public partial class rnCatUpm: inCatUpm
	{
		public const string strNombreLisForm = "Listado de cat_upm";
		public const string strNombreDocForm = "Detalle de cat_upm";
		public const string strNombreForm = "Registro de cat_upm";
		/// <summary>
		/// 	Funcion que obtiene la Descripcion de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entCatUpm.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetDesc(entCatUpm.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entCatUpm.Fields.id_upm == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Es el identificador unico que representa al registro en la tabla";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Es el identificador unico que representa al registro en la tabla";
					return "Es el identificador unico que representa al registro en la tabla";
				}
				if (entCatUpm.Fields.id_proyecto == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Es el identificador unico de la tabla seg_proyecto";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Es el identificador unico de la tabla seg_proyecto";
					return "Es el identificador unico de la tabla seg_proyecto";
				}
				if (entCatUpm.Fields.id_departamento == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Es el identificador unico de la tabla cat_departamento";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Es el identificador unico de la tabla cat_departamento";
					return "Es el identificador unico de la tabla cat_departamento";
				}
				if (entCatUpm.Fields.codigo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Codigo con el que se representa a la UPM";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Codigo con el que se representa a la UPM";
					return "Codigo con el que se representa a la UPM";
				}
				if (entCatUpm.Fields.nombre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Nombre con el que se representa a la UPM";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Nombre con el que se representa a la UPM";
					return "Nombre con el que se representa a la UPM";
				}
				if (entCatUpm.Fields.fecinicio == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Fecha desde la que es valida la UPM";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Fecha desde la que es valida la UPM";
					return "Fecha desde la que es valida la UPM";
				}
				if (entCatUpm.Fields.latitud == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Latitud del punto CERO de la Capital de Departamento";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Latitud del punto CERO de la Capital de Departamento";
					return "Latitud del punto CERO de la Capital de Departamento";
				}
				if (entCatUpm.Fields.longitud == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Longitud del punto CERO de la Capital de Departamento";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Longitud del punto CERO de la Capital de Departamento";
					return "Longitud del punto CERO de la Capital de Departamento";
				}
				if (entCatUpm.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Estado en el que se encuentra el registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Estado en el que se encuentra el registro";
					return "Estado en el que se encuentra el registro";
				}
				if (entCatUpm.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Login o nombre de usuario que ha creado el registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Login o nombre de usuario que ha creado el registro";
					return "Login o nombre de usuario que ha creado el registro";
				}
				if (entCatUpm.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Fecha de creacion del registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Fecha de creacion del registro";
					return "Fecha de creacion del registro";
				}
				if (entCatUpm.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro";
					return "Login o nombre de usuario que ha realizado la ULTIMA modificacion registro";
				}
				if (entCatUpm.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Fecha en la que se ha realizado la ULTIMA modificacion registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Fecha en la que se ha realizado la ULTIMA modificacion registro";
					return "Fecha en la que se ha realizado la ULTIMA modificacion registro";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Error de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entCatUpm.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetError(entCatUpm.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entCatUpm.Fields.id_upm == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_upm";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_upm";
					return "int para id_upm";
				}
				if (entCatUpm.Fields.id_proyecto == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_proyecto";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_proyecto";
					return "int para id_proyecto";
				}
				if (entCatUpm.Fields.id_departamento == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_departamento";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_departamento";
					return "int para id_departamento";
				}
				if (entCatUpm.Fields.codigo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para codigo";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para codigo";
					return "String para codigo";
				}
				if (entCatUpm.Fields.nombre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para nombre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para nombre";
					return "String para nombre";
				}
				if (entCatUpm.Fields.fecinicio == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para fecinicio";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para fecinicio";
					return "DateTime para fecinicio";
				}
				if (entCatUpm.Fields.latitud == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Decimal para latitud";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Decimal para latitud";
					return "Decimal para latitud";
				}
				if (entCatUpm.Fields.longitud == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Decimal para longitud";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Decimal para longitud";
					return "Decimal para longitud";
				}
				if (entCatUpm.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para apiestado";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para apiestado";
					return "String para apiestado";
				}
				if (entCatUpm.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usucre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usucre";
					return "String para usucre";
				}
				if (entCatUpm.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para feccre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para feccre";
					return "DateTime para feccre";
				}
				if (entCatUpm.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usumod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usumod";
					return "String para usumod";
				}
				if (entCatUpm.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para fecmod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para fecmod";
					return "DateTime para fecmod";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Nombre de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entCatUpm.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetColumn(entCatUpm.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entCatUpm.Fields.id_upm == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_upm:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_upm:";
					return "id_upm:";
				}
				if (entCatUpm.Fields.id_proyecto == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_proyecto:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_proyecto:";
					return "id_proyecto:";
				}
				if (entCatUpm.Fields.id_departamento == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_departamento:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_departamento:";
					return "id_departamento:";
				}
				if (entCatUpm.Fields.codigo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "codigo:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "codigo:";
					return "codigo:";
				}
				if (entCatUpm.Fields.nombre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "nombre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "nombre:";
					return "nombre:";
				}
				if (entCatUpm.Fields.fecinicio == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "fecinicio:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "fecinicio:";
					return "fecinicio:";
				}
				if (entCatUpm.Fields.latitud == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "latitud:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "latitud:";
					return "latitud:";
				}
				if (entCatUpm.Fields.longitud == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "longitud:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "longitud:";
					return "longitud:";
				}
				if (entCatUpm.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "apiestado:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "apiestado:";
					return "apiestado:";
				}
				if (entCatUpm.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usucre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usucre:";
					return "usucre:";
				}
				if (entCatUpm.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "feccre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "feccre:";
					return "feccre:";
				}
				if (entCatUpm.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usumod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usumod:";
					return "usumod:";
				}
				if (entCatUpm.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "fecmod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "fecmod:";
					return "fecmod:";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
	}
}

