#region 
/***********************************************************************************************************
	NOMBRE:       rnEncPregunta
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla enc_pregunta

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Ine.Sice.Dal.Sociales; 
using Ine.Sice.PgConn.Sociales; 
using Ine.Sice.Dal.Sociales.Entidades;
using Ine.Sice.Dal.Sociales.Interface;
using System.Windows.Forms;
#endregion

namespace Ine.Sice.Dal.Sociales.Modelo
{
	public partial class rnEncPregunta: inEncPregunta
	{
		public const string strNombreLisForm = "Listado de enc_pregunta";
		public const string strNombreDocForm = "Detalle de enc_pregunta";
		public const string strNombreForm = "Registro de enc_pregunta";
		/// <summary>
		/// 	Funcion que obtiene la Descripcion de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetDesc(entEncPregunta.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entEncPregunta.Fields.id_pregunta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo Int64 para id_pregunta";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo Int64 para id_pregunta";
					return "Valor de tipo Int64 para id_pregunta";
				}
				if (entEncPregunta.Fields.id_proyecto == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para id_proyecto";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para id_proyecto";
					return "Valor de tipo int para id_proyecto";
				}
				if (entEncPregunta.Fields.id_nivel == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para id_nivel";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para id_nivel";
					return "Valor de tipo int para id_nivel";
				}
				if (entEncPregunta.Fields.id_seccion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para id_seccion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para id_seccion";
					return "Valor de tipo int para id_seccion";
				}
				if (entEncPregunta.Fields.codigo_pregunta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para codigo_pregunta";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para codigo_pregunta";
					return "Valor de tipo String para codigo_pregunta";
				}
				if (entEncPregunta.Fields.pregunta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para pregunta";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para pregunta";
					return "Valor de tipo String para pregunta";
				}
				if (entEncPregunta.Fields.ayuda == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para ayuda";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para ayuda";
					return "Valor de tipo String para ayuda";
				}
				if (entEncPregunta.Fields.id_tipo_pregunta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para id_tipo_pregunta";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para id_tipo_pregunta";
					return "Valor de tipo int para id_tipo_pregunta";
				}
				if (entEncPregunta.Fields.minimo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para minimo";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para minimo";
					return "Valor de tipo int para minimo";
				}
				if (entEncPregunta.Fields.maximo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para maximo";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para maximo";
					return "Valor de tipo int para maximo";
				}
				if (entEncPregunta.Fields.catalogo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para catalogo";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para catalogo";
					return "Valor de tipo String para catalogo";
				}
				if (entEncPregunta.Fields.longitud == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para longitud";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para longitud";
					return "Valor de tipo int para longitud";
				}
				if (entEncPregunta.Fields.codigo_especifique == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para codigo_especifique";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para codigo_especifique";
					return "Valor de tipo String para codigo_especifique";
				}
				if (entEncPregunta.Fields.mostrar_ventana == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para mostrar_ventana";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para mostrar_ventana";
					return "Valor de tipo int para mostrar_ventana";
				}
				if (entEncPregunta.Fields.variable == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para variable";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para variable";
					return "Valor de tipo String para variable";
				}
				if (entEncPregunta.Fields.formula == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para formula";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para formula";
					return "Valor de tipo String para formula";
				}
				if (entEncPregunta.Fields.rpn_formula == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para rpn_formula";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para rpn_formula";
					return "Valor de tipo String para rpn_formula";
				}
				if (entEncPregunta.Fields.regla == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para regla";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para regla";
					return "Valor de tipo String para regla";
				}
				if (entEncPregunta.Fields.rpn == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para rpn";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para rpn";
					return "Valor de tipo String para rpn";
				}
				if (entEncPregunta.Fields.mensaje == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para mensaje";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para mensaje";
					return "Valor de tipo String para mensaje";
				}
				if (entEncPregunta.Fields.revision == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para revision";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para revision";
					return "Valor de tipo String para revision";
				}
				if (entEncPregunta.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para apiestado";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para apiestado";
					return "Valor de tipo String para apiestado";
				}
				if (entEncPregunta.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para usucre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para usucre";
					return "Valor de tipo String para usucre";
				}
				if (entEncPregunta.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo DateTime para feccre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo DateTime para feccre";
					return "Valor de tipo DateTime para feccre";
				}
				if (entEncPregunta.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para usumod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para usumod";
					return "Valor de tipo String para usumod";
				}
				if (entEncPregunta.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo DateTime para fecmod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo DateTime para fecmod";
					return "Valor de tipo DateTime para fecmod";
				}
				if (entEncPregunta.Fields.instruccion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "En caso de ser necesario una instruccion en PopUp";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "En caso de ser necesario una instruccion en PopUp";
					return "En caso de ser necesario una instruccion en PopUp";
				}
				if (entEncPregunta.Fields.bucle == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Indica si esta pregunta es un FIN DE BUCLE (1) o No (0)";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Indica si esta pregunta es un FIN DE BUCLE (1) o No (0)";
					return "Indica si esta pregunta es un FIN DE BUCLE (1) o No (0)";
				}
				if (entEncPregunta.Fields.variable_bucle == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Necesario para las preguntas del Tipo BUCLE, en cuyo caso, se usara este campo para indicar las posibles respuestas";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Necesario para las preguntas del Tipo BUCLE, en cuyo caso, se usara este campo para indicar las posibles respuestas";
					return "Necesario para las preguntas del Tipo BUCLE, en cuyo caso, se usara este campo para indicar las posibles respuestas";
				}
				if (entEncPregunta.Fields.codigo_especial == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para codigo_especial";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para codigo_especial";
					return "Valor de tipo int para codigo_especial";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Error de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetError(entEncPregunta.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entEncPregunta.Fields.id_pregunta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Int64 para id_pregunta";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Int64 para id_pregunta";
					return "Int64 para id_pregunta";
				}
				if (entEncPregunta.Fields.id_proyecto == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_proyecto";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_proyecto";
					return "int para id_proyecto";
				}
				if (entEncPregunta.Fields.id_nivel == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_nivel";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_nivel";
					return "int para id_nivel";
				}
				if (entEncPregunta.Fields.id_seccion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_seccion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_seccion";
					return "int para id_seccion";
				}
				if (entEncPregunta.Fields.codigo_pregunta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para codigo_pregunta";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para codigo_pregunta";
					return "String para codigo_pregunta";
				}
				if (entEncPregunta.Fields.pregunta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para pregunta";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para pregunta";
					return "String para pregunta";
				}
				if (entEncPregunta.Fields.ayuda == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para ayuda";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para ayuda";
					return "String para ayuda";
				}
				if (entEncPregunta.Fields.id_tipo_pregunta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_tipo_pregunta";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_tipo_pregunta";
					return "int para id_tipo_pregunta";
				}
				if (entEncPregunta.Fields.minimo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para minimo";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para minimo";
					return "int para minimo";
				}
				if (entEncPregunta.Fields.maximo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para maximo";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para maximo";
					return "int para maximo";
				}
				if (entEncPregunta.Fields.catalogo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para catalogo";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para catalogo";
					return "String para catalogo";
				}
				if (entEncPregunta.Fields.longitud == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para longitud";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para longitud";
					return "int para longitud";
				}
				if (entEncPregunta.Fields.codigo_especifique == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para codigo_especifique";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para codigo_especifique";
					return "String para codigo_especifique";
				}
				if (entEncPregunta.Fields.mostrar_ventana == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para mostrar_ventana";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para mostrar_ventana";
					return "int para mostrar_ventana";
				}
				if (entEncPregunta.Fields.variable == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para variable";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para variable";
					return "String para variable";
				}
				if (entEncPregunta.Fields.formula == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para formula";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para formula";
					return "String para formula";
				}
				if (entEncPregunta.Fields.rpn_formula == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para rpn_formula";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para rpn_formula";
					return "String para rpn_formula";
				}
				if (entEncPregunta.Fields.regla == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para regla";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para regla";
					return "String para regla";
				}
				if (entEncPregunta.Fields.rpn == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para rpn";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para rpn";
					return "String para rpn";
				}
				if (entEncPregunta.Fields.mensaje == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para mensaje";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para mensaje";
					return "String para mensaje";
				}
				if (entEncPregunta.Fields.revision == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para revision";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para revision";
					return "String para revision";
				}
				if (entEncPregunta.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para apiestado";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para apiestado";
					return "String para apiestado";
				}
				if (entEncPregunta.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usucre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usucre";
					return "String para usucre";
				}
				if (entEncPregunta.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para feccre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para feccre";
					return "DateTime para feccre";
				}
				if (entEncPregunta.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usumod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usumod";
					return "String para usumod";
				}
				if (entEncPregunta.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para fecmod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para fecmod";
					return "DateTime para fecmod";
				}
				if (entEncPregunta.Fields.instruccion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para instruccion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para instruccion";
					return "String para instruccion";
				}
				if (entEncPregunta.Fields.bucle == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para bucle";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para bucle";
					return "int para bucle";
				}
				if (entEncPregunta.Fields.variable_bucle == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para variable_bucle";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para variable_bucle";
					return "String para variable_bucle";
				}
				if (entEncPregunta.Fields.codigo_especial == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para codigo_especial";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para codigo_especial";
					return "int para codigo_especial";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Nombre de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entEncPregunta.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetColumn(entEncPregunta.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entEncPregunta.Fields.id_pregunta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_pregunta:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_pregunta:";
					return "id_pregunta:";
				}
				if (entEncPregunta.Fields.id_proyecto == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_proyecto:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_proyecto:";
					return "id_proyecto:";
				}
				if (entEncPregunta.Fields.id_nivel == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_nivel:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_nivel:";
					return "id_nivel:";
				}
				if (entEncPregunta.Fields.id_seccion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_seccion:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_seccion:";
					return "id_seccion:";
				}
				if (entEncPregunta.Fields.codigo_pregunta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "codigo_pregunta:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "codigo_pregunta:";
					return "codigo_pregunta:";
				}
				if (entEncPregunta.Fields.pregunta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "pregunta:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "pregunta:";
					return "pregunta:";
				}
				if (entEncPregunta.Fields.ayuda == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "ayuda:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "ayuda:";
					return "ayuda:";
				}
				if (entEncPregunta.Fields.id_tipo_pregunta == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_tipo_pregunta:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_tipo_pregunta:";
					return "id_tipo_pregunta:";
				}
				if (entEncPregunta.Fields.minimo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "minimo:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "minimo:";
					return "minimo:";
				}
				if (entEncPregunta.Fields.maximo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "maximo:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "maximo:";
					return "maximo:";
				}
				if (entEncPregunta.Fields.catalogo == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "catalogo:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "catalogo:";
					return "catalogo:";
				}
				if (entEncPregunta.Fields.longitud == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "longitud:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "longitud:";
					return "longitud:";
				}
				if (entEncPregunta.Fields.codigo_especifique == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "codigo_especifique:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "codigo_especifique:";
					return "codigo_especifique:";
				}
				if (entEncPregunta.Fields.mostrar_ventana == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "mostrar_ventana:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "mostrar_ventana:";
					return "mostrar_ventana:";
				}
				if (entEncPregunta.Fields.variable == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "variable:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "variable:";
					return "variable:";
				}
				if (entEncPregunta.Fields.formula == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "formula:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "formula:";
					return "formula:";
				}
				if (entEncPregunta.Fields.rpn_formula == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "rpn_formula:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "rpn_formula:";
					return "rpn_formula:";
				}
				if (entEncPregunta.Fields.regla == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "regla:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "regla:";
					return "regla:";
				}
				if (entEncPregunta.Fields.rpn == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "rpn:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "rpn:";
					return "rpn:";
				}
				if (entEncPregunta.Fields.mensaje == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "mensaje:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "mensaje:";
					return "mensaje:";
				}
				if (entEncPregunta.Fields.revision == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "revision:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "revision:";
					return "revision:";
				}
				if (entEncPregunta.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "apiestado:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "apiestado:";
					return "apiestado:";
				}
				if (entEncPregunta.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usucre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usucre:";
					return "usucre:";
				}
				if (entEncPregunta.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "feccre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "feccre:";
					return "feccre:";
				}
				if (entEncPregunta.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usumod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usumod:";
					return "usumod:";
				}
				if (entEncPregunta.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "fecmod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "fecmod:";
					return "fecmod:";
				}
				if (entEncPregunta.Fields.instruccion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "instruccion:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "instruccion:";
					return "instruccion:";
				}
				if (entEncPregunta.Fields.bucle == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "bucle:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "bucle:";
					return "bucle:";
				}
				if (entEncPregunta.Fields.variable_bucle == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "variable_bucle:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "variable_bucle:";
					return "variable_bucle:";
				}
				if (entEncPregunta.Fields.codigo_especial == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "codigo_especial:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "codigo_especial:";
					return "codigo_especial:";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
	}
}

