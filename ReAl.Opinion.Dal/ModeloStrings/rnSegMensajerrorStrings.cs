#region 
/***********************************************************************************************************
	NOMBRE:       rnSegMensajerror
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla seg_mensajerror

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        22/10/2014  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Ine.Sice.PgConn.Sociales; 
using Ine.Sice.Dal.Sociales.Entidades;
using Ine.Sice.Dal.Sociales.Interface;
using System.Windows.Forms;
#endregion

namespace Ine.Sice.Dal.Sociales.Modelo
{
	public partial class rnSegMensajerror: inSegMensajerror
	{
		public const string strNombreLisForm = "Listado de seg_mensajerror";
		public const string strNombreDocForm = "Detalle de seg_mensajerror";
		public const string strNombreForm = "Registro de seg_mensajerror";
		/// <summary>
		/// 	Funcion que obtiene la Descripcion de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entSegMensajerror.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetDesc(entSegMensajerror.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entSegMensajerror.Fields.id_error == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo int para id_error";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo int para id_error";
					return "Valor de tipo int para id_error";
				}
				if (entSegMensajerror.Fields.id_aplicacion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para id_aplicacion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para id_aplicacion";
					return "Valor de tipo String para id_aplicacion";
				}
				if (entSegMensajerror.Fields.aplicacionerror == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para aplicacionerror";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para aplicacionerror";
					return "Valor de tipo String para aplicacionerror";
				}
				if (entSegMensajerror.Fields.descripcion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para descripcion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para descripcion";
					return "Valor de tipo String para descripcion";
				}
				if (entSegMensajerror.Fields.causa == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para causa";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para causa";
					return "Valor de tipo String para causa";
				}
				if (entSegMensajerror.Fields.accion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para accion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para accion";
					return "Valor de tipo String para accion";
				}
				if (entSegMensajerror.Fields.comentario == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para comentario";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para comentario";
					return "Valor de tipo String para comentario";
				}
				if (entSegMensajerror.Fields.origen == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Valor de tipo String para origen";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Valor de tipo String para origen";
					return "Valor de tipo String para origen";
				}
				if (entSegMensajerror.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Describe el estado en el que se encuentra un determinado registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Describe el estado en el que se encuentra un determinado registro";
					return "Describe el estado en el que se encuentra un determinado registro";
				}
				if (entSegMensajerror.Fields.apitransaccion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Describe la trasacciÃ³n en la que se encuentra un registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Describe la trasacciÃ³n en la que se encuentra un registro";
					return "Describe la trasacciÃ³n en la que se encuentra un registro";
				}
				if (entSegMensajerror.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Describe el usuario que creo un determinado un registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Describe el usuario que creo un determinado un registro";
					return "Describe el usuario que creo un determinado un registro";
				}
				if (entSegMensajerror.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Describe la fecha de creaciÃ³n de un determinado registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Describe la fecha de creaciÃ³n de un determinado registro";
					return "Describe la fecha de creaciÃ³n de un determinado registro";
				}
				if (entSegMensajerror.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Describe el usuario que modifico un determinado registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Describe el usuario que modifico un determinado registro";
					return "Describe el usuario que modifico un determinado registro";
				}
				if (entSegMensajerror.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "Describe la fecha de modificacciÃ³n de determinado un registro";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "Describe la fecha de modificacciÃ³n de determinado un registro";
					return "Describe la fecha de modificacciÃ³n de determinado un registro";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Error de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entSegMensajerror.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetError(entSegMensajerror.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entSegMensajerror.Fields.id_error == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "int para id_error";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "int para id_error";
					return "int para id_error";
				}
				if (entSegMensajerror.Fields.id_aplicacion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para id_aplicacion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para id_aplicacion";
					return "String para id_aplicacion";
				}
				if (entSegMensajerror.Fields.aplicacionerror == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para aplicacionerror";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para aplicacionerror";
					return "String para aplicacionerror";
				}
				if (entSegMensajerror.Fields.descripcion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para descripcion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para descripcion";
					return "String para descripcion";
				}
				if (entSegMensajerror.Fields.causa == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para causa";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para causa";
					return "String para causa";
				}
				if (entSegMensajerror.Fields.accion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para accion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para accion";
					return "String para accion";
				}
				if (entSegMensajerror.Fields.comentario == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para comentario";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para comentario";
					return "String para comentario";
				}
				if (entSegMensajerror.Fields.origen == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para origen";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para origen";
					return "String para origen";
				}
				if (entSegMensajerror.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para apiestado";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para apiestado";
					return "String para apiestado";
				}
				if (entSegMensajerror.Fields.apitransaccion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para apitransaccion";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para apitransaccion";
					return "String para apitransaccion";
				}
				if (entSegMensajerror.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usucre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usucre";
					return "String para usucre";
				}
				if (entSegMensajerror.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para feccre";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para feccre";
					return "DateTime para feccre";
				}
				if (entSegMensajerror.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "String para usumod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "String para usumod";
					return "String para usumod";
				}
				if (entSegMensajerror.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "DateTime para fecmod";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "DateTime para fecmod";
					return "DateTime para fecmod";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene el Nombre de Determinado Campo
		/// </summary>
		/// <param name="myField" type="entSegMensajerror.Fileds">
		///     <para>
		/// 		 Campo solicitado
		///     </para>
		/// </param>
		/// <param name="idioma" type="System.Globalization.CultureInfo">
		///     <para>
		/// 		 Idioma
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo STRING que describe al parametro
		/// </returns>
		public static string GetColumn(entSegMensajerror.Fields myField, CultureInfo idioma = null)
		{
			try
			{
				if (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;
				
				if (entSegMensajerror.Fields.id_error == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_error:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_error:";
					return "id_error:";
				}
				if (entSegMensajerror.Fields.id_aplicacion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "id_aplicacion:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "id_aplicacion:";
					return "id_aplicacion:";
				}
				if (entSegMensajerror.Fields.aplicacionerror == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "aplicacionerror:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "aplicacionerror:";
					return "aplicacionerror:";
				}
				if (entSegMensajerror.Fields.descripcion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "descripcion:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "descripcion:";
					return "descripcion:";
				}
				if (entSegMensajerror.Fields.causa == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "causa:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "causa:";
					return "causa:";
				}
				if (entSegMensajerror.Fields.accion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "accion:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "accion:";
					return "accion:";
				}
				if (entSegMensajerror.Fields.comentario == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "comentario:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "comentario:";
					return "comentario:";
				}
				if (entSegMensajerror.Fields.origen == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "origen:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "origen:";
					return "origen:";
				}
				if (entSegMensajerror.Fields.apiestado == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "apiestado:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "apiestado:";
					return "apiestado:";
				}
				if (entSegMensajerror.Fields.apitransaccion == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "apitransaccion:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "apitransaccion:";
					return "apitransaccion:";
				}
				if (entSegMensajerror.Fields.usucre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usucre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usucre:";
					return "usucre:";
				}
				if (entSegMensajerror.Fields.feccre == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "feccre:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "feccre:";
					return "feccre:";
				}
				if (entSegMensajerror.Fields.usumod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "usumod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "usumod:";
					return "usumod:";
				}
				if (entSegMensajerror.Fields.fecmod == myField)
				{
					if (idioma.Name.ToUpper().StartsWith("ES")) return "fecmod:";
					if (idioma.Name.ToUpper().StartsWith("EN")) return "fecmod:";
					return "fecmod:";
				}
				return string.Empty;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
	}
}

