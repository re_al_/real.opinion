namespace ReAl.Opinion.Dal
{
	public static class cApi
	{
		public enum Estado
		{
            ELABORADO,
            ELIMINADO,
            ACTIVO
        }

		public enum Transaccion
		{
            MODIFICAR,
            CREAR,
            ELIMINAR
		}
	}
}

