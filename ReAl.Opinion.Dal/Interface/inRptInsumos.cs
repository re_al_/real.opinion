#region 
/***********************************************************************************************************
	NOMBRE:       inRptInsumos
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla rpt_insumos

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        22/10/2014  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.Opinion.PgConn;
using ReAl.Opinion.Dal.Entidades;
using System.Windows.Forms;
#endregion

namespace ReAl.Opinion.Dal.Interface
{
	public interface inRptInsumos: IDisposable
	{
		string getTableScript();
		dynamic GetColumnType(object valor,entRptInsumos.Fields myField);
		entRptInsumos ObtenerObjeto(int intgestion);
		entRptInsumos ObtenerObjeto(int intgestion, ref cTrans localTrans);
		entRptInsumos ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		entRptInsumos ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		entRptInsumos ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		entRptInsumos ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		entRptInsumos ObtenerObjeto(entRptInsumos.Fields searchField, object searchValue);
		entRptInsumos ObtenerObjeto(entRptInsumos.Fields searchField, object searchValue, ref cTrans localTrans);
		entRptInsumos ObtenerObjeto(entRptInsumos.Fields searchField, object searchValue, string strParamAdicionales);
		entRptInsumos ObtenerObjeto(entRptInsumos.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Dictionary<String, entRptInsumos> ObtenerDiccionario();
		Dictionary<String, entRptInsumos> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, entRptInsumos> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Dictionary<String, entRptInsumos> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, entRptInsumos> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Dictionary<String, entRptInsumos> ObtenerDiccionario(entRptInsumos.Fields searchField, object searchValue);
		Dictionary<String, entRptInsumos> ObtenerDiccionario(entRptInsumos.Fields searchField, object searchValue, ref cTrans localTrans);
		Dictionary<String, entRptInsumos> ObtenerDiccionario(entRptInsumos.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, entRptInsumos> ObtenerDiccionario(entRptInsumos.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entRptInsumos> ObtenerLista();
		List<entRptInsumos> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entRptInsumos> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entRptInsumos> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entRptInsumos> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entRptInsumos> ObtenerLista(entRptInsumos.Fields searchField, object searchValue);
		List<entRptInsumos> ObtenerLista(entRptInsumos.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entRptInsumos> ObtenerLista(entRptInsumos.Fields searchField, object searchValue, string strParamAdicionales);
		List<entRptInsumos> ObtenerLista(entRptInsumos.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		string CreatePK(string[] args);
		
		int InsertUpdate(entRptInsumos obj);
		int InsertUpdate(entRptInsumos obj, ref cTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable CargarDataTable();
		DataTable CargarDataTable(String condicionesWhere);
		DataTable CargarDataTable(ArrayList arrColumnas);
		DataTable CargarDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable CargarDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable CargarDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable CargarDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable CargarDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable CargarDataTable(entRptInsumos.Fields searchField, object searchValue);
		DataTable CargarDataTable(entRptInsumos.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable CargarDataTable(ArrayList arrColumnas, entRptInsumos.Fields searchField, object searchValue);
		DataTable CargarDataTable(ArrayList arrColumnas, entRptInsumos.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable CargarDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable CargarDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable CargarDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entRptInsumos.Fields valueField, entRptInsumos.Fields textField);
		void CargarComboBox(ref ComboBox cmb, entRptInsumos.Fields valueField, entRptInsumos.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entRptInsumos.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, entRptInsumos.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entRptInsumos.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entRptInsumos.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entRptInsumos.Fields valueField, String textField, entRptInsumos.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entRptInsumos.Fields valueField, String textField, entRptInsumos.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entRptInsumos.Fields valueField, entRptInsumos.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entRptInsumos.Fields valueField, entRptInsumos.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entRptInsumos.Fields valueField, entRptInsumos.Fields textField, entRptInsumos.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entRptInsumos.Fields valueField, entRptInsumos.Fields textField, entRptInsumos.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, entRptInsumos.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, entRptInsumos.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entRptInsumos.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entRptInsumos.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		object FuncionesCount(entRptInsumos.Fields refField);
		object FuncionesCount(entRptInsumos.Fields refField, entRptInsumos.Fields whereField, object valueField);
		object FuncionesCount(entRptInsumos.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		object FuncionesMin(entRptInsumos.Fields refField);
		object FuncionesMin(entRptInsumos.Fields refField, entRptInsumos.Fields whereField, object valueField);
		object FuncionesMin(entRptInsumos.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		object FuncionesMax(entRptInsumos.Fields refField);
		object FuncionesMax(entRptInsumos.Fields refField, entRptInsumos.Fields whereField, object valueField);
		object FuncionesMax(entRptInsumos.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		object FuncionesSum(entRptInsumos.Fields refField);
		object FuncionesSum(entRptInsumos.Fields refField, entRptInsumos.Fields whereField, object valueField);
		object FuncionesSum(entRptInsumos.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		object FuncionesAvg(entRptInsumos.Fields refField);
		object FuncionesAvg(entRptInsumos.Fields refField, entRptInsumos.Fields whereField, object valueField);
		object FuncionesAvg(entRptInsumos.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

