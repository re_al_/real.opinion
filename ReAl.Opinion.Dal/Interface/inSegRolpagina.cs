#region 
/***********************************************************************************************************
	NOMBRE:       inSegRolpagina
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla seg_rolpagina

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.Opinion.Dal;
using ReAl.Opinion.PgConn;
using ReAl.Opinion.Dal.Entidades;
using System.Windows.Forms;
#endregion

namespace ReAl.Opinion.Dal.Interface
{
	public interface inSegRolpagina: IDisposable
	{
		string getTableScript();
		dynamic GetColumnType(object valor,entSegRolpagina.Fields myField);
		entSegRolpagina ObtenerObjeto(int intid_rolpagina);
		entSegRolpagina ObtenerObjeto(int intid_rolpagina, ref cTrans localTrans);
		entSegRolpagina ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		entSegRolpagina ObtenerObjeto(Hashtable htbFiltro);
		entSegRolpagina ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		entSegRolpagina ObtenerObjeto(Hashtable htbFiltro, ref cTrans localTrans);
		entSegRolpagina ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		entSegRolpagina ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		entSegRolpagina ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		entSegRolpagina ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		entSegRolpagina ObtenerObjeto(entSegRolpagina.Fields searchField, object searchValue);
		entSegRolpagina ObtenerObjeto(entSegRolpagina.Fields searchField, object searchValue, ref cTrans localTrans);
		entSegRolpagina ObtenerObjeto(entSegRolpagina.Fields searchField, object searchValue, string strParamAdicionales);
		entSegRolpagina ObtenerObjeto(entSegRolpagina.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Dictionary<String, entSegRolpagina> ObtenerDiccionario();
		Dictionary<String, entSegRolpagina> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, entSegRolpagina> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Dictionary<String, entSegRolpagina> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, entSegRolpagina> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Dictionary<String, entSegRolpagina> ObtenerDiccionario(entSegRolpagina.Fields searchField, object searchValue);
		Dictionary<String, entSegRolpagina> ObtenerDiccionario(entSegRolpagina.Fields searchField, object searchValue, ref cTrans localTrans);
		Dictionary<String, entSegRolpagina> ObtenerDiccionario(entSegRolpagina.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, entSegRolpagina> ObtenerDiccionario(entSegRolpagina.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entSegRolpagina> ObtenerLista();
		List<entSegRolpagina> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entSegRolpagina> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entSegRolpagina> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entSegRolpagina> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entSegRolpagina> ObtenerLista(entSegRolpagina.Fields searchField, object searchValue);
		List<entSegRolpagina> ObtenerLista(entSegRolpagina.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entSegRolpagina> ObtenerLista(entSegRolpagina.Fields searchField, object searchValue, string strParamAdicionales);
		List<entSegRolpagina> ObtenerLista(entSegRolpagina.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entSegRolpagina> ObtenerLista(Hashtable htbFiltro);
		List<entSegRolpagina> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<entSegRolpagina> ObtenerLista(Hashtable htbFiltro, ref cTrans localTrans);
		List<entSegRolpagina> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		
		List<entSegRolpagina> ObtenerListaDesdeVista(String strVista);
		List<entSegRolpagina> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entSegRolpagina> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entSegRolpagina> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entSegRolpagina> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entSegRolpagina> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<entSegRolpagina> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref cTrans localTrans);
		List<entSegRolpagina> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<entSegRolpagina> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		List<entSegRolpagina> ObtenerListaDesdeVista(String strVista, entSegRolpagina.Fields searchField, object searchValue);
		List<entSegRolpagina> ObtenerListaDesdeVista(String strVista, entSegRolpagina.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entSegRolpagina> ObtenerListaDesdeVista(String strVista, entSegRolpagina.Fields searchField, object searchValue, string strParamAdicionales);
		List<entSegRolpagina> ObtenerListaDesdeVista(String strVista, entSegRolpagina.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Queue<entSegRolpagina> ObtenerCola();
		Queue<entSegRolpagina> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<entSegRolpagina> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Queue<entSegRolpagina> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<entSegRolpagina> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Queue<entSegRolpagina> ObtenerCola(entSegRolpagina.Fields searchField, object searchValue);
		Queue<entSegRolpagina> ObtenerCola(entSegRolpagina.Fields searchField, object searchValue, ref cTrans localTrans);
		Queue<entSegRolpagina> ObtenerCola(entSegRolpagina.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<entSegRolpagina> ObtenerCola(entSegRolpagina.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Stack<entSegRolpagina> ObtenerPila();
		Stack<entSegRolpagina> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<entSegRolpagina> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Stack<entSegRolpagina> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<entSegRolpagina> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Stack<entSegRolpagina> ObtenerPila(entSegRolpagina.Fields searchField, object searchValue);
		Stack<entSegRolpagina> ObtenerPila(entSegRolpagina.Fields searchField, object searchValue, ref cTrans localTrans);
		Stack<entSegRolpagina> ObtenerPila(entSegRolpagina.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<entSegRolpagina> ObtenerPila(entSegRolpagina.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		string CreatePK(string[] args);
		
		bool Insert(entSegRolpagina obj);
		bool Insert(entSegRolpagina obj, ref cTrans localTrans);
		int Update(entSegRolpagina obj);
		int Update(entSegRolpagina obj, ref cTrans localTrans);
		int Delete(entSegRolpagina obj);
		int Delete(entSegRolpagina obj, ref cTrans localTrans);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(entSegRolpagina.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(entSegRolpagina.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entSegRolpagina.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entSegRolpagina.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entSegRolpagina.Fields valueField, entSegRolpagina.Fields textField);
		void CargarComboBox(ref ComboBox cmb, entSegRolpagina.Fields valueField, entSegRolpagina.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entSegRolpagina.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, entSegRolpagina.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entSegRolpagina.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entSegRolpagina.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entSegRolpagina.Fields valueField, String textField, entSegRolpagina.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entSegRolpagina.Fields valueField, String textField, entSegRolpagina.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entSegRolpagina.Fields valueField, entSegRolpagina.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entSegRolpagina.Fields valueField, entSegRolpagina.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entSegRolpagina.Fields valueField, entSegRolpagina.Fields textField, entSegRolpagina.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entSegRolpagina.Fields valueField, entSegRolpagina.Fields textField, entSegRolpagina.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, entSegRolpagina.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, entSegRolpagina.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entSegRolpagina.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entSegRolpagina.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(entSegRolpagina.Fields refField);
		int FuncionesCount(entSegRolpagina.Fields refField, entSegRolpagina.Fields whereField, object valueField);
		int FuncionesCount(entSegRolpagina.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(entSegRolpagina.Fields refField);
		int FuncionesMin(entSegRolpagina.Fields refField, entSegRolpagina.Fields whereField, object valueField);
		int FuncionesMin(entSegRolpagina.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(entSegRolpagina.Fields refField);
		int FuncionesMax(entSegRolpagina.Fields refField, entSegRolpagina.Fields whereField, object valueField);
		int FuncionesMax(entSegRolpagina.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(entSegRolpagina.Fields refField);
		int FuncionesSum(entSegRolpagina.Fields refField, entSegRolpagina.Fields whereField, object valueField);
		int FuncionesSum(entSegRolpagina.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(entSegRolpagina.Fields refField);
		int FuncionesAvg(entSegRolpagina.Fields refField, entSegRolpagina.Fields whereField, object valueField);
		int FuncionesAvg(entSegRolpagina.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

