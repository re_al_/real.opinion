#region 
/***********************************************************************************************************
	NOMBRE:       inSegMensajerror
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla seg_mensajerror

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        22/10/2014  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.Opinion.PgConn;
using ReAl.Opinion.Dal.Entidades;
using System.Windows.Forms;
#endregion

namespace ReAl.Opinion.Dal.Interface
{
	public interface inSegMensajerror: IDisposable
	{
		string getTableScript();
		dynamic GetColumnType(object valor,entSegMensajerror.Fields myField);
		entSegMensajerror ObtenerObjeto(int intid_error, String Stringid_aplicacion);
		entSegMensajerror ObtenerObjeto(int intid_error, String Stringid_aplicacion, ref cTrans localTrans);
		entSegMensajerror ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		entSegMensajerror ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		entSegMensajerror ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		entSegMensajerror ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		entSegMensajerror ObtenerObjeto(entSegMensajerror.Fields searchField, object searchValue);
		entSegMensajerror ObtenerObjeto(entSegMensajerror.Fields searchField, object searchValue, ref cTrans localTrans);
		entSegMensajerror ObtenerObjeto(entSegMensajerror.Fields searchField, object searchValue, string strParamAdicionales);
		entSegMensajerror ObtenerObjeto(entSegMensajerror.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Dictionary<String, entSegMensajerror> ObtenerDiccionario();
		Dictionary<String, entSegMensajerror> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, entSegMensajerror> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Dictionary<String, entSegMensajerror> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, entSegMensajerror> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Dictionary<String, entSegMensajerror> ObtenerDiccionario(entSegMensajerror.Fields searchField, object searchValue);
		Dictionary<String, entSegMensajerror> ObtenerDiccionario(entSegMensajerror.Fields searchField, object searchValue, ref cTrans localTrans);
		Dictionary<String, entSegMensajerror> ObtenerDiccionario(entSegMensajerror.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, entSegMensajerror> ObtenerDiccionario(entSegMensajerror.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entSegMensajerror> ObtenerLista();
		List<entSegMensajerror> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entSegMensajerror> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entSegMensajerror> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entSegMensajerror> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entSegMensajerror> ObtenerLista(entSegMensajerror.Fields searchField, object searchValue);
		List<entSegMensajerror> ObtenerLista(entSegMensajerror.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entSegMensajerror> ObtenerLista(entSegMensajerror.Fields searchField, object searchValue, string strParamAdicionales);
		List<entSegMensajerror> ObtenerLista(entSegMensajerror.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		string CreatePK(string[] args);
		
		int InsertUpdate(entSegMensajerror obj);
		int InsertUpdate(entSegMensajerror obj, ref cTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable CargarDataTable();
		DataTable CargarDataTable(String condicionesWhere);
		DataTable CargarDataTable(ArrayList arrColumnas);
		DataTable CargarDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable CargarDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable CargarDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable CargarDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable CargarDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable CargarDataTable(entSegMensajerror.Fields searchField, object searchValue);
		DataTable CargarDataTable(entSegMensajerror.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable CargarDataTable(ArrayList arrColumnas, entSegMensajerror.Fields searchField, object searchValue);
		DataTable CargarDataTable(ArrayList arrColumnas, entSegMensajerror.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable CargarDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable CargarDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable CargarDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entSegMensajerror.Fields valueField, entSegMensajerror.Fields textField);
		void CargarComboBox(ref ComboBox cmb, entSegMensajerror.Fields valueField, entSegMensajerror.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entSegMensajerror.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, entSegMensajerror.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entSegMensajerror.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entSegMensajerror.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entSegMensajerror.Fields valueField, String textField, entSegMensajerror.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entSegMensajerror.Fields valueField, String textField, entSegMensajerror.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entSegMensajerror.Fields valueField, entSegMensajerror.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entSegMensajerror.Fields valueField, entSegMensajerror.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entSegMensajerror.Fields valueField, entSegMensajerror.Fields textField, entSegMensajerror.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entSegMensajerror.Fields valueField, entSegMensajerror.Fields textField, entSegMensajerror.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, entSegMensajerror.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, entSegMensajerror.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entSegMensajerror.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entSegMensajerror.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		object FuncionesCount(entSegMensajerror.Fields refField);
		object FuncionesCount(entSegMensajerror.Fields refField, entSegMensajerror.Fields whereField, object valueField);
		object FuncionesCount(entSegMensajerror.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		object FuncionesMin(entSegMensajerror.Fields refField);
		object FuncionesMin(entSegMensajerror.Fields refField, entSegMensajerror.Fields whereField, object valueField);
		object FuncionesMin(entSegMensajerror.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		object FuncionesMax(entSegMensajerror.Fields refField);
		object FuncionesMax(entSegMensajerror.Fields refField, entSegMensajerror.Fields whereField, object valueField);
		object FuncionesMax(entSegMensajerror.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		object FuncionesSum(entSegMensajerror.Fields refField);
		object FuncionesSum(entSegMensajerror.Fields refField, entSegMensajerror.Fields whereField, object valueField);
		object FuncionesSum(entSegMensajerror.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		object FuncionesAvg(entSegMensajerror.Fields refField);
		object FuncionesAvg(entSegMensajerror.Fields refField, entSegMensajerror.Fields whereField, object valueField);
		object FuncionesAvg(entSegMensajerror.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

