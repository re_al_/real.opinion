#region 
/***********************************************************************************************************
	NOMBRE:       inEncSeccion
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla enc_seccion

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.Opinion.Dal;
using ReAl.Opinion.PgConn;
using ReAl.Opinion.Dal.Entidades;
using System.Windows.Forms;
#endregion

namespace ReAl.Opinion.Dal.Interface
{
	public interface inEncSeccion: IDisposable
	{
		string getTableScript();
		dynamic GetColumnType(object valor,entEncSeccion.Fields myField);
		entEncSeccion ObtenerObjeto(int intid_seccion);
		entEncSeccion ObtenerObjeto(int intid_seccion, ref cTrans localTrans);
		entEncSeccion ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		entEncSeccion ObtenerObjeto(Hashtable htbFiltro);
		entEncSeccion ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		entEncSeccion ObtenerObjeto(Hashtable htbFiltro, ref cTrans localTrans);
		entEncSeccion ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		entEncSeccion ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		entEncSeccion ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		entEncSeccion ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		entEncSeccion ObtenerObjeto(entEncSeccion.Fields searchField, object searchValue);
		entEncSeccion ObtenerObjeto(entEncSeccion.Fields searchField, object searchValue, ref cTrans localTrans);
		entEncSeccion ObtenerObjeto(entEncSeccion.Fields searchField, object searchValue, string strParamAdicionales);
		entEncSeccion ObtenerObjeto(entEncSeccion.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Dictionary<String, entEncSeccion> ObtenerDiccionario();
		Dictionary<String, entEncSeccion> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, entEncSeccion> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Dictionary<String, entEncSeccion> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, entEncSeccion> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Dictionary<String, entEncSeccion> ObtenerDiccionario(entEncSeccion.Fields searchField, object searchValue);
		Dictionary<String, entEncSeccion> ObtenerDiccionario(entEncSeccion.Fields searchField, object searchValue, ref cTrans localTrans);
		Dictionary<String, entEncSeccion> ObtenerDiccionario(entEncSeccion.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, entEncSeccion> ObtenerDiccionario(entEncSeccion.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entEncSeccion> ObtenerLista();
		List<entEncSeccion> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entEncSeccion> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entEncSeccion> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entEncSeccion> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entEncSeccion> ObtenerLista(entEncSeccion.Fields searchField, object searchValue);
		List<entEncSeccion> ObtenerLista(entEncSeccion.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entEncSeccion> ObtenerLista(entEncSeccion.Fields searchField, object searchValue, string strParamAdicionales);
		List<entEncSeccion> ObtenerLista(entEncSeccion.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entEncSeccion> ObtenerLista(Hashtable htbFiltro);
		List<entEncSeccion> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<entEncSeccion> ObtenerLista(Hashtable htbFiltro, ref cTrans localTrans);
		List<entEncSeccion> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		
		List<entEncSeccion> ObtenerListaDesdeVista(String strVista);
		List<entEncSeccion> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entEncSeccion> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entEncSeccion> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entEncSeccion> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entEncSeccion> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<entEncSeccion> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref cTrans localTrans);
		List<entEncSeccion> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<entEncSeccion> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		List<entEncSeccion> ObtenerListaDesdeVista(String strVista, entEncSeccion.Fields searchField, object searchValue);
		List<entEncSeccion> ObtenerListaDesdeVista(String strVista, entEncSeccion.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entEncSeccion> ObtenerListaDesdeVista(String strVista, entEncSeccion.Fields searchField, object searchValue, string strParamAdicionales);
		List<entEncSeccion> ObtenerListaDesdeVista(String strVista, entEncSeccion.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Queue<entEncSeccion> ObtenerCola();
		Queue<entEncSeccion> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<entEncSeccion> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Queue<entEncSeccion> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<entEncSeccion> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Queue<entEncSeccion> ObtenerCola(entEncSeccion.Fields searchField, object searchValue);
		Queue<entEncSeccion> ObtenerCola(entEncSeccion.Fields searchField, object searchValue, ref cTrans localTrans);
		Queue<entEncSeccion> ObtenerCola(entEncSeccion.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<entEncSeccion> ObtenerCola(entEncSeccion.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Stack<entEncSeccion> ObtenerPila();
		Stack<entEncSeccion> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<entEncSeccion> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Stack<entEncSeccion> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<entEncSeccion> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Stack<entEncSeccion> ObtenerPila(entEncSeccion.Fields searchField, object searchValue);
		Stack<entEncSeccion> ObtenerPila(entEncSeccion.Fields searchField, object searchValue, ref cTrans localTrans);
		Stack<entEncSeccion> ObtenerPila(entEncSeccion.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<entEncSeccion> ObtenerPila(entEncSeccion.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		string CreatePK(string[] args);
		
		bool Insert(entEncSeccion obj);
		bool Insert(entEncSeccion obj, ref cTrans localTrans);
		int Update(entEncSeccion obj);
		int Update(entEncSeccion obj, ref cTrans localTrans);
		int Delete(entEncSeccion obj);
		int Delete(entEncSeccion obj, ref cTrans localTrans);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(entEncSeccion.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(entEncSeccion.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entEncSeccion.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entEncSeccion.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entEncSeccion.Fields valueField, entEncSeccion.Fields textField);
		void CargarComboBox(ref ComboBox cmb, entEncSeccion.Fields valueField, entEncSeccion.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entEncSeccion.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, entEncSeccion.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entEncSeccion.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entEncSeccion.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entEncSeccion.Fields valueField, String textField, entEncSeccion.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entEncSeccion.Fields valueField, String textField, entEncSeccion.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entEncSeccion.Fields valueField, entEncSeccion.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entEncSeccion.Fields valueField, entEncSeccion.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entEncSeccion.Fields valueField, entEncSeccion.Fields textField, entEncSeccion.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entEncSeccion.Fields valueField, entEncSeccion.Fields textField, entEncSeccion.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, entEncSeccion.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, entEncSeccion.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entEncSeccion.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entEncSeccion.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(entEncSeccion.Fields refField);
		int FuncionesCount(entEncSeccion.Fields refField, entEncSeccion.Fields whereField, object valueField);
		int FuncionesCount(entEncSeccion.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(entEncSeccion.Fields refField);
		int FuncionesMin(entEncSeccion.Fields refField, entEncSeccion.Fields whereField, object valueField);
		int FuncionesMin(entEncSeccion.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(entEncSeccion.Fields refField);
		int FuncionesMax(entEncSeccion.Fields refField, entEncSeccion.Fields whereField, object valueField);
		int FuncionesMax(entEncSeccion.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(entEncSeccion.Fields refField);
		int FuncionesSum(entEncSeccion.Fields refField, entEncSeccion.Fields whereField, object valueField);
		int FuncionesSum(entEncSeccion.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(entEncSeccion.Fields refField);
		int FuncionesAvg(entEncSeccion.Fields refField, entEncSeccion.Fields whereField, object valueField);
		int FuncionesAvg(entEncSeccion.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

