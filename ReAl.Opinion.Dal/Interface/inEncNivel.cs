#region 
/***********************************************************************************************************
	NOMBRE:       inEncNivel
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla enc_nivel

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.Opinion.Dal;
using ReAl.Opinion.PgConn;
using ReAl.Opinion.Dal.Entidades;
using System.Windows.Forms;
#endregion

namespace ReAl.Opinion.Dal.Interface
{
	public interface inEncNivel: IDisposable
	{
		string getTableScript();
		dynamic GetColumnType(object valor,entEncNivel.Fields myField);
		entEncNivel ObtenerObjeto(int intid_nivel);
		entEncNivel ObtenerObjeto(int intid_nivel, ref cTrans localTrans);
		entEncNivel ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		entEncNivel ObtenerObjeto(Hashtable htbFiltro);
		entEncNivel ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		entEncNivel ObtenerObjeto(Hashtable htbFiltro, ref cTrans localTrans);
		entEncNivel ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		entEncNivel ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		entEncNivel ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		entEncNivel ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		entEncNivel ObtenerObjeto(entEncNivel.Fields searchField, object searchValue);
		entEncNivel ObtenerObjeto(entEncNivel.Fields searchField, object searchValue, ref cTrans localTrans);
		entEncNivel ObtenerObjeto(entEncNivel.Fields searchField, object searchValue, string strParamAdicionales);
		entEncNivel ObtenerObjeto(entEncNivel.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Dictionary<String, entEncNivel> ObtenerDiccionario();
		Dictionary<String, entEncNivel> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, entEncNivel> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Dictionary<String, entEncNivel> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, entEncNivel> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Dictionary<String, entEncNivel> ObtenerDiccionario(entEncNivel.Fields searchField, object searchValue);
		Dictionary<String, entEncNivel> ObtenerDiccionario(entEncNivel.Fields searchField, object searchValue, ref cTrans localTrans);
		Dictionary<String, entEncNivel> ObtenerDiccionario(entEncNivel.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, entEncNivel> ObtenerDiccionario(entEncNivel.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entEncNivel> ObtenerLista();
		List<entEncNivel> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entEncNivel> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entEncNivel> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entEncNivel> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entEncNivel> ObtenerLista(entEncNivel.Fields searchField, object searchValue);
		List<entEncNivel> ObtenerLista(entEncNivel.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entEncNivel> ObtenerLista(entEncNivel.Fields searchField, object searchValue, string strParamAdicionales);
		List<entEncNivel> ObtenerLista(entEncNivel.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entEncNivel> ObtenerLista(Hashtable htbFiltro);
		List<entEncNivel> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<entEncNivel> ObtenerLista(Hashtable htbFiltro, ref cTrans localTrans);
		List<entEncNivel> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		
		List<entEncNivel> ObtenerListaDesdeVista(String strVista);
		List<entEncNivel> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entEncNivel> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entEncNivel> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entEncNivel> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entEncNivel> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<entEncNivel> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref cTrans localTrans);
		List<entEncNivel> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<entEncNivel> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		List<entEncNivel> ObtenerListaDesdeVista(String strVista, entEncNivel.Fields searchField, object searchValue);
		List<entEncNivel> ObtenerListaDesdeVista(String strVista, entEncNivel.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entEncNivel> ObtenerListaDesdeVista(String strVista, entEncNivel.Fields searchField, object searchValue, string strParamAdicionales);
		List<entEncNivel> ObtenerListaDesdeVista(String strVista, entEncNivel.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Queue<entEncNivel> ObtenerCola();
		Queue<entEncNivel> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<entEncNivel> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Queue<entEncNivel> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<entEncNivel> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Queue<entEncNivel> ObtenerCola(entEncNivel.Fields searchField, object searchValue);
		Queue<entEncNivel> ObtenerCola(entEncNivel.Fields searchField, object searchValue, ref cTrans localTrans);
		Queue<entEncNivel> ObtenerCola(entEncNivel.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<entEncNivel> ObtenerCola(entEncNivel.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Stack<entEncNivel> ObtenerPila();
		Stack<entEncNivel> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<entEncNivel> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Stack<entEncNivel> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<entEncNivel> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Stack<entEncNivel> ObtenerPila(entEncNivel.Fields searchField, object searchValue);
		Stack<entEncNivel> ObtenerPila(entEncNivel.Fields searchField, object searchValue, ref cTrans localTrans);
		Stack<entEncNivel> ObtenerPila(entEncNivel.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<entEncNivel> ObtenerPila(entEncNivel.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		string CreatePK(string[] args);
		
		bool Insert(entEncNivel obj);
		bool Insert(entEncNivel obj, ref cTrans localTrans);
		int Update(entEncNivel obj);
		int Update(entEncNivel obj, ref cTrans localTrans);
		int Delete(entEncNivel obj);
		int Delete(entEncNivel obj, ref cTrans localTrans);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(entEncNivel.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(entEncNivel.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entEncNivel.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entEncNivel.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entEncNivel.Fields valueField, entEncNivel.Fields textField);
		void CargarComboBox(ref ComboBox cmb, entEncNivel.Fields valueField, entEncNivel.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entEncNivel.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, entEncNivel.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entEncNivel.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entEncNivel.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entEncNivel.Fields valueField, String textField, entEncNivel.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entEncNivel.Fields valueField, String textField, entEncNivel.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entEncNivel.Fields valueField, entEncNivel.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entEncNivel.Fields valueField, entEncNivel.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entEncNivel.Fields valueField, entEncNivel.Fields textField, entEncNivel.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entEncNivel.Fields valueField, entEncNivel.Fields textField, entEncNivel.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, entEncNivel.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, entEncNivel.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entEncNivel.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entEncNivel.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(entEncNivel.Fields refField);
		int FuncionesCount(entEncNivel.Fields refField, entEncNivel.Fields whereField, object valueField);
		int FuncionesCount(entEncNivel.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(entEncNivel.Fields refField);
		int FuncionesMin(entEncNivel.Fields refField, entEncNivel.Fields whereField, object valueField);
		int FuncionesMin(entEncNivel.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(entEncNivel.Fields refField);
		int FuncionesMax(entEncNivel.Fields refField, entEncNivel.Fields whereField, object valueField);
		int FuncionesMax(entEncNivel.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(entEncNivel.Fields refField);
		int FuncionesSum(entEncNivel.Fields refField, entEncNivel.Fields whereField, object valueField);
		int FuncionesSum(entEncNivel.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(entEncNivel.Fields refField);
		int FuncionesAvg(entEncNivel.Fields refField, entEncNivel.Fields whereField, object valueField);
		int FuncionesAvg(entEncNivel.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

