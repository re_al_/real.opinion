#region 
/***********************************************************************************************************
	NOMBRE:       inIndIndustria
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla ind_industria

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/05/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.Opinion.Dal;
using ReAl.Opinion.Dal.Entidades;
using System.Windows.Forms;
using ReAl.Opinion.PgConn;

#endregion

namespace ReAl.Opinion.Dal.Interface
{
	public interface inIndIndustria: IDisposable
	{
		string getTableScript();
		dynamic GetColumnType(object valor,entIndIndustria.Fields myField);
		entIndIndustria ObtenerObjeto(int intid_industria);
		entIndIndustria ObtenerObjeto(int intid_industria, ref cTrans localTrans);
		entIndIndustria ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		entIndIndustria ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		entIndIndustria ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		entIndIndustria ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		entIndIndustria ObtenerObjeto(entIndIndustria.Fields searchField, object searchValue);
		entIndIndustria ObtenerObjeto(entIndIndustria.Fields searchField, object searchValue, ref cTrans localTrans);
		entIndIndustria ObtenerObjeto(entIndIndustria.Fields searchField, object searchValue, string strParamAdicionales);
		entIndIndustria ObtenerObjeto(entIndIndustria.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Dictionary<String, entIndIndustria> ObtenerDiccionario();
		Dictionary<String, entIndIndustria> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, entIndIndustria> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Dictionary<String, entIndIndustria> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, entIndIndustria> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Dictionary<String, entIndIndustria> ObtenerDiccionario(entIndIndustria.Fields searchField, object searchValue);
		Dictionary<String, entIndIndustria> ObtenerDiccionario(entIndIndustria.Fields searchField, object searchValue, ref cTrans localTrans);
		Dictionary<String, entIndIndustria> ObtenerDiccionario(entIndIndustria.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, entIndIndustria> ObtenerDiccionario(entIndIndustria.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entIndIndustria> ObtenerLista();
		List<entIndIndustria> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entIndIndustria> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entIndIndustria> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entIndIndustria> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entIndIndustria> ObtenerLista(entIndIndustria.Fields searchField, object searchValue);
		List<entIndIndustria> ObtenerLista(entIndIndustria.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entIndIndustria> ObtenerLista(entIndIndustria.Fields searchField, object searchValue, string strParamAdicionales);
		List<entIndIndustria> ObtenerLista(entIndIndustria.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entIndIndustria> ObtenerListaDesdeVista(String strVista);
		List<entIndIndustria> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entIndIndustria> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entIndIndustria> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entIndIndustria> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entIndIndustria> ObtenerListaDesdeVista(String strVista, entIndIndustria.Fields searchField, object searchValue);
		List<entIndIndustria> ObtenerListaDesdeVista(String strVista, entIndIndustria.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entIndIndustria> ObtenerListaDesdeVista(String strVista, entIndIndustria.Fields searchField, object searchValue, string strParamAdicionales);
		List<entIndIndustria> ObtenerListaDesdeVista(String strVista, entIndIndustria.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Queue<entIndIndustria> ObtenerCola();
		Queue<entIndIndustria> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<entIndIndustria> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Queue<entIndIndustria> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<entIndIndustria> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Queue<entIndIndustria> ObtenerCola(entIndIndustria.Fields searchField, object searchValue);
		Queue<entIndIndustria> ObtenerCola(entIndIndustria.Fields searchField, object searchValue, ref cTrans localTrans);
		Queue<entIndIndustria> ObtenerCola(entIndIndustria.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<entIndIndustria> ObtenerCola(entIndIndustria.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Stack<entIndIndustria> ObtenerPila();
		Stack<entIndIndustria> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<entIndIndustria> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Stack<entIndIndustria> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<entIndIndustria> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Stack<entIndIndustria> ObtenerPila(entIndIndustria.Fields searchField, object searchValue);
		Stack<entIndIndustria> ObtenerPila(entIndIndustria.Fields searchField, object searchValue, ref cTrans localTrans);
		Stack<entIndIndustria> ObtenerPila(entIndIndustria.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<entIndIndustria> ObtenerPila(entIndIndustria.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		string CreatePK(string[] args);
		
		bool Insert(entIndIndustria obj);
		bool Insert(entIndIndustria obj, ref cTrans localTrans);
		int Update(entIndIndustria obj);
		int Update(entIndIndustria obj, ref cTrans localTrans);
		int Delete(entIndIndustria obj);
		int Delete(entIndIndustria obj, ref cTrans localTrans);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable CargarDataTable();
		DataTable CargarDataTable(String condicionesWhere);
		DataTable CargarDataTable(ArrayList arrColumnas);
		DataTable CargarDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable CargarDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable CargarDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable CargarDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable CargarDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable CargarDataTable(entIndIndustria.Fields searchField, object searchValue);
		DataTable CargarDataTable(entIndIndustria.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable CargarDataTable(ArrayList arrColumnas, entIndIndustria.Fields searchField, object searchValue);
		DataTable CargarDataTable(ArrayList arrColumnas, entIndIndustria.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable CargarDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable CargarDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable CargarDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entIndIndustria.Fields valueField, entIndIndustria.Fields textField);
		void CargarComboBox(ref ComboBox cmb, entIndIndustria.Fields valueField, entIndIndustria.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entIndIndustria.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, entIndIndustria.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entIndIndustria.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entIndIndustria.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entIndIndustria.Fields valueField, String textField, entIndIndustria.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entIndIndustria.Fields valueField, String textField, entIndIndustria.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entIndIndustria.Fields valueField, entIndIndustria.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entIndIndustria.Fields valueField, entIndIndustria.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entIndIndustria.Fields valueField, entIndIndustria.Fields textField, entIndIndustria.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entIndIndustria.Fields valueField, entIndIndustria.Fields textField, entIndIndustria.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, entIndIndustria.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, entIndIndustria.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entIndIndustria.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entIndIndustria.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		object FuncionesCount(entIndIndustria.Fields refField);
		object FuncionesCount(entIndIndustria.Fields refField, entIndIndustria.Fields whereField, object valueField);
		object FuncionesCount(entIndIndustria.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		object FuncionesMin(entIndIndustria.Fields refField);
		object FuncionesMin(entIndIndustria.Fields refField, entIndIndustria.Fields whereField, object valueField);
		object FuncionesMin(entIndIndustria.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		object FuncionesMax(entIndIndustria.Fields refField);
		object FuncionesMax(entIndIndustria.Fields refField, entIndIndustria.Fields whereField, object valueField);
		object FuncionesMax(entIndIndustria.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		object FuncionesSum(entIndIndustria.Fields refField);
		object FuncionesSum(entIndIndustria.Fields refField, entIndIndustria.Fields whereField, object valueField);
		object FuncionesSum(entIndIndustria.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		object FuncionesAvg(entIndIndustria.Fields refField);
		object FuncionesAvg(entIndIndustria.Fields refField, entIndIndustria.Fields whereField, object valueField);
		object FuncionesAvg(entIndIndustria.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

