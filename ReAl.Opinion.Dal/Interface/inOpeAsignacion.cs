#region 
/***********************************************************************************************************
	NOMBRE:       inOpeAsignacion
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla ope_asignacion

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.Opinion.Dal;
using ReAl.Opinion.PgConn;
using ReAl.Opinion.Dal.Entidades;
using System.Windows.Forms;
#endregion

namespace ReAl.Opinion.Dal.Interface
{
	public interface inOpeAsignacion: IDisposable
	{
		string getTableScript();
		dynamic GetColumnType(object valor,entOpeAsignacion.Fields myField);
		entOpeAsignacion ObtenerObjeto(int intid_asignacion);
		entOpeAsignacion ObtenerObjeto(int intid_asignacion, ref cTrans localTrans);
		entOpeAsignacion ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		entOpeAsignacion ObtenerObjeto(Hashtable htbFiltro);
		entOpeAsignacion ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		entOpeAsignacion ObtenerObjeto(Hashtable htbFiltro, ref cTrans localTrans);
		entOpeAsignacion ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		entOpeAsignacion ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		entOpeAsignacion ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		entOpeAsignacion ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		entOpeAsignacion ObtenerObjeto(entOpeAsignacion.Fields searchField, object searchValue);
		entOpeAsignacion ObtenerObjeto(entOpeAsignacion.Fields searchField, object searchValue, ref cTrans localTrans);
		entOpeAsignacion ObtenerObjeto(entOpeAsignacion.Fields searchField, object searchValue, string strParamAdicionales);
		entOpeAsignacion ObtenerObjeto(entOpeAsignacion.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Dictionary<String, entOpeAsignacion> ObtenerDiccionario();
		Dictionary<String, entOpeAsignacion> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, entOpeAsignacion> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Dictionary<String, entOpeAsignacion> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, entOpeAsignacion> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Dictionary<String, entOpeAsignacion> ObtenerDiccionario(entOpeAsignacion.Fields searchField, object searchValue);
		Dictionary<String, entOpeAsignacion> ObtenerDiccionario(entOpeAsignacion.Fields searchField, object searchValue, ref cTrans localTrans);
		Dictionary<String, entOpeAsignacion> ObtenerDiccionario(entOpeAsignacion.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, entOpeAsignacion> ObtenerDiccionario(entOpeAsignacion.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entOpeAsignacion> ObtenerLista();
		List<entOpeAsignacion> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entOpeAsignacion> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entOpeAsignacion> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entOpeAsignacion> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entOpeAsignacion> ObtenerLista(entOpeAsignacion.Fields searchField, object searchValue);
		List<entOpeAsignacion> ObtenerLista(entOpeAsignacion.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entOpeAsignacion> ObtenerLista(entOpeAsignacion.Fields searchField, object searchValue, string strParamAdicionales);
		List<entOpeAsignacion> ObtenerLista(entOpeAsignacion.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entOpeAsignacion> ObtenerLista(Hashtable htbFiltro);
		List<entOpeAsignacion> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<entOpeAsignacion> ObtenerLista(Hashtable htbFiltro, ref cTrans localTrans);
		List<entOpeAsignacion> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		
		List<entOpeAsignacion> ObtenerListaDesdeVista(String strVista);
		List<entOpeAsignacion> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entOpeAsignacion> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entOpeAsignacion> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entOpeAsignacion> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entOpeAsignacion> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<entOpeAsignacion> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref cTrans localTrans);
		List<entOpeAsignacion> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<entOpeAsignacion> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		List<entOpeAsignacion> ObtenerListaDesdeVista(String strVista, entOpeAsignacion.Fields searchField, object searchValue);
		List<entOpeAsignacion> ObtenerListaDesdeVista(String strVista, entOpeAsignacion.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entOpeAsignacion> ObtenerListaDesdeVista(String strVista, entOpeAsignacion.Fields searchField, object searchValue, string strParamAdicionales);
		List<entOpeAsignacion> ObtenerListaDesdeVista(String strVista, entOpeAsignacion.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Queue<entOpeAsignacion> ObtenerCola();
		Queue<entOpeAsignacion> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<entOpeAsignacion> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Queue<entOpeAsignacion> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<entOpeAsignacion> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Queue<entOpeAsignacion> ObtenerCola(entOpeAsignacion.Fields searchField, object searchValue);
		Queue<entOpeAsignacion> ObtenerCola(entOpeAsignacion.Fields searchField, object searchValue, ref cTrans localTrans);
		Queue<entOpeAsignacion> ObtenerCola(entOpeAsignacion.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<entOpeAsignacion> ObtenerCola(entOpeAsignacion.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Stack<entOpeAsignacion> ObtenerPila();
		Stack<entOpeAsignacion> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<entOpeAsignacion> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Stack<entOpeAsignacion> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<entOpeAsignacion> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Stack<entOpeAsignacion> ObtenerPila(entOpeAsignacion.Fields searchField, object searchValue);
		Stack<entOpeAsignacion> ObtenerPila(entOpeAsignacion.Fields searchField, object searchValue, ref cTrans localTrans);
		Stack<entOpeAsignacion> ObtenerPila(entOpeAsignacion.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<entOpeAsignacion> ObtenerPila(entOpeAsignacion.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		string CreatePK(string[] args);
		
		bool Insert(entOpeAsignacion obj);
		bool Insert(entOpeAsignacion obj, ref cTrans localTrans);
		int Update(entOpeAsignacion obj);
		int Update(entOpeAsignacion obj, ref cTrans localTrans);
		int Delete(entOpeAsignacion obj);
		int Delete(entOpeAsignacion obj, ref cTrans localTrans);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(entOpeAsignacion.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(entOpeAsignacion.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entOpeAsignacion.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entOpeAsignacion.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entOpeAsignacion.Fields valueField, entOpeAsignacion.Fields textField);
		void CargarComboBox(ref ComboBox cmb, entOpeAsignacion.Fields valueField, entOpeAsignacion.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entOpeAsignacion.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, entOpeAsignacion.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entOpeAsignacion.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entOpeAsignacion.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entOpeAsignacion.Fields valueField, String textField, entOpeAsignacion.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entOpeAsignacion.Fields valueField, String textField, entOpeAsignacion.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entOpeAsignacion.Fields valueField, entOpeAsignacion.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entOpeAsignacion.Fields valueField, entOpeAsignacion.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entOpeAsignacion.Fields valueField, entOpeAsignacion.Fields textField, entOpeAsignacion.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entOpeAsignacion.Fields valueField, entOpeAsignacion.Fields textField, entOpeAsignacion.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, entOpeAsignacion.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, entOpeAsignacion.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entOpeAsignacion.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entOpeAsignacion.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(entOpeAsignacion.Fields refField);
		int FuncionesCount(entOpeAsignacion.Fields refField, entOpeAsignacion.Fields whereField, object valueField);
		int FuncionesCount(entOpeAsignacion.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(entOpeAsignacion.Fields refField);
		int FuncionesMin(entOpeAsignacion.Fields refField, entOpeAsignacion.Fields whereField, object valueField);
		int FuncionesMin(entOpeAsignacion.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(entOpeAsignacion.Fields refField);
		int FuncionesMax(entOpeAsignacion.Fields refField, entOpeAsignacion.Fields whereField, object valueField);
		int FuncionesMax(entOpeAsignacion.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(entOpeAsignacion.Fields refField);
		int FuncionesSum(entOpeAsignacion.Fields refField, entOpeAsignacion.Fields whereField, object valueField);
		int FuncionesSum(entOpeAsignacion.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(entOpeAsignacion.Fields refField);
		int FuncionesAvg(entOpeAsignacion.Fields refField, entOpeAsignacion.Fields whereField, object valueField);
		int FuncionesAvg(entOpeAsignacion.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

