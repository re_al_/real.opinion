#region 
/***********************************************************************************************************
	NOMBRE:       inCatTipopregunta
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla cat_tipo_pregunta

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.Opinion.Dal;
using ReAl.Opinion.PgConn;
using ReAl.Opinion.Dal.Entidades;
using System.Windows.Forms;
#endregion

namespace ReAl.Opinion.Dal.Interface
{
	public interface inCatTipopregunta: IDisposable
	{
		string getTableScript();
		dynamic GetColumnType(object valor,entCatTipopregunta.Fields myField);
		entCatTipopregunta ObtenerObjeto(int intid_tipo_pregunta);
		entCatTipopregunta ObtenerObjeto(int intid_tipo_pregunta, ref cTrans localTrans);
		entCatTipopregunta ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		entCatTipopregunta ObtenerObjeto(Hashtable htbFiltro);
		entCatTipopregunta ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		entCatTipopregunta ObtenerObjeto(Hashtable htbFiltro, ref cTrans localTrans);
		entCatTipopregunta ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		entCatTipopregunta ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		entCatTipopregunta ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		entCatTipopregunta ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		entCatTipopregunta ObtenerObjeto(entCatTipopregunta.Fields searchField, object searchValue);
		entCatTipopregunta ObtenerObjeto(entCatTipopregunta.Fields searchField, object searchValue, ref cTrans localTrans);
		entCatTipopregunta ObtenerObjeto(entCatTipopregunta.Fields searchField, object searchValue, string strParamAdicionales);
		entCatTipopregunta ObtenerObjeto(entCatTipopregunta.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Dictionary<String, entCatTipopregunta> ObtenerDiccionario();
		Dictionary<String, entCatTipopregunta> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, entCatTipopregunta> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Dictionary<String, entCatTipopregunta> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, entCatTipopregunta> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Dictionary<String, entCatTipopregunta> ObtenerDiccionario(entCatTipopregunta.Fields searchField, object searchValue);
		Dictionary<String, entCatTipopregunta> ObtenerDiccionario(entCatTipopregunta.Fields searchField, object searchValue, ref cTrans localTrans);
		Dictionary<String, entCatTipopregunta> ObtenerDiccionario(entCatTipopregunta.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, entCatTipopregunta> ObtenerDiccionario(entCatTipopregunta.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entCatTipopregunta> ObtenerLista();
		List<entCatTipopregunta> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entCatTipopregunta> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entCatTipopregunta> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entCatTipopregunta> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entCatTipopregunta> ObtenerLista(entCatTipopregunta.Fields searchField, object searchValue);
		List<entCatTipopregunta> ObtenerLista(entCatTipopregunta.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entCatTipopregunta> ObtenerLista(entCatTipopregunta.Fields searchField, object searchValue, string strParamAdicionales);
		List<entCatTipopregunta> ObtenerLista(entCatTipopregunta.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entCatTipopregunta> ObtenerLista(Hashtable htbFiltro);
		List<entCatTipopregunta> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<entCatTipopregunta> ObtenerLista(Hashtable htbFiltro, ref cTrans localTrans);
		List<entCatTipopregunta> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		
		List<entCatTipopregunta> ObtenerListaDesdeVista(String strVista);
		List<entCatTipopregunta> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entCatTipopregunta> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entCatTipopregunta> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entCatTipopregunta> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entCatTipopregunta> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<entCatTipopregunta> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref cTrans localTrans);
		List<entCatTipopregunta> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<entCatTipopregunta> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		List<entCatTipopregunta> ObtenerListaDesdeVista(String strVista, entCatTipopregunta.Fields searchField, object searchValue);
		List<entCatTipopregunta> ObtenerListaDesdeVista(String strVista, entCatTipopregunta.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entCatTipopregunta> ObtenerListaDesdeVista(String strVista, entCatTipopregunta.Fields searchField, object searchValue, string strParamAdicionales);
		List<entCatTipopregunta> ObtenerListaDesdeVista(String strVista, entCatTipopregunta.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Queue<entCatTipopregunta> ObtenerCola();
		Queue<entCatTipopregunta> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<entCatTipopregunta> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Queue<entCatTipopregunta> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<entCatTipopregunta> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Queue<entCatTipopregunta> ObtenerCola(entCatTipopregunta.Fields searchField, object searchValue);
		Queue<entCatTipopregunta> ObtenerCola(entCatTipopregunta.Fields searchField, object searchValue, ref cTrans localTrans);
		Queue<entCatTipopregunta> ObtenerCola(entCatTipopregunta.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<entCatTipopregunta> ObtenerCola(entCatTipopregunta.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Stack<entCatTipopregunta> ObtenerPila();
		Stack<entCatTipopregunta> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<entCatTipopregunta> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Stack<entCatTipopregunta> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<entCatTipopregunta> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Stack<entCatTipopregunta> ObtenerPila(entCatTipopregunta.Fields searchField, object searchValue);
		Stack<entCatTipopregunta> ObtenerPila(entCatTipopregunta.Fields searchField, object searchValue, ref cTrans localTrans);
		Stack<entCatTipopregunta> ObtenerPila(entCatTipopregunta.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<entCatTipopregunta> ObtenerPila(entCatTipopregunta.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		string CreatePK(string[] args);
		
		bool Insert(entCatTipopregunta obj);
		bool Insert(entCatTipopregunta obj, ref cTrans localTrans);
		int Update(entCatTipopregunta obj);
		int Update(entCatTipopregunta obj, ref cTrans localTrans);
		int Delete(entCatTipopregunta obj);
		int Delete(entCatTipopregunta obj, ref cTrans localTrans);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(entCatTipopregunta.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(entCatTipopregunta.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entCatTipopregunta.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entCatTipopregunta.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entCatTipopregunta.Fields valueField, entCatTipopregunta.Fields textField);
		void CargarComboBox(ref ComboBox cmb, entCatTipopregunta.Fields valueField, entCatTipopregunta.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entCatTipopregunta.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, entCatTipopregunta.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entCatTipopregunta.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entCatTipopregunta.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entCatTipopregunta.Fields valueField, String textField, entCatTipopregunta.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entCatTipopregunta.Fields valueField, String textField, entCatTipopregunta.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entCatTipopregunta.Fields valueField, entCatTipopregunta.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entCatTipopregunta.Fields valueField, entCatTipopregunta.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entCatTipopregunta.Fields valueField, entCatTipopregunta.Fields textField, entCatTipopregunta.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entCatTipopregunta.Fields valueField, entCatTipopregunta.Fields textField, entCatTipopregunta.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, entCatTipopregunta.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, entCatTipopregunta.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entCatTipopregunta.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entCatTipopregunta.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(entCatTipopregunta.Fields refField);
		int FuncionesCount(entCatTipopregunta.Fields refField, entCatTipopregunta.Fields whereField, object valueField);
		int FuncionesCount(entCatTipopregunta.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(entCatTipopregunta.Fields refField);
		int FuncionesMin(entCatTipopregunta.Fields refField, entCatTipopregunta.Fields whereField, object valueField);
		int FuncionesMin(entCatTipopregunta.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(entCatTipopregunta.Fields refField);
		int FuncionesMax(entCatTipopregunta.Fields refField, entCatTipopregunta.Fields whereField, object valueField);
		int FuncionesMax(entCatTipopregunta.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(entCatTipopregunta.Fields refField);
		int FuncionesSum(entCatTipopregunta.Fields refField, entCatTipopregunta.Fields whereField, object valueField);
		int FuncionesSum(entCatTipopregunta.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(entCatTipopregunta.Fields refField);
		int FuncionesAvg(entCatTipopregunta.Fields refField, entCatTipopregunta.Fields whereField, object valueField);
		int FuncionesAvg(entCatTipopregunta.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

