#region 
/***********************************************************************************************************
	NOMBRE:       inSegRol
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla seg_rol

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.Opinion.Dal;
using ReAl.Opinion.PgConn;
using ReAl.Opinion.Dal.Entidades;
using System.Windows.Forms;
#endregion

namespace ReAl.Opinion.Dal.Interface
{
	public interface inSegRol: IDisposable
	{
		string getTableScript();
		dynamic GetColumnType(object valor,entSegRol.Fields myField);
		entSegRol ObtenerObjeto(int intid_rol);
		entSegRol ObtenerObjeto(int intid_rol, ref cTrans localTrans);
		entSegRol ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		entSegRol ObtenerObjeto(Hashtable htbFiltro);
		entSegRol ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		entSegRol ObtenerObjeto(Hashtable htbFiltro, ref cTrans localTrans);
		entSegRol ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		entSegRol ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		entSegRol ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		entSegRol ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		entSegRol ObtenerObjeto(entSegRol.Fields searchField, object searchValue);
		entSegRol ObtenerObjeto(entSegRol.Fields searchField, object searchValue, ref cTrans localTrans);
		entSegRol ObtenerObjeto(entSegRol.Fields searchField, object searchValue, string strParamAdicionales);
		entSegRol ObtenerObjeto(entSegRol.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Dictionary<String, entSegRol> ObtenerDiccionario();
		Dictionary<String, entSegRol> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, entSegRol> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Dictionary<String, entSegRol> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, entSegRol> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Dictionary<String, entSegRol> ObtenerDiccionario(entSegRol.Fields searchField, object searchValue);
		Dictionary<String, entSegRol> ObtenerDiccionario(entSegRol.Fields searchField, object searchValue, ref cTrans localTrans);
		Dictionary<String, entSegRol> ObtenerDiccionario(entSegRol.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, entSegRol> ObtenerDiccionario(entSegRol.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entSegRol> ObtenerLista();
		List<entSegRol> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entSegRol> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entSegRol> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entSegRol> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entSegRol> ObtenerLista(entSegRol.Fields searchField, object searchValue);
		List<entSegRol> ObtenerLista(entSegRol.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entSegRol> ObtenerLista(entSegRol.Fields searchField, object searchValue, string strParamAdicionales);
		List<entSegRol> ObtenerLista(entSegRol.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entSegRol> ObtenerLista(Hashtable htbFiltro);
		List<entSegRol> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<entSegRol> ObtenerLista(Hashtable htbFiltro, ref cTrans localTrans);
		List<entSegRol> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		
		List<entSegRol> ObtenerListaDesdeVista(String strVista);
		List<entSegRol> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entSegRol> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entSegRol> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entSegRol> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entSegRol> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<entSegRol> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref cTrans localTrans);
		List<entSegRol> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<entSegRol> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		List<entSegRol> ObtenerListaDesdeVista(String strVista, entSegRol.Fields searchField, object searchValue);
		List<entSegRol> ObtenerListaDesdeVista(String strVista, entSegRol.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entSegRol> ObtenerListaDesdeVista(String strVista, entSegRol.Fields searchField, object searchValue, string strParamAdicionales);
		List<entSegRol> ObtenerListaDesdeVista(String strVista, entSegRol.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Queue<entSegRol> ObtenerCola();
		Queue<entSegRol> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<entSegRol> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Queue<entSegRol> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<entSegRol> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Queue<entSegRol> ObtenerCola(entSegRol.Fields searchField, object searchValue);
		Queue<entSegRol> ObtenerCola(entSegRol.Fields searchField, object searchValue, ref cTrans localTrans);
		Queue<entSegRol> ObtenerCola(entSegRol.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<entSegRol> ObtenerCola(entSegRol.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Stack<entSegRol> ObtenerPila();
		Stack<entSegRol> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<entSegRol> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Stack<entSegRol> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<entSegRol> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Stack<entSegRol> ObtenerPila(entSegRol.Fields searchField, object searchValue);
		Stack<entSegRol> ObtenerPila(entSegRol.Fields searchField, object searchValue, ref cTrans localTrans);
		Stack<entSegRol> ObtenerPila(entSegRol.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<entSegRol> ObtenerPila(entSegRol.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		string CreatePK(string[] args);
		
		bool Insert(entSegRol obj);
		bool Insert(entSegRol obj, ref cTrans localTrans);
		int Update(entSegRol obj);
		int Update(entSegRol obj, ref cTrans localTrans);
		int Delete(entSegRol obj);
		int Delete(entSegRol obj, ref cTrans localTrans);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(entSegRol.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(entSegRol.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entSegRol.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entSegRol.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entSegRol.Fields valueField, entSegRol.Fields textField);
		void CargarComboBox(ref ComboBox cmb, entSegRol.Fields valueField, entSegRol.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entSegRol.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, entSegRol.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entSegRol.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entSegRol.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entSegRol.Fields valueField, String textField, entSegRol.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entSegRol.Fields valueField, String textField, entSegRol.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entSegRol.Fields valueField, entSegRol.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entSegRol.Fields valueField, entSegRol.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entSegRol.Fields valueField, entSegRol.Fields textField, entSegRol.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entSegRol.Fields valueField, entSegRol.Fields textField, entSegRol.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, entSegRol.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, entSegRol.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entSegRol.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entSegRol.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(entSegRol.Fields refField);
		int FuncionesCount(entSegRol.Fields refField, entSegRol.Fields whereField, object valueField);
		int FuncionesCount(entSegRol.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(entSegRol.Fields refField);
		int FuncionesMin(entSegRol.Fields refField, entSegRol.Fields whereField, object valueField);
		int FuncionesMin(entSegRol.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(entSegRol.Fields refField);
		int FuncionesMax(entSegRol.Fields refField, entSegRol.Fields whereField, object valueField);
		int FuncionesMax(entSegRol.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(entSegRol.Fields refField);
		int FuncionesSum(entSegRol.Fields refField, entSegRol.Fields whereField, object valueField);
		int FuncionesSum(entSegRol.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(entSegRol.Fields refField);
		int FuncionesAvg(entSegRol.Fields refField, entSegRol.Fields whereField, object valueField);
		int FuncionesAvg(entSegRol.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

