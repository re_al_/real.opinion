#region 
/***********************************************************************************************************
	NOMBRE:       inOpeListado
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla ope_listado

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        24/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.Opinion.Dal;
using ReAl.Opinion.PgConn;
using ReAl.Opinion.Dal.Entidades;
using System.Windows.Forms;
#endregion

namespace ReAl.Opinion.Dal.Interface
{
	public interface inOpeListado: IDisposable
	{
		string getTableScript();
		dynamic GetColumnType(object valor,entOpeListado.Fields myField);
		entOpeListado ObtenerObjeto(int intid_listado);
		entOpeListado ObtenerObjeto(int intid_listado, ref cTrans localTrans);
		entOpeListado ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		entOpeListado ObtenerObjeto(Hashtable htbFiltro);
		entOpeListado ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		entOpeListado ObtenerObjeto(Hashtable htbFiltro, ref cTrans localTrans);
		entOpeListado ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		entOpeListado ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		entOpeListado ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		entOpeListado ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		entOpeListado ObtenerObjeto(entOpeListado.Fields searchField, object searchValue);
		entOpeListado ObtenerObjeto(entOpeListado.Fields searchField, object searchValue, ref cTrans localTrans);
		entOpeListado ObtenerObjeto(entOpeListado.Fields searchField, object searchValue, string strParamAdicionales);
		entOpeListado ObtenerObjeto(entOpeListado.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Dictionary<String, entOpeListado> ObtenerDiccionario();
		Dictionary<String, entOpeListado> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, entOpeListado> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Dictionary<String, entOpeListado> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, entOpeListado> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Dictionary<String, entOpeListado> ObtenerDiccionario(entOpeListado.Fields searchField, object searchValue);
		Dictionary<String, entOpeListado> ObtenerDiccionario(entOpeListado.Fields searchField, object searchValue, ref cTrans localTrans);
		Dictionary<String, entOpeListado> ObtenerDiccionario(entOpeListado.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, entOpeListado> ObtenerDiccionario(entOpeListado.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entOpeListado> ObtenerLista();
		List<entOpeListado> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entOpeListado> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entOpeListado> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entOpeListado> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entOpeListado> ObtenerLista(entOpeListado.Fields searchField, object searchValue);
		List<entOpeListado> ObtenerLista(entOpeListado.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entOpeListado> ObtenerLista(entOpeListado.Fields searchField, object searchValue, string strParamAdicionales);
		List<entOpeListado> ObtenerLista(entOpeListado.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entOpeListado> ObtenerLista(Hashtable htbFiltro);
		List<entOpeListado> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<entOpeListado> ObtenerLista(Hashtable htbFiltro, ref cTrans localTrans);
		List<entOpeListado> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		
		List<entOpeListado> ObtenerListaDesdeVista(String strVista);
		List<entOpeListado> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entOpeListado> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entOpeListado> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entOpeListado> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entOpeListado> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<entOpeListado> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref cTrans localTrans);
		List<entOpeListado> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<entOpeListado> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		List<entOpeListado> ObtenerListaDesdeVista(String strVista, entOpeListado.Fields searchField, object searchValue);
		List<entOpeListado> ObtenerListaDesdeVista(String strVista, entOpeListado.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entOpeListado> ObtenerListaDesdeVista(String strVista, entOpeListado.Fields searchField, object searchValue, string strParamAdicionales);
		List<entOpeListado> ObtenerListaDesdeVista(String strVista, entOpeListado.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Queue<entOpeListado> ObtenerCola();
		Queue<entOpeListado> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<entOpeListado> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Queue<entOpeListado> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<entOpeListado> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Queue<entOpeListado> ObtenerCola(entOpeListado.Fields searchField, object searchValue);
		Queue<entOpeListado> ObtenerCola(entOpeListado.Fields searchField, object searchValue, ref cTrans localTrans);
		Queue<entOpeListado> ObtenerCola(entOpeListado.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<entOpeListado> ObtenerCola(entOpeListado.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Stack<entOpeListado> ObtenerPila();
		Stack<entOpeListado> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<entOpeListado> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Stack<entOpeListado> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<entOpeListado> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Stack<entOpeListado> ObtenerPila(entOpeListado.Fields searchField, object searchValue);
		Stack<entOpeListado> ObtenerPila(entOpeListado.Fields searchField, object searchValue, ref cTrans localTrans);
		Stack<entOpeListado> ObtenerPila(entOpeListado.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<entOpeListado> ObtenerPila(entOpeListado.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		string CreatePK(string[] args);
		
		bool Insert(entOpeListado obj);
		bool Insert(entOpeListado obj, ref cTrans localTrans);
		int Update(entOpeListado obj);
		int Update(entOpeListado obj, ref cTrans localTrans);
		int Delete(entOpeListado obj);
		int Delete(entOpeListado obj, ref cTrans localTrans);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(entOpeListado.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(entOpeListado.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entOpeListado.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entOpeListado.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entOpeListado.Fields valueField, entOpeListado.Fields textField);
		void CargarComboBox(ref ComboBox cmb, entOpeListado.Fields valueField, entOpeListado.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entOpeListado.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, entOpeListado.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entOpeListado.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entOpeListado.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entOpeListado.Fields valueField, String textField, entOpeListado.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entOpeListado.Fields valueField, String textField, entOpeListado.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entOpeListado.Fields valueField, entOpeListado.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entOpeListado.Fields valueField, entOpeListado.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entOpeListado.Fields valueField, entOpeListado.Fields textField, entOpeListado.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entOpeListado.Fields valueField, entOpeListado.Fields textField, entOpeListado.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, entOpeListado.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, entOpeListado.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entOpeListado.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entOpeListado.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(entOpeListado.Fields refField);
		int FuncionesCount(entOpeListado.Fields refField, entOpeListado.Fields whereField, object valueField);
		int FuncionesCount(entOpeListado.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(entOpeListado.Fields refField);
		int FuncionesMin(entOpeListado.Fields refField, entOpeListado.Fields whereField, object valueField);
		int FuncionesMin(entOpeListado.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(entOpeListado.Fields refField);
		int FuncionesMax(entOpeListado.Fields refField, entOpeListado.Fields whereField, object valueField);
		int FuncionesMax(entOpeListado.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(entOpeListado.Fields refField);
		int FuncionesSum(entOpeListado.Fields refField, entOpeListado.Fields whereField, object valueField);
		int FuncionesSum(entOpeListado.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(entOpeListado.Fields refField);
		int FuncionesAvg(entOpeListado.Fields refField, entOpeListado.Fields whereField, object valueField);
		int FuncionesAvg(entOpeListado.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

