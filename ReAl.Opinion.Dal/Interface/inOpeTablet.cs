#region 
/***********************************************************************************************************
	NOMBRE:       inOpeTablet
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla ope_tablet

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.Opinion.Dal;
using ReAl.Opinion.PgConn;
using ReAl.Opinion.Dal.Entidades;
using System.Windows.Forms;
#endregion

namespace ReAl.Opinion.Dal.Interface
{
	public interface inOpeTablet: IDisposable
	{
		string getTableScript();
		dynamic GetColumnType(object valor,entOpeTablet.Fields myField);
		entOpeTablet ObtenerObjeto(int intid_tablet);
		entOpeTablet ObtenerObjeto(int intid_tablet, ref cTrans localTrans);
		entOpeTablet ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		entOpeTablet ObtenerObjeto(Hashtable htbFiltro);
		entOpeTablet ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		entOpeTablet ObtenerObjeto(Hashtable htbFiltro, ref cTrans localTrans);
		entOpeTablet ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		entOpeTablet ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		entOpeTablet ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		entOpeTablet ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		entOpeTablet ObtenerObjeto(entOpeTablet.Fields searchField, object searchValue);
		entOpeTablet ObtenerObjeto(entOpeTablet.Fields searchField, object searchValue, ref cTrans localTrans);
		entOpeTablet ObtenerObjeto(entOpeTablet.Fields searchField, object searchValue, string strParamAdicionales);
		entOpeTablet ObtenerObjeto(entOpeTablet.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Dictionary<String, entOpeTablet> ObtenerDiccionario();
		Dictionary<String, entOpeTablet> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, entOpeTablet> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Dictionary<String, entOpeTablet> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, entOpeTablet> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Dictionary<String, entOpeTablet> ObtenerDiccionario(entOpeTablet.Fields searchField, object searchValue);
		Dictionary<String, entOpeTablet> ObtenerDiccionario(entOpeTablet.Fields searchField, object searchValue, ref cTrans localTrans);
		Dictionary<String, entOpeTablet> ObtenerDiccionario(entOpeTablet.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, entOpeTablet> ObtenerDiccionario(entOpeTablet.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entOpeTablet> ObtenerLista();
		List<entOpeTablet> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entOpeTablet> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entOpeTablet> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entOpeTablet> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entOpeTablet> ObtenerLista(entOpeTablet.Fields searchField, object searchValue);
		List<entOpeTablet> ObtenerLista(entOpeTablet.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entOpeTablet> ObtenerLista(entOpeTablet.Fields searchField, object searchValue, string strParamAdicionales);
		List<entOpeTablet> ObtenerLista(entOpeTablet.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entOpeTablet> ObtenerLista(Hashtable htbFiltro);
		List<entOpeTablet> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<entOpeTablet> ObtenerLista(Hashtable htbFiltro, ref cTrans localTrans);
		List<entOpeTablet> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		
		List<entOpeTablet> ObtenerListaDesdeVista(String strVista);
		List<entOpeTablet> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entOpeTablet> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entOpeTablet> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entOpeTablet> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entOpeTablet> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<entOpeTablet> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref cTrans localTrans);
		List<entOpeTablet> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<entOpeTablet> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		List<entOpeTablet> ObtenerListaDesdeVista(String strVista, entOpeTablet.Fields searchField, object searchValue);
		List<entOpeTablet> ObtenerListaDesdeVista(String strVista, entOpeTablet.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entOpeTablet> ObtenerListaDesdeVista(String strVista, entOpeTablet.Fields searchField, object searchValue, string strParamAdicionales);
		List<entOpeTablet> ObtenerListaDesdeVista(String strVista, entOpeTablet.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Queue<entOpeTablet> ObtenerCola();
		Queue<entOpeTablet> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<entOpeTablet> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Queue<entOpeTablet> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<entOpeTablet> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Queue<entOpeTablet> ObtenerCola(entOpeTablet.Fields searchField, object searchValue);
		Queue<entOpeTablet> ObtenerCola(entOpeTablet.Fields searchField, object searchValue, ref cTrans localTrans);
		Queue<entOpeTablet> ObtenerCola(entOpeTablet.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<entOpeTablet> ObtenerCola(entOpeTablet.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Stack<entOpeTablet> ObtenerPila();
		Stack<entOpeTablet> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<entOpeTablet> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Stack<entOpeTablet> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<entOpeTablet> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Stack<entOpeTablet> ObtenerPila(entOpeTablet.Fields searchField, object searchValue);
		Stack<entOpeTablet> ObtenerPila(entOpeTablet.Fields searchField, object searchValue, ref cTrans localTrans);
		Stack<entOpeTablet> ObtenerPila(entOpeTablet.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<entOpeTablet> ObtenerPila(entOpeTablet.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		string CreatePK(string[] args);
		
		bool Insert(entOpeTablet obj);
		bool Insert(entOpeTablet obj, ref cTrans localTrans);
		int Update(entOpeTablet obj);
		int Update(entOpeTablet obj, ref cTrans localTrans);
		int Delete(entOpeTablet obj);
		int Delete(entOpeTablet obj, ref cTrans localTrans);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(entOpeTablet.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(entOpeTablet.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entOpeTablet.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entOpeTablet.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entOpeTablet.Fields valueField, entOpeTablet.Fields textField);
		void CargarComboBox(ref ComboBox cmb, entOpeTablet.Fields valueField, entOpeTablet.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entOpeTablet.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, entOpeTablet.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entOpeTablet.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entOpeTablet.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entOpeTablet.Fields valueField, String textField, entOpeTablet.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entOpeTablet.Fields valueField, String textField, entOpeTablet.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entOpeTablet.Fields valueField, entOpeTablet.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entOpeTablet.Fields valueField, entOpeTablet.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entOpeTablet.Fields valueField, entOpeTablet.Fields textField, entOpeTablet.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entOpeTablet.Fields valueField, entOpeTablet.Fields textField, entOpeTablet.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, entOpeTablet.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, entOpeTablet.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entOpeTablet.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entOpeTablet.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(entOpeTablet.Fields refField);
		int FuncionesCount(entOpeTablet.Fields refField, entOpeTablet.Fields whereField, object valueField);
		int FuncionesCount(entOpeTablet.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(entOpeTablet.Fields refField);
		int FuncionesMin(entOpeTablet.Fields refField, entOpeTablet.Fields whereField, object valueField);
		int FuncionesMin(entOpeTablet.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(entOpeTablet.Fields refField);
		int FuncionesMax(entOpeTablet.Fields refField, entOpeTablet.Fields whereField, object valueField);
		int FuncionesMax(entOpeTablet.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(entOpeTablet.Fields refField);
		int FuncionesSum(entOpeTablet.Fields refField, entOpeTablet.Fields whereField, object valueField);
		int FuncionesSum(entOpeTablet.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(entOpeTablet.Fields refField);
		int FuncionesAvg(entOpeTablet.Fields refField, entOpeTablet.Fields whereField, object valueField);
		int FuncionesAvg(entOpeTablet.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

