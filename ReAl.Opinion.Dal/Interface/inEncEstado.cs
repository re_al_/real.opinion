#region 
/***********************************************************************************************************
	NOMBRE:       inEncEstado
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla enc_estado

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.Opinion.Dal;
using ReAl.Opinion.PgConn;
using ReAl.Opinion.Dal.Entidades;
using System.Windows.Forms;
#endregion

namespace ReAl.Opinion.Dal.Interface
{
	public interface inEncEstado: IDisposable
	{
		string getTableScript();
		dynamic GetColumnType(object valor,entEncEstado.Fields myField);
		entEncEstado ObtenerObjeto(int intid_informante, int intid_seccion);
		entEncEstado ObtenerObjeto(int intid_informante, int intid_seccion, ref cTrans localTrans);
		entEncEstado ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		entEncEstado ObtenerObjeto(Hashtable htbFiltro);
		entEncEstado ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		entEncEstado ObtenerObjeto(Hashtable htbFiltro, ref cTrans localTrans);
		entEncEstado ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		entEncEstado ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		entEncEstado ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		entEncEstado ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		entEncEstado ObtenerObjeto(entEncEstado.Fields searchField, object searchValue);
		entEncEstado ObtenerObjeto(entEncEstado.Fields searchField, object searchValue, ref cTrans localTrans);
		entEncEstado ObtenerObjeto(entEncEstado.Fields searchField, object searchValue, string strParamAdicionales);
		entEncEstado ObtenerObjeto(entEncEstado.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Dictionary<String, entEncEstado> ObtenerDiccionario();
		Dictionary<String, entEncEstado> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, entEncEstado> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Dictionary<String, entEncEstado> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, entEncEstado> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Dictionary<String, entEncEstado> ObtenerDiccionario(entEncEstado.Fields searchField, object searchValue);
		Dictionary<String, entEncEstado> ObtenerDiccionario(entEncEstado.Fields searchField, object searchValue, ref cTrans localTrans);
		Dictionary<String, entEncEstado> ObtenerDiccionario(entEncEstado.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, entEncEstado> ObtenerDiccionario(entEncEstado.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entEncEstado> ObtenerLista();
		List<entEncEstado> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entEncEstado> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entEncEstado> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entEncEstado> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entEncEstado> ObtenerLista(entEncEstado.Fields searchField, object searchValue);
		List<entEncEstado> ObtenerLista(entEncEstado.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entEncEstado> ObtenerLista(entEncEstado.Fields searchField, object searchValue, string strParamAdicionales);
		List<entEncEstado> ObtenerLista(entEncEstado.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entEncEstado> ObtenerLista(Hashtable htbFiltro);
		List<entEncEstado> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<entEncEstado> ObtenerLista(Hashtable htbFiltro, ref cTrans localTrans);
		List<entEncEstado> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		
		List<entEncEstado> ObtenerListaDesdeVista(String strVista);
		List<entEncEstado> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entEncEstado> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entEncEstado> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entEncEstado> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entEncEstado> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<entEncEstado> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref cTrans localTrans);
		List<entEncEstado> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<entEncEstado> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		List<entEncEstado> ObtenerListaDesdeVista(String strVista, entEncEstado.Fields searchField, object searchValue);
		List<entEncEstado> ObtenerListaDesdeVista(String strVista, entEncEstado.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entEncEstado> ObtenerListaDesdeVista(String strVista, entEncEstado.Fields searchField, object searchValue, string strParamAdicionales);
		List<entEncEstado> ObtenerListaDesdeVista(String strVista, entEncEstado.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Queue<entEncEstado> ObtenerCola();
		Queue<entEncEstado> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<entEncEstado> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Queue<entEncEstado> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<entEncEstado> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Queue<entEncEstado> ObtenerCola(entEncEstado.Fields searchField, object searchValue);
		Queue<entEncEstado> ObtenerCola(entEncEstado.Fields searchField, object searchValue, ref cTrans localTrans);
		Queue<entEncEstado> ObtenerCola(entEncEstado.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<entEncEstado> ObtenerCola(entEncEstado.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Stack<entEncEstado> ObtenerPila();
		Stack<entEncEstado> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<entEncEstado> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Stack<entEncEstado> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<entEncEstado> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Stack<entEncEstado> ObtenerPila(entEncEstado.Fields searchField, object searchValue);
		Stack<entEncEstado> ObtenerPila(entEncEstado.Fields searchField, object searchValue, ref cTrans localTrans);
		Stack<entEncEstado> ObtenerPila(entEncEstado.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<entEncEstado> ObtenerPila(entEncEstado.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		string CreatePK(string[] args);
		
		bool Insert(entEncEstado obj);
		bool Insert(entEncEstado obj, ref cTrans localTrans);
		int Update(entEncEstado obj);
		int Update(entEncEstado obj, ref cTrans localTrans);
		int Delete(entEncEstado obj);
		int Delete(entEncEstado obj, ref cTrans localTrans);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(entEncEstado.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(entEncEstado.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entEncEstado.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entEncEstado.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entEncEstado.Fields valueField, entEncEstado.Fields textField);
		void CargarComboBox(ref ComboBox cmb, entEncEstado.Fields valueField, entEncEstado.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entEncEstado.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, entEncEstado.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entEncEstado.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entEncEstado.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entEncEstado.Fields valueField, String textField, entEncEstado.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entEncEstado.Fields valueField, String textField, entEncEstado.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entEncEstado.Fields valueField, entEncEstado.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entEncEstado.Fields valueField, entEncEstado.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entEncEstado.Fields valueField, entEncEstado.Fields textField, entEncEstado.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entEncEstado.Fields valueField, entEncEstado.Fields textField, entEncEstado.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, entEncEstado.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, entEncEstado.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entEncEstado.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entEncEstado.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(entEncEstado.Fields refField);
		int FuncionesCount(entEncEstado.Fields refField, entEncEstado.Fields whereField, object valueField);
		int FuncionesCount(entEncEstado.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(entEncEstado.Fields refField);
		int FuncionesMin(entEncEstado.Fields refField, entEncEstado.Fields whereField, object valueField);
		int FuncionesMin(entEncEstado.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(entEncEstado.Fields refField);
		int FuncionesMax(entEncEstado.Fields refField, entEncEstado.Fields whereField, object valueField);
		int FuncionesMax(entEncEstado.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(entEncEstado.Fields refField);
		int FuncionesSum(entEncEstado.Fields refField, entEncEstado.Fields whereField, object valueField);
		int FuncionesSum(entEncEstado.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(entEncEstado.Fields refField);
		int FuncionesAvg(entEncEstado.Fields refField, entEncEstado.Fields whereField, object valueField);
		int FuncionesAvg(entEncEstado.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

