#region 
/***********************************************************************************************************
	NOMBRE:       inCatDepartamento
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla cat_departamento

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.Opinion.Dal;
using ReAl.Opinion.PgConn;
using ReAl.Opinion.Dal.Entidades;
using System.Windows.Forms;
#endregion

namespace ReAl.Opinion.Dal.Interface
{
	public interface inCatDepartamento: IDisposable
	{
		string getTableScript();
		dynamic GetColumnType(object valor,entCatDepartamento.Fields myField);
		entCatDepartamento ObtenerObjeto(int intid_departamento);
		entCatDepartamento ObtenerObjeto(int intid_departamento, ref cTrans localTrans);
		entCatDepartamento ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		entCatDepartamento ObtenerObjeto(Hashtable htbFiltro);
		entCatDepartamento ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		entCatDepartamento ObtenerObjeto(Hashtable htbFiltro, ref cTrans localTrans);
		entCatDepartamento ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		entCatDepartamento ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		entCatDepartamento ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		entCatDepartamento ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		entCatDepartamento ObtenerObjeto(entCatDepartamento.Fields searchField, object searchValue);
		entCatDepartamento ObtenerObjeto(entCatDepartamento.Fields searchField, object searchValue, ref cTrans localTrans);
		entCatDepartamento ObtenerObjeto(entCatDepartamento.Fields searchField, object searchValue, string strParamAdicionales);
		entCatDepartamento ObtenerObjeto(entCatDepartamento.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Dictionary<String, entCatDepartamento> ObtenerDiccionario();
		Dictionary<String, entCatDepartamento> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, entCatDepartamento> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Dictionary<String, entCatDepartamento> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, entCatDepartamento> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Dictionary<String, entCatDepartamento> ObtenerDiccionario(entCatDepartamento.Fields searchField, object searchValue);
		Dictionary<String, entCatDepartamento> ObtenerDiccionario(entCatDepartamento.Fields searchField, object searchValue, ref cTrans localTrans);
		Dictionary<String, entCatDepartamento> ObtenerDiccionario(entCatDepartamento.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, entCatDepartamento> ObtenerDiccionario(entCatDepartamento.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entCatDepartamento> ObtenerLista();
		List<entCatDepartamento> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entCatDepartamento> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entCatDepartamento> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entCatDepartamento> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entCatDepartamento> ObtenerLista(entCatDepartamento.Fields searchField, object searchValue);
		List<entCatDepartamento> ObtenerLista(entCatDepartamento.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entCatDepartamento> ObtenerLista(entCatDepartamento.Fields searchField, object searchValue, string strParamAdicionales);
		List<entCatDepartamento> ObtenerLista(entCatDepartamento.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entCatDepartamento> ObtenerLista(Hashtable htbFiltro);
		List<entCatDepartamento> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<entCatDepartamento> ObtenerLista(Hashtable htbFiltro, ref cTrans localTrans);
		List<entCatDepartamento> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		
		List<entCatDepartamento> ObtenerListaDesdeVista(String strVista);
		List<entCatDepartamento> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entCatDepartamento> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entCatDepartamento> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entCatDepartamento> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entCatDepartamento> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<entCatDepartamento> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref cTrans localTrans);
		List<entCatDepartamento> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<entCatDepartamento> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		List<entCatDepartamento> ObtenerListaDesdeVista(String strVista, entCatDepartamento.Fields searchField, object searchValue);
		List<entCatDepartamento> ObtenerListaDesdeVista(String strVista, entCatDepartamento.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entCatDepartamento> ObtenerListaDesdeVista(String strVista, entCatDepartamento.Fields searchField, object searchValue, string strParamAdicionales);
		List<entCatDepartamento> ObtenerListaDesdeVista(String strVista, entCatDepartamento.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Queue<entCatDepartamento> ObtenerCola();
		Queue<entCatDepartamento> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<entCatDepartamento> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Queue<entCatDepartamento> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<entCatDepartamento> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Queue<entCatDepartamento> ObtenerCola(entCatDepartamento.Fields searchField, object searchValue);
		Queue<entCatDepartamento> ObtenerCola(entCatDepartamento.Fields searchField, object searchValue, ref cTrans localTrans);
		Queue<entCatDepartamento> ObtenerCola(entCatDepartamento.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<entCatDepartamento> ObtenerCola(entCatDepartamento.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Stack<entCatDepartamento> ObtenerPila();
		Stack<entCatDepartamento> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<entCatDepartamento> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Stack<entCatDepartamento> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<entCatDepartamento> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Stack<entCatDepartamento> ObtenerPila(entCatDepartamento.Fields searchField, object searchValue);
		Stack<entCatDepartamento> ObtenerPila(entCatDepartamento.Fields searchField, object searchValue, ref cTrans localTrans);
		Stack<entCatDepartamento> ObtenerPila(entCatDepartamento.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<entCatDepartamento> ObtenerPila(entCatDepartamento.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		string CreatePK(string[] args);
		
		bool Insert(entCatDepartamento obj);
		bool Insert(entCatDepartamento obj, ref cTrans localTrans);
		int Update(entCatDepartamento obj);
		int Update(entCatDepartamento obj, ref cTrans localTrans);
		int Delete(entCatDepartamento obj);
		int Delete(entCatDepartamento obj, ref cTrans localTrans);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(entCatDepartamento.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(entCatDepartamento.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entCatDepartamento.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entCatDepartamento.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entCatDepartamento.Fields valueField, entCatDepartamento.Fields textField);
		void CargarComboBox(ref ComboBox cmb, entCatDepartamento.Fields valueField, entCatDepartamento.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entCatDepartamento.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, entCatDepartamento.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entCatDepartamento.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entCatDepartamento.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entCatDepartamento.Fields valueField, String textField, entCatDepartamento.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entCatDepartamento.Fields valueField, String textField, entCatDepartamento.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entCatDepartamento.Fields valueField, entCatDepartamento.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entCatDepartamento.Fields valueField, entCatDepartamento.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entCatDepartamento.Fields valueField, entCatDepartamento.Fields textField, entCatDepartamento.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entCatDepartamento.Fields valueField, entCatDepartamento.Fields textField, entCatDepartamento.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, entCatDepartamento.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, entCatDepartamento.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entCatDepartamento.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entCatDepartamento.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(entCatDepartamento.Fields refField);
		int FuncionesCount(entCatDepartamento.Fields refField, entCatDepartamento.Fields whereField, object valueField);
		int FuncionesCount(entCatDepartamento.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(entCatDepartamento.Fields refField);
		int FuncionesMin(entCatDepartamento.Fields refField, entCatDepartamento.Fields whereField, object valueField);
		int FuncionesMin(entCatDepartamento.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(entCatDepartamento.Fields refField);
		int FuncionesMax(entCatDepartamento.Fields refField, entCatDepartamento.Fields whereField, object valueField);
		int FuncionesMax(entCatDepartamento.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(entCatDepartamento.Fields refField);
		int FuncionesSum(entCatDepartamento.Fields refField, entCatDepartamento.Fields whereField, object valueField);
		int FuncionesSum(entCatDepartamento.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(entCatDepartamento.Fields refField);
		int FuncionesAvg(entCatDepartamento.Fields refField, entCatDepartamento.Fields whereField, object valueField);
		int FuncionesAvg(entCatDepartamento.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

