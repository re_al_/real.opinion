#region 
/***********************************************************************************************************
	NOMBRE:       inSegUsuariorestriccion
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla seg_usuariorestriccion

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.Opinion.Dal;
using ReAl.Opinion.PgConn;
using ReAl.Opinion.Dal.Entidades;
using System.Windows.Forms;
#endregion

namespace ReAl.Opinion.Dal.Interface
{
	public interface inSegUsuariorestriccion: IDisposable
	{
		string getTableScript();
		dynamic GetColumnType(object valor,entSegUsuariorestriccion.Fields myField);
		entSegUsuariorestriccion ObtenerObjeto(int intid_usurestriccion);
		entSegUsuariorestriccion ObtenerObjeto(int intid_usurestriccion, ref cTrans localTrans);
		entSegUsuariorestriccion ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		entSegUsuariorestriccion ObtenerObjeto(Hashtable htbFiltro);
		entSegUsuariorestriccion ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		entSegUsuariorestriccion ObtenerObjeto(Hashtable htbFiltro, ref cTrans localTrans);
		entSegUsuariorestriccion ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		entSegUsuariorestriccion ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		entSegUsuariorestriccion ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		entSegUsuariorestriccion ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		entSegUsuariorestriccion ObtenerObjeto(entSegUsuariorestriccion.Fields searchField, object searchValue);
		entSegUsuariorestriccion ObtenerObjeto(entSegUsuariorestriccion.Fields searchField, object searchValue, ref cTrans localTrans);
		entSegUsuariorestriccion ObtenerObjeto(entSegUsuariorestriccion.Fields searchField, object searchValue, string strParamAdicionales);
		entSegUsuariorestriccion ObtenerObjeto(entSegUsuariorestriccion.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Dictionary<String, entSegUsuariorestriccion> ObtenerDiccionario();
		Dictionary<String, entSegUsuariorestriccion> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, entSegUsuariorestriccion> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Dictionary<String, entSegUsuariorestriccion> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, entSegUsuariorestriccion> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Dictionary<String, entSegUsuariorestriccion> ObtenerDiccionario(entSegUsuariorestriccion.Fields searchField, object searchValue);
		Dictionary<String, entSegUsuariorestriccion> ObtenerDiccionario(entSegUsuariorestriccion.Fields searchField, object searchValue, ref cTrans localTrans);
		Dictionary<String, entSegUsuariorestriccion> ObtenerDiccionario(entSegUsuariorestriccion.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, entSegUsuariorestriccion> ObtenerDiccionario(entSegUsuariorestriccion.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entSegUsuariorestriccion> ObtenerLista();
		List<entSegUsuariorestriccion> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entSegUsuariorestriccion> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entSegUsuariorestriccion> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entSegUsuariorestriccion> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entSegUsuariorestriccion> ObtenerLista(entSegUsuariorestriccion.Fields searchField, object searchValue);
		List<entSegUsuariorestriccion> ObtenerLista(entSegUsuariorestriccion.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entSegUsuariorestriccion> ObtenerLista(entSegUsuariorestriccion.Fields searchField, object searchValue, string strParamAdicionales);
		List<entSegUsuariorestriccion> ObtenerLista(entSegUsuariorestriccion.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entSegUsuariorestriccion> ObtenerLista(Hashtable htbFiltro);
		List<entSegUsuariorestriccion> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<entSegUsuariorestriccion> ObtenerLista(Hashtable htbFiltro, ref cTrans localTrans);
		List<entSegUsuariorestriccion> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		
		List<entSegUsuariorestriccion> ObtenerListaDesdeVista(String strVista);
		List<entSegUsuariorestriccion> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entSegUsuariorestriccion> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entSegUsuariorestriccion> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entSegUsuariorestriccion> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entSegUsuariorestriccion> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<entSegUsuariorestriccion> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref cTrans localTrans);
		List<entSegUsuariorestriccion> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<entSegUsuariorestriccion> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		List<entSegUsuariorestriccion> ObtenerListaDesdeVista(String strVista, entSegUsuariorestriccion.Fields searchField, object searchValue);
		List<entSegUsuariorestriccion> ObtenerListaDesdeVista(String strVista, entSegUsuariorestriccion.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entSegUsuariorestriccion> ObtenerListaDesdeVista(String strVista, entSegUsuariorestriccion.Fields searchField, object searchValue, string strParamAdicionales);
		List<entSegUsuariorestriccion> ObtenerListaDesdeVista(String strVista, entSegUsuariorestriccion.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Queue<entSegUsuariorestriccion> ObtenerCola();
		Queue<entSegUsuariorestriccion> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<entSegUsuariorestriccion> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Queue<entSegUsuariorestriccion> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<entSegUsuariorestriccion> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Queue<entSegUsuariorestriccion> ObtenerCola(entSegUsuariorestriccion.Fields searchField, object searchValue);
		Queue<entSegUsuariorestriccion> ObtenerCola(entSegUsuariorestriccion.Fields searchField, object searchValue, ref cTrans localTrans);
		Queue<entSegUsuariorestriccion> ObtenerCola(entSegUsuariorestriccion.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<entSegUsuariorestriccion> ObtenerCola(entSegUsuariorestriccion.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Stack<entSegUsuariorestriccion> ObtenerPila();
		Stack<entSegUsuariorestriccion> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<entSegUsuariorestriccion> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Stack<entSegUsuariorestriccion> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<entSegUsuariorestriccion> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Stack<entSegUsuariorestriccion> ObtenerPila(entSegUsuariorestriccion.Fields searchField, object searchValue);
		Stack<entSegUsuariorestriccion> ObtenerPila(entSegUsuariorestriccion.Fields searchField, object searchValue, ref cTrans localTrans);
		Stack<entSegUsuariorestriccion> ObtenerPila(entSegUsuariorestriccion.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<entSegUsuariorestriccion> ObtenerPila(entSegUsuariorestriccion.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		string CreatePK(string[] args);
		
		bool Insert(entSegUsuariorestriccion obj);
		bool Insert(entSegUsuariorestriccion obj, ref cTrans localTrans);
		int Update(entSegUsuariorestriccion obj);
		int Update(entSegUsuariorestriccion obj, ref cTrans localTrans);
		int Delete(entSegUsuariorestriccion obj);
		int Delete(entSegUsuariorestriccion obj, ref cTrans localTrans);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(entSegUsuariorestriccion.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(entSegUsuariorestriccion.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entSegUsuariorestriccion.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entSegUsuariorestriccion.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entSegUsuariorestriccion.Fields valueField, entSegUsuariorestriccion.Fields textField);
		void CargarComboBox(ref ComboBox cmb, entSegUsuariorestriccion.Fields valueField, entSegUsuariorestriccion.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entSegUsuariorestriccion.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, entSegUsuariorestriccion.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entSegUsuariorestriccion.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entSegUsuariorestriccion.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entSegUsuariorestriccion.Fields valueField, String textField, entSegUsuariorestriccion.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entSegUsuariorestriccion.Fields valueField, String textField, entSegUsuariorestriccion.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entSegUsuariorestriccion.Fields valueField, entSegUsuariorestriccion.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entSegUsuariorestriccion.Fields valueField, entSegUsuariorestriccion.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entSegUsuariorestriccion.Fields valueField, entSegUsuariorestriccion.Fields textField, entSegUsuariorestriccion.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entSegUsuariorestriccion.Fields valueField, entSegUsuariorestriccion.Fields textField, entSegUsuariorestriccion.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, entSegUsuariorestriccion.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, entSegUsuariorestriccion.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entSegUsuariorestriccion.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entSegUsuariorestriccion.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(entSegUsuariorestriccion.Fields refField);
		int FuncionesCount(entSegUsuariorestriccion.Fields refField, entSegUsuariorestriccion.Fields whereField, object valueField);
		int FuncionesCount(entSegUsuariorestriccion.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(entSegUsuariorestriccion.Fields refField);
		int FuncionesMin(entSegUsuariorestriccion.Fields refField, entSegUsuariorestriccion.Fields whereField, object valueField);
		int FuncionesMin(entSegUsuariorestriccion.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(entSegUsuariorestriccion.Fields refField);
		int FuncionesMax(entSegUsuariorestriccion.Fields refField, entSegUsuariorestriccion.Fields whereField, object valueField);
		int FuncionesMax(entSegUsuariorestriccion.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(entSegUsuariorestriccion.Fields refField);
		int FuncionesSum(entSegUsuariorestriccion.Fields refField, entSegUsuariorestriccion.Fields whereField, object valueField);
		int FuncionesSum(entSegUsuariorestriccion.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(entSegUsuariorestriccion.Fields refField);
		int FuncionesAvg(entSegUsuariorestriccion.Fields refField, entSegUsuariorestriccion.Fields whereField, object valueField);
		int FuncionesAvg(entSegUsuariorestriccion.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

