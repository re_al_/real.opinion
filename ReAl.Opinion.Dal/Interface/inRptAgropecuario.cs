#region 
/***********************************************************************************************************
	NOMBRE:       inRptAgropecuario
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla rpt_agropecuario

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        22/04/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.Opinion.Dal;
using ReAl.Opinion.Dal.Entidades;
using System.Windows.Forms;
using ReAl.Opinion.PgConn;

#endregion

namespace ReAl.Opinion.Dal.Interface
{
	public interface inRptAgropecuario: IDisposable
	{
		string getTableScript();
		dynamic GetColumnType(object valor,entRptAgropecuario.Fields myField);
		entRptAgropecuario ObtenerObjeto(int intgestion, int intid_persona);
		entRptAgropecuario ObtenerObjeto(int intgestion, int intid_persona, ref cTrans localTrans);
		entRptAgropecuario ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		entRptAgropecuario ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		entRptAgropecuario ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		entRptAgropecuario ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		entRptAgropecuario ObtenerObjeto(entRptAgropecuario.Fields searchField, object searchValue);
		entRptAgropecuario ObtenerObjeto(entRptAgropecuario.Fields searchField, object searchValue, ref cTrans localTrans);
		entRptAgropecuario ObtenerObjeto(entRptAgropecuario.Fields searchField, object searchValue, string strParamAdicionales);
		entRptAgropecuario ObtenerObjeto(entRptAgropecuario.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Dictionary<String, entRptAgropecuario> ObtenerDiccionario();
		Dictionary<String, entRptAgropecuario> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, entRptAgropecuario> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Dictionary<String, entRptAgropecuario> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, entRptAgropecuario> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Dictionary<String, entRptAgropecuario> ObtenerDiccionario(entRptAgropecuario.Fields searchField, object searchValue);
		Dictionary<String, entRptAgropecuario> ObtenerDiccionario(entRptAgropecuario.Fields searchField, object searchValue, ref cTrans localTrans);
		Dictionary<String, entRptAgropecuario> ObtenerDiccionario(entRptAgropecuario.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, entRptAgropecuario> ObtenerDiccionario(entRptAgropecuario.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entRptAgropecuario> ObtenerLista();
		List<entRptAgropecuario> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entRptAgropecuario> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entRptAgropecuario> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entRptAgropecuario> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entRptAgropecuario> ObtenerLista(entRptAgropecuario.Fields searchField, object searchValue);
		List<entRptAgropecuario> ObtenerLista(entRptAgropecuario.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entRptAgropecuario> ObtenerLista(entRptAgropecuario.Fields searchField, object searchValue, string strParamAdicionales);
		List<entRptAgropecuario> ObtenerLista(entRptAgropecuario.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Queue<entRptAgropecuario> ObtenerCola();
		Queue<entRptAgropecuario> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<entRptAgropecuario> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Queue<entRptAgropecuario> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<entRptAgropecuario> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Queue<entRptAgropecuario> ObtenerCola(entRptAgropecuario.Fields searchField, object searchValue);
		Queue<entRptAgropecuario> ObtenerCola(entRptAgropecuario.Fields searchField, object searchValue, ref cTrans localTrans);
		Queue<entRptAgropecuario> ObtenerCola(entRptAgropecuario.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<entRptAgropecuario> ObtenerCola(entRptAgropecuario.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Stack<entRptAgropecuario> ObtenerPila();
		Stack<entRptAgropecuario> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<entRptAgropecuario> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Stack<entRptAgropecuario> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<entRptAgropecuario> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Stack<entRptAgropecuario> ObtenerPila(entRptAgropecuario.Fields searchField, object searchValue);
		Stack<entRptAgropecuario> ObtenerPila(entRptAgropecuario.Fields searchField, object searchValue, ref cTrans localTrans);
		Stack<entRptAgropecuario> ObtenerPila(entRptAgropecuario.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<entRptAgropecuario> ObtenerPila(entRptAgropecuario.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		string CreatePK(string[] args);
		
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable CargarDataTable();
		DataTable CargarDataTable(String condicionesWhere);
		DataTable CargarDataTable(ArrayList arrColumnas);
		DataTable CargarDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable CargarDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable CargarDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable CargarDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable CargarDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable CargarDataTable(entRptAgropecuario.Fields searchField, object searchValue);
		DataTable CargarDataTable(entRptAgropecuario.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable CargarDataTable(ArrayList arrColumnas, entRptAgropecuario.Fields searchField, object searchValue);
		DataTable CargarDataTable(ArrayList arrColumnas, entRptAgropecuario.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable CargarDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable CargarDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable CargarDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entRptAgropecuario.Fields valueField, entRptAgropecuario.Fields textField);
		void CargarComboBox(ref ComboBox cmb, entRptAgropecuario.Fields valueField, entRptAgropecuario.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entRptAgropecuario.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, entRptAgropecuario.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entRptAgropecuario.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entRptAgropecuario.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entRptAgropecuario.Fields valueField, String textField, entRptAgropecuario.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entRptAgropecuario.Fields valueField, String textField, entRptAgropecuario.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entRptAgropecuario.Fields valueField, entRptAgropecuario.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entRptAgropecuario.Fields valueField, entRptAgropecuario.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entRptAgropecuario.Fields valueField, entRptAgropecuario.Fields textField, entRptAgropecuario.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entRptAgropecuario.Fields valueField, entRptAgropecuario.Fields textField, entRptAgropecuario.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, entRptAgropecuario.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, entRptAgropecuario.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entRptAgropecuario.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entRptAgropecuario.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		object FuncionesCount(entRptAgropecuario.Fields refField);
		object FuncionesCount(entRptAgropecuario.Fields refField, entRptAgropecuario.Fields whereField, object valueField);
		object FuncionesCount(entRptAgropecuario.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		object FuncionesMin(entRptAgropecuario.Fields refField);
		object FuncionesMin(entRptAgropecuario.Fields refField, entRptAgropecuario.Fields whereField, object valueField);
		object FuncionesMin(entRptAgropecuario.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		object FuncionesMax(entRptAgropecuario.Fields refField);
		object FuncionesMax(entRptAgropecuario.Fields refField, entRptAgropecuario.Fields whereField, object valueField);
		object FuncionesMax(entRptAgropecuario.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		object FuncionesSum(entRptAgropecuario.Fields refField);
		object FuncionesSum(entRptAgropecuario.Fields refField, entRptAgropecuario.Fields whereField, object valueField);
		object FuncionesSum(entRptAgropecuario.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		object FuncionesAvg(entRptAgropecuario.Fields refField);
		object FuncionesAvg(entRptAgropecuario.Fields refField, entRptAgropecuario.Fields whereField, object valueField);
		object FuncionesAvg(entRptAgropecuario.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

