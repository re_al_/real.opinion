#region 
/***********************************************************************************************************
	NOMBRE:       inEncRespuesta
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla enc_respuesta

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.Opinion.Dal;
using ReAl.Opinion.PgConn;
using ReAl.Opinion.Dal.Entidades;
using System.Windows.Forms;
#endregion

namespace ReAl.Opinion.Dal.Interface
{
	public interface inEncRespuesta: IDisposable
	{
		string getTableScript();
		dynamic GetColumnType(object valor,entEncRespuesta.Fields myField);
		entEncRespuesta ObtenerObjeto(Int64 Int64id_respuesta);
		entEncRespuesta ObtenerObjeto(Int64 Int64id_respuesta, ref cTrans localTrans);
		entEncRespuesta ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		entEncRespuesta ObtenerObjeto(Hashtable htbFiltro);
		entEncRespuesta ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		entEncRespuesta ObtenerObjeto(Hashtable htbFiltro, ref cTrans localTrans);
		entEncRespuesta ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		entEncRespuesta ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		entEncRespuesta ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		entEncRespuesta ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		entEncRespuesta ObtenerObjeto(entEncRespuesta.Fields searchField, object searchValue);
		entEncRespuesta ObtenerObjeto(entEncRespuesta.Fields searchField, object searchValue, ref cTrans localTrans);
		entEncRespuesta ObtenerObjeto(entEncRespuesta.Fields searchField, object searchValue, string strParamAdicionales);
		entEncRespuesta ObtenerObjeto(entEncRespuesta.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Dictionary<String, entEncRespuesta> ObtenerDiccionario();
		Dictionary<String, entEncRespuesta> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, entEncRespuesta> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Dictionary<String, entEncRespuesta> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, entEncRespuesta> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Dictionary<String, entEncRespuesta> ObtenerDiccionario(entEncRespuesta.Fields searchField, object searchValue);
		Dictionary<String, entEncRespuesta> ObtenerDiccionario(entEncRespuesta.Fields searchField, object searchValue, ref cTrans localTrans);
		Dictionary<String, entEncRespuesta> ObtenerDiccionario(entEncRespuesta.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, entEncRespuesta> ObtenerDiccionario(entEncRespuesta.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entEncRespuesta> ObtenerLista();
		List<entEncRespuesta> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entEncRespuesta> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entEncRespuesta> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entEncRespuesta> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entEncRespuesta> ObtenerLista(entEncRespuesta.Fields searchField, object searchValue);
		List<entEncRespuesta> ObtenerLista(entEncRespuesta.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entEncRespuesta> ObtenerLista(entEncRespuesta.Fields searchField, object searchValue, string strParamAdicionales);
		List<entEncRespuesta> ObtenerLista(entEncRespuesta.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entEncRespuesta> ObtenerLista(Hashtable htbFiltro);
		List<entEncRespuesta> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<entEncRespuesta> ObtenerLista(Hashtable htbFiltro, ref cTrans localTrans);
		List<entEncRespuesta> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		
		List<entEncRespuesta> ObtenerListaDesdeVista(String strVista);
		List<entEncRespuesta> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entEncRespuesta> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entEncRespuesta> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entEncRespuesta> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entEncRespuesta> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<entEncRespuesta> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref cTrans localTrans);
		List<entEncRespuesta> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<entEncRespuesta> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		List<entEncRespuesta> ObtenerListaDesdeVista(String strVista, entEncRespuesta.Fields searchField, object searchValue);
		List<entEncRespuesta> ObtenerListaDesdeVista(String strVista, entEncRespuesta.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entEncRespuesta> ObtenerListaDesdeVista(String strVista, entEncRespuesta.Fields searchField, object searchValue, string strParamAdicionales);
		List<entEncRespuesta> ObtenerListaDesdeVista(String strVista, entEncRespuesta.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Queue<entEncRespuesta> ObtenerCola();
		Queue<entEncRespuesta> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<entEncRespuesta> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Queue<entEncRespuesta> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<entEncRespuesta> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Queue<entEncRespuesta> ObtenerCola(entEncRespuesta.Fields searchField, object searchValue);
		Queue<entEncRespuesta> ObtenerCola(entEncRespuesta.Fields searchField, object searchValue, ref cTrans localTrans);
		Queue<entEncRespuesta> ObtenerCola(entEncRespuesta.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<entEncRespuesta> ObtenerCola(entEncRespuesta.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Stack<entEncRespuesta> ObtenerPila();
		Stack<entEncRespuesta> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<entEncRespuesta> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Stack<entEncRespuesta> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<entEncRespuesta> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Stack<entEncRespuesta> ObtenerPila(entEncRespuesta.Fields searchField, object searchValue);
		Stack<entEncRespuesta> ObtenerPila(entEncRespuesta.Fields searchField, object searchValue, ref cTrans localTrans);
		Stack<entEncRespuesta> ObtenerPila(entEncRespuesta.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<entEncRespuesta> ObtenerPila(entEncRespuesta.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		string CreatePK(string[] args);
		
		bool Insert(entEncRespuesta obj);
		bool Insert(entEncRespuesta obj, ref cTrans localTrans);
		int Update(entEncRespuesta obj);
		int Update(entEncRespuesta obj, ref cTrans localTrans);
		int Delete(entEncRespuesta obj);
		int Delete(entEncRespuesta obj, ref cTrans localTrans);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(entEncRespuesta.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(entEncRespuesta.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entEncRespuesta.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entEncRespuesta.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entEncRespuesta.Fields valueField, entEncRespuesta.Fields textField);
		void CargarComboBox(ref ComboBox cmb, entEncRespuesta.Fields valueField, entEncRespuesta.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entEncRespuesta.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, entEncRespuesta.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entEncRespuesta.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entEncRespuesta.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entEncRespuesta.Fields valueField, String textField, entEncRespuesta.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entEncRespuesta.Fields valueField, String textField, entEncRespuesta.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entEncRespuesta.Fields valueField, entEncRespuesta.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entEncRespuesta.Fields valueField, entEncRespuesta.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entEncRespuesta.Fields valueField, entEncRespuesta.Fields textField, entEncRespuesta.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entEncRespuesta.Fields valueField, entEncRespuesta.Fields textField, entEncRespuesta.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, entEncRespuesta.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, entEncRespuesta.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entEncRespuesta.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entEncRespuesta.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(entEncRespuesta.Fields refField);
		int FuncionesCount(entEncRespuesta.Fields refField, entEncRespuesta.Fields whereField, object valueField);
		int FuncionesCount(entEncRespuesta.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(entEncRespuesta.Fields refField);
		int FuncionesMin(entEncRespuesta.Fields refField, entEncRespuesta.Fields whereField, object valueField);
		int FuncionesMin(entEncRespuesta.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(entEncRespuesta.Fields refField);
		int FuncionesMax(entEncRespuesta.Fields refField, entEncRespuesta.Fields whereField, object valueField);
		int FuncionesMax(entEncRespuesta.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(entEncRespuesta.Fields refField);
		int FuncionesSum(entEncRespuesta.Fields refField, entEncRespuesta.Fields whereField, object valueField);
		int FuncionesSum(entEncRespuesta.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(entEncRespuesta.Fields refField);
		int FuncionesAvg(entEncRespuesta.Fields refField, entEncRespuesta.Fields whereField, object valueField);
		int FuncionesAvg(entEncRespuesta.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

