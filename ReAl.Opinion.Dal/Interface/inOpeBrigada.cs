#region 
/***********************************************************************************************************
	NOMBRE:       inOpeBrigada
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla ope_brigada

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.Opinion.Dal;
using ReAl.Opinion.PgConn;
using ReAl.Opinion.Dal.Entidades;
using System.Windows.Forms;
#endregion

namespace ReAl.Opinion.Dal.Interface
{
	public interface inOpeBrigada: IDisposable
	{
		string getTableScript();
		dynamic GetColumnType(object valor,entOpeBrigada.Fields myField);
		entOpeBrigada ObtenerObjeto(int intid_brigada);
		entOpeBrigada ObtenerObjeto(int intid_brigada, ref cTrans localTrans);
		entOpeBrigada ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		entOpeBrigada ObtenerObjeto(Hashtable htbFiltro);
		entOpeBrigada ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		entOpeBrigada ObtenerObjeto(Hashtable htbFiltro, ref cTrans localTrans);
		entOpeBrigada ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		entOpeBrigada ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		entOpeBrigada ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		entOpeBrigada ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		entOpeBrigada ObtenerObjeto(entOpeBrigada.Fields searchField, object searchValue);
		entOpeBrigada ObtenerObjeto(entOpeBrigada.Fields searchField, object searchValue, ref cTrans localTrans);
		entOpeBrigada ObtenerObjeto(entOpeBrigada.Fields searchField, object searchValue, string strParamAdicionales);
		entOpeBrigada ObtenerObjeto(entOpeBrigada.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Dictionary<String, entOpeBrigada> ObtenerDiccionario();
		Dictionary<String, entOpeBrigada> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, entOpeBrigada> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Dictionary<String, entOpeBrigada> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, entOpeBrigada> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Dictionary<String, entOpeBrigada> ObtenerDiccionario(entOpeBrigada.Fields searchField, object searchValue);
		Dictionary<String, entOpeBrigada> ObtenerDiccionario(entOpeBrigada.Fields searchField, object searchValue, ref cTrans localTrans);
		Dictionary<String, entOpeBrigada> ObtenerDiccionario(entOpeBrigada.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, entOpeBrigada> ObtenerDiccionario(entOpeBrigada.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entOpeBrigada> ObtenerLista();
		List<entOpeBrigada> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entOpeBrigada> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entOpeBrigada> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entOpeBrigada> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entOpeBrigada> ObtenerLista(entOpeBrigada.Fields searchField, object searchValue);
		List<entOpeBrigada> ObtenerLista(entOpeBrigada.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entOpeBrigada> ObtenerLista(entOpeBrigada.Fields searchField, object searchValue, string strParamAdicionales);
		List<entOpeBrigada> ObtenerLista(entOpeBrigada.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entOpeBrigada> ObtenerLista(Hashtable htbFiltro);
		List<entOpeBrigada> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<entOpeBrigada> ObtenerLista(Hashtable htbFiltro, ref cTrans localTrans);
		List<entOpeBrigada> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		
		List<entOpeBrigada> ObtenerListaDesdeVista(String strVista);
		List<entOpeBrigada> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entOpeBrigada> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entOpeBrigada> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entOpeBrigada> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entOpeBrigada> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<entOpeBrigada> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref cTrans localTrans);
		List<entOpeBrigada> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<entOpeBrigada> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		List<entOpeBrigada> ObtenerListaDesdeVista(String strVista, entOpeBrigada.Fields searchField, object searchValue);
		List<entOpeBrigada> ObtenerListaDesdeVista(String strVista, entOpeBrigada.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entOpeBrigada> ObtenerListaDesdeVista(String strVista, entOpeBrigada.Fields searchField, object searchValue, string strParamAdicionales);
		List<entOpeBrigada> ObtenerListaDesdeVista(String strVista, entOpeBrigada.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Queue<entOpeBrigada> ObtenerCola();
		Queue<entOpeBrigada> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<entOpeBrigada> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Queue<entOpeBrigada> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<entOpeBrigada> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Queue<entOpeBrigada> ObtenerCola(entOpeBrigada.Fields searchField, object searchValue);
		Queue<entOpeBrigada> ObtenerCola(entOpeBrigada.Fields searchField, object searchValue, ref cTrans localTrans);
		Queue<entOpeBrigada> ObtenerCola(entOpeBrigada.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<entOpeBrigada> ObtenerCola(entOpeBrigada.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Stack<entOpeBrigada> ObtenerPila();
		Stack<entOpeBrigada> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<entOpeBrigada> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Stack<entOpeBrigada> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<entOpeBrigada> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Stack<entOpeBrigada> ObtenerPila(entOpeBrigada.Fields searchField, object searchValue);
		Stack<entOpeBrigada> ObtenerPila(entOpeBrigada.Fields searchField, object searchValue, ref cTrans localTrans);
		Stack<entOpeBrigada> ObtenerPila(entOpeBrigada.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<entOpeBrigada> ObtenerPila(entOpeBrigada.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		string CreatePK(string[] args);
		
		bool Insert(entOpeBrigada obj);
		bool Insert(entOpeBrigada obj, ref cTrans localTrans);
		int Update(entOpeBrigada obj);
		int Update(entOpeBrigada obj, ref cTrans localTrans);
		int Delete(entOpeBrigada obj);
		int Delete(entOpeBrigada obj, ref cTrans localTrans);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(entOpeBrigada.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(entOpeBrigada.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entOpeBrigada.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entOpeBrigada.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entOpeBrigada.Fields valueField, entOpeBrigada.Fields textField);
		void CargarComboBox(ref ComboBox cmb, entOpeBrigada.Fields valueField, entOpeBrigada.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entOpeBrigada.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, entOpeBrigada.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entOpeBrigada.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entOpeBrigada.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entOpeBrigada.Fields valueField, String textField, entOpeBrigada.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entOpeBrigada.Fields valueField, String textField, entOpeBrigada.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entOpeBrigada.Fields valueField, entOpeBrigada.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entOpeBrigada.Fields valueField, entOpeBrigada.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entOpeBrigada.Fields valueField, entOpeBrigada.Fields textField, entOpeBrigada.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entOpeBrigada.Fields valueField, entOpeBrigada.Fields textField, entOpeBrigada.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, entOpeBrigada.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, entOpeBrigada.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entOpeBrigada.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entOpeBrigada.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(entOpeBrigada.Fields refField);
		int FuncionesCount(entOpeBrigada.Fields refField, entOpeBrigada.Fields whereField, object valueField);
		int FuncionesCount(entOpeBrigada.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(entOpeBrigada.Fields refField);
		int FuncionesMin(entOpeBrigada.Fields refField, entOpeBrigada.Fields whereField, object valueField);
		int FuncionesMin(entOpeBrigada.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(entOpeBrigada.Fields refField);
		int FuncionesMax(entOpeBrigada.Fields refField, entOpeBrigada.Fields whereField, object valueField);
		int FuncionesMax(entOpeBrigada.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(entOpeBrigada.Fields refField);
		int FuncionesSum(entOpeBrigada.Fields refField, entOpeBrigada.Fields whereField, object valueField);
		int FuncionesSum(entOpeBrigada.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(entOpeBrigada.Fields refField);
		int FuncionesAvg(entOpeBrigada.Fields refField, entOpeBrigada.Fields whereField, object valueField);
		int FuncionesAvg(entOpeBrigada.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

