#region 
/***********************************************************************************************************
	NOMBRE:       inIndIndustriagps
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla ind_industriagps

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/05/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.Opinion.Dal;
using ReAl.Opinion.Dal.Entidades;
using System.Windows.Forms;
using ReAl.Opinion.PgConn;

#endregion

namespace ReAl.Opinion.Dal.Interface
{
	public interface inIndIndustriagps: IDisposable
	{
		string getTableScript();
		dynamic GetColumnType(object valor,entIndIndustriagps.Fields myField);
		entIndIndustriagps ObtenerObjeto(int intgestion, int intmes, int intid_gps, String Stringusucre);
		entIndIndustriagps ObtenerObjeto(int intgestion, int intmes, int intid_gps, String Stringusucre, ref cTrans localTrans);
		entIndIndustriagps ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		entIndIndustriagps ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		entIndIndustriagps ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		entIndIndustriagps ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		entIndIndustriagps ObtenerObjeto(entIndIndustriagps.Fields searchField, object searchValue);
		entIndIndustriagps ObtenerObjeto(entIndIndustriagps.Fields searchField, object searchValue, ref cTrans localTrans);
		entIndIndustriagps ObtenerObjeto(entIndIndustriagps.Fields searchField, object searchValue, string strParamAdicionales);
		entIndIndustriagps ObtenerObjeto(entIndIndustriagps.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Dictionary<String, entIndIndustriagps> ObtenerDiccionario();
		Dictionary<String, entIndIndustriagps> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, entIndIndustriagps> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Dictionary<String, entIndIndustriagps> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, entIndIndustriagps> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Dictionary<String, entIndIndustriagps> ObtenerDiccionario(entIndIndustriagps.Fields searchField, object searchValue);
		Dictionary<String, entIndIndustriagps> ObtenerDiccionario(entIndIndustriagps.Fields searchField, object searchValue, ref cTrans localTrans);
		Dictionary<String, entIndIndustriagps> ObtenerDiccionario(entIndIndustriagps.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, entIndIndustriagps> ObtenerDiccionario(entIndIndustriagps.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entIndIndustriagps> ObtenerLista();
		List<entIndIndustriagps> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entIndIndustriagps> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entIndIndustriagps> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entIndIndustriagps> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entIndIndustriagps> ObtenerLista(entIndIndustriagps.Fields searchField, object searchValue);
		List<entIndIndustriagps> ObtenerLista(entIndIndustriagps.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entIndIndustriagps> ObtenerLista(entIndIndustriagps.Fields searchField, object searchValue, string strParamAdicionales);
		List<entIndIndustriagps> ObtenerLista(entIndIndustriagps.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entIndIndustriagps> ObtenerListaDesdeVista(String strVista);
		List<entIndIndustriagps> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entIndIndustriagps> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entIndIndustriagps> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entIndIndustriagps> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entIndIndustriagps> ObtenerListaDesdeVista(String strVista, entIndIndustriagps.Fields searchField, object searchValue);
		List<entIndIndustriagps> ObtenerListaDesdeVista(String strVista, entIndIndustriagps.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entIndIndustriagps> ObtenerListaDesdeVista(String strVista, entIndIndustriagps.Fields searchField, object searchValue, string strParamAdicionales);
		List<entIndIndustriagps> ObtenerListaDesdeVista(String strVista, entIndIndustriagps.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Queue<entIndIndustriagps> ObtenerCola();
		Queue<entIndIndustriagps> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<entIndIndustriagps> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Queue<entIndIndustriagps> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<entIndIndustriagps> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Queue<entIndIndustriagps> ObtenerCola(entIndIndustriagps.Fields searchField, object searchValue);
		Queue<entIndIndustriagps> ObtenerCola(entIndIndustriagps.Fields searchField, object searchValue, ref cTrans localTrans);
		Queue<entIndIndustriagps> ObtenerCola(entIndIndustriagps.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<entIndIndustriagps> ObtenerCola(entIndIndustriagps.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Stack<entIndIndustriagps> ObtenerPila();
		Stack<entIndIndustriagps> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<entIndIndustriagps> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Stack<entIndIndustriagps> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<entIndIndustriagps> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Stack<entIndIndustriagps> ObtenerPila(entIndIndustriagps.Fields searchField, object searchValue);
		Stack<entIndIndustriagps> ObtenerPila(entIndIndustriagps.Fields searchField, object searchValue, ref cTrans localTrans);
		Stack<entIndIndustriagps> ObtenerPila(entIndIndustriagps.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<entIndIndustriagps> ObtenerPila(entIndIndustriagps.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		string CreatePK(string[] args);
		
		bool Insert(entIndIndustriagps obj);
		bool Insert(entIndIndustriagps obj, ref cTrans localTrans);
		int Update(entIndIndustriagps obj);
		int Update(entIndIndustriagps obj, ref cTrans localTrans);
		int Delete(entIndIndustriagps obj);
		int Delete(entIndIndustriagps obj, ref cTrans localTrans);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable CargarDataTable();
		DataTable CargarDataTable(String condicionesWhere);
		DataTable CargarDataTable(ArrayList arrColumnas);
		DataTable CargarDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable CargarDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable CargarDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable CargarDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable CargarDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable CargarDataTable(entIndIndustriagps.Fields searchField, object searchValue);
		DataTable CargarDataTable(entIndIndustriagps.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable CargarDataTable(ArrayList arrColumnas, entIndIndustriagps.Fields searchField, object searchValue);
		DataTable CargarDataTable(ArrayList arrColumnas, entIndIndustriagps.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable CargarDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable CargarDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable CargarDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entIndIndustriagps.Fields valueField, entIndIndustriagps.Fields textField);
		void CargarComboBox(ref ComboBox cmb, entIndIndustriagps.Fields valueField, entIndIndustriagps.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entIndIndustriagps.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, entIndIndustriagps.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entIndIndustriagps.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entIndIndustriagps.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entIndIndustriagps.Fields valueField, String textField, entIndIndustriagps.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entIndIndustriagps.Fields valueField, String textField, entIndIndustriagps.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entIndIndustriagps.Fields valueField, entIndIndustriagps.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entIndIndustriagps.Fields valueField, entIndIndustriagps.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entIndIndustriagps.Fields valueField, entIndIndustriagps.Fields textField, entIndIndustriagps.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entIndIndustriagps.Fields valueField, entIndIndustriagps.Fields textField, entIndIndustriagps.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, entIndIndustriagps.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, entIndIndustriagps.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entIndIndustriagps.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entIndIndustriagps.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		object FuncionesCount(entIndIndustriagps.Fields refField);
		object FuncionesCount(entIndIndustriagps.Fields refField, entIndIndustriagps.Fields whereField, object valueField);
		object FuncionesCount(entIndIndustriagps.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		object FuncionesMin(entIndIndustriagps.Fields refField);
		object FuncionesMin(entIndIndustriagps.Fields refField, entIndIndustriagps.Fields whereField, object valueField);
		object FuncionesMin(entIndIndustriagps.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		object FuncionesMax(entIndIndustriagps.Fields refField);
		object FuncionesMax(entIndIndustriagps.Fields refField, entIndIndustriagps.Fields whereField, object valueField);
		object FuncionesMax(entIndIndustriagps.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		object FuncionesSum(entIndIndustriagps.Fields refField);
		object FuncionesSum(entIndIndustriagps.Fields refField, entIndIndustriagps.Fields whereField, object valueField);
		object FuncionesSum(entIndIndustriagps.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		object FuncionesAvg(entIndIndustriagps.Fields refField);
		object FuncionesAvg(entIndIndustriagps.Fields refField, entIndIndustriagps.Fields whereField, object valueField);
		object FuncionesAvg(entIndIndustriagps.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

