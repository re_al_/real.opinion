#region 
/***********************************************************************************************************
	NOMBRE:       inEncInformante
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla enc_informante

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.Opinion.Dal;
using ReAl.Opinion.PgConn;
using ReAl.Opinion.Dal.Entidades;
using System.Windows.Forms;
#endregion

namespace ReAl.Opinion.Dal.Interface
{
	public interface inEncInformante: IDisposable
	{
		string getTableScript();
		dynamic GetColumnType(object valor,entEncInformante.Fields myField);
		entEncInformante ObtenerObjeto(Int64 Int64id_informante);
		entEncInformante ObtenerObjeto(Int64 Int64id_informante, ref cTrans localTrans);
		entEncInformante ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		entEncInformante ObtenerObjeto(Hashtable htbFiltro);
		entEncInformante ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		entEncInformante ObtenerObjeto(Hashtable htbFiltro, ref cTrans localTrans);
		entEncInformante ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		entEncInformante ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		entEncInformante ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		entEncInformante ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		entEncInformante ObtenerObjeto(entEncInformante.Fields searchField, object searchValue);
		entEncInformante ObtenerObjeto(entEncInformante.Fields searchField, object searchValue, ref cTrans localTrans);
		entEncInformante ObtenerObjeto(entEncInformante.Fields searchField, object searchValue, string strParamAdicionales);
		entEncInformante ObtenerObjeto(entEncInformante.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Dictionary<String, entEncInformante> ObtenerDiccionario();
		Dictionary<String, entEncInformante> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, entEncInformante> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Dictionary<String, entEncInformante> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, entEncInformante> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Dictionary<String, entEncInformante> ObtenerDiccionario(entEncInformante.Fields searchField, object searchValue);
		Dictionary<String, entEncInformante> ObtenerDiccionario(entEncInformante.Fields searchField, object searchValue, ref cTrans localTrans);
		Dictionary<String, entEncInformante> ObtenerDiccionario(entEncInformante.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, entEncInformante> ObtenerDiccionario(entEncInformante.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entEncInformante> ObtenerLista();
		List<entEncInformante> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entEncInformante> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entEncInformante> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entEncInformante> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entEncInformante> ObtenerLista(entEncInformante.Fields searchField, object searchValue);
		List<entEncInformante> ObtenerLista(entEncInformante.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entEncInformante> ObtenerLista(entEncInformante.Fields searchField, object searchValue, string strParamAdicionales);
		List<entEncInformante> ObtenerLista(entEncInformante.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entEncInformante> ObtenerLista(Hashtable htbFiltro);
		List<entEncInformante> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<entEncInformante> ObtenerLista(Hashtable htbFiltro, ref cTrans localTrans);
		List<entEncInformante> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		
		List<entEncInformante> ObtenerListaDesdeVista(String strVista);
		List<entEncInformante> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entEncInformante> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entEncInformante> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entEncInformante> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entEncInformante> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<entEncInformante> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref cTrans localTrans);
		List<entEncInformante> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<entEncInformante> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		List<entEncInformante> ObtenerListaDesdeVista(String strVista, entEncInformante.Fields searchField, object searchValue);
		List<entEncInformante> ObtenerListaDesdeVista(String strVista, entEncInformante.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entEncInformante> ObtenerListaDesdeVista(String strVista, entEncInformante.Fields searchField, object searchValue, string strParamAdicionales);
		List<entEncInformante> ObtenerListaDesdeVista(String strVista, entEncInformante.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Queue<entEncInformante> ObtenerCola();
		Queue<entEncInformante> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<entEncInformante> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Queue<entEncInformante> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<entEncInformante> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Queue<entEncInformante> ObtenerCola(entEncInformante.Fields searchField, object searchValue);
		Queue<entEncInformante> ObtenerCola(entEncInformante.Fields searchField, object searchValue, ref cTrans localTrans);
		Queue<entEncInformante> ObtenerCola(entEncInformante.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<entEncInformante> ObtenerCola(entEncInformante.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Stack<entEncInformante> ObtenerPila();
		Stack<entEncInformante> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<entEncInformante> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Stack<entEncInformante> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<entEncInformante> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Stack<entEncInformante> ObtenerPila(entEncInformante.Fields searchField, object searchValue);
		Stack<entEncInformante> ObtenerPila(entEncInformante.Fields searchField, object searchValue, ref cTrans localTrans);
		Stack<entEncInformante> ObtenerPila(entEncInformante.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<entEncInformante> ObtenerPila(entEncInformante.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		string CreatePK(string[] args);
		
		bool Insert(entEncInformante obj);
		bool Insert(entEncInformante obj, ref cTrans localTrans);
		int Update(entEncInformante obj);
		int Update(entEncInformante obj, ref cTrans localTrans);
		int Delete(entEncInformante obj);
		int Delete(entEncInformante obj, ref cTrans localTrans);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(entEncInformante.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(entEncInformante.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entEncInformante.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entEncInformante.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entEncInformante.Fields valueField, entEncInformante.Fields textField);
		void CargarComboBox(ref ComboBox cmb, entEncInformante.Fields valueField, entEncInformante.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entEncInformante.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, entEncInformante.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entEncInformante.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entEncInformante.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entEncInformante.Fields valueField, String textField, entEncInformante.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entEncInformante.Fields valueField, String textField, entEncInformante.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entEncInformante.Fields valueField, entEncInformante.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entEncInformante.Fields valueField, entEncInformante.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entEncInformante.Fields valueField, entEncInformante.Fields textField, entEncInformante.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entEncInformante.Fields valueField, entEncInformante.Fields textField, entEncInformante.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, entEncInformante.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, entEncInformante.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entEncInformante.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entEncInformante.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(entEncInformante.Fields refField);
		int FuncionesCount(entEncInformante.Fields refField, entEncInformante.Fields whereField, object valueField);
		int FuncionesCount(entEncInformante.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(entEncInformante.Fields refField);
		int FuncionesMin(entEncInformante.Fields refField, entEncInformante.Fields whereField, object valueField);
		int FuncionesMin(entEncInformante.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(entEncInformante.Fields refField);
		int FuncionesMax(entEncInformante.Fields refField, entEncInformante.Fields whereField, object valueField);
		int FuncionesMax(entEncInformante.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(entEncInformante.Fields refField);
		int FuncionesSum(entEncInformante.Fields refField, entEncInformante.Fields whereField, object valueField);
		int FuncionesSum(entEncInformante.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(entEncInformante.Fields refField);
		int FuncionesAvg(entEncInformante.Fields refField, entEncInformante.Fields whereField, object valueField);
		int FuncionesAvg(entEncInformante.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

