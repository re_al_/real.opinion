#region 
/***********************************************************************************************************
	NOMBRE:       inCatUpm
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla cat_upm

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.Opinion.Dal;
using ReAl.Opinion.PgConn;
using ReAl.Opinion.Dal.Entidades;
using System.Windows.Forms;
#endregion

namespace ReAl.Opinion.Dal.Interface
{
	public interface inCatUpm: IDisposable
	{
		string getTableScript();
		dynamic GetColumnType(object valor,entCatUpm.Fields myField);
		entCatUpm ObtenerObjeto(int intid_upm);
		entCatUpm ObtenerObjeto(int intid_upm, ref cTrans localTrans);
		entCatUpm ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		entCatUpm ObtenerObjeto(Hashtable htbFiltro);
		entCatUpm ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		entCatUpm ObtenerObjeto(Hashtable htbFiltro, ref cTrans localTrans);
		entCatUpm ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		entCatUpm ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		entCatUpm ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		entCatUpm ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		entCatUpm ObtenerObjeto(entCatUpm.Fields searchField, object searchValue);
		entCatUpm ObtenerObjeto(entCatUpm.Fields searchField, object searchValue, ref cTrans localTrans);
		entCatUpm ObtenerObjeto(entCatUpm.Fields searchField, object searchValue, string strParamAdicionales);
		entCatUpm ObtenerObjeto(entCatUpm.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Dictionary<String, entCatUpm> ObtenerDiccionario();
		Dictionary<String, entCatUpm> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, entCatUpm> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Dictionary<String, entCatUpm> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, entCatUpm> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Dictionary<String, entCatUpm> ObtenerDiccionario(entCatUpm.Fields searchField, object searchValue);
		Dictionary<String, entCatUpm> ObtenerDiccionario(entCatUpm.Fields searchField, object searchValue, ref cTrans localTrans);
		Dictionary<String, entCatUpm> ObtenerDiccionario(entCatUpm.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, entCatUpm> ObtenerDiccionario(entCatUpm.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entCatUpm> ObtenerLista();
		List<entCatUpm> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entCatUpm> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entCatUpm> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entCatUpm> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entCatUpm> ObtenerLista(entCatUpm.Fields searchField, object searchValue);
		List<entCatUpm> ObtenerLista(entCatUpm.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entCatUpm> ObtenerLista(entCatUpm.Fields searchField, object searchValue, string strParamAdicionales);
		List<entCatUpm> ObtenerLista(entCatUpm.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entCatUpm> ObtenerLista(Hashtable htbFiltro);
		List<entCatUpm> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<entCatUpm> ObtenerLista(Hashtable htbFiltro, ref cTrans localTrans);
		List<entCatUpm> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		
		List<entCatUpm> ObtenerListaDesdeVista(String strVista);
		List<entCatUpm> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entCatUpm> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entCatUpm> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entCatUpm> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entCatUpm> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<entCatUpm> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref cTrans localTrans);
		List<entCatUpm> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<entCatUpm> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		List<entCatUpm> ObtenerListaDesdeVista(String strVista, entCatUpm.Fields searchField, object searchValue);
		List<entCatUpm> ObtenerListaDesdeVista(String strVista, entCatUpm.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entCatUpm> ObtenerListaDesdeVista(String strVista, entCatUpm.Fields searchField, object searchValue, string strParamAdicionales);
		List<entCatUpm> ObtenerListaDesdeVista(String strVista, entCatUpm.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Queue<entCatUpm> ObtenerCola();
		Queue<entCatUpm> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<entCatUpm> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Queue<entCatUpm> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<entCatUpm> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Queue<entCatUpm> ObtenerCola(entCatUpm.Fields searchField, object searchValue);
		Queue<entCatUpm> ObtenerCola(entCatUpm.Fields searchField, object searchValue, ref cTrans localTrans);
		Queue<entCatUpm> ObtenerCola(entCatUpm.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<entCatUpm> ObtenerCola(entCatUpm.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Stack<entCatUpm> ObtenerPila();
		Stack<entCatUpm> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<entCatUpm> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Stack<entCatUpm> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<entCatUpm> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Stack<entCatUpm> ObtenerPila(entCatUpm.Fields searchField, object searchValue);
		Stack<entCatUpm> ObtenerPila(entCatUpm.Fields searchField, object searchValue, ref cTrans localTrans);
		Stack<entCatUpm> ObtenerPila(entCatUpm.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<entCatUpm> ObtenerPila(entCatUpm.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		string CreatePK(string[] args);
		
		bool Insert(entCatUpm obj);
		bool Insert(entCatUpm obj, ref cTrans localTrans);
		int Update(entCatUpm obj);
		int Update(entCatUpm obj, ref cTrans localTrans);
		int Delete(entCatUpm obj);
		int Delete(entCatUpm obj, ref cTrans localTrans);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(entCatUpm.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(entCatUpm.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entCatUpm.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entCatUpm.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entCatUpm.Fields valueField, entCatUpm.Fields textField);
		void CargarComboBox(ref ComboBox cmb, entCatUpm.Fields valueField, entCatUpm.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entCatUpm.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, entCatUpm.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entCatUpm.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entCatUpm.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entCatUpm.Fields valueField, String textField, entCatUpm.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entCatUpm.Fields valueField, String textField, entCatUpm.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entCatUpm.Fields valueField, entCatUpm.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entCatUpm.Fields valueField, entCatUpm.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entCatUpm.Fields valueField, entCatUpm.Fields textField, entCatUpm.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entCatUpm.Fields valueField, entCatUpm.Fields textField, entCatUpm.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, entCatUpm.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, entCatUpm.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entCatUpm.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entCatUpm.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(entCatUpm.Fields refField);
		int FuncionesCount(entCatUpm.Fields refField, entCatUpm.Fields whereField, object valueField);
		int FuncionesCount(entCatUpm.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(entCatUpm.Fields refField);
		int FuncionesMin(entCatUpm.Fields refField, entCatUpm.Fields whereField, object valueField);
		int FuncionesMin(entCatUpm.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(entCatUpm.Fields refField);
		int FuncionesMax(entCatUpm.Fields refField, entCatUpm.Fields whereField, object valueField);
		int FuncionesMax(entCatUpm.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(entCatUpm.Fields refField);
		int FuncionesSum(entCatUpm.Fields refField, entCatUpm.Fields whereField, object valueField);
		int FuncionesSum(entCatUpm.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(entCatUpm.Fields refField);
		int FuncionesAvg(entCatUpm.Fields refField, entCatUpm.Fields whereField, object valueField);
		int FuncionesAvg(entCatUpm.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

