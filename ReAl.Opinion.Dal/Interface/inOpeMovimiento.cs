#region 
/***********************************************************************************************************
	NOMBRE:       inOpeMovimiento
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla ope_movimiento

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        22/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.Opinion.Dal;
using ReAl.Opinion.PgConn;
using ReAl.Opinion.Dal.Entidades;
using System.Windows.Forms;
#endregion

namespace ReAl.Opinion.Dal.Interface
{
	public interface inOpeMovimiento: IDisposable
	{
		string getTableScript();
		dynamic GetColumnType(object valor,entOpeMovimiento.Fields myField);
		entOpeMovimiento ObtenerObjeto(int intid_movimiento);
		entOpeMovimiento ObtenerObjeto(int intid_movimiento, ref cTrans localTrans);
		entOpeMovimiento ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		entOpeMovimiento ObtenerObjeto(Hashtable htbFiltro);
		entOpeMovimiento ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		entOpeMovimiento ObtenerObjeto(Hashtable htbFiltro, ref cTrans localTrans);
		entOpeMovimiento ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		entOpeMovimiento ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		entOpeMovimiento ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		entOpeMovimiento ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		entOpeMovimiento ObtenerObjeto(entOpeMovimiento.Fields searchField, object searchValue);
		entOpeMovimiento ObtenerObjeto(entOpeMovimiento.Fields searchField, object searchValue, ref cTrans localTrans);
		entOpeMovimiento ObtenerObjeto(entOpeMovimiento.Fields searchField, object searchValue, string strParamAdicionales);
		entOpeMovimiento ObtenerObjeto(entOpeMovimiento.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Dictionary<String, entOpeMovimiento> ObtenerDiccionario();
		Dictionary<String, entOpeMovimiento> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, entOpeMovimiento> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Dictionary<String, entOpeMovimiento> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, entOpeMovimiento> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Dictionary<String, entOpeMovimiento> ObtenerDiccionario(entOpeMovimiento.Fields searchField, object searchValue);
		Dictionary<String, entOpeMovimiento> ObtenerDiccionario(entOpeMovimiento.Fields searchField, object searchValue, ref cTrans localTrans);
		Dictionary<String, entOpeMovimiento> ObtenerDiccionario(entOpeMovimiento.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, entOpeMovimiento> ObtenerDiccionario(entOpeMovimiento.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entOpeMovimiento> ObtenerLista();
		List<entOpeMovimiento> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entOpeMovimiento> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entOpeMovimiento> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entOpeMovimiento> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entOpeMovimiento> ObtenerLista(entOpeMovimiento.Fields searchField, object searchValue);
		List<entOpeMovimiento> ObtenerLista(entOpeMovimiento.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entOpeMovimiento> ObtenerLista(entOpeMovimiento.Fields searchField, object searchValue, string strParamAdicionales);
		List<entOpeMovimiento> ObtenerLista(entOpeMovimiento.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entOpeMovimiento> ObtenerLista(Hashtable htbFiltro);
		List<entOpeMovimiento> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<entOpeMovimiento> ObtenerLista(Hashtable htbFiltro, ref cTrans localTrans);
		List<entOpeMovimiento> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		
		List<entOpeMovimiento> ObtenerListaDesdeVista(String strVista);
		List<entOpeMovimiento> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entOpeMovimiento> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entOpeMovimiento> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entOpeMovimiento> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entOpeMovimiento> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<entOpeMovimiento> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref cTrans localTrans);
		List<entOpeMovimiento> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<entOpeMovimiento> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		List<entOpeMovimiento> ObtenerListaDesdeVista(String strVista, entOpeMovimiento.Fields searchField, object searchValue);
		List<entOpeMovimiento> ObtenerListaDesdeVista(String strVista, entOpeMovimiento.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entOpeMovimiento> ObtenerListaDesdeVista(String strVista, entOpeMovimiento.Fields searchField, object searchValue, string strParamAdicionales);
		List<entOpeMovimiento> ObtenerListaDesdeVista(String strVista, entOpeMovimiento.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Queue<entOpeMovimiento> ObtenerCola();
		Queue<entOpeMovimiento> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<entOpeMovimiento> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Queue<entOpeMovimiento> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<entOpeMovimiento> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Queue<entOpeMovimiento> ObtenerCola(entOpeMovimiento.Fields searchField, object searchValue);
		Queue<entOpeMovimiento> ObtenerCola(entOpeMovimiento.Fields searchField, object searchValue, ref cTrans localTrans);
		Queue<entOpeMovimiento> ObtenerCola(entOpeMovimiento.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<entOpeMovimiento> ObtenerCola(entOpeMovimiento.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Stack<entOpeMovimiento> ObtenerPila();
		Stack<entOpeMovimiento> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<entOpeMovimiento> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Stack<entOpeMovimiento> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<entOpeMovimiento> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Stack<entOpeMovimiento> ObtenerPila(entOpeMovimiento.Fields searchField, object searchValue);
		Stack<entOpeMovimiento> ObtenerPila(entOpeMovimiento.Fields searchField, object searchValue, ref cTrans localTrans);
		Stack<entOpeMovimiento> ObtenerPila(entOpeMovimiento.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<entOpeMovimiento> ObtenerPila(entOpeMovimiento.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		string CreatePK(string[] args);
		
		bool Insert(entOpeMovimiento obj);
		bool Insert(entOpeMovimiento obj, ref cTrans localTrans);
		int Update(entOpeMovimiento obj);
		int Update(entOpeMovimiento obj, ref cTrans localTrans);
		int Delete(entOpeMovimiento obj);
		int Delete(entOpeMovimiento obj, ref cTrans localTrans);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(entOpeMovimiento.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(entOpeMovimiento.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entOpeMovimiento.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entOpeMovimiento.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entOpeMovimiento.Fields valueField, entOpeMovimiento.Fields textField);
		void CargarComboBox(ref ComboBox cmb, entOpeMovimiento.Fields valueField, entOpeMovimiento.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entOpeMovimiento.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, entOpeMovimiento.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entOpeMovimiento.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entOpeMovimiento.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entOpeMovimiento.Fields valueField, String textField, entOpeMovimiento.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entOpeMovimiento.Fields valueField, String textField, entOpeMovimiento.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entOpeMovimiento.Fields valueField, entOpeMovimiento.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entOpeMovimiento.Fields valueField, entOpeMovimiento.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entOpeMovimiento.Fields valueField, entOpeMovimiento.Fields textField, entOpeMovimiento.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entOpeMovimiento.Fields valueField, entOpeMovimiento.Fields textField, entOpeMovimiento.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, entOpeMovimiento.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, entOpeMovimiento.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entOpeMovimiento.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entOpeMovimiento.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(entOpeMovimiento.Fields refField);
		int FuncionesCount(entOpeMovimiento.Fields refField, entOpeMovimiento.Fields whereField, object valueField);
		int FuncionesCount(entOpeMovimiento.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(entOpeMovimiento.Fields refField);
		int FuncionesMin(entOpeMovimiento.Fields refField, entOpeMovimiento.Fields whereField, object valueField);
		int FuncionesMin(entOpeMovimiento.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(entOpeMovimiento.Fields refField);
		int FuncionesMax(entOpeMovimiento.Fields refField, entOpeMovimiento.Fields whereField, object valueField);
		int FuncionesMax(entOpeMovimiento.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(entOpeMovimiento.Fields refField);
		int FuncionesSum(entOpeMovimiento.Fields refField, entOpeMovimiento.Fields whereField, object valueField);
		int FuncionesSum(entOpeMovimiento.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(entOpeMovimiento.Fields refField);
		int FuncionesAvg(entOpeMovimiento.Fields refField, entOpeMovimiento.Fields whereField, object valueField);
		int FuncionesAvg(entOpeMovimiento.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

