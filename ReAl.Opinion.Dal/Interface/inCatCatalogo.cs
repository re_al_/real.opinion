#region 
/***********************************************************************************************************
	NOMBRE:       inCatCatalogo
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla cat_catalogo

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        05/10/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.Opinion.Dal;
using ReAl.Opinion.PgConn;
using ReAl.Opinion.Dal.Entidades;
using System.Windows.Forms;
#endregion

namespace ReAl.Opinion.Dal.Interface
{
	public interface inCatCatalogo: IDisposable
	{
		string getTableScript();
		dynamic GetColumnType(object valor,entCatCatalogo.Fields myField);
		entCatCatalogo ObtenerObjeto(int intid_catalogo);
		entCatCatalogo ObtenerObjeto(int intid_catalogo, ref cTrans localTrans);
		entCatCatalogo ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		entCatCatalogo ObtenerObjeto(Hashtable htbFiltro);
		entCatCatalogo ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		entCatCatalogo ObtenerObjeto(Hashtable htbFiltro, ref cTrans localTrans);
		entCatCatalogo ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		entCatCatalogo ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		entCatCatalogo ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		entCatCatalogo ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		entCatCatalogo ObtenerObjeto(entCatCatalogo.Fields searchField, object searchValue);
		entCatCatalogo ObtenerObjeto(entCatCatalogo.Fields searchField, object searchValue, ref cTrans localTrans);
		entCatCatalogo ObtenerObjeto(entCatCatalogo.Fields searchField, object searchValue, string strParamAdicionales);
		entCatCatalogo ObtenerObjeto(entCatCatalogo.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Dictionary<String, entCatCatalogo> ObtenerDiccionario();
		Dictionary<String, entCatCatalogo> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, entCatCatalogo> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Dictionary<String, entCatCatalogo> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, entCatCatalogo> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Dictionary<String, entCatCatalogo> ObtenerDiccionario(entCatCatalogo.Fields searchField, object searchValue);
		Dictionary<String, entCatCatalogo> ObtenerDiccionario(entCatCatalogo.Fields searchField, object searchValue, ref cTrans localTrans);
		Dictionary<String, entCatCatalogo> ObtenerDiccionario(entCatCatalogo.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, entCatCatalogo> ObtenerDiccionario(entCatCatalogo.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entCatCatalogo> ObtenerLista();
		List<entCatCatalogo> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entCatCatalogo> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entCatCatalogo> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entCatCatalogo> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entCatCatalogo> ObtenerLista(entCatCatalogo.Fields searchField, object searchValue);
		List<entCatCatalogo> ObtenerLista(entCatCatalogo.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entCatCatalogo> ObtenerLista(entCatCatalogo.Fields searchField, object searchValue, string strParamAdicionales);
		List<entCatCatalogo> ObtenerLista(entCatCatalogo.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entCatCatalogo> ObtenerLista(Hashtable htbFiltro);
		List<entCatCatalogo> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<entCatCatalogo> ObtenerLista(Hashtable htbFiltro, ref cTrans localTrans);
		List<entCatCatalogo> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		
		List<entCatCatalogo> ObtenerListaDesdeVista(String strVista);
		List<entCatCatalogo> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entCatCatalogo> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entCatCatalogo> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entCatCatalogo> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entCatCatalogo> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<entCatCatalogo> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref cTrans localTrans);
		List<entCatCatalogo> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<entCatCatalogo> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		List<entCatCatalogo> ObtenerListaDesdeVista(String strVista, entCatCatalogo.Fields searchField, object searchValue);
		List<entCatCatalogo> ObtenerListaDesdeVista(String strVista, entCatCatalogo.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entCatCatalogo> ObtenerListaDesdeVista(String strVista, entCatCatalogo.Fields searchField, object searchValue, string strParamAdicionales);
		List<entCatCatalogo> ObtenerListaDesdeVista(String strVista, entCatCatalogo.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Queue<entCatCatalogo> ObtenerCola();
		Queue<entCatCatalogo> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<entCatCatalogo> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Queue<entCatCatalogo> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<entCatCatalogo> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Queue<entCatCatalogo> ObtenerCola(entCatCatalogo.Fields searchField, object searchValue);
		Queue<entCatCatalogo> ObtenerCola(entCatCatalogo.Fields searchField, object searchValue, ref cTrans localTrans);
		Queue<entCatCatalogo> ObtenerCola(entCatCatalogo.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<entCatCatalogo> ObtenerCola(entCatCatalogo.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Stack<entCatCatalogo> ObtenerPila();
		Stack<entCatCatalogo> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<entCatCatalogo> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Stack<entCatCatalogo> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<entCatCatalogo> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Stack<entCatCatalogo> ObtenerPila(entCatCatalogo.Fields searchField, object searchValue);
		Stack<entCatCatalogo> ObtenerPila(entCatCatalogo.Fields searchField, object searchValue, ref cTrans localTrans);
		Stack<entCatCatalogo> ObtenerPila(entCatCatalogo.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<entCatCatalogo> ObtenerPila(entCatCatalogo.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		string CreatePK(string[] args);
		
		bool Insert(entCatCatalogo obj);
		bool Insert(entCatCatalogo obj, ref cTrans localTrans);
		int Update(entCatCatalogo obj);
		int Update(entCatCatalogo obj, ref cTrans localTrans);
		int Delete(entCatCatalogo obj);
		int Delete(entCatCatalogo obj, ref cTrans localTrans);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(entCatCatalogo.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(entCatCatalogo.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entCatCatalogo.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entCatCatalogo.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entCatCatalogo.Fields valueField, entCatCatalogo.Fields textField);
		void CargarComboBox(ref ComboBox cmb, entCatCatalogo.Fields valueField, entCatCatalogo.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entCatCatalogo.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, entCatCatalogo.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entCatCatalogo.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entCatCatalogo.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entCatCatalogo.Fields valueField, String textField, entCatCatalogo.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entCatCatalogo.Fields valueField, String textField, entCatCatalogo.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entCatCatalogo.Fields valueField, entCatCatalogo.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entCatCatalogo.Fields valueField, entCatCatalogo.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entCatCatalogo.Fields valueField, entCatCatalogo.Fields textField, entCatCatalogo.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entCatCatalogo.Fields valueField, entCatCatalogo.Fields textField, entCatCatalogo.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, entCatCatalogo.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, entCatCatalogo.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entCatCatalogo.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entCatCatalogo.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(entCatCatalogo.Fields refField);
		int FuncionesCount(entCatCatalogo.Fields refField, entCatCatalogo.Fields whereField, object valueField);
		int FuncionesCount(entCatCatalogo.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(entCatCatalogo.Fields refField);
		int FuncionesMin(entCatCatalogo.Fields refField, entCatCatalogo.Fields whereField, object valueField);
		int FuncionesMin(entCatCatalogo.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(entCatCatalogo.Fields refField);
		int FuncionesMax(entCatCatalogo.Fields refField, entCatCatalogo.Fields whereField, object valueField);
		int FuncionesMax(entCatCatalogo.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(entCatCatalogo.Fields refField);
		int FuncionesSum(entCatCatalogo.Fields refField, entCatCatalogo.Fields whereField, object valueField);
		int FuncionesSum(entCatCatalogo.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(entCatCatalogo.Fields refField);
		int FuncionesAvg(entCatCatalogo.Fields refField, entCatCatalogo.Fields whereField, object valueField);
		int FuncionesAvg(entCatCatalogo.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

