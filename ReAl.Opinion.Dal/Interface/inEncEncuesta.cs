#region 
/***********************************************************************************************************
	NOMBRE:       inEncEncuesta
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla enc_encuesta

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.Opinion.Dal;
using ReAl.Opinion.PgConn;
using ReAl.Opinion.Dal.Entidades;
using System.Windows.Forms;
#endregion

namespace ReAl.Opinion.Dal.Interface
{
	public interface inEncEncuesta: IDisposable
	{
		string getTableScript();
		dynamic GetColumnType(object valor,entEncEncuesta.Fields myField);
		entEncEncuesta ObtenerObjeto(Int64 Int64id_encuesta);
		entEncEncuesta ObtenerObjeto(Int64 Int64id_encuesta, ref cTrans localTrans);
		entEncEncuesta ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		entEncEncuesta ObtenerObjeto(Hashtable htbFiltro);
		entEncEncuesta ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		entEncEncuesta ObtenerObjeto(Hashtable htbFiltro, ref cTrans localTrans);
		entEncEncuesta ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		entEncEncuesta ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		entEncEncuesta ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		entEncEncuesta ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		entEncEncuesta ObtenerObjeto(entEncEncuesta.Fields searchField, object searchValue);
		entEncEncuesta ObtenerObjeto(entEncEncuesta.Fields searchField, object searchValue, ref cTrans localTrans);
		entEncEncuesta ObtenerObjeto(entEncEncuesta.Fields searchField, object searchValue, string strParamAdicionales);
		entEncEncuesta ObtenerObjeto(entEncEncuesta.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Dictionary<String, entEncEncuesta> ObtenerDiccionario();
		Dictionary<String, entEncEncuesta> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, entEncEncuesta> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Dictionary<String, entEncEncuesta> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, entEncEncuesta> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Dictionary<String, entEncEncuesta> ObtenerDiccionario(entEncEncuesta.Fields searchField, object searchValue);
		Dictionary<String, entEncEncuesta> ObtenerDiccionario(entEncEncuesta.Fields searchField, object searchValue, ref cTrans localTrans);
		Dictionary<String, entEncEncuesta> ObtenerDiccionario(entEncEncuesta.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, entEncEncuesta> ObtenerDiccionario(entEncEncuesta.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entEncEncuesta> ObtenerLista();
		List<entEncEncuesta> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entEncEncuesta> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entEncEncuesta> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entEncEncuesta> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entEncEncuesta> ObtenerLista(entEncEncuesta.Fields searchField, object searchValue);
		List<entEncEncuesta> ObtenerLista(entEncEncuesta.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entEncEncuesta> ObtenerLista(entEncEncuesta.Fields searchField, object searchValue, string strParamAdicionales);
		List<entEncEncuesta> ObtenerLista(entEncEncuesta.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entEncEncuesta> ObtenerLista(Hashtable htbFiltro);
		List<entEncEncuesta> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<entEncEncuesta> ObtenerLista(Hashtable htbFiltro, ref cTrans localTrans);
		List<entEncEncuesta> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		
		List<entEncEncuesta> ObtenerListaDesdeVista(String strVista);
		List<entEncEncuesta> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entEncEncuesta> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entEncEncuesta> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entEncEncuesta> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entEncEncuesta> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<entEncEncuesta> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref cTrans localTrans);
		List<entEncEncuesta> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<entEncEncuesta> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		List<entEncEncuesta> ObtenerListaDesdeVista(String strVista, entEncEncuesta.Fields searchField, object searchValue);
		List<entEncEncuesta> ObtenerListaDesdeVista(String strVista, entEncEncuesta.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entEncEncuesta> ObtenerListaDesdeVista(String strVista, entEncEncuesta.Fields searchField, object searchValue, string strParamAdicionales);
		List<entEncEncuesta> ObtenerListaDesdeVista(String strVista, entEncEncuesta.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Queue<entEncEncuesta> ObtenerCola();
		Queue<entEncEncuesta> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<entEncEncuesta> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Queue<entEncEncuesta> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<entEncEncuesta> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Queue<entEncEncuesta> ObtenerCola(entEncEncuesta.Fields searchField, object searchValue);
		Queue<entEncEncuesta> ObtenerCola(entEncEncuesta.Fields searchField, object searchValue, ref cTrans localTrans);
		Queue<entEncEncuesta> ObtenerCola(entEncEncuesta.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<entEncEncuesta> ObtenerCola(entEncEncuesta.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Stack<entEncEncuesta> ObtenerPila();
		Stack<entEncEncuesta> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<entEncEncuesta> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Stack<entEncEncuesta> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<entEncEncuesta> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Stack<entEncEncuesta> ObtenerPila(entEncEncuesta.Fields searchField, object searchValue);
		Stack<entEncEncuesta> ObtenerPila(entEncEncuesta.Fields searchField, object searchValue, ref cTrans localTrans);
		Stack<entEncEncuesta> ObtenerPila(entEncEncuesta.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<entEncEncuesta> ObtenerPila(entEncEncuesta.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		string CreatePK(string[] args);
		
		bool Insert(entEncEncuesta obj);
		bool Insert(entEncEncuesta obj, ref cTrans localTrans);
		int Update(entEncEncuesta obj);
		int Update(entEncEncuesta obj, ref cTrans localTrans);
		int Delete(entEncEncuesta obj);
		int Delete(entEncEncuesta obj, ref cTrans localTrans);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(entEncEncuesta.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(entEncEncuesta.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entEncEncuesta.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entEncEncuesta.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entEncEncuesta.Fields valueField, entEncEncuesta.Fields textField);
		void CargarComboBox(ref ComboBox cmb, entEncEncuesta.Fields valueField, entEncEncuesta.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entEncEncuesta.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, entEncEncuesta.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entEncEncuesta.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entEncEncuesta.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entEncEncuesta.Fields valueField, String textField, entEncEncuesta.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entEncEncuesta.Fields valueField, String textField, entEncEncuesta.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entEncEncuesta.Fields valueField, entEncEncuesta.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entEncEncuesta.Fields valueField, entEncEncuesta.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entEncEncuesta.Fields valueField, entEncEncuesta.Fields textField, entEncEncuesta.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entEncEncuesta.Fields valueField, entEncEncuesta.Fields textField, entEncEncuesta.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, entEncEncuesta.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, entEncEncuesta.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entEncEncuesta.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entEncEncuesta.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(entEncEncuesta.Fields refField);
		int FuncionesCount(entEncEncuesta.Fields refField, entEncEncuesta.Fields whereField, object valueField);
		int FuncionesCount(entEncEncuesta.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(entEncEncuesta.Fields refField);
		int FuncionesMin(entEncEncuesta.Fields refField, entEncEncuesta.Fields whereField, object valueField);
		int FuncionesMin(entEncEncuesta.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(entEncEncuesta.Fields refField);
		int FuncionesMax(entEncEncuesta.Fields refField, entEncEncuesta.Fields whereField, object valueField);
		int FuncionesMax(entEncEncuesta.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(entEncEncuesta.Fields refField);
		int FuncionesSum(entEncEncuesta.Fields refField, entEncEncuesta.Fields whereField, object valueField);
		int FuncionesSum(entEncEncuesta.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(entEncEncuesta.Fields refField);
		int FuncionesAvg(entEncEncuesta.Fields refField, entEncEncuesta.Fields whereField, object valueField);
		int FuncionesAvg(entEncEncuesta.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

