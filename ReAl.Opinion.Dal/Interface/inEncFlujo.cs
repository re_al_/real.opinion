#region 
/***********************************************************************************************************
	NOMBRE:       inEncFlujo
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla enc_flujo

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.Opinion.Dal;
using ReAl.Opinion.PgConn;
using ReAl.Opinion.Dal.Entidades;
using System.Windows.Forms;
#endregion

namespace ReAl.Opinion.Dal.Interface
{
	public interface inEncFlujo: IDisposable
	{
		string getTableScript();
		dynamic GetColumnType(object valor,entEncFlujo.Fields myField);
		entEncFlujo ObtenerObjeto(int intid_flujo);
		entEncFlujo ObtenerObjeto(int intid_flujo, ref cTrans localTrans);
		entEncFlujo ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		entEncFlujo ObtenerObjeto(Hashtable htbFiltro);
		entEncFlujo ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		entEncFlujo ObtenerObjeto(Hashtable htbFiltro, ref cTrans localTrans);
		entEncFlujo ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		entEncFlujo ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		entEncFlujo ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		entEncFlujo ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		entEncFlujo ObtenerObjeto(entEncFlujo.Fields searchField, object searchValue);
		entEncFlujo ObtenerObjeto(entEncFlujo.Fields searchField, object searchValue, ref cTrans localTrans);
		entEncFlujo ObtenerObjeto(entEncFlujo.Fields searchField, object searchValue, string strParamAdicionales);
		entEncFlujo ObtenerObjeto(entEncFlujo.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Dictionary<String, entEncFlujo> ObtenerDiccionario();
		Dictionary<String, entEncFlujo> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, entEncFlujo> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Dictionary<String, entEncFlujo> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, entEncFlujo> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Dictionary<String, entEncFlujo> ObtenerDiccionario(entEncFlujo.Fields searchField, object searchValue);
		Dictionary<String, entEncFlujo> ObtenerDiccionario(entEncFlujo.Fields searchField, object searchValue, ref cTrans localTrans);
		Dictionary<String, entEncFlujo> ObtenerDiccionario(entEncFlujo.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, entEncFlujo> ObtenerDiccionario(entEncFlujo.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entEncFlujo> ObtenerLista();
		List<entEncFlujo> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entEncFlujo> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entEncFlujo> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entEncFlujo> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entEncFlujo> ObtenerLista(entEncFlujo.Fields searchField, object searchValue);
		List<entEncFlujo> ObtenerLista(entEncFlujo.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entEncFlujo> ObtenerLista(entEncFlujo.Fields searchField, object searchValue, string strParamAdicionales);
		List<entEncFlujo> ObtenerLista(entEncFlujo.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entEncFlujo> ObtenerLista(Hashtable htbFiltro);
		List<entEncFlujo> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<entEncFlujo> ObtenerLista(Hashtable htbFiltro, ref cTrans localTrans);
		List<entEncFlujo> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		
		List<entEncFlujo> ObtenerListaDesdeVista(String strVista);
		List<entEncFlujo> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entEncFlujo> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entEncFlujo> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entEncFlujo> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entEncFlujo> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<entEncFlujo> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref cTrans localTrans);
		List<entEncFlujo> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<entEncFlujo> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		List<entEncFlujo> ObtenerListaDesdeVista(String strVista, entEncFlujo.Fields searchField, object searchValue);
		List<entEncFlujo> ObtenerListaDesdeVista(String strVista, entEncFlujo.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entEncFlujo> ObtenerListaDesdeVista(String strVista, entEncFlujo.Fields searchField, object searchValue, string strParamAdicionales);
		List<entEncFlujo> ObtenerListaDesdeVista(String strVista, entEncFlujo.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Queue<entEncFlujo> ObtenerCola();
		Queue<entEncFlujo> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<entEncFlujo> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Queue<entEncFlujo> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<entEncFlujo> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Queue<entEncFlujo> ObtenerCola(entEncFlujo.Fields searchField, object searchValue);
		Queue<entEncFlujo> ObtenerCola(entEncFlujo.Fields searchField, object searchValue, ref cTrans localTrans);
		Queue<entEncFlujo> ObtenerCola(entEncFlujo.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<entEncFlujo> ObtenerCola(entEncFlujo.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Stack<entEncFlujo> ObtenerPila();
		Stack<entEncFlujo> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<entEncFlujo> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Stack<entEncFlujo> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<entEncFlujo> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Stack<entEncFlujo> ObtenerPila(entEncFlujo.Fields searchField, object searchValue);
		Stack<entEncFlujo> ObtenerPila(entEncFlujo.Fields searchField, object searchValue, ref cTrans localTrans);
		Stack<entEncFlujo> ObtenerPila(entEncFlujo.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<entEncFlujo> ObtenerPila(entEncFlujo.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		string CreatePK(string[] args);
		
		bool Insert(entEncFlujo obj);
		bool Insert(entEncFlujo obj, ref cTrans localTrans);
		int Update(entEncFlujo obj);
		int Update(entEncFlujo obj, ref cTrans localTrans);
		int Delete(entEncFlujo obj);
		int Delete(entEncFlujo obj, ref cTrans localTrans);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(entEncFlujo.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(entEncFlujo.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entEncFlujo.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entEncFlujo.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entEncFlujo.Fields valueField, entEncFlujo.Fields textField);
		void CargarComboBox(ref ComboBox cmb, entEncFlujo.Fields valueField, entEncFlujo.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entEncFlujo.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, entEncFlujo.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entEncFlujo.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entEncFlujo.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entEncFlujo.Fields valueField, String textField, entEncFlujo.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entEncFlujo.Fields valueField, String textField, entEncFlujo.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entEncFlujo.Fields valueField, entEncFlujo.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entEncFlujo.Fields valueField, entEncFlujo.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entEncFlujo.Fields valueField, entEncFlujo.Fields textField, entEncFlujo.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entEncFlujo.Fields valueField, entEncFlujo.Fields textField, entEncFlujo.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, entEncFlujo.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, entEncFlujo.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entEncFlujo.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entEncFlujo.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(entEncFlujo.Fields refField);
		int FuncionesCount(entEncFlujo.Fields refField, entEncFlujo.Fields whereField, object valueField);
		int FuncionesCount(entEncFlujo.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(entEncFlujo.Fields refField);
		int FuncionesMin(entEncFlujo.Fields refField, entEncFlujo.Fields whereField, object valueField);
		int FuncionesMin(entEncFlujo.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(entEncFlujo.Fields refField);
		int FuncionesMax(entEncFlujo.Fields refField, entEncFlujo.Fields whereField, object valueField);
		int FuncionesMax(entEncFlujo.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(entEncFlujo.Fields refField);
		int FuncionesSum(entEncFlujo.Fields refField, entEncFlujo.Fields whereField, object valueField);
		int FuncionesSum(entEncFlujo.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(entEncFlujo.Fields refField);
		int FuncionesAvg(entEncFlujo.Fields refField, entEncFlujo.Fields whereField, object valueField);
		int FuncionesAvg(entEncFlujo.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

