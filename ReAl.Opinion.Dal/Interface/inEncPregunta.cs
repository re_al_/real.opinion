#region 
/***********************************************************************************************************
	NOMBRE:       inEncPregunta
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla enc_pregunta

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.Opinion.Dal;
using ReAl.Opinion.PgConn;
using ReAl.Opinion.Dal.Entidades;
using System.Windows.Forms;
#endregion

namespace ReAl.Opinion.Dal.Interface
{
	public interface inEncPregunta: IDisposable
	{
		string getTableScript();
		dynamic GetColumnType(object valor,entEncPregunta.Fields myField);
		entEncPregunta ObtenerObjeto(Int64 Int64id_pregunta);
		entEncPregunta ObtenerObjeto(Int64 Int64id_pregunta, ref cTrans localTrans);
		entEncPregunta ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		entEncPregunta ObtenerObjeto(Hashtable htbFiltro);
		entEncPregunta ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		entEncPregunta ObtenerObjeto(Hashtable htbFiltro, ref cTrans localTrans);
		entEncPregunta ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		entEncPregunta ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		entEncPregunta ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		entEncPregunta ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		entEncPregunta ObtenerObjeto(entEncPregunta.Fields searchField, object searchValue);
		entEncPregunta ObtenerObjeto(entEncPregunta.Fields searchField, object searchValue, ref cTrans localTrans);
		entEncPregunta ObtenerObjeto(entEncPregunta.Fields searchField, object searchValue, string strParamAdicionales);
		entEncPregunta ObtenerObjeto(entEncPregunta.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Dictionary<String, entEncPregunta> ObtenerDiccionario();
		Dictionary<String, entEncPregunta> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, entEncPregunta> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Dictionary<String, entEncPregunta> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, entEncPregunta> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Dictionary<String, entEncPregunta> ObtenerDiccionario(entEncPregunta.Fields searchField, object searchValue);
		Dictionary<String, entEncPregunta> ObtenerDiccionario(entEncPregunta.Fields searchField, object searchValue, ref cTrans localTrans);
		Dictionary<String, entEncPregunta> ObtenerDiccionario(entEncPregunta.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, entEncPregunta> ObtenerDiccionario(entEncPregunta.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entEncPregunta> ObtenerLista();
		List<entEncPregunta> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entEncPregunta> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entEncPregunta> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entEncPregunta> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entEncPregunta> ObtenerLista(entEncPregunta.Fields searchField, object searchValue);
		List<entEncPregunta> ObtenerLista(entEncPregunta.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entEncPregunta> ObtenerLista(entEncPregunta.Fields searchField, object searchValue, string strParamAdicionales);
		List<entEncPregunta> ObtenerLista(entEncPregunta.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entEncPregunta> ObtenerLista(Hashtable htbFiltro);
		List<entEncPregunta> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<entEncPregunta> ObtenerLista(Hashtable htbFiltro, ref cTrans localTrans);
		List<entEncPregunta> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		
		List<entEncPregunta> ObtenerListaDesdeVista(String strVista);
		List<entEncPregunta> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entEncPregunta> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entEncPregunta> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entEncPregunta> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entEncPregunta> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<entEncPregunta> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref cTrans localTrans);
		List<entEncPregunta> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<entEncPregunta> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		List<entEncPregunta> ObtenerListaDesdeVista(String strVista, entEncPregunta.Fields searchField, object searchValue);
		List<entEncPregunta> ObtenerListaDesdeVista(String strVista, entEncPregunta.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entEncPregunta> ObtenerListaDesdeVista(String strVista, entEncPregunta.Fields searchField, object searchValue, string strParamAdicionales);
		List<entEncPregunta> ObtenerListaDesdeVista(String strVista, entEncPregunta.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Queue<entEncPregunta> ObtenerCola();
		Queue<entEncPregunta> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<entEncPregunta> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Queue<entEncPregunta> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<entEncPregunta> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Queue<entEncPregunta> ObtenerCola(entEncPregunta.Fields searchField, object searchValue);
		Queue<entEncPregunta> ObtenerCola(entEncPregunta.Fields searchField, object searchValue, ref cTrans localTrans);
		Queue<entEncPregunta> ObtenerCola(entEncPregunta.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<entEncPregunta> ObtenerCola(entEncPregunta.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Stack<entEncPregunta> ObtenerPila();
		Stack<entEncPregunta> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<entEncPregunta> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Stack<entEncPregunta> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<entEncPregunta> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Stack<entEncPregunta> ObtenerPila(entEncPregunta.Fields searchField, object searchValue);
		Stack<entEncPregunta> ObtenerPila(entEncPregunta.Fields searchField, object searchValue, ref cTrans localTrans);
		Stack<entEncPregunta> ObtenerPila(entEncPregunta.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<entEncPregunta> ObtenerPila(entEncPregunta.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		string CreatePK(string[] args);
		
		bool Insert(entEncPregunta obj);
		bool Insert(entEncPregunta obj, ref cTrans localTrans);
		int Update(entEncPregunta obj);
		int Update(entEncPregunta obj, ref cTrans localTrans);
		int Delete(entEncPregunta obj);
		int Delete(entEncPregunta obj, ref cTrans localTrans);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(entEncPregunta.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(entEncPregunta.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entEncPregunta.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entEncPregunta.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entEncPregunta.Fields valueField, entEncPregunta.Fields textField);
		void CargarComboBox(ref ComboBox cmb, entEncPregunta.Fields valueField, entEncPregunta.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entEncPregunta.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, entEncPregunta.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entEncPregunta.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entEncPregunta.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entEncPregunta.Fields valueField, String textField, entEncPregunta.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entEncPregunta.Fields valueField, String textField, entEncPregunta.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entEncPregunta.Fields valueField, entEncPregunta.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entEncPregunta.Fields valueField, entEncPregunta.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entEncPregunta.Fields valueField, entEncPregunta.Fields textField, entEncPregunta.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entEncPregunta.Fields valueField, entEncPregunta.Fields textField, entEncPregunta.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, entEncPregunta.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, entEncPregunta.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entEncPregunta.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entEncPregunta.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(entEncPregunta.Fields refField);
		int FuncionesCount(entEncPregunta.Fields refField, entEncPregunta.Fields whereField, object valueField);
		int FuncionesCount(entEncPregunta.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(entEncPregunta.Fields refField);
		int FuncionesMin(entEncPregunta.Fields refField, entEncPregunta.Fields whereField, object valueField);
		int FuncionesMin(entEncPregunta.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(entEncPregunta.Fields refField);
		int FuncionesMax(entEncPregunta.Fields refField, entEncPregunta.Fields whereField, object valueField);
		int FuncionesMax(entEncPregunta.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(entEncPregunta.Fields refField);
		int FuncionesSum(entEncPregunta.Fields refField, entEncPregunta.Fields whereField, object valueField);
		int FuncionesSum(entEncPregunta.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(entEncPregunta.Fields refField);
		int FuncionesAvg(entEncPregunta.Fields refField, entEncPregunta.Fields whereField, object valueField);
		int FuncionesAvg(entEncPregunta.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

