#region 
/***********************************************************************************************************
	NOMBRE:       inSegProyecto
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla seg_proyecto

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        12/10/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.Opinion.Dal;
using ReAl.Opinion.PgConn;
using ReAl.Opinion.Dal.Entidades;
using System.Web.UI.WebControls;
#endregion

namespace ReAl.Opinion.Dal.Interface
{
	public interface inSegProyecto: IDisposable
	{
		string getTableScript();
		dynamic GetColumnType(object valor,entSegProyecto.Fields myField);
		entSegProyecto ObtenerObjeto(int intid_proyecto);
		entSegProyecto ObtenerObjeto(int intid_proyecto, ref cTrans localTrans);
		entSegProyecto ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		entSegProyecto ObtenerObjeto(Hashtable htbFiltro);
		entSegProyecto ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		entSegProyecto ObtenerObjeto(Hashtable htbFiltro, ref cTrans localTrans);
		entSegProyecto ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		entSegProyecto ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		entSegProyecto ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		entSegProyecto ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		entSegProyecto ObtenerObjeto(entSegProyecto.Fields searchField, object searchValue);
		entSegProyecto ObtenerObjeto(entSegProyecto.Fields searchField, object searchValue, ref cTrans localTrans);
		entSegProyecto ObtenerObjeto(entSegProyecto.Fields searchField, object searchValue, string strParamAdicionales);
		entSegProyecto ObtenerObjeto(entSegProyecto.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Dictionary<String, entSegProyecto> ObtenerDiccionario();
		Dictionary<String, entSegProyecto> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, entSegProyecto> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Dictionary<String, entSegProyecto> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, entSegProyecto> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Dictionary<String, entSegProyecto> ObtenerDiccionario(entSegProyecto.Fields searchField, object searchValue);
		Dictionary<String, entSegProyecto> ObtenerDiccionario(entSegProyecto.Fields searchField, object searchValue, ref cTrans localTrans);
		Dictionary<String, entSegProyecto> ObtenerDiccionario(entSegProyecto.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, entSegProyecto> ObtenerDiccionario(entSegProyecto.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entSegProyecto> ObtenerLista();
		List<entSegProyecto> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entSegProyecto> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entSegProyecto> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entSegProyecto> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entSegProyecto> ObtenerLista(entSegProyecto.Fields searchField, object searchValue);
		List<entSegProyecto> ObtenerLista(entSegProyecto.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entSegProyecto> ObtenerLista(entSegProyecto.Fields searchField, object searchValue, string strParamAdicionales);
		List<entSegProyecto> ObtenerLista(entSegProyecto.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entSegProyecto> ObtenerLista(Hashtable htbFiltro);
		List<entSegProyecto> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<entSegProyecto> ObtenerLista(Hashtable htbFiltro, ref cTrans localTrans);
		List<entSegProyecto> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		
		List<entSegProyecto> ObtenerListaDesdeVista(String strVista);
		List<entSegProyecto> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entSegProyecto> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entSegProyecto> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entSegProyecto> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entSegProyecto> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<entSegProyecto> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref cTrans localTrans);
		List<entSegProyecto> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<entSegProyecto> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		List<entSegProyecto> ObtenerListaDesdeVista(String strVista, entSegProyecto.Fields searchField, object searchValue);
		List<entSegProyecto> ObtenerListaDesdeVista(String strVista, entSegProyecto.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entSegProyecto> ObtenerListaDesdeVista(String strVista, entSegProyecto.Fields searchField, object searchValue, string strParamAdicionales);
		List<entSegProyecto> ObtenerListaDesdeVista(String strVista, entSegProyecto.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Queue<entSegProyecto> ObtenerCola();
		Queue<entSegProyecto> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<entSegProyecto> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Queue<entSegProyecto> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<entSegProyecto> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Queue<entSegProyecto> ObtenerCola(entSegProyecto.Fields searchField, object searchValue);
		Queue<entSegProyecto> ObtenerCola(entSegProyecto.Fields searchField, object searchValue, ref cTrans localTrans);
		Queue<entSegProyecto> ObtenerCola(entSegProyecto.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<entSegProyecto> ObtenerCola(entSegProyecto.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Stack<entSegProyecto> ObtenerPila();
		Stack<entSegProyecto> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<entSegProyecto> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Stack<entSegProyecto> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<entSegProyecto> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Stack<entSegProyecto> ObtenerPila(entSegProyecto.Fields searchField, object searchValue);
		Stack<entSegProyecto> ObtenerPila(entSegProyecto.Fields searchField, object searchValue, ref cTrans localTrans);
		Stack<entSegProyecto> ObtenerPila(entSegProyecto.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<entSegProyecto> ObtenerPila(entSegProyecto.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		string CreatePK(string[] args);
		
		bool Insert(entSegProyecto obj);
		bool Insert(entSegProyecto obj, ref cTrans localTrans);
		int Update(entSegProyecto obj);
		int Update(entSegProyecto obj, ref cTrans localTrans);
		int Delete(entSegProyecto obj);
		int Delete(entSegProyecto obj, ref cTrans localTrans);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(entSegProyecto.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(entSegProyecto.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entSegProyecto.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entSegProyecto.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarDropDownList(ref DropDownList cmb);
		void CargarDropDownList(ref DropDownList cmb, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, entSegProyecto.Fields valueField, entSegProyecto.Fields textField);
		void CargarDropDownList(ref DropDownList cmb, entSegProyecto.Fields valueField, entSegProyecto.Fields textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, entSegProyecto.Fields valueField, String textField);
		void CargarDropDownList(ref DropDownList cmb, entSegProyecto.Fields valueField, String textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, entSegProyecto.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, entSegProyecto.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, entSegProyecto.Fields valueField, String textField, entSegProyecto.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, entSegProyecto.Fields valueField, String textField, entSegProyecto.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, entSegProyecto.Fields valueField, entSegProyecto.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, entSegProyecto.Fields valueField, entSegProyecto.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, entSegProyecto.Fields valueField, entSegProyecto.Fields textField, entSegProyecto.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, entSegProyecto.Fields valueField, entSegProyecto.Fields textField, entSegProyecto.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarGridView(ref GridView dtg);
		void CargarGridView(ref GridView dtg, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, entSegProyecto.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, entSegProyecto.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, entSegProyecto.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, entSegProyecto.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(entSegProyecto.Fields refField);
		int FuncionesCount(entSegProyecto.Fields refField, entSegProyecto.Fields whereField, object valueField);
		int FuncionesCount(entSegProyecto.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(entSegProyecto.Fields refField);
		int FuncionesMin(entSegProyecto.Fields refField, entSegProyecto.Fields whereField, object valueField);
		int FuncionesMin(entSegProyecto.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(entSegProyecto.Fields refField);
		int FuncionesMax(entSegProyecto.Fields refField, entSegProyecto.Fields whereField, object valueField);
		int FuncionesMax(entSegProyecto.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(entSegProyecto.Fields refField);
		int FuncionesSum(entSegProyecto.Fields refField, entSegProyecto.Fields whereField, object valueField);
		int FuncionesSum(entSegProyecto.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(entSegProyecto.Fields refField);
		int FuncionesAvg(entSegProyecto.Fields refField, entSegProyecto.Fields whereField, object valueField);
		int FuncionesAvg(entSegProyecto.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

