#region 
/***********************************************************************************************************
	NOMBRE:       inSegUsuario
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla seg_usuario

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.Opinion.Dal;
using ReAl.Opinion.PgConn;
using ReAl.Opinion.Dal.Entidades;
using System.Windows.Forms;
#endregion

namespace ReAl.Opinion.Dal.Interface
{
	public interface inSegUsuario: IDisposable
	{
		string getTableScript();
		dynamic GetColumnType(object valor,entSegUsuario.Fields myField);
		entSegUsuario ObtenerObjeto(int intid_usuario);
		entSegUsuario ObtenerObjeto(int intid_usuario, ref cTrans localTrans);
		entSegUsuario ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		entSegUsuario ObtenerObjeto(Hashtable htbFiltro);
		entSegUsuario ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		entSegUsuario ObtenerObjeto(Hashtable htbFiltro, ref cTrans localTrans);
		entSegUsuario ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		entSegUsuario ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		entSegUsuario ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		entSegUsuario ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		entSegUsuario ObtenerObjeto(entSegUsuario.Fields searchField, object searchValue);
		entSegUsuario ObtenerObjeto(entSegUsuario.Fields searchField, object searchValue, ref cTrans localTrans);
		entSegUsuario ObtenerObjeto(entSegUsuario.Fields searchField, object searchValue, string strParamAdicionales);
		entSegUsuario ObtenerObjeto(entSegUsuario.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Dictionary<String, entSegUsuario> ObtenerDiccionario();
		Dictionary<String, entSegUsuario> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, entSegUsuario> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Dictionary<String, entSegUsuario> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, entSegUsuario> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Dictionary<String, entSegUsuario> ObtenerDiccionario(entSegUsuario.Fields searchField, object searchValue);
		Dictionary<String, entSegUsuario> ObtenerDiccionario(entSegUsuario.Fields searchField, object searchValue, ref cTrans localTrans);
		Dictionary<String, entSegUsuario> ObtenerDiccionario(entSegUsuario.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, entSegUsuario> ObtenerDiccionario(entSegUsuario.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entSegUsuario> ObtenerLista();
		List<entSegUsuario> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entSegUsuario> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entSegUsuario> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entSegUsuario> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entSegUsuario> ObtenerLista(entSegUsuario.Fields searchField, object searchValue);
		List<entSegUsuario> ObtenerLista(entSegUsuario.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entSegUsuario> ObtenerLista(entSegUsuario.Fields searchField, object searchValue, string strParamAdicionales);
		List<entSegUsuario> ObtenerLista(entSegUsuario.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entSegUsuario> ObtenerLista(Hashtable htbFiltro);
		List<entSegUsuario> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<entSegUsuario> ObtenerLista(Hashtable htbFiltro, ref cTrans localTrans);
		List<entSegUsuario> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		
		List<entSegUsuario> ObtenerListaDesdeVista(String strVista);
		List<entSegUsuario> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entSegUsuario> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entSegUsuario> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entSegUsuario> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entSegUsuario> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<entSegUsuario> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref cTrans localTrans);
		List<entSegUsuario> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<entSegUsuario> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		List<entSegUsuario> ObtenerListaDesdeVista(String strVista, entSegUsuario.Fields searchField, object searchValue);
		List<entSegUsuario> ObtenerListaDesdeVista(String strVista, entSegUsuario.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entSegUsuario> ObtenerListaDesdeVista(String strVista, entSegUsuario.Fields searchField, object searchValue, string strParamAdicionales);
		List<entSegUsuario> ObtenerListaDesdeVista(String strVista, entSegUsuario.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Queue<entSegUsuario> ObtenerCola();
		Queue<entSegUsuario> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<entSegUsuario> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Queue<entSegUsuario> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<entSegUsuario> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Queue<entSegUsuario> ObtenerCola(entSegUsuario.Fields searchField, object searchValue);
		Queue<entSegUsuario> ObtenerCola(entSegUsuario.Fields searchField, object searchValue, ref cTrans localTrans);
		Queue<entSegUsuario> ObtenerCola(entSegUsuario.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<entSegUsuario> ObtenerCola(entSegUsuario.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Stack<entSegUsuario> ObtenerPila();
		Stack<entSegUsuario> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<entSegUsuario> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Stack<entSegUsuario> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<entSegUsuario> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Stack<entSegUsuario> ObtenerPila(entSegUsuario.Fields searchField, object searchValue);
		Stack<entSegUsuario> ObtenerPila(entSegUsuario.Fields searchField, object searchValue, ref cTrans localTrans);
		Stack<entSegUsuario> ObtenerPila(entSegUsuario.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<entSegUsuario> ObtenerPila(entSegUsuario.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		string CreatePK(string[] args);
		
		bool Insert(entSegUsuario obj);
		bool Insert(entSegUsuario obj, ref cTrans localTrans);
		int Update(entSegUsuario obj);
		int Update(entSegUsuario obj, ref cTrans localTrans);
		int Delete(entSegUsuario obj);
		int Delete(entSegUsuario obj, ref cTrans localTrans);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(entSegUsuario.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(entSegUsuario.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entSegUsuario.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entSegUsuario.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entSegUsuario.Fields valueField, entSegUsuario.Fields textField);
		void CargarComboBox(ref ComboBox cmb, entSegUsuario.Fields valueField, entSegUsuario.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entSegUsuario.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, entSegUsuario.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entSegUsuario.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entSegUsuario.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entSegUsuario.Fields valueField, String textField, entSegUsuario.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entSegUsuario.Fields valueField, String textField, entSegUsuario.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entSegUsuario.Fields valueField, entSegUsuario.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entSegUsuario.Fields valueField, entSegUsuario.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entSegUsuario.Fields valueField, entSegUsuario.Fields textField, entSegUsuario.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entSegUsuario.Fields valueField, entSegUsuario.Fields textField, entSegUsuario.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, entSegUsuario.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, entSegUsuario.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entSegUsuario.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entSegUsuario.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(entSegUsuario.Fields refField);
		int FuncionesCount(entSegUsuario.Fields refField, entSegUsuario.Fields whereField, object valueField);
		int FuncionesCount(entSegUsuario.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(entSegUsuario.Fields refField);
		int FuncionesMin(entSegUsuario.Fields refField, entSegUsuario.Fields whereField, object valueField);
		int FuncionesMin(entSegUsuario.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(entSegUsuario.Fields refField);
		int FuncionesMax(entSegUsuario.Fields refField, entSegUsuario.Fields whereField, object valueField);
		int FuncionesMax(entSegUsuario.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(entSegUsuario.Fields refField);
		int FuncionesSum(entSegUsuario.Fields refField, entSegUsuario.Fields whereField, object valueField);
		int FuncionesSum(entSegUsuario.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(entSegUsuario.Fields refField);
		int FuncionesAvg(entSegUsuario.Fields refField, entSegUsuario.Fields whereField, object valueField);
		int FuncionesAvg(entSegUsuario.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

