#region 
/***********************************************************************************************************
	NOMBRE:       inSegPagina
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla seg_pagina

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        21/09/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.Opinion.Dal;
using ReAl.Opinion.PgConn;
using ReAl.Opinion.Dal.Entidades;
using System.Windows.Forms;
#endregion

namespace ReAl.Opinion.Dal.Interface
{
	public interface inSegPagina: IDisposable
	{
		string getTableScript();
		dynamic GetColumnType(object valor,entSegPagina.Fields myField);
		entSegPagina ObtenerObjeto(int intid_pagina);
		entSegPagina ObtenerObjeto(int intid_pagina, ref cTrans localTrans);
		entSegPagina ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		entSegPagina ObtenerObjeto(Hashtable htbFiltro);
		entSegPagina ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		entSegPagina ObtenerObjeto(Hashtable htbFiltro, ref cTrans localTrans);
		entSegPagina ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		entSegPagina ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		entSegPagina ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		entSegPagina ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		entSegPagina ObtenerObjeto(entSegPagina.Fields searchField, object searchValue);
		entSegPagina ObtenerObjeto(entSegPagina.Fields searchField, object searchValue, ref cTrans localTrans);
		entSegPagina ObtenerObjeto(entSegPagina.Fields searchField, object searchValue, string strParamAdicionales);
		entSegPagina ObtenerObjeto(entSegPagina.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Dictionary<String, entSegPagina> ObtenerDiccionario();
		Dictionary<String, entSegPagina> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, entSegPagina> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Dictionary<String, entSegPagina> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, entSegPagina> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Dictionary<String, entSegPagina> ObtenerDiccionario(entSegPagina.Fields searchField, object searchValue);
		Dictionary<String, entSegPagina> ObtenerDiccionario(entSegPagina.Fields searchField, object searchValue, ref cTrans localTrans);
		Dictionary<String, entSegPagina> ObtenerDiccionario(entSegPagina.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, entSegPagina> ObtenerDiccionario(entSegPagina.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entSegPagina> ObtenerLista();
		List<entSegPagina> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entSegPagina> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entSegPagina> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entSegPagina> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entSegPagina> ObtenerLista(entSegPagina.Fields searchField, object searchValue);
		List<entSegPagina> ObtenerLista(entSegPagina.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entSegPagina> ObtenerLista(entSegPagina.Fields searchField, object searchValue, string strParamAdicionales);
		List<entSegPagina> ObtenerLista(entSegPagina.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		List<entSegPagina> ObtenerLista(Hashtable htbFiltro);
		List<entSegPagina> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<entSegPagina> ObtenerLista(Hashtable htbFiltro, ref cTrans localTrans);
		List<entSegPagina> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		
		List<entSegPagina> ObtenerListaDesdeVista(String strVista);
		List<entSegPagina> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<entSegPagina> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		List<entSegPagina> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<entSegPagina> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		List<entSegPagina> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<entSegPagina> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref cTrans localTrans);
		List<entSegPagina> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<entSegPagina> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref cTrans localTrans);
		List<entSegPagina> ObtenerListaDesdeVista(String strVista, entSegPagina.Fields searchField, object searchValue);
		List<entSegPagina> ObtenerListaDesdeVista(String strVista, entSegPagina.Fields searchField, object searchValue, ref cTrans localTrans);
		List<entSegPagina> ObtenerListaDesdeVista(String strVista, entSegPagina.Fields searchField, object searchValue, string strParamAdicionales);
		List<entSegPagina> ObtenerListaDesdeVista(String strVista, entSegPagina.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Queue<entSegPagina> ObtenerCola();
		Queue<entSegPagina> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<entSegPagina> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Queue<entSegPagina> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<entSegPagina> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Queue<entSegPagina> ObtenerCola(entSegPagina.Fields searchField, object searchValue);
		Queue<entSegPagina> ObtenerCola(entSegPagina.Fields searchField, object searchValue, ref cTrans localTrans);
		Queue<entSegPagina> ObtenerCola(entSegPagina.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<entSegPagina> ObtenerCola(entSegPagina.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		Stack<entSegPagina> ObtenerPila();
		Stack<entSegPagina> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<entSegPagina> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		Stack<entSegPagina> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<entSegPagina> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);
		Stack<entSegPagina> ObtenerPila(entSegPagina.Fields searchField, object searchValue);
		Stack<entSegPagina> ObtenerPila(entSegPagina.Fields searchField, object searchValue, ref cTrans localTrans);
		Stack<entSegPagina> ObtenerPila(entSegPagina.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<entSegPagina> ObtenerPila(entSegPagina.Fields searchField, object searchValue, string strParamAdicionales, ref cTrans localTrans);
		
		string CreatePK(string[] args);
		
		bool Insert(entSegPagina obj);
		bool Insert(entSegPagina obj, ref cTrans localTrans);
		int Update(entSegPagina obj);
		int Update(entSegPagina obj, ref cTrans localTrans);
		int Delete(entSegPagina obj);
		int Delete(entSegPagina obj, ref cTrans localTrans);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(entSegPagina.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(entSegPagina.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entSegPagina.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, entSegPagina.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entSegPagina.Fields valueField, entSegPagina.Fields textField);
		void CargarComboBox(ref ComboBox cmb, entSegPagina.Fields valueField, entSegPagina.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entSegPagina.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, entSegPagina.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entSegPagina.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entSegPagina.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entSegPagina.Fields valueField, String textField, entSegPagina.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entSegPagina.Fields valueField, String textField, entSegPagina.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entSegPagina.Fields valueField, entSegPagina.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, entSegPagina.Fields valueField, entSegPagina.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, entSegPagina.Fields valueField, entSegPagina.Fields textField, entSegPagina.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, entSegPagina.Fields valueField, entSegPagina.Fields textField, entSegPagina.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, entSegPagina.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, entSegPagina.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entSegPagina.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entSegPagina.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(entSegPagina.Fields refField);
		int FuncionesCount(entSegPagina.Fields refField, entSegPagina.Fields whereField, object valueField);
		int FuncionesCount(entSegPagina.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(entSegPagina.Fields refField);
		int FuncionesMin(entSegPagina.Fields refField, entSegPagina.Fields whereField, object valueField);
		int FuncionesMin(entSegPagina.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(entSegPagina.Fields refField);
		int FuncionesMax(entSegPagina.Fields refField, entSegPagina.Fields whereField, object valueField);
		int FuncionesMax(entSegPagina.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(entSegPagina.Fields refField);
		int FuncionesSum(entSegPagina.Fields refField, entSegPagina.Fields whereField, object valueField);
		int FuncionesSum(entSegPagina.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(entSegPagina.Fields refField);
		int FuncionesAvg(entSegPagina.Fields refField, entSegPagina.Fields whereField, object valueField);
		int FuncionesAvg(entSegPagina.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

