﻿namespace ReAl.Opinion.BitBucket.Models
{
    public class EmailModel
    {
        public bool Active { get; set; }
        public string Email { get; set; }
        public bool Primary { get; set; }
    }
}
