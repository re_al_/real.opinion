namespace ReAl.Opinion.BitBucket.Models
{
    public class SSHKeyModel
    {
        public string Pk { get; set; }
        public string Key { get; set; }
    }
}

