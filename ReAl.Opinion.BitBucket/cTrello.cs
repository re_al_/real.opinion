﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace ReAl.Opinion.BitBucket
{
    public class cTrello
    {
        public static void CreateCard(String ListName, String CardName, String CardDescription, out String CardId)
        {
            ArrayList Boards, Lists;
            Hashtable Card;

            //retreive json string containing all the boards token has access to
            string BoardsJSON = GetBoards("https://trello.com/1/members/my/boards?");

            //parse the json string into an arraylist of hashtables
            Boards = (ArrayList)JSON.JsonDecode(BoardsJSON);

            //find the id of the board that we are interested
            string BoardId = FindValueInArrayList(Boards, "name", (string)cParametrosIssues.strTrelloBoardName, "id");

            if (BoardId != null)
            {
                //retrieve json string containing all the lists in the board
                string ListsJSON = GetLists(String.Format("https://trello.com/1/boards/{0}/lists?", BoardId));

                //parse the json string into an arraylist of hashtables
                Lists = (ArrayList)JSON.JsonDecode(ListsJSON);

                //find the id of the list that we want to create the card in
                string ListId = FindValueInArrayList(Lists, "name", (string)ListName, "id");

                if (ListId == null)
                {
                    throw new Exception("List not found. Check that list name is correct and that the token has access to the list");
                }

                //create the card in the correct list and retrieve a json string containing the new card
                string CardJSON = PostCard(String.Format("https://api.trello.com/1/lists/{0}/cards?", ListId), CardName, CardDescription);

                //parse the json string into a hashtable
                Card = (Hashtable)JSON.JsonDecode(CardJSON);

                //extract the id of the card and return it through the output paramter
                CardId = (string)Card["id"];

                return;
            }
            else
            {
                throw new Exception("Board not found. Check that board name is correct and that the token has access to the board");
            }

        }

        public static void AddCommentsToCard(String CardId, String CardComments)
        {
            Hashtable Comment;

            //create the comment against the card and retrieve a json string
            string CommentsJSON = PostComment(String.Format("https://trello.com/1/cards/{0}/actions/comments?", CardId), CardComments);

            //parse the json string into a hashtable
            Comment = (Hashtable)JSON.JsonDecode(CommentsJSON);
        }

        static string GetBoards(string GetBoardsUrl)
        {
            //build up correct url + querystring and make request
            var Boards = SendRequest(GetBoardsUrl + String.Format("key={0}&token={1}", cParametrosIssues.strTrelloApiKey, cParametrosIssues.strTrelloToken) +
                "&fields=name,closed,url", "GET");
            return Boards;
        }

        static string GetLists(string GetListsUrl)
        {
            //build up correct url + querystring and make request
            var Lists = SendRequest(GetListsUrl + String.Format("key={0}&token={1}", cParametrosIssues.strTrelloApiKey, cParametrosIssues.strTrelloToken) +
                "&fields=name,closed&cards=none", "GET");
            return Lists;
        }

        static string PostCard(string PostCardUrl, String CardName, String CardDescription)
        {
            //build up correct url + querystring and make request
            var Card = SendRequest(PostCardUrl + String.Format("key={0}&token={1}", cParametrosIssues.strTrelloApiKey, cParametrosIssues.strTrelloToken) +
                String.Format("&name={0}&desc={1}", CardName, CardDescription), "POST");
            return Card;
        }

        static string PostComment(string PostCommentsUrl, String CardComments)
        {
            //build up correct url + querystring and make request
            var Card = SendRequest(PostCommentsUrl + String.Format("key={0}&token={1}", cParametrosIssues.strTrelloApiKey, cParametrosIssues.strTrelloToken) +
                String.Format("&text={0}", CardComments), "POST");
            return Card;
        }

        static string FindValueInArrayList(ArrayList Hashtables, string SearchKey, string SearchValue, string ReturnKey)
        {
            foreach (Hashtable h in Hashtables)
            {
                if ((string)h[SearchKey] == SearchValue)
                {
                    return (string)h[ReturnKey];
                }
            }
            return null;
        }

        static string SendRequest(string Url, string Method)
        {
            //create new web request
            WebRequest wc = WebRequest.Create(Url);
            wc.Method = Method;

            //get response stream from the request and read it into a string. this will be a json string
            WebResponse wr = wc.GetResponse();
            Stream receiveStream = wr.GetResponseStream();
            StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
            string json = readStream.ReadToEnd();

            //tidy up
            receiveStream.Close();
            readStream.Close();
            wr.Close();

            readStream.Dispose();
            receiveStream.Dispose();

            return json;
        }
    }
}
