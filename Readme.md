ReAl Opinion
=========

ReAl Opinion es una herramienta que permite crear catalogos de preguntas, respuestas, flujos y saltos para Encuestas.


Version
----

1.4

 
Equipo de Desarrollo
-----------

Desarrollado por Ing. Reynaldo Alonzo Vera Arias 

  - [Correo Electrónico]
  - [Twiter]

[Correo Electrónico]:mailto:7.re.al.7@gmail.com?Subject=ReAlOpinion
[Twiter]:https://twitter.com/re_al_