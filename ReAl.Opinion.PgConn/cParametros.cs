namespace ReAl.Opinion.PgConn
{
    static public class cParametros
    {
        private enum miEntornoEnum
        {
            DESARROLLO,
            PRODUCCION,
            VIRTUAL
        }

        private const miEntornoEnum miEntorno = miEntornoEnum.VIRTUAL;

        //Constructor
        static cParametros()
        {
            switch (miEntorno)
            {
                case miEntornoEnum.VIRTUAL:
                    bChangeUserOnLogon = true;
                    bUseIntegratedSecurity = false;
                    server = "127.0.0.1";
                    puerto = "5432";
                    user = "postgres";
                    pass = "Desa2015Desa";
                    schema = "";
                    bd = "db_opinion";
                    userDefault = "postgres";
                    passDefault = "Des2015";
                    break;

                case miEntornoEnum.DESARROLLO:
                    bChangeUserOnLogon = true;
                    bUseIntegratedSecurity = false;
                    server = "127.0.0.1";
                    puerto = "5432";
                    user = "postgres";
                    pass = "Desa2015";
                    schema = "";
                    bd = "db_opinion";
                    userDefault = "postgres";
                    passDefault = "Des2015";
                    break;

                case miEntornoEnum.PRODUCCION:
                    bChangeUserOnLogon = true;
                    bUseIntegratedSecurity = false;
                    server = "pellefant-01.db.elephantsql.com";
                    puerto = "5432";
                    user = "jvpucjls";
                    pass = "pF9NMVEM-OGPAGfmOCRjXRBUbIlvAuDz";
                    schema = "";
                    bd = "jvpucjls";
                    userDefault = "jvpucjls";
                    passDefault = "pF9NMVEM-OGPAGfmOCRjXRBUbIlvAuDz";
                    break;
            }
        }

        //Parametros de conexion
        public static bool bChangeUserOnLogon;
        public static bool bUseIntegratedSecurity;
        public static string server;
        public static string puerto;
        public static string user;
        public static string pass;
        public static string schema;
        public static string bd;
        public static string userDefault;
        public static string passDefault;

        //Otros parametros
        public static string parFormatoFechaHora = "dd/MM/yyyy HH:mm:ss.ffffff";
        public static string parFormatoFecha = "dd/MM/yyyy";        
    }
}
